<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaction
 * 
 * @property int $TransactionID
 * @property int $UserWalletId
 * @property string $TransactionTypeCode
 * @property string $PromoCode
 * @property string $IsScheduled
 * @property string $CurrencyCode
 * @property string $TransactionStatus
 * @property string $FailureCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property float $Amount
 * 
 * @property \App\Models\Currency $currency
 * @property \App\Models\Promotion $promotion
 * @property \App\Models\Transactionfailurecode $transactionfailurecode
 * @property \App\Models\Transactiontype $transactiontype
 * @property \App\Models\Walletaccount $walletaccount
 * @property \Illuminate\Database\Eloquent\Collection $notificationlogs
 * @property \App\Models\Partypreferredpaymentmethod $partypreferredpaymentmethod
 * @property \App\Models\Transactionacknowledgement $transactionacknowledgement
 * @property \App\Models\Transactionschedule $transactionschedule
 * @property \Illuminate\Database\Eloquent\Collection $walletaccountdaybooks
 * @property \App\Models\Walletrecharge $walletrecharge
 *
 * @package App\Models
 */
class Transaction extends Eloquent
{
	protected $primaryKey = 'TransactionID';
	public $timestamps = false;

	protected $casts = [
		'UserWalletId' => 'int',
		'Amount' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletId',
		'TransactionTypeCode',
		'PromoCode',
		'IsScheduled',
		'CurrencyCode',
		'TransactionStatus',
		'FailureCode',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'Amount'
	];

	public function currency()
	{
		return $this->belongsTo(\App\Models\Currency::class, 'CurrencyCode');
	}

	public function promotion()
	{
		return $this->belongsTo(\App\Models\Promotion::class, 'PromoCode');
	}

	public function transactionfailurecode()
	{
		return $this->belongsTo(\App\Models\Transactionfailurecode::class, 'FailureCode');
	}

	public function transactiontype()
	{
		return $this->belongsTo(\App\Models\Transactiontype::class, 'TransactionTypeCode');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'UserWalletId', 'UserWalletId');
	}

	public function notificationlogs()
	{
		return $this->hasMany(\App\Models\Notificationlog::class, 'LinkedTransactionID');
	}

	public function partypreferredpaymentmethod()
	{
		return $this->hasOne(\App\Models\Partypreferredpaymentmethod::class, 'PaymentMethodID');
	}

	public function transactionacknowledgement()
	{
		return $this->hasOne(\App\Models\Transactionacknowledgement::class, 'TransactionID');
	}

	public function transactionschedule()
	{
		return $this->hasOne(\App\Models\Transactionschedule::class, 'TransactionID');
	}

	public function walletaccountdaybooks()
	{
		return $this->hasMany(\App\Models\Walletaccountdaybook::class, 'TransactionID');
	}

	public function walletrecharge()
	{
		return $this->hasOne(\App\Models\Walletrecharge::class, 'TransactionID');
	}
}
