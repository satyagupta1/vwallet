<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Cardbeneficiary
 * 
 * @property int $CardBeneficiaryId
 * @property int $CardUsers_id
 * @property string $BeneficiaryType
 * @property string $CardNumber
 * @property string $TripleClickAmount
 * @property string $Comments
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreataedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Carduser $carduser
 *
 * @package App\Models
 */
class Cardbeneficiary extends Eloquent
{
	protected $table = 'cardbeneficiary';
	protected $primaryKey = 'CardBeneficiaryId';
	public $timestamps = false;

	protected $casts = [
		'CardUsers_id' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CardUsers_id',
		'BeneficiaryType',
		'CardNumber',
		'TripleClickAmount',
		'Comments',
		'CreatedDateTime',
		'CreataedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function carduser()
	{
		return $this->belongsTo(\App\Models\Carduser::class, 'CardUsers_id');
	}
}
