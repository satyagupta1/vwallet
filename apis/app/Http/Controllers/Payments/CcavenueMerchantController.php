<?php

namespace App\Http\Controllers\Payments;

/**
 * PaymentsMerchantController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */
class CcavenueMerchantController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->encKey                = sha1(config('vwallet.ENCRYPT_SALT'));
        $this->workingKeyCcA         = config('vwallet.workingKeyCcA');  //Working Key should be provided here.
        $this->accessCodeCcA         = config('vwallet.accessCodeCcA');  //Access Key should be provided here.
        $this->doWebTransCcavenueUrl = config('vwallet.doWebTransCcavenueUrl');
    }

    /*
     *  Get CcAvenue Response
     *  @param Request $request
     *  @return HTML
     */

    public function kotakWebTransaction(\Illuminate\Http\Request $request)
    {

        $referenceNo   = trim($request->input('referenceNo'));
        $amount        = $request->input('amount');
        $command       = $request->input('command');
        //print_r($request->input('referenceNo'));exit;
        $baseUrl       = config('vwallet.baseUrl');
        $output        = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validate($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }

        //$requestArray = ['order_List' => [['reference_no' => '306003412816', 'amount' => '14.0']]]; 
        //$command = 'confirmOrder';
        if ( $command != 'refundOrder' )
        {
            //$requestArray = ['reference_no' => '306003412816', 'refund_amount' => '1.00', 'refund_ref_no' => '13112017114417344469'];  
            $output['display_msg']['info'] = $err;
            return response()->json($output);
        }

        $transactionOrderNumber = $request->input('transactionOrderNumber');

        $requestArray = [ 'reference_no' => $referenceNo, 'refund_amount' => $amount, 'refund_ref_no' => $transactionOrderNumber ];

        $jsonOrder = json_encode($requestArray);
        //print_r($jsonOrder);exit;

        $encRequestData = \App\Libraries\Payments\Ccavenue::encrypt($jsonOrder, $this->workingKeyCcA);

        $requestUrl = $this->doWebTransCcavenueUrl . "?enc_request=" . $encRequestData . "&access_code=" . $this->accessCodeCcA . "&request_type=JSON&response_type=JSON&command=" . $command . "&version=1.1";
        $curl       = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_HTTPHEADER     => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ( $err )
        {
            $output['display_msg']['info'] = $err;
            return response()->json($output);
        }
                

        $getArray           = array();
        parse_str($response, $getArray);
        $encryptedResponse  = trim($getArray['enc_response']);
        $decryptRequestData = \App\Libraries\Payments\Ccavenue::decrypt($encryptedResponse, $this->workingKeyCcA);
        $statusArray        = json_decode($decryptRequestData, TRUE);
        /*print_r($getArray);
        echo "<br>";
        print_r($decryptRequestData);
        echo "<br>";
        print_r($statusArray);
        echo $statusArray['refund_status'];exit;
         * 
         */
        //Insert into log messages
        $select            = array(
            'TransactionDetailID',
            'TransactionOrderID',
            'PaymentsProductID',
            'UserWalletId',
            'FromAccountId',
            'ToAccountId',
            'TransactionDetailID',
            'FeeAmount',
            'TaxAmount',
            'TransactionType',
            'Amount',
            'ToWalletId'
        );
        $dateTimeOfTrans   = date('Y-m-d H:i:s');
        $update_txn_detail = \App\Models\Transactiondetail::select($select)
                        ->where('TransactionOrderID', $transactionOrderNumber)->first();

        if ( $statusArray['refund_status'] == "0" )
        { //status success
            $sendData     = array( 'refundFrom' => 'ccavenue', 'transactionId' => $update_txn_detail->TransactionDetailID, 'partyId' => $update_txn_detail->UserWalletId, 'amount' => $amount );
            $request->request->add($sendData);
            $api_response = app(\App\Http\Controllers\Topup\TopupReversal::class)->topUpReversal($request);
        }
        //Log CCAVenue refund response
        //$path                = 'SystemLog/CCARefund/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $transDetailId       = $update_txn_detail->TransactionDetailID;
        $uniqueId = uniqid();
       // \App\Libraries\Vwallet::ResponseLog($path, $decryptRequestData, $transDetailId);
        $paramsData = array('transDetailId' => $transDetailId, 'messageType'=>'CCARefund', 'uniqueId'=>$uniqueId);
        \App\Libraries\Vwallet::ResponseLog($decryptRequestData,$paramsData);
            
        //$transactionMessages = array( 'TransactionDetailID' => $transDetailId, 'MessageType' => 'CCARefund', 'MessageFilePath' => $path, 'CreatedBy' => config('vwallet.adminTypeKey'), 'CreatedDateTime' => $dateTimeOfTrans );
        //\App\Models\Transactionapimessage::insertGetId($transactionMessages);
        
        $output['is_error'] = FALSE;
        $output['res_data'] = $decryptRequestData;
        return response()->json($output); 

        //$sendData = array('refundFrom' => 'ccavenue', 'transactionId' => '461', 'partyId' => '26', 'amount' => $amount);
        //$request->request->add($sendData);
        //$api_response = app(\App\Http\Controllers\Topup\TopupReversal::class)->topUpReversal($request);
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validate($request)
    {
        $messages                           = [];
        //$validate['command'] = 'required|in:confirmOrder,cancelOrder,refundOrder,orderStatusTracker,orderLookup,getPendingOrders';
        $validate['command']                = 'required|in:refundOrder';
        $validate['referenceNo']            = 'required|numeric';
        $validate['amount']                 = 'required|regex:/^\d+([.]\d{1,2})?$/';
        //if($request->input('command') == 'refundOrder') {
        $validate['transactionOrderNumber'] = 'required';
        //}
        $validate_response                  = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}

/* End of file CcavenueController.php */ /* Location: ./Http/Test/TestController.php */
