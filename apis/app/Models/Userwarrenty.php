<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userwarrenty
 * 
 * @property int $UserWalletID
 * @property int $WarrentyID
 * @property string $WarrentyTitle
 * @property string $ProductName
 * @property string $PurchaseDate
 * @property string $ExpiryDate
 * @property float $ProductAmount
 * @property string $TransactionID
 * @property string $SerialNumber
 * @property string $InvoiceNumber
 * @property string $Notes
 * @property \Carbon\Carbon $ReminderStartTime
 * @property \Carbon\Carbon $ReminderEndTime
 * @property int $InvoiceDocumentID
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Partydocument $partydocument
 * @property \App\Models\Userwallet $userwallet
 * @property \Illuminate\Database\Eloquent\Collection $usertransactionsurveys
 *
 * @package App\Models
 */
class Userwarrenty extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'WarrentyID' => 'int',
		'ProductAmount' => 'float',
		'InvoiceDocumentID' => 'int'
	];

	protected $dates = [
		'ReminderStartTime',
		'ReminderEndTime',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'WarrentyTitle',
		'ProductName',
		'PurchaseDate',
		'ExpiryDate',
		'ProductAmount',
		'TransactionID',
		'SerialNumber',
		'InvoiceNumber',
		'Notes',
		'ReminderStartTime',
		'ReminderEndTime',
		'InvoiceDocumentID',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partydocument()
	{
		return $this->belongsTo(\App\Models\Partydocument::class, 'InvoiceDocumentID');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function usertransactionsurveys()
	{
		return $this->hasMany(\App\Models\Usertransactionsurvey::class, 'UserWalletID');
	}
}
