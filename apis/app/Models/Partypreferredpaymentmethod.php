<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partypreferredpaymentmethod
 * 
 * @property int $PaymentMethodID
 * @property int $PartyID
 * @property string $MoneySourceCode
 * @property string $CardVendor
 * @property int $CardNumber
 * @property string $CardExpiryDate
 * @property int $CardCVVNumber
 * @property string $NetBankCode
 * @property string $SaveCardReference
 * @property float $RequestedAmount
 * @property int $TransactionDetailID
 * @property int $DailyTransactionSequenceNumber
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Dailytransactionlog $dailytransactionlog
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \App\Models\Moneysource $moneysource
 * @property \App\Models\Transaction $transaction
 *
 * @package App\Models
 */
class Partypreferredpaymentmethod extends Eloquent
{
	protected $table = 'partypreferredpaymentmethod';
	protected $primaryKey = 'PaymentMethodID';
	public $timestamps = false;

	protected $casts = [
		'PaymentMethodID' => 'int',
		'PartyID' => 'int',
		'CardNumber' => 'string',
		'CardCVVNumber' => 'int',
		'RequestedAmount' => 'float',
		'TransactionDetailID' => 'int',
		'DailyTransactionSequenceNumber' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyID',
		'MoneySourceCode',
		'CardVendor',
		'CardNumber',
		'CardExpiryDate',
		'CardCVVNumber',
		'NetBankCode',
		'SaveCardReference',
		'RequestedAmount',
		'TransactionDetailID',
		'DailyTransactionSequenceNumber',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
                'CardHolderName'
	];

	public function dailytransactionlog()
	{
		return $this->belongsTo(\App\Models\Dailytransactionlog::class, 'DailyTransactionSequenceNumber');
	}

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}

	public function moneysource()
	{
		return $this->belongsTo(\App\Models\Moneysource::class, 'MoneySourceCode');
	}

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'PaymentMethodID');
	}
}
