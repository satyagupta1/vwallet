<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Viola Viola Wallet Messages Language Lines
      |--------------------------------------------------------------------------
      |
      |
     */
    'accepted'   => 'The :attribute must be accepted.',
    'fromAmount' => [
        'between' => 'The from amount must be valid range.',
    ],
    'toAmount'   => [
        'between' => 'The to amount must be between valid range',
    ],
    /*
      |--------------------------------------------------------------------------
      | Viola Viola Wallet Web FAQ'S 
      |--------------------------------------------------------------------------
      |
      |
     */
    'webFaqs'    => [
        [
            'title' => 'About ViolaWallet',
            'body'  => [
                [
                    'question' => 'How can I contact ViolaWallet?',
                    'answer'     => 'We\'re available 24/7 for our customers and you can drop us a line on support@violawallet.com You can also give us a call at +91 90300-84652(Viola) between 9AM-9PM all 7 days (excluding National Holidays).'
                ],
                [
                    'question' => ' Where do I spend the money on my ViolaWallet account?',
                    'answer'     => 'You can spend the money loaded onto your ViolaWallet account for a variety of things. You can send money to or request money from friends and family. You can split bills with your friends when you dine out, collect money for movie tickets from your friends, remind friends that they owe you money and more! You can of course also use it to pay with select merchants online.'
                ],
                [
                    'question' => 'What can I do with ViolaWallet?',
                    'answer'     => '<p>You can:</p>
  <ul> 
      <li>Deposit money to Wallet</li>  
      <li>Prepaid Recharge</li> 
 <li>Postpaid bill payments</li> 
 <li>DTH Recharge</li> 
 <li>Utility Payments</li> 
 <li> Payment at Merchants</li> 
  </ul>'
                ],
                [
                    'question' => 'What are the benefits of using ViolaWallet?',
                    'answer'     => '<ul>    
      <li>By registering for ViolaWallet, you would gain access to a variety of services, which include:</li>
      <li>Secure cash-free transactions anytime from anywhere</li>
<li>Securely store all your credit/debit cards and bank accounts for convenient and faster payments</li>
<li>Make in-store and online payments across a variety of merchants</li>
<li> Recharge Mobile/DTH using a single app</li>
<li>Get great offers, deals and coupons from top brands and also from your neighborhood stores</li>
  </ul>'
                ],
                [
                    'question' => 'What are the modes/channels to register on ViolaWallet?',
                    'answer'   => '<p>You can register through:</p>
    <ul>
	<li>ViolaWallet Web application</li>
	<li>ViolaWallet Mobile App</li>
	<li>ViolaWallet Call Centre 90300-84652(Viola)</li>
</ul>'
                ],
                [
                    'question' => 'I have not turned 10 yet. Can I sign up for ViolaWallet?',
                    'answer'     => 'To sign up for ViolaWallet, your minimum age should be 18 years (on your last birthday).'
                ],
                [
                    'question' => 'Do you verify my e-mail ID?',
                    'answer'   => 'Yes, we will verify your email id by sending you a link. If you do not receive the email, check your spam or junk mail folder or click on resend tab on sign up page to get new link.'
                ],
                [
                    'question' => 'Do you verify my mobile number?',
                    'answer'   => 'Yes. We will verify your mobile number by sending a mobile verification code on your registered mobile number. If you do not receive a mobile verification code within 10 minutes, click on the resend verification tab on the sign up page to get a new mobile verification code.'
                ],
                [
                    'question' => 'Which phones are supported by ViolaWallet?',
                    'answer'   => 'ul>
	<li>Users can install ViolaWallet on all Android devices with OS 4.4 and above</li>
	<li>For apple users, ViolaWallet is available for iOS devices with 9.3 version or higher. </li>
	<li>If you are on an older version, please make sure you upgrade your phone before you install ViolaWallet application. Else, you can always go to our website ViolaWallet.in -to access our services.</li>
</ul>'
                ],
                [
                    'question' => 'Forgot Password?',
                    'answer'   => 'To set a new password, click on "Forgot Password" . Once you enter your credentials, a One-Time Passcode(OTP) will be sent to your email id through which you\'ll be able to set a new password'
                ],
                [
                    'question' => 'How can I download the ViolaWallet App ?',
                    'answer'   => '<p>Anyone can download the ViolaWallet App using any Android or Apple Smartphone from the Google PlayStore or iOS App store. <br>Follow the below steps:</p>
     <ul>
	<li>Users can install ViolaWallet on all Android devices with OS ·	Just click on the Google PlayStore or iOS App store icon</li>
	<li>·	Search for \'ViolaWallet\'</li>
	<li>·	Click on Install</li>
	<li>·	Do a quick one time registration before  using ViolaWallet</li>
</ul>'
                ],               
            ],
        ],
        [
            'title' => 'Wallet Upgrade',
            'body'  => [
                [
                    'question' => 'Who is a ViolaWallet KYC Customer?',
                    'answer'     => 'When you Sign Up on ViolaWallet, a wallet gets created for you in which you can add upto Rs. 10,000/- in a calendar month. However, you can upgrade your wallet for FREE by getting your KYC (Know Your Customer) done and increase your limit to 100,000. This would entitle you for  amazing offers and cashbacks'
                ],
                [
                    'question' => 'How do I become a ViolaWallet KYC customer?',
                    'answer'     => '<p>In order to become a ViolaWallet KYC customer, you need to get your KYC (Know Your Customer) done . You can do this by uploading your documents ( address proof and identity proof) for  verification on ViolaWallet app itself. You can find the “ViolaWallet KYC” section on the Profile Page.
 
</p>
<ul>
	<li>	Request a visit from our agent at your home/office</li>
	<li>	Verify documents at your nearest ViolaWallet KYC centre</li>
	<li>	You can also follow the URL: www.violawallet.com</li>
</ul>'
                ],
                [
                    'question' => 'What are the documents needed to complete my KYC?',
                    'answer'     => '<p>In order to complete your KYC, we need to verify your Aadhaar card electronically or physically.
<br>Other accepted documents are
</p>
<ul>
	<li>	PAN Card</li>
	<li>	Voter ID</li>
	<li>	Passport</li>
	<li>	Driving license</li>
</ul>'
                ],
                [
                    'question' => 'Are there any limits on an upgraded (Full KYC) wallet?',
                    'answer'     => '<p>You can add up to 1,000,000 in an year, not exceeding 100,000 in a month  
Coupons/Deals/promo code  </p>'
                ],
            ],
        ],
        [
            'title' => 'Coupons/Deals/promo code',
            'body'  => [
                [
                    'question' => 'What is promo code?',
                    'answer'     => 'Promo code is a promotional code directly from online store to promote the products and using promo code you can get discounts from the respective store on your checkout cart.'
                ],
                [
                    'question' => 'What is coupon?',
                    'answer'     => 'Coupon is a code, which you can use on the shopping site to avail a discount.'
                ],
                [
                    'question' => 'How do I use a promo code?',
                    'answer'     => 'If you have a promo code for your purchase or recharge, you can add it on the checkout page.'
                ],
                [
                    'question' => 'Can I use the coupon multiple times?',
                    'answer'     => 'No, we do not allow user to use the same coupon multiple times.'
                ],
                [
                    'question' => 'How often do you update your offers?',
                    'answer'     => 'Updates happen round the clock. We keep adding Coupons & Deals on every day basis as well.'
                ],
                [
                    'question' => 'Will I be charged extra for the coupons?',
                    'answer'     => 'There is no extra charge for the coupons. However, we do have exclusive coupons that are paid. The price will be mentioned on the individual coupons. You will not be charged anything apart from the recharge amount if you do not opt for any coupons.'
                ],
                [
                    'question' => 'What should I do if coupons have not been received after a successful recharge',
                    'answer'     => 'You can write us an email at support@violawallet.com  with the order ID and we’ll re-send the coupons to you. Before that, please check your “Spam” and “Promotions” folder.'
                ],
                [
                    'question' => 'Can I give these coupons to anybody?',
                    'answer'     => 'As our offers are curated, Only who can see the offer can utilize it.'
                ],
                [
                    'question' => ' What is the validity of the coupons selected?',
                    'answer'     => 'Different coupons have different validity and this is mentioned in the Terms & Conditions o f each coupon. You can go through the same and redeem the coupons within the respective validity period.'
                ],
            ],
        ],
        [
            'title' => 'Referral Code',
            'body'  => [
                [
                    'question' => 'What is the ViolaWallet Refer and Earn Program?',
                    'answer'     => '<p>
     When you refer your friend, both of you get 50/ post your friend has made his/her first successful recharge/ bill payment.
     </p>
     <ul>
	<li>Every user should be assigned with a Referral Code upon Registration</li>
	<li>User should be able to refer his friends using the Referral Code</li>
	<li>System should track the first ever transactions for each user in services category such as Bill Pay, Recharges etc.</li>
</ul>'
                ],
                [
                    'question' => 'Can I invite anyone?',
                    'answer'     => 'Yes, you can invite any of your friends or acquaintances. However, to ensure that users don\'t get spammed, please ensure that you only send invites to people you are acquainted with.'
                ],                
                [
                    'question' => 'How long is the referral link valid for?',
                    'answer'     => 'Depends on the offer'
                ],                
                [
                    'question' => 'Is there a limit on the number of email invites I can send? ',
                    'answer'     => 'Currently, NO'
                ],                
            ],
        ],
        [
            'title' => 'Sign-up/Login Page',
            'body'  => [
                [
                    'question' => ' I tried to transfer money from wallet to bank account. Money deducted from wallet, but not reflecting in your bank account?',
                    'answer'     => 'ViolaWallet customers can transfer money from their ViolaWallets to a bank account at any time. We process this transaction instantly and in most cases[Duplicate question with different answers] money is reflected in user’s Bank accounts within few minutes. However, when the banking systems are going through stress due load spikes, banks could take typically upto 7 working days to credit the amount in your bank account.'
                ],
                [
                    'question' => 'I tried adding money to my wallet. Money deducted from bank account but not reflected in your ViolaWallet wallet?',
                    'answer'     => 'If you receive a notification from the bank saying money has been deducted, but the amount doesn’t reflect in your ViolaWallet account, don’t worry. Your money is absolutely safe in the banking system. Transaction goes in pending when we don’t receive the final confirmation from your bank or ViolaWallet gateway. These exceptions happen when banking payment gateways are facing some technical issues or load spikes.  Banks could take typically upto 7 working days to credit the amount in your bank account. '
                ],                
            ],
        ],
        [
            'title' => 'My Account',
            'body'  => [
                [
                    'question' => 'How can I view my account limits?',
                    'answer'     => 'You can go to wallet option on Dasboard and can check your limits'
                ],
                [
                    'question' => 'How can I stop receiving emails or messages from ViolaWallet?',
                    'answer'     => 'It is possible to unsubscribe our promotional emails. Click on the unsubscribe link at the bottom of the promotional emails  or just write to us at support@violawallet.com . However, you will still receive transactional emails or messages from ViolaWallet.'
                ],                
                [
                    'question' => 'How can I check the status of a transaction?',
                    'answer'     => 'In order to check the status of your transaction, log in to your ViolaWallet and select \'Transaction History\' tab from the menu.'
                ],                
                [
                    'question' => 'I am unable to access my account because of security reasons, what should I do ?',
                    'answer'     => 'In such cases, please contact support@violawallet.com '
                ],                
                [
                    'question' => 'Is my account information safe?',
                    'answer'     => 'Don\'t worry! ViolaWallet use your mobile number for identification and correspondence and does not share any of your bank details with anyone. All the information transmitted between you and ViolaWallet is guarded by our advanced encryption which ensures that any unauthorized access (hacking) is rejected at the entry level. For additional security, whenever you try to send money through www.ViolaWallet.com; an OTP (One Time Password) is sent to your registered mobile number.'
                ],                
                [
                    'question' => 'Is there an expiry date on the account?',
                    'answer'     => 'Yes, if your account remains inactive for 6 months, we hold the right to make your account inactive. We will be sending you an email notifying you about your account inactivity as well two more communications before your account expires.'
                ],                
                [
                    'question' => 'What are my monthly limits?',
                    'answer'     => 'If you are a non-KYC customer, you have a monthly incoming limit of Rs.10, 000. Your balance limit is also Rs 10,000.'
                ],                
                [
                    'question' => 'How do I dispute an error on my ViolaWallet account?',
                    'answer'     => 'We have built stringent checks at our end to ensure that there is no error/dispute while you transact through ViolaWallet. In fact, after each transaction you will receive a SMS and/or email alert from us and a monthly e-statement detailing your transactions for the given month. We also recommend that you periodically keep checking your wallet summary for your transaction history. However, if you suspect an error on your statement or receipt or you need more information about a transfer listed on your statement, we request you to let us know about the probable anomaly within sixty (60) days after we sent the FIRST statement on which the problem or error appeared. In order to reach us, please call us at +91 90300-84652 or write to us at support@violawallet.com.'
                ],                
                [
                    'question' => 'Is there a limit to receiving payments in ViolaWallet?',
                    'answer'     => 'You can accept payments up to Rs 10,000 per month in your Non-KYC ViolaWallet account. After the limit exceeds, the payments will automatically start flowing into your linked Bank account. You can also upgrade to a full KYC Wallet with Rs 100K monthly limit. To know more, reach us at +91 90300-84652 or email us at support@violawallet.com .'
                ],                
                [
                    'question' => 'How do I load Money on to my account? ',
                    'answer'     => '<p>To add money to your account</p>
     <ul>
	<li>Click on "Add Money" after logging in to your account.</li>
	<li>Choose the mode of transfer (debit/card/credit card or net banking).</li>
	<li>Enter the required card/net banking details.</li>
	<li>The amount will reflect in your account in no time.</li>
</ul>'
                ],                
            ],
        ],
        [
            'title' => 'Add money/Request money',
            'body'  => [
                [
                    'question' => 'What is add money?',
                    'answer'     => 'It means adding money to your wallet. Whenever you add money, we refer to it as "add money" and the amount you add reflects as your ViolaWallet balance.'
                ],
                [
                    'question' => 'Why is it necessary to add money to the wallet?',
                    'answer'     => 'ViolaWallet is a prepaid wallet therefore, you will have to load money to do transactions.'
                ],                
                [
                    'question' => 'What is Balance?',
                    'answer'     => 'Your \'Balance\' is the money that is available in your ViolaWallet after all pending transactions, if any, are processed.'
                ],                
                [
                    'question' => 'How do I request money from ViolaWallet?',
                    'answer'     => 'Go to \'Request Money\' tab. Enter the amount of money that you wish to request. Select a friend & send \'request money\' request.'
                ],                
            ],
        ],
        [
            'title' => 'Send money',
            'body'  => [
                [
                    'question' => 'Send Money through UPI',
                    'answer'     => 'Coming soon.'
                ],
                [
                    'question' => 'TripleClik',
                    'answer'     => 'Coming soon.'
                ],                
                [
                    'question' => 'Virtual cards',
                    'answer'     => 'Coming soon.'
                ],            
            ],
        ],
        [
            'title' => 'Recharges and Bill Payments',
            'body'  => [
                [
                    'question' => 'How can I recharge through SMS?',
                    'answer'     => 'This feature is coming soon.'
                ],
                [
                    'question' => 'What should I do if my recharge is not done?',
                    'answer'     => 'Wait for 5 minutes after the payment is successfully done. If your mobile number is still not recharged, Call us at 90300-84652 or write to us at support@violawallet.com . We will respond to it as soon as possible.'
                ],                
                [
                    'question' => 'Does the recharge happen immediately?',
                    'answer'     => 'Yes, you get your recharge immediately. As soon as the payment is made, you will get a confirmation mail from ViolaWallet.in and a recharge message from your mobile operator. Generally, it takes less than 10 seconds for the transaction to complete.'
                ],                
                [
                    'question' => 'Can I recharge without selecting coupons?',
                    'answer'     => 'Yes, you can also choose to recharge without opting for coupons. However, we would like you to reap maximum benefits from the coupons available.'
                ],                
                [
                    'question' => 'What are the fees for making a payment?',
                    'answer'     => 'For most of our services, such as prepaid recharges, postpaid bill payments, paying online merchants etc there are no charges. However, in case of utility bills (Electricity, Gas, Broadband, Landline), you are charged a minimum fee  of INRXX as it is  mandatory by BBPS to make this payment.'
                ],                
                [
                    'question' => 'How Do I make payments?',
                    'answer'     => 'We can make payments through UPI, Credit and Debit Cards, Net Banking.'
                ],                
                [
                    'question' => 'Can I pay bills through ViolaWallet?',
                    'answer'     => 'Yes you can pay bills through ViolaWallet. However, this convenience comes to you for a minimal convenience charge. '
                ],                
                [
                    'question' => 'Can I pay a bill of amount >10k on ViolaWallet?',
                    'answer'     => 'No, user cannot spend more than 5K per transaction. '
                ],                
                [
                    'question' => 'Can I transfer the cashback amount to my bank account?',
                    'answer'     => 'No, We won’t allow user to transfer Cashbacks to user’s bank account.'
                ],                
            ],
        ],
        [
            'title' => 'Foreign Exchange',
            'body'  => [
                [
                    'question' => 'What is foreign exchange?',
                    'answer'     => 'This feature will be coming soon.'
                ],                               
            ],
        ],
        [
            'title' => 'Text and Email Notifications',
            'body'  => [
                [
                    'question' => 'What are ViolaWallet email and text alerts?',
                    'answer'     => 'These are notifications about updates to your account or confirmations of payments or changes that are sent to you by email and/or text message. '
                ],
                [
                    'question' => 'What type of information can I receive with ViolaWallet alerts?',
                    'answer'     => '<p>There are three categories of alerts that you can choose to receive:</p>
     <ul>
	<li>>Bill ready</li>
	<li>		>Payment confirmations (includes when payments and credits are applied)</li>
	<li>		>Account activity confirmations (includes when plan and features changes are made)</li>
	<li>Go to the Email and Text Alert Preferences page in ViolaWallet for more details on these categories.</li>
</ul>'
                ],                
                [
                    'question' => 'Can I set multiple email addresses to receive alerts about my account?',
                    'answer'     => 'No, you cannot enter multiple email addresses to receive email alerts in ViolaWallet.'
                ],                
                [
                    'question' => 'I\'m no longer a ViolaWallet customer, but I\'m still receiving notifications by text and/or email. Why is this?',
                    'answer'     => 'If you\'ve cancelled your service but are still receiving alerts, this could mean that you have an outstanding payment balance. Please contact our Customer Support Team at 90300-84652.'
                ],                
            ],
        ],
        [
            'title' => 'Account ID, Secure ID, Passwords and Security Questions',
            'body'  => [
                [
                    'question' => 'Is my credit/debit card, net banking information secure on ViolaWallet?',
                    'answer'     => 'ViolaWallet uses highest level of encryption methodology and is PCI-DSS (Payment Card Industry Data Security Standard) certified to ensure the security of your credit/debit card, net banking information at all times. No matter what the transaction, no one can access the information but you,  So… yes your information is probably more secure on ViolaWallet than it is anywhere else! '
                ],
                [
                    'question' => 'Why am I receiving the new device alert even if I have used the device before?',
                    'answer'     => 'We might send you an alert for a device that you\'ve signed in from before. This can happen if you\'ve recently deleted cookies, updated your browser or an app, or used incognito mode to log in to your ViolaWallet account.'
                ],                
                [
                    'question' => 'When does one get these new device alerts?',
                    'answer'     => 'New device alert can be sent in case you have signed in for the first time on a new computer, phone or browser, when you use your browser\'s incognito or private browsing mode or clear your cookies, or when somebody else is accessing your account.'
                ],                
                [
                    'question' => 'My account was locked because of a suspicious activity. What should I do?',
                    'answer'     => 'If you have received an email or SMS from ViolaWallet stating that your account has been locked because of security reasons, we strongly recommend that you change your password. Changing password will secure your account and log you out from all other platforms where you are logged in with your ViolaWallet account.'
                ],                
                [
                    'question' => 'I am unable to access my account because of security reasons, what should I do?',
                    'answer'     => 'Please contact support@violawallet.com'
                ],                
                [
                    'question' => 'I am receiving many alerts. What should I do ?',
                    'answer'     => 'You can go to notifications and choose to disable notification modes'
                ],                
            ],
        ],
        [
            'title' => 'General queries',
            'body'  => [
                [
                    'question' => 'How many prepaid accounts can I have?',
                    'answer'     => 'Each user can have only one prepaid account. It’s an RBI guideline.'
                ],
                [
                    'question' => 'How do I know about the new offers from ViolaWallet?',
                    'answer'     => 'Please follow us on our official Facebook (www.facebook.com/ViolaWallet**) and Twitter (https://twitter.com/ViolaWallet**) page for new offers.'
                ],                
                [
                    'question' => 'Other than support@violawallet.com is there any other way to contact you?',
                    'answer'     => 'Yes, you can reach us through Facebook and Twitter or call us at 90300-84652.'
                ],                
            ],
        ],
        [
            'title' => 'Customer FAQ\'s',
            'body'  => [
                [
                    'question' => 'How do I protect my ViolaWallet account?',
                    'answer'     => 'DO NOT SHARE your ViolaWallet Password with anyone. Still if you feel, your ViolaWallet password has been compromised at any time, please write to us at support@violawallet.com  or simply SMS RESET to 987***. You will receive a new password on your registered mobile number, which you can change to your liking.
Please Note: We never ask for your password when you call us.'
                ],
                [
                    'question' => 'Do I need to maintain a minimum balance in my ViolaWallet?',
                    'answer'     => 'No, you can add as much money as you wish and use it.  We just help you secure your money electronically and use it for online payments or at pay points.'
                ],                
                [
                    'question' => 'I forgot my ViolaPIN, what should I do? ',
                    'answer'     => 'You can reset your PIN in your mobile/web application or  write to us at support@violawallet.com with transaction details. We will investigate and get back to you. '
                ],                
                [
                    'question' => 'Who is eligible to hold a ViolaWallet account?',
                    'answer'     => 'Any person aged above 10 years is eligible to hold a ViolaWallet account.'
                ],                
                [
                    'question' => 'Is my money safe if I lose my phone?',
                    'answer'     => 'Yes, your money is  100% safe with us even if you lose your phone. Nobody can access your ViolaWallet account, as you have the secure 5 digit ViolaPIN and password. . ViolaWallet locks your account for your own safety after 3 consecutive incorrect ViolaPIN attempts. If you suspect unauthorized activity in your account, you can also suspend your ViolaWallet account and block payments by calling our customer Support Team at 90300-84652 or write to us at support@violawallet.com .'
                ],                
                [
                    'question' => 'By mistake, if I recharge a  wrong number! Can I get my money back?',
                    'answer'     => 'If you accidentally recharge an incorrect mobile number, then please contact our Customer Support team & the money will be sent back to your account. However, if you send money to a correct but unknown person’s number, you can reach out to the person and request to charge your number.'
                ],                
                [
                    'question' => 'How a Merchant is different from a Retail Outlet?',
                    'answer'     => 'Retail Outlet (RO) is a business partner who does Recharges/Bill payments/Money transfers for their customers paying in cash by using a prepaid trading advance account of ViolaWallet. However, Merchant could be any business who register and uses ViolaWallet to accept payments from customers. It could be a grocery/Kirana store, a pharmacy, a doctor clinic, a hawker or a service provider selling goods or services.'
                ],                
                [
                    'question' => 'How do I use Chat & Pay?',
                    'answer'     => 'It\'s simple. Just click on chat icon sand select a contact from your contact list on ViolaWallet application'
                ],                
            ],
        ],
        /*[
            'title' => 'Heading xxxx',
            'body'  => [
                [
                    'question' => 'What is xxxx?',
                    'answer'     => 'xxxxx'
                ],
                [
                    'question' => 'What is xxxx?',
                    'answer'     => 'xxxx'
                ],                
            ],
        ],*/
    ]
];
