<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property string $BankCode
 * @property string $BankName
 * @property int $PreferencePriority
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $bankbranches
 *
 * @package App\Models
 */
class Pincodes extends Eloquent
{
	protected $table = 'pincodes';
	protected $primaryKey = 'PinCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'DistrictName',
		'StateName',
	];
        
        
}
