<?php

namespace App\Http\Controllers\Test;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Helpers;
use Validator;
use Log;
/**
 * TestController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */
class TestController extends Controller {
    /*
     *  Get country code
     *  @param Request $request
     *  @return JSON
     */

    public function countryCode(Request $request)
    {
        Log::Info($request);
        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate request parameters
        $validator = $this->_validateRequestDetails($request);
        Log::error(json_encode($validator));
        if ( $validator !== TRUE)
        {
            $response['display_msg'] = $validator;
            return $response;
        }

        //  request parameters 
        $country_code = $request->input('countryCode');
        $country_name = $request->input('countryName');

        $result = Country::select('ISDCode')
                ->where('CountryCode', '=', $country_code)
                ->orwhere('CountryName', '=', $country_name)
                ->first();

        if ( $result->count() )
        {
            $statusMsgSuccessful = trans('messages.attributes.successful');
            //$statusMsgFailed = trans('messages.attribute.failed');
            $moduleMsgTransaction = trans('messages.attributes.transaction');
           // $response['display_msg']['info'] = trans('messages.success');//The test request successful   
           // $response['display_msg']['info'] = trans('messages.delete', ['name' => 'transaction', 'status' =>'success']); //'Your :module has been :status', //Ex: Your transaction has been successful  
            $response['display_msg']['info'] = trans('messages.moduleStatus', ['module' => $moduleMsgTransaction, 'status' =>$statusMsgSuccessful]);
            $response['is_error'] = FALSE;
            $response['res_data'] = $result;
        }
        else
        {
            $response['display_msg']['info'] = trans('messages.emptyRecords');
        }
        return $response;
    }

    /*
     *  Validate Request Information
     *  @param Request $request
     *  @return viod / JSON
     */

    private function _validateRequestDetails($request)
    {
        // validation rules
        $validate = [
            'countryCode' => 'required',
            'countryName' => 'required',
        ];
        $messages = [];
        // validation messages 
        /*$messages = [
            'countryCode.required' => 'Country Code Is Required',
            'countryName.required' => 'Country Name Is Required',
        ];*/
        // calling validation method
        $validator  = Validator::make($request->all(), $validate, $messages);
        
        // using helper method to fech response
        $validation = Helpers::validateErrorResponse($validator);
        
        return $validation;
    }
    
    public function kotakTestApiResponse(\Illuminate\Http\Request $request)
    {

        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $messageContent = "<center>";
        if ( $request->input('encResp') !== NULL )
        {
            $workingKey     = config('vwallet.workingKeyCcA');
            ;  //Working Key should be provided here.
            $encResponse    = $_POST["encResp"];   //This is the response sent by the CCAvenue Server
            $rcvdString     = \App\Libraries\Payments\Ccavenue::decrypt($encResponse, $workingKey);  //Crypto Decryption used as per the specified working key.
            $order_status   = "";
            $decryptValues  = explode('&', $rcvdString);
            $dataSize       = sizeof($decryptValues);          

            for ( $i = 0; $i < $dataSize; $i ++  )
            {
                $information  = explode('=', $decryptValues[$i]);
                if ( $i == 3 )
                    $order_status = $information[1];
            }

            if ( $order_status === "Success" )
            {
                $messageContent .= "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
            }
            else if ( $order_status === "Aborted" )
            {
                $messageContent .= "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
            }
            else if ( $order_status === "Failure" )
            {
                $messageContent .= "<br>Thank you for shopping with us.However,the transaction has been declined.";
            }
            else
            {
                $messageContent .= "<br>Security Error. Illegal access detected";
            }

            $messageContent . "<br><br>";

            $messageContent .= "<table cellspacing=4 cellpadding=4>";
            for ( $i = 0; $i < $dataSize; $i ++  )
            {
                $information    = explode('=', $decryptValues[$i]);
                $messageContent .= '<tr><td>' . $information[0] . '</td><td>' . urldecode($information[1]) . '</td></tr>';
            }

            $messageContent .= "</table><br>";
            
        }
        else
        {
            $messageContent .= "<div>Security Notice. Illegal access detected</div>";
        }
        $messageContent .= "</center>";
        $html = view('payments.ccavResponse')->with('messageContent', $messageContent);        
        return response($html)
            ->header('Content-Type', 'html')            
            ->header('Page-Status', 'Close_Browser');
    }

    public function kotakApi(\Illuminate\Http\Request $request)
    {      
        // return $returnData = \App\Libraries\Payments\Ccavenue::kotakApiRequest($request);
        //placed in library file.
        $orderId = rand(111111111, 999999999);
        $ccaMerchantId = config('vwallet.ccaMerchantId');
        $accessCodeCcA = config('vwallet.accessCodeCcA');
        $workingKeyCcA = config('vwallet.workingKeyCcA');
        $ccavenueTransUrl = config('vwallet.ccavenueTransUrl');
        //$apiUrl = 'https://192.168.100.86/apis/kotakTestApi';
        //$responseUrl = 'https://192.168.100.86/apis/kotakApiResponse';
        $responseUrl = config('vwallet.ccavenueResponseUrl');
        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        
       
        $sendData = [
            'tid' => time()*1000,
            'merchant_id' => $ccaMerchantId,
            'order_id' => $orderId,
            'amount' => '10.00',
            'currency' => 'INR',
            'redirect_url' => $responseUrl,
            'cancel_url' => $responseUrl,
            'language' => 'EN',
            'billing_name' => 'Charli',            
            'billing_address' => 'Beside DLF Cyber City Gachibowli',
            'billing_city' => 'Hyderabad',
            'billing_state' => 'TG',
            'billing_zip' => '500032',
            'billing_country' => 'India',
            'billing_tel' => '4066497788',
            'billing_email' => 'test@test.com',
            'delivery_name' => 'Viola',
            'delivery_address' => 'Beside DLF Cyber City Gachibowli',
            'delivery_city' => 'Hyderabad',
            'delivery_state' => 'TG',
            'delivery_zip' => '500032',
            'delivery_country' => 'India',
            'delivery_tel' => '4066497788',
            'merchant_param1' => 'additional Info.',
            'merchant_param2' => 'additional Info.',
            'merchant_param3' => 'additional Info.',
            'merchant_param4' => 'additional Info.',
            'merchant_param5' => 'additional Info.',
            'card_type' => 'CRDC',
            'card_name' => 'Visa',
            'data_accept' => 'N',
            'card_number' => '4111111111111111',
            'expiry_month' => '02',
            'expiry_year' => '2020',
            'cvv_number' => '123',
            'issuing_bank' => 'ICICI',
            'mobile_number' => '9770707070'           
        ];
        
        $merchant_data = NULL;
        foreach ($sendData as $key => $value)
        {           
            $merchant_data.= $key . '=' . urlencode($value) . '&';
        }
        $encryptedData=\App\Libraries\Payments\Ccavenue::encrypt($merchant_data,$workingKeyCcA);  
        
        $responseData = view('payments.ccavRequest')
                ->with('ccavenueUrl', $ccavenueTransUrl)
                ->with('encryptedData', $encryptedData)
                ->with('accessCodeCcA', $accessCodeCcA); 
        
       /* $responseData = '<!DOCTYPE html><html lang="en"><head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <title>Viola Wallet: Payment Initiate</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          </head>
          <body>
          <form method="post" name="redirect" action="'.$ccavenueTransUrl.'">'
              . '<input type=hidden name=encRequest value='.$encryptedData.'>'
              . '<input type=hidden name=access_code value='.$accessCodeCcA.'>
                  </form>
                  <script language="javascript">document.redirect.submit();</script>
                  </body>
                  </html>';
        * 
        */
        /*$postData = [
            'encRequest' => $encryptedData,
            'access_code' => $accessCodeCcA
        ];        
        
        $output         = \App\Libraries\Helpers::curl_send($ccavenueTransUrl, $postData);
        $trimOne = explode( '<body>' , $output );
        $trimTwo = explode("</body>" , $trimOne[1] );
        $responseData = $trimTwo[0];
         * 
         */
        return response($responseData)
            ->header('Content-Type', 'html')            
            ->header('Page-Status', 'Open_Browser');  
        return $response;     
    }

    private function testNotificationData()
    {
        $results = \App\Models\Userwallet::select( 'FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID' )
                        ->where('UserWalletId', $request->input('partyId') )->first();
                    
        // Send Notification
        $sendMobileData = json_encode(array());                
        $sendEmailData = json_encode(array(
                'fullname' => $results->FirstName.' '.$results->LastName,
                'img_url' => $this->baseUrl,
                'front_url' => $this->baseUrl,  

                ));
     $communicationParams = array(                    
            'party_id' => $results->UserWalletID,
            'linked_id' => '0',
            'notification_for' => 'profile_changePass',
            'notification_type' => 'email,sms',
            'mobile_no' => $results->MobileNumber,
            'mobile_data' => $sendMobileData,
            'email_id' => $results->EmailID, 
            'email_data' => $sendEmailData,

            );  
        // Push notification parameters.          
         $device_type      = $request->header('device-type');                
        if($device_type!='W') { 
            $pushData =  array();   
            $communicationParams['notification_type'] = $communicationParams['notification_type'].",push";
            $communicationParams['device_type'] = $device_type;
            $communicationParams['device_token'] = $request->header('device-token');  
            $communicationParams['push_data'] = json_encode($pushData); 
        }
     //common function to send notification_type msgs.
        $response = \App\Libraries\Helpers::sendMsg($communicationParams);

    }
    
    function transctionFinish()
    {
        return 'Transction Finished..!';
    }
    
    


    public function cashBack(\Illuminate\Http\Request $request)
    {
        $regPartyId   = $request->input('partyId');        
        $results      = \App\Models\Partyreferral::select('PartyID', 'ReferralCode', 'ReferralMediumCode', 'RefereeName', 'RefereeMobileNumber', 'RefereeEmailId')->where('PartyID', $regPartyId)->first();
        if($results && $results->ReferralCode!=NULL) {
            $request->request->add([ 'usedReferralCode' => $results->ReferralCode ]);            
            $referralCode = $request->input('usedReferralCode');
            $referralCashBackStats = \App\Libraries\OfferRules::referralCashBack($request);
           
            $referralInfo = json_decode($referralCashBackStats);
            if ( $referralInfo->is_error == FALSE )
            {
                $data_send['display_msg']['cashBackMsg'] = $referralInfo->display_msg->info;
            }
        }   
        print_r($data_send);exit;
        /*     
        $amountMain = $request->input('cashBackamount');
        $productEventId = 12;
         $transStaus = 'Success';
         $event = 'CASHBACK';
          $entityId = 1; 
          $message = 'Cashback Done Successfully';
          $vendor = 'Viola Wallet';
          $txnType = 'Cashback Offer';
          $amountTag = 'CASH_BACK_AMT';
            $insertCashback = \App\Libraries\TransctionHelper::cashBack($request,$amountMain,$productEventId,$transStaus,$event,$entityId,$message,$vendor,$txnType,$amountTag);
           $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array( 'info' => 'Amount Cashback Successfully' );
            return $output_arr;
        * 
        */
       
    }
    /**
     * 
     * @param type $type
     * @return type
     */
    private static function getPaymentProductIdArray($type){
        $result = \App\Models\Paymentsproduct::select('PaymentsProductID')
                ->where('ProductType', 'INTERNAL')->get();
        $arrayValue = [];
        foreach($result as $eachRecord) {
             $arrayValue[] = $eachRecord->PaymentsProductID;           
        }
        return $arrayValue;
    }


    public function testMethod(\Illuminate\Http\Request $request){
        $extraParams = array('partyId' => '1', 'requestId' => '2', 'referenceId' => '3', 'status' => 'Initiated');
        //echo $msg = trans('messages.ERROR.checkuser.R26');  exit;
//        $actionName = 'CHANGEBENE';
//        $params     = array( 'p1'  => '9699099641', 'p2'=> 14124, 'p3' => 100); //9038213799
        
//        $actionName = 'FETCHBENE';
//        $params     = array( 'p1'  => '9573398780', 'p2'=> 14453); //9038213799
        
        
        //$actionName = 'CHECKUSER';
        //$params     = array( 'p1'  => '9949207896'); //9038213799
        
        $actionName = 'WALLETBAL';
        $params     = array( 'p1'  => '9699099641'); //9038213799
        
        //$actionName = 'CHECKKYCSTATUS';
        //$params     = array( 'p1'  => '9949207896');
        
//       $actionName = 'REGUSER';
//        $params     = array( 'p1'  => '9876666667', 'p2' => 'Y');
        
        //$actionName = 'VERIFYMOTP';
        //$params     = array( 'p1'  => '7702529807', 'p2' => '19032', 'p3' => '111111');
        
//        $actionName = 'ADDOWNACC';
//        $params     = array( 'p1'  => '9699099641', 'p2' => '9912392050', 'p3' => 'KKBK0007466');
        
//        $actionName = 'VERIFYOWNACC';
//        $params     = array( 'p1'  => '9699099641', 'p2' => '2.74', 'p3' => '16521');
       
        //$actionName = 'REGAADHAAR';
        //$params     = array( 'p1'  => '9949207896', 'p2' => '747526189316', 'p3' => 'afddb4fe-58d1-4079-82dc-9dabad00056b', 'p4' => 'Y', 'p5' => 'Authentication');
        
        //$actionName = 'VERIFYAOTP';
        //$params     = array( 'p1'  => '9949207896', 'p2' => '747526189316', 'p3' => '180515011166', 'p4' => '475661', 'p5' => '967676', 'p6' => 'Y', 'p7' => 'Authentication');
        $response = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params, $extraParams);
        print_r($response);exit;
       /*
        $yesWalletUrl = 'http://ybl2.sodelsolutions.com/white_labeled_partners/white_labeled_wallet_v2.json';
        //$secrectKey = config('vwallet.yesBankWallet.secrectKey');
        $wlapCode = config('vwallet.yesBankWallet.wlapCode');
        $wlapKey = config('vwallet.yesBankWallet.wlapKey');
        $actionName = 'CHECKKYCSTATUS';
        $p1 = '9949207896';
        $p2 = 'Y';
        //$plainText = 'ACTION|PSK_secrect_key|wlap_code|wlap_secret_key|P1|P2|P3|P4|P5|P6|P7|P8|P9|P10|P11|P12|P13|P14';
        $plainText = $p1.'|'.$p2.'||||||||||||';        
        $checksum = \App\Libraries\Wallet\Yesbank::checksum($actionName, $plainText);           
        $finalUrl = $yesWalletUrl.'?action_name='.$actionName.'&wlap_code='.$wlapCode.'&wlap_secret_key='.$wlapKey.'&p1='.$p1.'&p2='.$p2.'&checksum='.$checksum;
        print_r($finalUrl);exit;
        $response = \App\Libraries\Wallet\Yesbank::requestCURL($finalUrl, 'GET');
        print_r($response);exit;
        * 
        */
        $regPartyId   = $request->input('partyId');
          
          $request->request->add([ 'usedReferralCode' => 'MAHE207896' ]);
          $request->request->add([ 'partyId' => '2005' ]);          
          $referralCashBackStats = \App\Libraries\OfferRules::referralCashBack($request);
          $referralInfo = json_decode($referralCashBackStats);
          print_r($referralInfo);
          if ( $referralInfo->is_error == FALSE )
          {
          $data_send['display_msg']['cashBackMsg'] = $referralInfo->display_msg->info;
          }
    exit;
        $list = array
(
"Peter,Griffin,Oslo,Norway",
"Glenn,Quagmire,Oslo,Norway",
);
        $path     = 'CSV/';
        $newPath  = rtrim(app()->basePath('public/' . $path), '/');
        $dateTime = date('Y-m-d H:i:s');
        if ( ! is_dir($newPath) )
        {
            mkdir($newPath, 0777, TRUE);
        }
        $uniqueId      = date("Ym", strtotime($dateTime));
        $fileName      = $uniqueId . '.csv';
        $storeFullPath = $path . $fileName;
        $file          = fopen($newPath . '/' . $fileName, 'a+');      
        foreach ( $list as $line )
        {
            fputcsv($file, explode(',', $line));
        }

        fclose($file);

        exit;
        
        $results = \DB::select("select pro.*,userActivity,UserWalletID from vwallet_in618.promotionaloffers pro left join (select ufs.UserWalletID, ufs.userActivity as userActivity, po.OfferBusinessID as offerbid from vwallet_in618.promotionaloffers po 
left join vwallet_in618.useroffersstats ufs on po.OfferBusinessID = ufs.OfferBusinessID WHERE ufs.userActivity ='Like' and ufs.UserWalletID =113
group by ufs.OfferBusinessID) temp on temp.offerbid=pro.OfferBusinessID");
        print_r($results);exit;
        $partyId = 113;
        $orderAmount = 4000;
        $internalProducts = self::getPaymentProductIdArray('Internal');
         if ( in_array(2, $internalProducts) )
        {
             echo "Internal";
         }
        print_r($internalProducts);exit;
        //$update_userWallet = \App\Models\Walletaccount::select('CurrentAmount')->where('UserWalletId', $partyId)->first();
        //print_r($update_userWallet->CurrentAmount);exit;
        //\App\Libraries\Statistics::walletMonthBalanceUpdate($partyId, $orderAmount);
        $userTransactionStats                  = \App\Libraries\Statistics::getUserTransactionStats($partyId);
       print_r($userTransactionStats);
        echo "test";exit;
        $partyId = '113';
        $amount = 100;
       
        
        return "Ok";
    //}
        
        $toCur = 'GBP';
        $fromCur = 'USD';
        $amount = '100';
        
        //$rateNow = \App\Libraries\Constant_GRP::BuySellCurrency($fromCur, $toCur, $amount);
        $rateNow = \App\Libraries\Constant_GRP::get_rate($fromCur, $toCur, $amount);
        print_r($rateNow);exit;
        //add benefit ammount
       // $result = \App\Libraries\OfferBussness::addbenefitTrack('3609');
       // print_r($result);exit;
        $selectedColumns = [
            'billdeskbillers.BilldeskBillersid',
            'billdeskbillers.BillerID', 
            'paymentsproduct.ProductName',
            'paymentsproduct.PaymentsProductID'
        ];
        
        $query = \App\Models\Paymentsproduct::join('billdeskbillers', 'paymentsproduct.ProductName', '=', 'billdeskbillers.BillerID')                
                ->where('billdeskbillers.BillerCategory', '=', 'Electricity');
        
         $resultData = $query->select($selectedColumns)->get()->toArray();
         $paymetArray = [];
         foreach($resultData as $result) {
            $paymetArray[] =  $result['PaymentsProductID'];
         }
        $productstring = implode(',', $paymetArray);
        print_r($productstring);exit;
        $sms_arr    = array(
            'mobile_no' => '9949207896',
            'message'   => "atest",
        );
        
        $msg_id_str = \App\Libraries\Smshub::send_message($sms_arr);
        print_r($msg_id_str);exit;

         $updateData = array(
            'UserActivity' => 'Used',           
            'ActivityDateTime'   => date('Y-m-d H:i:s')
        );
         $updateId = '3';
        \App\Models\Useroffersstat::where('UserUsedOffersId', $updateId)->update($updateData);
        
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        $userOfferStats = array(
            'UserWalletID'       => '1',
            'OfferBusinessID'        => '1001',
            'PromoCode'             => 'VIOLA123', // Wallet user id of from wallet account
            'UserActivity'            => 'View',
            'ActivityDateTime'          => $dateTimeOfTrans,
            'AmountUsed'          => '100.00',
            'CreatedDateTime'            => $dateTimeOfTrans,
            'CreatedBy'              => 'User',
        );

        return $insertId = \App\Models\Useroffersstat::insertGetId($userOfferStats);
    }
    
    /*
    public function addbenefittrack()
    {
        $transactionId = '2124';
        $response = \App\Libraries\OfferBussness::addbenefitTrack($transactionId);
        return $response;
    }
     * 
     */
    
    public function addreferbenefittrack(\Illuminate\Http\Request $request)
    {
        $requestArray          = \App\Libraries\TransctionHelper::referralPromoCodeInfo($request);
        $response = \App\Libraries\OfferBussness::addrefferbenefitTrack($request, $requestArray);
        return $response;
    }
    

}

/* End of file TestController.php */ /* Location: ./Http/Test/TestController.php */
