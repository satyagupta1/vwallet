<?php

namespace App\Http\Controllers\Pays;

/**
 * SendMoneyToBankController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class SendMoneyToBankController extends \App\Http\Controllers\Controller {

    // send Money Wallet to Bank
    public function sendMoneyWB(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateSendMoneyWB($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        //code to check vpin
        $params     = array( 'partyId' => $request->input('partyId'), 'vPin' => $request->input('vPin') );
        $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
        if ( $verifyVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'vPin' => 'Please enter valid V-PIN' );
            return response()->json($output_arr);
        }

        $partyId         = $request->input('partyId');
        $amount          = $request->input('amount');
        $businessModelid = $request->input('businessModelId');
        $transStaus      = config('vwallet.transactionStatus.pending');
        $transBool       = TRUE;
        //trans_type whether it is WW or WB
        $transType       = $request->input('transType');
        $productEventId  = 4;
        $benfType        = 'eWallet';
        $serviceType     = 'send_money_wtow';
        $beneficiaryId   = $request->input('beneficiaryId');
        if ( $transType == 'WB' )
        {
            $serviceType    = 'send_money_wtob';
            $benfType       = 'Bank';
            $productEventId = 5;
        }

        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        $banificiaryInfo = array();
        // get Beneficiary details if beneficiary Exists.
        if ( $request->input('isSavedBenificiary') == 'Y' )
        {
            $banificiaryInfo = \App\Libraries\TransctionHelper::_beneficiaryValidate($beneficiaryId, $benfType, TRUE, FALSE);
            if ( $banificiaryInfo === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.benificiartNotFound') );
                return $output_arr;
            }
        }

        // session Check
        $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if ( is_bool($sessionId) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidSession') );
            return $output_arr;
        }

        // check Balance
        if ( $partyInfo->CurrentAmount < $amount )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output_arr;
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ( $partyTransLimit['error'] )
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);
//        print_r($fee); exit;
        $transRes       = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelid, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transStaus, $sessionId);

        // \App\Libraries\CommunicationLib::_communicationSend($partyId, $beneficiaryId, $transRes, $serviceType);

        if ( $transRes['error'] )
        {
            $output_arr['display_msg'] = array( 'info' => $transRes['info'] );
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $transRes['response'];
        return $output_arr;
    }

    private function _validateSendMoneyWB($request)
    {
        $validateArray      = array(
            'partyId'            => 'required|numeric|digits_between:1,10',
            'walletacntId'       => 'required|numeric|digits_between:1,10',
            'amount'             => 'required|numeric',
            'vPin'               => 'required|numeric|digits_between:1,4',
            'businessModelId'    => 'required|numeric',
            'isSavedBenificiary' => 'required|in:Y,N',
            'remarks'            => 'regex:/^[a-zA-Z0-9@ .\-]+$/i'
        );
        $validatenetBanking = array();

        if ( $request->input('savedBenificiary') == 'Y' )
        {
            $validatenetBanking = array(
                'beneficiaryId' => 'required|numeric'
            );
        }
        else
        {
            if ( $request->input('businessModelId') == 2 )
            {
                $validatenetBanking = array(
                    'vpa'                => 'required|numeric',
                    'saveThisBenificiar' => 'required|in:Y,N',
                );
            }
            if ( $request->input('businessModelId') == 3 )
            {
                $validatenetBanking = array(
                    'accountNumber'      => 'required|numeric',
                    'ifscCode'           => 'required|alpha_num|max:12|min:11',
                    'saveThisBenificiar' => 'required|in:Y,N',
                );
            }
            if ( $request->input('businessModelId') == 4 )
            {
                $validatenetBanking = array(
                    'benificiaryMobileNumber' => 'required|numeric|digits:10',
                    'mmid'                    => 'required|numeric|digits_between:8,16',
                    'saveThisBenificiar'      => 'required|in:Y,N',
                );
            }
            if ( $request->input('saveThisBenificiar') == 'Y' )
            {
                $validatenetBanking['benificiaryName'] = 'required|regex:/^[a-zA-Z0-9@ .\-]+$/i';
            }
        }

        $validate = array_merge($validateArray, $validatenetBanking);

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
