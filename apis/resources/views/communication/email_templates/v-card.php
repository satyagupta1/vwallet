<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body style="margin:0px;padding:0px;">
	<table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif">
    <tbody><tr style="margin:0;padding:0">
        <td style="margin:0;padding:0"></td>
        <td bgcolor="#F6F6F6" style="margin:0 auto!important;padding:0;background:#f6f6f6;display:block!important;max-width:800px!important;clear:both!important;padding-bottom:5px;">
            <br style="margin:0;padding:0">
            <div style="margin:0 auto 15px;padding:0;max-width:540px;border-radius:5px;overflow:hidden;display:block;background:#fff;box-shadow: 0 0 2px #ddd;letter-spacing:.5">
                             
                {{mail_contant}}      
				<br style="margin:0;padding:0">				  
				 <div style="margin:0;padding:0">                    
                    <table width="100%" cellspacing="0" cellpadding="0" style="padding:0 15px;border-top:0px solid #e9e9e9;border-bottom:2px solid #68c5df;margin:0">
                        <tbody><tr style="margin:0;padding:0">
                           <img src="img/strip.png" style="height:4px; width:100%;">
                            <td style="margin:0;padding:0">                                
                                <table cellspacing="0" cellpadding="0" align="left" style="margin:0;padding:15px 0;max-width:130px">
                                    <tbody><tr style="margin:0;padding:0">
                                        <td style="margin:0;padding:0">
                                            <table cellspacing="0" cellpadding="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;margin:0;padding:0">
                                                <tbody style="margin:0;padding:0">
                                                    <tr style="margin:0;padding:0">
                                                        <td style="margin:0;padding:0">Get the App:&nbsp;&nbsp;</td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="http://uat.violacards.in/first/">
                                                                <img src="http://uat.violacards.in/email/img/android.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
                                                            </a>
                                                        </td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="http://uat.violacards.in/first/">
                                                                <img src="http://uat.violacards.in/email/img/mac.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
															</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
								</table> 
                                <table cellspacing="0" cellpadding="0" align="right" style="margin:0;padding:15px 0;max-width:150px">
                                    <tbody><tr style="margin:0;padding:0">
                                        <td style="margin:0;padding:0">
                                            <table cellspacing="0" cellpadding="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;margin:0;padding:0">
                                                <tbody style="margin:0;padding:0">
                                                    <tr style="margin:0;padding:0">
                                                        <td style="margin:0;padding:0;text-align:left">Follow us:&nbsp;&nbsp;</td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a  target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="#">
                                                                <img border="0" alt="Facebook" src="http://uat.violacards.in/email/img/facebook.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
															</a>
                                                        </td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="#">
                                                                <img border="0" alt="Twitter" src="http://uat.violacards.in/email/img/twiiter.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
															</a>
                                                        </td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a  target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="#">
                                                                <img border="0" alt="pinterest" src="http://uat.violacards.in/email/img/pintrest.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
															</a>
                                                        </td>
                                                        <td style="margin:0;padding:0;padding-right:5px">
                                                            <a  target="_blank" style="margin:0;padding:0;color:#2ba6cb;display:block" href="#">
                                                                <img border="0" alt="instagram" src="http://uat.violacards.in/email/img/histro.png" style="min-height:15px;width:auto;margin:0;padding:0;max-width:100%;max-height:15px!important">
															</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
								</table>                                
                            </td>
                        </tr>
                    </tbody>
					</table>
                </div>                
                <div style="margin:0;padding:0 15px;background:#FbFbFb">
                    <br style="margin:0;padding:0">
                    <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
                        <tbody><tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:200;color:#9b9b9b;margin:0;padding:0">
                                    <tbody style="margin:0;padding:0">
                                        <tr style="margin:0;padding:0">
                                            <td align="center" style="margin:0;padding:0">
                                                &copy; 2017-V-Card. All rights reserved.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody></table>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;padding:10px 0;color:#9b9b9b;margin:0">
                        <tbody style="margin:0;padding:0">
                            <tr style="margin:0;padding:0">
                                <td width="33.3333%" align="center" style="margin:0;padding:0">
                                    Registered in England and Wales 10056664, Registered Office:3 Waterton Park, Bridgend Wales CF313PH 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br style="margin:0;padding:0">
                </div>
            </div>
        </td>
        <td style="margin:0;padding:0"></td>		
    </tr>
		</tbody>
	</table>		
</body>
</html>
	