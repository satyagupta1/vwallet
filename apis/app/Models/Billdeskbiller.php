<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Dec 2017 07:38:36 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Billdeskbiller
 * 
 * @property int $BilldeskBillersid
 * @property string $BBPSBiller
 * @property string $BillerID
 * @property string $BillerName
 * @property string $Remarks
 * @property string $PostDuePayFlag
 * @property string $AmtUICaptureFlag
 * @property string $BillerCategory
 * @property string $BillerPreference
 * @property string $Rapdrp
 * @property string $OnlineBillFetchFlag
 * @property string $PartPayFlag
 * @property string $OnlinePayFlag
 * @property string $KeyAuthenticaotrs
 * @property string $ResponseAuthenticators
 * @property string $Ref1Label
 * @property string $Ref1RegEx
 * @property string $Ref1Message
 * @property string $Ref2Label
 * @property string $Ref2RegEx
 * @property string $Ref2Message
 * @property string $Ref3Label
 * @property string $Ref3RegEx
 * @property string $Ref3Message
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 *
 * @package App\Models
 */
class Billdeskbiller extends Eloquent
{
        protected $table = 'billdeskbillers';
	protected $primaryKey = 'BilldeskBillersid';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'BBPSBiller',
		'BillerID',
		'BillerName',
		'Remarks',
		'PostDuePayFlag',
		'AmtUICaptureFlag',
		'BillerCategory',
		'BillerPreference',
		'Rapdrp',
		'OnlineBillFetchFlag',
		'PartPayFlag',
		'OnlinePayFlag',
		'KeyAuthenticaotrs',
		'ResponseAuthenticators',
		'Ref1Label',
		'Ref1RegEx',
		'Ref1Message',
		'Ref2Label',
		'Ref2RegEx',
		'Ref2Message',
		'Ref3Label',
		'Ref3RegEx',
		'Ref3Message',
		'CreatedDateTime',
		'CreatedBy'
	];
}
