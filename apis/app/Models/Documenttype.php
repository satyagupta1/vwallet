<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Documenttype
 * 
 * @property string $DocumentTypeCode
 * @property string $DocumentTypeDescription
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partydocuments
 *
 * @package App\Models
 */
class Documenttype extends Eloquent
{
	protected $table = 'documenttype';
	protected $primaryKey = 'DocumentTypeCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'DocumentTypeDescription',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partydocuments()
	{
		return $this->hasMany(\App\Models\Partydocument::class, 'DocumentTypeCode');
	}
}
