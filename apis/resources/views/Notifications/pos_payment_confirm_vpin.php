<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback on hold (bus ticket booking)
 *  param $salutationName varchar
 *  param $merchant varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($primaryName) ? $salutation.' '.$primaryName : $salutation;
$array = [
    'email' => [
        'subject' => '',
        'text'    => $salutationName . ', <br/><br/>
    your payment of '.$ammount.' is successful for the 
    transaction ID: '.$transactionId.' at Time: '.$dateTime.' and Merchant ID: '.$merchantId.' for Transaction Description.'],
    'sms'   => [
        'text' => 'Your payment for INR '.$ammount.' at '.$merchant.' is successful.'
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'busbooking'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
