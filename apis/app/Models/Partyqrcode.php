<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 11 Oct 2017 06:13:32 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyqrcode
 * 
 * @property int $PartyQRCodeId
 * @property int $SenderPartyID
 * @property float $Amount
 * @property \Carbon\Carbon $InitiatedDTTM
 * @property \Carbon\Carbon $ExpiryDTTM
 * @property string $UsedStatus
 * @property string $ReceiverMobNum
 * @property int $ReceiverPartyID
 * @property string $QRType
 * @property \Carbon\Carbon $UsedDTTM
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partyqrcode extends Eloquent
{
	protected $table = 'partyqrcode';
	protected $primaryKey = 'RequestMoneyId';
	public $timestamps = false;

	protected $casts = [
		'SenderPartyID' => 'int',
		'Amount' => 'float',
		'ReceiverPartyID' => 'int'
	];

	protected $dates = [
		'InitiatedDTTM',
		'ExpiryDTTM',
		'UsedDTTM',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'SenderPartyID',
		'Amount',
		'InitiatedDTTM',
		'ExpiryDTTM',
		'UsedStatus',
		'ReceiverMobNum',
		'ReceiverPartyID',
		'QRType',
		'UsedDTTM',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'SenderPartyID');
	}
}
