<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantpaydevice
 * 
 * @property int $MerchangePayDevicesID
 * @property int $MerchantID
 * @property int $DeviceTypeRefID
 * @property int $DeviceID
 * @property string $DeviceStatus
 * @property string $NameOnPoint
 * @property string $MobileNumber
 * @property string $EmailId
 * @property string $Pincode
 * @property string $City
 * @property string $state
 * @property string $Building
 * @property string $Colony
 * @property string $Landmark
 * @property string $Status
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 * @property \Illuminate\Database\Eloquent\Collection $merchanttransactiondetails
 *
 * @package App\Models
 */
class Merchantpaydevice extends Eloquent
{
	protected $primaryKey = 'MerchangePayDevicesID';
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int',
		'DeviceTypeRefID' => 'int',
		'DeviceID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'MerchantID',
		'DeviceTypeRefID',
		'DeviceID',
		'DeviceStatus',
		'NameOnPoint',
		'MobileNumber',
		'EmailId',
		'Pincode',
		'City',
		'state',
		'Building',
		'Colony',
		'Landmark',
		'Status',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}

	public function merchanttransactiondetails()
	{
		return $this->hasMany(\App\Models\Merchanttransactiondetail::class, 'MerchangePayDevicesID');
	}
}
