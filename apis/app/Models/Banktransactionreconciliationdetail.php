<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Banktransactionreconciliationdetail
 * 
 * @property int $BankTransactionReconciliationDetailID
 * @property string $BankTransactionID
 * @property string $InternalTransactionID
 * @property \Carbon\Carbon $ReconReportDate
 * @property string $ReconStatus
 * @property \Carbon\Carbon $ReconDate
 * @property int $BankBranchID
 * @property string $BankCode
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \App\Models\Bankbranch $bankbranch
 *
 * @package App\Models
 */
class Banktransactionreconciliationdetail extends Eloquent
{
	protected $table = 'banktransactionreconciliationdetail';
	protected $primaryKey = 'BankTransactionReconciliationDetailID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BankTransactionReconciliationDetailID' => 'int',
		'BankBranchID' => 'int'
	];

	protected $dates = [
		'ReconReportDate',
		'ReconDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BankTransactionID',
		'InternalTransactionID',
		'ReconReportDate',
		'ReconStatus',
		'ReconDate',
		'BankBranchID',
		'BankCode',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function bankbranch()
	{
		return $this->belongsTo(\App\Models\Bankbranch::class, 'BankBranchID')
					->where('bankbranch.BankBranchID', '=', 'banktransactionreconciliationdetail.BankBranchID')
					->where('bankbranch.BankCode', '=', 'banktransactionreconciliationdetail.BankCode');
	}
}
