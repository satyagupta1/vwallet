<?php

namespace App\Libraries\Wallet;

class Yesbank {    
    /**
     * Creating checksum hash
     * @param type $actionName
     * @param type $plainText
     * @return type
     */
    private static function checksum($actionName, $plainText)
    {
        $secrectKey = config('vwallet.yesBankWallet.secrectKey');
        $wlapCode = config('vwallet.yesBankWallet.wlapCode');
        $wlapKey = config('vwallet.yesBankWallet.wlapKey');
        $string = $actionName.'|'.$secrectKey.'|'.$wlapCode.'|'.$wlapKey.'|'.$plainText;  
        return hash('SHA512', $string);       
        //return hash_hmac('SHA512', $string, $secrectKey);
    }
    /**
     * Handle for request data
     * @param type $actionName
     * @param type $requestParams
     * @param type $extraParams
     * @return type
     */
    public static function handleRequest($actionName, $requestParams, $extraParams = array())
    {   
        $wlapCode = config('vwallet.yesBankWallet.wlapCode');
        $wlapKey  = config('vwallet.yesBankWallet.wlapKey');

        $allParams = array( 'p1'  => '', 'p2'  => '', 'p3'  => '', 'p4'  => '', 'p5'  => '', 'p6'  => '',
            'p7'  => '', 'p8'  => '', 'p9'  => '', 'p10' => '', 'p11' => '', 'p12' => '', 'p13' => '', 'p14' => '' );

        $paramsData = array_merge($allParams, $requestParams);
        
        // remove transaction_type for checksum
        if ($actionName === 'TRANSACTION' && isset($paramsData['transaction_type']))
        {
            unset($paramsData['transaction_type']);
        }
        
        $rawText    = implode('|', $paramsData);
        $checksum   = self::checksum($actionName, $rawText);        
        $urlParms   = "";
        if ($requestParams)
        {
            foreach ( $requestParams as $key => $value )
            {
                $urlParms .= '&' . $key . '=' . $value;
            }        
        }        
        $logId = self::createLog($actionName, $requestParams, $extraParams);
        $requestData = 'action_name=' . $actionName . '&wlap_code=' . $wlapCode . '&wlap_secret_key=' . $wlapKey . $urlParms . '&checksum=' . $checksum;
       
        //add upload document after checksum
        if ($actionName === 'SUBMITKYC' && isset($extraParams['id_proof_image_1']) && isset($extraParams['addr_proof_image_1']))
        {
            $requestData.='&id_proof_image_1='.$extraParams['id_proof_image_1'].'&addr_proof_image_1='.$extraParams['addr_proof_image_1'];
        }
        return $response = self::requestPostCURL($requestData, $logId);
    }
    /**
     * This method used to format yes bank wallet response data
     * @param type $responseData
     * @return type
     */
    public static function handleResponse($responseData)
    {
        $data_send    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        if ( $responseData['status_code'] == 'SUCCESS' )
        {
            $data_send['is_error'] = FALSE;
        }
        $data_send['display_msg']['info'] = isset($responseData['message'])?$responseData['message']:"";
        $data_send['res_data']            = $responseData;
        return response()->json($data_send);
    }

    /*
     *  This method is used to connect with yes bank api url using curl method
     *  
     * @param $url 
     * @return jsonObject
     */

    private static function requestPostCURL($requestData, $logId = 0)
    {
        $baseUrl  = config('vwallet.yesBankWallet.baseUrl');
        $timeOut = config('vwallet.curlTime');
        $curl    = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $baseUrl,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT        => 180,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $requestData,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);
        $logRequest = array('response' => $response, 'status'   => 'Success');
        if ( $err )
        {
            $logRequest = array('response' => "cURL Error #:" . $err, 'status'   => 'Failed');
            $response = json_encode(array( 'code' => 'VYW00', 'status_code' => 'ERROR', 'message' => $err ));            
        }
        if ( $logId )
        {            
            \App\Libraries\LogHelper::createLog($logRequest, $logId);
        }
        return $response;
    }
    /**
     * This method used create logs
     * @param type $actionName
     * @param type $requestParams
     * @param type $extraParams
     * @return type
     */
    private static function createLog($actionName, $requestParams, $extraParams)
    {
        $logRequest = array(
            'partyId'        => (isset($extraParams['partyId']))?$extraParams['partyId']:0,
            'requestId'      => (isset($extraParams['requestId']))?$extraParams['requestId']:0,
            'thirdPartyName' => 'YesBankWallet',
            'thirdPartyCall' => $actionName,
            'referenceId'    => (isset($extraParams['referenceId']))?$extraParams['referenceId']:0,
            'request'        => $requestParams,
            'status'         => (isset($extraParams['status']))?$extraParams['status']:'Initiated',
        );
        return $logId      = \App\Libraries\LogHelper::createLog($logRequest);
    }

}

?>
