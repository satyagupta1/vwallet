<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Party
 * 
 * @property int $PartyID
 * @property string $PartyType
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $bankbranches
 * @property \App\Models\Carduser $carduser
 * @property \App\Models\Merchantwallet $merchantwallet
 * @property \Illuminate\Database\Eloquent\Collection $notificationlogs
 * @property \Illuminate\Database\Eloquent\Collection $partyaddresses
 * @property \Illuminate\Database\Eloquent\Collection $partydocuments
 * @property \Illuminate\Database\Eloquent\Collection $partyidentifers
 * @property \App\Models\Partyloginhistory $partyloginhistory
 * @property \App\Models\Partynotificationstatistic $partynotificationstatistic
 * @property \Illuminate\Database\Eloquent\Collection $partyotps
 * @property \Illuminate\Database\Eloquent\Collection $partypasswordhistories
 * @property \Illuminate\Database\Eloquent\Collection $policyterms
 * @property \App\Models\Partyreferral $partyreferral
 * @property \Illuminate\Database\Eloquent\Collection $partysecurities
 * @property \Illuminate\Database\Eloquent\Collection $partysessionusagedetails
 * @property \App\Models\Userwallet $userwallet
 * @property \Illuminate\Database\Eloquent\Collection $vendordetails
 * @property \App\Models\Verificationcenter $verificationcenter
 *
 * @package App\Models
 */
class Party extends Eloquent
{
	protected $table = 'party';
	protected $primaryKey = 'PartyID';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyType',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bankbranches()
	{
		return $this->hasMany(\App\Models\Bankbranch::class, 'BankBranchID');
	}

	public function carduser()
	{
		return $this->hasOne(\App\Models\Carduser::class, 'idCardUsers');
	}

	public function merchantwallet()
	{
		return $this->hasOne(\App\Models\Merchantwallet::class, 'MerchantID');
	}

	public function notificationlogs()
	{
		return $this->hasMany(\App\Models\Notificationlog::class, 'PartyID');
	}

	public function partyaddresses()
	{
		return $this->hasMany(\App\Models\Partyaddress::class, 'PartyID');
	}

	public function partydocuments()
	{
		return $this->hasMany(\App\Models\Partydocument::class, 'PartyID');
	}

	public function partyidentifers()
	{
		return $this->hasMany(\App\Models\Partyidentifer::class, 'PartyID');
	}

	public function partyloginhistory()
	{
		return $this->hasOne(\App\Models\Partyloginhistory::class, 'PartyID');
	}

	public function partynotificationstatistic()
	{
		return $this->hasOne(\App\Models\Partynotificationstatistic::class, 'PartyID');
	}

	public function partyotps()
	{
		return $this->hasMany(\App\Models\Partyotp::class, 'PartyID');
	}

	public function partypasswordhistories()
	{
		return $this->hasMany(\App\Models\Partypasswordhistory::class, 'PartyID');
	}

	public function policyterms()
	{
		return $this->belongsToMany(\App\Models\Policyterm::class, 'partypolicyterms', 'PartyID', 'PolicyTermID')
					->withPivot('PartyPolicyTermID', 'TermsAcceptedDate', 'CreatedDateTime', 'CreatedBy', 'UpdatedDateTime', 'UpdatedBy');
	}

	public function partyreferral()
	{
		return $this->hasOne(\App\Models\Partyreferral::class, 'PartyID');
	}

	public function partysecurities()
	{
		return $this->hasMany(\App\Models\Partysecurity::class, 'PartyID');
	}

	public function partysessionusagedetails()
	{
		return $this->hasMany(\App\Models\Partysessionusagedetail::class, 'PartyID');
	}

	public function userwallet()
	{
		return $this->hasOne(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function vendordetails()
	{
		return $this->hasMany(\App\Models\Vendordetail::class, 'PartyID');
	}

	public function verificationcenter()
	{
		return $this->hasOne(\App\Models\Verificationcenter::class, 'VerificationCenterID');
	}
}
