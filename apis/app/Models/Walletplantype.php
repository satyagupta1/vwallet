<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 05 Oct 2017 13:35:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletplantype
 * 
 * @property int $WalletPlanTypeId
 * @property string $PlanName
 * @property string $PlanDescription
 * @property string $ActiveFlag
 * @property string $PlanCategory
 * @property \Carbon\Carbon $StartDate
 * @property \Carbon\Carbon $EndDate
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $productplanconfigs
 * @property \Illuminate\Database\Eloquent\Collection $userwallets
 * @property \Illuminate\Database\Eloquent\Collection $walletplanconfigurations
 *
 * @package App\Models
 */
class Walletplantype extends Eloquent
{
	protected $table = 'walletplantype';
	protected $primaryKey = 'WalletPlanTypeId';
	public $timestamps = false;

	protected $dates = [
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PlanName',
		'PlanDescription',
		'ActiveFlag',
		'PlanCategory',
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function productplanconfigs()
	{
		return $this->hasMany(\App\Models\Productplanconfig::class, 'WalletPlanTypeId');
	}

	public function userwallets()
	{
		return $this->hasMany(\App\Models\Userwallet::class, 'WalletPlanTypeId');
	}

	public function walletplanconfigurations()
	{
		return $this->hasMany(\App\Models\Walletplanconfiguration::class, 'WalletPlanTypeId');
	}
}
