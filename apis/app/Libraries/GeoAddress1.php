<?php

namespace App\Libraries;

class GeoAdress {
    /*
     * Given longitude and latitude return the address using The Google Geocoding API V3
     *
     */

    public static function getAddressFromGoogleMaps($lat, $lon)
    {

        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false";

// Make the HTTP request
        $data     = @file_get_contents($url);
// Parse the json response
        $jsondata = json_decode($data, true);

// If the json data is invalid, return empty array
        $address = array();
        if ( self::checkStatus($jsondata) )
        {

            $address = array(
                'country'           => googleGetCountry($jsondata),
                'province'          => googleGetProvince($jsondata),
                'city'              => googleGetCity($jsondata),
                'street'            => googleGetStreet($jsondata),
                'postal_code'       => googleGetPostalCode($jsondata),
                'country_code'      => googleGetCountryCode($jsondata),
                'formatted_address' => googleGetAddress($jsondata),
            );
        }

        return $address;
    }

    /*
     * Check if the json data from Google Geo is valid 
     */

    public static function checkStatus($jsondata)
    {
        if ( $jsondata["status"] == "OK" )
            return TRUE;
        return FALSE;
    }

    /*
     * Given Google Geocode json, return the value in the specified element of the array
     */

    public static function googleGetCountry($jsondata)
    {
        return getAddressName("country", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetProvince($jsondata)
    {
        return getAddressName("administrative_area_level_1", $jsondata["results"][0]["address_components"], true);
    }

    public static function googleGetCity($jsondata)
    {
        return getAddressName("locality", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetStreet($jsondata)
    {
        return getAddressName("street_number", $jsondata["results"][0]["address_components"]) . ' ' . getAddressName("route", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetPostalCode($jsondata)
    {
        return getAddressName("postal_code", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetCountryCode($jsondata)
    {
        return getAddressName("country", $jsondata["results"][0]["address_components"], true);
    }

    public static function googleGetAddress($jsondata)
    {
        return $jsondata["results"][0]["formatted_address"];
    }

    /*
     * Searching in Google Geo json, return the long name given the type. 
     * (If short_name is true, return short name)
     */

    public static function getAddressName($type, $array, $short_name = false)
    {
        foreach ( $array as $value )
        {
            if ( in_array($type, $value["types"]) )
            {
                if ( $short_name )
                    return $value["short_name"];
                return $value["long_name"];
            }
        }
    }

}

?>
