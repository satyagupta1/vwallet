<?php

namespace App\Http\Controllers\Offers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionalOffersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyPromoCode(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validatePromoCode($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $requestArray                     = $request->all();
        $requestArray['aplicableChannel'] = $request->header('device-type');

        $verifyData = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
        return $verifyData;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getBenefitsData(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validatePromoCode($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $promoCode = $request->input('promoCode');


        $selectedColumns = [
            'offerbenefitsbridge.ActiveInd AS BridgeStatus',
            'offerbenefitsbridge.OfferBusinessID',
            'offerbenefitsbridge.BenefitBusinessID',
            'promotionaloffers.OfferVersion',
            'promotionaloffers.OfferName',
            'promotionaloffers.OfferDescription',
            'promotionaloffers.OfferCategoryRefID',
            'promotionaloffers.OfferTypeRefID',
            'promotionaloffers.PromoCode',
            'promotionaloffers.OfferUserTypeRefID',
            'promotionaloffers.BusinessEffectiveDate',
            'promotionaloffers.BusinessExpiryDate',
            'promotionaloffers.ApplicableChannel',
            'promotionaloffers.TermsConditionsFilePath',
            'promotionaloffers.EligibilityDetailsFilePath',
            'promotionaloffers.ActiveInd as PromotionStatus',
            'offerbenefits.BenefitVersion',
            'offerbenefits.BenefitName',
            'offerbenefits.BenefitDescription',
            'offerbenefits.BenefitEffectiveDate',
            'offerbenefits.BenefitExpiryDate',
            'offerbenefits.BenefitCategoryRefID',
            'offerbenefits.BenefitCriteriaRefID',
            'offerbenefits.AdminWalletAccountId',
            'offerbenefits.ActiveInd AS BenefitStatus',
            'offerbenefits.BenefitApplicableChannel',
            'offerbenefits.BenefitFixedAmount',
            'offerbenefits.BenefictPercentageValue',
        ];



        $clauses = [ 'offerbenefitsbridge.ActiveInd' => 'Y', 'offerbenefits.ActiveInd' => 'Y', 'promotionaloffers.ActiveInd' => 'Y' ];
        if ( $promoCode !== null )
        {
            $clauses = array_merge($clauses, [ 'promotionaloffers.PromoCode' => $promoCode ]);
        }
        $dbResults = \App\Models\Offerbenefit::join('offerbenefitsbridge', 'offerbenefits.BenefitBusinessID', '=', 'offerbenefitsbridge.BenefitBusinessID')
                //->join('userwallet', 'dailytransactionlog.PartyId', '=', 'userwallet.UserWalletID')
                ->rightjoin('promotionaloffers', 'promotionaloffers.OfferBusinessID', '=', 'offerbenefitsbridge.OfferBusinessID')
                ->orderBy('offerbenefits.OfferBenefitID')
                ->where($clauses)
                ->get($selectedColumns);
        //->toSql();exit;
        
        $output_arr['is_error'] = FALSE;
        if ( ! $dbResults )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return $output_arr;
        }

        
        $output_arr['res_data'] = $dbResults;
        return response()->json($output_arr);
    }

    /**
     * Display the specified Promotions.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPromotionalOffers(\Illuminate\Http\Request $request)
    {
        $headers = $request->header();
        if($headers['device-type']['0']!='W') {
            //$deviceInfoObj = json_decode($headers['device-info']['0']); 
            $deviceInfoObj = json_decode(stripslashes($headers['device-info']['0']), TRUE);  
            $lat = $deviceInfoObj['lat'];
            $lon = $deviceInfoObj['longt'];       
            if($lat > 0) {
                $address = \App\Libraries\GeoAddress::getAddressFromGoogleMaps($lat, $lon);
            }
        }
        $demoOfferCount = 0;
        //common search field check
        if ( ! empty($address) )
        {
            $query = \App\Models\Demographicoffer::select('OfferBusinessID', 'Key', 'Value')->where('demographicoffers.ActiveInd', '=', 'Y');
            $query->where(function ($query1) use ($address) {
                $query1->where('demographicoffers.Value', 'LIKE', '%' . $address['countryCode'] . '%')
                        ->orWhere('demographicoffers.Value', 'LIKE', '%' . $address['province'] . '%')
                        ->orWhere('demographicoffers.Value', 'LIKE', '%' . $address['city'] . '%')
                        ->orWhere('demographicoffers.Value', 'LIKE', '%' . $address['postalCode'] . '%');                        
            });

            $demoOffersData = $query->get();           
            foreach ( $demoOffersData as $demoOfferData )
            {
                $myOfferList[] = $demoOfferData->OfferBusinessID;
            }
            $demoOfferCount = $query->count();
        }

        $ruleNames    = \App\Libraries\OfferRules::offerReferenceData();
        $ruleFlipData = array_flip($ruleNames);
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateOfferParams($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $this->perpage      = config('vwallet.perpage');
        $isActive           = $request->input('isActive');
        $promoCode          = $request->input('promoCode');
        $applicableChannel  = $request->input('applicableChannel');
        $offerUserTypeRefID = $request->input('offerUserTypeId');
        $offerCategoryRefID = $request->input('offerCategoryId');
        $offerTypeRefID     = $request->input('offerTypeRefID');
        $orderBy            = $request->input('orderBy');
        $orderByColumn      = $request->input('orderColumn');
        $limit              = $request->input('limit');
        $paginate           = $request->input('paginate');

        //$promotionalOfferID       = $request->input('promotionalOfferID');
        //$promotionalOfferID       = 1;

        $selectedColumns = array(
            'OfferBusinessID',
            'OfferVersion',
            'OfferName',
            'OfferDescription',
            'OfferCategoryRefID',
            'OfferTypeRefID',
            'OfferBannerImageID',
            'OfferManagerRefID',
            'PromoCode',
            'OfferUserTypeRefID',
            'BusinessEffectiveDate',
            'BusinessExpiryDate',
            'TermsConditionsFilePath',
            'EligibilityDetailsFilePath',
            'OfferStatusRefID',
            'OfferApprovedStatusRefID',
            'ApprovedBy',
            'Budget',
            'ApplicableChannel',
            'SMSNotification',
            'EmailNotification',
            'WebScreenNotification',
            'MobileScreenNotification',
            'QRCodeGenerated',
            'Comments',
            'ActiveInd',
            'promotionaloffers.CreatedDateTime',
            'promotionaloffers.CreatedBy',
            'promotionaloffers.UpdatedDateTime',
            'promotionaloffers.UpdatedBy',
            'screenbanners.BannerName',
            'screenbanners.BannerImageLocation',
            'screenbanners.Filler1',
            'screenbanners.ActiveIndicator',
        );
        if ( $isActive == NULL )
        {
            $isActive = 'Y';
        }
         $clauses                = [ 'promotionaloffers.ActiveInd' => $isActive, 'promotionaloffers.OfferTypeRefID' => 12];
       
        //filter by ApplicableChannel type
        if ( $promoCode !== NULL )
        {
            $clauses = array_merge($clauses, [ 'promotionaloffers.PromoCode' => $promoCode ]);
            //$dbResults->where('promotionaloffers.ApplicableChannel', '=', $applicableChannel);
        }
        //filter by ApplicableChannel type
        if ( $applicableChannel !== NULL )
        {
            $clauses = array_merge($clauses, [ 'promotionaloffers.ApplicableChannel' => $applicableChannel ]);
            //$dbResults->where('promotionaloffers.ApplicableChannel', '=', $applicableChannel);
        }
        //filter by ApplicableChannel type
        if ( $offerUserTypeRefID !== NULL )
        {
            $offerUserTypeRefID = $ruleFlipData[$offerUserTypeRefID];
            $clauses = array_merge($clauses, [ 'promotionaloffers.OfferUserTypeRefID' => $offerUserTypeRefID ]);
            //$dbResults->where('promotionaloffers.OfferUserTypeRefID', '=', $offerUserTypeRefID);
        }
        //filter by ApplicableChannel type
        if ( $offerCategoryRefID !== NULL )
        {
            $offerCategoryRefID = $ruleFlipData[$offerCategoryRefID];
            $clauses = array_merge($clauses, [ 'promotionaloffers.OfferCategoryRefID' => $offerCategoryRefID ]);
            //$dbResults->where('promotionaloffers.OfferCategoryRefID', '=', $offerCategoryRefID);
        }
        //filter by ApplicableChannel type
        if ( $offerTypeRefID !== NULL )
        {
            $offerTypeRefID = $ruleFlipData[$offerTypeRefID];
            $clauses = array_merge($clauses, [ 'promotionaloffers.OfferTypeRefID' => $offerTypeRefID ]);
            //$dbResults->where('promotionaloffers.OfferTypeRefID', '=', $offerTypeRefID);
        }
        
        $dbResults = \App\Models\Promotionaloffer::join('screenbanners', 'screenbanners.ScreenBannerID', '=', 'promotionaloffers.OfferBannerImageID')
                        ->where($clauses);
       
        if($demoOfferCount > 0) {
            $dbResults->whereIn('promotionaloffers.OfferBusinessID', $myOfferList);
        }
                       
        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectedColumns) )
        {
            $dbResults->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $dbResults->orderBy('promotionaloffers.PromotionalOfferID', $orderBy);
        }
        //Limit records
        if ( $limit !== NULL )
        {
            $dbResults->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $records      = $dbResults->select($selectedColumns)->paginate($this->perpage)->toArray();
            $countResults = $records['total'];
        }
        else
        {
            $records      = $dbResults->get($selectedColumns)->toArray();
            $countResults = count($records);
        }
        
        if ( $countResults > 0 )
        {
            if ( isset($records['data']) )
            {
                $records['data'] = $this->_formateOfferBanner($records['data'], $ruleNames);
            }
            else
            {
                $records = $this->_formateOfferBanner($records, $ruleNames);
            }
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array();
            $output_arr['res_data']    = $records;
            return response()->json($output_arr);
        }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }
    
    public function getPromoTermsElegibility(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validTermsElegibility($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $partyId    = $request->input('partyId');
        $promoCode  = $request->input('promoCode');
        $type       = $request->input('type');
        $promoCodeData = \App\Models\Promotionaloffer::select('PromotionalOfferID', 'TermsConditionsFilePath', 'EligibilityDetailsFilePath')->where('PromoCode', $promoCode)->where('ActiveInd', 'Y')->first();
        if ($promoCodeData === NULL)
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return response()->json($output_arr);
        }
        $fileData = '';
        if ($type === 'terms')
        {
            $output_arr['display_msg']['info'] = 'Terms Conditions Details';
            $dirPath = 'public/TermsAndConditions/';
            $file = $dirPath.$promoCodeData->TermsConditionsFilePath;
            if (file_exists($file)) {
                $output_arr['is_error'] = FALSE;
                $fileData = config('vwallet.baseUrl').'public/TermsAndConditions/'.$promoCodeData->TermsConditionsFilePath;
            }
            else
            {
                $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
                return $output_arr;
                
            }
        }
        elseif($type === 'eligibility')
        {
            $output_arr['is_error'] = FALSE;
            $output_arr['display_msg']['info'] = 'Eligibility Details';
            $dirPath = 'public/EligibilityDetails/';
            $file = $dirPath.$promoCodeData->EligibilityDetailsFilePath;
            if (file_exists($file)) {
                $fileData = config('vwallet.baseUrl').'public/EligibilityDetails/'.$promoCodeData->TermsConditionsFilePath;
            }
            else
            {
                $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
                return $output_arr;
                
            }
        }
        $resData = array(
            'fileData'  => $fileData
        );
        $output_arr['res_data'] = $resData;
        return $output_arr;
    }
    
    private function _validTermsElegibility($request)
    {
        $messages = [];
        $validate = [
            'promoCode' => 'required',
            'type'      => 'required|in:terms,eligibility',
        ];
        if ( $request->input('partyId') !== null )
        {
            $validate['partyId'] = 'required|numeric';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _formateOfferBanner($arrayData, $ruleNames) {
        $offersData = [];
        $bannerUrl  = \App\Libraries\Helpers::bannerUrl();
        foreach ( $arrayData as $key => $eachOffer )
        {
            $offersData[$key]                        = $eachOffer;
            $offersData[$key]['OfferCategoryRefID']      = $ruleNames[$eachOffer['OfferCategoryRefID']];
            $offersData[$key]['BannerImageLocation'] = $bannerUrl . $eachOffer['BannerImageLocation'];
            $offersData[$key]['Filler1'] = $bannerUrl . $eachOffer['Filler1'];
            
        }
        return $offersData;
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateOfferParams($request)
    {
        $messages = [];
        $validate = [
            // 'partyId' => 'required|numeric',
        ];
        if ( $request->input('isActive') !== null )
        {
            $validate['isActive'] = 'required|in:Y,N';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate code request data
     * @param $request
     * @return String
     */
    private function _validatePromoCode($request)
    {
        $messages = [];
        $validate = [
            //'partyId' => 'required|numeric',            
            'promoCode' => 'required',
            'paymentProductId' => 'required|numeric',
        ];
        
        //if ($request->input('offerCategoryId') !== null)
        //{
            $validate['offerCategoryId'] = 'required|string';
        //}
        if ($request->input('offerUserTypeId') !== null)
        {
            $validate['offerUserTypeId'] = 'required|string';
        }
        if ($request->input('amount') !== null)
        {
            $validate['amount'] = 'required|numeric';
        }
        if ($request->input('partyId') !== null)
        {
            $validate['partyId'] = 'required|numeric';
        }
        /*
         *
          if ($request->input('benefitCategoryId') !== null)
          {
               $validate['benefitCategoryId'] = 'required|numeric';
          }
          if ($request->input('isActive') !== null)
          {
          $validate['isActive'] = 'required|in:Y,N';
          } */

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function cronAddCashback(\Illuminate\Http\Request $request)
    {
        $response = \App\Libraries\OfferBussness::benefitTrack($request);
        return $response;
    }

    /*public function updateUsedAmount(\Illuminate\Http\Request $request)
    {
        $dateTime = date('Y-m-d H:i:s');

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount')->where('EarnedAmount', '>', '0.00')->where('ExtendedValidityDate', '>', $dateTime)->orderBy('ExtendedValidityDate', 'desc')->first();
        if ( $bnftTrackData )
        {
            $updateData = array(
                'EarnedAmount'    => '0.00',
                'UsedAmount'      => $bnftTrackData->EarnedAmount,
                'UpdatedDateTime' => $dateTime,
                'UpdatedBy'       => config('vwallet.userTypeKey')
            );
            \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $bnftTrackData->UserOfferBenefitTrackerID)->update($updateData);
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        }

        return $output_arr;
    }

    public function updateExpiryAmount(\Illuminate\Http\Request $request)
    {
        $dateTime = date('Y-m-d H:i:s');

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount')->where('EarnedAmount', '>', '0.00')->where('ExtendedValidityDate', '<', $dateTime)->orderBy('ExtendedValidityDate', 'desc')->first();
        if ( $bnftTrackData )
        {
            $updateData = array(
                'EarnedAmount'    => '0.00',
                'ExpiredAmount'   => $bnftTrackData->EarnedAmount,
                'UpdatedDateTime' => $dateTime,
                'UpdatedBy'       => config('vwallet.userTypeKey')
            );
            \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $bnftTrackData->UserOfferBenefitTrackerID)->update($updateData);
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        }

        return $output_arr;
    }

    static protected $staticBenefitCriteria = array( '28' => 'Fixed', '29' => 'Percentage', '30' => 'Least', '31' => 'Highest' );

    public static function getBenefitAmount($dbRecords)
    {
        $promoCode       = $dbRecords->PromoCode;
        $amount          = $dbRecords->Amount;
        $getAmountStatus = TRUE;
        $output_arr      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $benefitCriteriaRefID       = $dbRecords->BenefitCriteriaRefID;
        $staticBenefitCriteriaArray = self::$staticBenefitCriteria;
        $benefitCriteriaCode        = NULL;
        if ( isset($staticBenefitCriteriaArray[$benefitCriteriaRefID]) )
        {
            $benefitCriteriaCode = $staticBenefitCriteriaArray[$benefitCriteriaRefID];
        }

        switch ( trim($benefitCriteriaCode) )
        {

            case 'Fixed':
                //BenefitFixedAmount
                $benifitAmount = $dbRecords->BenefitFixedAmount;
                break;
            case 'Percentage':
                //BenefictPercentageValue
                $benifitAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                break;
            case 'Least':
                //Least Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount < $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            case 'Highest':
                //Heighest Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / $amount) * 100;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount > $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            default:
                $benifitAmount = '0.00';
        }

        $benifitAmount          = number_format(( float ) $benifitAmount, 2, '.', '');
        $output_arr['res_data'] = array( 'benifitAmount' => $benifitAmount );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }
   
    public static function offerReferenceData($requestData = array())
    {
        if ( empty($requestData) )
        {
            $requestData = [];
            //$requestData = [ 'ActiveIndicator' => 'Y' ];
        }
        $dbResults  = \App\Models\Reference::select('ReferenceID', 'ReferenceCode', 'ReferenceDesc', 'ActiveIndicator')
                ->orderBy('ReferenceId')
                ->where($requestData)
                ->get();
        $returnData = [];
        if ( $dbResults->count() > 0 )
        {
            foreach ( $dbResults as $eachReference )
            {
                $returnData[$eachReference->ReferenceID] = $eachReference->ReferenceCode;
            }
        }
        return $returnData;
    } */

    /**
     * Get offerBenefitTrack
     * @param Request $request
     * @return type
     */
    public function getBeniftTrack(\Illuminate\Http\Request $request)
    {
        $maxCashback = config('vwallet.maxCashback');
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateUser($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $dateTime              = date('Y-m-d H:i:s');
        $partyId               = $request->input('partyId');
        $amount                = $request->input('amount');
                
        $superCashData = \App\Libraries\OfferRules::getSuperCash($partyId, $amount);
       
        return response()->json($superCashData);
    }

    /**
     * Validate code request data
     * @param $request
     * @return String
     */
    private function _validateUser($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
        ];

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    public function favouritePromoCode(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validFavouritePromoCode($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $partyId = $request->input('partyId');
        $promoCode = $request->input('promoCode');
        $status = $request->input('status');
        $msg = ($status == 'Like') ? 'added to' : 'removed from';        
        //$offerStatsData = \App\Models\Useroffersstat::where('UserWalletID', $partyId)->where('PromoCode', $promoCode)->where('UserActivity', 'Like')->orWhere('UserActivity', 'Unlike')->first();
        $offerStatsData     = \App\Models\Useroffersstat::where('UserWalletID', $partyId)->where('PromoCode', $promoCode)->where(function ($query) {
              $query->where('UserActivity', '=', 'Like')
                    ->orWhere('UserActivity', '=', 'Unlike');
        })->first();
        
        if ($offerStatsData === NULL)
        {
            $verifyArray = array(
                'partyId'   => $partyId,
                'promoCode' => $promoCode,
                'paymentProductId' => '0',
                'isVerified' => TRUE
            );
            $verifyPromoCode = \App\Libraries\OfferRules::verifyPromoCode($verifyArray);
            $verifyData = json_decode($verifyPromoCode);
                        
            if ($verifyData->is_error === TRUE)
            {
                $output_arr['display_msg']['info'] = $verifyData->display_msg->info;
                return $output_arr;
            }
            $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($verifyData->res_data); 
            $insertUserOfferStats = \App\Libraries\TransctionHelper::userOfferStatsInsert($partyId, $pamentInfo, $status);
            $output_arr['display_msg']['info'] = trans('messages.updatefavpromocode', ['action' => $msg]);
            $output_arr['is_error'] = FALSE;
            return $output_arr;
        }
        else
        {
            $updateAarray = array(
                'UserActivity'    => $status,
            );
            \App\Models\Useroffersstat::where('UserUsedOffersId', $offerStatsData->UserUsedOffersId)->update($updateAarray);
            $output_arr['display_msg']['info'] = trans('messages.updatefavpromocode', ['action' => $msg]);
            $output_arr['is_error'] = FALSE;
            return $output_arr;
        }
    }
    
    public function getFavouritePromos(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validFavouritePromos($request);
        $ruleNames    = \App\Libraries\OfferRules::offerReferenceData();
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId        = $request->input('partyId');
        $promoCode      = $request->input('promoCode');
        $status         = $request->input('status');
        $msg            = ($status == 'Like') ? 'added' : 'remove';
        
        $selectedColumns = [
            'screenbanners.BannerName',
            'screenbanners.BannerImageLocation',
            'screenbanners.Filler1',
            'screenbanners.ActiveIndicator',
            'promotionaloffers.OfferBusinessID',
            'promotionaloffers.OfferCategoryRefID',
            'promotionaloffers.OfferName',
            'promotionaloffers.PromoCode',
            'promotionaloffers.OfferDescription',
            'promotionaloffers.OfferBannerImageID',
            'promotionaloffers.ActiveInd',
            'useroffersstats.userActivity',
            'useroffersstats.UserWalletID'
        ];
        $dbResults       = \App\Models\Promotionaloffer::join('screenbanners', 'screenbanners.ScreenBannerID', '=', 'promotionaloffers.OfferBannerImageID')
                        ->leftjoin('useroffersstats', 'useroffersstats.OfferBusinessID', '=', 'promotionaloffers.OfferBusinessID')
                        ->where('promotionaloffers.ActiveInd', 'Y')
                        ->where('useroffersstats.UserWalletID', $partyId)
                        ->where('useroffersstats.userActivity', 'Like') 
                        ->get($selectedColumns)->toArray();
        if ( empty($dbResults) )
        {
            $output_arr['is_error']    = FALSE;
            return response()->json($output_arr);
        }
        $offersData = [];
        $bannerUrl  = \App\Libraries\Helpers::bannerUrl();
        foreach ( $dbResults as $key => $eachOffer )
        {
            $offersData[$key]                        = $eachOffer;
            $offersData[$key]['OfferCategoryRefID']      = $ruleNames[$eachOffer['OfferCategoryRefID']];
            $offersData[$key]['BannerImageLocation'] = $bannerUrl . $eachOffer['BannerImageLocation'];
            $offersData[$key]['Filler1'] = $bannerUrl . $eachOffer['Filler1'];
        }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array();
        $output_arr['res_data']    = $offersData;
        return response()->json($output_arr);
    }
   
    private function _validFavouritePromoCode($request)
    {
        $validate = [
            'partyId'   => 'required|numeric',
            'promoCode' => 'required',
            'status'    => 'required|in:Like,Unlike',
        ];
        $messages = [];
        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    private function _validFavouritePromos($request)
    {
        $validate = [
            'partyId'   => 'required|numeric',            
        ];
        $messages = [];
        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
