<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 16 Nov 2017 06:22:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Operatorfeeplan
 * 
 * @property int $FeePlanID
 * @property string $Operator
 * @property string $Circle
 * @property string $CategoryName
 * @property float $Mrp
 * @property float $TalkTime
 * @property float $ServiceTax
 * @property float $ProcessingFees
 * @property float $AccessFees
 * @property string $Validity
 * @property string $Remarks
 * @property string $Type
 * @property string $Description
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 *
 * @package App\Models
 */
class Operatorfeeplan extends Eloquent
{
	protected $table = 'operatorfeeplan';
	protected $primaryKey = 'PlanID';
	public $timestamps = false;

	protected $casts = [
		'Mrp' => 'float',
		'TalkTime' => 'float',
		'ServiceTax' => 'float',
		'ProcessingFees' => 'float',
		'AccessFees' => 'float'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'Operator',
		'Circle',
		'CategoryName',
		'Mrp',
		'TalkTime',
		'ServiceTax',
		'ProcessingFees',
		'AccessFees',
		'Validity',
		'Remarks',
		'Type',
		'Description',
		'CreatedDateTime',
		'CreatedBy'
	];
}
