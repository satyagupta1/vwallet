<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Refer (By Sender) template
 * 
 *  param $senderName varchar
 *  param $receiverName varchar
 *  param $referalURL varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($senderName) ? $salutation . ' ' . $senderName : $salutation;
$array = [
    'email' => [
        'subject' => 'Join ViolaWallet Family and start earning.',
        'text'    => $salutationName . ',<br/><br/>
                    '.$receiverName.' has invited you to be a part of ViolaFamily. Click on the link below to get started and earn ( feature, offers, benefits) with us.
                   ',
    ],
    'sms'   => [
        'text' => '"Hello, '.$receiverName.' has invited you to be a part of ViolaFamily. Click on the link to get started and earn ( feature, offers, benefits) with us.' ,
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file refer_by_sender.php */
