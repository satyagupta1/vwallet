<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Offersponsorcommercial
 * 
 * @property int $OfferCommercialID
 * @property int $OfferBusinessID
 * @property float $OfferVersion
 * @property int $OfferSponserRefID
 * @property string $ShareType
 * @property float $ShareAmount
 * @property float $SharePercentage
 * @property float $VarianceAmout
 * @property float $VariancePercentage
 * @property float $CommissionPerTrans
 * @property float $PaymentFee
 * @property float $Tax
 * 
 * @property \App\Models\Promotionaloffer $promotionaloffer
 *
 * @package App\Models
 */
class Offersponsorcommercial extends Eloquent
{
	protected $primaryKey = 'OfferCommercialID';
	public $timestamps = false;

	protected $casts = [
		'OfferBusinessID' => 'int',
		'OfferVersion' => 'float',
		'OfferSponserRefID' => 'int',
		'ShareAmount' => 'float',
		'SharePercentage' => 'float',
		'VarianceAmout' => 'float',
		'VariancePercentage' => 'float',
		'CommissionPerTrans' => 'float',
		'PaymentFee' => 'float',
		'Tax' => 'float'
	];

	protected $fillable = [
		'OfferBusinessID',
		'OfferVersion',
		'OfferSponserRefID',
		'ShareType',
		'ShareAmount',
		'SharePercentage',
		'VarianceAmout',
		'VariancePercentage',
		'CommissionPerTrans',
		'PaymentFee',
		'Tax'
	];

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'OfferBusinessID', 'OfferBusinessID')
					->where('promotionaloffers.OfferBusinessID', '=', 'offersponsorcommercials.OfferBusinessID')
					->where('promotionaloffers.OfferVersion', '=', 'offersponsorcommercials.OfferVersion');
	}
}
