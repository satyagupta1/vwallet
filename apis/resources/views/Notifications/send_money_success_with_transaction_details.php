<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Send Money Success with Transaction details device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $receivername varchar
 *  param $number varchar
 *  param $time varchar
 *  param $date varchar
 *  param $medium varchar
 *  param $transctionid varchar
 *  param $accountnumber varchar
 *  param $refnumber varchar
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($senderName) ? $salutation . ' ' . $senderName : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction Successful',
        'text'    => $salutationName . ',<br/><br/>
INR ' . $amount . ' has been transferred to ' . $accountName . ' and ' . $mobileNumber . ' on '.$date . ' '.$time.'. <br/>
Available Balance INR '.$balanceAmount.'
',
    ],
    'sms'   => [
        'text' => 'Payment of ' . $amount . ' was successfully transferred to  ' . $accountName . ' / ' . $mobileNumber . '. The current amount in your ViolaWallet is ' . $balanceAmount . '.
Transaction ID: ' . $transactionId . '
Time: ' . $time . '
Date: ' . $date . ''
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Rs '.$amount.' debited towards payment to '.$accountName,
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Rs ' . $amount . ' transferred to ' . $receiverName . ' successfully',
    ],
];

echo json_encode($array);

/* End of file send_money_success_with_transaction_details.php */
