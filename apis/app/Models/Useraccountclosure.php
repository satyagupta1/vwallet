<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Useraccountclosure
 * 
 * @property int $UserID
 * @property string $ClosureReason
 * @property \Carbon\Carbon $CreatedDateTime
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Useraccountclosure extends Eloquent
{
	protected $table = 'useraccountclosure';
	protected $primaryKey = 'UserID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'ClosureReason',
		'CreatedDateTime'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserID');
	}
}
