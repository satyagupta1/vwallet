<?php

namespace App\Http\Controllers\User;

class PaymentCardController extends \App\Http\Controllers\Controller {

    
    public function __construct()
    {
        $this->encKey = sha1(config('vwallet.ENCRYPT_STATIC'));
    }
    
    /**
     * This used to  save cards details
     * @desc  This used to  save cards details provided from user profile screen.
     * @param PartyId integer, cardNo integer, cardExp integer, cardholderName string, cardCvv integer, moneySource string.  
     * @return Array
     */
    public function saveCardDetails(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        
        $check_card_details = $this->_cardValid($request);
        if ( $check_card_details !== TRUE )
        {
            $output_arr['display_msg'] = $check_card_details;
            return json_encode($output_arr);
        }
        
        $partyId                = $request->input('partyId');
        
        $userCheck = $this->_userIdValidate($partyId);
        if ($userCheck !== TRUE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }
        
        // Check if card details already exists.
        
        $isDuplicateCard = FALSE;
        $select_wallet_savedcards = \App\Models\Userexternalcard::
                select( 'CardNumber', 'MoneySourceCode', 'CardVendor','CardExpiryDate', 'CardHolderName')
              ->where('PartyID', '=', $request->input('partyId'))
              ->where('ActiveIndicator', '=', "Y" )
              ->get();
        if( count($select_wallet_savedcards) )
        {
            foreach( $select_wallet_savedcards as $singleRow2 )
            {
                if( $request->input('cardNo') == \App\Libraries\Crypt\Decryptor::decrypt( $singleRow2 -> CardNumber, $this->encKey, true) )
                {
                    $isDuplicateCard = TRUE;
                    break;
                }
            }
        }    
            
        if ($isDuplicateCard == TRUE)
        {
            $output_arr['display_msg'] = array('info' => "Card details already exists.");
            return json_encode($output_arr);
        }  
        $encryCardNo   = \App\Libraries\Crypt\Encryptor::encrypt($request->input('cardNo'), $this->encKey);
           
        // insert card details..
        $saveCardDetails          = new \App\Models\Userexternalcard;
        $saveCardDetails->CardHolderName    = $request->input('cardholderName');
        $saveCardDetails->MoneySourceCode   = $request->input('moneySource');
        $saveCardDetails->CardNumber        = $encryCardNo;
        $saveCardDetails->CardDisplayNumber = substr_replace($request->input('cardNo'), str_repeat('x', strlen($request->input('cardNo')) - 4), 0, -4);
        $saveCardDetails->CardExpiryDate    = $request->input('cardExp');
        
        $saveCardDetails->CreatedDateTime   = date('Y-m-d H:i:s');
        $saveCardDetails->CreatedBy         = $request->input('partyId');
        $saveCardDetails->PartyID         = $request->input('partyId');
        $saveCardDetails->save();
        
        $output_arr['display_msg'] = array('info' => "Card details inserted");
        $output_arr['is_error']    = FALSE;
        return $output_arr;
    }
    
    /**
     * This used to  delete cards details
     * @desc  This used to  delete cards details provided from user profile screen.
     * @param PartyId integer, savedcardId integer
     * @return Array
     */
    public function deleteCardDetails(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        
        $check_card_details = $this->_validateDeleteCard($request);
        if ( $check_card_details !== TRUE )
        {
            $output_arr['display_msg'] = $check_card_details;
            return json_encode($output_arr);
        }
        
        $partyId                = $request->input('partyId');
        $deleteCardID           = $request->input('savedcardId');
        
        $userCheck = $this->_userIdValidate($partyId);
        if ($userCheck !== TRUE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }
        
        // Check if card details exists.
        
        $select_wallet_savedcards = \App\Models\Userexternalcard::
                select( 'UserExternalCardId', 'CardNumber')
                ->where('PartyID', '=', $request->input('partyId'))
                ->where('UserExternalCardId', '=',  $deleteCardID )
                ->where('ActiveIndicator', '=', "Y" )
                ->get();
        
        if( ! count($select_wallet_savedcards) )
        {
             $output_arr['display_msg'] = array('info' => "Card does not exists.");
            return json_encode($output_arr);
        }    
        
//        $cardDetails      = \App\Models\Userexternalcard::find($deleteCardID);
//        $cardDetails->ActiveIndicator = 'N';
//        $cardDetails->save();

        
        \App\Models\Userexternalcard::where('UserExternalCardId', $deleteCardID)
                ->update(array('ActiveIndicator' => 'N'));
            
        $output_arr['display_msg'] = array('info' => "Card details deleted.");
        $output_arr['is_error']    = FALSE;
        return $output_arr;
    }
    
    
    /**
     * This used to call saved cards method in this listSavedcards method
     * @desc  This used to call saved cards method in this listSavedcards method
     * @desc In this we will check validationCheck_wallet method for validation of party id and device type
     * @desc If all validations have no errors will call _getPartyExternalCards method to get list of all saved cards in array list response
     * 
     * @param PartyId integer
     *  
     * @return Array
     */
    public function listSavedcards(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        // check validation
        $chk_valid  = $this->validationCheck_wallet($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        else
        {
            $party_id = $request->input('partyId');
            $listCards = $this->_getPartyExternalCards($party_id);
            if (count($listCards) == 0)
            {
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array( 'info' => trans('messages.cardsNotFound') );
                return json_encode($output_arr);
            }

            $output_arr['is_error'] = FALSE;
            $output_arr['res_data'] = $listCards;
            return json_encode($output_arr);
        }
    }
    
    /**
     * This used to get all users saved cards details
     * @desc  This used to get all users saved cards details
     * @param PartyId integer
     *  
     * @return Array
     */
    private function _getPartyExternalCards($party_id)
    {

        $select_wallet_savedcards = \App\Models\Userexternalcard::
                select('MoneySourceCode', 'CardVendor', 'CardNumber', 'CardDisplayNumber', 'CardExpiryDate', 'NetBankCode', 'CardHolderName', 'UserExternalCardID', 'ActiveIndicator')
                ->where('PartyID', '=', $party_id)
                ->where('ActiveIndicator', '=', "Y" )
                ->get();
        if($select_wallet_savedcards){
            foreach($select_wallet_savedcards as $cardDetails){
                if(strlen($cardDetails['CardNumber']) <= '16') {
                    $cardDetails['CardNumber'] = $cardDetails['CardNumber'];
                } else{
                    $cardDetails['CardNumber'] = \App\Libraries\Crypt\Decryptor::decrypt($cardDetails['CardNumber'], $this->encKey);
                }
            }
            return $select_wallet_savedcards;
        }
        return FALSE;
    }
    
    /**
     * Validate before insert into card details
     * @desc  this is used to check validation for add topup card details insert
     * @param Request $request
     * @param card_no integer
     * @param card_exp integer
     * @param card_hname string
     * @param money_source string
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _cardValid($request)
    {

        $validat  = array(
            'cardNo'         => 'required|min:13|max:19',
            'cardExp'        => 'required|min:4|max:4',
            'cardholderName' => 'required|min:3|max:50',
            //'cardCvv'        => 'required|min:3|max:4',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validat, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    private function _validateDeleteCard($request)
    {

        $validat  = array(
            'savedcardId'         => 'required|numeric',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validat, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
/*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _userIdValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', '=', $partyId)->where('ProfileStatus', '=', 'A');
        if (!$userExsist)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    /**
     * Validate before showing cards and wallet details
     * @desc  this is used to check validation for card details and wallet details
     * @param Request $request
     * @param PartyId integer
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    public function validationCheck_wallet($request)
    {
        // validations
        $validate = array(
            'partyId' => 'required|numeric',
        );
        $messages = array();

        // validation for mobile device

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
