<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partydocument
 * 
 * @property int $DocumentID
 * @property int $PartyID
 * @property string $CategoryCode
 * @property string $DocumentTypeCode
 * @property string $Status
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Documentcategory $documentcategory
 * @property \App\Models\Documenttype $documenttype
 * @property \Illuminate\Database\Eloquent\Collection $merchantverificationdetails
 * @property \Illuminate\Database\Eloquent\Collection $userwarrenties
 *
 * @package App\Models
 */
class Partydocument extends Eloquent
{
	protected $table = 'partydocument';
	protected $primaryKey = 'DocumentID';
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyID',
		'CategoryCode',
		'DocumentTypeCode',
		'Status',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function documentcategory()
	{
		return $this->belongsTo(\App\Models\Documentcategory::class, 'CategoryCode');
	}

	public function documenttype()
	{
		return $this->belongsTo(\App\Models\Documenttype::class, 'DocumentTypeCode');
	}

	public function merchantverificationdetails()
	{
		return $this->hasMany(\App\Models\Merchantverificationdetail::class, 'DocumentID');
	}

	public function userwarrenties()
	{
		return $this->hasMany(\App\Models\Userwarrenty::class, 'InvoiceDocumentID');
	}
}
