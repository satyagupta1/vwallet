<?php
namespace App\Http\Controllers\StaticContent;

/**
 * StaticContentController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */

class StaticContentController extends \App\Http\Controllers\Controller {
    /*
     *  Get Terms and Conditions content
     *  @param Object $request
     *  @return JSON
     */

    public function getTermsandConditions(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyTermID', 'PolicyName', 'PolicyVersion', 'PolicyEffectiveDate', 'PolicyExpiryDate', 'ActiveInd', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'TermsandConditions')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            //return response($response)->header('session-expired', 'N');
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);
        //$returnFileContent = str_replace( "\r\n", "<br />", $returnFileContent );

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array(
            'PolicyTermID'        => $result['PolicyTermID'],
            'PolicyName'          => $result['PolicyName'],
            'PolicyVersion'       => $result['PolicyVersion'],
            'PolicyEffectiveDate' => $result['PolicyEffectiveDate'],
            'PolicyExpiryDate'    => $result['PolicyExpiryDate'],
            'ActiveInd'           => $result['ActiveInd'],
            'content'             => $returnFileContent );
        //return response($response)->header('session-expired', 'N');
        return $response;
    }

    /*
     *  Get Anti Bribery and Corruption Policy content
     *  @param Object $request
     *  @return JSON
     */

    public function getAntiBriberyandCorruptionPolicy(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'AntiBriberyandCorruptionPolicy')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get Coockie Policy content
     *  @param Object $request
     *  @return JSON
     */

    public function getCoockiePolicy(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'CoockiePolicy')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get Privacy Policy content
     *  @param Object $request
     *  @return JSON
     */

    public function getPrivacyPolicy(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'PrivacyPolicy')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get AML Policies and Procedures content
     *  @param Object $request
     *  @return JSON
     */

    public function getAMLPoliciesandProcedures(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'AMLPoliciesandProcedures')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $imagePath     = config('vwallet.baseUrl') . config('vwallet.RESOURCE_LANG_URL') . $fileLang . config('vwallet.STATIC_CONTENT_IMAGE_URL');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);
        $returnFileContent = str_replace("BASE_IMAGE_URL", $imagePath, $returnFileContent);


        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get AML Policies and Procedures content TEST
     *  @param Object $request
     *  @return JSON
     */

    public function getAMLPoliciesandProceduresBase(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'AMLPoliciesandProceduresBase')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get AML Policies and Procedures content TEST
     *  @param Object $request
     *  @return JSON
     */

    public function getAMLPoliciesandProceduresDynamic(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'AMLPoliciesandProceduresDynamic')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $imagePath     = config('vwallet.baseUrl') . config('vwallet.RESOURCE_LANG_URL') . $fileLang . config('vwallet.STATIC_CONTENT_IMAGE_URL');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);
        $returnFileContent = str_replace("BASE_IMAGE_URL", $imagePath, $returnFileContent);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get Refund Policy content
     *  @param Object $request
     *  @return JSON
     */

    public function getRefundPolicy(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'RefundPolicy')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get Risk Policy content
     *  @param Object $request
     *  @return JSON
     */

    public function getRiskPolicy(\Illuminate\Http\Request $request)
    {

        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $result = \App\Models\Policyterm::select('PolicyName', 'TermsCompleteTextPath')
                ->where('PolicyName', '=', 'RiskPolicy')
                ->where('ActiveInd', '=', 'Y')
                ->orderBy('PolicyTermID', 'DESC')
                ->first();

        if ( ! $result )
        {
            $response['display_msg'] = array( 'info' => trans('messages.emptyRecords'));
            return $response;
        }

        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $imagePath     = config('vwallet.baseUrl') . config('vwallet.RESOURCE_LANG_URL') . $fileLang . config('vwallet.STATIC_CONTENT_IMAGE_URL');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = $result['TermsCompleteTextPath'];

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);
        $returnFileContent = str_replace("BASE_IMAGE_URL", $imagePath, $returnFileContent);

        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $returnFileContent );

        return $response;
    }

    /*
     *  Get FAQ content
     *  @param Object $request
     *  @return JSON
     */

    public function getFaqs_Old(\Illuminate\Http\Request $request)
    {
        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        
          // check validation
        $chk_valid = $this->_ValidationFAQ($request);
        if ( $chk_valid !== TRUE )
        {
            $response['display_msg'] = $chk_valid;
            return json_encode($response);
        } 
        $partyId  = $request->input('partyId');
        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = config('vwallet.STATIC_CONTENT_FAQ_FILE');

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            //return response($response)->header('session-expired', 'N');
            return $response;
        }
        $returnFileContent = file_get_contents($fileFullPath);

        if( $partyId > 0 )
        {
           $returnFileContent .=  "<script>
     scrollToView();</script> ";
        }
        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'partyId' => $request->input('partyId'),
            'content' => $returnFileContent );

        //return response($response)->header('session-expired', 'N');
        return $response;
    }
    private function _ValidationFAQ($request)
    {
        // validations
        $validate = [];
        if($request->input('partyId')!= NULL){        
            $validate['partyId'] = 'required|numeric';
        }
        $messages = array(); 
        
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function getWebFaqs(\Illuminate\Http\Request $request)
    {
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $faqs = trans('violaWalletWebFaqs.webFaqs');
        // If no FAQ data found for that language.
        if ( count($faqs) == 1 )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return $response;
        }
        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'content' => $faqs );
        return response()->json($response);
    }

    public function getFaqs(\Illuminate\Http\Request $request)
    {
        // Default Response
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        
          // check validation
        $chk_valid = $this->_ValidationFAQ($request);
        if ( $chk_valid !== TRUE )
        {
            $response['display_msg'] = $chk_valid;
            return json_encode($response);
        } 
        $partyId  = $request->input('partyId');
       
        // get language of file from header. Default: en 
        $fileLang      = $request->header('language', 'en');
        $filePathOne   = config('vwallet.RESOURCE_LANG_PATH');
        $filePathTwo   = config('vwallet.STATIC_CONTENT_PATH');
        $fetchFileName = config('vwallet.STATIC_CONTENT_FAQ_FILE');

        $fileFullPath = base_path() . $filePathOne . $fileLang . $filePathTwo . $fetchFileName;

        if ( ! file_exists($fileFullPath) )
        {
            $response['display_msg'] = array( 'info' => trans('messages.noFile') );
            return response($response)->header('session-expired', 'N');
            //return $response;
        }
       //$returnFileContent =  view('StaticContent' . DIRECTORY_SEPARATOR . 'faq'  );
        $returnFileContent = file_get_contents($fileFullPath);
        /* die;
        
        $returnFileContent = file_get_contents($fileFullPath);
*/
        if( $partyId > 0 )
        {
           $returnFileContent .=  "
               <script>
                setTimeout(function(){
                    var elem = document.getElementById('post_login');
                     elem.nextElementSibling.nextElementSibling.click();
                     window.scrollTo(0, elem.offsetTop);
                }, 500);
            </script>
               ";
        } 
        $response['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $response['is_error']    = FALSE;
        $response['res_data']    = array( 'partyId' => $request->input('partyId'),
            'content' => $returnFileContent );

        return response($response)->header('session-expired', 'N');
        //return $response;  
    }
}

/* End of file StaticContentController.php */ /* Location: ./Http/StaticContent/StaticContentController.php */