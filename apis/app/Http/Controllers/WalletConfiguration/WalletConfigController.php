<?php

namespace App\Http\Controllers\WalletConfiguration;

use \Illuminate\Http\Request;

class WalletConfigController extends \App\Http\Controllers\Controller {

    public function setPlanConfigs(Request $request)
    {
        $checkValidate = $this->_validateParams($request);
        if ( $checkValidate !== TRUE )
        {
            return response()->json($checkValidate);
        }

        $walletType = $request->input('WalletPlanTypeId');

        $productEntity = \App\Models\Paymententity::select('PaymentEntityID', 'PaymentEntityName')->where('ActiveFlag', 'Y')->get();

        foreach ( $productEntity as $prodEnt )
        {
            $dateTime      = date('Y-m-d H:i:s');
            $user          = 'Admin';
            $effictiveDate = date('Y-m-d');
            $endDate       = date('Y-m-d', strtotime('+1 years'));

            if ( $prodEnt->PaymentEntityID < 10 )
            {
                switch ( $prodEnt->PaymentEntityID )
                {
                    case 1:
                        $payProdId = array( 2, 3, 5 );
                        break;
                    case 2:
                        $payProdId = array( 1 );
                        break;
                    case 3:
                        $payProdId = array( 4 );
                        break;
                    case 4:
                        $payProdId = array( 4 );
                        break;
                    case 5:
                        $payProdId = array( 1 );
                        break;
                    case 6:
                        $payProdId = array( 1 );
                        break;
                    case 7:
                        $payProdId = array( 1 );
                        break;
                    case 8:
                        $payProdId = array( 1 );
                        break;
                    case 9:
                        $payProdId = array( 1 );
                        break;
                }
                $products = \App\Models\Paymentsproduct::select('PaymentsProductID', 'ProductName')->whereIn('PaymentsProductID', $payProdId)->get();
            }
            else
            {
                $products = \App\Models\Paymentsproduct::select('PaymentsProductID', 'ProductName')->where('PaymentsProductID', '>', 7)->get();
            }

            foreach ( $products as $prod )
            {
                $configParamNum   = \App\Models\Productplanconfigparameter::max('ConfigParamNum');
                $configParamNum   += 1;
                $max              = ($prod->PaymentsProductID <= 7) ? 10000 : 100000;
                $planCongigParams = array(
                    array( 'ParameterTagName' => 'MinTransAmt', 'ParameterTagValue' => 0.01, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'MaxTransAmt', 'ParameterTagValue' => $max, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'TransFeePerc', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'TransFixFeeAmt', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'TaxGSTPerc', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'TaxSGSTPerc', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'TaxIGSTPerc', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'FeeMode', 'ParameterTagValue' => 'F', 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'RevFeePerc', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                    array( 'ParameterTagName' => 'RevFixFeeAmt', 'ParameterTagValue' => 0, 'ActiveFlag' => 'Y', 'ConfigParamNum' => $configParamNum, 'EffectiveDate' => $effictiveDate, 'ExpiryDate' => $endDate, 'CreatedDatetime' => $dateTime, 'CreatedBy' => $user, 'UpdatedDateTime' => $dateTime, 'UpdatedBy' => $user ),
                );

                $planconfigArray = array(
                    'PaymentEntityID'  => $prodEnt->PaymentEntityID,
                    'PaymentProductID' => $prod->PaymentsProductID,
                    'WalletPlanTypeId' => $walletType,
                    'FeeFreqCycle'     => 'D',
                    'ActiveFlag'       => 'Y',
                    'ConfigParamNum'   => $configParamNum,
                    'EffectiveDate'    => $effictiveDate,
                    'ExpiryDate'       => $endDate,
                    'CreatedDatetime'  => $dateTime,
                    'CreatedBy'        => $user,
                    'UpdatedDateTime'  => $dateTime,
                    'UpdatedBy'        => $user
                );

//                print_r($planconfigArray);
//                print_r($planCongigParams);
//                exit;

                \DB::beginTransaction();
                try
                {
                    \App\Models\Productplanconfigparameter::insert($planCongigParams);
                    \App\Models\Productplanconfig::insert($planconfigArray);
                    \DB::commit();
                }
                catch ( \Exception $e )
                {
                    \DB::rollback();
                    \log::alert('DB Error' . json_encode($e, JSON_PRETTY_PRINT));
                    \log::alert(json_encode($planCongigParams, JSON_PRETTY_PRINT));
                    \log::alert(json_encode($planconfigArray, JSON_PRETTY_PRINT));
                }
//                break;
            }
           // break;
        }


        return 'Done';
    }

    private function _validateParams($request)
    {
        $validate = array(
            'WalletPlanTypeId' => 'required|numeric',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
