<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  OTP with fund details template
 *  param $fullname varchar
 *  param $otpCode varchar 
 *  param $balanceAmount numeric 
 *  param $posMerchantName varchar 
 *  param $amount varchar  
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'One Time Password',
        'text'    => $salutationName . ",<br/><br/> 
" . $otpCode . " is One time password (OTP)for your online transaction at " . $posMerchantName . " for 
    INR " . $amount . " on your ViolaWallet.
This OTP is valid for 10 minutes or 1 successful attempt, whichever is earlier. Please note that this OPT is valid only for this transaction and cannot be used for any other transaction.
Please don't share your OTP with anyone."],
    'sms'   => [
        'text' => $otpCode . ' is your One Time Passcode(OTP). Please use this code to make the payment at '. $posMerchantName .'. 
     Available Balance:' . $balanceAmount,
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file pos_otp_with_fund_details.php */
