<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * BusTicket Transaction Fails
 * 
 *  param $placeOne varchar
 *  param $placeTwo varchar 
 *  param $customerName varchar 
 *  param $busOperator varchar 
 *  param $referenceNum varchar 
 *  param $busServiceprovider varchar 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $bookingId varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($customerName) ? $salutation.' '.$customerName : $salutation;
$array = [
    'email' => [
        'subject' => 'Bus Booking from '.$placeOne.' to '.$placeTwo.', declined ',
        'text'    => $salutationName . ',<br/><br/>
This is to let you know that your '.$placeOne.' to '.$placeTwo.' bus booking could not be confirmed. We apologise for the inconvenience caused to you. Please note your '.$bookingId.' for reference. Use this for all future communication related to this booking with your '.$busOperator.'. <br/><br/>
In case money has been deducted from your wallet, rest assured that your money is safe and will be refunded automatically and should reflect in your wallet/card/account within 10 days depending on your bank.<br/><br/>
Book Again.'],
    'sms'   => [
        'text' => 'Your payment of INR '.$amount.' did not go through.
Date:'.$date.'
Time:'.$time.'
Reference Number:'.$referenceNum.'
Bus Service Provider:'.$busServiceprovider,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Bus Booking was declined',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Sorry! Your bus booking could not get confirmed Booking ID : '.$bookingId,
    ],
];

echo json_encode($array);

/* End of file bus_ticket_transaction_fails */
