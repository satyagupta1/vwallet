<?php

/**
 * MyAccountController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\User;

/**
 * MyAccountController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class MyAccountController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
    }

    /*
     *  profileDetails - To Read User Information
     *  @param Request $request
     *  @return JSON
     */

    private function _UserVerify($request, $validMethod)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->$validMethod($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }

        // user exsistens test
        $partyId   = $request->input('partyId');
        $userCheck = $this->_userIdValidate($partyId);
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;

        return $output_arr;
    }

    /*
     *  profileDetails - To Read User Information
     *  @param Request $request
     *  @return JSON
     */

    public function profileDetails(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_userValidation');

        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        $partyId       = $request->input('partyId');
        $userInfoArray = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.LastName', 'userwallet.MiddleName', 'userwallet.Gender', 'userwallet.DateofBirth', 'partyaddress.AddressLine1', 'partyaddress.AddressLine2', 'partyaddress.City', 'partyaddress.State', 'partyaddress.Country', 'partyaddress.PostalZipCode', 'userwallet.MobileNumber', 'userwallet.EmailID', 'userbeneficiary.BeneficiaryVPA')
                        ->join('partyaddress', 'partyaddress.PartyID', '=', 'userwallet.UserWalletID', 'LEFT')
                        ->join('userbeneficiary', 'userbeneficiary.UserWalletID', '=', 'userwallet.UserWalletID', 'LEFT')
                        ->where('userwallet.UserWalletID', $partyId)
                        ->where('partyaddress.AddressTypeCode', '=', 'HOM')
                        ->where('userwallet.ProfileStatus', 'A')->first();
        if ( ! $userInfoArray )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }
        else
        {
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => $userInfoArray );
        }

        return $output_arr;
    }

    /*
     *  updateUserProfile - Update User Profile
     *  @param Request $request
     *  @return JSON
     */

    public function updateUserProfile(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_profileInfoValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        $partyId       = $request->input('partyId');
        // get User Info
        $userInfoArray = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.DateofBirth', 'partyaddress.AddressLine1', 'partyaddress.AddressLine2', 'partyaddress.City', 'partyaddress.State', 'partyaddress.Country', 'partyaddress.PostalZipCode')
                        ->join('partyaddress', 'partyaddress.PartyID', '=', 'userwallet.UserWalletID', 'LEFT')
                        ->where('UserWalletID', '=', $partyId)
                        ->where('ProfileStatus', '=', 'A')->first();

        if ( ! $userInfoArray )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }

        // date of birth validation
        $d1 = new \DateTime(date('Y-m-d'));
        $d2 = new \DateTime($request->input('DateofBirth'));
        /* $diff = $d2->diff($d1);

          if ($diff->y > 100)
          {
          $output_arr['display_msg'] = array('info' => trans('messages.notValidDOB'));
          return $output_arr;
          }

          if ($diff->y < 18)
          {
          $output_arr['display_msg'] = array('info' => trans('messages.minorUser'));
          return $output_arr;
          }
         */

        // converting result array for utalising protecting information
        $userInfo = $userInfoArray->toArray();
        unset($userInfo['UserWalletID'], $userInfo['FirstName'], $userInfo['MiddleName'], $userInfo['LastName'], $userInfo['AddressLine1'], $userInfo['AddressLine2'], $userInfo['City'], $userInfo['State'], $userInfo['Country'], $userInfo['PostalZipCode']);
        if ( $userInfo['DateofBirth'] != '' )
        {
            $userInfo['DateofBirth'] = date('Y-m-d', strtotime($userInfo['DateofBirth']));
        }

        $date_added = date('Y-m-d H:i:s');

        // verify Data
        $userUpdateTracker = array();
        foreach ( $userInfo as $key => $value )
        {
            if ( $value != $request->input($key) )
            {
                $userUpdateTracker[] = array(
                    'UserWalletID'    => $partyId,
                    'UpdateFieldName' => $key,
                    'OldValue'        => $value,
                    'NewValue'        => $request->input($key),
                    'SystemIpAddress' => $request->header('ip-address'),
                    'CreatedDateTime' => $date_added,
                    'CreatedBy'       => 'User',
                );
            }
        }
        $userWalletUpdate = array(
            'Gender'          => $request->input('Gender'),
            'DateofBirth'     => $request->input('DateofBirth'),
            'UpdatedDateTime' => $date_added,
            'UpdatedBy'       => $request->input('UpdatedBy'),
        );
        if ( $request->input('FullName') != '' )
        {
            $fullName                       = $request->input('FullName');
            $result_name                    = \App\Libraries\Helpers::splitFullName($fullName);
            $userWalletUpdate['FirstName']  = $result_name['FirstName'];
            $userWalletUpdate['MiddleName'] = $result_name['MiddleName'];
            $userWalletUpdate['LastName']   = $result_name['LastName'];
        }

        if ( $request->header('device-type') == 'W' )
        {
            $userWalletUpdate['EmailID'] = $request->input('EmailID');
        }

        // update userwallet
        $userWallet = \App\Models\Userwallet::where('UserWalletID', $partyId)->update($userWalletUpdate);

        $parrtyAddressArray = array(
            'PartyID'          => $partyId,
            'AddressTypeCode'  => 'HOM',
            'AddressLine1'     => $request->input('AddressLine1'),
            'AddressLine2'     => $request->input('AddressLine2'),
            'City'             => $request->input('City'),
            'State'            => $request->input('State'),
            'Country'          => $request->input('Country'),
            'PostalZipCode'    => $request->input('PostalZipCode'),
            'AddressPartyType' => 'User',
            'PrimaryAddress'   => 'Y',
            'CreatedDateTime'  => $date_added,
            'CreatedBy'        => 'User',
            'UpdatedDateTime'  => $date_added
        );

        // check info exsist 
        $addressId = \App\Models\Partyaddress::select('AddressId')->where('PartyID', $partyId)->first();
        if ( ! $addressId )
        {
            // create partyAddress
            $partyAddress = \App\Models\Partyaddress::create($parrtyAddressArray);
        }
        else
        {
            // update partyAddress
            $partyAddress = \App\Models\Partyaddress::where('AddressId', $addressId->AddressId)->update($parrtyAddressArray);
        }

        // insert userupdatetracker
        if ( $userUpdateTracker )
        {
            $uptrack = \App\Models\Userupdatetracker::insert($userUpdateTracker);
        }

        $results = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                        ->where('UserWalletID', $partyId)->first();

        $usInAttr   = trans('messages.attributes.userInfo');
        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.updatedSuccessfully', [ 'name' => $usInAttr ]) ), 'status_code' => 200, 'res_data' => array() );

        return $output_arr;
    }

    /*
     *  uploadProfile - Get User Profile Image
     *  @param Request $request
     *  @return string
     */

    public function getProfileImage(\Illuminate\Http\Request $request)
    {

        // Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $validateImage = $this->_profileIdValidation($request);

        $partyId = $request->input('partyId');

        if ( $validateImage !== TRUE )
        {
            $output_arr['display_msg'] = $validateImage;
            return $output_arr;
        }
        $listprofileImages = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                        ->where('PartyID', $partyId)->limit(3)->orderBy('PrimaryImage', 'asc')->get();
        if ( $listprofileImages->count() < 1 )
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['res_data']            = '';
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return $output_arr;
        }

        $profileUrl = \App\Libraries\Helpers::profileUrl($partyId);


        foreach ( $listprofileImages as $mainProfileImages )
        {
            $arrayPosition[] = $mainProfileImages->ImagePosition;
            $profileImage[]  = array( 'ImagePosition' => $mainProfileImages->ImagePosition, 'ProfileImagePath' => $profileUrl . $mainProfileImages->ProfileImage, 'ImagePreference' => $mainProfileImages->PrimaryImage );
        }
        if ( ! in_array("first", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'first', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("second", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'second', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("third", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'third', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }


        $output_arr['is_error']            = FALSE;
        $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
        $output_arr['res_data']            = $profileImage;
        return $output_arr;
    }

    /*
     *  uploadProfile - Update User Profile Image
     *  @param Request $request
     *  @return string
     */

    public function updateProfileImage(\Illuminate\Http\Request $request)
    {

        // Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $validateImage = $this->_profileImageValidation($request);
        $partyId       = $request->input('partyId');

        if ( $validateImage !== TRUE )
        {
            $output_arr['display_msg'] = $validateImage;
            return $output_arr;
        }
        if ( $request->input('profileImage') !== NULL )
        {
            $profileImage = $this->_uploadProfile($request);
            if ( $profileImage != NULL )
            {
                $profileUrl        = \App\Libraries\Helpers::profileUrl($partyId);
                $listprofileImages = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                                ->where('PartyID', $partyId)->limit(3)->orderBy('PrimaryImage', 'asc')->get();

                foreach ( $listprofileImages as $mainProfileImages )
                {
                    $arrayPosition[] = $mainProfileImages->ImagePosition;
                    $profileImages[] = array( 'ImagePosition' => $mainProfileImages->ImagePosition, 'ProfileImagePath' => $profileUrl . $mainProfileImages->ProfileImage, 'ImagePreference' => $mainProfileImages->PrimaryImage );
                }

                if ( ! in_array("first", $arrayPosition) )
                {

                    $profileImages[] = array( 'ImagePosition' => 'first', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
                }
                if ( ! in_array("second", $arrayPosition) )
                {

                    $profileImages[] = array( 'ImagePosition' => 'second', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
                }
                if ( ! in_array("third", $arrayPosition) )
                {

                    $profileImages[] = array( 'ImagePosition' => 'third', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
                }
            }
        }
        $usInAttr   = trans('messages.attributes.profile');
        return $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.imgupdatedSuccessfully', [ 'name' => $usInAttr ]) ), 'status_code' => 200, 'res_data' => $profileImages );
    }

    /*
     *  uploadProfile - Update User Profile Image
     *  @param Request $request
     *  @return string
     */

    private function _uploadProfile($request)
    {
        $attachment_url = NULL;
        $partyId        = $request->input('partyId');
        $imageNo        = $request->input('imageNumber');

        $userFolder = sha1($partyId);
        \Intervention\Image\ImageManagerStatic::configure([ 'driver' => 'gd' ]);
        $imageType  = $request->input('profileImageType');

        // upload image
        $image = $request->input('profileImage');
        if ( ! empty($image) )
        {
            $fileName = md5(time()) . '.' . $imageType;
            $path     = 'Party/' . $userFolder . '/profile/';
            $NewPath  = \App\Libraries\Helpers::publicPath($path);
            $NPath    = $NewPath . '/' . $fileName;

            if ( ! is_dir($NewPath) )
            {
                mkdir($NewPath, 0777, TRUE);
            }

            $is_saved = \Intervention\Image\ImageManagerStatic::make(base64_decode($image))->save($NPath);
            if ( $is_saved )
            {
                // $attachment_url['oldProfileImage'] = NULL;
                $userConfigArray['ProfileImagePath'] = $fileName;


                $userConfigArray['UpdatedDateTime'] = date('Y-m-d H:i:s');
                $userConfigArray['UpdatedBy']       = 'User';

                // check imiage exists or not 
                $CheckPartyImagesPos = \App\Models\Partyprofileimages::select('ProfileImage')
                                ->where('PartyID', $partyId)->where('ImagePosition', $request->input('imageNumber'))->first();

                $CheckPartyImagesHasprimary = \App\Models\Partyprofileimages::select('ProfileImage')
                                ->where('PartyID', $partyId)->where('PrimaryImage', 'Y')->first();

                $makePrimary = (empty($CheckPartyImagesHasprimary)) ? 'Y' : 'N';

                if ( empty($CheckPartyImagesPos) )
                {

                    $insetPartyImages                  = new \App\Models\Partyprofileimages();
                    $insetPartyImages->PartyID         = $partyId;
                    $insetPartyImages->ProfileImage    = $fileName;
                    $insetPartyImages->PrimaryImage    = $makePrimary;
                    $insetPartyImages->ImagePosition   = $request->input('imageNumber');
                    $insetPartyImages->CreatedBy       = 'User';
                    $insetPartyImages->UpdatedBy       = 'User';
                    $insetPartyImages->CreatedDateTime = date('Y-m-d H:i:s');
                    $insetPartyImages->UpdatedDateTime = date('Y-m-d H:i:s');
                    $insetPartyImages->save();
                }
                else if ( ! $CheckPartyImagesPos )
                {
                    // update profile Image
                    $insetPartyImages                  = new \App\Models\Partyprofileimages();
                    $insetPartyImages->PartyID         = $partyId;
                    $insetPartyImages->ProfileImage    = $fileName;
                    $insetPartyImages->ImagePosition   = $request->input('imageNumber');
                    $insetPartyImages->CreatedBy       = 'User';
                    $insetPartyImages->UpdatedBy       = 'User';
                    $insetPartyImages->CreatedDateTime = date('Y-m-d H:i:s');
                    $insetPartyImages->UpdatedDateTime = date('Y-m-d H:i:s');
                    $insetPartyImages->save();
                }
                else
                {
                    $userConfigArray = array(
                        'ProfileImage' => $fileName,
                    );
                    \App\Models\Partyprofileimages::where('PartyID', $partyId)->where('ImagePosition', $request->input('imageNumber'))->update($userConfigArray);
                }

                $userUpdateTracker[] = array(
                    'UserWalletID'    => $partyId,
                    'UpdateFieldName' => 'ProfileImage',
                    'OldValue'        => (empty($CheckPartyImagesPos['ProfileImage'])) ? '' : $CheckPartyImagesPos['profileImage'],
                    'NewValue'        => $fileName,
                    'SystemIpAddress' => $request->header('ip-address'),
                    'CreatedDateTime' => date('Y-m-d H:i:s'),
                    'CreatedBy'       => 'User',
                );
                $uptrack             = \App\Models\Userupdatetracker::insert($userUpdateTracker);

                //\App\Models\Partyprofileimages::where('UserWalletID', $partyId)->update($userConfigArray);

                $attachment_url['profileImage'] = $fileName;
                //   $attachment_url['ImagePreference'] = $configId->ImagePreference;
            }
        }
        return $attachment_url;
    }

    /**
     * preferenceProfileImage - make primary image
     * @param Request $request
     * return JSON
     */
    public function preferenceProfileImage(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr              = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $validatePreferenceImage = $this->_preferenceProfileValidation($request);
        if ( $validatePreferenceImage !== TRUE )
        {
            $output_arr['display_msg'] = $validatePreferenceImage;
            return $output_arr;
        }
        $partyId     = $request->input('partyId');
        $imageNumber = $request->input('imageNumber');

        $userData = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                        ->where('PartyID', $partyId)->limit(3)->orderBy('PrimaryImage', 'asc')->get();

        if ( $userData === NULL )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return $output_arr;
        }
        $userArray['UpdatedDateTime'] = date('Y-m-d H:i:s');
        $userArray['UpdatedBy']       = 'User';
        \App\Models\Partyprofileimages::where('PartyID', $partyId)->update(array( 'PrimaryImage' => 'N' ));
        \App\Models\Partyprofileimages::where('PartyID', $partyId)->where('ImagePosition', $imageNumber)->update(array( 'PrimaryImage' => 'Y' ));


        $userData   = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                        ->where('PartyID', $partyId)->limit(3)->orderBy('PrimaryImage', 'asc')->get();
        $profileUrl = \App\Libraries\Helpers::profileUrl($partyId);
        foreach ( $userData as $mainProfileImages )
        {
            $arrayPosition[] = $mainProfileImages->ImagePosition;
            $profileImage[]  = array( 'ImagePosition' => $mainProfileImages->ImagePosition, 'ProfileImagePath' => $profileUrl . $mainProfileImages->ProfileImage, 'ImagePreference' => $mainProfileImages->PrimaryImage );
        }
        if ( ! in_array("first", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'first', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("second", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'second', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("third", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'third', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        $usInAttr   = trans('messages.attributes.profile');
        return $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.updatedSuccessfully', [ 'name' => $usInAttr ]) ), 'status_code' => 200, 'res_data' => $profileImage );
    }

    /**
     * preferenceProfileImage - make primary image
     * @param Request $request
     * return JSON
     */
    public function deleteProfileImage(\Illuminate\Http\Request $request)
    {

        // Default Response
        $output_arr              = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $validatePreferenceImage = $this->_preferenceProfileValidation($request);
        if ( $validatePreferenceImage !== TRUE )
        {
            $output_arr['display_msg'] = $validatePreferenceImage;
            return $output_arr;
        }
        $partyId     = $request->input('partyId');
        $imageNumber = $request->input('imageNumber');
        $userData    = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                        ->where('PartyID', $partyId)->where('ImagePosition', $imageNumber)->first();
        if ( empty($userData) )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return $output_arr;
        }
        $userArray['UpdatedDateTime'] = date('Y-m-d H:i:s');
        $userArray['UpdatedBy']       = 'User';



        $mainUpdate = \App\Models\Partyprofileimages::where('PartyID', $partyId)->where('ImagePosition', $imageNumber)->delete();

        //  $userData1 = \App\Models\Userwallet::select(array('UserWalletID', 'FirstName', 'ProfileImagePath', 'ProfileImagePath1', 'ProfileImagePath2', 'ImagePreference'))->where('UserWalletID', $partyId)->first(); 

        $userDataImages = \App\Models\Partyprofileimages::select('ImagePosition', 'PartyProfileImageId', 'ProfileImage', 'PrimaryImage')
                        ->where('PartyID', $partyId)->limit(3)->orderBy('PrimaryImage', 'asc')->get();
        $profileUrl     = \App\Libraries\Helpers::profileUrl($partyId);
        $arrayPosition  = array();
        foreach ( $userDataImages as $mainProfileImages )
        {
            $arrayPosition[] = $mainProfileImages->ImagePosition;
            $profileImage[]  = array( 'ImagePosition' => $mainProfileImages->ImagePosition, 'ProfileImagePath' => $profileUrl . $mainProfileImages->ProfileImage, 'ImagePreference' => $mainProfileImages->PrimaryImage );
        }
        if ( ! in_array("first", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'first', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("second", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'second', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }
        if ( ! in_array("third", $arrayPosition) )
        {

            $profileImage[] = array( 'ImagePosition' => 'third', 'ProfileImagePath' => '', 'ImagePreference' => 'N' );
        }


        $usInAttr   = trans('messages.attributes.profile');
        return $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.imgdeletedSuccessfully', [ 'name' => $usInAttr ]) ), 'status_code' => 200, 'res_data' => $profileImage );
    }

    /**
     * _preferenceProfileValidation - for validation
     * @param Request $request
     * @return JSON
     */
    private function _preferenceProfileValidation($request)
    {
        // update profile image 
        $validations['imageNumber'] = 'required|in:first,second,third';

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    /*
     *  changeUniqueValue - change Mobile & EmailId with v-pin verify
     *  @param Request $request
     *  @return JSON
     */

    public function changeUniqueValue(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_changeMobileEmailValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $newEmail = $request->input('email');
        $oldEmail = $request->input('oldEmail');
        $key      = trans('messages.attributes.email');

        //check new Mobile already in use or not
        $checkEmail = \App\Models\Userwallet::select('UserWalletID', 'EmailID', 'MobileNumber', 'FirstName', 'LastName')->where('EmailID', $newEmail)->first();
        if ( $checkEmail )
        {
            if ( $checkEmail->UserWalletID == $partyId )
            {
                $output_arr['display_msg'] = array( 'email' => $key . ' ' . trans('messages.cannotBeSame') );
                return $output_arr;
            }

            $output_arr['display_msg'] = array( 'email' => trans('messages.emailExist') );
            return $output_arr;
        }

        $userInfo          = \App\Models\Userwallet::find($partyId);
        $userInfo->EmailID = $newEmail;
        $userInfo->save();

        // update usertracker
        $userUpdateTracker = array(
            'UserWalletID'    => $partyId,
            'UpdateFieldName' => 'EmailID',
            'OldValue'        => $oldEmail,
            'NewValue'        => $request->input('email'),
            'SystemIpAddress' => $request->header('ip-address'),
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'User',
        );
        \App\Models\Userupdatetracker::insert($userUpdateTracker);

        // Communication Start
        $templateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW014',
            'senderEmail'    => $newEmail,
            'senderMobile'   => $userInfo->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullName'   => ($userInfo->FirstName) ? $userInfo->FirstName : '',
                'otpCode'    => '',
                'email'      => '',
                'deviceName' => '',
                'supportURL' => "violamoney.com"
            )
        );

        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        // Communication End

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.changedSuccessfully', [ 'name' => $key ]) ), 'status_code' => 200, 'res_data' => array() );
        return $output_arr;
    }

    /*
     *  updateMobileNumber - validate Mobile Number & send OTP
     *  @param Reqquest $request
     *  @return JSON
     */
    /*
    public function updateMobileNumber(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_validateUpdateMobileNumber($request);
        if ( $validateUser !== TRUE )
        {
            $output_arr['display_msg'] = $validateUser;
            return json_encode($output_arr);
        }

        // request Variables
        $partyId         = $request->input('partyId');
        $mobileNumber    = $request->input('mobileNumber');
        $password        = $request->input('password');
        $oldMobileNumber = $request->input('oldMobileNumber');
        $validateService = $request->input('validateService');
        $key             = trans('messages.attributes.mobile');

        // user exsistens test
        $partyDetails = \App\Models\Userwallet::find($partyId);
        if ( ! $partyDetails )
        {
            $output_arr['display_msg']['partyId'] = 'User doesn`t exist.';
            return $output_arr;
        }

        if ( $validateService == 'P' )
        {
            if ( \Illuminate\Support\Facades\Hash::check($password, $partyDetails->Password) === FALSE )
            {
                $output_arr['display_msg']['password'] = 'Please enter valid password';
                return $output_arr;
            }

            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array( 'info' => 'Success' );
            return $output_arr;
        }

        // check new Mobile & exsisting number same
        $checkMobile = \App\Models\Userwallet::where('MobileNumber', $mobileNumber)->first();
        if ( $checkMobile )
        {
            if ( $checkMobile->UserWalletID == $partyId )
            {
                $output_arr['display_msg'] = array( 'info' => $key . ' ' . trans('messages.cannotBeSame') );
                return $output_arr;
            }

            $output_arr['display_msg'] = array( 'info' => trans('messages.mobileExist') );
            return $output_arr;
        }

        if ( $validateService == 'CO' )
        {
            $updateMobile = self::_updateMobileNumber($request, $oldMobileNumber);
            return $updateMobile;
        }
        $emailId     = $partyDetails->EmailID;
        $username    = ''; //"username" 
        // send Otp for varification 
        $params      = array(
            'UserWalletID' => $partyId,
            'mobileNumber' => $mobileNumber,
            'emailId'      => $emailId,
            'userName'     => $username,
            'typeCode'     => 'VLD',
            'templateKey'   => 'VW013',
            'oldMobileNo'   => $oldMobileNumber,
            'newMobileNo'   => $mobileNumber,
            'attempts'     => 0
        );
        $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
        if ( $otp_success === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.otpSentFaild') );
            return $output_arr;
        }

        $res = $otp_success;
        unset($res['otpMssage']);

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => $otp_success['otpMssage'] );
        $output_arr['res_data']    = $res;
        return $output_arr;
    }
    */

    private static function _updateMobileNumber($request, $oldMobile)
    {
        $output_arr  = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $partyId     = $request->input('partyId');
        $otpNumber   = $request->input('otpNumber');
        $otpId       = $request->input('otpId');
        $otpType     = $request->input('otpType');
        // validate OTP
        $otpPramas   = array( 'partyId' => $partyId, 'otpNumber' => $otpNumber, 'otpId' => $otpId, 'otpType' => $otpType );
        $validateOtp = \App\Libraries\Helpers::validateOtp($otpPramas);

        if ( $validateOtp === FALSE )
        {
            $output_arr['display_msg']['otpNumber'] = trans('messages.validOtp');
            return $output_arr;
        }
        $partyDetails               = \App\Models\Userwallet::find($partyId);
        $partyDetails->MobileNumber = $request->input('mobileNumber');
        $partyDetails->save();

        // update usertracker
        $userUpdateTracker = array(
            'UserWalletID'    => $partyId,
            'UpdateFieldName' => 'MobileNumber',
            'OldValue'        => $oldMobile,
            'NewValue'        => $request->input('mobileNumber'),
            'SystemIpAddress' => $request->header('ip-address'),
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'User',
        );
        \App\Models\Userupdatetracker::insert($userUpdateTracker);

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => 'Success' );
        return $output_arr;
    }
    /*
    private function _validateUpdateMobileNumber($request)
    {
        $validations = array(
            'partyId'         => 'required|integer|digits_between:1,10',
            'validateService' => 'required|in:P,C,CO'
        );

        if ( $request->input('validateService') == 'P' )
        {
            $validations['password'] = 'required|min:8|max:21|password';
        }

        if ( $request->input('validateService') == 'C' )
        {
            $validations['mobileNumber']    = 'required|numeric|digits:10';
            $validations['oldMobileNumber'] = 'required|numeric|digits:10';
        }

        if ( $request->input('validateService') == 'CO' )
        {
            $validations['mobileNumber']    = 'required|numeric|digits:10';
            $validations['oldMobileNumber'] = 'required|numeric|digits:10';
            $validations['otpNumber']       = 'required|numeric|digits:6';
            $validations['otpId']           = 'required|numeric';
            $validations['otpType']         = 'required|in:VLD';
        }
        // common input fiels
        $messages = array();

        return $this->_validate_method($request, $validations, $messages);
    }
     * 
     */

    /**
     * Resend Otp mobile change.
     *
     * @param Request $request
     * @return JSON
     */
    /*public function changeMobileResendOtp(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_array = array( 'is_error' => TRUE, 'display_msg' => '', 'status_code' => 200, 'res_data' => array() );

        $validateUser = $this->_UserVerify($request, '_updatemobileValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        // request Variables
        $key     = trans('messages.attributes.mobile');
        $partyId = $request->input('partyId');


        // check new Mobile & exsisting number same
        $partyDetails = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')->find($partyId);
        $getMobile    = $partyDetails->MobileNumber;
        if ( $getMobile == $request->input('mobileNumber') )
        {
            $output_array['display_msg'] = array( 'info' => $key . ' ' . trans('messages.cannotBeSame') );
            return $output_array;
        }

        $mobileNumber = $request->input('mobileNumber');
        $OTPType      = 'VLD';
        $OTPId        = $request->input('otpId');
        $partyId      = $request->input('partyId');

        $results = \App\Models\Partyotp::select('partyotp.OTP', 'partyotp.TransactionTypeCode')
                        ->where('partyotp.OTPRequestID', '=', $OTPId)->first();

        if ( empty($results) )
        {
            return $output_array['display_msg']['info'] = trans('messages.enterValidDetails');
        }
        $otp = json_decode($results->OTP);

        if ( $otp->attempts === 1 )
        {
            $output_array['display_msg'] = array( 'info' => trans('messages.callMe') );
        }
        else
        {
            $emailId  = $partyDetails->EmailID;
            $username = ''; //"username" 
            $attmpt   = $otp->attempts - 1;
            $params   = array(
                'OTPRequestID'    => $OTPId,
                'UserWalletID'    => $partyId,
                'mobileNumber'    => $mobileNumber,
                'typeCode'        => $OTPType,
                'attempts'        => $attmpt,
                'emailId'         => $emailId,
                'userName'        => $username,
                'CreatedDateTime' => date('Y-m-d H:i:s A')
            );

            $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
            if ( $otp_success === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.otpSentFaild') );
                return $output_arr;
            }
   
            $res = $otp_success;
            unset($res['otpMssage']);

            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => $otp_success['otpMssage'] ), 'status_code' => 200, 'res_data' => $res );
            return $output_arr;
        }


        return response()->json($output_array);
    }*/

    /*
     *  validateUpdateMobile - Update Mobile Number with OTP
     *  @param Reqquest $request
     *  @return JSON
     */
    /*
    public function validateUpdateMobile(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_mobileOtpValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $otp      = $request->input('otpNumber');
        $otpId    = $request->input('otpId');
        $otp_Type = $request->input('otpType');
        $key      = trans('messages.attributes.mobile');

        // check new Mobile & exsisting number same
        $results   = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                ->find($partyId);
        $getMobile = $results->MobileNumber;
        if ( $getMobile == $request->input('mobileNumber') )
        {
            $output_arr['display_msg'] = array( 'info' => $key . ' ' . trans('messages.cannotBeSame') );
            return $output_arr;
        }

        // validate OTP
        $otpPramas    = array( 'partyId' => $partyId, 'otpNumber' => $otp, 'otpId' => $otpId, 'otpType' => $otp_Type );
        $validateVpin = \App\Libraries\Helpers::validateOtp($otpPramas);

        if ( $validateVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.validOtp') );
            return $output_arr;
        }

        //check new Mobile already in use or not
        $checkMobile = \App\Models\Userwallet::where('MobileNumber', $request->input('mobileNumber'))->first();
        if ( $checkMobile )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.mobileExist') );
            return $output_arr;
        }

        $partyVPin               = \App\Models\Userwallet::find($partyId);
        $partyVPin->MobileNumber = $request->input('mobileNumber');
        $partyVPin->save();

        // update usertracker
        $userUpdateTracker = array(
            'UserWalletID'    => $partyId,
            'UpdateFieldName' => 'MobileNumber',
            'OldValue'        => $getMobile,
            'NewValue'        => $request->input('mobileNumber'),
            'SystemIpAddress' => $request->header('ip-address'),
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'User',
        );
        \App\Models\Userupdatetracker::insert($userUpdateTracker);

        // Communication Start
        $templateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW013',
            'senderEmail'    => $results->EmailID,
            'senderMobile'   => $results->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullName'   => ($results->FirstName) ? $results->FirstName : '',
                'otpCode'    => '',
                'supportURL' => "violamoney.com",
            )
        );
        // Communication End
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);


        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.updatedSuccessfully', [ 'name' => $key ]) ), 'status_code' => 200, 'res_data' => array() );
        return $output_arr;
    }*/

    /*
     *  changeSecureKeys - change password & vpin with old details
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function changeSecureKeys(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_secureKeysValidation');

        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId = $request->input('partyId');

        $new     = TRUE;
        $results = \App\Models\Userwallet::select('Password', 'TPin', 'FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                        ->where('UserWalletID', $partyId)->first();

        if ( ! $results )
        {
            $output_arr['display_msg'] = array( 'info' => 'User doesn`t exist' );
            return response()->json($output_arr);
        }
        if ( $request->input('password') )
        {
            $newPassword  = $secureKey    = \Illuminate\Support\Facades\Hash::make($request->input('newPassword'));
            $oldEncryPass = $results->Password;
            $key          = trans('messages.attributes.password');
            if ( \Illuminate\Support\Facades\Hash::check($request->input('password'), $oldEncryPass) === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.attributes.wrong') . ' ' . $key );
                return $output_arr;
            }

            // check last 3 passwords for uniqueness
            $keyType = 'PASSWORD';
            $new     = $this->_checkSecureKey($partyId, $request->input('newPassword'), $keyType);
            if ( $new === FALSE )
            {
                $attrPassword              = trans('validation.attributes.password');
                $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $attrPassword ]) );
                return $output_arr;
            }

            $partyVPin = \App\Models\Userwallet::where('UserWalletID', $partyId)
                    ->update([ 'Password' => $newPassword ]);
        }

        if ( $request->input('vPin') )
        {
            $newVpin      = $secureKey    = \Illuminate\Support\Facades\Hash::make($request->input('newVPin'));
            $oldEncryVpin = $results->TPin;
            $key          = trans('messages.attributes.vpin');
            if ( \Illuminate\Support\Facades\Hash::check($request->input('vPin'), $oldEncryVpin) === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.attributes.wrong') . ' ' . $key );
                return $output_arr;
            }

            $keyType = 'PIN';
            $new     = $this->_checkSecureKey($partyId, $secureKey, $keyType);

            if ( $new === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $key ]) );
                return $output_arr;
            }

            $partyVPin = \App\Models\Userwallet::where('UserWalletID', $partyId)
                    ->update([ 'TPin' => $newVpin ]);
        }

        // update password history
        $this->_updatePasswordHistory($partyId, $secureKey, $keyType);

        // Communication Start
        $templateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW011',
            'senderEmail'    => $results->EmailID,
            'senderMobile'   => $results->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullName'   => ($results->FirstName) ? $results->FirstName : '',
                'otpCode'    => '',
                'supportURL' => "violamoney.com",
            )
        );
        if ( $keyType == 'PIN' )
        {
            $templateData['templateKey'] = 'VW012';
        }
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData); 
        // Communication End

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.changedSuccessfully', [ 'name' => $key ]) );

        return $output_arr;
    }

    /*
     *  forgotVpin / set vpin - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */
    /*
    public function forgotVpin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_forgetPinValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $vPin     = $request->input('vPin');
        $userInfo = \App\Models\Userwallet::select('MobileNumber', 'TPin')->find($partyId);

        $getMobile = $userInfo->MobileNumber;
        // check last 3 passwords for uniqueness
        $secureKey = sha1($vPin);
        $keyType   = 'PIN';
        $key       = trans('messages.attributes.vpin');
        $new       = $this->_checkSecureKey($partyId, $secureKey, $keyType);

        if ( $new === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $key ]) );
            return $output_arr;
        }

        // send Otp for varification 
        $params      = array(
            'UserWalletID' => $partyId,
            'mobileNumber' => $getMobile,
            'typeCode'     => $keyType,
            'attempts'     => 0
        );
        $otp_success = \App\Libraries\Helpers::OtpSend($params);
        if ( $otp_success === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.otpSentFaild') );
            return $output_arr;
        }
        $sendMobileData = json_encode(array(
            //'fullname' => $results->FirstName.' '.$results->LastName,
            'site_name' => $this->applicationName,
            'otp_code'  => $otp_success['otpNumber'] ));


        $communicationParams = array(
            'party_id'          => $partyId,
            'linked_id'         => '0',
            'notification_for'  => 'otp_verify',
            'notification_type' => 'sms',
            'mobile_no'         => $getMobile,
            'mobile_data'       => $sendMobileData,
            //'email_id' =>   $results->EmailID, 
            //'email_data' => $sendEmailData,
            'application_name'  => $this->applicationName
        );

        //common function to send notification_type msgs.
        //$response = \App\Libraries\Helpers::sendMsg($communicationParams);

        $res = $otp_success;
        unset($res['otpMssage']);

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => $otp_success['otpMssage'] ), 'status_code' => 200, 'res_data' => $res );
        return $output_arr;
    }*/

    /*
     *  forgotVpin / set vpin - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */
    /*
    public function setPin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_forgetOtpPinValidate');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $otp      = $request->input('otpNumber');
        $otpId    = $request->input('otpId');
        $otp_Type = $request->input('otpType');
        $vPin     = $request->input('vPin');

        // validate OTP
        $otpPramas    = array( 'partyId' => $partyId, 'otpNumber' => $otp, 'otpId' => $otpId, 'otpType' => $otp_Type );
        $validateVpin = \App\Libraries\Helpers::validateOtp($otpPramas);

        if ( $validateVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.validOtp') );
            return $output_arr;
        }

        $secureKey = sha1($vPin);
        $keyType   = 'PIN';
        $key       = trans('messages.attributes.vpin');
        $new       = $this->_checkSecureKey($partyId, $secureKey, $keyType);

        if ( $new === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $key ]) );
            return $output_arr;
        }

        $walletDetails = \App\Models\Userwallet::find($partyId);

        if ( ! $walletDetails )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }
        $walletDetails->TPin = sha1($request->input('vPin'));
        $walletDetails->save();

        // update password history
        $this->_updatePasswordHistory($partyId, $secureKey, $keyType);

        // Send Notification
        $sendMobileData = json_encode(array());

        $sendEmailData       = json_encode(array(
            'fullname'  => $walletDetails->FirstName . ' ' . $walletDetails->LastName,
            'img_url'   => $this->baseUrl,
            'front_url' => $this->baseUrl,
            'date'      => date('Y-m-d'),
            'time'      => date('H:i:s'),
        ));
        $communicationParams = array(
            'party_id'          => $walletDetails->UserWalletID,
            'linked_id'         => '0',
            'notification_for'  => 'login_chnge_pin',
            'notification_type' => 'email,sms',
            'mobile_no'         => $walletDetails->MobileNumber,
            'mobile_data'       => $sendMobileData,
            'email_id'          => $walletDetails->EmailID,
            'email_data'        => $sendEmailData,
        );
        // Push notification parameters.          
        $device_type         = $request->header('device-type');
        if ( $device_type != 'W' )
        {
            $pushData                                 = array();
            $communicationParams['notification_type'] = $communicationParams['notification_type'] . ",push";
            $communicationParams['device_type']       = $device_type;
            $communicationParams['device_token']      = $request->header('device-token');
            $communicationParams['push_data']         = json_encode($pushData);
        }
        //common function to send notification_type msgs.
        //$response = \App\Libraries\Helpers::sendMsg($communicationParams);


        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.setSuccessfully', [ 'name' => $key ]) ), 'status_code' => 200, 'res_data' => array() );

        return $output_arr;
    }
    */
    /*
     *  nomineDetails - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function nomineDetails(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request);
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId = $request->input('partyId');

        // nomine relations
        $relationShips = \App\Models\Relationshiptype::select('RelationshipTypeCode', 'RelationshipDescription')->get();

        // user nomine details
        $partyNomine = \App\Models\Usernomineedetail::join('relationshiptype', 'relationshiptype.RelationshipTypeCode', '=', 'usernomineedetails.NomineeRelationTypeCode')
                ->join('relationshiptype AS rs', 'rs.RelationshipTypeCode', '=', 'usernomineedetails.MinorGuardianRelationTypeCode')
                ->where('usernomineedetails.UserWalletID', '=', $partyId)
                ->select('usernomineedetails.UserWalletID', 'usernomineedetails.NomineeRelationTypeCode', 'relationshiptype.RelationshipDescription as NomineeRelation', 'usernomineedetails.NomineeName', 'usernomineedetails.NomineeDOB', 'usernomineedetails.NomineeAddressID', 'usernomineedetails.NomineeContactNumber', 'usernomineedetails.IsNomineeMinor', 'usernomineedetails.NameOfGuardian', 'usernomineedetails.MinorGuardianRelationTypeCode', 'rs.RelationshipDescription AS MinorRelationShipName', 'usernomineedetails.GaurdianDOB', 'usernomineedetails.GuardianContactNumber')
                ->first();

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array( 'relationShips' => $relationShips, 'nomineeInfo' => $partyNomine ) );

        return $output_arr;
    }

    /*
     *  validateVpin - verify Party V-pin
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function validateVpin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_userVpinValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId = $request->input('partyId');
        $vPin    = $request->input('vPin');
        $key     = trans('messages.attributes.vpin');

        // validate Vpin
        $param    = array(
            'partyId' => $partyId,
            'vPin'    => $vPin
        );
        $partyPin = \App\Libraries\Helpers::validateVPin($param);

        if ( ! $partyPin )
        {
            $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
            return $output_arr;
        }

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.validateSuccessfully', [ 'name' => $key ]) ), 'status_code' => 200, 'res_data' => array() );

        return $output_arr;
    }

    /*
     *  balanceViewPrefarence - set Balance View Prefarence
     *  @param Array $request
     *  @return JSON
     */

    public function balanceViewPrefarence(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_balanceViewValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId     = $request->input('partyId');
        $balanceView = $request->input('balanceView');
        // User Configrations
        $userConfig  = \App\Models\Userconfiguration::select('ConfigurationID')
                ->where('UserWalletID', $partyId)
                ->where('ConfigType', 'balanceView')
                ->first();

        $dataArray = array(
            'UserWalletID'    => $partyId,
            'ConfigType'      => 'balanceView',
            'Details'         => $balanceView,
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'User'
        );

        if ( ! $userConfig )
        {
            // create partyAddress
            $partyConfig = \App\Models\Userconfiguration::create($dataArray);
        }
        else
        {
            // update partyAddress
            $partyConfig = \App\Models\Userconfiguration::where('ConfigurationID', $userConfig->ConfigurationID)->update($dataArray);
        }

        if ( ! $partyConfig )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrong') );
            return $output_arr;
        }
        $balPrefarance = trans('messages.attributes.balPrefarance');
        $output_arr    = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.updatedSuccessfully', [ 'name' => $balPrefarance ]) ), 'status_code' => 200, 'res_data' => array( 'balanceView' => $balanceView ) );

        return $output_arr;
    }

    /*
     *  balanceViewPrefarence - set Balance View Prefarence
     *  @param Array $request
     *  @return JSON
     */

    public function fastTrackSetting(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_fastTrackValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId   = $request->input('partyId');
        $fastTrack = $request->input('fastTrack');
        $fastArray = explode(',', $fastTrack);

        // check fastrack 
        $defaultOder = config('vwallet.FASTTRACK_MENU');
        if ( count($defaultOder) != count(array_unique($fastArray)) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.fastrackOrderBroken') );
            return $output_arr;
        }

        // check order is numenrics or not 
        $all_numeric = true;
        foreach ( $fastArray as $key )
        {
            if ( is_float($key) )
            {
                $all_numeric = false;
                break;
            }
        }
        if ( ! $all_numeric )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.fastrackUnknown') );
            return $output_arr;
        }

        // User Configrations
        $userConfig = \App\Models\Userconfiguration::select('ConfigurationID')
                ->where('UserWalletID', $partyId)
                ->where('ConfigType', 'fastTrack')
                ->first();

        $dataArray = array(
            'UserWalletID'    => $partyId,
            'ConfigType'      => 'fastTrack',
            'Details'         => $fastTrack,
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'User'
        );

        if ( ! $userConfig )
        {
            // create partyAddress
            $partyConfig = \App\Models\Userconfiguration::create($dataArray);
        }
        else
        {
            // update partyAddress
            $partyConfig = \App\Models\Userconfiguration::where('ConfigurationID', $userConfig->ConfigurationID)->update($dataArray);
        }

        if ( ! $partyConfig )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrong') );
            return $output_arr;
        }
        $fastrackPrefence = trans('messages.attributes.fastrackPrefence');
        $output_arr       = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.updatedSuccessfully', [ 'name' => $fastrackPrefence ]) ), 'status_code' => 200, 'res_data' => array( 'fastTrack' => $fastTrack ) );

        return $output_arr;
    }

    /*
     *  checkSecureKey - check password & pin is already used or not
     *  @param int $partyId
     *  @param string $secureKey
     *  @param string $keyType 
     *  @return JSON
     */

    private function _checkSecureKey($partyId, $secureKey, $keyType)
    {
        // check last 3 passwords for uniqueness
        $lastPass = \App\Models\Partypasswordhistory::select('SecretKey')
                        ->where('PartyID', $partyId)
                        ->where('SecretKeyType', $keyType)
                        ->orderBy('PartyPasswordHistoryID', 'desc')
                        ->take(3)->get();
        $new      = TRUE;
        foreach ( $lastPass as $value )
        {
            if ( \Illuminate\Support\Facades\Hash::check($secureKey, $value->SecretKey) === TRUE )
            {
                $new = FALSE;
                break;
            }
            else
            {
                $new = TRUE;
            }
        }
        return $new;
    }

    /*
     *  _updatePasswordHistory - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _updatePasswordHistory($partyId, $secureKey, $keyType)
    {
        // update all Active passwords to Non- Active
        $pssHis = \App\Models\Partypasswordhistory::where('PartyID', $partyId)
                ->where('SecretKeyType', $keyType)
                ->where('Active', 'Y')
                ->update([ 'Active' => 'N' ]);

        $date           = date('Y-m-d H:i:s');
        $passwordExpiry = config('vwallet.PASSWORD_EXPIRY');
        $expireDate     = date('Y-m-d H:i:s', strtotime(date('Y-m-d') . ' + ' . $passwordExpiry . ' days'));

        // update Password history
        $updatePasswordHistory = array(
            'PartyID'         => $partyId,
            'SecretKey'       => $secureKey,
            'SecretKeyType'   => $keyType,
            'EffectiveDate'   => $date,
            'ExpiryDate'      => $expireDate,
            'Active'          => 'Y',
            'CreatedDateTime' => $date,
            'CreatedBy'       => 'User'
        );
        \App\Models\Partypasswordhistory::insert($updatePasswordHistory);
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _userIdValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', '=', $partyId)->where('ProfileStatus', '=', 'A');
        if ( ! $userExsist )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /*
     *  _validateInput - Input validation Method
     *  @param Request $request
     * @param validation for $validationFor
     * As this is a common method for all validations $validationFor is key to saparate things
     *  @return Boolean
     */

    private function _userVpinValidation($request)
    {
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $validations['vPin']    = 'required|numeric|digits:4';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _userValidation($request)
    {
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _balanceViewValidation($request)
    {
        $validations['partyId']     = 'required|integer|digits_between:1,10';
        $validations['balanceView'] = 'required|in:Y,N';
        $messages                   = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _fastTrackValidation($request)
    {
        $validations['partyId']   = 'required|integer|digits_between:1,10';
        $validations['fastTrack'] = 'required';
        $messages                 = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _profileInfoValidation($request)
    {
        // city, state values are text as of kk said.            
        $validations = array(
            'FullName'      => 'alpha_space|min:3|max:75',
            'Gender'        => 'in:M,F,T,N',
            'DateofBirth'   => 'date_format:Y-m-d',
            'AddressLine1'  => 'min:3|max:45',
            'AddressLine2'  => 'min:3|max:45',
            'City'          => 'min:3|max:45',
            'State'         => 'min:3|max:45',
            'Country'       => 'min:3|max:45',
            'PostalZipCode' => 'alpha_num|digits_between:5,6'
        );

        if ( $request->header('device-type') == 'W' )
        {
            $validations['EmailID'] = 'required|email|max:100';
        }

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _profileImageValidation($request)
    {
        // update profile image 
        $validations['profileImage']     = 'required';
        $validations['imageNumber']      = 'required|in:first,second,third';
        $validations['profileImageType'] = 'required|in:gif,jpeg,pjpeg,png,x-png';

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _profileIdValidation($request)
    {
        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = array();

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _forgetPinValidation($request)
    {
        $validations['vPin']    = 'required|numeric|digits:4';
        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _forgetOtpPinValidate($request)
    {
        $validations = array(
            'otpNumber' => 'required|integer|digits:6',
            'otpType'   => 'required|in:PIN',
            'otpId'     => 'required|integer',
            'vPin'      => 'required|numeric|digits:4',
        );


        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _secureKeysValidation($request)
    {
        if ( $request->exists('vPin') )
        {
            $validations['vPin']    = 'required|numeric|digits:4';
            $validations['newVPin'] = 'required|numeric|digits:4|different:vPin';
        }
        else
        {
            $validations['password']    = 'required|min:8|max:20|password';
            $validations['newPassword'] = 'required|min:8|max:20|password|different:password';
        }

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _mobileOtpValidation($request)
    {
        $validations = array(
            'otpNumber'    => 'required|integer|digits:6',
            'otpType'      => 'required|in:VLD',
            'otpId'        => 'required|integer',
            'mobileNumber' => 'required|numeric|digits:10'
        );
        $messages    = array();

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _updatemobileValidation($request)
    {
        $validations = array(
            'mobileNumber' => 'required|numeric|digits:10',
        );

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _changeMobileEmailValidation($request)
    {
        $validations['email']    = 'required|email';
        $validations['oldEmail'] = 'required|email';

        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _validate_method($request, $validations, $messages)
    {
        $validate_response = \Validator::make($request->all(), $validations, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /*
     *  userBalance - To get user balance
     *  @param Request $request
     *  @return JSON
     */

    public function userBalance(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $validateImage = $this->_profileIdValidation($request);
        if ( $validateImage !== TRUE )
        {
            $output_arr['display_msg'] = $validateImage;
            return $output_arr;
        }

        // user exsistens test
        $partyId   = $request->input('partyId');
        $userCheck = $this->_userIdValidate($partyId);
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }

        // Get user balances 
        $userBalances         = \App\Models\Walletaccount::select('WalletAccountNumber', 'WalletCurrencyCode', 'MaximumBalanceLimit', 'AvailableBalance', 'CurrentAmount', 'NonWithdrawAmount', 'HoldAmount')
                        ->where('UserWalletId', $partyId)->first()->toArray();
        $userTransactionStats = \App\Libraries\Statistics::getUserTransactionStats($partyId);
        $resultData           = array_merge($userBalances, $userTransactionStats);

        if ( ! $userBalances )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return $output_arr;
        }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        $output_arr['res_data']    = ( object ) $resultData;

        return $output_arr;
    }

    /**
     * YesBank User Balance Check
     * 
     * @param type $request
     */
    public function CheckYesBankWalletBalance(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatcheckBalanceYesbank($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $mobileNumber = $request->input('mobileNumber');


        $userData = \App\Models\Userwallet::select('UserWalletID')
                        ->where('MobileNumber', '=', $mobileNumber)->first();

        if ( ! $userData )
        {
            $data_send['display_msg'] = array( 'info' => 'User not Found.' );
            return response()->json($data_send);
        }

        $actionName   = 'WALLETBAL';
        $params       = array( 'p1' => $mobileNumber );
        $response     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params);
        $responseData = json_decode($response, TRUE);


        if ( $responseData['status_code'] == 'ERROR' )
        {
            $data_send['display_msg']['info'] = $responseData['message'];
            return response()->json($data_send);
        }
        //update Yesbank Balance and limit
        $arrayUpdate                      = array(
            'YesBankBalance'            => $responseData['balance'],
            'YesBankRemainBalanceLimit' => $responseData['remaining_load_limit'],
        );
        \App\Models\Walletaccount::where('UserWalletId', $userData['UserWalletID'])->update($arrayUpdate);
        $data_send['is_error']            = false;
        $data_send['display_msg']['info'] = $responseData['message'];
        $data_send['res_data']            = array(
            'balance'                   => $responseData['balance'],
            'statusCode'                => $responseData['status_code'],
            'mobileNumber'              => $responseData['phone_number'],
            'remainingLoadBalanceLimit' => $responseData['remaining_load_limit'],
            'tagBalance'                => $responseData['tag_balance'],
            'walletType'                => $responseData['wallet_type'],
        );
        return response()->json($data_send);
    }

    public function updateTranVpinLimit(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $validateImage = $this->_updateTranVpinLimitValidation($request);
        if ( $validateImage !== TRUE )
        {
            $output_arr['display_msg'] = $validateImage;
            return $output_arr;
        }

        // user exsistens test
        $partyId        = $request->input('partyId');
        $vpinTransLimit = $request->input('vpinTransLimit');
        // get Logged in User data if Exists.
        $partyInfo      = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        $updateVpinLimit = array(
            'Details' => $vpinTransLimit
        );

        \App\Models\Userconfiguration::where('ConfigType', 'vpinTransLimit')
                ->where('UserWalletID', $partyId)
                ->update($updateVpinLimit);

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        $output_arr['res_data']    = array( 'vpinTransLimit' => $vpinTransLimit );
        return response()->json($output_arr);
    }

    /**
     * Validate Yesbank User Balance Check
     * @param type $request
     * @return type
     */
    private function _validatcheckBalanceYesbank($request)
    {
        $validate          = array(
            'mobileNumber' => 'numeric|required|digits:10',
        );
        $messages          = array();
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    private function _updateTranVpinLimitValidation($request)
    {
        // common input fiels
        $validations = array(
            'partyId'        => 'required|integer|digits_between:1,10',
            'vpinTransLimit' => 'required|numeric||min:10'
        );
        $messages    = array();

        return $this->_validate_method($request, $validations, $messages);
    }

    public function UpdatePreferences(\Illuminate\Http\Request $request)
    {

        $chk_valid  = $this->updatecheck_valid($request, $type       = 'updatePreferences');
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ( ($chk_valid !== TRUE ) )
        {
            $output_arr['display_msg'] = $chk_valid;
            return $output_arr;
        }

        $custid       = $request->input('partyId');
        $notification = $request->input('notification');
        $trans_msg    = $request->input('trans_msg');
        $trans_email  = $request->input('trans_email');
        $offer_alert  = $request->input('offer_alert');
        $d_not_d      = $request->input('d_not_d');

        $preferenceData = \App\Models\Userconfiguration::select('UserWalletID', 'ConfigType', 'Details')->where([ [ 'UserWalletID', $custid ], [ 'ConfigType', 'preferences' ] ])->first();

        if ( empty($preferenceData->UserWalletID) )
        {
            $output_arr['is_error']    = TRUE;
            $output_arr['display_msg'] = array( 'info' => 'User Not Found.' );
            $output_arr['res_data']    = array();

            return $output_arr;
        }
        $prefrence  = json_decode($preferenceData);
        $prefer     = json_decode($prefrence->Details);
        $main_array = array();
        if ( ! empty($notification) )
        {
            $main_array['notification'] = $notification;
        }

        if ( ! empty($trans_msg) )
        {
            $main_array['trans_msg'] = $trans_msg;
        }
        if ( ! empty($trans_email) )
        {
            $main_array['trans_email'] = $trans_email;
        }
        if ( ! empty($offer_alert) )
        {
            $main_array['offer_alert'] = $offer_alert;
        }
        if ( ! empty($d_not_d) )
        {
            $main_array['d_not_d'] = $d_not_d;

            // code for DND  
            // If we pass DND as Y all keys will change to Y
            // If we Pass DND as N all keys will change according to our values i.e Y or N
            if ( $d_not_d == 'Y' )
            {
                $main_array['offer_alert']  = $d_not_d;
                $main_array['trans_email']  = $d_not_d;
                $main_array['trans_msg']    = $d_not_d;
                $main_array['notification'] = $d_not_d;
            }
        }


        $array                     = [
            'Details'         => json_encode($main_array),
            'UpdatedDateTime' => date('Y-m-d H:i:s'),
        ];
        \App\Models\Userconfiguration::where('UserWalletID', $custid)->where('ConfigType', 'preferences')->update($array);
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => 'Successfully Updated.' );
        $output_arr['res_data']    = array();

        return $output_arr;
    }

    // my preferences
    public function MyPreferences(\Illuminate\Http\Request $request)
    {
        $chk_valid  = $this->updatecheck_valid($request, $type       = 'viewPreferences');
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ( ($chk_valid !== TRUE ) )
        {
            $output_arr['display_msg'] = $chk_valid;
            return $output_arr;
        }


        $custid         = $request->input('partyId');
        $preferenceData = \App\Models\Userconfiguration::select('UserWalletID', 'ConfigType', 'Details')->where([ [ 'UserWalletID', $custid ], [ 'ConfigType', 'preferences' ] ])->first();

        if ( empty($preferenceData->UserWalletID) )
        {
            $data_json = array( 'is_error' => TRUE, 'message' => 'User Not Found.', 'res_data' => [] );
            return $data_json;
        }
        $prefrence = json_decode($preferenceData);
        $prefer    = json_decode($prefrence->Details);

        $array     = [
            'partyId' => $preferenceData->UserWalletID,
            'Details' => $prefer,
        ];
        $data_send = array( 'is_error' => FALSE, 'error_msg' => '', 'code' => '200', 'res_data' => $array );
        return $data_json = json_encode($data_send);
    }

    // code for validation for update preferences and view prefernces
    private function updatecheck_valid($request, $type)
    {

        if ( $type == 'updatePreferences' )
        {
            $validations = [
                'partyId'      => 'required|numeric',
                'notification' => 'required|in:Y,N',
                'trans_msg'    => 'required|in:Y,N',
                'trans_email'  => 'required|in:Y,N',
                'offer_alert'  => 'required|in:Y,N',
                'd_not_d'      => 'required|in:Y,N',
            ];
        }
        if ( $type == 'viewPreferences' )
        {
            $validations = [
                'partyId' => 'required|numeric',
            ];
        }


        $messages = [];

        return $this->_validate_method($request, $validations, $messages);
    }

}

/* End of file MyAccountController.php *//* Location: ./controller/User/MyAccountController.php */
