<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactionstatistic
 * 
 * @property string $TransactionType
 * @property int $TotalNumberofTransactions
 * @property \Carbon\Carbon $ReportDate
 * @property int $TotalAmount
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property int $PartyId
 *
 * @package App\Models
 */
class Transactionstatistic extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TotalNumberofTransactions' => 'int',
		'TotalAmount' => 'int',
		'PartyId' => 'int'
	];

	protected $dates = [
		'ReportDate',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TotalNumberofTransactions',
		'TotalAmount',
		'UpdatedDateTime',
		'UpdatedBy',
		'PartyId'
	];
}
