<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a Bus Ticket Booking</li>
        <li>System should check if the transaction date is less than 30th April, 2018</li>
        <li>System should calculate the total number of transactions on which the coupon/discount is applied for that customer </li>
        <li>The total number of transactions on which the discount has been applied (as identified in step 3) should not exceed 3 </li>
        <li>System should calculate the discount amount as 10% of Ticket Base Fare Amount and the same shouldn't exceed INR 100</li>
        <li>System should deduct the Discount Amount identified in step 5 from the Bus Ticket Fare Amount </li>
    </ul> 
</div>
<?php include 'footer.php';?>