<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * request money
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */

$salutation     = trans('messages.salutation');
$salutationName = ($receiverName) ? $salutation . ' ' . $fullname : $salutation;
$array          = [
    'email' => [
        'subject' => 'ViolaWallet Transfer Request',
        'text'    => 'Hello '.$receiverName.',<br/> '.$SenderName.' has request for an amount of '.$amount.' . Please click <a href="" target="_blanck" >here</a> to login to pay or reject' ],
    'sms'   => [
        'text' => $SenderName.' has requested you $amount at '.$date .' '. $time.'. Please login to make payment.'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => $SenderName.' has requested you $amount at '.$date .' '. $time.'. Please login to make payment.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'genral'
    ],
    'web'   => [
        'text' => $SenderName.' has requested you $amount at '.$date .' '. $time.'.',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
