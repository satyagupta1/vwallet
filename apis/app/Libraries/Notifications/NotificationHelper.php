<?php

namespace App\Libraries\Notifications;

/*
 * Helper used for sending Notifications to mediums
 */

class NotificationHelper {

    // static variables
    static protected $partyId     = '';
    static protected $linkTransId = '';

    public static function sendNotifications($request, $templateData)
    {
        NotificationHelper::$partyId     = $templateData['partyId'];
        NotificationHelper::$linkTransId = $templateData['linkedTransId'];

        $sendsMaasArray = $receiveMaasArray = array(
            'partyId'             => $templateData['partyId'],
            'transactionId'       => $templateData['linkedTransId'],
            'email'               => FALSE,
            'sms'                 => FALSE,
            'push'                => FALSE,
            'countryCode'         => config('vwallet.countryCode'),
            'product'             => config('vwallet.product'),
            'priority'            => config('vwallet.priority'),
            'AppnotificationId'   => '0',
            'linkedTransactionId' => $templateData['linkedTransId'],
            'environment'         => config('vwallet.environment'), );

        // Halt excution if communication setting is off
        if ( config('vwallet.communicateUser') !== TRUE )
        {

            return TRUE;
        }


        $conditions = self::_getAvailableModeOfCommunications($request, NotificationHelper::$partyId, $templateData['templateKey'], $templateData);

        if ( ! $conditions['templatePath'] )
        {
            \Log::error('Notification Failure Request Data: ' . json_encode($templateData, JSON_PRETTY_PRINT) . ' Template Conditions:' . json_encode($conditions, JSON_PRETTY_PRINT));
            return FALSE;
        }

        $templateData['templateParams']['supportURL'] = "violamoney.com";

        $loadTemplate = view('Notifications.' . $conditions['templatePath'], $templateData['templateParams'])->render();

        $templet = json_decode($loadTemplate);

        // send web notification condition
        if ( $conditions['web'] === TRUE )
        {
            $webRes = self::_callWebLib($templateData, $templet->web, $sendsMaasArray);
        }

        $i = 0;
        // send SMS condition
        if ( $conditions['sms'] === TRUE )
        {
            $smsRes = self::_callSmsLib($templateData, $templet->sms, $sendsMaasArray);
            if ( ! empty($smsRes['senderSmsArray']) )
            {
                $sendsMaasArray['sms']            = TRUE;
                $sendsMaasArray['notificationId'] = $smsRes['senderNotifId'];
                $sendsMaasArray['smsData']        = ( array ) $smsRes['senderSmsArray'];

                //   $output = self::CallMaasCurl($sendsMaasArray);
            }
            if ( ! empty($smsRes['receiverSmsArray']) )
            {
                $i                                = 1;
                $receiveMaasArray['sms']            = TRUE;
                $receiveMaasArray['notificationId'] = $smsRes['receiverNotifId'];
                $receiveMaasArray['smsData']        = ( array ) $smsRes['receiverSmsArray'];
                //   $output = self::CallMaasCurl($sendsMaasArray);
            }
        }

        // send Email condition
        if ( $conditions['email'] === TRUE )
        {
            $emailRes = self::_callEmailLib($templateData, $templet->email, $sendsMaasArray);
            
            if ( ! empty($emailRes['senderEmailArray']) )
            {
                $sendsMaasArray['email']          = TRUE;
                $sendsMaasArray['notificationId'] = $emailRes['senderNotifId'];
                $sendsMaasArray['emailData']      = ( array ) $emailRes['senderEmailArray'];
                //$outputEmail = self::CallMaasCurl($sendsMaasArray);
            }

            if ( ! empty($emailRes['receiverEmailArray']) )
            {
                $i                                = 1;
                $receiveMaasArray['email']          = TRUE;
                $receiveMaasArray['notificationId'] = $emailRes['receiverNotifId'];
                $receiveMaasArray['emailData']      = ( array ) $emailRes['receiverEmailArray'];
                // $outputEmail = self::CallMaasCurl($sendsMaasArray);
            }
        }
        // send push condition
        if ( $conditions['push'] === TRUE )
        {
            $pushRes = self::_callPushLib($templateData, $templet->push, $conditions, $sendsMaasArray);
            if ( ! empty($pushRes['sender']) )
            {
                $sendsMaasArray['push']           = TRUE;
                $sendsMaasArray['notificationId'] = $pushRes['senderNotifId'];
                $sendsMaasArray['pushData']       = ( array ) $pushRes['sender'];
            }
            if ( ! empty($pushRes['receiver']) )
            {
                $i                                = 1;
                $receiveMaasArray['push']           = TRUE;
                $receiveMaasArray['notificationId'] = $pushRes['receiverNotifId'];
                $receiveMaasArray['pushData']       = ( array ) $pushRes['receiver'];
            }
        }
        
        $output = self::CallMaasCurl($sendsMaasArray); // for sender array

        if ( $i == 1 )
        {
            $output = self::CallMaasCurl($receiveMaasArray); // for receiver array
        }
        return TRUE;
    }

    private static function _getAvailableModeOfCommunications($request, $partyId, $templateKey, $templateData)
    {
        $comWays = array(
            'email'        => FALSE,
            'sms'          => FALSE,
            'web'          => FALSE,
            'push'         => FALSE,
            'mobileDevice' => 'W',
            'deviceId'     => '',
            'templatePath' => FALSE
        );
        if ( config('vwallet.communicateUser') === TRUE )
        {
            $commutWays = config('vwallet.communicationWays');
            $sms        = $commutWays['sms'];
            $email      = $commutWays['email'];
            $push       = $commutWays['push'];
            $web        = $commutWays['web'];
            $android    = $push['A'];
            $ios        = $push['I'];

            // get User Preferance

            $preferance = self::getUserPreferance($partyId);

            //if user has no preferance set as default
            if ( empty($preferance) )
            {
                $preferanceArray = array( 'd_not_d' => 'N', 'trans_email' => 'Y', 'trans_msg' => 'Y', 'push_alerts' => 'Y', 'notification' => 'Y', 'offer_alert' => 'Y' );
                $userPreferance  = ( object ) $preferanceArray;
            }
            else
            {
                $userPreferance = json_decode($preferance->Details);
            }

            if ( $userPreferance->d_not_d != 'Y' )
            {
                // get templet preferances 
                $templetPreferance = self::getTmpletConditions($templateKey);

                if ( ! $templetPreferance )
                {
                    return $comWays;
                }
                $comWays['templatePath'] = $templetPreferance->TemplatePath;
                if ( isset($userPreferance->push_alerts) && $userPreferance->push_alerts == 'Y' )
                {
                    if ( $request->header('device-type') == 'I' || $request->header('device-type') == 'A' )
                    {
                        // if device is not Web then process goes as below
                        $pushCheck               = self::pushCheck($templetPreferance->ScreenNotification, $request->header('device-type'), $android, $ios);
                        $comWays['push']         = $pushCheck;
                        $comWays['mobileDevice'] = $request->header('device-type');
                        $comWays['deviceId']     = $request->header('device-token');
                    }
                    else
                    {
                        // check any mobile session exsists for this user if yes send push notification else no push.
                        $userMobileSession = self::checkUserMobileSession($partyId);
                        if ( $userMobileSession )
                        {
                            $pushCheck               = self::pushCheck($templetPreferance->ScreenNotification, $userMobileSession->MobileType, $android, $ios);
                            $comWays['push']         = $pushCheck;
                            $comWays['deviceId']     = $userMobileSession->DeviceToken;
                            $comWays['mobileDevice'] = $userMobileSession->MobileType;
                        }
                        else
                        {
                            $comWays['mobileDevice'] = '';
                        }
                    }

                    // if recever notification sent
                    if ( $templateData['reciverId'] && ($templateData['reciverId'] !== NULL) )
                    {
                        // check any mobile session exsists for this user if yes send push notification else no push.
                        $userMobileSessionRec = self::checkUserMobileSession($templateData['reciverId']);
                        if ( $userMobileSessionRec )
                        {
                            $pushCheck                  = self::pushCheck($templetPreferance->ScreenNotification, $userMobileSessionRec->MobileType, $android, $ios);
                            $comWays['recDeviceId']     = $userMobileSessionRec->DeviceToken;
                            $comWays['recMobileDevice'] = $userMobileSessionRec->MobileType;
                        }
                    }
                }
                if ( $email === TRUE )
                {
                    $comWays['email'] = ($templetPreferance->EmailNotification == 'Y') ? (($userPreferance->trans_email == 'Y') ? TRUE : FALSE) : FALSE;
                }

                if ( $sms === TRUE )
                {
                    $comWays['sms'] = ($templetPreferance->SMSNotification == 'Y') ? (($userPreferance->trans_msg == 'Y') ? TRUE : FALSE) : FALSE;
                }

                if ( $web === TRUE )
                {
                    $comWays['web'] = ($templetPreferance->WebNotification == 'Y') ? (($userPreferance->notification == 'Y') ? TRUE : FALSE) : FALSE;
                }
            }
        }
        return $comWays;
    }

    public static function checkUserMobileSession($partyId, $deviceType = 'M')
    {
        $party = \App\Models\Partyloginhistory::select('MobileType', 'DeviceToken')
                ->where('PartyID', $partyId)
                ->where('LoginSuccess', 'Y')
                ->where('DeviceType', $deviceType)
                ->orderBy('PartyLoginHistoryId', 'DESC')
                ->first();
        return $party;
    }

    public static function getUserPreferance($partyId)
    {
        $partyPref = \App\Models\Userconfiguration::select('Details')
                ->where('UserWalletID', $partyId)
                ->where('ConfigType', 'preferences')
                ->first();
        return $partyPref;
    }

    public static function getTmpletConditions($templateKey)
    {
        $templatPref = \App\Models\Communicationtemplate::select('communicationtemplate.TemplatePath', 'communicationeventconfig.SMSNotification', 'communicationeventconfig.EmailNotification', 'communicationeventconfig.ScreenNotification', 'communicationeventconfig.WebNotification')
                ->join('communicationeventconfig', 'communicationeventconfig.SMSTemplateID', '=', 'communicationtemplate.TemplateID')
                ->where('TemplateKey', $templateKey)
                ->first();
        return $templatPref;
    }

    public static function pushCheck($ScreenNotification, $MobileType, $androidConfig, $iosConfig)
    {
//        echo $ScreenNotification . '--' . $MobileType . '--' . $androidConfig . '--' . $iosConfig;
        $res = FALSE;
        if ( $ScreenNotification == 'Y' )
        {
            if ( ($MobileType == 'A') && ($androidConfig === TRUE) )
            {
                $res = TRUE;
            }
            if ( ($MobileType == 'I') && ($iosConfig === TRUE) )
            {
                $res = TRUE;
            }
        }
        return $res;
    }

    private static function notificationArray($notificationData, $notifType, $msg, $msgStatus, $notify = '')
    {
        $dateAdded        = date('Y-m-d H:i:s');
        // Notification Log
        $notificationInti = array(
            'PartyID'             => ($notify == 'receiver') ? $notificationData['reciverId'] : $notificationData['partyId'],
            'LinkedTransactionID' => $notificationData['linkedTransId'],
            'NotificationType'    => $notifType,
            'SentDateTime'        => $dateAdded,
            'Message'             => $msg,
            'MessageStatus'       => $msgStatus,
            'CreatedBy'           => 'User',
            'CreatedDateTime'     => $dateAdded,
        );
        if ( $notifType == 'Email' )
        {
            $notificationInti['ToAddress'] = ($notify == 'receiver') ? $notificationData['reciverEmail'] : $notificationData['senderEmail'];
        }

        if ( $notifType == 'SMS' )
        {
            $notificationInti['ToAddress'] = ($notify == 'receiver') ? $notificationData['reciverMobile'] : $notificationData['senderMobile'];
        }

        if ( $notifType == 'Push' )
        {
            $notificationInti['ToAddress'] = $notificationData['senderUID'];
        }

        if ( $notifType == 'Web' )
        {
            $notificationInti['ToAddress']               = 'Notification List';
            $notificationInti['ReceipentAcknowledged']   = 'Y';
            $notificationInti['ReceipentReceivedDate']   = $dateAdded;
            $notificationInti['ReceipentDeliveryReport'] = 'Message sent';
        }

        $notificationId = \App\Models\Notificationlog::insertGetId($notificationInti);
        return $notificationId;
    }

    private static function _callEmailLib($notificationData, $templet, $sendsMaasArray)
    {


        $notifType = 'Email';
        $msgStatus = 'Hide';
        if ( array_key_exists('sender', $templet) )
        {
            $msgSender      = strip_tags($templet->sender->text);
            $msgReciver     = strip_tags($templet->receiver->text);
            $nifyIdSender   = self::notificationArray($notificationData, $notifType, $msgSender, $msgStatus, 'sender');
            $nifyIdReceiver = self::notificationArray($notificationData, $notifType, $msgReciver, $msgStatus, 'receiver');
            
            // add common email header and footer for sender
            $tempData = array('text' => $templet->sender->text);
            $emailTempSend = view('communication.email_templates.v-wallet', $tempData)->render();
            $senderEmailArray = array( 'emailTo'           => $notificationData['senderEmail'],
                'emailSubject'      => $templet->sender->subject,
                'emailText'         => $emailTempSend,
                'AppnotificationId' => $nifyIdSender,
            );

            // add common email header and footer for receiver
            $templateData = array('text' => $templet->receiver->text);
            $emailTempRec = view('communication.email_templates.v-wallet', $templateData)->render();

            $receiverEmailArray = array( 'emailTo'           => $notificationData['reciverEmail'],
                'emailSubject'      => $templet->receiver->subject,
                'emailText'         => $emailTempRec,
                'AppnotificationId' => $nifyIdReceiver,
            );

            $notificationIdSender   = $nifyIdSender;
            $notificationIdReceiver = $nifyIdReceiver;
            return array( 'senderEmailArray'   => ( ! empty($senderEmailArray)) ? $senderEmailArray : '',
                'senderNotifId'      => $notificationIdSender,
                'receiverEmailArray' => ( ! empty($receiverEmailArray)) ? $receiverEmailArray : '',
                'receiverNotifId'    => $notificationIdReceiver,
            );


            //\App\Libraries\Notifications\EmailHelper::sendEmail($notificationData['senderEmail'], $templet->sender->subject, $templet->sender->text, $nifyIdSender);
            //\App\Libraries\Notifications\EmailHelper::sendEmail($notificationData['reciverEmail'], $templet->receiver->subject, $templet->receiver->text, $nifyIdReceiver);
        }
        else
        {
            // Add common email header and footer for receiver
            $msgSender    = $templet->text;
            $templateData = array('text' => $msgSender);
            $emailTemp = view('communication.email_templates.v-wallet', $templateData)->render();

            $nifyIdSender = self::notificationArray($notificationData, $notifType, $msgSender, $msgStatus);

            $senderEmailArray = array( 'emailTo'           => $notificationData['senderEmail'],
                'emailSubject'      => $templet->subject,
                'emailText'         => $emailTemp,
                'AppnotificationId' => $nifyIdSender,
            );

            $notificationIdSender = $nifyIdSender;
            
            return array( 'senderEmailArray'   => ( ! empty($senderEmailArray)) ? $senderEmailArray : '',
                'senderNotifId'      => $notificationIdSender,
                'receiverEmailArray' => '',
                'receiverNotifId'    => '',
            );

            // \App\Libraries\Notifications\EmailHelper::sendEmail($notificationData['senderEmail'], $templet->subject, $templet->text, $nifyIdSender);
        }
    }

    private static function _callSmsLib($notificationData, $templet, $sendsMaasArray)
    {
        $notifType = 'SMS';
        $msgStatus = 'Hide';
        if ( array_key_exists('sender', $templet) )
        {
            $nifyIdSender           = self::notificationArray($notificationData, $notifType, $templet->sender->text, $msgStatus, 'sender');
            $nifyIdReceiver         = self::notificationArray($notificationData, $notifType, $templet->receiver->text, $msgStatus, 'receiver');
            $senderSmsArray         = array(
                'to'                => $notificationData['senderMobile'],
                'text'              => $templet->sender->text,
                'AppnotificationId' => $nifyIdSender,
            );
            $reciverSmsArray        = array(
                'to'                => $notificationData['reciverMobile'],
                'text'              => $templet->receiver->text,
                'AppnotificationId' => $nifyIdReceiver,
            );
            $notificationIdSender   = $nifyIdSender;
            $notificationIdReceiver = $nifyIdReceiver;


//             \App\Libraries\Notifications\SmsHelper::sendSmsNow($senderArray, $nifyIdSender);
//            \App\Libraries\Notifications\SmsHelper::sendSmsNow($reciverArray, $nifyIdReceiver);
//           
            return array( 'senderSmsArray'   => ( ! empty($senderSmsArray)) ? $senderSmsArray : '',
                'senderNotifId'    => $notificationIdSender,
                'receiverSmsArray' => ( ! empty($reciverSmsArray)) ? $reciverSmsArray : '',
                'receiverNotifId'  => $notificationIdReceiver
            );
        }
        else
        {
            $nifyIdSender = self::notificationArray($notificationData, $notifType, $templet->text, $msgStatus);

            $notificationIdSender = $nifyIdSender;
            $senderSmsArray       = array(
                'to'                => $notificationData['senderMobile'],
                'text'              => $templet->text,
                'AppnotificationId' => $nifyIdSender,
            );

            return array( 'senderSmsArray'   => ( ! empty($senderSmsArray)) ? $senderSmsArray : '',
                'senderNotifId'    => $notificationIdSender,
                'receiverSmsArray' => '',
                'receiverNotifId'  => ''
            );


            //\App\Libraries\Notifications\SmsHelper::sendSmsNow($senderArray, $nifyIdSender);
        }
    }

    private static function _callPushLib($notificationData, $templet, $conditions, $sendsMaasArray)
    {
        $notifType = 'bigTextStyle';
        $msgStatus = 'Hide';

        if ( array_key_exists('sender', $templet) )
        {

            if ( $conditions['mobileDevice'] == 'A' )
            {
                $notificationData['senderUID'] = $conditions['deviceId'];
                $nifyIdSender                  = self::notificationArray($notificationData, $notifType, $templet->sender->body, $msgStatus, 'sender');

                $sender = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != '')) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? 0 : $templet->actionParams,
                );
                //\App\Libraries\Notifications\AndriodPushHelper::sendAndroidNotification($conditions['deviceId'], $templet->sender, $nifyIdSender);
            }

            if ( $conditions['mobileDevice'] == 'I' )
            {
                $notificationData['senderUID'] = $conditions['deviceId'];
                $nifyIdSender                  = self::notificationArray($notificationData, $notifType, $templet->sender->body, $msgStatus, 'sender');
                //\App\Libraries\Notifications\IosPushHelper::sendNotification($conditions['deviceId'], $templet->sender, $nifyIdSender);

                $sender = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != '')) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? 0 : $templet->actionParams,
                );
            }

            if ( $conditions['recMobileDevice'] == 'A' )
            {
                $notificationData['senderUID'] = $conditions['recDeviceId'];
                $nifyIdReceiver                = self::notificationArray($notificationData, $notifType, $templet->receiver->body, $msgStatus, 'receiver');
                // \App\Libraries\Notifications\AndriodPushHelper::sendAndroidNotification($conditions['recDeviceId'], $templet->receiver, $nifyIdReceiver);

                $receiver = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != 0)) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? '' : $templet->actionParams,
                );
            }

            if ( $conditions['recMobileDevice'] == 'I' )
            {
                $notificationData['senderUID'] = $conditions['recDeviceId'];
                $nifyIdReceiver                = self::notificationArray($notificationData, $notifType, $templet->receiver->body, $msgStatus, 'receiver');

                $receiver = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != '')) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? 0 : $templet->actionParams,
                );

//  \App\Libraries\Notifications\IosPushHelper::sendNotification($conditions['recDeviceId'], $templet->receiver, $nifyIdReceiver);
            }

            return $dataArray = array(
                'sender'          => $sender,
                'senderNotifId'   => $nifyIdSender,
                'receiver'        => $receiver,
                'receiverNotifId' => $nifyIdReceiver,
            );
        }
        else
        {
            if ( $conditions['mobileDevice'] == 'A' )
            {
                $notificationData['senderUID'] = $conditions['deviceId'];

                $nifyIdSender = self::notificationArray($notificationData, $notifType, $templet->body, $msgStatus);


                $sender = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != '')) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? 0 : $templet->actionParams,
                );
                //       \App\Libraries\Notifications\AndriodPushHelper::sendAndroidNotification($conditions['deviceId'], $templet, $nifyIdSender);
            }

            if ( $conditions['mobileDevice'] == 'I' )
            {
                $notificationData['senderUID'] = $conditions['deviceId'];
                $nifyIdSender                  = self::notificationArray($notificationData, $notifType, $templet->body, $msgStatus);

                $sender = array(
                    'to'                 => $conditions['deviceId'],
                    'deviceType'         => $conditions['mobileDevice'],
                    'title'              => ($templet->title != '') ? $templet->title : 0,
                    'body'               => ($templet->body != '') ? $templet->body : 0,
                    'summaryText'        => ($templet->summaryText != '') ? $templet->summaryText : 0,
                    'notificationType'   => (empty($templet->notificationType != '')) ? $templet->notificationType : 'bigTextStyle',
                    'imageUrl'           => (empty($templet->imageUrl)) ? 0 : $templet->imageUrl, // image Url for imageStyle
                    'button1'            => (empty($templet->button1)) ? 0 : $templet->button1, // button 1 name 
                    'actionUrl1'         => (empty($templet->actionUrl1)) ? 0 : $templet->actionUrl1, // acction url endpoint 1
                    'button2'            => (empty($templet->button2)) ? 0 : $templet->button2,
                    'actionUrl2'         => (empty($templet->actionUrl2)) ? 0 : $templet->actionUrl2,
                    'screenNavigationId' => (empty($templet->screenNavigationId)) ? 0 : $templet->screenNavigationId, // navigation Id for app
                    'AppnotificationId'  => (empty($templet->notificationId)) ? 0 : $templet->notificationId,
                    'actionParams'       => (empty($templet->actionParams)) ? 0 : $templet->actionParams,
                );

//  \App\Libraries\Notifications\IosPushHelper::sendNotification($conditions['deviceId'], $templet, $nifyIdSender);
            }

            return $dataArray = array(
                'sender'          => $sender,
                'senderNotifId'   => $nifyIdSender,
                'receiver'        => '',
                'receiverNotifId' => '',
            );
        }
    }

    private static function _callWebLib($notificationData, $templet)
    {
        $notifType = 'Web';
        $msgStatus = 'UnRead';
        if ( array_key_exists('sender', $templet) )
        {
            self::notificationArray($notificationData, $notifType, $templet->sender->text, $msgStatus, 'sender');
            self::notificationArray($notificationData, $notifType, $templet->receiver->text, $msgStatus, 'receiver');
        }
        else
        {
            self::notificationArray($notificationData, $notifType, $templet->text, $msgStatus);
        }
    }

    public static function CallMaasCurl($paramsArray)
    {
        \Log::info('mass Request:' . json_encode($paramsArray, JSON_PRETTY_PRINT));
        $url = config('vwallet.MaasUrl'); // Maas Test Url

        $enKey             = config('vwallet.ENCRYPT_SALT');
        $paramsDataEncrypt = \App\Libraries\Helpers::encryptData(json_encode($paramsArray), $enKey, TRUE);
        $output            = self::_curl_send_sms($url, array( 'encryptedData' => $paramsDataEncrypt ));
        $outData           = json_decode($output, TRUE);
        //$paramsDataDecrypt = \App\Libraries\Helpers::decryptDataWithoutInput($outData, $enKey, TRUE);
        return $output;
    }

    private static function _curl_send_sms($url, $feilds)
    {

        $ch     = curl_init();                       // initialize CURL
        curl_setopt($ch, CURLOPT_POST, FALSE);    // Set CURL Post Data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $feilds);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        //curl_error($ch);

        curl_close($ch);                         // Close CURL

        return $output;
    }

}
