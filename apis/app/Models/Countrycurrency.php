<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Countrycurrency
 * 
 * @property string $CountryCode
 * @property string $CurrencyCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Country $country
 * @property \App\Models\Currency $currency
 *
 * @package App\Models
 */
class Countrycurrency extends Eloquent
{
	protected $table = 'countrycurrency';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'CountryCode');
	}

	public function currency()
	{
		return $this->belongsTo(\App\Models\Currency::class, 'CurrencyCode');
	}
}
