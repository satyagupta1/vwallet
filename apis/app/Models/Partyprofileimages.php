<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyprofileimages
 * 
 * @property int $PartyID
 * @property string $ProfileImage
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partyaddresses
 *
 * @package App\Models
 */
class Partyprofileimages extends Eloquent {

    protected $table      = 'partyprofileimages';
    protected $primaryKey = 'PartyProfileImageId';
    public $incrementing  = false;
    public $timestamps    = false;
    protected $dates = [
        'CreatedDateTime',
        'UpdatedDateTime'
    ];
    protected $fillable = [
        'PartyID',
        'ProfileImage',
        'PrimaryImage',
        'UpdatedDateTime',
        'UpdatedBy',
        'CreatedDateTime',
        'CreatedBy',
        'ImagePosition'
    ];

  

}
