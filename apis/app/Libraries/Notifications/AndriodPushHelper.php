<?php

namespace App\Libraries\Notifications;

class AndriodPushHelper {

    static protected $apiKey = "AIzaSyByC4fA-7v1fO-tmFsKQfAsSeIehQuf15w";

    public static function sendAndroidNotification($deviceid, $msg, $notificationId)
    {
        // Log variables
        $requestId      = uniqid();
        $startTime      = microtime(TRUE);
        // Log Messge 
        $requestDetails = 'Android Request ID : ' . $requestId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ', decviceId:' . $deviceid . ', Templet : ' . json_encode($msg, JSON_PRETTY_PRINT);
        \Log::info('Request:  ' . $requestDetails);

        if ( ! $deviceid )
        {
            return 'device unique id required.';
        }

        if ( ! $msg )
        {
            return 'Please enter message text.';
        }

        // push Body actuval mssage
        $registatoin_ids = array( $deviceid );

        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $message = array();
        foreach ( $msg as $key => $value )
        {
            $message['"' . $key . '"'] = '"' . $value . '"';
        }

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data'             => $message,
        );

        $headers = array(
            'Authorization: key=' . self::$apiKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch      = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        $notifyUpdate = array(
            'ReceipentReceivedDate' => date('Y-m-d H:i:s')
        );

        if ( $result === FALSE )
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = curl_error($ch);
            $requestDetails                          = 'Android Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . 'Message Status: ' . curl_error($ch);
            \Log::info('Reponse:  ' . $requestDetails);
            // die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        $res = json_decode($result);
        if ( $res->failure > 0 )
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = $result;
        }
        else
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'Y';
            $notifyUpdate['ReceipentDeliveryReport'] = $result;
        }

        $requestDetails = 'Android Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . 'Message Status: Sent ' . $result;
        \Log::info('Reponse:  ' . $requestDetails);

        \App\Models\Notificationlog::where('NotificationID', $notificationId)->update($notifyUpdate);

        return $result;
    }

}
