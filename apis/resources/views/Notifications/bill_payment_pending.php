<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Recharge Fails
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction in Process',
        'text'    => $salutationName.',<br/><br/>
            Payment for '.$billerName.' for INR '.$amount.' is in process. Please check the status after sometime. Incase, transaction is failed, we will refund your amount automatically.',
    ],
    'sms'   => [
        'text' => 'Payment for '.$billerName.' for INR '.$amount.' is in process. Please check the status after sometime. Incase, transaction is failed, we will refund your amount automatically.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Payment for '.$billerName.' for INR '.$amount.' is in process. Please check the status after sometime. Incase, transaction is failed, we will refund your amount automatically.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Payment for '.$billerName.' for INR '.$amount.' is in process. Please check the status after sometime. Incase, transaction is failed, we will refund your amount automatically',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
