<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a TopUp/Load Money of Transaction Amount >= INR 1000.</li>
        <li>System should check if the transaction date is less than 31st Jan, 2018.</li>
        <li>System should credit INR 50 cashback into the users wallet for every successfull top up >=  INR 1000.</li>
    </ul> 
</div>
<?php include 'footer.php';?>