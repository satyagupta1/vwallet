<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Viola Viola Wallet Yes Bank Error Messages Language Lines
      |--------------------------------------------------------------------------
      |
      |
     */

    '00'  => [ // Code of yes bank response
        'code'        => '00', // Code of yes bank response
        'status_code' => 'SUCCESS', // Status code of yes bank response
        'message'     => 'User Found', // Error description from Yes bank response
        'VWMsg'       => '', // viola User Readable Messages
    ],
    'R26' => [
        'code'        => 'R26',
        'status_code' => 'ERROR',
        'message'     => 'User Not Found',
        'VWMsg'       => '',
    ],
    'R27' => [
        'code'        => 'R27',
        'status_code' => 'ERROR',
        'message'     => 'User present but not found in central SOR',
        'VWMsg'       => '',
    ],
    '01'  => [
        'code'        => '01',
        'status_code' => 'SUCCESS',
        'message'     => 'New User',
        'VWMsg'       => '',
    ],
];
