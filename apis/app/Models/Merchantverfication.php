<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantverfication
 * 
 * @property int $MerchantVerficationID
 * @property int $MerchantID
 * @property string $VerificationStatus
 * @property int $VerificationCenterID
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 * @property \App\Models\Verificationcenter $verificationcenter
 * @property \Illuminate\Database\Eloquent\Collection $merchantverificationdetails
 *
 * @package App\Models
 */
class Merchantverfication extends Eloquent
{
	protected $table = 'merchantverfication';
	protected $primaryKey = 'MerchantVerficationID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantVerficationID' => 'int',
		'MerchantID' => 'int',
		'VerificationCenterID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'MerchantID',
		'VerificationStatus',
		'VerificationCenterID',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDatetime',
		'UpdatedBy'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}

	public function verificationcenter()
	{
		return $this->belongsTo(\App\Models\Verificationcenter::class, 'VerificationCenterID');
	}

	public function merchantverificationdetails()
	{
		return $this->hasMany(\App\Models\Merchantverificationdetail::class, 'MerchantVerficationID');
	}
}
