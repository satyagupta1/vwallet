<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a Bus Ticket Booking</li>
        <li>System should check if the transaction date is less than 30th April, 2018</li>
        <li>System should check if the user selected the Source while booking ticket as 'Hyderabad'</li>
        <li>System should calculate the cashback amount as 15% of Ticket Base Fare Amount and the same shouldn't exceed INR 100</li>
        <li>System should credit the cashback Amount identified in step 4 after successful payment into user's account within 3 business days. </li>
    </ul> 
</div>
<?php include 'footer.php';?>
