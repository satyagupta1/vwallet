<?php

/**
 * ContactusController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in/ Licence
 */

namespace App\Http\Controllers\Support;

/**
 * ContactusController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in/ Licence
 */
class ContactusController extends \App\Http\Controllers\Controller {

    /**
     * This ContactusInsert method is used to insert contact details before login and after login
     * @desc In this we will check _validateContactusInsert method for validation of fullname,mobileno,emailid,message,CreatedDateTime,CreatedBy and device type
     * @deesc In this we will call _get_exist_partyId method to get party id with given mobile number or email from database if user exists else 0 will return
     * @desc If all validations have no errors will Insert else validation errors will show
     * 
     * @param Request $request
     * @param fullname string
     * @param mobileno integer
     * @param emailid string
     * @param message string
     * @param emailid string 
     * @return JSON
     */
    public function ContactusInsert(\Illuminate\Http\Request $request)
    {

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        // check validation
        $chk_valid  = $this->_validateContactusInsert($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        $CRM_data = \App\Libraries\Helpers::crmData();

        $insert_array   = array(
            'entryPoint'  => 'API',
            'action'      => 'create',
            'module'      => 'Tickets',
            'msg'         => $request->input('message'),
            'priority'    => $CRM_data['ticket_priority'],
            'subject'     => $request->input('subject'),
            'status'      => $CRM_data['ticket_status'],
            'customer'    => $request->input('fullname'),
            'product'     => $CRM_data['product'],
            'ticket_type' => $request->input('ticket_type'),
            'mobile'      => $request->input('mobileno'),
            'email'       => $request->input('emailid'),
            'ip'          => $request->input('ip'),
            'type'        => $CRM_data['ticket_type'],
            'source'      => $CRM_data['ticket_source'],
            'custid'      => $this->_get_exist_partyId($request),
        );
        // $insert_array['custid'] = $user_id;
        $attachment_url = "";
        $output         = \App\Libraries\Helpers::curl_send($CRM_data['actionurl'], $insert_array);
        $out            = json_decode($output);
        if ( $out->status === 'success' )
        {
            // code to insert attachment with ticket
            
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array( 'info' => trans('messages.contactSuccessMsg', ['ticketNumber' => $out->ticketno]) );
            return json_encode($output_arr);
        }

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.contactFaildMsg') );
        return json_encode($output_arr);
    }

    /**
     * This UpdateUserDetailsCrm method is used to Update contact details
     * @desc In this we will check _validateContactusInsert method for validation of fullname,mobileno,emailid,message,CreatedDateTime,CreatedBy and device type
     * @deesc In this we will call _get_exist_partyId method to get party id with given mobile number or email from database if user exists else 0 will return
     * @desc If all validations have no errors will Insert else validation errors will show
     * 
     * @param Request $request
     * @param fullname string
     * @param mobileno integer
     * @param emailid string
     * @param message string
     * @param emailid string 
     * @return JSON
     */
    public function UpdateUserStatusCrm(\Illuminate\Http\Request $request)
    {

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        // check validation
        $chk_valid  = $this->_validateUserStatusCrm($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        //trans attributes
        $attrUpdate = trans('messages.attributes.update');
        
        $CRM_data = \App\Libraries\Helpers::crmData();

        $insert_array = array(
            'entryPoint' => 'API',
            'action'     => 'update',
            'module'     => 'Customer',
            'email'      => $request->input('emailid'),
            'custid'     => $request->input('PartyId'),
            'status'     => $request->input('status'),
        );

        $output = \App\Libraries\Helpers::curl_send($CRM_data['actionurl'], $insert_array);
        $out    = json_decode($output);
        if ( $out === 'success' )
        {
            // code to insert attachment with ticket
            $message = trans('messages.nameSuccessful', ['name' => $attrUpdate]);
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array( 'info' => $message );
            return json_encode($output_arr);
        }
        
        $message = trans('messages.nameFaild', ['name' => $attrUpdate]);
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => $message );
        return json_encode($output_arr);
    }

    /**
     * This _get_exist_partyId method is used to get partyId from database
     * 
     * @param Request $request
     * @param mobileno integer
     * @param emailid string
     *
     * @return string
     */
    private function _get_exist_partyId($request)
    {
        $sel_userwallet = \App\Models\Userwallet::
                select('UserWalletID')
                ->where('MobileNumber', $request->input('mobileno'))
                ->orwhere('EmailID', $request->input('emailid'))
                ->first();
        return ($sel_userwallet) ? $sel_userwallet->UserWalletID : 0;
    }

    /**
     * This _validateContactusInsert is used to validate input parameters
     *
     * @param Request $request
     * @param fullname string
     * @param mobileno integer
     * @param emailid string
     * @param message string
     * @param emailid string 
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _validateContactusInsert($request)
    {
        // validations

        $validate = array(
            'fullname'    => 'required|min:3|max:30|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/',
            'mobileno'    => 'numeric|required|digits:10',
            'emailid'     => 'required|email|max:100',
            'message'     => 'required',
            'subject'     => 'required|min:3',
            'ticket_type' => 'required|digits_between:1,25',
            'ip'          => 'required',
        );
        $messages = [];

        // validation for mobile device
        if ( ( $request->input('device_type') != 'W' ) )
        {
            $device_valid = \App\Libraries\Helpers::deviceValid();
            $validate     = array_merge($validate, $device_valid);
            $device_msges = \App\Libraries\Helpers::deviceValidMsg();
            $messages     = array_merge($messages, $device_msges);
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    // code to validate update crm status
    private function _validateUserStatusCrm($request)
    {
        // validations

        $validate = array(
            'PartyId' => 'numeric|required',
            'status'  => 'required|in:Active,DeActive,Block',
            'emailid' => 'required|email|max:100',
        );
        $messages = [];

        // validation for mobile device
        if ( ( $request->input('device_type') != 'W' ) )
        {
            $device_valid = \App\Libraries\Helpers::deviceValid();
            $validate     = array_merge($validate, $device_valid);
            $device_msges = \App\Libraries\Helpers::deviceValidMsg();
            $messages     = array_merge($messages, $device_msges);
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
