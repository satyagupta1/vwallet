<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 05 Oct 2017 13:35:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletbusinessmodel
 * 
 * @property int $WalletBusinessModelid
 * @property string $BusinessModelName
 * @property string $ActiveFlag
 * @property \Carbon\Carbon $StartDate
 * @property \Carbon\Carbon $EndDate
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $productplanconfigs
 *
 * @package App\Models
 */
class Walletbusinessmodel extends Eloquent
{
	protected $table = 'walletbusinessmodel';
	protected $primaryKey = 'WalletBusinessModelid';
	public $timestamps = false;

	protected $dates = [
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BusinessModelName',
		'ActiveFlag',
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function productplanconfigs()
	{
		return $this->hasMany(\App\Models\Productplanconfig::class, 'WalletBusinessModelid');
	}
}
