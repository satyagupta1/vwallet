<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Ticket Status template
 *  param $fullname varchar
 *  param $queryName varchar
 *  param $ticketNumber varchar
 *  param $solutionDescription varchar
 * 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Ticket Number : ' . $ticketNumber . ' from ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
                        Thank you for contacting us regarding ' . $queryName . '. 
                            We believe that ' . $ticketNumber . ' has now been resolved now.
                        ' . $solutionDescription . '
                        Status: Closed
                        If you find that the problem still persists, kindly click on the below status as Re-Open 
                        as soon as possible. We will reopen the query and investigate again
                        Status: <a href="">Re-Open</a>',
    ],
    'sms'   => [
        'text' => $salutationName . ',
                    Thank you for contacting us regarding '.$queryName.'. 
                    We believe that ' . $ticketNumber . ' has now been resolved now.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Ticket number ' . $ticketNumber . ' resolved',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Query ' . $queryName . ' resolved. Ticket number:' . $ticketNumber . '',
    ],
];

echo json_encode($array);

/* End of file support_ticket_status.php */
