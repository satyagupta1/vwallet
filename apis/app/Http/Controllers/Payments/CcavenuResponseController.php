<?php

namespace App\Http\Controllers\Payments;

/**
 * CcavenuResponseController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class CcavenuResponseController extends \App\Http\Controllers\Controller {
    /*
     *  Get CcAvenue Response
     *  @param Request $request
     *  @return HTML
     */

    public function paymentResponse(\Illuminate\Http\Request $request)
    {
        $messageContent = NULL;
        $messageArray   = array();
        $response       = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ( $request->input('encResp') !== NULL )
        {
            $order_transaction_id = 0;
            $workingKey           = config('vwallet.workingKeyCcA');  //Working Key should be provided here.
            $encResponse          = $_POST['encResp'];   //This is the response sent by the CCAvenue Server
            $rcvdString           = \App\Libraries\Payments\Ccavenue::decrypt($encResponse, $workingKey);  //Crypto Decryption used as per the specified working key.
            //if you want to html
            $ourStatus            = NULL;
            $decryptValues        = explode('&', $rcvdString);
            $pgResponseArray      = array();
            foreach ( $decryptValues as $value )
            {
                $pgKeys = explode('=', $value);
                if ( $pgKeys )
                {
                    $pgResponseArray[$pgKeys[0]] = $pgKeys[1];
                }
            }
            $attrSuccessful  = trans('messages.attributes.successful');
            $attrTransaction = trans('messages.attributes.transaction');

            switch ( $pgResponseArray['order_status'] )
            {
                case 'Success':
                    $ourStatus      = 'Success';
                    $successMsg     = trans('messages.moduleStatus', [ 'module' => $attrTransaction, 'status' => $attrSuccessful ]);
                    $displayMessage = $successMsg;
                    break;
                case 'Aborted':
                    $ourStatus      = 'Failed';
                    $displayMessage = trans('messages.ccAvanueAborted');
                    break;
                case 'Failure':
                    $ourStatus      = 'Failed';
                    $displayMessage = trans('messages.ccAvanueFailure');
                    break;
                case 'Failed':
                    $ourStatus      = 'Failed';
                    $displayMessage = trans('messages.ccAvanueFailure');
                    break;
                case 'Invalid':
                    $ourStatus      = 'Invalid';
                    $displayMessage = trans('messages.ccAvanueInvalid');
                    break;
                default:
                    $displayMessage = trans('messages.securityError');
                    break;
            }

            //we got response from CCAvenue
            $updateTxnDetail = \App\Libraries\PaymentGatewayResponse::updateTransctionStaus($ourStatus, $pgResponseArray);
            if ( $ourStatus === 'Success' && $updateTxnDetail->YesBankStatusCode === 'SUCCESS' )
            {
                \App\Libraries\PaymentGatewayResponse::updateTransctionwallets($pgResponseArray, $updateTxnDetail);
                \App\Libraries\OfferBussness::addbenefitTrack($updateTxnDetail->TransactionDetailID);
                $partyId = $updateTxnDetail->ToWalletId;
                $orderAmount = $updateTxnDetail->Amount;
                //Aggregate with forward wallet balance
                
                //each transaction aggregate
                \App\Libraries\Statistics::yearlyCreditTransactionsUpdate($partyId, $orderAmount);                  
            }
            $paymentMode = isset($pgResponseArray['payment_mode']) ? $pgResponseArray['payment_mode']:''; 
            $commData = array('status' => $ourStatus, 'payment_mode' => $paymentMode);
            $this->_topupCommunications($request, $commData, $updateTxnDetail);
        }
        else
        {
            $displayMessage = trans('messages.securityError');
        }

        $pgResponseArray['msg_heading']     = $displayMessage;
        $jsonEncodeData                     = json_encode($pgResponseArray);
        $enKey               = config('vwallet.ENCRYPT_SALT'); 
        $responseDataEncrypt                = \App\Libraries\Helpers::encryptData($jsonEncodeData, $enKey, TRUE);
        $response['is_error']               = FALSE;
        $response['display_msg']            = array( 'info' => 'Transction ' . $displayMessage );
        $response['res_data']['pageStatus'] = 'closeBrowser';
        $response['res_data']['result']     = '';
        //return redirect('transctionComplete');
        $frontUrl   = config('vwallet.frontUrl');
        return redirect($frontUrl.'payment-redirect?res='.$responseDataEncrypt);

    }

    private static function _topupCommunications($request, $commData, $updateTxnDetail) 
    {   
        $templateData = [];
        $order_amount = $updateTxnDetail->Amount;
        $dateTime = $updateTxnDetail->CreatedDatetime;

        $date = date('m/d/Y', strtotime($dateTime));
        $time = date('h:m A', strtotime($dateTime));

        $partyId = $updateTxnDetail->ToWalletId;
        $userDetails = \App\Models\Userwallet::select('userwallet.FirstName', 'userwallet.LastName', 'userwallet.EmailID', 'userwallet.MobileNumber', 'walletaccount.CurrentAmount')
                ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $partyId)
                ->first();
        $cardDetails = \App\Models\Partypreferredpaymentmethod::select('CardDisplayNumber')
                ->where('PaymentMethodID', $updateTxnDetail->FromAccountId)
                ->first();

        $senderName = ($userDetails->FirstName) ? $userDetails->FirstName : '';
        
         if ($commData['status'] === 'Success') {
            $templateData = [
                'partyId' => $partyId,
                'linkedTransId' => '0', // if transction related notification
                'templateKey' => 'VW038',
                'senderEmail' => $userDetails->EmailID,
                'senderMobile' => $userDetails->MobileNumber,
                'reciverId' => '', // if reciver / sender exsists
                'reciverEmail' => '',
                'reciverMobile' => '',
                'templateParams' => array(// template utalisation parameters
                    'fullname' => $senderName,
                    'paymentMode' => $commData['payment_mode'],
                    'cardnumber' => (isset($cardDetails->CardDisplayNumber)) ? $cardDetails->CardDisplayNumber : '',
                    'amount' => $order_amount,
                    'totalamount' => $userDetails->CurrentAmount,
                    'date' => $date,
                    'time' => $time,
                    
                )
            ];
         }
                
        if ($commData['status'] === 'Failed') {
            $templateData = [
                'partyId' => $partyId,
                'linkedTransId' => '0', // if transction related notification
                'templateKey' => 'VW039',
                'senderEmail' => $userDetails->EmailID,
                'senderMobile' => $userDetails->MobileNumber,
                'reciverId' => '', // if reciver / sender exsists
                'reciverEmail' => '',
                'reciverMobile' => '',
                'templateParams' => array(// template utalisation parameters
                    'fullname' => $senderName,
                    'cardnumber' => '',
                    'amount' => $order_amount, 
                    'date' => $date,
                    'time' => $time,
                    'supportURL'    => "violamoney.com",
                )
            ];
        }

        if($templateData)
        {
          \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        }
        
        // End Communication
    }
}
