<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantfeedetail
 * 
 * @property int $MerchantFeeDetailid
 * @property int $MerchantID
 * @property string $FeeAmtTag
 * @property string $FeeAmtType
 * @property float $FeeAmtValue
 * @property string $FeeDetailStatus
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 *
 * @package App\Models
 */
class Merchantfeedetail extends Eloquent
{
	protected $table = 'merchantfeedetail';
	protected $primaryKey = 'MerchantFeeDetailid';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantFeeDetailid' => 'int',
		'MerchantID' => 'int',
		'FeeAmtValue' => 'float'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'MerchantID',
		'FeeAmtTag',
		'FeeAmtType',
		'FeeAmtValue',
		'FeeDetailStatus',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}
}
