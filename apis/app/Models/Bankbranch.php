<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bankbranch
 * 
 * @property int $BankBranchID
 * @property string $BankCode
 * @property string $BranchName
 * @property int $BranchAddressId
 * @property int $BranchLocalCurrency
 * @property string $IFSCCode
 * @property string $MICRCode
 * @property string $SWIFTCode
 * @property string $OperatingCurrencyCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Partyaddress $partyaddress
 * @property \App\Models\Bank $bank
 * @property \App\Models\Currency $currency
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $banktransactionreconciliationdetails
 *
 * @package App\Models
 */
class Bankbranch extends Eloquent {

    protected $table     = 'bankbranch';
    public $incrementing = false;
    public $timestamps   = false;
    protected $casts = [
        'BankBranchID'        => 'int',
        'BranchAddressId'     => 'int',
        'BranchLocalCurrency' => 'int'
    ];
    protected $dates = [
        'CreatedDateTime',
        'UpdatedDateTime'
    ];
    protected $fillable = [
        'BranchName',
        'BankCode',
        'AddressLine1',
        'AddressLine2',
        'City',
        'State',
        'Country',
        'PinCode',
        'BranchLocalCurrency',
        'IFSCCode',
        'MICRCode',
        'SWIFTCode',
        'OperatingCurrencyCode',
        'CreatedDateTime',
        'CreatedBy',
        'UpdatedDateTime',
        'UpdatedBy'
    ];

    public function partyaddress()
    {
        return $this->belongsTo(\App\Models\Partyaddress::class, 'BranchAddressId');
    }

    public function bank()
    {
        return $this->belongsTo(\App\Models\Bank::class, 'BankCode');
    }

    public function currency()
    {
        return $this->belongsTo(\App\Models\Currency::class, 'OperatingCurrencyCode');
    }

    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class, 'BankBranchID');
    }

    public function banktransactionreconciliationdetails()
    {
        return $this->hasMany(\App\Models\Banktransactionreconciliationdetail::class, 'BankBranchID');
    }

}
