<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Sep 2017 07:38:39 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;;

/**
 * Class Usertransactionstatsaggregate
 * 
 * @property int $UserTransactionStatsAggregateId
 * @property int $UserWalletID
 * @property string $TagName
 * @property string $Key
 * @property string $Value
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Usertransactionstatsaggregate extends Eloquent
{
	protected $table = 'usertransactionstatsaggregate';
	protected $primaryKey = 'UserTransactionStatsAggregateId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserTransactionStatsAggregateId' => 'int',
		'UserWalletID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'TagName',
		'Key',
		'Value',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
