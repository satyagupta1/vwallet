<?php

namespace App\Http\Controllers\QrManagement;

/**
 * QrController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class QrController extends \App\Http\Controllers\Controller {

    public function createQrCode(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateCreateQrCode($request);

        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $params     = array('partyId' => $request->input('partyId'), 'vPin' => $request->input('vPin'));
        $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
        if ($verifyVpin === FALSE)
        {
            $output_arr['display_msg'] = array('vPin' => 'Please enter valid V-PIN');
            return response()->json($output_arr);
        }

        $requestAmount = $request->input('amount');

        // Party Exist or Not
        $party_info = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ProfileStatus', 'walletaccount.CurrentAmount', 'userwallet.IsVerificationCompleted', 'userwallet.WalletPlanTypeId')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->where('userwallet.UserWalletId', '=', $request->input('partyId'))->first();
        if (!$party_info)
        {
            $output_arr['display_msg'] = array('partyId' => 'Sender doesn`t exist.');
            return response()->json($output_arr);
        }

        $receiverID = '';
        if ($request->input('receiverMobileNumber'))
        {
            // Check Beneficiary Exist or Not
            $receiver_info = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'IsPriorityUser', 'IsSubAccount', 'walletaccount.CurrentAmount', 'userwallet.WalletPlanTypeId')
                            ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID')
                            ->where('userwallet.MobileNumber', '=', $request->input('receiverMobileNumber'))->first();
            if (!$receiver_info)
            {
                $output_arr['display_msg'] = array('recieverId' => 'Mobile doesn`t exist.');
                return response()->json($output_arr);
            }

            // Check Beneficiary is Sub Account or Not
            if ($receiver_info->IsSubAccount == 'Y')
            {
                $output_arr['display_msg'] = array('recieverId' => 'You can`t send to this mobile');
                return response()->json($output_arr);
            }

            $receiverID = $receiver_info->UserWalletID;

            if ($party_info->UserWalletID == $receiverID)
            {
                $output_arr['display_msg'] = array('recieverId' => 'You can`t use your mobile number to Create QrCode');
                return response()->json($output_arr);
            }
        }

        // Check Per Transaction Limit Exceded or Not
        if ($requestAmount > config('vwallet.PER_TXN_MAX'))
        {
            $output_arr['display_msg'] = array('amount' => 'You can send max ' . config('vwallet.PER_TXN_MAX') . '.');
            return response()->json($output_arr);
        }

        // Check Per Transaction Limit Exceded or Not
        if (($request->input('qrType') != 'request') && ($requestAmount > $party_info->CurrentAmount))
        {
            $output_arr['display_msg'] = array('amount' => 'You doesn`t have sufficient balance');
            return response()->json($output_arr);
        }

        $expiryDateTime = strtotime(date('Y-m-d H:i:s') . ' + ' . config('vwallet.QRCODEEXPIRY') . ' minute');

        $partyQrcode                  = new \App\Models\Partyqrcode;
        $partyQrcode->SenderPartyID   = $request->input('partyId');
        $partyQrcode->Amount          = $requestAmount;
        $partyQrcode->InitiatedDTTM   = date('Y-m-d H:i:s');
        $partyQrcode->ExpiryDTTM      = date('Y-m-d H:i:s', $expiryDateTime);
        $partyQrcode->ReceiverMobNum  = $request->input('receiverMobileNumber');
        $partyQrcode->ReceiverPartyID = $receiverID;
        $partyQrcode->QRType          = $request->input('qrType');
        $partyQrcode->CreatedDateTime = date('Y-m-d H:i:s');
        $partyQrcode->UpdatedDateTime = date('Y-m-d H:i:s');
        $partyQrcode->CreatedBy       = 'user';
        $partyQrcode->save();

        $encryptArray = array(
            'senderId'       => $request->input('partyId'),
            'productId'      => '12',
            'receiverId'     => $receiverID,
            'amount'         => $requestAmount,
            'initiationTime' => date('Y-m-d H:i:s'),
            'expiryTime'     => date('Y-m-d H:i:s', $expiryDateTime),
            'qrType'         => $request->input('qrType'),
            'comments'       => $request->input('comments'),
            'qrId'           => $partyQrcode->PartyQRCodeId
        );

        $encryptUserData = \App\Libraries\Crypt\Encryptor::encrypt(json_encode($encryptArray), sha1(config('vwallet.ENCRYPT_SALT')));

        $output_arr['res_data'] = $encryptUserData;
        $output_arr['is_error'] = FALSE;

        $results = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                        ->where('UserWalletId', $request->input('partyId'))->first();

        return response()->json($output_arr);
    }

    public function _validateCreateQrCode($request)
    {
        $validations = array(
            'partyId'              => 'required|numeric|digits_between:1,10',
            'productId'            => 'required|in:12',
            'receiverMobileNumber' => 'numeric|digits:10',
            'amount'               => 'required',
            'qrType'               => 'required|in:request,send',
            'vPin'                 => 'required|digits:4',
        );

        $validate_response = \Validator::make($request->all(), $validations);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function scanQrCode(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateScanQrCode($request);

        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        if ($request->input('qrType') != 'standard')
        {
            $qrData = \App\Models\Partyqrcode::select('UsedStatus')->find($request->input('qrId'));
            if (!$qrData)
            {
                $output_arr['display_msg'] = array('info' => 'Qr Code doesn`t exist.');
                return response()->json($output_arr);
            }
            if ($qrData->UsedStatus == 'U')
            {
                $output_arr['display_msg'] = array('info' => 'Qr Code already used.');
                return response()->json($output_arr);
            }

            $expiryTime  = strtotime($request->expiryLimit);
            $currentTime = strtotime(date('Y-m-d H:i:s'));
            if ($expiryTime < $currentTime)
            {
                $output_arr['display_msg'] = array('info' => 'QR code expired.');
                return response()->json($output_arr);
            }
        }

        $partyId = $request->input('partyId');

        // Check logged user and QR Generated user same or not
        if (($request->input('qrType') != 'standard') && ($partyId == $request->input('senderId')))
        {
            $output_arr['display_msg'] = array('partyId' => 'Can`t scan your own QRCode');
            return response()->json($output_arr);
        }

        $party_info = \App\Models\Userwallet::select('MobileNumber', 'UserWalletID')->where('UserWalletId', '=', $request->input('partyId'))->first();
        if (!$party_info)
        {
            $output_arr['display_msg'] = array('info' => 'Party doesn`t exist.');
            return response()->json($output_arr);
        }

        if (($request->input('qrType') != 'standard') && ($request->input('receiverId') != $party_info->UserWalletID))
        {
            $output_arr['display_msg'] = array('partyId' => 'Can`t scan this QRCode');
            return response()->json($output_arr);
        }

        if ($request->input('qrType') != 'standard')
        {
            $sender_info = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.MobileNumber', 'userwallet.ViolaID', 'userwallet.ProfileStatus', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userwallet.IsVerificationCompleted', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'userwallet.ProfileStatus', 'walletaccount.CurrentAmount', 'walletaccount.WalletCurrencyCode')
                            ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID')
                            ->where('userwallet.UserWalletID', '=', $request->input('senderId'))->first();

            if (!$sender_info)
            {
                $output_arr['display_msg'] = array('partyId' => 'sender doesn`t exist.');
                return response()->json($output_arr);
            }

            // create return response array
            $user_data_arr = array(
                'receiverPartyId'      => $sender_info->UserWalletID,
                'productId'            => '12',
                'receiverWalletId'     => $sender_info->WalletAccountId,
                'receiverWCCode'       => $sender_info->WalletCurrencyCode,
                'receiverStatus'       => ($sender_info->ProfileStatus === 'A') ? 'Active' : 'Deactive',
                'receiverFullName'     => $sender_info->FirstName . ' ' . $sender_info->MiddleName . ' ' . $sender_info->LastName,
                'receiverViolaId'      => $sender_info->ViolaID,
                'receiverMobileNumber' => $sender_info->MobileNumber,
                'qrType'               => $request->input('qrType'),
                'comments'             => $request->input('comments'),
                'amount'               => $request->input('amount'),
                'qrId'                 => $request->input('qrId')
            );
        }

        if ($request->input('qrType') == 'standard')
        {
            $reciever_info = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.MobileNumber', 'userwallet.ViolaID', 'userwallet.ProfileStatus', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userwallet.IsVerificationCompleted', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'walletaccount.WalletCurrencyCode')
                            ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID')
                            ->where('userwallet.UserWalletID', '=', $request->input('receiverId'))->first();

            if (!$reciever_info)
            {
                $output_arr['display_msg'] = array('info' => 'User doesn`t exist.');
                return response()->json($output_arr);
            }

            if ($reciever_info->IsSubAccount == 'Y')
            {
                $output_arr['display_msg'] = array('info' => 'You can`t send to this mobile');
                return response()->json($output_arr);
            }

            $banificiaryInfo      = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $reciever_info->MobileNumber, 'eWallet');
            $yesBankBeneficiaryId = isset($banificiaryInfo->yesBankBeneficiaryId) ? $banificiaryInfo->yesBankBeneficiaryId : '';
            $beneficiarymobile    = $reciever_info->MobileNumber;
            $nickName             = $reciever_info->FirstName;
            $senderMobile         = $party_info->MobileNumber;
            $AccountName          = $nickName;
            if (($banificiaryInfo == FALSE) || ($yesBankBeneficiaryId == ''))
            {
                // calling add benenficiary
                $identifier1 = $beneficiarymobile;
                $benId       = '';
                $extraParams = array(
                    'partyId'        => $partyId,
                    'referenceId'    => $benId,
                    'referenceTable' => 'userbeneficiary',
                    'status'         => 'Initiated'
                );

                $ybkReq        = array(
                    'mobile'         => $senderMobile,
                    'benifisaryName' => (!$nickName) ? $nickName : '',
                    'IdentifierType' => 'mobile',
                    'identifier1'    => $identifier1,
                    'identifier2'    => '',
                );
                $outputRespo   = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);
                $reqForYesBank = json_decode($outputRespo);

                if (isset($reqForYesBank->beneficiary))
                {
                    $YbBeneficiaryId = $reqForYesBank->beneficiary->id;
                }
                else
                {
                    if ($reqForYesBank->status_code == 'ERROR')
                    {
                        $output_arr['display_msg'] = array('info' => $reqForYesBank->message);
                        return $output_arr;
                    }
                    $YbBeneficiaryId = $reqForYesBank->beneficiary_id;
                }

                $this->addBenficiary($request, $AccountName, $partyId, $reciever_info->UserWalletID, 'eWallet', $YbBeneficiaryId, 'N');
            }
            // create return response array
            $user_data_arr = array(
                'receiverPartyId'      => $reciever_info->UserWalletID,
                'productId'            => '12',
                'receiverWalletId'     => $reciever_info->WalletAccountId,
                'receiverWCCode'       => $reciever_info->WalletCurrencyCode,
                'receiverStatus'       => ($reciever_info->ProfileStatus === 'A') ? 'Active' : 'Deactive',
                'receiverFullName'     => $reciever_info->FirstName . ' ' . $reciever_info->MiddleName . ' ' . $reciever_info->LastName,
                'receiverViolaId'      => $reciever_info->ViolaID,
                'receiverMobileNumber' => $reciever_info->MobileNumber,
                'qrType'               => $request->input('qrType'),
                'comments'             => $request->input('comments'),
                'qrId'                 => $request->input('qrId')
            );
        }

        $output_arr['res_data'] = $user_data_arr;
        $output_arr['is_error'] = FALSE;

        return response()->json($output_arr);
    }

    private function _validateScanQrCode($request)
    {
        $validations = array(
            'partyId'    => 'required|numeric|digits_between:1,10',
            'receiverId' => 'required|numeric|digits_between:1,10',
            'productId'  => 'required|in:12',
            'qrType'     => 'required|in:standard,send,request',
        );

        if (($request->input('qrType') != '') && ($request->input('qrType') != 'standard'))
        {
            $validations['qrId']           = 'required|numeric|digits_between:1,10';
            $validations['senderId']       = 'required|numeric|digits_between:1,10';
            $validations['amount']         = 'required';
            $validations['initiationTime'] = 'required|date_format:Y-m-d H:i:s';
            $validations['expiryLimit']    = 'required|date_format:Y-m-d H:i:s';
        }

        $validate_response = \Validator::make($request->all(), $validations);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function sendMoneyQrCode(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateSendRequestMoney($request, TRUE);

        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        // check amount exist in from user amount
        $senderPartyId    = $request->input('partyId');
        $senderWalletId   = $request->input('partyWalletId');
        $receiverPartyId  = $request->input('receiverId');
        $receiverWalletId = $request->input('receiverWalletId');
        $requestAmount    = $request->input('amount');
        $qrType           = $request->input('qrType');
        $businessModelId  = $request->input('businessModelid');
        $qrId             = $request->input('qrId');
        $transStaus       = config('vwallet.transactionStatus.pending');
        $transBool        = TRUE;
        $transMode        = 'Debit';
        $productEventId   = 7;
        if ($qrType != 'send')
        {
            $productEventId = 6;
        }
        if ($qrType == 'send')
        {
            $senderPartyId    = $request->input('receiverId');
            $senderWalletId   = $request->input('receiverWalletId');
            $receiverPartyId  = $request->input('partyId');
            $receiverWalletId = $request->input('partyWalletId');
            $transMode        = 'Credit';
        }

        if ($qrType != 'standard')
        {
            $qrData = \App\Models\Partyqrcode::select('UsedStatus')->find($qrId);
            if (!$qrData)
            {
                $output_arr['display_msg'] = array('info' => 'Qr Code doesn`t exist.');
                $output_arr['is_error']    = FALSE;
                return response()->json($output_arr);
            }

            if ($qrData->UsedStatus == 'U')
            {
                $output_arr['display_msg'] = array('info' => 'Qr Code already used.');
                $output_arr['is_error']    = FALSE;
                return response()->json($output_arr);
            }

            $expiryTime  = strtotime($request->expiryLimit);
            $currentTime = strtotime(date('Y-m-d H:i:s'));
            if ($expiryTime < $currentTime)
            {
                $output_arr['display_msg'] = array('info' => 'QR code expired.');
                return response()->json($output_arr);
            }
        }

        if ($request->input('vPin'))
        {
            $params     = array('partyId' => $request->input('partyId'), 'vPin' => $request->input('vPin'));
            $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
            if ($verifyVpin === FALSE)
            {
                $output_arr['display_msg'] = array('vpin' => 'Please enter valid V-PIN');
                return json_encode($output_arr);
            }
        }

        if ($senderPartyId == $receiverPartyId)
        {
            $output_arr['display_msg'] = array('partyId' => 'Can`t scan your own QRCode');
            return response()->json($output_arr);
        }

        // session Check
        $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sessionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return $output_arr;
        }

        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($senderPartyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        // Check Per Transaction Limit Exceded or Not
        if ($requestAmount > config('vwallet.PER_TXN_MAX'))
        {
            if ($qrType != 'send')
            {
                $output_arr['display_msg'] = array('amount' => 'You can send max ' . config('vwallet.PER_TXN_MAX') . '.');
            }
            if ($qrType == 'send')
            {
                $output_arr['display_msg'] = array('amount' => 'You can receive max ' . config('vwallet.PER_TXN_MAX') . '.');
            }
            return response()->json($output_arr);
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyInfo->UserWalletID, $partyInfo->WalletPlanTypeId, $requestAmount);
//        print_r($partyTransLimit);
        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // get Beneficiary details if beneficiary Exists.
        $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($senderPartyId, $receiverPartyId, 'eWallet', 'partyId');

        if ($banificiaryInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.benificiartNotFound'));
            return $output_arr;
        }

        // Check Beneficiary is Sub Account or Not
        if (($qrType != 'send') && ($banificiaryInfo->IsSubAccount == 'Y'))
        {
            $output_arr['display_msg'] = array('recieverId' => 'You can`t send to this mobile');
            return response()->json($output_arr);
        }

        // Check Beneficiary is Sub Account or Not
        if (($qrType == 'send') && ($partyInfo->IsSubAccount == 'Y'))
        {
            $output_arr['display_msg'] = array('recieverId' => 'You can`t receive from to this mobile');
            return response()->json($output_arr);
        }

        // calling YesBank Api
        $SendermobileNumber = $partyInfo->MobileNumber;

        $yesBankApi = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($SendermobileNumber);

        if ($yesBankApi['is_error'] == TRUE)
        {
            return response()->json($yesBankApi);
        }
        $yesBankApiData = $yesBankApi['res_data'];
        if ($yesBankApiData['remaining_load_limit'] < $requestAmount)
        {
            $output_arr['display_msg'] = array('info' => 'You Benificiary Exceeded Your Monthly Limit');
            return $output_arr;
        }

        $receiverMobileNumber = $banificiaryInfo->MobileNumber;
        $yesBankApiBenficiary = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($receiverMobileNumber);
        if ($yesBankApiBenficiary['is_error'] == TRUE)
        {
            return response()->json($yesBankApiBenficiary);
        }
        $yesBankApiBenfData = $yesBankApi['res_data'];
        if ($yesBankApiBenfData['remaining_load_limit'] < $requestAmount)
        {
            $output_arr['display_msg'] = array('info' => 'You Benificiary Exceeded Your Monthly Limit');
            return $output_arr;
        }

        if (($qrType == 'send') && ($requestAmount > $banificiaryInfo->CurrentAmount))
        {
            $output_arr['display_msg'] = array('amount' => 'Beneficiary wallet has no sufficient amount to transfer');
            response()->json($output_arr);
        }

        if (($qrType != 'send') && ($requestAmount > $partyInfo->CurrentAmount))
        {
            $output_arr['display_msg'] = array('amount' => 'Your wallet has no sufficient amount to transfer');
            return response()->json($output_arr);
        }

        // code to check min amount for transaction
        if ($requestAmount < config('vwallet.W2W_MIN_AMOUNT'))
        {
            $output_arr['display_msg'] = array('info' => 'Min Amount Of Rs.' . config('vwallet.W2W_MIN_AMOUNT') . ' Is Required To Transfer');
            response()->json($output_arr);
        }

        // check Benifisary transction or wallet credit limits 
        $benificaryTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($banificiaryInfo->UserWalletID, $banificiaryInfo->WalletPlanTypeId, $requestAmount, TRUE);
        if ($benificaryTransLimit['error'])
        {
            $output_arr['display_msg'] = array('info' => $benificaryTransLimit['info']);
            $transStaus                = 'Failed';
            $transBool                 = FALSE;
        }

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);

        // fee calculation 
        $fee = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $requestAmount, $businessModelId, $senderPartyId);

//        print_r($fee); exit;
        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelId, $paymentProduct, $requestAmount, $fee, $partyInfo, $banificiaryInfo, $transStaus, $sessionId);
        if ($qrType != 'standard')
        {
            // Update QR Code Used Status
            $this->_changeQrCodeStatus($qrId);
        }

        $transactionDetails = $transRes['response']['TransactionDetails'];
        $transactionDetails['AccountName']             = $banificiaryInfo->FirstName . ' ' . $banificiaryInfo->LastName;
        $transactionDetails['BeneficiaryMobileNumber'] = $request->input('mobileNumber');
        \App\Libraries\Helpers::_sendMoneyCommunication($request, $partyInfo, $banificiaryInfo, $transactionDetails);

        if ($transRes['error'])
        {
            $output_arr['display_msg'] = array('info' => $transRes['info']);
            return $output_arr;
        }
        // get Logged in User data if Exists.       
        $retunInfo                       = \App\Libraries\TransctionHelper::getWalletInfo($request->input('partyId'));
        $retunInfo['TransactionDetails'] = $transRes['response']['TransactionDetails'];
        $output_arr['res_data']          = $retunInfo;
        $output_arr['is_error']          = FALSE;
        return $output_arr;
    }

    private function _validateSendRequestMoney($request)
    {
        $validations = array(
            'qrType'           => 'required|in:standard,send,request',
            'partyId'          => 'required|numeric|digits_between:1,10',
            'partyWalletId'    => 'required|numeric|digits_between:1,10',
            'receiverId'       => 'required|numeric|digits_between:1,10',
            'receiverWalletId' => 'required|numeric|digits_between:1,10',
            'amount'           => 'required|numeric',
            'vPin'             => 'digits:4',
            'businessModelid'  => 'required|numeric',
            'transType'        => 'required|in:WW',
        );
        if (($request->input('qrType') != '') && ($request->input('qrType') != 'standard'))
        {
            $validations['qrId']           = 'required|numeric|digits_between:1,10';
            $validations['initiationTime'] = 'required|date_format:Y-m-d H:i:s';
            $validations['expiryLimit']    = 'required|date_format:Y-m-d H:i:s';
        }

        $validate_response = \Validator::make($request->all(), $validations);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _changeQrCodeStatus($qrId)
    {
        $updateQrStatus             = \App\Models\Partyqrcode::find($qrId);
        $updateQrStatus->UsedStatus = 'U';
        $updateQrStatus->save();
    }

    public function viewQrTransactions(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateViewQrTransactions($request, TRUE);

        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $qrTransactionsList = \App\Models\Partyqrcode::join('userwallet', 'userwallet.UserWalletID', '=', 'partyqrcode.ReceiverPartyID', 'left')
                        ->where('partyqrcode.SenderPartyID', '=', $request->input('partyId'))
                        ->orderBy('partyqrcode.PartyQRCodeId', 'DESC')
                        ->get(array('partyqrcode.PartyQRCodeId', 'partyqrcode.Amount', 'partyqrcode.InitiatedDTTM', 'partyqrcode.ExpiryDTTM', 'partyqrcode.UsedStatus', 'partyqrcode.ReceiverMobNum', 'partyqrcode.ReceiverPartyID', 'partyqrcode.QRType', 'partyqrcode.UsedDTTM', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName'))->toArray();
        if (count($qrTransactionsList) <= 0)
        {
            $output_arr['display_msg'] = array('info' => 'No records found.');
            return response()->json($output_arr);
        }

        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $qrTransactionsList;
        return response()->json($output_arr);
    }

    private function _validateViewQrTransactions($request)
    {
        $validations = array(
            'partyId' => 'required|numeric|digits_between:1,10',
        );

        $validate_response = \Validator::make($request->all(), $validations);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function addBenficiary($request, $AccountName, $userWalletId, $beneficiaryUserId, $benfType, $YbBneficiaryId, $saveBenf)
    {
        $insertBeneficiryData = array(
            'BeneficiaryWalletID'      => $beneficiaryUserId,
            'UserWalletID'             => $userWalletId,
            'BeneficiaryStatus'        => 'N',
            'BeneficiaryType'          => $benfType,
            'BeneficiaryMobileNumber'  => ($request->input('mobileNumber')) ? $request->input('mobileNumber') : '',
            'WalletBeneficiaryName'    => $request->input('nickName'),
            'isTripleClickUser'        => 'N',
            'BankIFSCCode'             => ($request->input('ifscCode')) ? $request->input('ifscCode') : '',
            'MMID'                     => '',
            'AccountName'              => ($AccountName == '') ? $AccountName : '',
            'AccountNumber'            => ($request->input('accountNo')) ? $request->input('accountNo') : '',
            'BeneficiaryVPA'           => '',
            'CreatedDateTime'          => date('Y-m-s H:i:s'),
            'yesBankBeneficiaryId'     => $YbBneficiaryId,
            'yesBankBeneficiaryStatus' => 'Y',
            'BeneficiaryStatus'        => $saveBenf
        );

        $insertBeneficiry = \App\Models\Userbeneficiary::insertGetId($insertBeneficiryData);
        return $benId            = $insertBeneficiry;
    }

}
