<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bookingdetail
 * 
 * @property int $PassengerId
 * @property int $BookingMasterId
 * @property string $PassengerName
 * @property int $Age
 * @property string $IdType
 * @property string $IdNumber
 * @property string $IsPrimary
 * @property string $BookingStaus
 * @property string $Gender
 * @property string $SeatNum
 * @property string $MobileNumber
 * @property string $Email
 * @property string $AlternateMobileNumber
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Bookingmaster $bookingmaster
 * @property \Illuminate\Database\Eloquent\Collection $bookingcancellations
 *
 * @package App\Models
 */
class Bookingdetail extends Eloquent
{
	protected $primaryKey = 'PassengerId';
	public $timestamps = false;

	protected $casts = [
		'BookingMasterId' => 'int',
		'Age' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BookingMasterId',
		'PassengerName',
		'Age',
		'IdType',
		'IdNumber',
		'IsPrimary',
		'BookingStaus',
		'Gender',
		'SeatNum',
		'MobileNumber',
		'Email',
		'AlternateMobileNumber',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bookingmaster()
	{
		return $this->belongsTo(\App\Models\Bookingmaster::class, 'BookingMasterId');
	}

	public function bookingcancellations()
	{
		return $this->hasMany(\App\Models\Bookingcancellation::class, 'PassengerId');
	}
}
