<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Virtualcard
 * 
 * @property int $id
 * @property int $idCardUsers
 * @property int $idCardType
 * @property string $CardType
 * @property string $CardNumber
 * @property string $ShortCardNumber
 * @property string $CVV
 * @property string $CardExpiry
 * @property string $Nameoncard
 * @property float $Balance
 * @property string $CurrencyCode
 * @property string $Images
 * 
 * @property \App\Models\Cardtype $cardtype
 * @property \App\Models\Carduser $carduser
 *
 * @package App\Models
 */
class Virtualcard extends Eloquent
{
	protected $table = 'virtualcard';
	public $timestamps = false;

	protected $casts = [
		'idCardUsers' => 'int',
		'idCardType' => 'int',
		'Balance' => 'float'
	];

	protected $fillable = [
		'idCardUsers',
		'idCardType',
		'CardType',
		'CardNumber',
		'ShortCardNumber',
		'CVV',
		'CardExpiry',
		'Nameoncard',
		'Balance',
		'CurrencyCode',
		'Images'
	];

	public function cardtype()
	{
		return $this->belongsTo(\App\Models\Cardtype::class, 'idCardType');
	}

	public function carduser()
	{
		return $this->belongsTo(\App\Models\Carduser::class, 'idCardUsers');
	}
}
