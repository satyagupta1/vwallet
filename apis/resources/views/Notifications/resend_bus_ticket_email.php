<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Resend Booking Confirmation Mail/SMS
 * 
 *  param $fullname varchar
 *  param $amount varchar
 * 
 *  param $boardingPointLocation varchar
 *  param $busType varchar
 *  param $landmark varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullName) ? $salutation.' '.$fullName : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaWallet Bus booking: Ticket for '.$from.' - '.$to.'.',
        'text'    => $salutationName . ',<br/><br/>
        Your Bus ticket booking is confirmed from '.$from.' to '.$to.', <br/>
        Ticket Number: '.$ticketNo.', <br/>
        Bus Type: '.$busType.', <br/>
        Land Mark: '.$landmark.', <br/>
        Boarding Point: '.$boardingPointLocation.', <br/>
        Seat no.: '.$passengerDetails.', <br/>
        <a href="'.$printUrl.'" target="_blank" style="color:#68c5df;text-decoration:none;font-weight:500;font-size: 12px;">PRINT</a>'],
    'sms'   => [
    'text' => 'TKT No '.$ticketNo.' Bording Point: ' . $boardingPointLocation . ', '.$from.'-'.$to.', SRVC NO:'
        .$srvceno.',Seats:'.$passengerDetails.',Fare:'.$fare.', DOJ : '.$doj,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Bus Ticket From ' . $from . ' to ' . $to . ' confirmed ',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => 'Bus booking confirmed. Ticket number: ' . $ticketNo,
    ]
];

echo json_encode($array);

/* End of file resend_bus_ticket_email.php */
