<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletaccountcard
 * 
 * @property int $CardID
 * @property string $CardType
 * @property string $SerialNumber
 * @property string $DeliveryAddressID
 * @property string $Status
 * @property string $RequestDate
 * @property string $DeliveredDate
 * @property string $ReceivedDate
 * @property string $CardEffectiveDate
 * @property string $CardExpiryDate
 * @property string $CardLimit
 * @property string $WalletAccountID
 * @property string $Credit
 *
 * @package App\Models
 */
class Walletaccountcard extends Eloquent
{
	protected $table = 'walletaccountcard';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'CardID' => 'int'
	];

	protected $fillable = [
		'CardType',
		'SerialNumber',
		'DeliveryAddressID',
		'Status',
		'RequestDate',
		'DeliveredDate',
		'ReceivedDate',
		'CardEffectiveDate',
		'CardExpiryDate',
		'CardLimit',
		'Credit'
	];
}
