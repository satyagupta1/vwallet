<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletrecharge
 * 
 * @property int $TransactionID
 * @property int $WalletAccountId
 * @property int $UserWalletId
 * @property string $MoneySourceCode
 * @property string $CardVendor
 * @property int $CardNumber
 * @property string $CardExpiryDate
 * @property int $CardCVVNumber
 * @property string $NetBankCode
 * @property string $SaveCardReference
 * @property int $WalletRequestBeneficiaryID
 * @property float $RequestedAmount
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Moneysource $moneysource
 * @property \App\Models\Transaction $transaction
 *
 * @package App\Models
 */
class Userexternalcard extends Eloquent
{
	protected $table = 'userexternalcard';
	protected $primaryKey = 'UserExternalCardID';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletId',
		'MoneySourceCode',
		'CardVendor',
		'CardNumber',
		'CardExpiryDate',
		'NetBankCode',
		'ActiveIndicator',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
                'CardHolderName'
	];

	public function moneysource()
	{
		return $this->belongsTo(\App\Models\Moneysource::class, 'MoneySourceCode');
	}

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'TransactionID');
	}
}
