<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userstandinginstruction
 * 
 * @property int $UserWalletId
 * @property string $IsActive
 * @property string $TransferPartyType
 * @property int $BeneficiaryOrVendorID
 * @property float $TransactionAmount
 * @property \Carbon\Carbon $StartDateTime
 * @property \Carbon\Carbon $EndDateTime
 * @property string $OneTime
 * @property string $Recurrence
 * @property string $RecurrencePattern
 * @property int $PatternRepeatNumofDays
 * @property string $PatternDailyWeekend
 * @property string $PatternWeeklyDay
 * @property int $PatternMonthlyDay
 * @property int $RangeNumberofOccurrences
 * @property \Carbon\Carbon $RangeEndDate
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userstandinginstruction extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletId' => 'int',
		'BeneficiaryOrVendorID' => 'int',
		'TransactionAmount' => 'float',
		'PatternRepeatNumofDays' => 'int',
		'PatternMonthlyDay' => 'int',
		'RangeNumberofOccurrences' => 'int'
	];

	protected $dates = [
		'StartDateTime',
		'EndDateTime',
		'RangeEndDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletId',
		'IsActive',
		'TransferPartyType',
		'BeneficiaryOrVendorID',
		'TransactionAmount',
		'StartDateTime',
		'EndDateTime',
		'OneTime',
		'Recurrence',
		'RecurrencePattern',
		'PatternRepeatNumofDays',
		'PatternDailyWeekend',
		'PatternWeeklyDay',
		'PatternMonthlyDay',
		'RangeNumberofOccurrences',
		'RangeEndDate',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletId');
	}
}
