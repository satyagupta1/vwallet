<?php

/**
* A Functions file which has all the common functions for the project
* @category  PHP
* @package   Communication
* @author    Viola Services (India) PVT LTD <development@violamoney.com>
* @license   https://violamoney.com Licence
*/

namespace App\Libraries\Communication;

// use Models
use App\Models\Notificationlog;
use App\Models\Notificationlogparameters;

/**
* A Functions class
* @category  PHP
* @package   Communication
* @author    Viola Services (India) PVT LTD <development@violamoney.com>
* @license   https://violamoney.com Licence
*/

class Functions 
{
    /**
    * The Method isJson will use to cehck if the string is JSON or not?
    * @param $string string; the JSON string which we need to check.
    * 
    * @return bool
    */
    public static function isJson($string)
    {
        return is_string($string) && is_array(json_decode($string, TRUE)) ? TRUE: FALSE;
    }

    /**
    * The static method createCommunicationLog will to insert log record into databse. Right now we have only tables to keep records for the communication.
    * 
    * @param $notification_log array; The array which has all the data need to insert into database.
    * @param $notification_logparameter array; The array which has all the data for logparameter table.
    * 
    * @return $isertID numeric; the database last inserted ID of the record.
    */
    public static function createCommunicationLog($notification_log, $notification_logparameter = 1)
    {
        $isertID = Notificationlog::insertGetId($notification_log);

        // add the notification ID into $notification_logparameter array;
        $notification_logparameter['NotificationID'] = $isertID;
        Notificationlogparameters::insertGetId($notification_logparameter);

        return TRUE;
    }
}

?>
