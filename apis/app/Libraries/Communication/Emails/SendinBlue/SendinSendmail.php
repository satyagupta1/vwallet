<?php

    namespace App\Libraries\Emails\SendinBlue;

    

/**
 * Created by PhpStorm.
 * User: mahendravikhar
 * Date: 20/02/17
 * Time: 3:40 PM
 */

// namespace Mailgun;

require_once('V2.0/Mailin.php');

class SendinSendmail extends Mailin
{

    /**
     * @var null|string
     */
     public $api_key = '6RNvwJgCmVDqKOf2';

    /**
     * @var null|string
     */
     public $base_url = 'https://api.sendinblue.com/v2.0';

    /**
     * @var null|string
     */
     public $timeout = '30000';


    /**
    * @var null|string;
    */
    protected $smtp_server = 'smtp-relay.sendinblue.com';

    /**
    * @var null|string;
    */
    protected $port = 587;

    /**
    * @var null|string;
    */
    protected $login = 'webadmin@indorehemophilia.org.in';

    /**
    * @var null|string;
    */
    protected $login_name = 'Hemophilia Society Indore';

    /**
    * @var null|string;
    */
    protected $password = '6RNvwJgCmVDqKOf2';

    /**
    * @var null|string;
    */
    protected $x_mailin_custom = '';

    /**
    * @var null|string;
    */
    protected $x_mailin_ip = '';

    /**
    * @var null|string;
    */
    protected $x_mailin_tag = '';

    /**
     *  This function allows the sending of a fully formed message OR a custom
     *  MIME string. If sending MIME, the string must be passed in to the 3rd
     *  position of the function call.
     *
     * @param array $final_data
     *
     * @return string;
     */

    function ssmtp_mail($final_data) {
        $data = array(
              "to" => array($final_data['to_email']=>$final_data['to_name']),

              "from" => array($this->login, $this->login_name),
              "replyto" => array($this->login, $this->login_name),

              "subject" => $final_data['subject'],
        			"html" => $final_data['mail_body'],
        			"attachment" => array(),
        			"headers" => array(
                "Content-Type"=> "text/html; charset=iso-8859-1",
                "X-param1"=> "value1",
                "X-param2"=> "value2",
                "X-Mailin-custom"=>$this->x_mailin_custom,
                "X-Mailin-IP"=> $this->x_mailin_ip,
                "X-Mailin-Tag" => $this->x_mailin_tag
              ),
        );

        $result = $this->send_email($data);
        return $result;
    }
}
