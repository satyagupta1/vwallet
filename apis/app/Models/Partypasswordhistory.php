<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partypasswordhistory
 * 
 * @property int $PartyID
 * @property string $Password
 * @property string $EffectiveDate
 * @property string $ExpiryDate
 * @property string $Active
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partypasswordhistory extends Eloquent
{
	protected $table = 'partypasswordhistory';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
                'PartyID',
		'SecretKey',
		'SecretKeyType',
		'ExpiryDate',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
}
