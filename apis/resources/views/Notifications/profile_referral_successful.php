<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Referral Successful template
 * 
 *  param $senderName varchar
 *  param $receiverName varchar
 *  param $ammount decimal
 *  param $referalCode varchar  
 */

$salutation = trans('messages.salutation');
$salutationName = ($senderName) ? $salutation . ' ' . $senderName : $salutation;
$array = [
    'email' => [
        'sender'   => [
            'subject' => 'Referral Bonus',
            'text'    => $salutationName . ', <br/><br/>'
            . 'Congratulations! ' . $receiverName . ' has registered with your referral code.<br/><br/>
We have sent you a referral bonus for referring a friend to Viola. Your  reward bonus of INR ' . $ammount . ' has been added to your account<br/><br/>
Continue referring friends and family and earn money.'],
        'receiver' => [
            'subject' => 'Congratulations! Welcome to Viola Family',
            'text'    => 'Hi ' . $receiverName . ', <br/><br/>

                        Thank you for joining Viola Family. Share your referral code now to enjoy referral bonus.<br/>
                        ' . $referalCode . '<br/>
                        Have friends/ Family wh`re not on ViolaWallet?<br/>
                        Refer them and earn money.',
        ],
    ],
    'sms'   => [
        'sender'   => [
            'text' => 'Hi ' . $senderName . ', Congratulations! ' . $receiverName . ' has registered with your referral code. Referral bonus has been added to your account',
        ],
        'receiver' => [
            'text' => 'Hi ' . $receiverName . ', Thank you for joining Viola Family. Referral bonus has been added to your account. Enjoy ViolaWallet!',
        ],
    ],
    'push'  => [
        'sender'   => [
            'title'              => '',
            'body'               => '',
            'summaryText'        => '',
            'notificationType'   => 'bigTextStyle',
            'screenNavigationId' => 'general'
        ],
        'receiver' => [
            'title'              => '',
            'body'               => '',
            'summaryText'        => '',
            'notificationType'   => 'bigTextStyle',
            'screenNavigationId' => 'general'
        ],
    ],
    'web'   => [
        'sender'   => [
            'text' => '',
        ],
        'receiver' => [
            'text' => '',
        ],
    ],
];

echo json_encode($array);

/* End of file profile_referral_successful.php */
