<?php

/**
 * LogOutController file
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

/**
 * LogOutController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-card.com Licence
 */
class LogOutController extends Controller {

    // we will create new method
    public function partyLogout(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $chk_valid  = $this->validationCheck($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }

        $partyId       = $request->input('partyId');
        $device_type   = $request->header('device-type');
        $ip_add        = $request->header('ip-address');
        $session_token = $request->header('session-token');
        $device_uid    = $request->header('device-uid');
        $where         = array(
            'PartyID'   => $partyId,
            'SessionID' => $session_token
        );

        if ( $device_type != 'W' )
        {
            $where['DeviceUniqueID'] = $device_uid;
            $where['SessionID']      = $session_token;
        }
        $party                     = \App\Models\Partyloginhistory::select('PartyLoginHistoryId')
                ->where($where)
                ->orderBy('PartyLoginHistoryId', 'DESC')
                ->first();               
        $output_arr['display_msg'] = array( 'info' => trans('messages.sessionExpired'));

        if ( $party && $party->PartyLoginHistoryId > 0 )
        {
            $datetime                = date('Y-m-d H:i:s');
            $update_device           = array( 'SessionEndTime' => $datetime );
            $results                 = \App\Models\Partyloginhistory::find($party->PartyLoginHistoryId);
            $results->SessionEndTime = $datetime;
            $results->save();
        }
        if ( $results )
        {
            $output_arr['is_error']    = FALSE;
            $attrLogout = trans('messages.attributes.logout');
            $output_arr['display_msg'] = array( 'info' => trans('messages.success', ['name' => $attrLogout]));
        }
        return response()->json($output_arr);
    }

    public function validationCheck($request)
    {
        $validate = [
            'partyId' => 'required|numeric',
        ];
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
