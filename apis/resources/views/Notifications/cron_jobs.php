<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback Received
 * 
 *  param $amount decimal
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullName) ? $salutation.' '.$fullName : $salutation;
$array = [
    'email' => [
        'subject' => 'Cron Job Executed - '.$title,
        'text'    => $salutationName . ',<br/><br/>
            Cronjob Executed:
            Time - '.$date.' '.$time.'<br/><br/>
            Title - '.$title.'<br/>
            Body - '.$description.'<br/>   
<br/><br/>Team ViolaWallet<br/>'.$baseUrl,
    ],
    'sms'   => [
        'text' => 'Cronjob Executed Title - '.$title.' ',
    ],
    'push'  => [
        'title'              => $title,
        'body'               => $description,
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'cronjob'
    ],
    'web'   => [
        'text' => 'Cronjob Executed Title - '.$title.' ',
    ],
];

echo json_encode($array);

/* End of file cashback_received */
