<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>The Refer and Earn Program is only valid on new installs and sign ups on mobile apps.</li>
        <li>Your friends must not have installed the ViolaWallet app on any of their device before.</li>
        <li>The Email ID and/or phone number through which the friend signs up with ViolaWallet app, must not have been used for signing up with the ViolaWallet app earlier.</li>
        <li>A sign up with a different email address or phone number using the same device will not qualify as a valid referral.</li>
        <li>The mobile number provided by the friend to receive the OTP must not have been used to sign up earlier.</li>
        <li>The device on which the friend downloads the ViolaWallet app should not be rooted or jail-broken.</li>
        <li>The mobile number provided by the friend to receive the OTP must be an Indian mobile number as this program is not valid for users based out of India.</li>
        <li>If your friends use someone else's referral code/link, the person whose link was used would get benefits, even though you had referred them first. The last link used to install the app by your friend would be considered for the referral cashback.</li>
        <li>Cashback earned through referral program will fall under cashback in the ViolaWallet.</li>
        <li>Cashback earned using the referral program will have a validity of 90 days from the date of credit. If unutilized, it will cease to exist and will not be renewed under any circumstances.</li>
        <li>You can earn Rs. 50 per person you refer.</li>
        <li>You will earn Rs. 50 Cashback instantly in ViolaWallet for everyone (as per the terms above) who installs the app using your unique referral link and makes a bill payment/recharge successfully.</li>
        <li>On download, your friends (first time users) will earn Rs. 50 Cashback instantly in ViolaWallet after successful Recharge/Bill Payment.</li>
        <li>You can refer any number of people, but the maximum amount you can earn using this referral program is Rs.1000.</li>
        <li>In case the Cashback is not credited to your ViolaWallet, please write to support.violamoney.com</li>
    </ul>
    <h5>Cashback validity and conditions</h5>
    <ul>
        <li>Cashback earned through referral program can be used for Bill Payments and Recharges only using the ViolaWallet mobile apps for Android and iOS.</li>
        <li>Validity of Cashback earned through the referral program will be 90 days from the day the it is credited to the ViolaWallet.</li>
        <li>The maximum amount of Cashback that can be used for each transaction will be as per wallet T&Cs.</li>
        <li>Cashback cannot be transferred to your bank account or any other wallet.</li>
        <li>Usage conditions of cashback may change at the discretion of ViolaWallet, at any point in time.</li>
        <li>Your unique referral links should only be used for personal and non-commercial purposes. You cannot distribute/publish your unique referral link where there is no reasonable basis for believing that most of the recipients are personal friends (such as coupon websites, forums, Wikipedia etc.)</li>
    </ul>
    <h5>Termination and change</h5>
    <ul>
        <li>ViolaWallet reserves all rights to change the amounts conferred under Refer and Earn program at any point in time.</li>
        <li>ViolaWalletmay suspend or terminate the Refer and Earn program or any user's ability to participate in the program at any time for any reason at their discretion. Cashback earned as a result of fraudulent activities will be revoked and deemed invalid.</li>
    </ul>
    <h5>Update to the Terms and Conditions</h5>
    <ul>
        <li>ViolaWallet reserves the right to amend these terms and conditions at any time without any prior notice. </li>
        <li>Modifications of these terms will be effective from the time they are updated in the Terms and Conditions section.</li>
    </ul>
</div>
<?php include 'footer.php';?>