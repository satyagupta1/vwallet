<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Funds received Successfully with transaction details template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $beneficiaryName varchar
 *  param $dateTime varchar 
 *  param $transactionId varchar 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($receiverName) ? $salutation . ' ' . $receiverName : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction completed
',
        'text'    => $salutationName . ',<br/><br/>
INR ' . $amount . '  was received from ' . $senderName . ' on ' . $date . ' '.$time . ' 
 Transaction Details <br/>Transaction ID:' . $transactionId . ''
        . '<br/> Total Amount:' . $amount . ' <br/>Datetime: ' . $date . ' '.$time],
    'sms'   => [
        'text' => ' 
Hello ' . $receiverName . ',
INR ' . $amount . '  was received from ' . $senderName . ' on ' . $date . ' '.$time . '. '
        . 'Transaction Details 
        Transaction ID:' . $transactionId . ' 
        Balance:' . $balanceAmount . '.

   ',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Funds received successfully',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Funds Received. Amount : '.$amount.' Available balance : '.$balanceAmount,
    ],
];

echo json_encode($array);

/* End of file send_money_beneficiary_funds_received_tranx.php */
