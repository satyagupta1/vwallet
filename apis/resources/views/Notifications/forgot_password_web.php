<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Forgot Password on Web template
 * 
 *  param $changedDate date
 *  param $changedTime time
 *  param $fullname varchar           
 *  param $supportURL varchar 
 */


$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Viola Password updated',
        'text'    => $salutationName . ', <br/><br/>
    Forgot your Password??<br/>
    We received a request that you changed your password on '.$changedDate.', at '.$changedTime.'<br/>
    If you didn`t make this request, your account might be compromised. To get back into your account, you`ll need to reset your credentials.'],
    'sms'   => [
        'text' => 'Hello '.$fullname.', You have reset your password on '.$changedDate.', at '.$changedTime.'.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your Password has been changed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file forgot_password_web.php */
