<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>System should identify first successful transaction on services such as Recharge/Bill Pay with a minimum value of INR 100.</li>
        <li>System should verify if the user performing the above transaction is referred by another user.</li>
        <li>System should identify the referral code and track the Referrer User.</li>
        <li>System should credit INR 50 each in both Referrer and the Referred user wallets.</li>
    </ul> 
</div>
<?php include 'footer.php';?>