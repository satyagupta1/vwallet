<?php

namespace App\Libraries\Communication\Smss;

use App\Libraries\Communication\Smss\Smshub\SmsHubLib;

/**
 * Created by PhpStorm.
 * User: mahendravikhar
 * Date: 11/02/17
 * Time: 11:53 PM
 */
class SmsNow
{
    public static function sendSms($sms_data){
        $email_status = SmsHubLib::sendSmsNow($sms_data);
        return $email_status;
    }
}