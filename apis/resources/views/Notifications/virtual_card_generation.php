<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Virtual Card Generation template
 * 
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Welcome to ViolaWallet !',
        'text'    => $salutationName . ',<br/><br/> 
                        Thank you for creating ViolaWallet account, your virtual Card is generated. We will send you an OTP to your registered mobile number to ensure it`s you. Enjoy using ViolaWallet.',
    ],
    'sms'   => [
        'text' => 'Hello! ' . $fullname . ' Enjoy ViolaWallet, your virtual Card is generated. We will send you an OTP to your registered mobile number to ensure it`s you.',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Hello! ' . $fullname . ' Enjoy ViolaWallet, your virtual Card is generated.',
    ],
];

echo json_encode($array);

/* End of file virtual_card_generation.php */
