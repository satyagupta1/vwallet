<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Offer is valid till 31st January, 2017 (“Offer Period”).</li>
        <li>Users will get Rs 50 cashback on applying Promo code TOPUP1000 on every topup/load money transaction on ViolaWallet application during offer period.</li>
        <li>Offer applicable on minimum load money of Rs 1000, with a cashback of Rs. 50.</li>
        <li>Offer can be redeemed only three times during the Offer Period, with one registered mobile number.</li>
        <li>Cashback will be auto-credited to User's ViolaWallet within 48 hours from the date of successful transaction.</li>
        <li>In case User does not receive the cashback within 48 hours from the time of transaction or for any cashback related query, User should write to support@violamoney.com</li>
        <li>Cashback amount cannot be transferred to any other person or to any bank account.</li>
        <li>Users shall take sole responsibility for privacy and confidentiality of their ViolaWallet account and password.</li>
        <li>Viola reserves the right to add, modify, or discontinue the terms and conditions of this offer at any time without prior notice, at its sole discretion.</li>
        <li>In no event the aggregate liability of Viola shall exceed the amount of cashback, the User is entitled for, under this Offer.</li>
        <li>Unlawful modification or abuse of the Offer is prohibited.</li>
        <li>Viola disclaims all third party liabilities that may arise from or in connection with this offer. This Offer is subject to force majeure terms.</li>
    </ul> 
</div>
<?php include 'footer.php';?>