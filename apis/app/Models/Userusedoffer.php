<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userusedoffer
 * 
 * @property int $UserUsedOffersId
 * @property int $UserWalletID
 * @property int $OfferID
 * @property \Carbon\Carbon $UsedDateTime
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userusedoffer extends Eloquent
{
	protected $primaryKey = 'UserUsedOffersId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserUsedOffersId' => 'int',
		'UserWalletID' => 'int',
		'OfferID' => 'int'
	];

	protected $dates = [
		'UsedDateTime',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'OfferID',
		'UsedDateTime',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
