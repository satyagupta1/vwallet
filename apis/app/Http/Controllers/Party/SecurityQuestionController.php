<?php

namespace App\Http\Controllers\Party;

use App\Http\Controllers\Controller;

/**
 * PartySecurityController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class SecurityQuestionController extends Controller {

    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');
    }

    /*
     *  List Security Question Controller
     *  @Desc It will give all Security Question list, 
     *  with filter by security question Question Category, IsStatusActive.and Question Id.
     *  @param Request $request
     *  @return JSON
     */

    public function getSecurityQuestionList(\Illuminate\Http\Request $request)
    {
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateFields($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $quesionCategory = $request->input('questionCategory');
        $status          = $request->input('status');
        $orderBy         = $request->input('orderBy');
        $orderByColumn   = $request->input('orderColumn');
        $limit           = $request->input('limit');
        $paginate        = $request->input('paginate');


        $selectColumns = array(
            'QuestionId',
            'QuestionDetails',
            'QuestionCategory',
            'IsStatusActive',
            'CreatedDateTime',
        );

        
        $query = \App\Models\Securityquestion::select($selectColumns);
        //filter by security question type
        if ( $quesionCategory !== NULL )
        {
            $query->where('QuestionCategory', '=', $quesionCategory);
        }

        //filter by triple click
        if ( $status !== NULL )
        {
            $query->where('IsStatusActive', '=', $status);
        }


        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectColumns) )
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('QuestionId', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $finalRecords = $query->paginate($this->perpage)->toArray();
            $countResults = $finalRecords['total'];
        }
        else
        {
            $finalRecords = $query->get()->toArray();
            $countResults = count($finalRecords);
        }

        if ( $countResults > 0 )
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = '';
            $output_arr['res_data']    = $finalRecords;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /**
     * Validate Form Fields Data
     * @param $request
     * @return String
     */
    private function _validateFields($request)
    {
        $validate = [];
        if ( $request->input('questionCategory') !== NULL )
        {
            $validate['questionCategory'] = 'in:All,User,Merchant';
        }

        if ( $request->input('status') !== NULL )
        {
            $validate['status'] = 'required|in:Y,N';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
