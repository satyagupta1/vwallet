<?php

/**
 * UserProfileController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-wallet.com Licence
 */

namespace App\Http\Controllers\Ccteam;
use App\Libraries\Helpers;
use App\Libraries\Crypt\Encryptor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

/**
 * UserProfileController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-wallet.com Licence
 */
class UserProfileController extends Controller {

    /**
     * regDetail
     * @param Request $request in post data
     * @return JSON
     */
    public function actregDetail(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = '');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
            $typeReg = $request->input('regType');
            
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
           


            $from_result = \App\Models\Userwallet::select('userwallet.UserWalletID','userwallet.FirstName',
                    'userwallet.MiddleName','userwallet.LastName','userwallet.MobileNumber',
                    'userwallet.ViolaID','userwallet.EmailID','userwallet.Gender','userwallet.CreatedDateTime',
                    'userwallet.DateofBirth','userwallet.ProfileStatus','userwallet.IsVerificationCompleted',
                    'userwallet.PreferredPrimaryCurrency','userwallet.PreferredSecondaryCurrency',
                    'userwallet.IsPriorityUser','userwallet.IsSubAccount','userwallet.SocialMediaRegistration',
                    'userwallet.WalletPlanTypeId','userwallet.ProfileImagePath','ur.RegisteredReferralCode',
                    'ur.MobileVerified','ur.EmailVerified','ur.RegistrationCountryCode','ur.WalletCountryCode',
                    'ur.TermsAccepted','ur.TermsAcceptedDateTime','pa.AddressTypeCode','pa.AddressPartyType',
                    'pa.PrimaryAddress','pa.AddressLine1','pa.AddressLine2','pa.City','pa.State','pa.Country','pa.PostalZipCode',
                    'pa.PostalZipCode','userwallet.CreatedBy','wa.WalletAccountNumber','wa.WalletCurrencyCode',
                    'wa.AvailableBalance','wa.CurrentAmount','wa.WalletAccountId')
            ->join('userregistration as ur', 'userwallet.UserWalletID', '=', 'ur.UserWalletID')
            ->join('partyaddress as pa', 'userwallet.UserWalletID', '=', 'pa.PartyID')
            ->join('walletaccount as wa', 'userwallet.UserWalletID', '=', 'wa.UserWalletId')
         //    ->join('partydocument as pd', 'userwallet.UserWalletID', '=', 'pd.PartyID')
            ->where([$where, ['userwallet.ProfileStatus', '!=', 'N'],['wa.WalletAccountId', '!=', '1']])->first();

//print_r($from_result);
            if ( $from_result )
            {
                // Check kyc limit
              //  $kycLimit = Helpers::kycLimit($from_result->kycIDPStatus, $from_result->kycADDStatus);

                // Check kyc status
                //$kycstatus = Helpers::kycStatus($from_result->kycIDPStatus, $from_result->kycADDStatus);

              
                if ( $from_result->ProfileStatus == 'A' )
                {
                    $cardstatus = 'Active';
                }
                elseif ( $from_result->cardstatus == 'D' )
                {
                    $cardstatus = 'Deactive';
                }
                 elseif ( $from_result->cardstatus == 'F' )
                {
                    $cardstatus = 'Frozen';
                }
                
                $arrayChecklogin = array(
                    'W' => 'Website',
                    'F' => 'Facebook',
                    'T' => 'Twitter',
                    'G' => 'Google'
                    
                );
                // code to get currency symbol
                $get_currency_symbol = \App\Models\Currency::select('CurrencySymbol')->where('CurrencyCode',$from_result->WalletCurrencyCode)->first();
                
                // code to get kyc status
                $get_kycDocs = \App\Models\Partydocument::select('DocumentID','Status','CategoryCode','DocumentImage')
                        ->where('PartyID',$userid)->get();
              
                $userShaId = sha1($from_result->UserWalletID);
               $LiveUrl = Helpers::profileUrl($userShaId);
                
                $proPic = '';
                if ( ! empty($from_result->ProfileImagePath) )
                {
                    $proPic = $LiveUrl . $userShaId . '/profile/' . $from_result->ProfileImagePath;
                }
                $data_send['custid'] = $from_result->UserWalletID;
                $data_send['fullname'] = $from_result->FirstName.' '.$from_result->MiddleName.' '.$from_result->LastName;
                $data_send['calling_code'] = '91';
                $data_send['MobileNumber'] = $from_result->MobileNumber;
                $data_send['email'] = $from_result->EmailID;
                $data_send['violaid'] = $from_result->ViolaID . '@' . $from_result->Suffix;
                $data_send['isKyc'] = ($from_result->IsVerificationCompleted == 'N')?'No':'yes';
                $data_send['birthdate'] = $from_result->DateofBirth;
                $data_send['accountnum'] = $from_result->PreFix . $from_result->WalletNumber;
                $data_send['status'] = $cardstatus;
                $data_send['Creationdate'] = $from_result->CreatedDateTime;
                $data_send['Address1'] = $from_result->AddressLine1;
                $data_send['Address2'] = $from_result->AddressLine2;
                $data_send['city'] = $from_result->City;
                $data_send['state'] = $from_result->State;
                $data_send['country'] = $from_result->Country;
                $data_send['Postcode'] = $from_result->PostalZipCode;
                $data_send['acCreatedBy'] = $from_result->CreatedBy;
                $data_send['Profile_img'] = $proPic;
                $data_send['registeredSource'] = $arrayChecklogin[$from_result->SocialMediaRegistration];
                $data_send['socialLoginRefno'] = $from_result->SocialLoginRefNo;
                $data_send['referralCode'] = $from_result->RegisteredReferralCode;
                $data_send['primaryWallet'] = array('walletId' => $from_result->WalletAccountId,'walletNumber' => $from_result->WalletAccountNumber, 'availableBalance ' => $from_result->AvailableBalance, 'currentBalance' => $from_result->CurrentAmount,'CurrencyCode' => $from_result->WalletCurrencyCode, 'CurrencySymbol' => $get_currency_symbol->CurrencySymbol);
             $kycArray = array();
                foreach($get_kycDocs as $mainkycdata)
                {
                    $kycArray[] = array('status' => $mainkycdata->Status, 'documentId' => $mainkycdata->DocumentID, 'Proof' => $mainkycdata->CategoryCode,'Image' => \App\Libraries\Helpers::apiUrl() . 'public/'.$mainkycdata->DocumentImage);
                }
                
                $data_send['kyc'] = $kycArray ;
               $data_send['policyTerms'] = array('TermsAcceptedDate' => $from_result->TermsAcceptedDateTime, 'TermsAccepted' => ($from_result->TermsAccepted === 'Y') ? 'Accepted' : 'Not Accepted');

                //  $enc_card_str = Encryptor::encrypt(json_encode($data_send), sha1(Helpers::secureEncryption()));
                $data = array('is_error' => FALSE, 'message' => '', 'code' => http_response_code(), 'res_data' => $data_send);
                return $data;
            }
            else
            {
                $data_error = array('custid' => 'No User Details Found.');
            }
        }
        $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
    }
    
    
     /**
     * LeadReg Detail
     * @param Request $request in post data
     * @return JSON
     */
    public function leadregDetail(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = '');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
            
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
         
           $from_result = \App\Models\Userwallet::select('userwallet.UserWalletID','userwallet.FirstName',
                    'userwallet.MiddleName','userwallet.LastName','userwallet.MobileNumber',
                    'userwallet.ViolaID','userwallet.EmailID','userwallet.Gender','userwallet.CreatedDateTime',
                    'userwallet.DateofBirth','userwallet.ProfileStatus','userwallet.IsVerificationCompleted',
                    'userwallet.PreferredPrimaryCurrency','userwallet.PreferredSecondaryCurrency',
                    'userwallet.IsPriorityUser','userwallet.IsSubAccount','userwallet.SocialMediaRegistration',
                    'userwallet.WalletPlanTypeId','userwallet.ProfileImagePath')
            ->join('userregistration as ur', 'userwallet.UserWalletID', '=', 'ur.UserWalletID')
        ->where([$where, ['userwallet.ProfileStatus', '=', 'N']])->first();

//print_r($from_result);
            if ( $from_result )
            {
              
                $arrayChecklogin = array(
                    'W' => 'Website',
                    'F' => 'Facebook',
                    'T' => 'Twitter',
                    'G' => 'Google'
                    
                );
               
                $userShaId = sha1($from_result->UserWalletID);
               $LiveUrl = Helpers::profileUrl($userShaId);
                
                $proPic = '';
                if ( ! empty($from_result->ProfileImagePath) )
                {
                    $proPic = $LiveUrl . $userShaId . '/profile/' . $from_result->ProfileImagePath;
                }
                $data_send['custid'] = $from_result->UserWalletID;
                $data_send['fullname'] = $from_result->FirstName.' '.$from_result->MiddleName.' '.$from_result->LastName;
                $data_send['calling_code'] = '91';
                $data_send['MobileNumber'] = $from_result->MobileNumber;
                $data_send['email'] = $from_result->EmailID;
                $data_send['violaid'] = $from_result->ViolaID . '@' . $from_result->Suffix;
                $data_send['isKyc'] = ($from_result->IsVerificationCompleted == 'N')?'No':'yes';
                $data_send['birthdate'] = $from_result->DateofBirth;
                $data_send['accountnum'] = $from_result->PreFix . $from_result->WalletNumber;
                $data_send['status'] = 'New';
                $data_send['Creationdate'] = $from_result->CreatedDateTime;
                $data_send['acCreatedBy'] = $from_result->CreatedBy;
                $data_send['Profile_img'] = $proPic;
                $data_send['registeredSource'] = $arrayChecklogin[$from_result->SocialMediaRegistration];
                $data_send['socialLoginRefno'] = $from_result->SocialLoginRefNo;
                $data_send['referralCode'] = $from_result->RegisteredReferralCode;            
               

                //  $enc_card_str = Encryptor::encrypt(json_encode($data_send), sha1(Helpers::secureEncryption()));
                $data = array('is_error' => FALSE, 'message' => '', 'code' => http_response_code(), 'res_data' => $data_send);
                return $data;
            }
            else
            {
                $data_error = array('custid' => 'No User Details Found');
            }
        }
        $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
    }

   
    
     /**
     * user wallet accounts Detail
     * @param Request $request in post data
     * @return JSON
     */
    public function walletAccountsList(Request $request)
    {
        // $enc_key = sha1(Helpers::secureEncryption());

        $userid  = $request->input('custid');
        $violaid = $request->input('violaid');
        $phone   = $request->input('phone');
        $offset  = $request->input('offset');
        $limit   = $request->input('limit');

        if ( ! empty($violaid) )
        {
            $data  = explode('@', $violaid);
            $where = [ 'cardusers.ViolaCardId', '=', $data[0] ];
        }
        elseif ( ! empty($phone) )
        {
            $where = [ 'cardusers.mobileNumber', '=', $phone ];
        }

        $errs             = array();
        $errs['is_error'] = FALSE;
        $validator        = $this->_validationCheck($request, $type = 'accountdetails');

        if ( empty($userid) && empty($violaid) && empty($phone) )
        {
            $data_send = array( 'is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => [] );
            return $data_send;
        }

        if ( $validator === TRUE )
        {
            $where = [ 'UserWalletID', '=', $userid ];
            if ( ! empty($violaid) )
            {
                $data  = explode('@', $violaid);
                $where = [ 'ViolaID', '=', $data[0] ];
            }
            elseif ( ! empty($phone) )
            {
                $where = [ 'MobileNumber', '=', $phone ];
            }


// check user valid or not !
            $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
            if ( ! empty($result->UserWalletID) )
            {
                $walletDetails         = \App\Models\Walletaccount::select('walletaccount.WalletAccountId', 
                        'walletaccount.WalletAccountNumber', 'walletaccount.PrimaryAccountIndicator', 
                        'walletaccount.WalletCurrencyCode','walletaccount.CreatedDateTime',
                        'walletaccount.AvailableBalance','walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'c.CurrencySymbol')
                        ->join('currency as c','c.CurrencyCode','=','walletaccount.WalletCurrencyCode')
                        ->Where('UserWalletId', '=', $result->UserWalletID)
                        ->where('IsAdminWallet','N')->orderBy('WalletAccountId', 'DESC')
                        ->limit($limit)->offset($offset)->get();
                $walletDetails_array = array();
                $lcoation         = '';
                if ( $walletDetails )
                {

                    foreach ( $walletDetails as $history )
                    {

                        
                        $walletDetails_array[] = array(
                            'WalletAccountId'           => $history['WalletAccountId'],
                            'WalletAccountNumber'          => $history['WalletAccountNumber'],
                            'AvailableBalance'          => (empty($history['AvailableBalance']))?'No Balance':$history['AvailableBalance'],
                            'CurrentBalance'          => (empty($history['CurrentAmount']))?'No Balance':$history['CurrentAmount'],
                            'NonWithdrawAmount'          => (empty($history['NonWithdrawAmount']))?'No Balance':$history['NonWithdrawAmount'],
                            'HoldAmount'          => (empty($history['HoldAmount']))?'No Balance':$history['HoldAmount'],
                            'PrimaryAccountIndicator'          => $history['PrimaryAccountIndicator'],
                            'WalletCurrencyCode'          => $history['WalletCurrencyCode'],
                            'WalletCurrencySymbol'          => $history['CurrencySymbol'],
                            'createdAt'          => $history['CreatedDateTime'],
                                );
                    }
                }

                $data_send = array( 'is_error' => FALSE, 'message' => '', 'code' => 200, 'totalRecords' => count($walletDetails_array), 'res_data' => $walletDetails_array );
                return $data_send;
            }
            else
            {
                $validator = 'Invalid User!';
            }
        }
        $data_send = array( 'is_error' => TRUE, 'message' => $validator, 'code' => 200, 'res_data' => array() );
        return $data_send;
    }

   
     /**
     * regDetail
     * @param Request $request in post data
     * @return JSON
     */
    public function getKycDocuments(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = 'accountdetails');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
             $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
             $offset  = $request->input('offset');
             $limit   = $request->input('limit');
            
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
           
 $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
   
           if (empty($result->UserWalletID) )
            {
               $data_error = array('info' => 'No User Details Found.');
                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
            }
            
                // code to get kyc status
                $get_kycDocsfiles = \App\Models\Partydocument::select('DocumentID','Status','CategoryCode','DocumentImage')
                        ->where('PartyID',$userid)->where('is_delete','No')->orderBy('DocumentID', 'DESC')
                        ->limit($limit)->offset($offset)->get();
        
      
              $kycArray =array();
                if(count($get_kycDocsfiles) <=0 )
                {
                    $data_error = array('info' => 'No Kyc Documents Found.');
                     $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
                }
                
                foreach($get_kycDocsfiles as $mainkycdata)
                {
                    $kycArray[] = array('status' => $mainkycdata->Status, 'documentId' => $mainkycdata->DocumentID, 'Proof' => $mainkycdata->CategoryCode,'Image' => \App\Libraries\Helpers::apiUrl() . 'public/'.$mainkycdata->DocumentImage);
                }
                
                $data = array('is_error' => FALSE, 'message' => '', 'code' => http_response_code(), 'res_data' => $kycArray);
                return $data;
           
        }
       
    }
    
    
     /**
     * delete Kyc Documents
     * @param Request $request in post data
     * @return JSON
     */
    public function deleteKycDocuments(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = 'deletekyc');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
             $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
            $documentId = $request->input('documentId');
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
           
 $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
   
           if (empty($result->UserWalletID) )
            {
               $data_error = array('info' => 'No User Details Found.');
                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
            }
            
                // code to get kyc status
                $get_kycDocsfiles = \App\Models\Partydocument::select('DocumentID','PartyID')
                        ->where('PartyID',$userid)->where('DocumentID',$documentId)->where('is_delete','No')->first();
        
                if(empty($get_kycDocsfiles->PartyID))
                {
                   
               $data_error = array('info' => 'No User Details Found In Kyc.');
                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
           
                }
      
              $kycArray =array();
                if(empty($get_kycDocsfiles) )
                {
                    $data_error = array('info' => 'No Kyc Documents Found To Delete.');
                     $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
                }
                
                $deleteKycDoc = \App\Models\Partydocument::where('DocumentID', $documentId)->update(array('is_delete' => 'Yes'));
                
                $data = array('is_error' => FALSE, 'message' => '', 'code' => http_response_code(), 'res_data' => $kycArray);
                return $data;
           
        }
       
    }
    
    
    
     /**
     * delete Kyc Documents
     * @param Request $request in post data
     * @return JSON
     */
    public function ApproveKycDocuments(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = 'approvekyc');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
             $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
            $documentId = $request->input('documentId');
            $docStatus = $request->input('status');
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
           
 $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
   
           if (empty($result->UserWalletID) )
            {
               $data_error = array('info' => 'No User Details Found.');
                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
            }
            
                // code to get kyc status
                $get_kycDocsfiles = \App\Models\Partydocument::select('DocumentID','PartyID')
                        ->where('PartyID',$userid)->where('DocumentID',$documentId)->where('is_delete','No')->first();
        
//                if(empty($get_kycDocsfiles->PartyID))
//                {
//                   
//               $data_error = array('info' => 'No Kyc Document Details Found In Kyc.');
//                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
//                return $data;
//           
//                }
      
              $kycArray =array();
                if(empty($get_kycDocsfiles) )
                {
                    $data_error = array('info' => 'No Kyc Documents Found.');
                     $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
                }
                
                $deleteKycDoc = \App\Models\Partydocument::where('DocumentID', $documentId)->update(array('Status' => $docStatus));
                
                $data = array('is_error' => FALSE, 'message' => array('info' => 'Kyc Document Status Changed Successfully'), 'code' => http_response_code(), 'res_data' => array());
                return $data;
           
        }
       
    }
    
    
    /**
     * updateUserStatus
     * @param Request $request in post data
     * @return JSON
     */
    public function updateUserStatus(Request $request)
    {
        $data_send = array();
        $chk_valid = $this->_validationCheck($request, $ticket = 'custStatus');
        if ( $chk_valid !== TRUE )
        {
            $data_error = $chk_valid;
             $data_send = array('is_error' => TRUE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => array());
        return $data_send;
        }
        else
        {
            $userid = $request->input('custid');
            $violaid = $request->input('violaid');
            $phone = $request->input('phone');
            $userStatus = $request->input('status');
            $where = ['userwallet.UserWalletID', '=', $userid];
            if ( ! empty($violaid) )
            {
                $data = explode('@', $violaid);
                $where = ['userwallet.ViolaID', '=', $data[0]];
            }
            elseif ( ! empty($phone) )
            {
                $where = ['userwallet.MobileNumber', '=', $phone];
            }
            if ( empty($userid) && empty($violaid) && empty($phone) )
            {
                $data_send = array('is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => []);
                return $data_send;
            }
           
 $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where])->first();
   
           if (empty($result->UserWalletID) )
            {
               $data_error = array('info' => 'No User Details Found.');
                $data = array('is_error' => FALSE, 'message' => $data_error, 'code' => http_response_code(), 'res_data' => '');
                return $data;
            }
            
            if($userStatus == 'Active')
            {
                $updateStatus = 'A';
            }
            else if($userStatus == 'Deactive')
            {
                $updateStatus = 'D';
            }
            if($userStatus == 'Frozen')
            {
                $updateStatus = 'F';
            }
                $updateUserStatus = \App\Models\Userwallet::where('UserWalletID', $result->UserWalletID)->update(array('ProfileStatus' => $updateStatus));
                
                $data = array('is_error' => FALSE, 'message' => array('info' => 'User Status Changes Successfully'), 'code' => http_response_code(), 'res_data' => array());
                return $data;
           
        }
       
    }
    
    
    
    function _validationCheck($request, $type = '')
    {
        $validat = [
        'custid' => 'numerics',
        'phone' => 'numerics|digits:10',
        'violaid' => 'min:8',
        ];

        $messages = [
        'custid.numerics' => 'Enter valid user ID',
        'phone.numerics' => 'Enter valid Mobile Number',
        'phone.digits' => 'Enter valid Mobile Number',
        'violaid.min' => 'Enter valid Viola ID',
        ];


        if ( $type === 'ticket' )
        {
            $validat['message'] = 'required';
            $messages['message.required'] = 'Enter Message';
        }

        if ( $type === 'status' )
        {
            $validat['deviceid'] = 'required|numerics';
            $validat['status'] = 'in:Active,Deactive,Block';
            $messages['deviceid.required'] = 'Enter Deactive ID';
            $messages['deviceid.numerics'] = 'Enter Valid Deviceid ID';
            $messages['status.in'] = 'Enter valid Status';
        }

         if ( $type === 'regType' )
        {
            $validat['regType'] = 'required|in:Leads,ActReg';
            $messages['regType.required'] = 'Enter Registration Type';
            $messages['regType.in'] = 'Enter Valid Registration Type i.e Leads or ActReg';
        }
        
         if ( $type === 'deletekyc' )
        {
            $validat['documentId'] = 'required|numeric';
            $messages['documentId.required'] = 'Enter Document Id';
            $messages['documentId.numeric'] = 'Enter Valid Document Id';
        }
         if ( $type === 'approvekyc' )
        {
            $validat['documentId'] = 'required|numeric';
            $validat['status'] = 'required|in:Approved,Pending';
            $messages['documentId.required'] = 'Enter Document Id';
            $messages['documentId.numeric'] = 'Enter Valid Document Id';
            $messages['status.in'] = 'Enter Customer Status i.e Approved or Pending.';
        }
        
        if($type === 'accountdetails')
        {
            $validat['limit']   = 'required|integer|min:1';
            $validat['offset']  = 'required|integer|min:0';
            $messages['limit.required'] = 'Limit Is required';
            $messages['limit.integer'] = 'Limit must be an Interger';
            $messages['limit.min'] = 'Limit Minimum is 1';
            $messages['offset.required'] = 'Offeset is required';
            $messages['offset.integer'] = 'Offset must be an integer';
            $messages['offset.min'] = 'Minimum is required';
            
        }
         if ( $type === 'custStatus' )
        {
            $validat['status'] = 'required|in:Active,Deactive,Frozen';
            $messages['status.required'] = 'Enter valid Status';
            $messages['status.in'] = 'Enter Customer Status i.e Active or Deactive or Frozen';
        }
        $validator = Validator::make($request->all(), $validat, $messages);
        $errs = array();
        $has_is_error = TRUE;
        foreach ($validator->messages()->toArray() as $key => $is_error)
        {
            $errs[$key] = $is_error[0];
            $has_is_error = FALSE;
        }
        if ( $has_is_error === TRUE )
        {
            return TRUE;
        }
        else
        {
            return $errs;
        }
    }
    
 
    

   

}
