<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 26 Feb 2018 10:39:42 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Remindernotification
 * 
 * @property int $ReminderNotificationId
 * @property int $RemiderId
 * @property string $Status
 * @property \Carbon\Carbon $DateTime
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Reminder $reminder
 *
 * @package App\Models
 */
class Remindernotification extends Eloquent
{
	protected $table = 'remindernotification';
	protected $primaryKey = 'ReminderNotificationId';
	public $timestamps = false;

	protected $casts = [
		'RemiderId' => 'int'
	];

	protected $dates = [
		'DateTime',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'RemiderId',
		'Status',
		'DateTime',
                'Title',
		'Description',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function reminder()
	{
		return $this->belongsTo(\App\Models\Reminder::class, 'RemiderId');
	}
}
