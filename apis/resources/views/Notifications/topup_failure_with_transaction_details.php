<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Topup failure with Transaction details device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $date varchar
 *  param $time varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction Failed',
        'text'    => $salutationName . ',<br/><br/>
                        Attention!!<br/>
                        Your payment of ' . $amount . ' on ' . $date . ' at ' . $time . ' was not successful due to network error/ technical error/ no response from third party/ session expired/ transaction cancelled.<br/> 
                        Please check and try again later.'],
    'sms'   => [
        'text' => 'Hello ' . $fullname . ', Top up of INR ' . $amount . ' was unsuccessful due to insufficient funds/Network error/ Technical error/ No response from third party/ Session expired. Please try again. Time: '.$time.' Date: '.$date,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Top Up Unsuccessful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Top Up of Rs ' . $amount . ' was unsuccessful',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
