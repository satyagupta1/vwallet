<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletaccountdaybook
 * 
 * @property int $DayBookID
 * @property int $TransactionID
 * @property int $WalletAccountID
 * @property string $EntryType
 * @property string $SubTransactionIndicator
 * @property float $TransactionAmount
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transaction $transaction
 *
 * @package App\Models
 */
class Walletaccountdaybook extends Eloquent
{
	protected $table = 'walletaccountdaybook';
	protected $primaryKey = 'DayBookID';
	public $timestamps = false;

	protected $casts = [
		'TransactionID' => 'int',
		'WalletAccountID' => 'int',
		'TransactionAmount' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionID',
		'WalletAccountID',
		'EntryType',
		'SubTransactionIndicator',
		'TransactionAmount',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'TransactionID');
	}
}
