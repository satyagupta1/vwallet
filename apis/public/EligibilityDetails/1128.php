<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is an Idea mobile recharge pertaining to users in South and West circles valued >= INR 250.</li>
        <li>System should check if the transaction date is between 25th Dec, 2017  to 31st Dec, 2017.</li>
        <li>System should credit INR 10 cashback into the users wallet every month starting from the transaction date.</li>
    </ul> 
</div>
<?php include 'footer.php';?>