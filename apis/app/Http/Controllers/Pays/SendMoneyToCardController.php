<?php

namespace App\Http\Controllers\Pays;

/**
 * SendMoneyController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class SendMoneyToCardController extends \App\Http\Controllers\Controller {

    // send Money Wallet to Wallet
    public function sendMoneyWC(\Illuminate\Http\Request $request)
    {
        ini_set('max_execution_time', 180); 
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateSendMoneyWC($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        /*
        //code to check vpin
        $params     = array( 'partyId' => $request->input('partyId'), 'vPin' => $request->input('vPin') );
        
        $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
        if ( $verifyVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'vPin' => 'Please enter valid V-PIN' );
            return response()->json($output_arr);
        }
         * 
         */

        $partyId         = $request->input('partyId');
        $beneficiaryId   = $request->input('beneficiaryId');
        $amount          = $request->input('amount');
        $convertedAmount = $request->input('convertedAmount');
        
        $businessModelid = 1;
        $transStaus      = config('vwallet.transactionStatus.pending');
        $transBool       = TRUE;
        //trans_type whether it is WW or WB
        $transType       = $request->input('transType');
        $productEventId  = 198;
        $benfType        = 'Vcard';
        $serviceType     = 'send_money_wtoc';
        
        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        // get Beneficiary details if beneficiary Exists.
        $banificiaryInfo = \App\Libraries\TransctionHelper::_beneficiaryValidate($beneficiaryId, $benfType, FALSE, FALSE);
        
        if ( $banificiaryInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiartNotFound') );
            // return $output_arr;
        }         

        if ( $partyId == $beneficiaryId )
        {
            $message                   = trans('messages.partySame');
            $output_arr['display_msg'] = array( 'info' => $message );
            return json_encode($output_arr);
        }
        
        // session Check
        $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        /*
        if ( is_bool($sessionId) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidSession') );
            // return $output_arr;
        }
         * 
         */
        // check Balance
        if ( $partyInfo->CurrentAmount < $amount )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output_arr;
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
        
        if ( $partyTransLimit['error'] )
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];
        
        /*
        // check Benifisary transction or wallet credit limits 
        $benificaryTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($banificiaryInfo->UserWalletID, $banificiaryInfo->WalletPlanTypeId, $amount, TRUE);
        if ( $benificaryTransLimit['error'] )
        {
            $output_arr['display_msg'] = array( 'info' => $benificaryTransLimit['info'] );
            $transStaus                = 'Failed';
            $transBool                 = FALSE;
        }
        
         * 
         */
        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelid, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transStaus, $sessionId);
        \App\Libraries\CommunicationLib::_communicationSend($partyId, $beneficiaryId = NULL, $transRes, $serviceType);
        if ( $transRes['error'] )
        {
            $output_arr['display_msg'] = array( 'info' => $transRes['info'] );
            return $output_arr;
        }
        $sendData = array(
            'amount'    => $convertedAmount,
            'mobileNumber'       => $banificiaryInfo->BeneficiaryMobileNumber,
            'transactionid' => $transRes['response']['TransactionDetails']['TransactionOrderID'],
            'currency'    => 'GBP',
        );
        $url = 'http://dev.violacard.com/apis/reciveMoney';
        $response = $this->curl_send($url, $sendData);
        $trnsStatus = 'Failed';
        $txnDetailId = $transRes['response']['TransactionDetails']['TransactionDetailID'];
        $updateData = array('TransactionStatus' => $trnsStatus);
        $output_arr['display_msg'] = array( 'info' => 'EXCHANGE Transaction Failed.' );
        if ($response)
        {
            $newResponse = json_decode($response);
            if ($newResponse->status == 1)
            {
                $trnsStatus = 'Success';
                $updateData = array('TransactionStatus' => $trnsStatus); 
                $output_arr['display_msg'] = array( 'info' => 'EXCHANGE Transaction Success.' );
            }
        }
        \App\Models\Transactiondetail::where('TransactionDetailID', $txnDetailId)->update($updateData);
        
        if ($trnsStatus === 'Failed')
        {
            $transactionDetails = \App\Libraries\PaymentRefund::paymentRefund($txnDetailId, $amount);
        }
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $transRes['response'];
        return $output_arr;
    }

    private function _validateSendMoneyWC($request)
    {
        $validate = array(
            'partyId'         => 'required|numeric|digits_between:1,10',
            'walletacntId'    => 'required|numeric|digits_between:1,10',
            'amount'          => 'required|numeric',
            'convertedAmount' => 'required|numeric',
            'transType'       => 'required|in:WW,WB,WC',
            // 'vPin'            => 'required|numeric|digits_between:1,4',
            'beneficiaryId'   => 'required|numeric|digits_between:1,4',
            // 'businessModelId' => 'required|numeric',
        );

        $messages = array();

        if ( $request->input('transType') == 'WW' )
        {
            $validate['beneficiaryId'] = 'required|numeric|digits:10';
            $messages                  = array(
                'beneficiaryId.required' => 'Mobile number is required',
                'beneficiaryId.numeric'  => 'Mobile number should be numeric',
                'beneficiaryId.digits'   => 'Mobile number should be 10 digits'
            );
        }
        if ( $request->input('transType') == 'WB' )
        {
            $validate['beneficiaryId'] = 'required|numeric';
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    public function reviceMoneyCW(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validatereviceMoneyCW($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $mobileNumber = $request->input('mobileNumber');
        $amount          = $request->input('amount');
        $businessModelid = 1;
        $transStaus      = config('vwallet.transactionStatus.success');
        $transBool       = TRUE;
        //trans_type whether it is WW or WB
        $transType       = 'CW';
        $productEventId  = 199;
        $benfType        = 'Vcard';
        $serviceType     = 'send_money_ctow';
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($mobileNumber, 'mobile');
        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }
        $partyId = $partyInfo->UserWalletID;
        
        // get Beneficiary details if beneficiary Exists.
        $banificiaryInfo = \App\Libraries\TransctionHelper::getAdminWalletInfo(106);//Exchange admin wallet id
        // return $banificiaryInfo;
        if ( $banificiaryInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiartNotFound') );
            // return $output_arr;
        }
        /*
        if ( $partyId == $beneficiaryId )
        {
            $message                   = trans('messages.partySame');
            $output_arr['display_msg'] = array( 'info' => $message );
            return json_encode($output_arr);
        }
         * 
         */
        $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        
        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
        
        if ( $partyTransLimit['error'] )
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];
        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);
        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelid, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transStaus, $sessionId, $transType);
        
        if ( $transRes['error'] )
        {
            $output_arr['display_msg'] = array( 'info' => $transRes['info'] );
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg'] = array( 'info' => 'EXCHANGE Transaction Success.' );
        $resData = array(
            'amount'    => $transRes['response']['TransactionDetails']['Amount'],
            'unique_id' => $transRes['response']['TransactionDetails']['TransactionDetailID'],
            'cur'  => 'INR',
            'status'    => ($transStaus === 'Success') ? 1 : 2,
            'msg'   => 'Your Transaction has been successfull.'
        );
        return $resData;
        $output_arr['res_data'] = $resData; // $transRes['response']
        return $output_arr;
    }
    
    private function _validatereviceMoneyCW($request)
    {
        $validate = array(
            'mobileNumber'    => 'required|numeric|digits_between:1,10',
            'amount'          => 'required|numeric',
            'currency'       => 'required',
        );

        $messages = array();

        if ( $request->input('transType') == 'WW' )
        {
            $validate['beneficiaryId'] = 'required|numeric|digits:10';
            $messages                  = array(
                'beneficiaryId.required' => 'Mobile number is required',
                'beneficiaryId.numeric'  => 'Mobile number should be numeric',
                'beneficiaryId.digits'   => 'Mobile number should be 10 digits'
            );
        }
        if ( $request->input('transType') == 'WB' )
        {
            $validate['beneficiaryId'] = 'required|numeric';
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    public function curl_send($url, $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        return $output;
    }

}
