<?php

namespace App\Http\Controllers\Topup;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TopupReversal extends \App\Http\Controllers\Controller {

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function __construct()
    {
        $this->encKey = sha1(config('vwallet.ENCRYPT_STATIC'));
    }

    /**
     * This used to call validationCheck_wallet method in this listWallets method
     * @desc In this we will check validationCheck_wallet method for validation of party id and device type
     * @desc If all validations have no errors will call _getPartyWalletAccounts and _getPartyExternalCards methods to get list of all wallets details and card details in array list response
     * 
     * @param PartyId integer
     *  
     * @return Array
     */
    // code for reversal transaction of add topup
    public function topUpReversal(\Illuminate\Http\Request $request)
    {
        $update_wallet_balance = TRUE;
        $txn_detail_id         = $request->input('transactionId');
        // Default Response
        $output_arr            = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        if ( $request->input('refundFrom') != 'ccavenue' )
        {
            // check validation
            $chk_valid = $this->_ValidationTopupReversal($request);
            if ( $chk_valid !== TRUE )
            {
                $output_arr['display_msg'] = $chk_valid;
                return json_encode($output_arr);
            }
            // get walletAccountId from transactionDetail table
            // check wallet exists with correct user or not


            if ( $this->_getUserWalletId($request) == FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.noWalletAccountFound') );
                return json_encode($output_arr);
            }


            // code to check refund transaction is Done or not
            if ( $this->_getUserWalletId($request)->EventCode == 'TOPREV' )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.reversalTransactionAlreadyDone') );
                return json_encode($output_arr);
            }




            // check wallet exists with correct user or not
            if ( $this->_checkUserAccountBalance($request, $this->_getUserWalletId($request)->ToAccountId) == FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.emptyWalletBalance') );
                return json_encode($output_arr);
            }

            if ( $this->_checkUserAccountBalance($request, $this->_getUserWalletId($request)->ToAccountId) < $this->_getUserWalletId($request)->Amount )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.addrequiredfunds') );
                return json_encode($output_arr);
            }


            $amountMain     = $request->input('amount');
            $productEventId = 7;
            $transStaus     = 'Success';
            $event          = 'TOPREV';
            $entityId       = 1;
            $message        = 'Transaction Successfully Reversed.';
            $vendor         = 'Viola Wallet';
            $txnType        = 'Topup Reversal';
            $amountTag      = 'TRAN_AMT';

            $this->_updateTransactionEvent($txn_detail_id, $status         = 'Refund', $event          = 'TOPREV');
            $insertCashback = \App\Libraries\TransctionHelper::cashBack($request, $amountMain, $productEventId, $transStaus, $event, $entityId, $message, $vendor, $txnType, $amountTag);
        }

        // insert notifictaions
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.amountReversalSuccess') );
        return json_encode($output_arr);
    }

    /**
     * code for insertion of update transaction detail
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    private function _updateTransactiondetail($txn_detail_id, $status, $insert_card_details)
    {
        $update_txn_detail                    = \App\Models\Transactiondetail::find($txn_detail_id);
        $update_txn_detail->TransactionStatus = $status;
        $update_txn_detail->FromAccountId     = $insert_card_details;
        $update_txn_detail->save();
    }

    /**
     * code for to check _checkWalletAccount in add money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    //code to check for wallet and user matches found with correct id
    private function _checkWalletAccount($request)
    {
        // code to get kyc accepted or not from table users
        $check_wallet_exists = \App\Models\Walletaccount::select('WalletAccountId')
                ->where('WalletAccountId', $request->input('walletacntId'))
                ->where('UserWalletId', $request->input('partyId'))
                ->count();
        return $check_wallet_exists;
        // return $check_limit->IsVerificationCompleted;
    }

    // methods for Add money methods in topup ends
    // methods start for refund money in topup
    /**
     * code for to check _checkUserAccountBalance in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    //code to check for wallet and user matches found with correct id
    private function _checkUserAccountBalance($request, $walletAccountId)
    {
        // code to get kyc accepted or not from table users
        $check_wallet_exists = \App\Models\Walletaccount::select('CurrentAmount')
                ->where('UserWalletId', $request->input('partyId'))
                ->where('WalletAccountId', $walletAccountId)
                ->first();
        return ($check_wallet_exists) ? $check_wallet_exists->CurrentAmount : FALSE;
        // return $check_limit->IsVerificationCompleted;
    }

    /**
     * code for to check _getUserWalletId in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    //code to check for wallet and user matches found with correct id
    private function _getUserWalletId($request)
    {

        // code to get kyc accepted or not from table users
        $check_walletAccount = \App\Models\Transactiondetail::select('ToAccountId', 'PaymentsProductID', 'Amount', 'EventCode', 'TransactionDate')
                ->where('TransactionDetailID', $request->input('transactionId'))
                ->where('ToWalletId', $request->input('partyId'))
                ->first();
        return ( ! empty($check_walletAccount)) ? $check_walletAccount : FALSE;
        // return $check_limit->IsVerificationCompleted;
    }

    /**
     * code for to check _updateTransactionEvent in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for update transaction detail event code for reversal
    private function _updateTransactionEvent($txn_detail_id, $status, $event)
    {
        $update_txn_detail                    = \App\Models\Transactiondetail::find($txn_detail_id);
        $update_txn_detail->EventCode         = $event;
        $update_txn_detail->TransactionStatus = $status;
        $update_txn_detail->TransactionDate   = date('Y-m-d H:i:s');
        $update_txn_detail->save();
    }

    // methods for refund money in topup ends

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param PartyId integer
     * @param WalletAcntId integer
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _ValidationTopupReversal($request)
    {

        // validations
        $validate = array(
            'partyId'       => 'required|numeric',
            'transactionId' => 'required|numeric',
            'amount'        => 'required|numeric',
        );
        $messages = array();

        // validation for mobile device


        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
