<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Welcome to ViolaWallet template
 * 
 *  param $fullname varchar 
 *  param $email varchar
 *  param $mobileNumber varchar
 *  param $supportURL varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Welcome to ViolaWallet!',
        'text'    => $salutationName . ',<br/><br/>
                        Welcome to ViolaWallet!! <br/>
                        Thank you for creating ViolaWallet account.<br/>
                        We are delighted that you joined us!',
    ],
    'sms'   => [
        'text' => 'Welcome to ViolaWallet!! Thank you for creating ViolaWallet account.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Welcome to ViolaWallet! We are delighted that you joined us.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file welcome_message.php */
