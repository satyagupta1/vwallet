<?php
    namespace App\Models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class NotificationLogParameters extends Model {
	
	    protected $table = 'notificationlogparameters';
	    public $timestamps = false;
        
    }
    /* End of file NotificationLogParameters.php // Location: ./app/Models/NotificationLogParameters.php */


