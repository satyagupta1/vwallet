<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Change Password template
 * 
 *  param $fullName varchar 
 *  param $otpCode varchar 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullName) ? $salutation.' '.$fullName : $salutation;
$array = [
    'email' => [
        'subject' => 'Password Changed',
        'text'    => $salutationName . ',<br/><br/>
Password Change Request!!<br/> 
Password for your ViolaWallet account has been updated.<br/> 
If you didn`t make this request, your account might be compromised. To get back into your account, you`ll need to reset your credentials.'],
    'sms'   => [
        'text' => 'Your Viola Password has been changed. Please do not share your PIN/Password/CVV with anyone',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your Password has been changed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => 'Your password is updated!',
    ],
];

echo json_encode($array);

/* End of file change_password.php */
