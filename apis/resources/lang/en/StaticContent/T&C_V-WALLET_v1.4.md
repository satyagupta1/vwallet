# TERMS & CONDITIONS
This document provides the terms and conditions of use related to the services offered by V-Wallet.

DEFINITIONS
-----
1. **Agreement** means these Terms and Conditions f Use f the Service.
2. **Authorized User** is you as the subscriber of V-Wallet or any individual who you allow to use the service or your password or other means to access your V-Wallet.
3. **Auto Pay** means the user wallet shall be auto loaded with defined amount once the balance in the wallet falls below certain amount.
4. **Billing Account** is the account you have with V-Wallet that is directly related to the mobile phone or email address and related services provided to you.
5. **Business Day** is Monday through Friday, but excluding holidays as required by law.
6. **Depositing Account** pertains to any account including bank or net banking account, debit or credit card account, checking account, and other accounts from which or to which funding will be debited or charged.
7. **Mobile Alerts** means text messages that are sent to your mobile phone to serve as additional notifications of system transactions related to your V-Wallet account.
8. **Mode of Payment** refers to the different payment options user can avail of on V-Wallet like
9. **Monthly Payment** pertains to settlement set for monthly recurring expenses or bills payable.
10. **Payment** means a settlement transaction initiated through the services provided by V-Wallet.
11. **Payment Date** is the day your V-Wallet account will be debited or charged for the payment.
12. **Payment Instruction** is the information provided by you for a payment to be made to and through V-Wallet such as, but not limited to, billing account number, your V-Wallet mobile number, amount of payment and payment date.
13. **Personal Information** is the information provided to specifically identify and verify a single person.
14. **Primary Account** refers to a V-Wallet account that has been designated as your default account which will be used to execute transactions.
15. **Scheduled Payment** is a Payment established to take place on a future date that has been established by you.
16. **Service** refers to the financial solutions offered by V-Wallet.

ELIGIBILITY
------
To be eligible, you should be: 
- At least 18 years of age
- Must be living in India

Your use of V-Wallet is your confirmation that you meet the eligibility requirements and your acceptance of our V-Wallet Terms and Conditions.

LIMITS
------
V-Wallet may impose limits on the amount of money you use or number of payments you may execute in a period. An account has an initial account balance limit of Rs. 210,000 as prescribed by the Reserve Bank of India (RBI).

Upgrade of account limit is possible and will require additional KYC from you.
Once your KYC is verified, your limit is increased to INR. 100,000.

DEPOSITS
------

You authorize V-Wallet to add depositing accounts to your V-Wallet account.
You also authorize V-Wallet to follow the Payment Instructions that it receives from you through. When the app receives a Payment Instruction, you authorize V-Wallet to debit or charge your depositing account and remit funds to V-Wallet on your behalf. You certify that any depositing account you add to your profile is an account held by you and from which you are legally authorized to debit or charge payments, and any payment you make using the app.

The first depositing account that you add to your profile shall be deemed to be your Primary Account, unless you later change the designation of what shall be the default account. If your identity cannot be verified satisfactorily, V-Wallet may not allow you to add a specific type of depositing account to your wallet.

Neither V-Wallet nor the app shall incur any liability if the service is unable to complete any payments initiated by you for any reason beyond our control, including the existence of any one or more of the following scenarios:

- If, through no fault of our service, your Depositing Account does not contain sufficient funds or a sufficient credit limit to complete the payment or if the payment would exceed the credit limit of your depositing account;
- The payment processing center is not working properly and you know or have been advised about the malfunction before you execute the payment;
- You have not provided the correct depositing account information, or information that you provided becomes incorrect if you have been issued a new card with a new card number; and/or
- Circumstances beyond our control such as, but not limited to, fire, flood, or interference from an outside force even if foreseeable or foreseen that prevent the proper execution of the payment and the Service has taken reasonable precautions to avoid those circumstances.

Provided none of the above are applicable, if the Service causes an incorrect amount of funds to be removed or causes funds from your depositing account to be directed in a manner that does not comply with your Payment Instructions, your sole remedy shall be the Service returning the improperly transferred funds to your depositing account, and must report immediately to V-Wallet any previously misdirected payments.

MODES OF DEPOSITS
-----
V-Wallet accepts the following modes of payments:

- Net Banking
- Debit/Credit Card
- Bank
- ATM
- Checking Account
- NEFT
- RTGS
- UPI

The above mentioned will be processed via our payment gateway Viola Pay and partners. V-Wallet assures that no information shall be shared or published by us as you enter payment related details on your depositing account sites.

WITHDRAWALS
------
Full KYC customers can apply for a Debit card. Users can withdraw money from ATM using the card. Users can send money to their bank accounts using their favorite mode of IMPS or NEFT.

AUTOPAY
------
An AutoPay option is a type of payment whereby you authorize V-Wallet to directly charge your primary account for on-demand or recurring charges (&quot;AutoPay&quot;). On-demand charges include, but are not limited to, your monthly service fees and due immediately charges. If you authorize this, AutoPay option will be made to your account to cover any due immediately charge not made by you using another mechanism, to ensure your service is not suspended. AutoPay service payments will be authorized five days from your monthly due date to pay for the Service. Prior to your billing due date, you will receive notification via mobile SMS of your balance, which will be charged to your V-Wallet account.

Upon successful AutoPay process, the Service will provide a payment alert via mobile SMS with confirmation number.

FAILED OR RETURNED PAYMENTS
------
In using V-Wallet, you are requesting to make Payments using your V-Wallet account. If we are unable to complete the Payment for any reason associated with your account for example, if there are insufficient funds in your account to cover the Payment or your credit limit has been reached, the Billing Account number is not valid, the Service is unable to locate your Billing Account, or your Billing Account is already paid in full, the Payment may not be completed. You understand that the Service, your financial institution, your card issuer, payment processors and/or other parties may return Payments for various reasons, including those outlined above.

Furthermore, to manage risk, the Service may limit the funds available for use, or the allowed Payment amount, for any or all Payments, including Auto Payments. The Service reserves the right to use any Depositing Account found within your V-Wallet to make your Payments. If we cannot process your Payment due to system upgrades, you are still responsible to make it, and you are responsible for ensuring timely payment of all your bills.

AUTO PAYMENT CANCELLATION
------
You can disable or cancel an Autopay option three (3) Business Days priorat any timeto the date of the payment scheduled. To cancel an Autopay option, log into your V-Wallet account on www.vwallet.in and follow the instructions to remove the AutoPay preference. In addition, if you cancel an Autopay option, you will still be liable to settle your monthly bills that can be done in other means offered by V-Wallet.

TEXT TO PAY
----
If you have not established AutoPay with a Primary Account, you may choose to utilize the Service&#39;s Text to Pay functionality. A Text to Pay settlement is a type of one-time payment whereby you authorize the Service via a mobile originated text message to directly charge your Primary Account. You will be prompted by V-Wallet through a free billing notification text message that you have bill charges pending. When you reply to the Service via a mobile originated text message with your Payment Instruction in an approved format, you authorize the Service to charge your Primary Account for such service.

BILLING ACCOUNT INFORMATION
-----
Your activation with V-Wallet shall be deemed to be your acceptance of these Terms and Conditions located at www.vwallet.in, and your authorization for the Service to obtain account information necessary to perform the Service from V-Wallet on your behalf.

Accuracy and Dispute - The Service is unrelated to the maintenance of your Billing Account information. The Service is only responsible for presenting to you the information that the Service receives. Any discrepancies or disputes regarding the accuracy of your Billing Account information or detail must be addressed by you with our customer support direct hotline at 1-800-VWALLET.

PASSWORD AND SECURITY
------
V-Wallet controls your registration in the Service, the issuance of your password, and your authentication when you enroll and each time you access the Service. You agree not to give or make available your password or other means to access the Service to any unauthorized individuals. You are responsible for all payments you authorize using the Service. If you permit Authorized Users or other persons to use the Service or your password or other means to access the Service, you are responsible for any payments they authorize. You will be responsible for any Payments that a person to whom you provided your password or other means to access the Service authorizes, even if that person disregards instructions or limits you communicated to that person about Payments they could authorize.

If you believe that your password or other means to access your V-Wallet has been hacked, or that someone may attempt to use the Service without your consent or has transferred money without your permission, you must immediately contact or customer support hotline at 1-800-VWALLET. You acknowledge and agree that V-Wallet shall not be liable or responsible for any unauthorized payments, charges or debits made by someone using your password.

INFORMATION AUTHORIZATION
------
You agree that the information you provide to the Service can go through a verification process. You agree that the Service reserves the right to obtain financial information regarding your Depositing Accounts from your financial institutions or card issuers in cases where there is a need to resolve payment posting problems or for verification or for other purposes related to the Service. You also agree that the Service may request information such as your birth date, PAN and/or Aadhaar details and other necessary information to help prevent fraud.

SERVICE FEES
------
You agree to pay V-Wallet on demand applicable fees and charges in respect of the provision of services by Us in accordance with the Fee Table enclosed to Illustration A of this Agreement.

You agree to pay Us, on demand, in addition to the fees, any duty, VAT or other taxes whatsoever arising in respect of any of the services. We are not required to give you prior notice of the imposition or variation in any duty, VAT or other taxes arising in respect of any of the Services.

|  Types of Transactions  |  Service Fees |
| --- | --- |
| Depositing Money into Wallet | 0.00 |
| Withdrawing Money from Wallet | 0.00 |
| Transferring of Money |   |
| Wallet to Wallet | 0.00 |
| Wallet to Bank | 0.00 |
| Payments of Bills | 0.00 |
| Recharging of Prepaid Mobiles | 0.00 |



ALTERATIONS AND AMENDMENTS
------
This Agreement, applicable fees and service charges may be altered or amended by V-Wallet from time to time. In such event, the Service shall provide notice to you as required by law. Any use of the Service after the Service provides you a notice of change will constitute your agreement to such change(s). Furthermore, the Service may, from time to time, revise or update the applications, services, and/or related material, which may render all such prior versions obsolete. Consequently, the Service reserves the right to terminate this Agreement as to all such prior versions of the applications, services, and/or related material and limit access to only the V-Wallet&#39;s more recent revisions and updates. You authorize us to send or provide by electronic communication any notice, communication, amendment or replacement to the Agreement, or disclosure required to be provided orally or in writing to you. You agree to receive any electronic communication provided to you and will not attempt to avoid receiving any such communication. You are deemed to have received any electronic communication provided to you when they are made available to you.

ADDRESS OR BANKING CHANGES
------
It is your sole responsibility to ensure that the contact information in your user profile that you establish with V-Wallet is current and accurate. This includes, but is not limited to your name, address, phone number(s) and e-mail address. V-Wallet is not responsible for any payment processing errors or fees incurred if you do not provide accurate contact information, or if you do not notify us promptly of the change.

NOTIFICATIONS
------
By accepting these Terms and Conditions, you agree to receive all notifications relating to the Service in electronic form delivered to your mobile phone or device as an SMS message or to your email address. It is your sole responsibility to ensure that the e-mail address and mobile phone number registered in your V-Wallet account are accurate and current.

SERVICE TERMINATION, CANCELLATION OR SUSPENSION
------
In the event, you wish to cancel the Service, you may do so by contacting our V-Wallet customer support hotline at 1-800-VWALLET or through other V-Wallet interfaces. Any payment(s) that the Service has already processed before the requested cancellation date will be completed by the Service. All monthly and scheduled payments will not be processed once the Service is canceled. The Service may terminate or suspend availability of the Service to you at any time. Neither termination nor suspension shall affect your liability or obligations under this Agreement.

DISPUTES
------
In the event of a dispute regarding the Service, you agree to resolve the dispute as set forth in this Agreement. If you dispute a Payment made from a Depositing Account that is a debit/credit card account, you acknowledge that such dispute including, but not limited to, chargeback or fraud must be reported directly to our customer support hotline at 1-800-VWALLET.

ENTIRE AGREEMENT
------
You agree that this Agreement is the complete and exclusive statement between you and the Service, and it supersedes any proposal or prior agreement, oral or written, and any other communications between you and the Service relating to the subject matter of this Agreement. If there is a conflict between what a representative of the Service or customer service department says and the terms of this Agreement, the terms of this Agreement will prevail.

ASSIGNMENT
------
You may not assign this Agreement to any other party. The Service may assign or delegate any or all its rights and responsibilities under this Agreement to independent contractors or other third parties.

NO WAIVER
------
We shall not be deemed to have waived any of our rights or remedies hereunder unless such waiver is in writing and signed by us. No delay or omission on our part in exercising any rights or remedies shall operate as a waiver of such or any rights or remedies. A waiver on any one occasion shall not be construed as a bar or waiver of any rights or remedies on future occasions.



CAPTIONS
------
The captions of sections hereof are for convenience only and shall not control or affect the meaning or construction of any of the provisions of this Agreement.



EXCLUSIONS OF WARRANTIES
------
The Service and related documentation are provided &quot;as is&quot; without warranty of any kind, either expressed or implied, including but not limited to the implied warranties of merchant ability for a purpose.

GOVERNING LAW
------
This Agreement shall be governed by and construed in accordance with the laws of the state in which Service is primarily provided to you, excluding such state&#39;s conflicts of laws principles.

LIMITATION OF REMEDIES
------
You agree that your sole and exclusive remedy for any failure to provide you with the service under this agreement or the failure of V-Wallet to perform hereunder shall be your right to raise suggestions on re-perform solutions for a specific or on all services offered by V-Wallet.

LIMITATION OF LIABILITY
------
This limitation shall constitute the entire liability of V-Wallet and your exclusive remedies about your use of our services. Furthermore, in no event shall we be held liable for any indirect, special, incidental, consequential and/or exemplary damages arising in any way out of our services, or the acts or omissions of V-Wallet.

You have read and understand the foregoing agreement and agree to be bound by all its terms.



TRADEMARK INFORMATION
------
Product names, logos, designs, titles, words or phrases within this publication may be trademarks, service marks, or trade names of V-Wallet, our affiliates and/or other entities and may be registered in certain jurisdictions.

WAIVER
------
The failure of Paytm V-Wallet to exercise or enforce any right or provision of this Agreement will not constitute a waiver of such right or provision. Any waiver of any provision of this Agreement will be effective only if in writing and signed by PaytmV-Wallet.