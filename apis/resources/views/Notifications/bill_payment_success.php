<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Recharge is Successful
 * 
 *  param $amount decimal
 *  param $rechargedate Date
 *  param $time time
 *  param $rechargeid varchar
 *  param $operator varchar
 *  param $offeramount int
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Payment Successful',
        'text'    => $salutationName.',<br/><br/>
    Enjoy!<br/>
Your Payment of  Rs '.$amount.' has been successful. Your transaction details are given below<br/><br/>

Utility Name: '.$billerName.'<br/>
CustomerID: '.$customerid.'<br/>
Payment Date: '.$date.'<br/>
Time: '.$time.'<br/>
Transaction ID: '.$transactionid.'<br/>
Points earned: 0',
    ],
    'sms'   => [
        'text' => 'Payment of INR '.$amount.' Successful for '.$billerName.' on '.$date.' at '.$time.'. Balance amount '.$amount,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Payment for '.$billerName.' successful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Payment of INR '.$amount.' Successful for '.$billerName.' on '.$date.' at '.$time.'. Balance amount '.$amount,
    ],
];

echo json_encode($array);

/* End of file recharge_is_successful */
