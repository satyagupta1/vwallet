<?php
    namespace App\Libraries\Communication\Push\Iphone;

    class PushNowI {

      public static function sendPush($pushData){

        if($pushData['dev_mode']=='2'){
          $devmod = '2';
        }
        else
        {
          $devmod = '1';
        }

        if( $pushData['deviceToken']){
	         // Put your private key's passphrase here:
	         $passphrase = 'sam1186$A';

           // Put your alert message here:
           date_default_timezone_set("UTC");

           ////////////////////////////////////////////////////////////////////////////////
           $ctx = stream_context_create();
           $folder_name = str_replace('-', '', $pushData['applicationNameKey']);
           
          // if ($deviceToken=="452a619b492620c2ec9ceaea75c8a5889e72c4671ca02765e44d75e5f268fd1f" OR "fe51de1b38010572dbc7ef5a0a4b6253598fc94f804d91b67cf873f0af608d55"){
          if ($devmod == '2'){
              $applesocketurl='ssl://gateway.sandbox.push.apple.com:2195';
              stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__.'/'.$folder_name.'/dev'.$folder_name.'.pem'); 
              stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
          }else{
              $applesocketurl='ssl://gateway.push.apple.com:2195';
              //stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__.'/'.$folder_name.'/disVCard.pem');
              stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__.'/'.$folder_name.'/dis'.$folder_name.'.pem');
              stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
          }
           // Open a connection to the APNS server
           $fp = stream_socket_client(
                	$applesocketurl, $err,
                  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx
           );

	          if (!$fp)
		          exit("Failed to connect: $err $errstr" . PHP_EOL);

            	//echo 'Connected to APNS' . PHP_EOL; , 'content-available'=>'1'
            	// Create the payload body


              $body['aps'] = array(
                                'deviceToken' => $pushData['deviceToken'],
                                'alert' => $pushData['message'],
                                'other' => $pushData['other_details'],
                                'sound' => 'default',
                                'content-available'=>'1',
                                'click_action' => '',
                                'color' => '',
                                'tag' => '',
                                );

	                // Encode the payload as JSON
                  $payload = json_encode($body);
                  
                    // Check if device token is valid IOS token              
                    if ( !ctype_xdigit($pushData['deviceToken']))  {
                        //echo ':  is not a valid hex string.\n';
                        return false;
                    } 
                // Build the binary notification
                  $msg = chr(0) . pack('n', 32) . pack('H*', $pushData['deviceToken']) . pack('n', strlen($payload)) . $payload;

                  // Send it to the server
                  //$result = fwrite($fp, $msg, strlen($msg));

                  // Send it to the server
                  $ret_msg = "";
                  try
                  {
                    $result = fwrite($fp, $msg, strlen($msg));
                    if ($result)
                    {
                      $ret_msg = 'delivered' . PHP_EOL;
                    }
                    else
                    {
                      $ret_msg = 'ndelivered' . PHP_EOL;
                    }
                  }
                  catch (Exception $ex)
                  {
                      // try once again for socket busy error (fwrite(): SSL operation failed with code 1.
                      // OpenSSL Error messages:\nerror:1409F07F:SSL routines:SSL3_WRITE_PENDING)
                      sleep(5); //sleep for 5 seconds
                      $result = fwrite($fp, $msg, strlen($msg));
                      if ($result)
                      {
                        $ret_msg = 'delivered' . PHP_EOL;
                      }
                      else
                      {
                        $ret_msg = 'ndelivered' . PHP_EOL;
                      }
                  }

                  // Close the connection to the server
                  fclose($fp);
                  return $ret_msg;
                }
                else{
                  return 'Device token is not available.';
                }
              }
    }
?>
