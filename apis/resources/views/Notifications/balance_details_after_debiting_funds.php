<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Balance details after debiting funds device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $coustomername varchar
 *  param $utilityname varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($coustomername) ? $salutation.' '.$coustomername : $salutation;
$array = [
    'email' => [
        'subject' => 'Bill Paid from ViolaWallet',
        'text'    => $salutationName .', <br/><br/>
Payment of '.$amount.' debited from your ViolaWallet account towards '.$utilityname.'<br/>.
The current balance  is '.$amount,
    ],
    'sms'   => [
        'text' => 'Payment of '.$amount.' debited from your ViolaWallet account.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Rs '.$amount.' debited towards '.$utilityname,
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Rs '.$amount.' debited from your ViolaWallet account for '.$utilityname.'
',
    ],
];

echo json_encode($array);

/* End of file balance_details_after_debiting_funds.php */
