<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Reference
 * 
 * @property int $ReferenceID
 * @property string $ReferenceType
 * @property string $ReferenceCode
 * @property string $ReferenceDesc
 * @property \Carbon\Carbon $EffectiveDate
 * @property \Carbon\Carbon $ExpiryDate
 * @property string $ActiveIndicator
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 *
 * @package App\Models
 */
class Reference extends Eloquent
{
	protected $table = 'reference';
	protected $primaryKey = 'ReferenceID';
	public $timestamps = false;

	protected $dates = [
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ReferenceType',
		'ReferenceCode',
		'ReferenceDesc',
		'EffectiveDate',
		'ExpiryDate',
		'ActiveIndicator',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];
}
