<?php
/**
 * A language file for text and email alert
 * @category PHP
 * @package  viola wallet
 * @author  Viola Services (India) PVT LTD
 */

/*1 email text for normal personal register nreg = normal register*/

return [
  'nreg'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'mobile'),
    'text'      => '<div style="margin:0;padding:0px; ">
          One Time Passcode (OTP) is sent to your mobile {{mobile}}. Please enter OTP to login
        </div> ',
  ],
  'nreg_notusing'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
                    <tbody>
                    <tr style="margin:0;padding:0">
                    <td style="color:#888;font-weight:200;letter-spacing:inherit;font-size:15px;line-height:1.6; text-align:left;">
                      <br><p >Hello {{fullname}},</p>
                      <br><p style="text-align:center">{{subject}}</p>
                      <p style="margin-bottom:5px;font-size:13px;text-align:center;"> Your account is successfully created but not been activated yet. </p>
                      <p style="margin-bottom:5px;font-size:13px;text-align:center;">To activate your account, please click on the button below and you will be directed to the activation.</p>
                    </td>
                    </tr>
                    </tbody>
                  </table>
                  </div>
                  <br style="margin:0;padding:0">
                  <div style="margin:0;padding:0 15px">
                    <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
                      <tbody>
                      <tr style="margin:0;padding:0">
                      <td width="100%" align="center" style="margin:0;padding:0;margin-bottom:15px;font-size:12px;min-width:100px;max-width:100%;display:inline-block;vertical-align:top">
                        <a href="{{activation_url}}" style="margin:0;padding:5px 10px;color:#f1910b;display:inherit;text-decoration:none;text-align:center;line-height:30px;width:150px;min-height:30px;font-weight:200;border-radius:4px;font-size:15px;outline:0;border:1px solid #f1910b;letter-spacing:1.5px" alt="" target="_blank">Active My Account</a>
                      </td>
                      </tr>
                      </tbody></table>
                    </div>
                  <br style="margin:0;padding:0">
                  <div style="margin:0;padding:0 15px">
                    </div>
                  <div style="margin:0;padding:0 15px">
                    <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0;text-align:center">
                      <tbody><tr style="margin:0;padding:0">
                      <td style="margin:0;padding:0">
                        <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:13px;line-height:1.6">If above button does not work or is not clickable, please right click on the button and choose the option "Copy link location", open a new tab and paste the copied link in to address bar and hit enter or return.</p>
                        <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;line-height:1.6">Please follow the given instructions. Once you have given all the required information your account will active.</p>
                      </td></tr>
                      </tbody>
                    </table>
                    </div>',
  ],
  'after_nreg'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'mobile','viola_id','img_url','front_url'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}After-register.jpg" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
								<br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><b>Welcome</b> to <span style="color:#68c5df;">{{site_name}}.</span> <span style="display:block;">We are delighted you joined us!!</span></p>
                <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:left;line-height:1.6">Your Virtual <b style="font-weight:500;">{{site_name}}</b> is Generated. Your card <b style="font-weight:500;">details</b> are:</p>
								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>									
									<td width="126"; style="border:2px solid #ddd; padding:15px; height:130px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}img/2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;">Unique Viola ID</h5>
										<p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:10px; background-color:#68c5df; color:#fff;border-radius:5px; padding:3px;">{{viola_id}}</p>
									</td>
									<td width="126"; style="border:2px solid #ddd; padding:15px;height:130px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}img/3.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;">Card Type</h5>
										<p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:10px; background-color:#68c5df; color:#fff;border-radius:5px; padding:3px;">Individual</p>
									</td>
									</tr>
								</table>
								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">You can login to your <b style="font-weight:500;">{{site_name}}</b> by using your Viola ID or registered mobile number. Your <b style="font-weight:500;">{{site_name}}</b> is linked to your Mobile number<b style="font-weight:450; color:#68c5df;"> {{mobile}}.</b> Use the OTP which you will receive on your mobile number, to verify your card. </p>
								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'login_forgotpass'     => [
    'subject'     => 'Your Password Change Request for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'mobile','date','time','img_url','front_url'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}reset_pwd.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								We received a request on your <b style="font-weight:500;">{{site_name}}</b> account to reset the password on <b style="font-weight:450; color:#68c5df;"> {{date}} </b>, at <b style="font-weight:450; color:#68c5df;"> {{time}}</b>.Take a moment and click on the link below to complete your new password request.
								 </p>
								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:center;line-height:1.6"><a href="#" style="color:#68c5df;text-decoration: none;">Password Reset Link</a></p>
								  <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								If clicking the link does not work, don’t worry. Just copy and paste the URL into the browser instead
								 </p>

								  <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'profile_changePass'     => [
    'subject'     => 'Your secure key is changed for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url',),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Password-Changed.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>

                 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 Password for your <b style="font-weight:500;">{{site_name}}</b> account is updated as per your request.</p>
                <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                  If you did not make this request, please contact our Customer Support Team to reset your password immediately.</p>


						<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">Don’t forget, you can always <a href="{{front_url}}contact-us" style="color:#68c5df;">email us.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'dash_blockcards'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','card_number','img_url','front_url','subject'),
    'text'      => ' <div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}block-cards.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									Important notification for your <b style="font-weight:500;">{{site_name}}</b> number
									<b style="font-weight:450; color:#68c5df;"> {{card_number}} </b>
								 </p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                  Your card security is important to us. We are committed to safeguard your Card information to keep it secure and confidential.
								 </p>
								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                  We detected irregular activity/suspicious transaction on your card. We have blocked your card temporarily.
                  <br> Please contact our Customer Support Team using one of the below options:
								 </p>

								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'mypref_chngemobile'     => [
    'subject'     => 'Mobile number changed for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','mobile','img_url','front_url' ),
    'text'      => ' <div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal-Information.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
                <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:#68c5df;">Changed Your Mobile Number!!</span></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 Thank you for updating your mobile. Updated mobile is <b style="font-weight:450; color:#68c5df;"> {{mobile}}.</b><br>
                  <a href="{{front_url}}contact-us" style="color:#68c5df;"> Click Here.</a> to turn on your <b style="font-weight:500;">{{site_name}}</b> notifications to keep yourself updated on the latest from us. </p>

									<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'kyc_idproof'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','mobile','amount','img_url','front_url','subject'),
    'text'      => ' <div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}kyc1.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Your KYC verification is in process. For now, you can use your <b style="font-weight:500;">{{site_name}}</b> only for purchases, by doing a one-time upload of up to <b style="font-weight:450; color:#68c5df;"> &#163; {{amount}} </b>. Withdrawal from your <b style="font-weight:500;">{{site_name}}</b> is restricted, for now.
								 </p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Please contact our Customer Support Team using one of the below options, for any further queries.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/1-1.png" alt="" style="width:50px;">
										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/2.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/3-1.png" alt="" style="width:50px;">
										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a>
									</td>
									</tr>
								</table>
						  <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>  ',
  ],
  'kyc_addresproof'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','mobile','amount','img_url','front_url','subject'),
    'text'      => ' <div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}kyc2.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									Your KYC verification is partially completed. For now, you can use your <b style="font-weight:500;">{{site_name}}</b> only for purchases, by doing multiple uploads of up to <b style="font-weight:450; color:#68c5df;"> &#163; {{amount}} </b>. Withdrawal from your <b style="font-weight:500;">{{site_name}}</b> is restricted, as of now.
								 </p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Please contact our Customer Support Team using one of the below options, for any further queries.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/1-1.png" alt="" style="width:50px;">
										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/2.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="img/3-1.png" alt="" style="width:50px;">
										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a>
									</td>
									</tr>
							</table>
						  <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>  ',
  ],
  'topup_autodebit'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','subject'),
    'text'      => ' <div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}kyc-un.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello Vikas,</b></p>
								<br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:red;">Alert!!</span></p>


                <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.8;margin:auto;">
                Balance in your <b style="font-weight:500;">{{site_name}}</b> has fallen below the set limit. Auto Top Up feature was unsuccessful due to <a style="color:#68c5df;"> network issue </a>,
                <a style="color:#68c5df;"> over limit </a>, <a style="color:#68c5df;"> low balance </a>, <a style="color:#68c5df;"> over limit without KYC. </a> <br> (Please confirm your KYC details).
                </p>

                 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.8;margin:auto;">
                Please make alternate arrangements to load money into your <b style="font-weight:500;">{{site_name}}</b> and enjoy our services.
                </p>

              		<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>  ',
  ],
  'txn_card_debitamount'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','amount','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px;!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}successful-transaction-1.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>

        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									A<b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> is transferred from your <b style="font-weight:500;">{{site_name}}</b> to <b style="font-weight:450; color:#68c5df;">{{fullname}} </b> {{site_name}} account
                <b style="font-weight:450; color:#68c5df;"><br>{{card_number}} </b>
								 </p>



								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								If you did not make this request, please contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'txn_declined'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','amount','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Wrong.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									Your attempt of transferring <b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> from your <b style="font-weight:500;">{{site_name}}</b> to <b style="font-weight:450; color:#68c5df;"> {{fullname}} </b> {{site_name}} account
								 <b style="font-weight:450; color:#68c5df;"> {{card_number}} </b> was unsuccessful.</p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Please try again. For further queries, contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'feedback'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','min_count','max_count','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
               <div style="position:relative" background:#f7f7f7;>
               <img src="img/1.jpeg" style="width:100%;">
               <div style="width:350px;margin:25px auto;background:rgba(255,255,255,0.8); border-radius:5px;padding:15px;color:#fff; text-align:center; position: absolute;top:0px;left:20%;">
               <img src="{{img_url}}v-logo.png" style="width:100%; width:120px;margin:auto;" >
								<p style="color:#333; font-size:12px;margin-top:20px;">Click on one of stars below to rate your experience with us. </p>
									<ul style="padding:0px;margin-bottom:0px;">
										<li style="list-style:none;display:inline-block; margin:5px;"><a href="#"><img src="img/star.png" alt=""></a><span style="display:block;color:#777;">1</span></li>
								<li style="list-style:none;display:inline-block; margin:5px;"><a href="#"><img src="img/star.png" alt=""></a><span style="display:block;color:#777;">2</span></li>
								<li style="list-style:none;display:inline-block; margin:5px;"><a href="#"><img src="img/star.png" alt=""></a><span style="display:block;color:#777;">3</li>
								<li style="list-style:none;display:inline-block; margin:5px;"><a href="#"><img src="img/star.png" alt=""></a><span style="display:block;color:#777;">4</li>
								<li style="list-style:none;display:inline-block; margin:5px;"><a href="#"><img src="img/star.png" alt=""></a><span style="display:block;color:#777;">5</li>
								</ul>
								 </div>
             	</div>
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello {{fullname}},</b></p>
                <br>

                <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:left;line-height:1.6">
								Thank you for using <b>{{site_name}}</b>. We would love to hear from you about your recent experience with us. </p>
								    <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:left;line-height:1.6">Your feedback is really important for us and helps us serve you and others better. Give your opinion by clicking on the numbered stars above, where {{min_count}} represents lowest score and {{max_count}} the highest. We may reach out to you for a low score to get specific feedback.</p>
								<p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:left;line-height:1.6">
									We hope you enjoyed our services. We would like you to click on one of the above numbered stars. There is no lengthy survey or feedback form.
								</p>
								<p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:left;line-height:1.6">
								Please do select the stars above to rate us and press submit.
								</p>

								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;"></span>
							<b style="font-weight:500;">Team</b> <span style="color:#68c5df;font-weight:500;">Viola</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'general_birthwsh'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Birthday.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello {{fullname}},</b></p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                On this special day, we would like to extend our best wishes to you. May this year bring great joy to you. </p>
              <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;color:#333;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							<b style="font-weight:500;">Team</b> <span style="color:#68c5df;font-weight:500;">Viola</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'pay_addmobile'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}family.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                We received a request from {{card_number}} to transfer money into your account. However, since you are not a <b style="font-weight:500;">{{site_name}}</b> user, we cannot transfer the money into your account. Kindly click on the link below to become part of Viola family.
                </p>
                <p><a href="{{front_url}}contact-us" style="color:#68c5df;">link</a></p>

                <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here</a></p>
							<p style="font-weignt:200;text-align:right;color:#888; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'pay_addmobile'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}family.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello {{fullname}},</b></p>
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center; color:red;"><b>Money cannot be sent!!</b> </p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
               We cannot send money to {{card_number}} as your friend is not registered with <b style="font-weight:500;">{{site_name}}</b> </p>

              <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here</a></p>
							<p style="font-weignt:200;text-align:right;color:#888; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'trpleclk_txn_suc'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','amount','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px;!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}successful-transaction-1.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>

        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									A<b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> is transferred from your <b style="font-weight:500;">{{site_name}}</b> to <b style="font-weight:450; color:#68c5df;"> {{fullname}}</b> {{site_name}} account
                <b style="font-weight:450; color:#68c5df;"><br>{{card_number}} </b>
								 </p>



								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								If you did not make this request, please contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{site_name}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'trplclk_unsuc'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','amount','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}tripleclik-1.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
										Your payment of<b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> to Payee <b style="font-weight:450; color:#68c5df;"> {{fullname}} </b>did not go through.
								 </p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Try again later. For further queries, please contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>

									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a>
									</td>


									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							<b style="font-weight:500;">Team</b> <span style="color:#68c5df;font-weight:500;">Viola</span></p>

								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'frm_psp_suc_txn'     => [
    'subject'     => 'Transaction Alert',
    'text_vari'    => array('fullname', 'rec_fullname', 'img_url', 'card_number'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Receiving-Amount.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                A {{amount}} is transferred from your <b style="font-weight:500;">{{site_name}}</b> to {{rec_fullname}} bank account *{{card_number}}.<br>Didn’t make the request?? Please contact our Customer Support Team using one of the below options: </p>

                <table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


               <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'rec_psp_suc_txn'     => [
    'subject'     => 'Transaction Alert',
    'text_vari'    => array('fullname', 'rec_fullname', 'img_url', 'card_number'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Receiving-Amount.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                A {{amount}} is transferred from your <b style="font-weight:500;">{{site_name}}</b> to {{rec_fullname}} bank account *{{card_number}}.<br>Didn’t make the request?? Please contact our Customer Support Team using one of the below options: </p>

                <table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


               <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'psp_unsuc_txn'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}transfering-amount.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                We have transferred amount from your <b style="font-weight:500;">{{site_name}}</b> {{card_number}} to <b style="font-weight:500;">{{site_name}}</b> {{card_number}}. Didn’t make the request?? Please contact our Customer Support Team using one of the below options: </p>

                <table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>

              <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'login_chnge_pin'     => [
    'subject'     => 'Your request to change secure key for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname','img_url','front_url','date','time' ),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal-Information.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
                <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:#68c5df;">Forgot your Viola PIN??</span></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 <b style="font-weight:500;">{{site_name}}</b> received a request to reset the Viola PIN for your account on
                 <b style="font-weight:450; color:#68c5df;"> {{date}}, </b>at <br> <b style="font-weight:450; color:#68c5df;"> {{time}}. </b>
                  Take a moment and click on the link below to complete your New Viola PIN request.</p>
                  <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:center;line-height:1">
                 <a href="#" style="color:#68c5df;text-decoration: none;text-align:center">Viola PIN Reset Link</a>
                 </p>
                  <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                  If clicking the link does not work, don’t worry. Just copy and paste the URL into the browser instead.
                  </p>


              <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
   'add_payee'     => [
    'subject'     => 'Request to add beneficiary for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('payee_name','img_url','front_url','fullname','card_number'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal%20Information.jpg" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								We have added Payee <b style="font-weight:450; color:#68c5df;"> {{payee_name}} </b> to your <b style="font-weight:500;">{{site_name}}</b> number <b style="font-weight:450; color:#68c5df;"> {{card_number}} </b> as per your request.
								 </p>
								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								 Didn’t make the request?? Please contact our Customer Support Team using one of the below options:
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>

									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a>
									</td>


									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a>
									</td>
									</tr>
								</table>
								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>

								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'add_payeefail'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('payee_name','img_url','front_url','fullname','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal%20Information%202.jpg" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
								<br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center; color:red;"><b>Oops! something went wrong.</b> </p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								We are unable to add Payee <b style="font-weight:450; color:#68c5df;"> {{payee_name}} </b> to your <b style="font-weight:500;">{{site_name}}</b> number <b style="font-weight:450; color:#68c5df;"> {{card_number}} </b>
								 </p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								 For further information, please contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>

									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a>
									</td>


									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">

										<a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a>
									</td>
									</tr>
								</table>

								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'pay_suc_txn'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('amount','img_url','front_url','fullname','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px;!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}successful-transaction-1.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>

        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									A<b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> is transferred from your <b style="font-weight:500;">{{site_name}}</b> to <b style="font-weight:450; color:#68c5df;"> {{fullname}} </b> {{site_name}} account
                <b style="font-weight:450; color:#68c5df;"><br>{{card_number}} </b>
								 </p>



								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								If you did not make this request, please contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'pay_unsuc_txn'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('amount','img_url','front_url','fullname','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Wrong.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									Your attempt of transferring <b style="font-weight:450; color:#68c5df;"> &#163;{{amount}} </b> from your <b style="font-weight:500;">{{site_name}}</b> to <b style="font-weight:450; color:#68c5df;"> {{fullname}} </b> {{site_name}} account
								 <b style="font-weight:450; color:#68c5df;"> {{card_number}} </b> was unsuccessful.</p>

								 <p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
								Please try again. For further queries, contact our Customer Support Team using one of the below options.
								 </p>

								<table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


								<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'pay_unreg_user'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_ur}}family.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hello {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                We received a request from {{card_number}} to transfer money into your account. However, since you are not a <b style="font-weight:500;">{{site_name}}</b> user, we cannot transfer the money into your account. Kindly click on the link below to become part of Viola family.
                </p>
                <p><a href="{{front_url}}contact-us" style="color:#68c5df;">link</a></p>

                <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here</a></p>
							<p style="font-weignt:200;text-align:right;color:#888; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'move_funds'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','card_number','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Receiving-Amount.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>



								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                We have received amount from <b style="font-weight:500;">{{site_name}}</b>{{card_number}}to your <b style="font-weight:500;">{{site_name}}</b> {{card_number}}.<br>Didn’t make the request?? Please contact our Customer Support Team using one of the below options: </p>

                <table style="letter-spacing:.3px;display:block;margin:15px 10px;text-align:center; width:100%;" cellspacing="10">
								<tr>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}1-1.png" alt="" style="width:50px;">
                    <h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Live Chat</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px; height:80px;margin:0px 5px 0px; 0px; border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}2.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Email</a></h5>
									</td>
									<td width="116"; style="border:2px solid #ddd; padding:15px;height:80px;margin:0px 0px 0px 5px;border-top:2px solid #68c5df;border-bottom:2px solid #68c5df;border-radius:5px; vertical-align: top;">
										<img src="{{img_url}}3-1.png" alt="" style="width:50px;">
										<h5 style="margin:15px 0px;"><a href="#" style="margin:0;padding:0;margin-top:10px;color:#888;font-weight:200;font-size:12px; background-color:#68c5df; color:#fff;border-radius:5px; padding:5px; display:block; text-decoration: none;">Call Me</a></h5>
									</td>
									</tr>
								</table>


               <p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div>',
  ],
  'rqst_cards_cnfmrtn'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
              <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0" >
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">

                <img src="{{img_url}}city-3.jpg" alt="" style="width:100%;">

              </td>
            </tr>
          </tbody>
					</table>
          </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
								<br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center; color:#68c5df;"><b>Viola Card Request!</b> </p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									Thank you for contacting us for Viola Walelt. Once the card is dispatched, we will send you an email confirmation with a link to track your Card.
								 </p>
					<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'rqst_cards_sent'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}city.gif" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi {{fullname}},</b></p>
								<br style="margin:0;padding:0"><p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center; color:#68c5df;"><b>Plastic Card Dispatched!</b> </p>

								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
									We are happy to inform you that we have dispatched the Plastic Card to your address as per your request. You can check the status of your Card by
									<a href="{{front_url}}signin" style="font-weight:450; color:#68c5df;"> logging into your account </a>
								 </p>

								  	<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="http://uat.violacards.in/front/contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'chnge_viola_id'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','mobile','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal-Information.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi Vikas,</b></p>
                <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:#68c5df;">Changed Your Mobile Number!!</span></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 Thank you for updating your mobile number. It is changed to <b style="font-weight:450; color:#68c5df;"> {{mobile}}.</b><br>
                  <a href="{{front_url}}contact-us" style="color:#68c5df;"> Click Here.</a> to turn on your <b style="font-weight:500;">{{site_name}}</b> notifications to keep yourself updated on the latest from us. </p>

									<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'add_nominee'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','mobile','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal-Information.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi Vikas,</b></p>
                <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:#68c5df;">Changed Your Mobile Number!!</span></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 Thank you for updating your mobile number. It is changed to <b style="font-weight:450; color:#68c5df;"> {{mobile}}.</b><br>
                  <a href="{{front_url}}contact-us" style="color:#68c5df;"> Click Here.</a> to turn on your <b style="font-weight:500;">{{site_name}}</b> notifications to keep yourself updated on the latest from us. </p>

									<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'nominee_chnge'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('img_url','front_url','fullname','mobile','subject'),
    'text'      => '<div style="margin:0;padding:0px; ">
          <table cellspacing="0" cellpadding="0" style="margin:0;padding:0;width:100%">
            <tbody><tr style="margin:0;padding:0">
              <td bgcolor="#fff" style="margin:0 auto!important;padding:0;background:#fff;display:block!important;max-width:800px!important;clear:both!important;border-top:2px solid #68c5df">
                <img src="{{img_url}}Personal-Information.png" alt="" style="width:100%;">
              </td>
            </tr>
          </tbody>
					</table>
        </div>
        <div style="margin:0;padding:0 15px;">
          <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
            <tbody>
              <tr style="margin:0;padding:0">
              <td style="margin:0;padding:0">
                <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;"><b>Hi Vikas,</b></p>
                <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:22px;line-height:1.6;text-align:center;"><span style="display:block; color:#68c5df;">Changed Your Mobile Number!!</span></p>


								<p style="padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;text-align:justify;line-height:1.6">
                 Thank you for updating your mobile number. It is changed to <b style="font-weight:450; color:#68c5df;"> {{mobile}}.</b><br>
                  <a href="{{front_url}}contact-us" style="color:#68c5df;"> Click Here.</a> to turn on your <b style="font-weight:500;">{{site_name}}</b> notifications to keep yourself updated on the latest from us. </p>

									<p style="font-weignt:200;color:#888;text-align:left; margin:10px 0px; font-size:13px;">For any queries please contact us <a href="{{front_url}}contact-us" style="color:#68c5df;">here.</a></p>
							<p style="font-weignt:200;text-align:right; font-size:13px;">
								<span style="margin-bottom:2px;color:#888;!important;display:block;">Giving you the best service always!</span>
							Team <span style="color:#68c5df;">Viola.</span></p>
								</td>
               </tr>
          </tbody>
					</table>
        </div> ',
  ],
  'non_kycUser'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Welcome to {{site_name}}. We are delighted you joined us! Submit your KYC documents for verification
                    </div>',
  ],
  'login_otp'     => [
    'subject'     => 'Your login OTP for <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  You have logged in from a new device. If its not you, kindly contact our Customer Support Team immidiately.
                    </div>',
  ],
  'dash_restrctcards'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  The template would be same, content is "Important notification for your Viola Wallet number {{card_number}}
As per your request Card number {{card_number}} is restricted to online shopping
For any queries please contact us here."
                    </div>',
  ],
  'dash_reqcards'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Thank you for contacting us. As per your request, we have initiated the process of providing (number) Corporate / Individual / Travel Card into your account.
                    </div>',
  ],
  'topup_creditamount'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url','amount','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{fullname}},
£{{amount}} is successfully credited to your {{site_name}} {{card_number}}
If you did not make this request, please contact our Customer Support Team using one of the below options.
                    </div>',
  ],
  'txn_card_debitamount'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url','amount','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{fullname}},
£{{amount}} is successfully credited to your {{site_name}} {{card_number}}
If you did not make this request, please contact our Customer Support Team using one of the below options.
                    </div>',
  ],
  'topup_debtcard_creditamount'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url','amount','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{fullname}},
£{{amount}} is successfully credited to your {{site_name}} {{card_number}}
If you did not make this request, please contact our Customer Support Team using one of the below options.
                    </div>',
  ],
  'topup_bank_amountcredit'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url','amount','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{fullname}},
£{{amount}} is successfully credited to your {{site_name}} {{card_number}}
If you did not make this request, please contact our Customer Support Team using one of the below options.
                    </div>',
  ],
  'topup_paypoint_amountcredit'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'activation_url','amount','card_number'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{fullname}},
£{{amount}} is successfully credited to your {{site_name}} {{card_number}}
If you did not make this request, please contact our Customer Support Team using one of the below options.
                    </div>',
  ],
  'topup_monthlyplan'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject', 'amount','card_number','date','customer_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{customer_name}}
Alert!!
Your monthly frequency is set to £{{amount}} for the {{date}} of each month. Please maintain balance into your account for the payment to go through successfully.
For any queries please contact us here.
                    </div>',
  ],
  'help_assist'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject','customer_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{customer_name}}
We value you!
Thank you for contacting {{site_name}}. Your query is registered with us and we will contact you shortly.
                    </div>',
  ],
  'help_phone'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject','customer_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                  Hello {{customer_name}}
We value you!
Thank you for contacting {{site_name}}. Your query is registered with us and we will contact you shortly.
                    </div>',
  ],
  'help_livechat'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array('fullname', 'subject','customer_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                 Hello {{customer_name}}
Happy to help you!
We hope we were able to resolve your issue with Ticket number (dfhhglfhglfk). It was about {{subject}}.
We are constantly striving to provide excellent service to our customers
                    </div>',
  ],
   'trpleclk_addtrpeclk'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array( 'subject',),
    'text'      => '<div style="margin:0;padding:0 15px">
                mails ready-links need to be updated
                    </div>',
  ],
  'trpleclk_remove'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array( 'subject','fullname','payee_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                Hello {{fullname}},
As per your request we have deleted {{payee_name}} from your TripleClik list.
Deleted by mistake?? Go to the Payee section and add the <Name> back to your TripleClik List.
                    </div>',
  ],
  'notf_setngs'     => [
    'subject'     => 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>',
    'text_vari'    => array( 'subject','customer_name'),
    'text'      => '<div style="margin:0;padding:0 15px">
                Hello! {{customer_name}}
Your Notifications alert is Activated / Deactivated
Your Transactions alert is Activated / Deactivated
Your Email alert is Activated / Deactivated
Your In App Notifications are Activated / Deactivated
Your Do Not Disturb alert is Activated / Deactivated
To make changes, kindly use the toggle button.
                    </div>',
  ],

];

/* 2 email text for quick register qreg = quick register*/
$lang['qreg_email_subject'] = 'Thank you for registering with <span style="color:#f1910b">{{site_name}}</span>';
$lang['qreg_email_text'] = '<div style="margin:0;padding:0 15px">
            <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
              <tbody>
                <tr style="margin:0;padding:0">
                  <td style="margin:0;padding:0">
                    <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;font-weight:200;letter-spacing:.5;font-size:15px;line-height:1.6;">Hi {{fullname}},</p>
                    <br style="margin:0;padding:0"><p style="margin:0;padding:0;color:#888;margin-bottom:10px;font-weight:200;font-size:18px;line-height:1.6;text-align:center;">{{subject}}</span>. </p>
                    <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;line-height:1.6;text-align:center;"> Your account is successfully created but it\'s not been activated yet. </p>
                    <p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;line-height:1.6;text-align:center;">
                      To activate your account, please click on the button below and you will be directed to the activation.
                    </p>

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br style="margin:0;padding:0">
          <div style="margin:0;padding:0 15px">
            <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
              <tbody>
                <tr style="margin:0;padding:0">
                  <td width="100%" align="center" style="margin:0;padding:0;margin-bottom:15px;font-size:12px;min-width:100px;max-width:100%;display:inline-block;vertical-align:top">
                    <a href="{{activation_url}}" target="_blank" style="margin:0;padding:5px 10px;color:#f1910b;display:inherit;text-decoration:none;text-align:center;line-height:30px;width:150px;min-height:30px;font-weight:200;border-radius:4px;;font-size:15px;outline:0;border:1px solid #f1910b; letter-spacing:1.5px;" alt="">
                      Activate Now
                    </a>
                  </td>
                </tr>
              </tbody></table>
          </div>
          <br style="margin:0;padding:0">
          <div style="margin:0;padding:0 15px">

          </div>
          <div style="margin:0;padding:0 15px">
            <table width="100%" cellspacing="0" cellpadding="0" style="margin:0;padding:0; text-align:center;">
              <tbody><tr style="margin:0;padding:0">
                  <td style="margin:0;padding:0">
                    <p style="margin:0;padding:0;margin-bottom:10px;color:#888;font-weight:200;font-size:13px;line-height:1.6">If above button will not work or not clickable for you, please right click on the button and choose the option "Copy link location" start a new tab and past the copied link in to address bar and hit enter or return.</p>
        			<p style="margin:0;padding:0;margin-bottom:5px;color:#888;font-weight:200;font-size:13px;line-height:1.6">An account password is needed to activate your account. Since you used the Quick Register option, a temporary system generated password is given below. Use this to activate or access your account. You can immediately change your password, of your choice via section "My Account Preferences".
                    </p>
                    <div style="text-align:center; font-size:16px;">
                      <p style="border:1px solid #F1910B; color:#000; text-decoration:none; padding:10px 15px;">{{password}}</p>
                    </div>
                </tr>
              </tbody>
            </table>
          </div>';
$lang['qreg_email_text_vari'] = array('fullname','subject','activation_url');

/* 3 email text for business register breg = business register*/
$lang['breg_email_subject'] = '';
$lang['breg_email_text'] = '';
$lang['breg_email_text_veri'] = '';

/* 4 email text for actication of all account aaact = activate all account*/
$lang['aaact_email_subject'] = '';
$lang['aaact_email_text'] = '';
$lang['aaact_email_text_veri'] = '';

/* 5 email text for forgot Password forpas = forgot password*/
$lang['forpas_email_subject'] = '';
$lang['forpas_email_text'] = '';
$lang['forpas_email_text_veri'] = '';

/* 6 email text for forgot Password forpin = forgot pin*/
$lang['forpin_email_subject'] = '';
$lang['forpin_email_text'] = '';
$lang['forpin_email_text_veri'] = '';

/* 7 email text for forgot PIN forpin = forgot pin*/
$lang['forpin_email_subject'] = '';
$lang['forpin_email_text'] = '';
$lang['forpin_email_text_veri'] = '';

/* 8 email text for Change PIN cpin = Change pin*/
$lang['cpin_email_subject'] = '';
$lang['cpin_email_text'] = '';
$lang['cpin_email_text_veri'] = '';

/* 9 email text for Change Password cpass = Change Password*/
$lang['cpass_email_subject'] = '';
$lang['cpass_email_text'] = '';
$lang['cpass_email_text_veri'] = '';

/* 10 email text for Change Mobile cmob = Change Mobile*/
$lang['cmob_email_subject'] = '';
$lang['cmob_email_text'] = '';
$lang['cmob_email_text_veri'] = '';

/* 11 email text for Change Email cmail = Change Email*/
$lang['cmail_email_subject'] = '';
$lang['cmail_email_text'] = '';
$lang['cmail_email_text_veri'] = '';

/* 12 email text for Change Address cadd = Change Address*/
$lang['cadd_email_subject'] = '';
$lang['cadd_email_text'] = '';
$lang['cadd_email_text_veri'] = '';

/* 13 email text for Change Telephone ctel = Change Telephone*/
$lang['ctel_email_subject'] = '';
$lang['ctel_email_text'] = '';
$lang['ctel_email_text_veri'] = '';

/* 14 email text for Change Name Request cnreq = Change name request*/
$lang['cnreq_email_subject'] = '';
$lang['cnreq_email_text'] = '';
$lang['cnreq_email_text_veri'] = '';

/* 15 email text for Change Currency Watch ccur = Change Currency Watch*/
$lang['ccur_email_subject'] = '';
$lang['ccur_email_text'] = '';
$lang['ccur_email_text_veri'] = '';

/* 16 email text for Add Beneficiary aben = Add Beneficiary*/
$lang['aben_email_subject'] = '';
$lang['aben_email_text'] = '';
$lang['aben_email_text_veri'] = '';

/* 17 email text for Add Second Mobile amob = Add Second Mobile*/
$lang['amob_email_subject'] = '';
$lang['amob_email_text'] = '';
$lang['amob_email_text_veri'] = '';

/* 18 email text for Add Additional Email email = Add Additional Email*/
$lang['amail_email_subject'] = '';
$lang['amail_email_text'] = '';
$lang['amail_email_text_veri'] = '';

/* 19 email text for Wallet to Wallet Receive wtwr = Wallet to Wallet Receive*/
$lang['wtwr_email_subject'] = '';
$lang['wtwr_email_text'] = '';
$lang['wtwr_email_text_veri'] = '';

/* 20 email text for Wallet to Wallet Send wtws = Wallet to Wallet Send*/
$lang['wtws_email_subject'] = '';
$lang['wtws_email_text'] = '';
$lang['wtws_email_text_veri'] = '';

/* 21 email text for Move Money Within My Wallet mmmw = Move Money Within My Wallet*/
$lang['mmmw_email_subject'] = '';
$lang['mmmw_email_text'] = '';
$lang['mmmw_email_text_veri'] = '';

/* 22 email text for Move Money Within My Wallet wtw = Wallet to wallet transaction.*/
$lang['wtw_email_subject'] = '';
$lang['wtw_email_text'] = '';
$lang['wtw_email_text_veri'] = '';

/* 23 email text for Move Money Within My Wallet wtwe = Wallet to wallet exchange transaction.*/
$lang['wtwe_email_subject'] = '';
$lang['wtwe_email_text'] = '';
$lang['wtwe_email_text_veri'] = '';

/* 25 email text for Move Money Within My Wallet wtwt = Wallet to wallet TripleClik transaction.*/
$lang['wtwt_email_subject'] = '';
$lang['wtwt_email_text'] = '';
$lang['wtwt_email_text_veri'] = '';

/* 26 email text for Move Money Within My Wallet wtwto = Wallet to wallet TripleClik Other beneficiary transaction.*/
$lang['wtwto_email_subject'] = '';
$lang['wtwto_email_text'] = '';
$lang['wtwto_email_text_veri'] = '';

/* 27 email text for Move Money Within My Wallet wtwte = Wallet to wallet TripleClik exchange transaction.*/
$lang['wtwte_email_subject'] = '';
$lang['wtwte_email_text'] = '';
$lang['wtwte_email_text_veri'] = '';

/* 28 email text for Move Money Within My Wallet wtwtoe = Wallet to wallet TripleClik Other beneficiary exchange transaction.*/
$lang['wtwtoe_email_subject'] = '';
$lang['wtwtoe_email_text'] = '';
$lang['wtwtoe_email_text_veri'] = '';

/* End of file textemail_alerts_lang.php */
