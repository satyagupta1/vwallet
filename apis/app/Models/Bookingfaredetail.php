<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bookingfaredetail
 * 
 * @property string $FareDetailsId
 * @property int $BookingMasterId
 * @property float $BaseFare
 * @property float $ServiceCharges
 * @property float $Tax
 * @property float $TollGate
 * @property float $ReservChrgs
 * @property float $OnlineFee
 * @property float $CancellationFee
 * @property float $RefundAmt
 * @property float $ConcessionAmt
 * @property string $ConcessionType
 * @property string $Remarks
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Bookingmaster $bookingmaster
 *
 * @package App\Models
 */
class Bookingfaredetail extends Eloquent
{
	protected $primaryKey = 'FareDetailsId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BookingMasterId' => 'int',
		'BaseFare' => 'float',
		'ServiceCharges' => 'float',
		'Tax' => 'float',
		'TollGate' => 'float',
		'ReservChrgs' => 'float',
		'OnlineFee' => 'float',
		'CancellationFee' => 'float',
		'RefundAmt' => 'float',
		'ConcessionAmt' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BookingMasterId',
		'BaseFare',
		'ServiceCharges',
		'Tax',
		'TollGate',
		'ReservChrgs',
		'OnlineFee',
		'CancellationFee',
		'RefundAmt',
		'ConcessionAmt',
		'ConcessionType',
		'Remarks',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bookingmaster()
	{
		return $this->belongsTo(\App\Models\Bookingmaster::class, 'BookingMasterId');
	}
}
