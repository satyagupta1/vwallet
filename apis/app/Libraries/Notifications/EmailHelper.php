<?php

/*
 * created by Anand Kumar Akurathi
 * 
 * referance link: https://github.com/PHPMailer/PHPMailer/
 */

namespace App\Libraries\Notifications;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Dbugphpmailer;

class EmailHelper {

    static protected $host           = 'smtp.gmail.com';
    static protected $userName       = 'violawalletvw@gmail.com';
    static protected $password       = 'V!L@wwlet#21457';
    static protected $port           = 587;
    static protected $fromEmail      = 'violawalletvw@gmail.com';
    static protected $senderName     = 'Viola Wallet';
    static protected $replyBackEmail = 'violawalletvw@gmail.com';
    static protected $mailSecurity   = 'tls';

    public static function sendEmail($toEmail, $subject, $message, $notificationId, $files = array(), $plainMsg = '')
    {
        // Mail Log variables
        $requestId = uniqid();
        $startTime = microtime(TRUE);

        // Log Messge 
        $requestDetails = 'Mail Request ID : ' . $requestId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ',Subject:' . $subject . ', Templet : ' . json_encode($message, JSON_PRETTY_PRINT);
        \Log::info('Reponse:  ' . $requestDetails);

        $notifyUpdate = array(
            'ReceipentReceivedDate' => date('Y-m-d H:i:s')
        );

        $mail = new PHPMailer(TRUE);                            // Passing `true` enables exceptions
        try
        {
            // Server settings
            $mail->SMTPDebug   = FALSE;                         // Enable verbose debug output
            $mail->isSMTP();                                   // Set mailer to use SMTP
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'       => FALSE,
                    'verify_peer_name'  => FALSE,
                    'allow_self_signed' => TRUE
                )
            );
            $mail->Host        = EmailHelper::$host;            // Specify main and backup SMTP servers
            $mail->SMTPAuth    = TRUE;                          // Enable SMTP authentication
            $mail->Username    = EmailHelper::$userName;        // SMTP username
            $mail->Password    = EmailHelper::$password;        // SMTP password
            $mail->SMTPSecure  = EmailHelper::$mailSecurity;    // Enable TLS encryption, `ssl` also accepted
            $mail->Port        = EmailHelper::$port;            // TCP port to connect to
            // Recipients
            $mail->setFrom(EmailHelper::$fromEmail, EmailHelper::$senderName);
            $mail->addAddress($toEmail);                        // Name is optional addAddress('joe@example.net', 'Joe User')
            if ( EmailHelper::$replyBackEmail )
            {
                $mail->addReplyTo(EmailHelper::$replyBackEmail, EmailHelper::$senderName);
            }

            // Attachments
            if ( $files && is_array($files) )
            {
                foreach ( $files as $value )
                {
                    // check file exsists
                    $exists = Storage::disk('local')->exists($value);
                    if ( $exists )
                    {
                        // file location
                        $file = storage_path($value);
                        $mail->addAttachment($file);         // Add attachments. file name Optional addAttachment('/tmp/image.jpg', 'new.jpg')
                    }
                }
            }

            // Content
            $mail->isHTML(TRUE);                             // Set email format to HTML
            $mail->Subject = $subject;
            $mail->MsgHTML($message);
            $mail->AltBody = ($plainMsg) ? $plainMsg : strip_tags($message);

            // send Mail
            $mail->send();
            $requestDetails                          = 'Mail Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . 'Message Status: Sent';
            \Log::info('Reponse:  ' . $requestDetails);
            $notifyUpdate['ReceipentAcknowledged']   = 'Y';
            $notifyUpdate['ReceipentDeliveryReport'] = 'Mail Sent';
        }
        catch ( phpmailerException $e )
        {
            // Log Messge 
            $requestDetails                          = 'Mail Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . 'Templet : ' . json_encode($e, JSON_PRETTY_PRINT);
            \Log::info('Reponse:  ' . $requestDetails);
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = $mail->ErrorInfo;
        }

        \App\Models\Notificationlog::where('NotificationID', $notificationId)->update($notifyUpdate);
        return TRUE;
    }

}
