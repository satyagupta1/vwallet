<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Userwallet
 * 
 * @property int $UserWalletID
 * @property string $UserName
 * @property string $MobileNumber
 * @property string $EmailID
 * @property string $Gender
 * @property string $Password
 * @property string $TPin
 * @property \Carbon\Carbon $DateofBirth
 * @property int $MasterUserId
 * @property string $ProfileStatus
 * @property int $WalletPlanTypeId
 * @property string $IsVerificationCompleted
 * @property string $PreferredPrimaryCurrency
 * @property string $PreferredSecondaryCurrency
 * @property string $IsPriorityUser
 * @property string $IsSubAccount
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $SocialMediaRegistration
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Walletplantype $wallettype
 * @property \App\Models\Currency $currency
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $partycontacts
 * @property \App\Models\Useraccountclosure $useraccountclosure
 * @property \App\Models\Userautoloaddetail $userautoloaddetail
 * @property \Illuminate\Database\Eloquent\Collection $userbeneficiaries
 * @property \Illuminate\Database\Eloquent\Collection $userconfigurations
 * @property \Illuminate\Database\Eloquent\Collection $usernomineedetails
 * @property \App\Models\Userregistration $userregistration
 * @property \App\Models\Userstandinginstruction $userstandinginstruction
 * @property \App\Models\Userupdatetracker $userupdatetracker
 * @property \Illuminate\Database\Eloquent\Collection $userusedoffers
 * @property \App\Models\Userverificationtracker $userverificationtracker
 * @property \Illuminate\Database\Eloquent\Collection $userwallets
 * @property \Illuminate\Database\Eloquent\Collection $userwarrenties
 * @property \Illuminate\Database\Eloquent\Collection $walletaccounts
 *
 * @package App\Models
 */
class Userwallet extends Eloquent implements AuthenticatableContract, AuthorizableContract, JWTSubject {

    use Authenticatable,
        Authorizable;

    protected $table      = 'userwallet';
    protected $primaryKey = 'UserWalletID';
    public $incrementing  = false;
    public $timestamps    = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    
    protected $casts = [
        'UserWalletID' => 'int',
        'MasterUserId' => 'int',
        'WalletPlanTypeId' => 'int'
    ];
    protected $dates = [
        'DateofBirth' => 'Y-m-d',
        'CreatedDateTime',
        'UpdatedDateTime'
    ];
    protected $fillable = [
        'UserWalletID',
        'FirstName',
        'LastName',
        'MobileNumber',
        'ViolaID',
        'EmailID',
        'Gender',
        'Password',
        'TPin',
        'DateofBirth',
        'MasterUserId',
        'ProfileStatus',
        'WalletPlanTypeId',
        'IsVerificationCompleted',
        'PreferredPrimaryCurrency',
        'PreferredSecondaryCurrency',
        'IsPriorityUser',
        'IsSubAccount',
        'CreatedDateTime',
        'CreatedBy',
        'UpdatedDateTime',
        'UpdatedBy',
        'SocialMediaRegistration',
        'SocialLoginRefNo'
    ];

    public function userwallet()
    {
        return $this->belongsTo(\App\Models\Userwallet::class, 'MasterUserId');
    }

    public function walletplantype()
    {
        return $this->belongsTo(\App\Models\Walletplantype::class, 'WalletPlanTypeId');
    }

    public function currency()
    {
        return $this->belongsTo(\App\Models\Currency::class, 'PreferredSecondaryCurrency');
    }

    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class, 'UserWalletID');
    }

    public function partycontacts()
    {
        return $this->hasMany(\App\Models\Partycontact::class, 'PartyId');
    }

    public function useraccountclosure()
    {
        return $this->hasOne(\App\Models\Useraccountclosure::class, 'UserID');
    }

    public function userautoloaddetail()
    {
        return $this->hasOne(\App\Models\Userautoloaddetail::class, 'UserWalletId');
    }

    public function userbeneficiaries()
    {
        return $this->hasMany(\App\Models\Userbeneficiary::class, 'UserWalletID');
    }

    public function userconfigurations()
    {
        return $this->hasMany(\App\Models\Userconfiguration::class, 'UserWalletID');
    }

    public function usernomineedetails()
    {
        return $this->hasMany(\App\Models\Usernomineedetail::class, 'UserWalletID');
    }

    public function userregistration()
    {
        return $this->hasOne(\App\Models\Userregistration::class, 'UserWalletID');
    }

    public function userstandinginstruction()
    {
        return $this->hasOne(\App\Models\Userstandinginstruction::class, 'UserWalletId');
    }

    public function userupdatetracker()
    {
        return $this->hasOne(\App\Models\Userupdatetracker::class, 'UserWalletID');
    }

    public function userusedoffers()
    {
        return $this->hasMany(\App\Models\Userusedoffer::class, 'UserWalletID');
    }

    public function userverificationtracker()
    {
        return $this->hasOne(\App\Models\Userverificationtracker::class, 'UserWalletID');
    }

    public function userwallets()
    {
        return $this->hasMany(\App\Models\Userwallet::class, 'MasterUserId');
    }

    public function userwarrenties()
    {
        return $this->hasMany(\App\Models\Userwarrenty::class, 'UserWalletID');
    }

    public function walletaccounts()
    {
        return $this->hasMany(\App\Models\Walletaccount::class, 'UserWalletId');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
