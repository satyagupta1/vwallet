<?php

namespace App\Libraries;

class BilldeskHelper {
    /*
     *  generateHMAC - This method is used to generate Hmac jey for bill desk apis
     *  
     * @param $httpMethod 
     * @param $traceId
     * @param $dateTime
     * 
     *  @return string
     */

    public static function generateHMAC($arrayparams)
    {
        return $hmac = hash_hmac('sha256', $arrayparams, config('vwallet.billDeskPassword'), FALSE);  // genaration of hash hmac
    }

    public static function generateHMAC1($arrayparams)
    {
        return $hmac = hash_hmac('sha256', $arrayparams, config('vwallet.billDeskPassword'));  // genaration of hash hmac
    }

    /*
     *  Get Circle Name - 
     *  
     * @param $httpMethod 
     * @param $traceId
     * @param $dateTime
     * 
     *  @return string
     */

    public static function getCircleName($id)
    {

        $getCircleName = \App\Models\Circlemaster::select('Master')->where('Circle_Id', $id)->first();
        return ( ! empty($getCircleName)) ? $getCircleName->Master : FALSE;
    }

    /*
     *  getTransctionId - Get unique transction Id
     *  @return integer
     */

    public static function gettraceId()
    {

        return uniqid();
    }

    /*
     *  billDeskCURL - This method is used to connect with bill desk api url using curl method
     *  
     * @param $url 
     * @param $httpMethod
     * @param $headersArray
     * 
     *  @return Array
     */

    public static function billDeskGETCURL($url, $logId = '')
    {
        $timeOut = config('vwallet.curlTime');
        $curl    = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT        => $timeOut,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST  => 'GET',
                //   CURLOPT_HTTPHEADER     => $headersArray,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ( $err )
        {
            if ( $logId )
            {
                $logRequest = array(
                    'response' => "cURL Error #:" . $err,
                    'status'   => 'Failed'
                );
                \App\Libraries\LogHelper::createLog($logRequest, $logId);
            }
            else
            {
                return "cURL Error #:" . $err;
            }
            return FALSE;
        }
        else
        {
            return $response;
        }
    }

    public static function billDeskPOSTCURL($url, $httpMethod, $headersArray)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS     => "{\r\n\t\"billerid\": \"AIRTELPRE\",\r\n\t\"authenticators\": \r\n\t[\r\n\t\t{\r\n\t\t\t\"parameter_name\": \"Mobile Number\",\r\n\t\t\t\"value\": \"9004381057\"\r\n\t\t}\r\n\t],\r\n\t\"payment_amount\": \"300.00\",\r\n\t\"currency\": \"356\",\r\n\t\"customer\": \r\n\t{\r\n\t\t\"firstname\": \"ABC\",\r\n\t\t\"lastname\": \"XYZ\",\r\n\t\t\"mobile\": \"9344895862\",\r\n\t\t\"mobile_alt\": \"9859585525\",\r\n\t\t\"email\": \"abc@billdesk.com\",\r\n\t\t\"email_alt\": \"abc2@billdesk.com\",\r\n\t\t\"pan\": \"BZABC1234L\",\r\n\t\t\"aadhaar\": \"123123123123\"\r\n\t},\r\n\t\"metadata\": \r\n\t{\r\n\t\t\"agent\": \r\n\t\t{\r\n\t\t\t\"agentid\": \"BD001MPY100000100001\"\r\n\t\t},\r\n\t\t\"device\": \r\n\t\t{\r\n\t\t\t\"init_channel\": \"Internet\",\r\n\t\t\t\"ip\": \"124.124.1.1\",\r\n\t\t\t\"mac\": \"11-AC-58-21-1B-AA\"\r\n\t\t}\r\n\t},\r\n\t\"risk\":\r\n\t[\r\n\t    {\r\n\t      \"score_provider\": \"OU01\",\r\n\t      \"score_value\": \"030\",\r\n\t      \"score_type\": \"TXNRISK\"\r\n\t    },\r\n\t    {\r\n\t      \"score_provider\": \"BBPS\",\r\n\t      \"score_value\": \"030\",\r\n\t      \"score_type\": \"TXNRISK\"\r\n\t    }\r\n\t]\r\n}",
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0),
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0),
            CURLOPT_CUSTOMREQUEST  => $httpMethod,
            CURLOPT_HTTPHEADER     => $headersArray,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ( $err )
        {
            return "cURL Error #:" . $err;
        }
        else
        {
            return $response;
        }
    }

    // code for Billdesk helper
    public static function userBillerTransactions($arrayparams, $userBillerParams, $request, $txndedtailId, $subscriberNum, $bbpsRefId, $authenticatorsObject, $valid)
    {

        // insert into biller transaction
        $insArray = array(
            'UserWalletID'          => $userBillerParams['CustomerID'],
            'WalletAccountId'       => self::GetWalletUserData($userBillerParams['CustomerID'])['WalletAccountId'],
            'BillerCategory'        => self::getBillerCategory($request->input('billerId'))['BillerCategory'],
            'Status'                => $userBillerParams['PaymentStatus'],
            'TransactionDetailID'   => $txndedtailId,
            'PaymentProductID'      => self::getBillerCategory($request->input('billerId'))['PaymentsProductID'],
            'BillDeskTransactionID' => $userBillerParams['TransactionId'],
            'ValidationID'          => $bbpsRefId,
            'Authenticators'        => $authenticatorsObject,
            'Valid'                 => $valid,
            'SubscriberNumber'      => $subscriberNum,
            'RechargeBillerId'      => $request->input('billerId'),
            'CreatedBy'             => 'User',
            'UpdatedBy'             => '',
        );
        $insertId = \App\Models\Userbillertransaction::insertGetId($insArray);
        // insertion in txn details
        foreach ( $arrayparams as $key => $val )
        {
            $insertBilltxnDetails                     = new \App\Models\Userbillertransdetail;
            $insertBilltxnDetails->UserBillerTransrid = $insertId;
            $insertBilltxnDetails->TagName            = $key;
            $insertBilltxnDetails->TagValue           = $val;
            $insertBilltxnDetails->save();
        }

        return $txndedtailId;
    }

    public static function GetWalletUserData($walletId)
    {
        $user_data = \App\Models\Walletaccount::
                select('WalletAccountId')
//                 ->where('walletaccount.IsAdminWallet', '=', 'Y')
                ->where('UserWalletId', $walletId)
                ->first();
        return ($user_data) ? $user_data : 0;
    }

    public static function getBillerCategory($billerId)
    {

        $userBiller_data = \App\Models\Billdeskbiller::join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billdeskbillers.BillerID')
                ->select('billdeskbillers.BillerCategory', 'billdeskbillers.BillerName', 'paymentsproduct.PaymentsProductID')
//                 ->where('walletaccount.IsAdminWallet', '=', 'Y')
                ->where('billdeskbillers.BillerID', $billerId)
                ->first();

        return ($userBiller_data) ? $userBiller_data : 0;
    }

    public static function getBillerProdEventId($billerId)
    {
        $userBiller_data = \App\Models\Paymentsproducteventdetail::
                select('PaymentProductEventID')
                ->where('EventCode', $billerId)
                ->first();
        return ($userBiller_data) ? $userBiller_data : 0;
    }

    /**
     * 
     * @param type $request
     */
    public static function getBillPayment($request)
    {

        $sourceid    = config('vwallet.sourceId');
        $txnId       = self::gettraceId(); // gen of transaction Id
        $dateTime    = config('vwallet.bdTimeStamp'); // gen of dateTime
        $arrayparams = array(
            'MessageCode'      => 'U07008', // this id is default code in billdesk apis
            'TraceID'          => $txnId,
            'SourceID'         => $sourceid,
            'TimeStamp'        => $dateTime,
            'UserID'           => config('vwallet.billdeskUserId'),
            'CustomerID'       => $request->input('partyId'),
            'RechargeBillerId' => $request->input('billerId'),
            'Authenticator1'   => $request->input('authenticator1'),
            'Authenticator2'   => empty($request->input('authenticator2')) ? 'NA' : $request->input('authenticator2'),
            'Authenticator3'   => empty($request->input('authenticator3')) ? 'NA' : $request->input('authenticator3'),
            'Filler1'          => config('vwallet.billdeskFiller1'),
            //'Filler2'          => '192.168.100.83!b126fa8bb6adec5b!IOS!VOL^VIOLAWALLET!9967471234',
            'Filler2'          => config('vwallet.billdeskFiller2'),
            'Filler3'          => config('vwallet.billdeskFiller3'),
        );

        return $arrayparams;
    }

    /**
     * 
     * @param type $request
     */
    public static function responseKeysU07009()
    {
        $responseKeys = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'RechargeBillerId',
            'Authenticator1',
            'Authenticator2',
            'Authenticator3',
            'BillNumber',
            'BillDate',
            'BillDueDate',
            'BillAmount',
            'PayWithOutBill',
            'PartialPayment',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );
        return $responseKeys;
    }

    /**
     * 
     * @param type $request
     */
    public static function responseCombineU07009($responseKeys, $responseArray)
    {
        $combine = array_combine($responseKeys, $responseArray);

        $combine['BillDateFormat']    = rtrim(chunk_split(chunk_split($combine['BillDate'], '4', '-'), '7', '-'), '-');
        $combine['BillDueDateFormat'] = rtrim(chunk_split(chunk_split($combine['BillDueDate'], '4', '-'), '7', '-'), '-');

        return $combine;
    }

    public static function arrangeResponseDataInorder($billerIdMain,$outputArray)
    {
        $getBillResponse = \App\Models\Billdeskbiller::select('ResponseAuthenticators')
                        ->where('BillerID', $billerIdMain)->first();

        $billerDetails    = array();
        $tempArray        = array();
        $tempFiller3Array = array();

        // checking if bill response have data or not
        if ( $getBillResponse )
        {
            $billResponseParams    = $getBillResponse->ResponseAuthenticators;
            $explodeResponseParams = explode('#', $billResponseParams);

            // looping for first 3 values
            for ( $i = 1; $i <= count($explodeResponseParams); $i ++ )
            {

                if ( $i <= 3 )
                {
                    $billerDetails[] = array(
                        $explodeResponseParams[$i - 1] => $outputArray['Authenticator' . $i],
                    );
                }
                else
                {

                    $tempArray[] = $explodeResponseParams[$i - 1];
                }
            }

            // checking data exists or not
            $filler3        = str_replace('!NA', '', $outputArray['Filler3']);
            $filler3Replace = str_replace('$', '!', $filler3);
            $explodeFiller3 = explode('!', $filler3Replace);

            if ( $tempArray )
            {
                $arrayCombine = array_combine($tempArray, $explodeFiller3);

                foreach ( $arrayCombine as $key => $value )
                {
                    if ( str_contains($value, '@') )
                    {
                        $tempFiller3Array[] = array(
                            $key => $value
                        );
                    }
                    else if ( str_contains($value, '$') )
                    {
                        $tempFiller3Array[] = array(
                            $key => $value
                        );
                    }
                    else
                    {
                        $billerDetails[] = array(
                            $key => $value,
                        );
                    }
                }
            }

            // looping if filler 3 has data to loop
            if ( $tempFiller3Array )
            {

                foreach ( $tempFiller3Array[0] as $key3 => $valueFiller3 )
                {

                    $explodeAttheRate = explode('@', $valueFiller3);
                    $explodeName      = explode('@', $key3);
                    foreach ( $explodeAttheRate as $keyAttherate => $valueatherate )
                    {
                        $billerDetails[] = array(
                            $explodeName[$keyAttherate] => $valueatherate,
                        );
                    }
                }
            }
        }

        return $billerDetails;
    }

}
