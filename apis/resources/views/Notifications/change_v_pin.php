<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Change V-Pin template
 * 
 *  param $fullName varchar 
 *  param $otpCode varchar 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullName) ? $salutation.' '.$fullName : $salutation;
$array = [
    'email' => [
        'subject' => 'Viola PIN Changed',
        'text' => $salutationName . ',<br/><br/>
Viola PIN for your ViolaWallet account has been updated as per your request. <br/> 
If you didn`t make this request, your account might be compromised. To get back into your account, you`ll need to reset your credentials.'],
    'sms' => [
        'text' => 'Your Viola PIN has been changed. Please do not share your PIN/Password/CVV with anyone',
    ],
    'push' => [
        'title' => 'ViolaWallet',
        'body' => 'Your Viola PIN is changed',
        'summaryText' => '',
        'notificationType' => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web' => [
        'text' => 'Your Viola PIN has been changed',
    ],
];

echo json_encode($array);

/* End of file change_v_pin.php */
