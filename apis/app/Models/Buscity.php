<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Buscity
 * 
 * @property int $Id
 * @property int $RedBusId
 * @property string $Name
 * @property string $State
 * @property int $RedBusStateId
 * @property string $BusCitiescol
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 *
 * @package App\Models
 */
class Buscity extends Eloquent
{
        protected $table = 'buscities';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'RedBusId' => 'int',
		'RedBusStateId' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'RedBusId',
		'Name',
		'State',
		'RedBusStateId',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];
}
