<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 14:19:00 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Banner
 * 
 * @property int $BannerId
 * @property string $Title
 * @property string $Category
 * @property string $Content
 * @property string $Active
 * @property string $Link
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 *
 * @package App\Models
 */
class Banner extends Eloquent
{
	protected $primaryKey = 'BannerId';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'Title',
		'Category',
		'BannerPath',
		'Content',
		'Active',
		'Link',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedBy',
		'UpdatedDateTime'
	];
}
