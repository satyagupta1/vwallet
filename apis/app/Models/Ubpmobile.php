<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 09:22:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Ubpmobile
 * 
 * @property int $MobileBin
 * @property string $BillerId
 * @property int $Circle
 * @property string $Status
 *
 * @package App\Models
 */
class Ubpmobile extends Eloquent
{
	protected $table = 'ubpmobile';
	protected $primaryKey = 'MobileBin';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MobileBin' => 'int',
		'Circle' => 'int'
	];

	protected $fillable = [
		'BillerId',
		'Circle',
		'Status'
	];
}
