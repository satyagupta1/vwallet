<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partypolicyterm
 * 
 * @property int $PartyPolicyTermID
 * @property int $PartyID
 * @property int $PolicyTermID
 * @property \Carbon\Carbon $TermsAcceptedDate
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Policyterm $policyterm
 *
 * @package App\Models
 */
class Partypolicyterm extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyPolicyTermID' => 'int',
		'PartyID' => 'int',
		'PolicyTermID' => 'int'
	];

	protected $dates = [
		'TermsAcceptedDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TermsAcceptedDate',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function policyterm()
	{
		return $this->belongsTo(\App\Models\Policyterm::class, 'PolicyTermID');
	}
}
