<?php

namespace App\Http\Controllers\Reminder;

class BillController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->createdBy = config('vwallet.adminTypeKey');
    }

    /**
     * 
     * @param type $request
     */
    public function getBillPayment(\Illuminate\Http\Request $request)
    {
        \Log::debug('Remind Fetch Bills Cron Started!');
        $requestInputData = array();
        $params['title']       = 'Remind Fetch Bills (findBill)';
        $output = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );

        $trasactions        = \App\Models\Remindtransaction::select('RemindTransactionId', 'PartyId', 'BillerId', 'BillerCategory', 'SubscriberNumber')->where(array( 'Status' => 'Y', 'ActionType' => 'Reminder', 'Category' => 'Postpaid' ))->where('SubscriberNumber', '!=', '')->get();
        $output['is_error'] = FALSE;
        if ( count($trasactions) > 0 )
        {

            foreach ( $trasactions as $transaction )
            {
                 
                //Verify bill generated in last 20days.
                $userRemin = \App\Models\Userreminder::select('PaymentDueDate', 'UserReminderId')->where(array( 'BillerID' => $transaction->SubscriberNumber, 'UserWalletID' => $transaction->PartyId ))->orderby('UserReminderId', 'DESC')->first();
                if ( count($userRemin) > 0 )
                {
                    $daysback   = date("Y-m-d H:i:s", strtotime('-20 days'));
                    $strdayBack = strtotime($daysback);
                    $dueDate    = strtotime($userRemin->PaymentDueDate);
                    if ( $strdayBack < $dueDate )
                    {
                        continue;
                    }
                }
                //end bill check
                
                $requestInput = array( 'partyId' => $transaction->PartyId, 'billerId' => $transaction->BillerId, 'authenticator1' => $transaction->SubscriberNumber );
                $requestInputData[] = $requestInput;
                $request->request->add($requestInput);
                $response = \App\Libraries\BilldeskHelper::getBillPayment($request);
                // check reponse if server is sending html reponse that their sever is down
                if ( $response != strip_tags($response) )
                {
                    $output['is_error'] = TRUE;
                    $output['display_msg']['info'] = trans('messages.unableToProcess');
                    $params['title']       = 'Faild - Remind Fetch Bills (findBill)';
                    $params['description'] = $output['display_msg']['info'] . ' <br/> Faild Remindtransactions Request Input :<br/> ' . print_r($requestInput, TRUE);
                    \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
                    //return response()->json($output);
                    continue; //continue for next record
                }
                $responseArray = explode('~', $response);
                $responseKeys  = \App\Libraries\BilldeskHelper::responseKeysU07009($request);
                $outputArray   = \App\Libraries\BilldeskHelper::responseCombineU07009($responseKeys, $responseArray);

                if ( $outputArray['is_error'] === FALSE && $outputArray['res_data']['Valid'] === 'Y' )
                {

                    //inserting record in to remainder table
                    $this->_insertFetchBill($outputArray['res_data'], $transaction);
                }
            }
            \Log::debug('Remind Fetch Bills Cron Success!');
            $output['is_error']            = FALSE;
            $output['display_msg']['info'] = trans('messages.requestSuccessfull');
        }        
        $params['description'] = $output['display_msg']['info'] . ' <br/> Remindtransactions request input data : <br/>' . print_r($requestInputData, TRUE);
        \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
        return response()->json($output);
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validategetBilldetails($request)
    {

        $validate['billerId']       = 'required';
        $validate['authenticator1'] = 'required';
        $validate_response          = \Validator::make($request->all(), $validate, array());
        $vali_response              = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * 
     * @param type $request
     */
    private function _insertFetchBill($billData, $transaction)
    {

       $partyId     = $transaction->PartyId;
       $dateTime    = date('Y-m-d H:i:s');
       $billDueDate = $billData['BillDueDateFormat'];
       $billDueDateDisplay = date('d-m-y', strtotime($billDueDate));
        // If bill due date is less than current date then no need of reminder.
        if ( strtotime($dateTime) > strtotime($billDueDate) )
        {
            return TRUE;
        }

        $todayDateObj   = new \DateTime();
        $billDueDateObj = new \DateTime($billDueDate);

        $diff = $billDueDateObj->diff($todayDateObj)->format("%a");

        \DB::beginTransaction();
        try
        {
                          
            $data             = array(
                'PartyId'                 => $partyId,
                'RemindTransactionId'           => $transaction->RemindTransactionId,               
                'NotificationCycleType'   => 'N',
                'NotificationCyclePeriod' => 0,
                'NextExectionDateTime'    => $dateTime,
                'Status'                  => 'Y',
                'CreatedDateTime'         => $dateTime,
                'UpdatedDateTime'         => $dateTime,
                'CreatedBy'               => $this->createdBy,
                'UpdatedBy'               => $this->createdBy
            );
            $remindId         = \App\Models\Reminders::insertGetId($data);
            $notifyInsertData = [];
            $notifyArray      = array(
                'RemiderId'       => $remindId,
                'Status'          => 'Y',
                'DateTime'        => $billDueDate,
                'Title'                   => 'ViolaWallet Reminder',                
                'CreatedDateTime' => $dateTime,
                'UpdatedDateTime' => $dateTime,
                'CreatedBy'       => $this->createdBy,
                'UpdatedBy'       => $this->createdBy
            );

            if ( $transaction->ActionType == 'AutoPay' && $transaction->DueTimeLine > 0 )
            {
                $dueTimeNum              = $transaction->DueTimeLine;
                $onDueTime               = date('Y-m-d', strtotime('-' . $dueTimeNum . ' days', strtotime($billDueDate)));
                $notifyArray['DateTime'] = $onDueTime;
                $notifyInsertData[]      = $notifyArray;
            }
            else
            {
                if ( $diff > 1 )
                {                    
                    $description = 'Alert! This is your last day to pay your '.$transaction->BillerCategory.' - '.$billData['Authenticator1'].' via online. Proceed to Pay '.$billData['BillAmount'].'';
                    $oneDayAgo               = date('Y-m-d', strtotime('-1 days', strtotime($billDueDate)));
                    $notifyArray['DateTime'] = $oneDayAgo;
                    $notifyArray['Description'] = $description;                    
                    $notifyInsertData[]      = $notifyArray;
                }
                if ( $diff > 3 )
                {
                    //$description =  'Your payment for '.$billData['RechargeBillerId'].' - '.$billData['Authenticator1'].' is due in '.$diff.' days. Please pay now to avoid late fees! ';
                    $description =  'Your payment for '.$transaction->BillerCategory.' - '.$billData['Authenticator1'].' is due on '.$billDueDateDisplay.'. Proceed to Pay!';
                    $threeDaysAgo            = date('Y-m-d', strtotime('-3 days', strtotime($billDueDate)));
                    $notifyArray['DateTime'] = $threeDaysAgo;
                    $notifyArray['Description'] = $description;
                    $notifyInsertData[]      = $notifyArray;
                }
                if ( $diff > 7 )
                {
                    $description =  'Your payment for '.$transaction->BillerCategory.' - '.$billData['Authenticator1'].' is due on '.$billDueDateDisplay.'. Proceed to Pay!';
                    $sevenDaysAgo            = date('Y-m-d', strtotime('-7 days', strtotime($billDueDate)));
                    $notifyArray['DateTime'] = $sevenDaysAgo;
                    $notifyArray['Description'] = $description;
                    $notifyInsertData[]      = $notifyArray;
                }
            }

            \App\Models\Remindernotification::insert($notifyInsertData);

            $insArray                     = array(
                'ReminderId'              => $remindId,
                'UserWalletID'            => $partyId,
                'Status'                  => 'Active',
                'BillAmount'              => $billData['BillAmount'],
                'BillerID'                => $billData['Authenticator1'],
                'BillerName'              => $billData['BillNumber'],
                'BillerCategory'          => $billData['RechargeBillerId'],
                'ResponseAuthenticator_1' => $billData['Authenticator1'],
                'ResponseAuthenticator_2' => $billData['Authenticator2'],
                'ResponseAuthenticator_3' => $billData['Authenticator3'],
                'PaymentDueDate'          => $billDueDate,
                'CreatedBy'               => 'Admin',
            );
            \App\Models\Userreminder::insertGetId($insArray);
            
            \DB::commit();
            // all good 
        }
        catch ( \Exception $e )
        {
           \DB::rollback();
            //print_r($e); die;
            return FALSE;
        }
        return TRUE;
    }

}
