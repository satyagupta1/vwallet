<?php

namespace App\Http\Controllers\Communication;

use App\Http\Controllers\Controller;

/**
 * PartySecurityController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class FrontController extends SendMessageController {

   /** This function can convert array in to request input 
     * And it calls SendMessage Controller sendMsg Function.
     * Send now is the main function which is used to send notifications to the users
     * on this function we will get the following parameters in post format and according
     * to the data we will send the messages.
     *
     * @param
     * application_name = The name of the application through which we are sending request.
     *  - V-Wallet
     *  - V-Business
     * notification_for = This will the key for the notification template. With the help of this filed
     *                      We will fetch the data from the language and template file.
     *                      This may has the section or page name.
     *  - nreg = Normal Registration Section.
     *  - qreg = quick Registration Section.
     *  - breg = Business Registration Section.
     *  - aaact = Activate email Account.
     *  - forpas = forgot password.
     *  - etc......
     * notification_type = It may be one of the following. If need to send multiple messages
     *                      Please send multiple things in comma separation format. The value
     *                      may be one of the following data. (all small case)
     *  - email = Email ID
     *  - sms = SMS
     *  - push = Push Notification
     *  - web = Web Notification
     *
     * In case if we need to send email and sms both we can send notification_type = email,sms
     * In the same way if we need to send email, sms and push notification then email,sms,push
     *
     * email_id = if we have email in notification_type then need to send email_id otherwise it should be blank.
     * email_data = if we have email in notification_type then need to send email_data, we will replace this data
     *              in the template otherwise it should be blank.
     * mobile_no = Mobile number if we want to send sms.
     * mobile_data = The data for the sms which need to send in SMS template will be pick from language.
     * device_type = The device type may be windows, android, iphone
     *  - android
     *  - iphone
     * device_token = The token of the device to send the puch notification.
     * device_data = The data which we need to send via push notification, the template will be pick from language file.
     * web_data = the web data which we need to send as web notification, the template will be pick from language file.
     *
    */

    public function callCommunication(\Illuminate\Http\Request $request, $paramInputs)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        return response()->json($output_arr);
    }
    //Testing communication services
    public function test(\Illuminate\Http\Request $request) {
        /*
       //sms after registration
        $communicationParams = array(
            'application_name' => 'V-Wallet',
            'party_id' => 1,
            'linked_id' => 1,
            'notification_for' => 'after_nreg',
            'mobile_no' => '9949207896', 
            'mobile_data' => json_encode(array('site_name' => 'V-Wallet')),
            );
        return $sms_response = \App\Libraries\Communication\Sendsms_lib::sendNow($communicationParams);exit;
          
        
       //emails
        $communicationParams = array(
            'application_name' => 'V-Wallet',
            'party_id' => 1,
            'linked_id' => 1,
            'notification_for' => 'after_nreg',
            'email_id' => 'mahesh.bairi@violamoney.com', 
            'email_data' => json_encode(array('site_name' => 'V-Wallet', 'fullname' => 'Mahesh', 'mobile' => '9949207896', 'viola_id' => '121212', 'img_url' => 'violamoney.com', 'front_url' => 'violamoney.com')),
            );
        return $sms_response = \App\Libraries\Communication\Sendemail_lib::sendNow($communicationParams);exit;
         */
        //OTP
        $params    = array(
                    'UserWalletID' => '1',                    
                    'mobileNumber' => '9949207896',
                    'typeCode'     => 'REG',                   
                );
        
        $communicationParams    = array(
                    'partyid' => '1',
                    'linkedid' => '1',
                    'fullName' => 'Mahesh',
                    'mobileNumber' => '9949207896',                    
                    'notificationFor' => 'nreg',
                    'notificationType' => 'sms'
                );
         
               return $data_json = \App\Libraries\Helpers::OtpSend($params, $communicationParams);
        $request->request->add(['notification_for' => 'nreg', 'notification_type' => 'email']);
        
        return $violaIdInfo = app('App\Http\Controllers\Communication\SendMessageController')->sendMsg($request);
//return $violaIdInfo = $this->sendMsg($request);
         //$output_arr['display_msg'] = $violaIdInfo;
        //return response()->json($output_arr);
    }
}
