<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Bus Ticket Cashback Received
 * 
 *  param $cashbackPercentage varchar
 *  param $amount decimal
 *  param $time time
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaWallet Enjoy your cashback!',
        'text'    => $salutationName . ',<br/><br/>
Great news- you received cashback of Rs '.$amount.' on your Bus Booking.<br/> 
Please refer your friends and help them receive cashbacks. You also earn more cashbacks on each referral.<br/>
Invite Friends Now.'],
    'sms'   => [
        'text' => $cashbackPercentage.'% cashback received on Bus Booking! Apply Promo Code RCFC'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => $cashbackPercentage.'% Cashback on Bus Booking received',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'offers'
    ],
    'web'   => [
        'text' => 'Cashback on Bus Booking  being processed.',
    ],
];

echo json_encode($array);

/* End of file bus_ticket_cashback_received */
