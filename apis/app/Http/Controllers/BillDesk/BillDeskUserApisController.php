<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\BillDesk;

class BillDeskUserApisController extends \App\Http\Controllers\Controller {
    /*
     *  Operators List - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function operatorsList(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $chk_valid  = $this->_ValidationOperators($request, $type       = 'operatorsList');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        $dbResultsPrepaid         = \App\Models\Billerlist::select('billerlist.BillerName', 'billerlist.BBPSBiller', 'billerlist.BillerId', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory')
                ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billerlist.BillerId')
                ->where('billerlist.SubCategory', 'PREPAID MOBILE')
                ->where('billerlist.BillerStatus', 'Y')
                ->get();       
        $dbResultsDth             = \App\Models\Billerlist::select('billerlist.BillerName', 'billerlist.BBPSBiller', 'billerlist.BillerId', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory')
                ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billerlist.BillerId')
                ->where('billerlist.SubCategory', 'PREPAID DTH')
                ->where('billerlist.BillerStatus', 'Y')
                ->get();
        $frequentlyUserdOperators = array();
        if ($request->partyId > 0)
        {           
        /*   Frequent used operators 
        $frequentlyUserdOperators = \App\Models\Userrechargedetail::select('billerlist.BillerId', 'billerlist.BillerName', 'billerlist.BBPSBiller', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory', \DB::raw("COUNT(userrechargedetails.SubScriberName) AS SubScriberCount"))
        ->join('billerlist', 'billerlist.BillerName', '=', 'userrechargedetails.SubScriberName')
        ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billerlist.BillerId')
        ->where('userrechargedetails.UserWalletID', $request->partyId)
        ->orderBy('SubScriberCount', 'desc')
        ->groupBy('billerlist.BillerId')
        ->take(10)
        ->get();*/
        //Most recent used operator    
        $recentlyUserdOperators = \App\Models\Userrechargedetail::select('userrechargedetails.RechargeDetailID','billerlist.BillerId', 'billerlist.BillerName', 'billerlist.BBPSBiller', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory')
        ->join('billerlist', 'billerlist.BillerName', '=', 'userrechargedetails.SubScriberName')
        ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billerlist.BillerId')
        ->where('userrechargedetails.UserWalletID', $request->partyId)        
        ->groupBy('userrechargedetails.SubScriberName')
        ->orderBy(\DB::raw("MAX(RechargeDetailID)"), 'DESC')
        ->take(10)->get()->toArray();
        }

        if ((!empty($dbResultsPrepaid)) && (!empty($dbResultsDth)))
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = array('PrepaidMobile' => $dbResultsPrepaid, 'DTH' => $dbResultsDth, 'frequentlyUsed' => $frequentlyUserdOperators, 'RecentUsed' => array());
            
            //Sorting as for user recently used operators 
            if ( ! empty($recentlyUserdOperators) )
            {
                $prepaidMobile = array();
                foreach ( $dbResultsPrepaid->toArray() as $prepaid )
                {
                    $prepaidMobile[$prepaid['PaymentsProductID']] = $prepaid;
                }
                $prepaidDth = array();
                foreach ( $dbResultsDth->toArray() as $dthTv )
                {
                    $prepaidDth[$dthTv['PaymentsProductID']] = $dthTv;
                }
                
                //reverce order get from last to first
                $userProductsOrder = array_reverse($recentlyUserdOperators, TRUE);
                foreach ( $userProductsOrder as $eachOperatorOrder )
                {
                    $usedProductId = $eachOperatorOrder['PaymentsProductID'];
                    if ( array_key_exists($usedProductId, $prepaidMobile) )
                    {
                        $moveMobileOperator       = $prepaidMobile[$usedProductId];
                        unset($prepaidMobile[$usedProductId]);
                        $prepaidMobile = array( $usedProductId => $moveMobileOperator ) + $prepaidMobile;
                    }
                    if ( array_key_exists($usedProductId, $prepaidDth) )
                    {
                        $moveDthOperator       = $prepaidDth[$usedProductId];
                        unset($prepaidDth[$usedProductId]);
                        $prepaidDth = array( $usedProductId => $moveDthOperator ) + $prepaidDth;
                    }
                }
                $output_arr['res_data']['PrepaidMobile']  = array_values($prepaidMobile);
                $output_arr['res_data']['DTH']  = array_values($prepaidDth);
                $output_arr['res_data']['RecentUsed']  = array_values($recentlyUserdOperators);                
            }
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /*
     *  circleList - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function circlesList(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $chk_valid = $this->_ValidationOperators($request, $type      = 'circlelist');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        $circlesArray = array();
        $circles      = \App\Models\Operatorfeeplan::
                join('circlemaster', 'circlemaster.Circle_Id', '=', 'operatorfeeplan.Circle')
                ->select('circlemaster.Circle_Id', 'circlemaster.Master')
                ->where('operatorfeeplan.Operator', $request->input('operatorId'))
                ->groupby('operatorfeeplan.Circle')
                ->orderby('operatorfeeplan.Circle', 'ASC')
                ->get();

        foreach ($circles as $mainCircles)
        {
            $circlesArray[] = array('CircleName' => $mainCircles->Master, 'CircleId' => $mainCircles->Circle_Id);
        }

        if (!empty($circles))
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = array('Operator' => $request->input('operatorId'), 'circles' => $circlesArray);
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /*
     *  Plan Categories List - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function planCategoriesList(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $chk_valid = $this->_ValidationOperators($request, $type      = 'plansCategorylist');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }

        $planCateg = \App\Models\Operatorfeeplan::select('CategoryName')
                ->where('Operator', $request->input('operatorId'))
                ->where('Circle', $request->input('circleId'))
                ->groupby('CategoryName')
                ->get();

        foreach ($planCateg as $mainPlanCateg)
        {
            $plansCategory[] = $mainPlanCateg->CategoryName;
        }

        if (!empty($planCateg))
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = array('Operator' => $request->input('operatorId'), 'CircleId' => $request->input('circleId'), 'PlansCategory' => $plansCategory);
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /*
     *  GetPlanList User Data - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function getPlanListUserData(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $chk_valid = $this->_ValidationOperators($request, $type      = 'planslist');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }

        $palanCategory   = $request->input('planCategory');
        $getDetailsPlans = \App\Models\Operatorfeeplan::select('Mrp', 'TalkTime', 'Validity', 'Description', 'FeePlanID')
                ->where('Operator', $request->input('operatorId'))
                ->where('Circle', $request->input('circleId'))
                ->where('CategoryName', 'LIKE', '%' . $palanCategory . '%')
                ->orderby('Mrp', 'ASC')
                ->get();

        $categoryNameData                = array();
        $categoryNameData['planName']    = $palanCategory;
        $categoryNameData['Circle']      = $request->input('circleId');
        $categoryNameData['Operator']    = $request->input('operatorId');
        $categoryNameData['planDetails'] = $getDetailsPlans;

        if ((count($getDetailsPlans) > 0))
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = $categoryNameData;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /*
     *  GetPlanList User Data - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function getoperatorByMobilenumber(\Illuminate\Http\Request $request)
    {

        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $chk_valid = $this->_ValidationOperators($request, $type      = 'operatorBymobile');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }

        $resultNumber = mb_substr($request->input('mobileNumber'), 0, 5);


//        $checkNumber = \App\Models\Ubpmobile::select('BillerId', 'Circle')
//                        ->where('MobileBin', $resultNumber)
//                        ->where('Status', 'ACTIVE')->first();

        $checkNumber = \App\Models\Ubpmobile::select('ubpmobile.BillerId', 'ubpmobile.Circle', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory')
                        ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'ubpmobile.BillerId')
                        ->where('ubpmobile.MobileBin', $resultNumber)
                        ->where('ubpmobile.Status', 'ACTIVE')->first();

        if (!empty($checkNumber))
        {
            $resData                           = array(
                'CircleName'        => \App\Libraries\BilldeskHelper::getCircleName($checkNumber->Circle),
                'CircleId'          => $checkNumber->Circle,
                'BillerId'          => $checkNumber->BillerId,
                'BillerName'        => $this->getBillersName($checkNumber->BillerId),
                'PaymentsProductID' => $checkNumber->PaymentsProductID,
                'ProductCategory'   => $checkNumber->ProductCategory,
            );
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = $resData;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    public function getBillersName($billerId)
    {

        $selBillers = \App\Models\Billerlist::select('BillerName')->where('BillerId', $billerId)->first();
        return (!empty($selBillers)) ? $selBillers->BillerName : FALSE;
    }

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param PartyId integer
     * @param WalletAcntId integer
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    public function billDeskRecharge(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        // check validation
        $chk_valid  = $this->_validationCheck($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }


        $partyId  = $request->input('partyId');
        $amount   = $request->input('amount');
        $entityId = $request->input('businessModelid');
        $cardId   = $request->input('savedcardId');

        $transStaus        = config('vwallet.transactionStatus.pending');
        $transBool         = TRUE;
        $paymentsProductID = 6;

        // user exsistens test
        $partyInfo = \App\Libraries\TransctionHelper::_userValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $sesionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sesionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return $output_arr;
        }

        \DB::enableQueryLog();
        $productParams = \App\Libraries\TransctionHelper::paymentEntity($entityId, $paymentsProductID, $partyInfo->WalletPlanTypeId);
        if (!$productParams)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $entityPrams = \App\Libraries\TransctionHelper::productPlanParams($productParams->ConfigParamNum);

        $feeInfo = array();
        foreach ($entityPrams as $key => $value)
        {
            $feeInfo[$value->ParameterTagName] = $value->ParameterTagValue;
        }

        // code to check min amount for transaction
        if ($request->input('amount') < $feeInfo['MinTransAmt'])
        {
            $cw_min                    = config('vwallet.C2W_MIN_AMOUNT');
            $output_arr['display_msg'] = array('info' => trans('messages.minAmounttoTransfer', ['name' => $feeInfo['MinTransAmt']]));
            return json_encode($output_arr);
        }

        // code to per limit amount per transaction
        if ($request->input('amount') > $feeInfo['MaxTransAmt'])
        {
            $output_arr['display_msg'] = array('info' => trans('messages.maxTransferPerTransaction', ['name' => $feeInfo['MaxTransAmt']]));
            return json_encode($output_arr);
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);

        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductID);

        // fee calculation 
        $fee = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $entityId, $partyId);

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, TRUE, $transBool, $sesionId);
        if ($transRes['error'])
        {
            $output_arr['display_msg'] = array('info' => $transRes['info']);
            return $output_arr;
        }

        $request->request->add(['transactionId' => $transRes['response']['TransactionDetails']['TransactionOrderID']]);
        $request->request->add(['transactionOrderId' => $transRes['response']['TransactionDetails']['TransactionDetailID']]);
        $returnData = \App\Libraries\Payments\Ccavenue::kotakApiRequest($request);

        $resArray = array(
            'pageStatus'         => 'openBrowser',
            'transactionOrderId' => $transRes['response']['TransactionDetails']['TransactionOrderID'],
            'result'             => $returnData
        );

        $output_arr['display_msg'] = array('info' => $transRes['info']);
        $output_arr['is_error']    = FALSE;
        $output_arr['res_data']    = $resArray;
        return $output_arr;
    }

    /*
     *  GetPlanList User Data - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function getBillersPostBillpay(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

//        $chk_valid = $this->_ValidationOperators($request, $type      = 'getBillersPostBill');
//        if ( $chk_valid !== TRUE )
//        {
//            $output_arr['display_msg'] = $chk_valid;
//            return json_encode($output_arr);
//        }
      
        $partyId  = $request->input('partyId'); 
        $userTransactions = \App\Models\Userbillertransaction::select('UserBillerTransid', 'PaymentProductID','BillerCategory')->where('UserWalletID', $partyId)->where('RechargeBillerId', '!=', '')->orderBy('UserBillerTransid', 'DESC')->limit(10)->get()->unique('PaymentProductID')->toArray();
        $categoryList = \App\Models\Billdeskbiller::select('BillerCategory')->where('BillerStatus', 'Y')->groupBy('BillerCategory')->get()->toArray();

        $categoryArray = array();
        foreach($categoryList as $categoryItem) {
          $categoryArray[] = $categoryItem['BillerCategory'];
        }        
        $userProducts = array();
        foreach($userTransactions as $userTrans) {
          $userProducts[$userTrans['PaymentProductID']] =  $userTrans['PaymentProductID']; 
        }
        $userProductsOrder = array_reverse($userProducts, TRUE);
       
        foreach ($categoryArray as $listCateg)
        {            
            $getDetailsPlans  = \App\Models\Billdeskbiller::select('BillerID', 'BillerName', 'BBPSBiller', 'Ref1Label', 'Ref1RegEx', 'Ref1Message', 'Ref2Label','Ref1Hint','Ref1IsDropDown', 
                    'Ref2RegEx', 'Ref2Message','Ref2Hint','Ref2IsDropDown', 'Ref3Label', 'Ref3RegEx', 'Ref3Message','Ref3Hint','Ref3IsDropDown', 'PostDuePayFlag', 'AmtUICaptureFlag', 'OnlineBillFetchFlag', 'PartPayFlag', 'paymentsproduct.PaymentsProductID', 'paymentsproduct.ProductCategory', 'BillerPreference')
                            ->join('paymentsproduct', 'paymentsproduct.ProductName', '=', 'billdeskbillers.BillerID')
                            ->where('BillerCategory', 'LIKE', '%' . $listCateg . '%')
                            ->orderby('BillerID', 'ASC')
                            ->where('BillerStatus', 'Y')
                            ->get()->toArray();
            $categoryNameData = array();

            foreach ($getDetailsPlans as $mainPlandetails)
            {
                $statesNames = array();

                if ($mainPlandetails['BillerID'] == 'CONBBOB' || $mainPlandetails['BillerID'] == 'MSEBOB' || $mainPlandetails['BillerID'] == 'RVVNLOB' || $mainPlandetails['BillerID'] == 'TORRENTOB' || $mainPlandetails['BillerID'] == 'WBSEDCLW' || $mainPlandetails['BillerID'] == 'WBSEDCLQOB')
                {

                    $statesNames = explode('/', $mainPlandetails['BillerPreference']);
                }
                $mainProductId = $mainPlandetails['PaymentsProductID'];
                $categoryNameData[$mainProductId] = [
                    'PaymentsProductID' => $mainProductId,
                    'ProductCategory'   => $mainPlandetails['ProductCategory'],
                    'billerCategory'    => $listCateg,
                    'billerId'          => $mainPlandetails['BillerID'],
                    'billerName'        => $mainPlandetails['BillerName'],
                    'bbpsBiller'        => $mainPlandetails['BBPSBiller'],
                    'authenticators'    => array(
                        'auth1Label'      => $mainPlandetails['Ref1Label'],
                        'auth1RegEx'      => $mainPlandetails['Ref1RegEx'],
                        'auth1RefMessage' => $mainPlandetails['Ref1Message'],
                        'auth1Hint' => ($mainPlandetails['Ref1Hint']==NULL)?'':$mainPlandetails['Ref1Hint'],
                        'auth1Dropdown' => $mainPlandetails['Ref1IsDropDown'],
                        'auth2Label'      => $mainPlandetails['Ref2Label'],
                        'auth2RegEx'      => $mainPlandetails['Ref2RegEx'],
                        'auth2RefMessage' => $mainPlandetails['Ref2Message'],
                        'auth2Hint' => ($mainPlandetails['Ref2Hint']==NULL)?'':$mainPlandetails['Ref2Hint'],
                        'auth2Dropdown' => $mainPlandetails['Ref2IsDropDown'],
                        'auth3Label'      => $mainPlandetails['Ref3Label'],
                        'auth3RegEx'      => $mainPlandetails['Ref3RegEx'],
                        'auth3RefMessage' => $mainPlandetails['Ref3Message'],
                        'auth3Hint' => ($mainPlandetails['Ref3Hint']==NULL)?'':$mainPlandetails['Ref3Hint'],
                        'auth3Dropdown' => $mainPlandetails['Ref3IsDropDown'],
                    ),
                    'conditions'        => array(
                        'PostDuePayFlag'   => $mainPlandetails['PostDuePayFlag'],
                        'AmtUICaptureFlag' => $mainPlandetails['AmtUICaptureFlag'],
                        'PartialPayFlag'   => $mainPlandetails['PartPayFlag'],
                    ),
                    'BillerPreference'  => $statesNames,
                ];               
            }
            if (($request->header('device-type') == 'W') && ($listCateg == 'Landline/Broadband'))
            {
                $listCateg = 'Landline_Broadband';
            }
            //update biller order as for user 
            foreach($userProductsOrder as $productOrder) {                
                if(array_key_exists($productOrder, $categoryNameData)) {
                    $moveBiller = $categoryNameData[$productOrder];
                    unset($categoryNameData[$productOrder]);
                    $categoryNameData = array($productOrder => $moveBiller) + $categoryNameData;
                }
            }            
            $mainData[$listCateg] = array_values($categoryNameData);             
        }

        if ((count($getDetailsPlans) > 0))
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = $mainData;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

     // get static master data of msedcl biller it is in electricty in billdesk
    public function getBillersDropdownData(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

//        $chk_valid = $this->_ValidationOperators($request, 'billdesklookup');
//        if ( $chk_valid !== TRUE )
//        {
//            $output_arr['display_msg'] = $chk_valid;
//            return json_encode($output_arr);
//        }
        $billerId = $request->input('billerId');
        $authType = $request->input('authType');
        if($billerId !='' && $authType!='')
        {
            $getData  = \App\Models\Billdesklookup::select('BillerID','TorrentId', 'BillingNo','AuthTypes')->where('BillerID', $billerId)
                ->where('AuthTypes', $authType)
                ->get();
        }
        else{
           $getData  = \App\Models\Billdesklookup::select('BillerID','TorrentId', 'BillingNo','AuthTypes')
                ->get(); 
        }
        
        if ( (count($getData) > 0 ) )
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = '';
            $output_arr['res_data']            = $getData;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param PartyId integer
     * @param WalletAcntId integer
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _ValidationOperators($request, $type)
    {
        $messages = array();
        // validations
        if ($type == 'circlelist')
        {
            $validate = array(
                'operatorId' => 'required',
            );
        }

        if ($type == 'plansCategorylist')
        {
            $validate = array(
                'operatorId' => 'required',
                'circleId'   => 'required',
            );
        }

        if ($type == 'planslist')
        {
            $validate = array(
                'operatorId'   => 'required',
                'circleId'     => 'required',
                'planCategory' => 'required',
            );
        }

        // code for operator by mobile number
        if ($type == 'operatorBymobile')
        {
            $validate = array(
                'mobileNumber' => 'required|numeric|regex:^[0-9]{10,12}$^',
            );
        }


        if ($type == 'getBillersPostBill')
        {
            $validate = array(
                'category' => 'required|in:Electricity,Landline/Broadband,Gas',
            );
        }

        if ($type == 'operatorsList')
        {
            $validate = array(
                'partyId' => 'required|numeric',
            );
        }
        if ( $type == 'billdesklookup' )
        {
            $validate = array(
                'billerId' => 'required',
                'authType' => 'required:in:auth1,auth2,auth3',
            );
        }


        // validation for mobile device


        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
