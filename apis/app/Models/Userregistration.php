<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userregistration
 * 
 * @property int $UserWalletID
 * @property string $RegisteredReferralCode
 * @property string $MobileVerified
 * @property string $EmailVerified
 * @property string $RegistrationCountryCode
 * @property string $WalletCountryCode
 * @property string $WalletTimeZone
 * @property string $TermsAccepted
 * @property \Carbon\Carbon $TermsAcceptedDateTime
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $CommunicationPreference
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userregistration extends Eloquent
{
	protected $table = 'userregistration';
	protected $primaryKey = 'UserWalletID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int'
	];

	protected $dates = [
		'TermsAcceptedDateTime',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
                'UserWalletID',
		'RegisteredReferralCode',
		'MobileVerified',
		'EmailVerified',
		'RegistrationCountryCode',
		'WalletCountryCode',
		'WalletTimeZone',
		'TermsAccepted',
		'TermsAcceptedDateTime',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'CommunicationPreference'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
