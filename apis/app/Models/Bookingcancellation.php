<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bookingcancellation
 * 
 * @property int $CancellationId
 * @property int $BookingMasterId
 * @property int $PassengerId
 * @property \Carbon\Carbon $CancellationDate
 * @property float $CancellationCharges
 * @property float $RefundAmt
 * @property float $CancellationFare
 * @property string $SeatNum
 * @property string $IsFull
 * @property string $CancellationStatus
 * @property string $Remarks
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Bookingdetail $bookingdetail
 * @property \App\Models\Bookingmaster $bookingmaster
 *
 * @package App\Models
 */
class Bookingcancellation extends Eloquent
{
	protected $table = 'bookingcancellation';
	protected $primaryKey = 'CancellationId';
	public $timestamps = false;

	protected $casts = [
		'BookingMasterId' => 'int',
		'PassengerId' => 'int',
		'CancellationCharges' => 'float',
		'RefundAmt' => 'float',
		'CancellationFare' => 'float'
	];

	protected $dates = [
		'CancellationDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BookingMasterId',
		'PassengerId',
		'CancellationDate',
		'CancellationCharges',
		'RefundAmt',
		'CancellationFare',
		'SeatNum',
		'IsFull',
		'CancellationStatus',
		'Remarks',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bookingdetail()
	{
		return $this->belongsTo(\App\Models\Bookingdetail::class, 'PassengerId');
	}

	public function bookingmaster()
	{
		return $this->belongsTo(\App\Models\Bookingmaster::class, 'BookingMasterId');
	}
}
