<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyaddress
 * 
 * @property int $AddressId
 * @property int $PartyID
 * @property string $AddressTypeCode
 * @property string $AddressPartyType
 * @property string $PrimaryAddress
 * @property string $AddressLine1
 * @property string $AddressLine2
 * @property string $City
 * @property string $State
 * @property string $Country
 * @property string $PostalZipCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Addresstype $addresstype
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $bankbranches
 * @property \Illuminate\Database\Eloquent\Collection $usernomineedetails
 * @property \Illuminate\Database\Eloquent\Collection $verificationcenters
 * @property \Illuminate\Database\Eloquent\Collection $walletcards
 *
 * @package App\Models
 */
class Partyaddress extends Eloquent
{
	protected $table = 'partyaddress';
	protected $primaryKey = 'AddressId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'AddressId' => 'int',
		'PartyID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyID',
		'AddressTypeCode',
		'AddressPartyType',
		'PrimaryAddress',
		'AddressLine1',
		'AddressLine2',
		'City',
		'State',
		'Country',
		'PostalZipCode',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function addresstype()
	{
		return $this->belongsTo(\App\Models\Addresstype::class, 'AddressTypeCode');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function bankbranches()
	{
		return $this->hasMany(\App\Models\Bankbranch::class, 'BranchAddressId');
	}

	public function usernomineedetails()
	{
		return $this->hasMany(\App\Models\Usernomineedetail::class, 'GaurdianAddressID');
	}

	public function verificationcenters()
	{
		return $this->hasMany(\App\Models\Verificationcenter::class, 'CenterAddressID');
	}

	public function walletcards()
	{
		return $this->hasMany(\App\Models\Walletcard::class, 'DeliveryAddressID');
	}
}
