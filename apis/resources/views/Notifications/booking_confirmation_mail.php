<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Booking Confirmation Mail
 * 
 *  param $amount decimal
 *  param $from varchar
 *  param $to varchar
 *  param $email varchar
 *  param $placeOne varchar
 *  param $placeTwo varchar
 *  param $ticketNumber varchar
 *  param $busType varchar
 *  param $landmark varchar
 *  param $boardingFrom varchar
 *  param $seatNum varchar
 *  param $placename varchar
 *  param $operatorName varchar
 *  param $date Date
 *  param $day varchar
 *  param $datetime datetime
 *  param $departureDateTime datetime
 *  param $fareAmount decimal
 *  param $discountFareAmount decimal
 *  param $savingsAmount decimal
 *  param $boardingPointLocation varchar
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaWallet Bus booking: Ticket for ' . $from . ' - ' . $to . '. ',
        'text'    => $salutationName . ',<br/><br/>
                        

You Bus ticket booking is confirmed<br/>
e-ticket sent to : ' . $email . '<br/>

' . $placeOne . ' to ' . $placeTwo . ', ' . $date . ' & ' . $day . '
Onward Journey<br/>                                    
Ticket Number:' . $ticketNumber . '<br/>                  
Bus Type:' . $busType . ' Land Mark:' . $landmark . '<br/>    
Boarding from:' . $boardingFrom . '<br/>                   
Seat no.:' . $seatNum . '<br/>
Original Fare: '.$fareAmount.'<br/>
Discount Fare: '.$discountFareAmount.'<br/>
Your Savings Fare: '.$savingsAmount.'<br/>
Boarding Point: '.$boardingPointLocation.'<br/>
Landmark: ' . $landmark],
    'sms'   => [ 'text' => 'Ticket Number: ' . $ticketNumber . '
To: ' . $placename . '
Travel Operator Name: ' . $operatorName . '
Departure time & Date: ' . $departureDateTime . '
seat No:' . $seatNum . '
original Fare Amount: ' . $fareAmount . '
Discounted Fare Amount: ' . $discountFareAmount . '
your Savings amount: ' . $savingsAmount . '
Boarding Point Location:' . $boardingPointLocation . '
Landmark: ' . $landmark
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Bus Ticket From ' . $placeOne . ' to ' . $placeTwo . ' confirmed ',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'booking'
    ],
    'web'   => [
        'text' => 'Busbooking confirmed. Ticket number:' . $ticketNumber,
    ],
];

echo json_encode($array);

/* End of file booking_confirmation_mail */
