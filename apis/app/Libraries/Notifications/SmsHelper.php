<?php

namespace App\Libraries\Notifications;

class SmsHelper {

    static protected $smshub_url       = 'http://smshub.co.in/';
    static protected $smshub_userid    = 'vwalletdev';
    static protected $smshub_password  = 'vDlx1%20Z3C5n3$@D';
    static protected $smshub_sender_id = 'VIOLAD';

    /** send sms
     * @name type
     * @param string $sms_data array witch is send to api
     * @return void
     */
    public static function sendSmsNow($sms_data, $notificationId)
    {
        // Log variables
        $requestId      = uniqid();
        $startTime      = microtime(TRUE);
        
        $notifyUpdate   = array(
            'ReceipentReceivedDate' => date('Y-m-d H:i:s')
        );
        // Log Messge 
        $requestDetails = 'SMS Request ID : ' . $requestId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ', Mobile Number: ' . $sms_data['mobile_no'] . ' Templet : ' . json_encode($sms_data['message'], JSON_PRETTY_PRINT);
        \Log::info('Reponse:  ' . $requestDetails);

        $api_element = 'websms/sendsms.aspx';

        $mobile_no = $sms_data['mobile_no'];
        $message   = urlencode($sms_data['message']);

        if ( strlen($mobile_no) == 10 && preg_match('/^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$/', $mobile_no) )
        {
            $msgType = 0;
            if( strlen($message) > 160) {
                $msgType = 2;
            }            
            $send_message = SmsHelper::$smshub_url . $api_element . '?userid=' . SmsHelper::$smshub_userid . '&password=' . SmsHelper::$smshub_password . '&sender=' . SmsHelper::$smshub_sender_id . '&mobileno=' . $mobile_no . '&msg=' . $message.'&msgType='.$msgType;
            $output       = SmsHelper::_curl_send_sms($send_message);

            $requestDetails                          = 'SMS Response ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . 'Message Status: Sent' . $output;
            \Log::info('Reponse:  ' . $requestDetails);
            $notifyUpdate['ReceipentAcknowledged']   = 'Y';
            $notifyUpdate['ReceipentDeliveryReport'] = $output;
        }
        else
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = json_encode($output);
        }
        \App\Models\Notificationlog::where('NotificationID', $notificationId)->update($notifyUpdate);
        return TRUE;
    }

    /** send curl method
     * @name         type
     *
     * @param string $url array witch is send to api
     *
     * @return void
     */
    private static function _curl_send_sms($url)
    {
        $ch     = curl_init();                       // initialize CURL
        curl_setopt($ch, CURLOPT_POST, FALSE);    // Set CURL Post Data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        curl_close($ch);                         // Close CURL

        return $output;
    }

    /** get balance
     * @name type
     * @return void
     */
    public static function get_balance()
    {
        $url         = SmsHelper::$smshub_url;
        $api_element = 'websms/getbalance.aspx';
        $userid      = SmsHelper::$smshub_userid;
        $password    = SmsHelper::$smshub_password;

        $send_request = $url . $api_element . '?userid=' . $userid . '&password=' . $password;

        $output = SmsHelper::_curl_send_sms($send_request);
        return $output;
    }

    /** get message status
     * @name type
     * @param string $msg_id message id
     * @return void
     */
    public static function message_status($msg_id)
    {
        $url          = SmsHelper::$smshub_url;
        $api_element  = 'websms/messagestatus.aspx';
        $send_request = $url . $api_element . '?msgid=' . $msg_id;

        $output = SmsHelper::_curl_send_sms($send_request);
        return $output;
    }

}

/* End of file Api_send_sms.php */
    /* Location: ./system/application/libraries/Api_send_sms.php */
