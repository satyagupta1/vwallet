# RISK POLICY

**Overview**
Risk Management is a key business tool which allows the Management of Viola Grouo to objectively assess and, if necessary, plan for future events whether known or not.

Viola Group will maintain its own risk management framework with the ultimate objective being to identify any areas where it is exposed to risks in any part of the business, both internally and externally, with partners, customers and events beyond their control.

For the purposes of this document, risk is defined as:

**&#39;an uncertain event or set of events that should they occur will have a material effect on the achievement of the firm&#39;s objectives (Time, Quality, Cost, Scope, Benefits)&#39;**

**Risk Management Process**
It is everyone&#39;s responsibility to identify potential risks and bring them to the attention of the Compliance Officer who is primarily responsible to lead the risk management process on a day to day basis.  If a risk is identified, the Compliance Officer should be given as much information as possible and he is responsible for categorizing the risk.

Once received, the Compliance Officer will perform an initial assessment of the risk.  The risk will be measured by the probability (likelihood) of it occurring, and the impact (consequence), monetary and non-monetary, should it occur.

Each identified risk will be given a probability score and an impact score (Low, Medium-Low, Medium-High or High) in accordance with the following objective risk matrices, the values of which have been set by the Board.

The Impact and Probability scores then need to be referred to the Overall Risk Score Table to determine the overall level of risk posed.



**Probability Score**
[Probability Score](BASE_IMAGE_URL/Risk_Policy_V0.2_Probability_Score.png)


**Impact Score**
[Impact Score](BASE_IMAGE_URL/Risk_Policy_V0.2_Impact_Score.png)

**Overall Risk Score**
[Overall Risk Score]{BASE_IMAGE_URL/Risk_Policy_V0.2_Overall_Risk_Score.png}

Once this is determined, the Compliance Officer will populate the details onto the Risk Management Log.

**Risk Monitoring**
All risks deemed to be **Low (L)** will be reviewed on a regular basis during a Board Meeting that is held at least every three months.

If a risk has an overall risk score of **Medium-Low (ML)** the Compliance Officer will bring it to the attention of a member of the Board for review at the next Board meeting.

If a risk has an overall risk score of **Medium-High (MH)** the Compliance Officer will bring it to the attention of the entire Board for review at the next Board meeting.

If a risk is deemed to be **High (H)** the Compliance Officer will bring it to the attention of the entire Board for immediate review and agreement of effective actions.

The final risk score must be agreed on by the Compliance Officer and the affected Senior Officers of the relevant department.

All risks are reviewed on a regular basis during a Board Meeting that is held at least every three months. All new Risks will be discussed based on their risk score.  The Risk Management Log forms the basis for this meeting.

**Prioritisation of Risks**
Once a risk has been categorized, actions need to be agreed to protect the firm from the material effect of the risk.

Risks with a **LOW** risk score (L) should have actions agreed to monitor or accept the risk.  Risks with a LOW risk score are discussed as part of the Board Meeting.

Risks with a **MEDIUM-LOW** risk rating (ML) should have action agreed to mitigate, transfer or monitor the risk at the next Board Meeting.  The actions must be agreed by the Compliance Officer and the Board.

Risks with a **MEDIUM-HIGH** risk score (MH) need actions agreed to mitigate or transfer the risk as soon as possible.  The actions must be agreed by the Compliance Officer and the Board as soon as practicable.

Risks with a **HIGH-RISK** score (H) need immediate actions to either mitigate or transfer the risk and should be treated as a priority.  The actions must be agreed by the Compliance Officer and the Board immediately.

Actions are reviewed on a regular basis during Board Meetings, depending on prioritization.

**Crystallised Risks**
If a risk actually occurs (the threat is realized), the Compliance Officer and the Board must address the risk and activate the steps outlined in the Risk Management Log, as relevant.  Once the risks are contained, the Compliance Officer must review the root cause for the event to see what lessons can be learnt and update the Risk Log accordingly.

If it should have been possible to positively affect the risk, then systems and controls should be implemented to protect against the risk reoccurring.

If it would not have been possible to affect whether the risk occurred (for example, the introduction of new regulations) then systems and controls should be put in place to reduce the effect on the business the next time the risk occurs.

**Management Information**
The key piece of management information will be the Risk Management Log.  This will be used to record all identified risks and will form the basis for the regular reviews at Board Meetings.

A dated version of the log should be saved immediately before each review in order that an archive exists showing the historic development of risks.

**Ownership**
The Compliance Officer is responsible for incorporating and maintaining this policy, and the day to day control of the Risk Management Log, ensuring it forms an effective part of Viola Group&#39;s attitude to risk.  The Board has ultimate responsibility for Risk Management within the company, and setting the risk appetite.