<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userrechargetracker
 * 
 * @property int $UserRechargeTrackerID
 * @property int $RechargeDetailID
 * @property string $Status
 * @property string $Comments
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * 
 * @property \App\Models\Userrechargedetail $userrechargedetail
 *
 * @package App\Models
 */
class Userrechargetracker extends Eloquent
{
	protected $table = 'userrechargetracker';
	public $timestamps = false;

	protected $casts = [
		'RechargeDetailID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime'
	];

	protected $fillable = [
		'Status',
		'Comments',
		'CreatedBy',
		'CreatedDatetime'
	];

	public function userrechargedetail()
	{
		return $this->belongsTo(\App\Models\Userrechargedetail::class, 'RechargeDetailID');
	}
}
