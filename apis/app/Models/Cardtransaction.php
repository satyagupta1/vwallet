<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Cardtransaction
 * 
 * @property int $idCardTransaction
 * @property int $fromCardId
 * @property int $toCardId
 * @property string $Curcode
 * @property float $Amount
 * @property string $exchange
 * @property string $Transactionid
 * @property string $TripleClickTransaction
 * @property string $Status
 * @property \Carbon\Carbon $TrasactionDate
 * @property string $TransactionMode
 * 
 * @property \App\Models\Carduser $carduser
 *
 * @package App\Models
 */
class Cardtransaction extends Eloquent
{
	protected $table = 'cardtransaction';
	protected $primaryKey = 'idCardTransaction';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'idCardTransaction' => 'int',
		'fromCardId' => 'int',
		'toCardId' => 'int',
		'Amount' => 'float'
	];

	protected $dates = [
		'TrasactionDate'
	];

	protected $fillable = [
		'fromCardId',
		'toCardId',
		'Curcode',
		'Amount',
		'exchange',
		'Transactionid',
		'TripleClickTransaction',
		'Status',
		'TrasactionDate',
		'TransactionMode'
	];

	public function carduser()
	{
		return $this->belongsTo(\App\Models\Carduser::class, 'toCardId');
	}
}
