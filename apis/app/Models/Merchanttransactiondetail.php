<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchanttransactiondetail
 * 
 * @property int $MerchantTransactionDetailID
 * @property int $MerchantID
 * @property string $ViolaloginID
 * @property int $MerchangePayDevicesID
 * @property int $MerchangeReferalType
 * @property int $TransactionMediumRefID
 * @property string $BillNumber
 * @property string $MerchantExternalTransactionID
 * @property string $ViolaTransactionID
 * @property string $OperatorID
 * @property string $PartyemailId
 * @property string $PartymobileNumber
 * @property int $TransactionDetailID
 * @property float $TransactionAmount
 * @property string $TransactionCurrency
 * @property string $TransactionStatus
 * @property \Carbon\Carbon $TransactionValueDate
 * @property \Carbon\Carbon $CreatedDateTime
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $CreatedBy
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Merchantpaydevice $merchantpaydevice
 * @property \App\Models\Merchantwalletregistration $merchantwalletregistration
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \Illuminate\Database\Eloquent\Collection $merchantapimessages
 *
 * @package App\Models
 */
class Merchanttransactiondetail extends Eloquent
{
	protected $table = 'merchanttransactiondetail';
	protected $primaryKey = 'MerchantTransactionDetailID';
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int',
		'MerchangePayDevicesID' => 'int',
		'MerchangeReferalType' => 'int',
		'TransactionMediumRefID' => 'int',
		'TransactionDetailID' => 'int',
		'TransactionAmount' => 'float'
	];

	protected $dates = [
		'TransactionValueDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'MerchantID',
		'ViolaloginID',
		'MerchangePayDevicesID',
		'MerchangeReferalType',
		'TransactionMediumRefID',
		'BillNumber',
		'MerchantExternalTransactionID',
		'ViolaTransactionID',
		'OperatorID',
		'PartyemailId',
		'PartymobileNumber',
		'TransactionDetailID',
		'TransactionAmount',
		'TransactionCurrency',
		'TransactionStatus',
		'TransactionValueDate',
		'CreatedDateTime',
		'UpdatedDateTime',
		'CreatedBy',
		'UpdatedBy'
	];

	public function merchantpaydevice()
	{
		return $this->belongsTo(\App\Models\Merchantpaydevice::class, 'MerchangePayDevicesID');
	}

	public function merchantwalletregistration()
	{
		return $this->belongsTo(\App\Models\Merchantwalletregistration::class, 'MerchantID');
	}

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}

	public function merchantapimessages()
	{
		return $this->hasMany(\App\Models\Merchantapimessage::class, 'MerchantTransactionDetailID');
	}
}
