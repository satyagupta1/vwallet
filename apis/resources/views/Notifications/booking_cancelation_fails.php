<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Booking Cancelation Fails a device template
 * 
 *  param $fullname varchar
 *  param $amount varchar
 *  param $bookingnumber varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Ticket booking failed',
        'text'    => $salutationName . ',<br/><br/>
Attention!!<br/>
Your payment of ' . $amount . ' towards movie name ' . $fullname . ' was not successful due to network error/ technical error/ no response from third party/ session expired/ transaction cancelled.<br/>
Please check and try again later.',
    ],
    'sms'   => [
        'text' => 'Your payment of INR ' . $amount . ' was cancelled Date Time Booking Number:' . $bookingnumber . ' Check with your service provider or try again later.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Movie Booking failed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'booking'
    ],
    'web'   => [
        'text' => 'Movie ticket booking failed',
    ],
];

echo json_encode($array);

/* End of file booking_cancelation_fails.php */
