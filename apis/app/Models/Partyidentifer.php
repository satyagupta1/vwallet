<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyidentifer
 * 
 * @property int $PartyID
 * @property int $IdentifierTypeID
 * @property string $IdentifierNumber
 * @property string $NameOnIdentifier
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Identifiertype $identifiertype
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partyidentifer extends Eloquent
{
	protected $table = 'partyidentifer';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int',
		'IdentifierTypeID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'IdentifierNumber',
		'NameOnIdentifier',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function identifiertype()
	{
		return $this->belongsTo(\App\Models\Identifiertype::class, 'IdentifierTypeID');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
}
