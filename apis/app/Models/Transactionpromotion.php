<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactionpromotion
 * 
 * @property int $TransactionDetailID
 * @property string $PromoCode
 * 
 * @property \App\Models\Promotion $promotion
 * @property \App\Models\Transactiondetail $transactiondetail
 *
 * @package App\Models
 */
class Transactionpromotion extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TransactionDetailID' => 'int'
	];

	public function promotion()
	{
		return $this->belongsTo(\App\Models\Promotion::class, 'PromoCode');
	}

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}
}
