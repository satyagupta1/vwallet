<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Offerbenefit
 * 
 * @property int $OfferBenefitID
 * @property int $BenefitBusinessID
 * @property float $BenefitVersion
 * @property string $BenefitName
 * @property string $BenefitDescription
 * @property string $BenefitEffectiveDate
 * @property string $BenefitExpiryDate
 * @property int $BenefitCategoryRefID
 * @property int $BenefitCriteriaRefID
 * @property int $BenefitStatusRefID
 * @property int $BenefitApprovedStatusRefID
 * @property string $ApprovedBy
 * @property int $AdminWalletAccountId
 * @property string $Comments
 * @property string $ActiveInd
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $BenefitApplicableChannel
 * @property float $BenefitFixedAmount
 * @property float $BenefictPercentageValue
 * 
 * @property \Illuminate\Database\Eloquent\Collection $offerbenefiteligibilities
 * @property \Illuminate\Database\Eloquent\Collection $offerbenefitsbridges
 *
 * @package App\Models
 */
class Offerbenefit extends Eloquent
{
	protected $primaryKey = 'OfferBenefitID';
	public $timestamps = false;

	protected $casts = [
		'BenefitBusinessID' => 'int',
		'BenefitVersion' => 'float',
		'BenefitCategoryRefID' => 'int',
		'BenefitCriteriaRefID' => 'int',
		'BenefitStatusRefID' => 'int',
		'BenefitApprovedStatusRefID' => 'int',
		'AdminWalletAccountId' => 'int',
		'BenefitFixedAmount' => 'float',
		'BenefictPercentageValue' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BenefitBusinessID',
		'BenefitVersion',
		'BenefitName',
		'BenefitDescription',
		'BenefitEffectiveDate',
		'BenefitExpiryDate',
		'BenefitCategoryRefID',
		'BenefitCriteriaRefID',
		'BenefitStatusRefID',
		'BenefitApprovedStatusRefID',
		'ApprovedBy',
		'AdminWalletAccountId',
		'Comments',
		'ActiveInd',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'BenefitApplicableChannel',
		'BenefitFixedAmount',
		'BenefictPercentageValue'
	];

	public function offerbenefiteligibilities()
	{
		return $this->hasMany(\App\Models\Offerbenefiteligibility::class, 'BenefitBusinessID', 'BenefitBusinessID');
	}

	public function offerbenefitsbridges()
	{
		return $this->hasMany(\App\Models\Offerbenefitsbridge::class, 'BenefitBusinessID', 'BenefitBusinessID');
	}
}
