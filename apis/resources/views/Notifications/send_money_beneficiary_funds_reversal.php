<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Funds Reversal template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $referenceNum varchar
 *  param $support_details varchar for support number and email 
 *  param $cbPercentage varchar for cashback percentage
 *  param $dateTime varchar for cashback percentage
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Cashback Reversal',
        'text'    => $salutationName . ',<br/><br/>
Payment of ' . $amount . ' was declined as the beneficiary is not a Viola Customer/ non KYC customer. <br/>
    Transaction ID: ' . $referenceNum . '<br/>
    Time: '.$dateTime.'<br/>
    Medium: ' . $medium],
    'sms'   => [
        'text' => $salutationName . ',
            Payment of '.$amount.' was declined as beneficiary is Not a Viola Customer/ non KYC customer. 
                Transaction ID: ' . $referenceNum . '
                Time: '.$dateTime.'
                Medium: ' . $medium,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cashback reversal',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'INR '. $ammount .' cashback reversed',
    ],
];

echo json_encode($array);

/* End of file send_money_beneficiary_funds_reversal.php */
