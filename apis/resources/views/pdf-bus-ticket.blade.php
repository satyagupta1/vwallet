<!doctype html>
<html>

    <head>
        <meta charset="utf-8">
        <title>ViolaWallet Bus Ticket Template</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                font-size: 12px;
                color: #333;
                font-weight: normal;
                letter-spacing: .5px;
            }

            span {
                display: block;
            }

            p,
            span {
                margin: 0px;
                padding: 0px;
            }

            i {
                font-style: normal;
                font-family: Arial, sans-serif;
            }

            h4 {
                margin: 0px;
                padding: 0px;
                font-weight: 200;
            }

            .passenger-content i {
                display: block;
                padding-bottom: 5px;
                padding-top: 5px;
            }

            .invoice-box {
                max-width: 700px;
                margin: auto;
                padding-bottom: 10px;
                margin-bottom: 10px;
            }

            b {
                font-size: 12px;
            }

            ol {
                padding-left: 15px;
            }
        </style>
    </head>

    <body>
        <div class="invoice-box">
            <table>
                <tr>
                    <td style="padding: 20px 0px;">
                        <div style="display:inline-block;margin-bottom:6pt;padding:0;border-right: 1px solid #ccc;"> <img src="{{ url('public/viola_wallet.png') }}" alt="ViolaWalletLogo" id="logo" border="0" style="margin-right: 15px; margin-top: 15px; width: 120px;"> </div>
                    </td>
                    <td colspan="2" style="padding:2.0pt; text-align: center"> <span style="font-size: 20px;">My-eTICKET</span> </td>
                    <td colspan="4" style="padding:5px 0px; text-align:right;"> <span style="font-size: 14px;"><b style="">Need help?</b></span> <span style="margin-top:5px;"><b>Boarding Point Contact:</b>
                            <p style="">{{$OperatorContactNo}}</p></span> <span style="margin-top:5px; margin-bottom: 15px;"><b>{{$OperatorName}} &amp; Travels-Customer Care:</b><br>4044454647</span> <span>Write to us <a href="mailto:supportviola.com">Supportviola.com</a></span> </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding:15px 0; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;">
                        <h4 style="font-size: 16px; font-weight:500;"> <b style="font-size: 16px;">{{$SourceName}} - {{$DestinationName}}</b><br>{{ date('l, F, j, Y', strtotime($JourneyDate)) }}</h4> </td>
                    <td colspan="4" style="padding:15px 0; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: right;"> <span><b style="font-size:12px; margin-bottom: 10px;">PNR no: {{$TicketNum}}</b></span>
                        <p style="font-size: 12px; margin-top: 5px;">Ticket no: {{$RedbusBookingId}}/{{$TransactionOrderID}}</p>
                    </td>
                </tr>
                <!--  bus details  -->
                <tr>
                    <td colspan="3" style="width: 30%; padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$OperatorName}} &amp; Travels</b></span> <i style="font-size: 9pt; color: #999;">{{$BusType}}</i> </td>
                    <td style="width: 20%; padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{date('H:i A', strtotime($boardingTime.' -15 minutes'))}}</b></span> <i style="font-size: 9pt; color: #999;">Reporting time</i> </td>
                    <td style="width: 20%; padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{date('H:i A', strtotime($boardingTime))}}</b></span> <i style="font-size: 9pt; color: #999;">Departure time</i> </td>
                    <td colspan="2" style="width: 30%;padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$NoOfBookedSeats - $NoOfCancelledSeats}}</b></span> <i style="font-size: 9pt; color: #999;">No. of Passengers</i> </td>				
                </tr>
                <tr>
                    <td colspan="3" style="padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>Boarding point details</b></span> </td>
                    <td style="padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$BoardingPoint}}</b></span> <i style="font-size: 9pt; color: #999;">Location</i> </td>
                    <td style="padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>Dropping Point</b></span> </td>
                    <td colspan="2" style="padding:7.5pt;border-bottom: 1px solid #ddd;"> <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$DroppngPoint}}</b></span> <i style="font-size: 9pt; color: #999;">Location  </i> </td>			
                </tr>
                <tr class="passenger-content">
                    <td colspan="7">
                        <table style="width:700px; " cellspacing="3" cellpadding="0" border="0">
                            <tr class="passenger-content">
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Passenger </b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Seat No.</b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Age</b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Gender</b></span>
                                </td>
                            </tr>
                            @foreach ($passengerInfo as $pass)
                            <tr class="passenger-content">
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['paxName']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['seatNumber']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['age']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['gender']}}</i>
                                </td>
                            </tr>
                            @endforeach
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0px;text-align: right;"> <span style="font-size:9pt; margin-bottom: 5px; margin-top: 20px;"><b>Total Fare : Rs. {{number_format($TotalFare, 2)}}</b></span> <span style="font-size:9pt;color: #999; margin-bottom:15px;">(Rs. {{number_format(($ServiceCharges + $Tax),2) }} inclusive of GST and service charge, if any)</span> <span style="font-size:10pt;"><b>Discounted Fare : Rs. {{number_format($BaseFare - ($ServiceCharges + $Tax),2) }}</b></span> </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; "> <span style="font-size:18px;margin-top: 15px;margin-bottom: 10px; border-bottom: 2px solid #333; padding-bottom: 5px; text-align: center;border-top:2px solid #333; padding-top: 5px;">Terms and Conditions </span>
                        <ol style="margin-bottom:0;">
                            <li style="margin:0;"><span style="font-size:9px;">ViolaWallet is enabler of online ticketing. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with an aggregator. ViolaWallet's advice to customers is to choose bus operators they are aware of and whose service they are comfortable
                                    with. </span> </li>
                        </ol>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; ">
                        <div style="margin:14pt 0 0 36pt;"> <span><b style="font-size:9px;">ViolaWallet responsibilities include:</b></span> </div>
                        <div style="margin:14pt 0 0 50pt;"> <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(1) Issuing a valid ticket (a ticket that will be accepted by the bus operator) for its network of bus operators</span> <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(2) Providing refund and support in the event of cancellation</span> <span style="text-indent:-18pt; font-size:9px; margin-bottom: 10px;">(3) Providing customer support and information in case of any delays / inconvenience</span> </div>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; ">
                        <div style="margin:14pt 0 0 36pt;"> <span ><b style="font-size:9px;">ViolaWallet responsibilities do not include:</b></span> </div>
                        <div style="margin:14pt 0 0 50pt;"> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(1) The bus operator's bus not departing / reaching on time</span> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(2) The bus operator's employees being rude.</span> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; ">
                        <div style="margin:0 0 0 50pt;"> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(3) The bus operator's bus seats etc not being up to the customer's expectation.</span> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(4) The bus operator canceling the trip due to unavoidable reasons.</span> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; ">
                        <div style="margin:0 0 0 50pt;"> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(5) he baggage of the customer getting lost / stolen / damaged.</span> 
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(6) The bus operator changing a customer's seat at the last minute to accommodate a lady / child.</span> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="padding:0; width: 100%; ">
                        <div style="margin:0 0 0 50pt;">
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(7) The customer waiting at the wrong boarding point (please call the bus operator to find out the exact boarding point if you are not a regular traveler on that particular bus).</span>
                            <span style="text-indent:-18pt; font-size: 9px; margin-bottom: 10px;">(8) The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take customers to the bus departure point.</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <ol start="2" style="margin-bottom:0;">
                            <li style="margin-bottom: 10px;"><span style="font-size:9px;">The departure time mentioned on the ticket are only tentative timings.However the bus will not leave the source before the time that is mentioned on the ticket.</span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Passengers are required to furnish the following at the time of boarding the bus: </span> <span style="text-indent:-15pt; font-size:9px; margin-bottom: 10px;margin-left: 18px;">(1) A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail).
                                </span> <span style="text-indent:-15pt; font-size: 9px; margin-bottom: 10px;margin-left: 18px;">(2) A valid identity proof  Failing to do so, they may not be allowed to board the bus.</span> </li>
                        </ol>
                        <ol start="4" style="margin-bottom:0;">
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Change of bus: In case the bus operator changes the type of bus due to some reason, ViolaWallet will refund the differential amount to the customer upon being intimated by the customers in 24 hours of the journey.</span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Amenities for this bus as shown on ViolaWallet have been configured and provided by the bus provider (bus operator). These amenities will be provided unless there are some exceptions on certain days. Please note that ViolaWallet provides this information in good faith to help passengers to make an informed decision. The liability of the amenity not being made available lies with the operator and not with ViolaWallet.</span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">In case a booking confirmation e-mail and sms gets delayed or fails because of technical reasons or as a result of incorrect e-mail ID / phone number provided by the user etc, a ticket will be considered 'booked' as long as the ticket shows up on the confirmation page of <a href="www.ViolaWallet.com"> www.ViolaWallet.com</a></span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Grievances and claims related to the bus journey should be reported to ViolaWallet support team within 10 days of your travel date.</span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Please note the following regarding the luggage policy for your journey:
                                </span> <span style="text-indent:-15pt; font-size: 9px; margin-bottom: 10px;margin-left: 18px;">(1) Each passenger is allowed to carry one bag of upto 10 kgs and one personal item such as a laptop bag, handbag, or briefcase of upto 5 kgs.

                                </span> <span style="text-indent:-15pt; font-size: 9px; margin-bottom: 10px;margin-left: 18px;">(2) Passengers should not carry any goods like weapons, inflammable, firearms, ammunition, drugs, liquor, smuggled goods etc and any other articles that are prohibited under law.

                                </span> <span style="text-indent:-15pt; font-size:9px; margin-bottom: 10px;margin-left: 18px;">(3) Bus Operator reserves the right to deny boarding or charge additional amount in case passenger is travelling with extra luggage than what is mentioned above.

                                </span> </li>
                            <li style="margin-bottom: 10px; "><span style="font-size:9px; margin-bottom: 10px;">Partial Cancellation is <b style="font-size:10px;">NOT</b> allowed for this ticket. Charges for complete ticket cancellation are mentioned below.</span> </li>
                            <li style="margin-bottom: 10px;"> <span style="font-size:9px; margin-bottom: 10px;">Cancellation of this ticket is <b style="font-size: 10px;" >NOT</b>	allowed after bus departure time. </span> </li>
                        </ol>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <table style="width:100%;margin-bottom:0.75pt;margin-top: 15px;" width="500" cellspacing="0" cellpadding="3" border="1px solid #ddd" align="left">
                            <tr>
                                <td style="width:60%;padding:5px 10px;"> <span style="font-size:7.5pt;"><b>Cancellation time</b></span> </td>
                                <td style="width:29%;padding:5px 10px;"> <span style="font-size:7.5pt;"><b>Cancellation charges</b></span> </td>
                            </tr>
                            @foreach ($cancelPolicy as $can)
                            <tr>
                                <td style="width:60%;padding:5px 10px;">
                                    <span style="font-size:7.5pt;">{{$can['policy']}}</span>
                                </td>
                                <td style="width:29%;padding:5px 10px;">
                                    <span style="font-size:7.5pt;">  Rs. {{number_format($can['amont'],2)}} </span>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
            </table>






        </div>
    </body>

</html>