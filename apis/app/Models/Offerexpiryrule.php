<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Offerexpiryrule
 * 
 * @property int $OfferUserEligibilityID
 * @property int $OfferBusinessID
 * @property float $OfferVersion
 * @property int $RuleCategoryRefID
 * @property int $RuleNameRefID
 * @property int $RuleOperatorRefID
 * @property string $RuleParamValues
 * 
 * @property \App\Models\Promotionaloffer $promotionaloffer
 *
 * @package App\Models
 */
class Offerexpiryrule extends Eloquent
{
	protected $primaryKey = 'OfferUserEligibilityID';
	public $timestamps = false;

	protected $casts = [
		'OfferBusinessID' => 'int',
		'OfferVersion' => 'float',
		'RuleCategoryRefID' => 'int',
		'RuleNameRefID' => 'int',
		'RuleOperatorRefID' => 'int'
	];

	protected $fillable = [
		'OfferBusinessID',
		'OfferVersion',
		'RuleCategoryRefID',
		'RuleNameRefID',
		'RuleOperatorRefID',
		'RuleParamValues'
	];

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'OfferBusinessID', 'OfferBusinessID')
					->where('promotionaloffers.OfferBusinessID', '=', 'offerexpiryrules.OfferBusinessID')
					->where('promotionaloffers.OfferVersion', '=', 'offerexpiryrules.OfferVersion');
	}
}
