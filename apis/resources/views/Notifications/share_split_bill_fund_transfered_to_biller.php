<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  After funds transaferred to the biller (With Transaction details and Available balance) template
 *  param $amount varchar
 *  param $billName varchar
 *  param $transactionId varchar 
 *  param $balanceAmount varchar  
 */

$salutation = trans('messages.salutation');
$salutationName = ($customerName) ? $salutation . ' ' . $customerName : $salutation;
$array = [
    'email' => [
        'subject' => 'Payment transferred',
        'text'    => $salutationName . ',<br/><br/>
    your payment of '. $ammount .' is successful for the <br/><br/>
        Transaction ID: '.$trackingNumber.'  <br/>  
        Date & Time: '.$dateTime.'  <br/>
        Merchant ID: '.$merchantId],
    'sms'   => [
        'text' => ' 
            Rs ' . $amount . ' is paid for ' . $billName . '. 
                Transaction ID: ' . $transactionId . ' .Avl bal Rs ' . $balanceAmount . '
 ',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file share_split_bill_fund_transfered_to_biller.php */
