<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 16 Nov 2017 06:22:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Billerlist
 * 
 * @property string $BillerId
 * @property string $BillerName
 * @property string $Category
 * @property string $SubCategory
 * @property string $Location
 * @property string $BillerType
 * @property string $NoOfAuthenticators
 * @property string $Ref1FieldName
 * @property string $Ref1FieldValidation
 * @property string $Ref1FieldMessage
 *
 * @package App\Models
 */
class Billerlist extends Eloquent
{
	protected $table = 'billerlist';
	protected $primaryKey = 'BillerId';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
    	'BillerId',	
            'BillerName',
		'Category',
		'SubCategory',
		'Location',
		'BillerType',
		'NoOfAuthenticators',
		'Ref1FieldName',
		'Ref1FieldValidation',
		'Ref1FieldMessage'
	];
}
