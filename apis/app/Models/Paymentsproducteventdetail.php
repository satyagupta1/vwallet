<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Paymentsproducteventdetail
 * 
 * @property int $PaymentsProductEventDetailID
 * @property int $PaymentProductEventID
 * @property string $EventCode
 * @property int $PaymentsProductID
 * @property string $CrDrIndicator
 * @property string $AmountTag
 * @property string $IsAdminWalletEntry
 * @property string $FromAccount
 * @property string $FromAccountTag
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \App\Models\Paymentproductevent $paymentproductevent
 *
 * @package App\Models
 */
class Paymentsproducteventdetail extends Eloquent
{
	protected $table = 'paymentsproducteventdetail';
	protected $primaryKey = 'PaymentsProductEventDetailID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PaymentsProductEventDetailID' => 'int',
		'PaymentProductEventID' => 'int',
		'PaymentsProductID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PaymentProductEventID',
		'EventCode',
		'PaymentsProductID',
		'CrDrIndicator',
		'AmountTag',
		'IsAdminWalletEntry',
		'FromAccount',
		'FromAccountTag',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function paymentproductevent()
	{
		return $this->belongsTo(\App\Models\Paymentproductevent::class, 'PaymentProductEventID')
					->where('paymentproductevent.PaymentProductEventID', '=', 'paymentsproducteventdetail.PaymentProductEventID')
					->where('paymentproductevent.EventCode', '=', 'paymentsproducteventdetail.EventCode')
					->where('paymentproductevent.PaymentsProductID', '=', 'paymentsproducteventdetail.PaymentsProductID');
	}
}
