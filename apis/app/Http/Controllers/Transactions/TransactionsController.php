<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;

/**
 * TransctionlistController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TransactionsController extends Controller {

    public function __construct()
    {
        $this->perpage  = config('vwallet.perpage');
        $this->kycLimit = config('vwallet.MONTH_LIMIT_KYC');
        $this->lang     = app('translator')->getLocale();
    }

    /*
     *  Search Transaction - Advanced Transaction Search - View Transaction
     *  @Desc This is used to get results with given params, orderby, limit and pagination.
     *  @param Request $request
     *  @return JSON
     */

    public function transactionList(\Illuminate\Http\Request $request)
    {

        //$currency = \App\Models\Transaction::find(1)->currency()->get(); 
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateMethodTTransaction($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId            = $request->input('partyId');
        $transactionId      = $request->input('transactionId');
        $transactionOrderId = $request->input('transactionOrderId');
        $tranTypeCode       = $request->input('transactionTypeCode');
        $tranType           = $request->input('transactionType');
        $tranMode           = $request->input('transactionMode');
        $tranIndicator      = $request->input('transactionIndicator');

        $tranStatus = $request->input('transactionStatus');
        $fromDate   = $request->input('fromDate');
        $toDate     = $request->input('toDate');
        $fromAmount = $request->input('fromAmount');
        $toAmount   = $request->input('toAmount');


        $searchField = $request->input('searchField');
        $mobileNo    = $request->input('mobileNumber');
        $fullName    = $request->input('fullName');
        $emailId     = $request->input('email');
        $violaId     = $request->input('violaId');

        $orderBy       = $request->input('orderBy');
        $orderByColumn = $request->input('orderColumn');
        $limit         = $request->input('limit');
        $paginate      = $request->input('paginate');

        $selectedColumns = [
            'transactiondetail.TransactionOrderID',
            'transactiondetail.FromAccountName',
            'transactiondetail.TransactionDate',
            'transactiondetail.PaymentsProductID',
            'transactiondetail.FromAccountId',
            'transactiondetail.ToWalletId',
            'Amount',
            'WalletAmount',
            'DiscountAmount',
            'CashbackAmount',
            'PromoCode',
            'TransactionTypeCode',
            'TransactionVendor',
            'TransactionStatus',
            'TransactionMode',
            'TransactionType',
            'FirstName',
            'LastName',
            'MobileNumber',
            'ViolaID',
            'EmailID',
            'FeeAmount',
            'TaxAmount',
            'partypreferredpaymentmethod.CardDisplayNumber',
            'paymentsproduct.ProductName',
            'paymentsproduct.DisplayName',
            'paymentsproduct.ProductType',
            'transactioneventlog.Comments',
            'dailytransactionlog.CrDrIndicator',
            'dailytransactionlog.TransactionCurrency',
            'dailytransactionlog.PartyId',
            'dailytransactionlog.TransactionDetailID'
        ];

        $whereArray['dailytransactionlog.PartyId'] = $partyId;
        //filter by beneficiary type
        if ( $transactionId !== NULL )
        {
            $whereArray['transactiondetail.TransactionDetailID'] = $transactionId;
        }
        //filter by beneficiary type
        if ( $transactionOrderId !== NULL )
        {
            $whereArray['transactiondetail.TransactionOrderID'] = $transactionOrderId;
        }
        //filter by Transaction Type Code
        if ( $tranTypeCode !== NULL )
        {
            $whereArray['transactiondetail.TransactionTypeCode'] = $tranTypeCode;
        }

        //filter by Transaction Mode
        if ( $tranMode !== NULL )
        {
            $whereArray['transactiondetail.TransactionMode'] = $tranMode;
        }

        //filter by Transaction Indicator

        if ( $tranIndicator !== NULL )
        {
            $whereArray['dailytransactionlog.CrDrIndicator'] = $tranIndicator;
            //$query->where('dailytransactionlog.CrDrIndicator', '=', $tranIndicator);
        }
        //filter by triple click
        if ( $tranType !== NULL )
        {
            if ( $tranType == 'W2W' )
            {
                $tranType = 'PAY W2W';
            }
            if ( $tranType == 'W2B' )
            {
                $tranType = 'PAY W2B';
            }
            if ( $tranType == 'QR' )
            {
                $tranType = 'QR CODE';
            }
            $whereArray['transactiondetail.TransactionType'] = $tranType;
        }


        if ( $tranStatus !== NULL )
        {
            $whereArray['transactiondetail.TransactionStatus'] = $tranStatus;
        }

        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }

        //Order by table column
        if ( $orderByColumn == NULL || ! in_array($orderByColumn, $selectedColumns) )
        {
            $orderByColumn = 'transactiondetail.TransactionDetailID';
        }



        //$query = \App\Models\Transactiondetail::join('userwallet', 'userwallet.UserWalletID', '=', 'transactiondetail.ToWalletId')
        $query = \App\Models\Transactiondetail::join('dailytransactionlog', 'transactiondetail.TransactionDetailID', '=', 'dailytransactionlog.TransactionDetailID')
                        ->join('userwallet', 'dailytransactionlog.PartyId', '=', 'userwallet.UserWalletID')
                        ->leftjoin('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                        ->join('paymentsproduct', 'transactiondetail.PaymentsProductID', '=', 'paymentsproduct.PaymentsProductID')
                        ->join('transactioneventlog', 'transactiondetail.TransactionDetailID', '=', 'transactioneventlog.TransactionDetailID')
                        ->where($whereArray)->where('dailytransactionlog.TransactionLineNumber', '<=', 2)->orderBy($orderByColumn, $orderBy);


        //common search field check
        if ( $searchField !== NULL )
        {

            $query->where(function ($query1) use ($searchField) {
                $query1->where('userwallet.FirstName', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.LastName', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.MobileNumber', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.EmailID', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.ViolaID', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.FromAccountName', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.TransactionOrderID', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.TransactionStatus', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.Amount', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.WalletAmount', 'LIKE', '%' . $searchField . '%');
            });
        }
        /* if ( $searchField !== NULL )
          {

          $query->where(function ($query1) use ($searchField) {
          $query1->where('userwallet.FirstName', 'LIKE', '%' . $searchField . '%')
          ->orWhere('userwallet.LastName', 'LIKE', '%' . $searchField . '%')
          ->orWhere('userwallet.MobileNumber', 'LIKE', '%' . $searchField . '%')
          ->orWhere('userwallet.EmailID', 'LIKE', '%' . $searchField . '%')
          ->orWhere('userwallet.ViolaID', 'LIKE', '%' . $searchField . '%')
          ->orWhere('transactiondetail.FromAccountName', 'LIKE', '%' . $searchField . '%');
          });
          }
          else
          {
          if ( $fullName !== NULL )
          {
          $query->where(function ($query1) use ($fullName) {
          $query1->where('userwallet.FirstName', 'LIKE', '%' . $fullName . '%')
          ->orWhere('userwallet.LastName', 'LIKE', '%' . $fullName . '%');
          });
          }
          //filter by recepent info
          if ( $mobileNo !== NULL )
          {
          $query->where('userwallet.MobileNumber', '=', $mobileNo);
          }
          if ( $emailId !== NULL )
          {
          $query->where('userwallet.EmailID', '=', $emailId);
          }
          if ( $violaId !== NULL )
          {
          $query->where('userwallet.ViolaID', '=', $violaId);
          }
          } */

        //Date filters 
        if ( $fromDate !== NULL && $toDate !== NULL )
        {
            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($fromDate));
            $ymdToDate   = date('Y-m-d' . ' 23:59:59', strtotime($toDate));
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        elseif ( $fromDate !== NULL )
        {

            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($fromDate));
            $ymdToDate   = date('Y-m-d' . ' 23:59:59');
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        elseif ( $toDate !== NULL )
        {
            $ymdToDate   = date('Y-m-d' . ' 23:59:59', strtotime($toDate));
            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($toDate));
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        else
        {
            $ymdToDate   = date('Y-m-d' . ' 23:59:59');
            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime('-90 days'));
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        //Amout filters 
        if ( $fromAmount !== NULL && $toAmount !== NULL )
        {
            $query->whereBetween('transactiondetail.Amount', [ $fromAmount, $toAmount ]);
        }
        //Limit records
        if ( $limit != NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $userDetails    = $query->select($selectedColumns)->paginate($this->perpage)->toArray();
            $countResults   = $userDetails['total'];
            $resData        = array();
            $toMobileNumber = '';
            $toName         = '';
            $toAcntNumber   = '';
            $toIfsc         = '';
            $fullName       = '';

            foreach ( $userDetails['data'] as $resUserdetails )
            {

                // wallet to wallet txn
                if ( $resUserdetails['PaymentsProductID'] == 3 || $resUserdetails['PaymentsProductID'] == 2 )
                {

                    $getBeneficaryData = $this->getUserBeneficiaryData($resUserdetails['ToWalletId']);

                    $toMobileNumber  = ($getBeneficaryData['BeneficiaryMobileNumber']) ? $getBeneficaryData['BeneficiaryMobileNumber'] : '';
                    $getNameByMobile = $this->getNameByMobile($toMobileNumber);
                    if ( $getNameByMobile['FirstName'] )
                    {
                        $fullName = $getNameByMobile['FirstName'] . ' ' . ($getNameByMobile['MiddleName'] ? $getNameByMobile['MiddleName'] : '') . ' ' . ($getNameByMobile['LastName'] ? $getNameByMobile['LastName'] : '');
                    }
                    $toName = ($fullName) ? $fullName : '';
                }
                // wallet To Bank txn
                if ( $resUserdetails['PaymentsProductID'] == 4 )
                {
                    $getBeneficaryData = $this->getUserBeneficiaryData($resUserdetails['ToWalletId']);
                    $toAcntNumber      = ($getBeneficaryData['AccountNumber']) ? $getBeneficaryData['AccountNumber'] : '';
                    $toName            = ($getBeneficaryData['AccountName']) ? $getBeneficaryData['AccountName'] : '';
                    $toIfsc            = ($getBeneficaryData['BankIFSCCode']) ? $getBeneficaryData['BankIFSCCode'] : '';
                }
                $resData[] = array(
                    'TransactionOrderID'  => $resUserdetails['TransactionOrderID'],
                    'FromAccountName'     => $resUserdetails['FromAccountName'],
                    'ToAccountNumber'     => $toAcntNumber,
                    'ToAccountName'       => $toName,
                    'ToMobileNumber'      => $toMobileNumber,
                    'ToIfsc'              => $toIfsc,
                    'TransactionDate'     => $resUserdetails['TransactionDate'],
                    'Amount'              => $resUserdetails['Amount'],
                    'WalletAmount'        => $resUserdetails['WalletAmount'],
                    'DiscountAmount'      => $resUserdetails['DiscountAmount'],
                    'CashbackAmount'      => $resUserdetails['CashbackAmount'],
                    'PromoCode'           => $resUserdetails['PromoCode'],
                    'TransactionTypeCode' => $resUserdetails['TransactionTypeCode'],
                    'TransactionVendor'   => $resUserdetails['TransactionVendor'],
                    'TransactionStatus'   => $resUserdetails['TransactionStatus'],
                    'TransactionMode'     => ($resUserdetails['CrDrIndicator'] == 'C') ? 'Credit' : 'Debit',
                    'TransactionType'     => $resUserdetails['TransactionType'],
                    'FirstName'           => $resUserdetails['FirstName'],
                    'LastName'            => $resUserdetails['LastName'],
                    'MobileNumber'        => $resUserdetails['MobileNumber'],
                    'ViolaID'             => $resUserdetails['ViolaID'],
                    'EmailID'             => $resUserdetails['EmailID'],
                    'FeeAmount'           => $resUserdetails['FeeAmount'],
                    'TaxAmount'           => $resUserdetails['TaxAmount'],
                    'CardDisplayNumber'   => $resUserdetails['CardDisplayNumber'],
                    'ProductName'         => $resUserdetails['ProductName'],
                    'DisplayName'         => $resUserdetails['DisplayName'],
                    'ProductType'         => $resUserdetails['ProductType'],
                    'Comments'            => $resUserdetails['Comments'],
                    'CrDrIndicator'       => $resUserdetails['CrDrIndicator'],
                    'TransactionCurrency' => $resUserdetails['TransactionCurrency'],
                    'PartyId'             => $resUserdetails['PartyId'],
                    'TransactionDetailID' => $resUserdetails['TransactionDetailID'],
                );
            }
//            if($userDetails['PaymentsProductID'] == '3')
//            {
//                echo 'W2W';
//            }
            //  die();
            $output_arr['res_data'] = $resData;
        }
        else
        {
            $userDetails  = $query->get($selectedColumns)->toArray();
            $countResults = count($userDetails);
        }
//echo $countResults;exit;
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
        if ( $countResults == 0 )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        }
        elseif ( $countResults == 1 )
        {

            $output_arr['res_data'] = array( 'TransactionDetails' => $userDetails[0] );

            return response()->json($output_arr);
        }
        elseif ( $transactionOrderId !== NULL )
        {
            $userDetail                                   = $userDetails[0];
            $rating                                       = $this->_getFeedBack($userDetail['PartyId'], $userDetail['TransactionOrderID']);
            $userDetail['userRating']                     = $rating;
            $remindData                                   = \App\Libraries\TransctionHelper::reminderByTransactionId($userDetail['PartyId'], $userDetail['TransactionDetailID']);
            $output_arr['res_data']['TransactionDetails'] = array_merge($userDetail, $remindData);
        }
        if ( $paginate == NULL && $paginate != 'Y' )
        {

            $resData        = array();
            $toMobileNumber = '';
            $toName         = '';
            $toAcntNumber   = '';
            $toIfsc         = '';
            $fullName       = '';

            foreach ( $userDetails as $resUserdetails )
            {
                // wallet to wallet txn
                if ( $resUserdetails['PaymentsProductID'] == 3 || $resUserdetails['PaymentsProductID'] == 2 )
                {

                    $getBeneficaryData = $this->getUserBeneficiaryData($resUserdetails['ToWalletId']);

                    $toMobileNumber  = ($getBeneficaryData['BeneficiaryMobileNumber']) ? $getBeneficaryData['BeneficiaryMobileNumber'] : '';
                    $getNameByMobile = $this->getNameByMobile($toMobileNumber);
                    if ( $getNameByMobile['FirstName'] )
                    {
                        $fullName = $getNameByMobile['FirstName'] . ' ' . ($getNameByMobile['MiddleName'] ? $getNameByMobile['MiddleName'] : '') . ' ' . ($getNameByMobile['LastName'] ? $getNameByMobile['LastName'] : '');
                    }
                    $toName = ($fullName) ? $fullName : '';
                }
                // wallet To Bank txn
                if ( $resUserdetails['PaymentsProductID'] == 4 )
                {
                    $getBeneficaryData = $this->getUserBeneficiaryData($resUserdetails['ToWalletId']);
                    $toAcntNumber      = ($getBeneficaryData['AccountNumber']) ? $getBeneficaryData['AccountNumber'] : '';
                    $toName            = ($getBeneficaryData['AccountName']) ? $getBeneficaryData['AccountName'] : '';
                    $toIfsc            = ($getBeneficaryData['BankIFSCCode']) ? $getBeneficaryData['BankIFSCCode'] : '';
                }

                $resData[] = array(
                    'TransactionOrderID'  => $resUserdetails['TransactionOrderID'],
                    'FromAccountName'     => $resUserdetails['FromAccountName'],
                    'ToAccountNumber'     => $toAcntNumber,
                    'ToAccountName'       => $toName,
                    'ToMobileNumber'      => $toMobileNumber,
                    'ToIfsc'              => $toIfsc,
                    'TransactionDate'     => $resUserdetails['TransactionDate'],
                    'Amount'              => $resUserdetails['Amount'],
                    'WalletAmount'        => $resUserdetails['WalletAmount'],
                    'DiscountAmount'      => $resUserdetails['DiscountAmount'],
                    'CashbackAmount'      => $resUserdetails['CashbackAmount'],
                    'PromoCode'           => $resUserdetails['PromoCode'],
                    'TransactionTypeCode' => $resUserdetails['TransactionTypeCode'],
                    'TransactionVendor'   => $resUserdetails['TransactionVendor'],
                    'TransactionStatus'   => $resUserdetails['TransactionStatus'],
                    'TransactionMode'     => ($resUserdetails['CrDrIndicator'] == 'C') ? 'Credit' : 'Debit',
                    'TransactionType'     => $resUserdetails['TransactionType'],
                    'FirstName'           => $resUserdetails['FirstName'],
                    'LastName'            => $resUserdetails['LastName'],
                    'MobileNumber'        => $resUserdetails['MobileNumber'],
                    'ViolaID'             => $resUserdetails['ViolaID'],
                    'EmailID'             => $resUserdetails['EmailID'],
                    'FeeAmount'           => $resUserdetails['FeeAmount'],
                    'TaxAmount'           => $resUserdetails['TaxAmount'],
                    'CardDisplayNumber'   => $resUserdetails['CardDisplayNumber'],
                    'ProductName'         => $resUserdetails['ProductName'],
                    'DisplayName'         => $resUserdetails['DisplayName'],
                    'ProductType'         => $resUserdetails['ProductType'],
                    'Comments'            => $resUserdetails['Comments'],
                    'CrDrIndicator'       => $resUserdetails['CrDrIndicator'],
                    'TransactionCurrency' => $resUserdetails['TransactionCurrency'],
                    'PartyId'             => $resUserdetails['PartyId'],
                    'TransactionDetailID' => $resUserdetails['TransactionDetailID'],
                );
            }
//            if($userDetails['PaymentsProductID'] == '3')
//            {
//                echo 'W2W';
//            }
            //  die();
            $output_arr['res_data'] = $resData;
        }
        return response()->json($output_arr);
    }

    public function getUserBeneficiaryData($benfId)
    {
        $selBenf = \App\Models\Userbeneficiary::select('BankIFSCCode', 'AccountName', 'AccountNumber', 'BeneficiaryMobileNumber')
                        ->where('BeneficiaryID', $benfId)->first();

        return $selBenf;
    }

    public function getNameByMobile($mobileNum)
    {
        $selNameMob = \App\Models\Userwallet::select('FirstName', 'MiddleName', 'LastName')
                        ->where('MobileNumber', $mobileNum)->first();

        return $selNameMob;
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateMethodTTransaction($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
                //'device_type' => 'required|in:W,I,A,M',
        ];

        if ( $request->input('searchField') !== NULL )
        {
            $validate['searchField'] = 'required|min:3';
        }
        if ( $request->input('transactionId') !== NULL )
        {
            $validate['transactionId'] = 'required|numeric';
        }
        if ( $request->input('transactionType') !== NULL )
        {
            $validate['transactionType'] = 'required|in:TOPUP,TRIPLECLICK,W2W,W2B,QR';
        }
        if ( $request->input('transactionMode') !== NULL )
        {
            $validate['transactionMode'] = 'required|in:Credit,Debit';
        }

        if ( $request->input('transactionTypeCode') !== NULL )
        {
            $validate['transactionTypeCode'] = 'required|alpha';
        }

        if ( $request->input('transactionOrderId') !== NULL )
        {
            $validate['transactionOrderId'] = 'required|numeric';
        }

        if ( $request->input('mobileNumber') !== NULL )
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
        }

        if ( $request->input('fullName') !== NULL )
        {
            $validate['fullName'] = 'required|min:3|max:70|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/';
        }

        if ( $request->input('violaId') !== NULL )
        {
            $validate['violaId'] = 'required|alpha_num';
        }

        if ( $request->input('email') !== NULL )
        {
            $validate['email'] = 'required|email|max:100';
        }

        if ( $request->input('transactionStatus') !== NULL )
        {
            $validate['transactionStatus'] = 'required|alpha';
        }
        if ( $request->input('fromAmount') !== NULL && $request->input('toAmount') !== NULL )
        {
            $validate['fromAmount'] = 'required|numeric|between:1,' . $this->kycLimit;
            $validate['toAmount']   = 'required|numeric|between:' . $request->input('fromAmount') . ',' . $this->kycLimit;

            $messages['from_amount.between'] = trans('messages.fromAmount.between');
            $messages['to_amount.between']   = trans('messages.toAmount.between');
        }

        if ( $request->input('fromDate') !== NULL && $request->input('toDate') !== NULL )
        {
            $validate['fromDate'] = 'required|date_format:"d-m-Y"|before:toDate';
            $validate['toDate']   = 'required|date_format:"d-m-Y"|after:fromDate';
        }
        elseif ( $request->input('fromDate') !== NULL )
        {
            $validate['fromDate'] = 'required|date_format:"d-m-Y"|before:' . date('d-m-Y');
        }
        elseif ( $request->input('toDate') !== NULL )
        {
            $validate['toDate'] = 'required|date_format:"d-m-Y"|before:' . date('d-m-Y');
        }



        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.         



        /* $messages = [
          'user_id.required' => trans($langfolder . '/validation.userId'),
         */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function getTransactionWalletData(\Illuminate\Http\Request $request)
    {

        //$currency = \App\Models\Transaction::find(1)->currency()->get(); 
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateTransactionsWalletData($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId            = $request->input('partyId');
        $transactionId      = $request->input('walletAccountId');
        $transactionOrderID = $request->input('transactionOrderId');

        $userWalletInfo = \App\Libraries\TransctionHelper::getWalletInfo($partyId);

        $getTransactionList = $this->transactionList($request);

        if ( empty($getTransactionList->original['res_data']) )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            $retunInfo['TransactionDetails']   = NULL;
            $output_arr['res_data']            = $retunInfo;
            return response()->json($output_arr);
        }
        $transactionDetails                   = $getTransactionList->original['res_data']['TransactionDetails'];
        $output_arr['is_error']               = FALSE;
        $output_arr['display_msg']['info']    = trans('messages.requestSuccessfull');
        $userWalletInfo['TransactionDetails'] = $transactionDetails;
        $output_arr['res_data']               = $userWalletInfo;
        return response()->json($output_arr);
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateTransactionsWalletData($request)
    {
        $messages = [];
        $validate = [
            'partyId'            => 'required|numeric',
            'walletAccountId'    => 'required|numeric',
            'transactionOrderId' => 'required|numeric',
        ];

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Get transaction feedback
     * @param type $partyId
     * @param type $transactionOrderId
     */
    private function _getFeedBack($partyId, $transactionOrderId)
    {
        $rating   = \App\Models\Feedback::select('Ratings')->where(array( 'PartyID' => $partyId, 'TransactionID' => $transactionOrderId ))->orderby('Id', 'DESC')->first();
        $userRate = 0;
        if ( count($rating) > 0 )
        {
            $userRate = $rating->Ratings;
        }
        return $userRate;
    }

}
