<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Balance Details after Crediting Funds template
 *  param $fullname varchar
 *  param $amount varchar 
 *  param $time varchar 
 *  param $date varchar 
 *  param $medium varchar 
 *  param $referenceNumber varchar 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Available balance details on your ViolaWallet',
        'text'    => $salutationName . ',<br/>
            Credit Alert!<br/><br/>
INR ' . $amount . 'credited to your ViolaWallet on ' . $date . ' ' . $time . '. as Cash back. Current Balance '.$amount.'.'
    ],
    'sms'   => [
        'text' => '
    Credit Alert!
    Dear ' . $fullname . ', 
INR ' . $amount . 'credited to your ViolaWallet on ' . $date . ' & ' . $time . ' as Cash back. Current Balance ' . $amount . '

Reference Number: ' . $referenceNumber . '
Medium:' . $medium,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'INR '.$amount . ' credited to your ViolaWallet',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'offers'
    ],
    'web'   => [
        'text' => 'INR ' . $amount . ' credited',
    ],
];

echo json_encode($array);

/* End of file cashbacks_balance_after_crediting_funds.php */
