<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Useroperatorfeeplan
 * 
 * @property int $UseroperatorfeeplanID
 * @property int $PlanID
 * @property string $FeePlanID
 * @property string $Operator
 * @property string $Circle
 * @property string $CategoryName
 * @property float $Mrp
 * @property float $TalkTime
 * @property float $ServiceTax
 * @property float $ProcessingFees
 * @property float $AccessFees
 * @property string $Validity
 * @property string $Remarks
 * @property string $Type
 * @property string $Description
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $userrechargedetails
 *
 * @package App\Models
 */
class Useroperatorfeeplan extends Eloquent
{
	protected $table = 'useroperatorfeeplan';
	protected $primaryKey = 'UseroperatorfeeplanID';
	public $timestamps = false;

	protected $casts = [
		'PlanID' => 'int',
		'Mrp' => 'float',
		'TalkTime' => 'float',
		'ServiceTax' => 'float',
		'ProcessingFees' => 'float',
		'AccessFees' => 'float'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'PlanID',
		'FeePlanID',
		'Operator',
		'Circle',
		'CategoryName',
		'Mrp',
		'TalkTime',
		'ServiceTax',
		'ProcessingFees',
		'AccessFees',
		'Validity',
		'Remarks',
		'Type',
		'Description',
		'CreatedDateTime',
		'CreatedBy'
	];

	public function userrechargedetails()
	{
		return $this->hasMany(\App\Models\Userrechargedetail::class, 'UseroperatorfeeplanID');
	}
}
