<!DOCTYPE html><html lang="en"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Viola Wallet: Payment Initiate</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form method="post" name="redirect" action="<?php echo $ccavenueUrl;?>">
            <input type=hidden name=encRequest value="<?php echo $encryptedData;?>">
            <input type=hidden name=access_code value="<?php echo $accessCodeCcA;?>">
        </form>
        <script language="javascript">document.redirect.submit();</script>
    </body>
</html>