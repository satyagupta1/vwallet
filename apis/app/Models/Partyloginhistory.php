<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyloginhistory
 * 
 * @property int $PartyID
 * @property string $Email
 * @property \Carbon\Carbon $time
 * @property string $ip
 * @property string $browser
 * @property string $success
 * @property string $existing_user
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partyloginhistory extends Eloquent
{
	protected $table = 'partyloginhistory';
	protected $primaryKey = 'PartyLoginHistoryId';
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int'
	];

	protected $dates = [
            'SessionStartTime',
            'SessionEndTime'
	];

	protected $fillable = [
		'PartyID',
                'SessionID',
		'Email',
		'MacAddress',
		'IPAddress',
                'DeviceUniqueID',
                'DeviceType',
                'MobileType',            
		'BrowserType',
                'ExistingUser',
                'DeviceToken',
                'DeviceInfo',
		'LoginSuccess',
                'SessionStartTime',
                'SessionEndTime'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
        
        public function partysessionusagedetail()
	{
		return $this->hasMany(\App\Models\Partysessionusagedetail::class, 'SessionID');
	}
}
