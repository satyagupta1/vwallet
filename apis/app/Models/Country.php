<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property string $CountryCode
 * @property string $CountryName
 * @property string $ISDCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $currencies
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $table = 'country';
	protected $primaryKey = 'CountryCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CountryName',
		'ISDCode',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function currencies()
	{
		return $this->belongsToMany(\App\Models\Currency::class, 'countrycurrency', 'CountryCode', 'CurrencyCode')
					->withPivot('CreatedDateTime', 'CreatedBy', 'UpdatedDateTime', 'UpdatedBy');
	}
}
