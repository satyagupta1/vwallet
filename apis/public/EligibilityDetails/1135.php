<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a Bus Ticket Booking and if it is To and From </li>
        <li>System should check if the transaction date is less than 30th April, 2018</li>
        <li>System should calculate the discount amount as 15% of Base Fare Amount and the same shouldn't exceed INR 300</li>
        <li>System should deduct the Discount Amount identified in step 3 from the total Bus Base Fare </li>
        
    </ul> 
</div>
<?php include 'footer.php';?>
