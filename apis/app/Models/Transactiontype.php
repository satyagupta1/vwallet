<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactiontype
 * 
 * @property string $TransactionTypeCode
 * @property string $TransactionDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 *
 * @package App\Models
 */
class Transactiontype extends Eloquent
{
	protected $table = 'transactiontype';
	protected $primaryKey = 'TransactionTypeCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class, 'TransactionTypeCode');
	}
}
