<?php

namespace App\Http\Controllers\RedBus;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use PDF;

class PdfRedbusTicketController extends \App\Http\Controllers\Controller {

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function busTicketPdfDownload(Request $request, $str)
    {
        $output        = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $baseString    = base64_decode(urldecode($str));
        $explodeParams = explode('&', $baseString);
        $params        = array();

        foreach ( $explodeParams as $value )
        {
            $resParams             = explode('=', $value);
            $params[$resParams[0]] = $resParams[1];
        }
        $request->merge($params);

        $valid = $this->_validateResendData($request);
        if ( $valid !== TRUE )
        {
            $output['display_msg'] = $valid;
            return response()->json($output);
        }

        $partyId = $request->input('partyId');
        $txnId   = $request->input('transactiondetailId');
        $pnr     = $request->input('PnrNum');

        $selectArray = array(
            'bookingmaster.Id',
            'bookingmaster.RedbusBookingId',
            'bookingmaster.Source',
            'bookingmaster.Destination',
            'bookingmaster.JourneyDate',
            'bookingmaster.NoOfBookedSeats',
            'bookingmaster.NoOfCancelledSeats',
            'bookingmaster.IsCancellable',
            'bookingmaster.IsPartialCancellable',
            'bookingmaster.CancelStatus',
            'bookingmaster.CancellationPolicy',
            'bookingmaster.BusType',
            'bookingmaster.ServiceId',
            'bookingmaster.TotalFare',
            'bookingmaster.BoardingPoint',
            'bookingmaster.BoardingPointId',
            'bookingmaster.BoardingAddress',
            'bookingmaster.DroppingPointId',
            'bookingmaster.DroppngPoint',
            'bookingmaster.DroppingAddress',
            'bookingmaster.TicketNum',
            'bookingmaster.BoardingTime',
            'bookingmaster.DroppingTime',
            'bookingmaster.BoardingLandmark',
            'bookingmaster.DroppingLandmark',
            'bookingmaster.OperatorName',
            'bookingmaster.OperatorContactNo',
            'bookingmaster.CancelStatus',
            'bookingmaster.BookingStatus',
            'bookingmaster.CancellationPolicy',
            'bookingfaredetails.BaseFare',
            'bookingfaredetails.ServiceCharges',
            'bookingfaredetails.Tax',
            'bookingfaredetails.TollGate',
            'bookingfaredetails.ReservChrgs',
            'bookingfaredetails.OnlineFee',
            'transactiondetail.TransactionDetailID',
            'transactiondetail.TransactionOrderID',
            'transactiondetail.TransactionDate',
            'transactiondetail.DiscountAmount'
        );

        $queryBooking = \App\Models\Transactiondetail::
                        join('bookingmaster', 'bookingmaster.TransactionId', 'transactiondetail.TransactionDetailID')
                        ->join('bookingfaredetails', 'bookingfaredetails.BookingMasterId', 'bookingmaster.Id')
                        ->select($selectArray)
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->where('transactiondetail.TransactionDetailID', $txnId)
                        ->where('bookingmaster.TicketNum', $pnr)
                        ->first()->toArray();

        $source      = $this->getcityName($queryBooking['Source']);
        $destination = $this->getcityName($queryBooking['Destination']);

        $bookingMasterId = (empty($queryBooking)) ? 0 : $queryBooking['Id'];
        $bordingTime     = $this->convertTime($queryBooking['BoardingTime']);
        $dropTime        = $this->convertTime($queryBooking['DroppingTime']);

        $passengersDetails = $this->getPassengerDetails($bookingMasterId);
        $paxInfo           = array();
        foreach ( $passengersDetails as $mainPassengers )
        {
            $paxInfo[] = array(
                'paxName'    => $mainPassengers['PassengerName'],
                'seatNumber' => $mainPassengers['SeatNum'],
                'age'        => $mainPassengers['Age'],
                'gender'     => $mainPassengers['Gender']
            );
        }

        $canPolicy = $this->cancellationPolicy($queryBooking['CancellationPolicy'], $queryBooking['TotalFare']);

        $queryBooking['passengerInfo']   = $paxInfo;
        $queryBooking['SourceName']      = $source;
        $queryBooking['DestinationName'] = $destination;
        $queryBooking['boardingTime']    = $bordingTime;
        $queryBooking['dropingTime']     = $dropTime;
        $queryBooking['cancelPolicy']    = $canPolicy;
//        $queryBooking['JourneyDate']    = date('l, F, j, Y', strtotime($queryBooking['JourneyDate']));


//        print_r($queryBooking);
//        exit;

//        view()->share('data', $queryBooking);
        // Set extra option
        PDF::setOptions([ 'dpi' => 100, 'defaultFont' => 'sans-serif' ]);
        PDF::setPaper('A4', 'landscape');
        // pass view file
        $pdf = PDF::loadView('pdf-bus-ticket', $queryBooking);
        // download pdf
        $pdfName = $source.'-'.$destination.'-'.$queryBooking['JourneyDate'].'.pdf';
        return $pdf->download($pdfName);
    }

    /**
     * @desc Query for passenger details for bus bookings method
     * 
     * @param $bookingMasterId type INT
     * return string if data exist other wise will return Boolean
     */
    public function getPassengerDetails($bookingMasterId)
    {

        $passengerDet = \App\Models\Bookingdetail::select(
                                'PassengerId', 'PassengerName', 'Age', 'IdType', 'IdNumber', 'BookingStaus', 'Gender', 'SeatNum', 'MobileNumber', 'Email', 'AlternateMobileNumber', 'BaseFare', 'OperatorServiceCharge', 'ServiceFees', 'Fare')
                        ->where('BookingMasterId', $bookingMasterId)->get();
        return (count($passengerDet) > 0) ? $passengerDet : FALSE;
    }

    public function getcityName($id)
    {
        $city = \App\Models\Buscity::select('Name')->where('RedBusId', $id)->first();
        return ($city) ? $city->Name : FALSE;
    }

    public function convertTime($time)
    {
        $hours   = number_format($time / 60);
        $minutes = $time % 60;

        $journeyDate = number_format($hours / 24);

        if ( $journeyDate > 24 )
        {
            $hours = $hours - 24;
        }

        return date('H:i', strtotime($hours . ':' . $minutes));
    }

    public function cancellationPolicy($policy, $fare)
    {
        $policies    = explode(';', trim($policy, ';'));
        $expandPolcy = array();
        foreach ( $policies as $value )
        {
            $canPal        = explode(':', $value);
            if($canPal[1] < 0){
                $policy = 'Till ' . $canPal[0] . ' Hrs';
            }else{
                $policy = 'Before ' . $canPal[0] . ' Hrs to '.$canPal[1].' Hrs';
            }
            $cp            = array(
                'policy' => $policy,
                'amont'  => $fare - ($fare * ($canPal[2] / 100))
            );
            $expandPolcy[] = $cp;
        }
        return $expandPolcy;
    }

    public function ticketPreview(Request $request)
    {
//        dd(class_exists('DOMDocument'));
        dd(url('public/viola_wallet.png'));
        $partyId = $request->input('partyId');
        $txnId   = $request->input('transactiondetailId');
        $output  = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $valid   = $this->_validateResendData($request, $type    = 'resendTicket');
        if ( $valid !== TRUE )
        {
            $output['display_msg'] = $valid;
            return response()->json($output);
        }
        return view('bus-ticket');
    }

    /**
     * @desc Validate request data for bus bookings
     * @param $partyId type INT
     * @return array
     */
    private function _validateResendData($request)
    {
        $validate['partyId']             = 'required';
        $validate['transactiondetailId'] = 'required|numeric';
        $validate['PnrNum']              = 'required';
        $messages                        = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
