<?php

/**
 * Registration Controller file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
namespace App\Http\Controllers\User;

class RegisterController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->mdmUrl          = config('vwallet.genrateViolaId');
        $this->userTypeKey     = config('vwallet.userTypeKey');
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
        $this->prefix          = config('vwallet.prefix');
        $this->productCode     = config('vwallet.productCode');
    }

    /**
     * User Registration step One.
     *
     * @param  Request  $request
     * @return Response
     */
    public function registrationOne(\Illuminate\Http\Request $request)
    {       
        $data_send  = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $date_added = date('Y-m-d H:i:s');
        $data_v     = $this->_validatRegOne($request);

        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return $data_send;
        }
        $regLocation = $this->_validateLocation($request);
        if ( $regLocation == FALSE )
        {
            $data_send['display_msg']['address'] = trans('messages.noGps');
            return $data_send;
        }
        $regAdd  = unserialize($regLocation);
        $address = \App\Libraries\GeoAddress::getAddressFromGoogleMaps($regAdd['lat'], $regAdd['longt']);
        if ( empty($address) )
        {
            $address = \App\Libraries\GeoAddress::getAddressFromGoogleMaps($regAdd['lat'], $regAdd['longt']);
        }
        $mobileNo = $request->input('mobileNumber');
        $emailId  = $request->input('email');
        $ref_code = $request->input('referralCode');

        // for check referral code
        if ( $ref_code )
        {
            $chk_ref = $this->_verifyReffer($ref_code);

            if ( $chk_ref === FALSE )
            {
                $data_v    = array( 'referralCode' => trans('messages.invalidReferralCode') );
                $data_send = array( 'is_error' => TRUE, 'display_msg' => $data_v, 'status_code' => '200', 'res_data' => array() );
                return response()->json($data_send);
            }
        }

        // check user already registerd or not
        $checkUserStatus = $this->_regUserChk($request);
        if ( $checkUserStatus !== TRUE )
        {
            return $checkUserStatus;
        }
        $defaultOder    = config('vwallet.FASTTRACK_MENU');
        $fastTrackKeys  = array_keys($defaultOder);
        $fastTrackOrder = implode(",", $fastTrackKeys);

        $violaId = 0;

        //DB transactions start from here.
        \DB::beginTransaction();

        try
        {
            $data        = array(
                'PartyType'       => $this->userTypeKey,
                'CreatedDateTime' => $date_added,
                'UpdatedDateTime' => $date_added,
                'CreatedBy'       => $this->userTypeKey,
                'UpdatedBy'       => $this->userTypeKey
            );
            $partyInsert = \App\Models\Party::insertGetId($data);
            $partyId     = $partyInsert;

            $userWalletArray = array(
                'UserWalletID'               => $partyId,
                'MobileNumber'               => $mobileNo,
                'ViolaID'                    => $violaId,
                'EmailID'                    => $emailId,
                'Password'                   => '',
                'ProfileStatus'              => 'N',
                'IsVerificationCompleted'    => 'N',
                'PreferredPrimaryCurrency'   => 'INR',
                'PreferredSecondaryCurrency' => 'INR',
                'CreatedDateTime'            => $date_added,
                'UpdatedDateTime'            => $date_added,
                'WalletPlanTypeId'           => '1'
            );
            \App\Models\Userwallet::insertGetId($userWalletArray);

            $referralCode = $this->_referralCode($request); // Generate user referer code

            $userRegistrationArray = array(
                'UserWalletID'            => $partyId,
                'RegisteredReferralCode'  => $referralCode,
                'MobileVerified'          => 'N',
                'EmailVerified'           => 'N',
                'RegistrationCountryCode' => 'IND',
                'WalletCountryCode'       => 'IND',
                'WalletTimeZone'          => '', // why not filling this colum
                'TermsAccepted'           => 'Y',
                'TermsAcceptedDateTime'   => $date_added,
                'CreatedDateTime'         => $date_added,
                'CreatedBy'               => $this->userTypeKey,
                'UpdatedDateTime'         => $date_added,
                'UpdatedBy'               => $this->userTypeKey );

            \App\Models\Userregistration::insertGetId($userRegistrationArray);
            $userConfigInsert = array(
                array( 'UserWalletID'    => $partyId, 'ConfigType'      => 'balanceView', 'Details'         => 'N', 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                    'UpdatedBy'       => $this->userTypeKey ),
                array( 'UserWalletID'    => $partyId, 'ConfigType'      => 'fastTrack', 'Details'         => $fastTrackOrder, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                    'UpdatedBy'       => $this->userTypeKey ),
                array( 'UserWalletID'    => $partyId, 'ConfigType'      => 'vpinTransLimit', 'Details'         => '2000', 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                    'UpdatedBy'       => $this->userTypeKey ),
                array( 'UserWalletID'    => $partyId, 'ConfigType'      => 'regLocation', 'Details'         => $regLocation, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                    'UpdatedBy'       => $this->userTypeKey ),
            );

            \App\Models\Userconfiguration::insert($userConfigInsert);

            // insert of user preferences
            $arrayPreferncesInsert = array(
                'UserWalletID'    => $partyId,
                'ConfigType'      => 'preferences',
                'Details'         => json_encode(array(
                    'notification' => 'Y',
                    'trans_msg'    => 'Y',
                    'trans_email'  => 'Y',
                    'offer_alert'  => 'Y',
                    'd_not_d'      => 'N',
                    'push_alerts'  => 'Y',
                )),
                'CreatedDateTime' => $date_added,
                'CreatedBy'       => $this->userTypeKey,
                'UpdatedDateTime' => $date_added,
                'UpdatedBy'       => $this->userTypeKey
            );
            \App\Models\Userconfiguration::insert($arrayPreferncesInsert);

            // for referral code
            if ( isset($chk_ref) && is_object($chk_ref) )
            {
                $ref_data = array(
                    'PartyID'             => $partyId,
                    'ReferralCode'        => $chk_ref->RegisteredReferralCode,
                    'ReferralMediumCode'  => 'VW',
                    'RefereeName'         => $chk_ref->FirstName,
                    'RefereeMobileNumber' => $chk_ref->MobileNumber,
                    'RefereeEmailId'      => $chk_ref->EmailID,
                    'CreatedDateTime'     => $date_added,
                    'CreatedBy'           => $this->userTypeKey,
                    'UpdatedDateTime'     => $date_added,
                    'UpdatedBy'           => $this->userTypeKey
                );
                \App\Models\Partyreferral::insertGetId($ref_data);
            }
            $partyAddInsert = array( 'PartyID'         => $partyId, 'CreatedBy'       => $this->userTypeKey, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                'UpdatedBy'       => $this->userTypeKey );
            if ( ! empty($address) )
            {
                $partyAddInsert['Country']          = $address['country'];
                $partyAddInsert['State']            = $address['province'];
                $partyAddInsert['City']             = $address['city'];
                $partyAddInsert['AddressLine1']     = $address['street'];
                $partyAddInsert['PostalZipCode']    = $address['postalCode'];
                $partyAddInsert['AddressTypeCode']  = 'GEO';
                $partyAddInsert['AddressPartyType'] = $this->userTypeKey;
                $partyAddInsert['PrimaryAddress']   = 'N';
            }
            \App\Models\Partyaddress::create($partyAddInsert);

            \DB::commit();
            // all good 
        }
        catch ( \Exception $e )
        {
            \DB::rollback();
            // something went wrong
            $data_send['display_msg']['info'] = trans('messages.wrong');
            return response()->json($data_send);
        }

        $params    = array(
            'UserWalletID' => $partyId,
            'mobileNumber' => $mobileNo,
            'typeCode'     => 'REG',
            'emailId'      => $emailId,
            'userName'     => '',
        );
        return $data_json = $this->_sendOtp($params);
    }

    /**
     * User Registration step second.
     *
     * @param  Request $request
     * @return Response
     */
    public function registrationTwo(\Illuminate\Http\Request $request)
    {

        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatRegTwo($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }

        $partyId        = $request->input('partyId');
        $vPin           = \Illuminate\Support\Facades\Hash::make($request->input('vPin'));
        $password       = \Illuminate\Support\Facades\Hash::make($request->input('password'));        
        $regToken   = $request->input('regToken');
        $mobileNumber   = $request->input('mobileNumber');
        $VPAId          = $request->input('vpa');        

        $results = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userwallet.KycStatus', 'userwallet.WalletStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                        ->join('userregistration', 'userregistration.UserWalletId', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                        ->where('userwallet.UserWalletID', '=', $partyId)->where('userwallet.MobileNumber', '=', $mobileNumber)->first();
        if ( ! $results )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return response()->json($data_send);
        }

        if ( $results->MobileVerified == 'N' )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.notAllowed') );
            return response()->json($data_send);
        }
        
        if ( $results->Password != '' )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.ERROR.reguser.R28') );
            return response()->json($data_send);
        }
        //DB transactions start from here.
        \DB::beginTransaction();

        try
        {
            $date_added       = date('Y-m-d H:i:s');
            $userWalletUpdate = array( 'ProfileStatus' => 'A', 'TPin' => $vPin, 'Password' => $password, 'UpdatedDateTime' => $date_added );

            \App\Models\Userwallet::where('UserWalletID', $partyId)->update($userWalletUpdate);

            //  $partyContactInsert = array( 'PartyId' => $partyId, 'ContactTypeId' => 1, 'ContactDetails' => $mobileNumber, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added );
            //    \App\Models\Partycontact::create($partyContactInsert);

            $walletNumber = \App\Libraries\Helpers::_check_wallet_id(); // Generate wallet number
            
            // get topup limits
            $topupLimits = config('vwallet.topupWalletLimits');

            $kycLimits = $topupLimits['02']['walletLimit'];
            if ( isset($topupLimits[$results->KycStatus]['walletLimit']) )
            {
                $kycLimits = $topupLimits[$results->KycStatus]['walletLimit'];
            }

            $walletAccInsert = array(
                'WalletAccountNumber'     => $walletNumber,
                'WalletAccountPrefix'     => $this->prefix,
                'UserWalletId'            => $partyId,
                'PrimaryAccountIndicator' => 'Y',
                'AccountLinked'           => 'N',
                'WalletCurrencyCode'      => 'INR',
                'MaximumBalanceLimit'     => $kycLimits,
                'CreatedDateTime'         => $date_added,
                'CreatedBy'               => $this->userTypeKey,
                'UpdatedDateTime'         => $date_added,
                'UpdatedBy'               => $this->userTypeKey,
                'IsAdminWallet'           => 'N'
            );
            $walletAccountId = \App\Models\Walletaccount::insertGetId($walletAccInsert);

            if ( $VPAId )
            {
                $upiBeneficiaryDetails = array(
                    'UserWalletID'            => $partyId,
                    'BeneficiaryWalletID'     => $walletAccountId,
                    'BeneficiaryType'         => 'UPI',
                    'BeneficiaryMobileNumber' => $mobileNumber,
                    'isTripleClickUser'       => 'N',
                    'BeneficiaryStatus'       => 'Y',
                    'SelfBenficiary'          => 'Y',
                    'CreatedDateTime'         => $date_added,
                    'CreatedBy'               => $this->userTypeKey,
                    'UpdatedDateTime'         => $date_added,
                    'UpdatedBy'               => $this->userTypeKey,
                    'BeneficiaryVPA'          => $VPAId
                );
                \App\Models\Userbeneficiary::insert($upiBeneficiaryDetails);
            }

            $partyPassHisVpinInsert = array( 'PartyID' => $partyId, 'SecretKey' => $vPin, 'SecretKeyType' => 'PIN', 'EffectiveDate' => $date_added, 'Active' => 'Y', 'CreatedDateTime' => $date_added, 'CreatedBy' => $partyId, 'UpdatedDateTime' => $date_added, 'UpdatedBy' => $partyId );
            \App\Models\Partypasswordhistory::create($partyPassHisVpinInsert);

            $partyPassHisPasswordInsert = array( 'PartyID' => $partyId, 'SecretKey' => $password, 'SecretKeyType' => 'PASSWORD', 'EffectiveDate' => $date_added, 'Active' => 'Y', 'CreatedDateTime' => $date_added, 'CreatedBy' => $partyId, 'UpdatedDateTime' => $date_added, 'UpdatedBy' => $partyId );
            \App\Models\Partypasswordhistory::create($partyPassHisPasswordInsert);
            \DB::commit();
            // all good
        }
        catch ( \Exception $e )
        {
            \DB::rollback();
            // something went wrong
            $data_send['display_msg']['info'] = trans('messages.wrong');
            return response()->json($data_send);
        }

        $loginHistory = \App\Libraries\Helpers::_addLoginHistory($request, $partyId, 'Y');
        $sessionData  = \App\Libraries\Vwallet::sessionDetails($request, $results->UserWalletID, $loginHistory);

        $data_send['is_error']    = FALSE;
        $data_send['display_msg'] = array( 'info' => trans('messages.registrationDone') );

        
        $userpreferance        = \App\Libraries\Helpers::userConfigData($results->UserWalletID);
        
        $userData                     = \App\Libraries\Helpers::userdata($results);
        $returnData                   = array_merge($userData, $sessionData, $userpreferance);
        $returnData['BeneficiaryVPA'] = $VPAId;
        $data_send['res_data']        = $returnData;

        // Start Communication
        // for referral code success  
        $senderName = ($results->FirstName) ? $results->FirstName : '';

        // For: Welcome Message
        $userTemplateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW004',
            'senderEmail'    => $results->EmailID,
            'senderMobile'   => $results->MobileNumber,
            'reciverId'      => 0, // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array( // template utalisation parameters 
                'fullname'     => $senderName,
                'email'        => $results->EmailID,
                'mobileNumber' => $results->MobileNumber,
            )
        );
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $userTemplateData);

        // End Communication
        // Check referral info
        /*
          $regPartyId   = $request->input('partyId');
          $refresults      = \App\Models\Partyreferral::select('PartyID', 'ReferralCode', 'ReferralMediumCode', 'RefereeName', 'RefereeMobileNumber', 'RefereeEmailId')->where('PartyID', $regPartyId)->first();
          if($refresults && $refresults->ReferralCode!=NULL) {
          $request->request->add([ 'usedReferralCode' => $refresults->ReferralCode ]);
          $referralCode = $request->input('usedReferralCode');
          $referralCashBackStats = \App\Libraries\OfferRules::referralCashBack($request);
          $referralInfo = json_decode($referralCashBackStats);
          if ( $referralInfo->is_error == FALSE )
          {
          $data_send['display_msg']['cashBackMsg'] = $referralInfo->display_msg->info;
          }
          }
         */
        $data_send['res_data']['regToken'] = $regToken;
        return response()->json($data_send);
    }

    /**
     * Aadhaar Registration
     * Initiates the Aadhaar linking with user mobile number to complete the registration process
     * @param type $request
     */
    public function regAadhaar(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatRegAadhaar($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $mobileNumber      = $request->input('mobileNumber');
        $aadhaarNumber     = $request->input('aadhaarNumber');
        $registrationToken = $request->input('registrationToken');

        $userData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ProfileStatus', 'userwallet.MobileNumber', 'userregistration.MobileVerified')
                        ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->where('MobileNumber', '=', $mobileNumber)->first();
        if ( $userData->MobileVerified == 'N' )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.notAllowed') );
            return response()->json($data_send);
        }

        $actionName   = 'REGAADHAAR';
        $extraParams = array('partyId' => $userData->UserWalletID);
        $params       = array( 'p1' => $mobileNumber, 'p2' => $aadhaarNumber, 'p3' => $registrationToken, 'p4' => 'Y', 'p5' => 'Authentication' );
        $response     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params, $extraParams);
        $responseData = json_decode($response, TRUE);
        $responseData['partyId'] = $userData->UserWalletID;
        //print_r($responseAObj);exit;
        if ( $responseData['status_code'] == 'SUCCESS' )
        {
            $data_send['is_error'] = FALSE;            
        }
        $data_send['display_msg']['info'] = $responseData['message'];
        $data_send['res_data']            = $responseData;
        return response()->json($data_send);
    }
    
    
    /**
     * Aadhaar Registration verification
     * Initiates the Aadhaar linking with user mobile number to complete the registration process
     * @param type $request
     */
    public function regAadhaarVerify(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatRegAadhaarVerify($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $mobileNumber      = $request->input('mobileNumber');
        $aadhaarNumber     = $request->input('aadhaarNumber');
        $transactionId = $request->input('transactionId');
        $otpId = $request->input('otpNumber');
        $otpTransactionId = $request->input('otpTransactionId');
        
        $userData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userwallet.KycStatus', 'userwallet.WalletStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                        ->join('userregistration', 'userregistration.UserWalletId', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                        ->where('userwallet.MobileNumber', '=', $mobileNumber)->first();
        if ( $userData->MobileVerified == 'N' )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.notAllowed') );
            return response()->json($data_send);
        }
        
        $actionName        = 'VERIFYAOTP';
        $extraParams = array('partyId' => $userData->UserWalletID);
        $params            = array( 'p1' => $mobileNumber, 'p2' => $aadhaarNumber, 'p3' => $transactionId, 'p4' => $otpId, 'p5' => $otpTransactionId, 'p6' => 'Y', 'p7' => 'Authentication' );
        $response          = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params, $extraParams);
        
        $responseData = json_decode($response, TRUE);
        $responseData['partyId'] = $userData->UserWalletID;
        $data_send['display_msg']['info'] = $responseData['message'];
        $data_send['res_data']            = $responseData;        
        //print_r($responseAObj);exit;
        if ( $responseData['status_code'] == 'SUCCESS' )
        {
            //DB transactions start from here.
            \DB::beginTransaction();

            try
            {
                $gender = self::_convertGender($responseData['gender']);
                
                $data_send['is_error'] = FALSE;
                $date_added            = date('Y-m-d H:i:s');
                $dob                   = explode('T', $responseData['DOB']);
                $result_name           = \App\Libraries\Helpers::splitFullName($responseData['name']);
                $encKey                = sha1(config('vwallet.ENCRYPT_STATIC'));
                $encryptAadhaar        = \App\Libraries\Crypt\Encryptor::encrypt(trim($aadhaarNumber), $encKey);
                $updateData            = array(
                    'FirstName'   => $result_name['FirstName'],
                    'LastName'    => ($result_name['LastName']) ? $result_name['LastName'] : '',
                    'MiddleName'  => $result_name['MiddleName'] ? $result_name['MiddleName'] : '',
                    'Gender'      => $gender,
                    'DateofBirth' => $dob[0],
                    'WalletPlanTypeId' => '1',
                    'IsVerificationCompleted' => 'Y',                    
                    'KycStatus' => '03', //Aadhaar KYC
                    'WalletStatus' => $responseData['wallet_status'],
                    'AadhaarNumber' => $encryptAadhaar,
                );
                \App\Models\Userwallet::where('UserWalletID', $userData->UserWalletID)->update($updateData);

                $partyAddInsert = array( 'PartyID'         => $userData->UserWalletID, 'CreatedBy'       => $this->userTypeKey, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy'       => $this->userTypeKey,
                    'UpdatedBy'       => $this->userTypeKey );
                $address        = array_reverse(explode(',', $responseData['address']));
                if ( ! empty($address) )
                {
                    $partyAddInsert['Country']          = 'India';
                    $partyAddInsert['PostalZipCode']    = $address['0'];
                    $partyAddInsert['State']            = $address['1'];
                    $partyAddInsert['City']             = $address['2'];
                    $partyAddInsert['AddressLine2']     = $address['6'].' - '.$address['5'].' - '.$address['4']; 
                    $partyAddInsert['AddressLine1']     = $address['8'].' - '.$address['7'];                     
                    $partyAddInsert['AddressTypeCode']  = 'HOM';
                    $partyAddInsert['AddressPartyType'] = $this->userTypeKey;
                    $partyAddInsert['PrimaryAddress']   = 'Y';
                }
                \App\Models\Partyaddress::create($partyAddInsert);
                
                // get topup limits
                $topupLimits = config('vwallet.topupWalletLimits');

                $kycLimits = $topupLimits['03']['walletLimit'];
                if ( isset($topupLimits[$userData->KycStatus]['walletLimit']) )
                {
                    $kycLimits = $topupLimits[$userData->KycStatus]['walletLimit'];
                }
                $walletAccUpdate = array(               
                    'MaximumBalanceLimit'     => $kycLimits,                
                    'UpdatedDateTime'         => $date_added,
                    'UpdatedBy'               => $this->userTypeKey                
                );
                \App\Models\Walletaccount::where('UserWalletID', $userData->UserWalletID)->update($walletAccUpdate);
            
                \DB::commit();
                // all good
            }
            catch ( \Exception $e )
            {
                \DB::rollback();
                // something went wrong
                $data_send['is_error']            = TRUE;
                $data_send['display_msg']['info'] = trans('messages.wrong');
                return response()->json($data_send);
            } 
            
            // generate violaId
            $vparams = array( 'product_code' => $this->productCode, 'party_id' => $userData->UserWalletID, 'email' => $userData->EmailID, 'firstname' => $result_name['FirstName'], 'lastname' => $result_name['LastName'], 'mobile' => $mobileNumber );
                      
            $logRequest    = array(
                'partyId'        => $userData->UserWalletID,
                'requestId'      => $userData->UserWalletID,
                'thirdPartyName' => 'mdmViolaId',
                'thirdPartyCall' => 'ViolaId',
                'request'        => $vparams,
                'status'         => 'Initiated',
            );
            $logId         = \App\Libraries\LogHelper::createLog($logRequest);

            $violaIdReturn = \App\Libraries\Helpers::curlSend($this->mdmUrl, $vparams, $logId);
            $violaIdInfo   = json_decode($violaIdReturn);
            if ( isset($violaIdInfo->res_data->viola_id1) )
            {
                $updateViolaId = array( 'ViolaID' => $violaIdInfo->res_data->viola_id );
            }
            else
            {
                $nameStr       = strtoupper(substr($result_name['FirstName'], 0, 4));
                $updateViolaId = array( 'ViolaID' => $nameStr . $userData->UserWalletID );
            }
            \App\Models\Userwallet::where('UserWalletID', $userData->UserWalletID)->update($updateViolaId);

            //Get updated information
            $userData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userwallet.KycStatus', 'userwallet.WalletStatus', 'userwallet.IsVerificationCompleted', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                        ->join('userregistration', 'userregistration.UserWalletId', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                        ->where('userwallet.MobileNumber', '=', $mobileNumber)->first();
            $loginHistory          = \App\Libraries\Helpers::_addLoginHistory($request, $userData->UserWalletID, 'Y', FALSE);
            $sessionData           = \App\Libraries\Vwallet::sessionDetails($request, $userData->UserWalletID, $loginHistory);
            $userConfigData        = \App\Libraries\Helpers::userConfigData($userData->UserWalletID);
            $userDataFilter        = \App\Libraries\Helpers::userdata($userData);
            $returnData            = array_merge($userDataFilter, $sessionData, $userConfigData);
            $data_send['res_data'] = $returnData;
        }
        return response()->json($data_send);
    }
    
    /**
     * Virtual Payment Registration
     * Register virtual payment address for financial transactions
     * @param type $request
     */
    public function regVpa(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatRegVpaId($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $partyId      = $request->input('partyId');
        $vpaId     = $request->input('vpaId');

        $userData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ProfileStatus', 'userwallet.MobileNumber', 'userregistration.MobileVerified', 'walletaccount.WalletAccountId')
                        ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                        ->where('userwallet.UserWalletID', '=', $partyId)->first();
       
        if ( $userData->MobileVerified == 'N' || $userData->ProfileStatus == 'N')
        {
            $data_send['display_msg'] = array( 'info' => 'Verificaion required for registration' );
            return response()->json($data_send);
        }
        $date_added            = date('Y-m-d H:i:s');
        $upiBeneficiaryDetails = array(
            'UserWalletID'            => $partyId,
            'BeneficiaryWalletID'     => $userData->WalletAccountId,
            'BeneficiaryType'         => 'UPI',
            'BeneficiaryMobileNumber' => $userData->MobileNumber,
            'isTripleClickUser'       => 'N',
            'BeneficiaryStatus'       => 'Y',
            'SelfBenficiary'          => 'Y',
            'CreatedDateTime'         => $date_added,
            'CreatedBy'               => $this->userTypeKey,
            'UpdatedDateTime'         => $date_added,
            'UpdatedBy'               => $this->userTypeKey,
            'BeneficiaryVPA'          => $vpaId
        );
        $insertId = \App\Models\Userbeneficiary::insertGetId($upiBeneficiaryDetails);
        $data_send['display_msg']['info'] = trans('messages.wrong');
        if($insertId) {
            $data_send['is_error'] = FALSE; 
            $data_send['display_msg']['info'] = trans('messages.addedSuccessfully', ['name' => 'Vpa']);
        }
        return response()->json($data_send);
    }
    
    /**
     * Aadhaar Registration
     * Submit KYC through Document Submission, The KYC team approve/reject.
     * @param type $request
     */
    public function kycDocumentUpload(\Illuminate\Http\Request $request)
    {        
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validateKycUpload($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $partyId      = $request->input('partyId');
        $documentId     = $request->input('documentId');
        $addressDocument     = $request->input('addressDocument');
        $imageTypeId     = $request->input('idContentType');
        $imageIdData     = $request->input('idContentData');
        $imageTypeAddr     = $request->input('addressContentType');
        $imageAddrData     = $request->input('addressContentData');
        
        
        $userData = \App\Models\Userwallet::select('userwallet.ProfileStatus', 'userwallet.MobileNumber', 'userregistration.MobileVerified')
                        ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->where('userwallet.UserWalletID', '=', $partyId)->first();
       
        if ( $userData->MobileVerified == 'N' || $userData->ProfileStatus == 'N')
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.notAllowed') );
            return response()->json($data_send);
        }
        
        $mobile    = $userData->MobileNumber;
        $actionName    = 'SUBMITKYC';
        $imageName = mt_rand(100000, 999999);
        $imageType1 = explode('/', $imageTypeId);
        $imageType2 = explode('/', $imageTypeAddr);
        $fileName1 = $imageName . '.' . $imageType1[1];
        $fileName2 = $imageName . '.' . $imageType2[1];
        
        $id_proof_image_1 = json_encode(array('data' => $imageIdData, 'content_type' => $imageTypeId, 'filename' => $fileName1));
       
        $addr_proof_image_1 = json_encode(array('data' => $imageAddrData, 'content_type' => $imageTypeAddr, 'filename' => $fileName2));
        
        $requestParams = array( 'p1' => $mobile, 'p2' => 'DOCS', 'p3' => '', 'p4' => $documentId, 'p5' => $addressDocument );
        $extraParams = array('partyId' => $partyId, 'requestId' => '0', 'referenceId' => '0', 'status' => 'Initiated');
        $extraParams['id_proof_image_1'] = $id_proof_image_1;
        $extraParams['addr_proof_image_1'] = $addr_proof_image_1;
        $response      = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams, $extraParams);
        
        $responseData  = json_decode($response, TRUE);
        if ( $responseData['status_code'] != 'SUCCESS' )
        {
           return $output = \App\Libraries\Wallet\Yesbank::handleResponse($responseData);
        }
        
        $serviceType = 'KYCDOCS';
        
        $documentType = $serviceType . '-' . 'Id';
        $responseId = \App\Libraries\UploadDoc::uploadDoc($partyId, $imageTypeId, $imageIdData, $documentType);
        
        $documentType = $serviceType . '-' . 'Addr';
        $responseAddr = \App\Libraries\UploadDoc::uploadDoc($partyId, $imageTypeAddr, $imageAddrData, $documentType);
        
        $data_send['is_error'] = FALSE;
        $data_send['display_msg']['info'] = trans('messages.SUCCESS.submitkyc.M07');
        $data_send['res_data'] = array($responseId, $responseAddr);       
        return response()->json($data_send);
    }
    
    
    /**
     * User Registration kyc upload validations 
     *
     * @param  Request  $request
     * @return Response
     */
    private function _validateKycUpload($request)
    {
        $validate = array(
            'partyId'         => 'required|numeric',
            'documentId'      => 'required|in:PAN,AADHAAR,PASSPORT,DR_LICENSE,VOTER_ID,NREGA',
            'addressDocument' => 'required|in:AADHAAR,PASSPORT,DR_LICENSE,VOTER_ID,NREGA,UTILITY_BILL,BANK_ACCOUNT_STATEMENT',
            'idContentType' => 'required',
            'idContentData' => 'required',            
            'addressContentData' => 'required',
            'addressContentType' => 'required',            
        );

        $validate_response = \Validator::make($request->all(), $validate, array());
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }
    /**
     * User Registration First Validation Check.
     *
     * @param  Request  $request
     * @return Response
     */
    private function _validatRegOne($request)
    {
        $validate = array(
            'mobileNumber' => 'numeric|required|digits:10',
            'email'        => 'required|email|max:100',
            'referralCode' => 'alpha_num|min:6|max:20',
        );

        $validate_response = \Validator::make($request->all(), $validate, array());
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    /**
     * validating location
     * @param  Request  $request
     * @return Response
     */
    private function _validateLocation($request)
    {
        $headers = $request->header();
        if ( ! isset($headers['device-info']['0']) )
        {
            return FALSE;
        }
        $deviceInfoObj = json_decode(stripslashes($headers['device-info']['0']), TRUE);
        $lat           = $deviceInfoObj['lat'];
        $longt         = $deviceInfoObj['longt'];
        if ( empty(round($lat)) )
        {
            return FALSE;
        }
        return serialize(array( 'lat' => $lat, 'longt' => $longt ));
    }

    /**
     * User Registration Second Validation Check.
     *
     * @param  Request  $request
     * @return Response
     */
    private function _validatRegTwo($request)
    {
        $validate = array(
            'mobileNumber' => 'numeric|required|digits:10',
            'partyId'      => 'required|numeric',
            'vPin'         => 'numeric|required|digits:4',
            'password'     => 'required|password|min:8|max:16',
        );
        $messages = array( 'password.password' => trans('validation.accepted') );

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    /**
     * Validate Aadhaar Registration
     * @param type $request
     * @return type
     */
    private function _validatRegAadhaar($request)
    {
        $validate          = array(
            'mobileNumber'      => 'numeric|required|digits:10',
            'partyId'           => 'required|numeric',
            'aadhaarNumber'     => 'numeric|required|digits:12',
            'registrationToken' => 'required',
        );
        $messages          = array();
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }
    /**
     * Validate VPAID Registration
     * @param type $request
     * @return type
     */
    private function _validatRegVpaId($request)
    {
        $validate          = array(            
            'partyId'           => 'required|numeric',
            'vpaId'     => 'required',            
        );
        $messages          = array();
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }
    /**
     * Validate Aadhaar Registration Verify
     * @param type $request
     * @return type
     */
    private function _validatRegAadhaarVerify($request)
    {
        $validate          = array(
            'mobileNumber'      => 'numeric|required|digits:10',
            'partyId'           => 'required|numeric',
            'aadhaarNumber'     => 'numeric|required|digits:12',
            'transactionId'     => 'numeric|required',
            'otpNumber'     => 'required|numeric',
            'otpTransactionId'     => 'required|numeric',            
        );
        $messages = array();
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }
    /**
     * checkEmail Address
     * @return JSON
     */
    /*public function checkEmail(\Illuminate\Http\Request $request)
    {
        $data_v    = $this->validatcheckEmail($request);
        $data_send = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => '200', 'res_data' => array() );
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return $data_send;
        }
        $userData = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'LastName', 'WalletPlanTypeId', 'MobileNumber', 'EmailID', 'ProfileStatus')
                        ->orWhere('EmailID', '=', $request->input('email'))
                        ->orderBy('UserWalletID', 'DESC')->get();
        if ( count($userData) > 0 )
        {
            $userData = $userData[0];
            if ( $userData['ProfileStatus'] == 'A' && strtolower($request->input('email')) == strtolower($userData['EmailID']) )
            {
                $data_send['is_error']    = TRUE;
                $err_msg['email']         = 'Email already registered.';
                $data_send['display_msg'] = $err_msg;
            }
        }
        return $data_send;
    }*/

    /* Verify services
     * 
     */

    /*public function verifyService(\Illuminate\Http\Request $request)
    {
        $data = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.requestSuccessfull') ), 'status_code' => '200', 'res_data' => array() );
        return $data;
    }*/

    /**
     * checkMobile Number
     * @return JSON
     */
    /*public function checkMobile(\Illuminate\Http\Request $request)
    {
        $data_v    = $this->validatcheckMobile($request);
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => '200', 'res_data' => array() );
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return $data_send;
        }
        $userData = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'LastName', 'MobileNumber', 'EmailID', 'ProfileStatus', 'ViolaID')
                        ->where('MobileNumber', '=', $request->input('mobileNumber'))
                        ->where('ProfileStatus', 'A')
                        ->orderBy('UserWalletID', 'DESC')->first();
        if ( $userData !== NULL )
        {
            if ( $userData->ViolaID == 0 )
                $userData->ViolaID                = $userData->MobileNumber;
            $data_send['is_error']            = FALSE;
            $data_send['display_msg']['info'] = trans('messages.requestSuccessfull');
        }
        $data_send['res_data'] = $userData;
        return $data_send;
    }

    public function validatcheckMobile($request)
    {
        $validate = array(
            'mobileNumber' => 'numeric|required|digits:10',
        );

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }*/

    /**
     * User Registration First Validation Check.
     *
     * @param  Request  $request
     * @return Response
     */
    /*public function validatcheckEmail($request)
    {
        $validate = array(
            'email' => 'required|email|max:100',
        );

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }*/

    /**
     * Check user already register or not
     * @param type $request
     * @return boolean 
     */
    private function _regUserChk($request)
    {
        $mobile    = $request->input('mobileNumber');
        $emailId   = $request->input('email');
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => '' ), 'status_code' => '200', 'res_data' => array() );
        $userData  = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber', 'EmailID', 'ProfileStatus')
                        ->where('MobileNumber', '=', $mobile)
                        ->orWhere('EmailID', '=', $emailId)->first();
        
        if ( count($userData) == 0 )
        {
            return TRUE;
        }        
        elseif ( count($userData) === 1 ) // Checking the status of registration
        { 
            $email = explode('@', $userData->EmailID);
            $emailPart1 = $email['0'];
            $displayMobile = substr($userData->MobileNumber, 0, 2).'XXXXXX'.substr($userData->MobileNumber,-2);
            $displayEmailId = substr($emailPart1, 0, 2).'XXXXXX'.substr($emailPart1,-2).'@'.$email['1'];
        
            // Checking status Blocked, Frozen, Deactivated, Activated
            if ( $userData['ProfileStatus'] === 'N' )
            {
                $updateUserData = array(
                    'MobileNumber' => $mobile,
                    'EmailID'      => $emailId,
                );
                \App\Models\Userwallet::where('UserWalletID', $userData['UserWalletID'])->update($updateUserData);

                //sending new otp to continue registraiton
                $params    = array(
                    'UserWalletID' => $userData->UserWalletID,
                    'mobileNumber' => $mobile,
                    'typeCode'     => 'REG',
                    'emailId'      => $emailId,
                    'userName'     => '',
                );
                $data_json = $this->_sendOtp($params);
                return $data_json; //sending otp and return response message
            }
            elseif ( $userData['ProfileStatus'] === 'B' )
            {
                $data_send['display_msg'] = array( 'info' => trans('messages.accountDeactivated') );
            }
            elseif ( $userData['ProfileStatus'] === 'F' )
            {
                $data_send['display_msg'] = array( 'info' => trans('messages.accountFrozen') );
            }
            elseif ( $userData['ProfileStatus'] === 'D' )
            {
                $data_send['display_msg'] = array( 'info' => trans('messages.accountDeactivated') );
            }
            elseif ( $userData['ProfileStatus'] === 'A' )
            {
                if ( strtolower($request->input('email')) === strtolower($userData['EmailID']) && $request->input('mobileNumber') === $userData['MobileNumber'] )
                {
                    $data_send['display_msg'] = array( 'info' => trans('messages.mobileEmailRegisterd') );
                }
                elseif ( $request->input('mobileNumber') === $userData['MobileNumber'] )
                {
                    $data_send['display_msg'] = array( 'mobileNumber' => trans('messages.mobileRegisterd', ['name' => $displayEmailId]) );
                }
                elseif ( strtolower($request->input('email')) === strtolower($userData['EmailID']) )
                {
                    $data_send['display_msg'] = array( 'email' => trans('messages.emailRegisterd', ['name' => $displayMobile]) );
                }
            }
        } else {
            $data_send['display_msg'] = array( 'info' => trans('messages.errorRegistration') );
        }
        return response()->json($data_send);
    }

    /**
     * Send Otp to user mobile.
     *
     * @param  Request $params
     * @return Json Response 
     */
    private function _sendOtp($params)
    {
        $data_send  = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $actionName    = 'REGUSER';
        $extraParams = array('requestId' => $params['mobileNumber']);
        $requestParams = array( 'p1' => $params['mobileNumber'], 'p2' => 'Y' );
        $response      = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams, $extraParams);
        $responseData  = json_decode($response, TRUE);
        if ( $responseData['status_code'] == 'SUCCESS' )
        {
            $data_send['is_error'] = FALSE;
            /* Otp verification from Yes Bank service
              $otpData             = array(
              'PartyID'             => $params['UserWalletID'],
              'OTP'                 => json_encode(array('otpRefNumber' => $responseData['otp_ref_number'], 'attempts' => 3)),
              'TransactionTypeCode' => $params['typeCode'],
              'ToMobileNumber' => $params['mobileNumber'],
              'CreatedDateTime'     => date('Y-m-d H:i:s'),
              'CreatedBy'           => 'User'
              );
              $inserytOTPData = \App\Models\Partyotp::insertGetId($otpData);
              $responseData['partyId'] = $params['UserWalletID'];
              $responseData['otpId'] = $inserytOTPData;
              $responseData['otpType'] = 'REG';
             */
        }
        $responseData['otpType'] = "REG";
        $data_send['display_msg']['info'] = $responseData['message'];
        $data_send['res_data']            = $responseData;
        return response()->json($data_send);
    }

    public function createCrmUser($request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $CRM_data   = \App\Libraries\Helpers::crmData();

        $insert_array   = array(
            'entryPoint' => 'API',
            'action'     => 'create',
            'module'     => 'Customer',
            'first_name' => $request->input('first_name'),
            'last_name'  => $request->input('last_name'),
            'phone'      => $request->input('mobileNumber'),
            'email'      => $request->input('email'),
            'custid'     => $partyId,
            'violaid'    => $viola_id,
            'accountno'  => $acnt_number,
        );
        // $insert_array['custid'] = $user_id;
        $attachment_url = '';
        $output         = \App\Libraries\Helpers::curl_send($CRM_data['actionurl'], $insert_array);
        $out            = json_decode($output);
        if ( $out->status === 'success' )
        {
            // code to insert attachment with ticket
        }
    }

    /**
     * Create a function to verify Referral code
     * @param type $request
     * @return boolean
     */
    private function _verifyReffer($ref_code)
    {
        $referer = \App\Models\Userwallet::join('userregistration', 'userwallet.UserWalletID', '=', 'userregistration.UserWalletID')
                ->where('userregistration.RegisteredReferralCode', $ref_code)
                ->select('userwallet.FirstName', 'userwallet.MobileNumber', 'userwallet.EmailID', 'userregistration.RegisteredReferralCode')
                ->first();
        if ( $referer )
        {
            return $referer;
        }

        return FALSE;
    }

    /**
     * User Referral Verification First Validation Check.
     *
     * @param  Request  $request
     * @return JSON Response
     */
    public function referralVerify(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validatReferralCode($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $referralCode = $request->input('referralCode');

        $referer = $this->_verifyReffer($referralCode);
        if ( $referer )
        {
            $data_send['is_error']    = FALSE;
            $data_send['display_msg'] = array( 'info' => trans('messages.referralCodeVerified') );
        }
        else
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.invalidReferralCode') );
        }

        return response()->json($data_send);
    }

    /**
     * User Referral Validation Check.
     *
     * @param  Request  $request
     * @return Response
     */
    private function _validatReferralCode($request)
    {
        $validate = [
            'referralCode' => 'required|min:6|max:30|alpha_num',
        ];

        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    /**
     * Creat User Referral Code
     * @return int
     */
    private function _referralCode($request)
    {
        $email        = $request->input('email');
        $mobile       = $request->input('mobileNumber');
        $emailStr     = strtoupper(substr($email, 0, 4));
        $mobileStr    = substr($mobile, -6);
        $referralCode = $emailStr . $mobileStr;

        //$referer = DB::table('userregistration')->select('RegisteredReferralCode')->where('RegisteredReferralCode', '=', $name_str)->first();
        $referer = \App\Models\Userregistration::select('RegisteredReferralCode')->where('RegisteredReferralCode', '=', $referralCode)->first();
        while ( $referer )
        {
            $randNum      = rand(111111, 999999);
            $referralCode = $emailStr . $randNum;
            //$referer  = DB::table('userregistration')->select('RegisteredReferralCode')->where('RegisteredReferralCode', '=', $name_str)->first();
            $referer      = \App\Models\Userregistration::select('RegisteredReferralCode')->where('RegisteredReferralCode', '=', $referralCode)->first();
        }
        return $referralCode;
    }
    
    private function _userConfigData($partyId)
    {
        $defaultOder    = config('vwallet.FASTTRACK_MENU');
        $fastTrackKeys  = array_keys($defaultOder);
        $fastTrackOrder = implode(",", $fastTrackKeys);
        $configArray    = array( 'balanceView' => 'N', 'fastTrack' => $fastTrackOrder );
        $userConfig     = \App\Models\Userconfiguration::select('ConfigType', 'Details')->where('UserWalletID', $partyId)->get();
        if ( $userConfig )
        {
            foreach ( $userConfig as $configData )
            {
                $configArray[$configData['ConfigType']] = $configData['Details'];
            }
        }

        return $configArray;
    }
    
    /**
     * Aadhaar Registration
     * Submit KYC through Document Submission, The KYC team approve/reject.
     * @param type $request
     */
    public function checkKycStatus(\Illuminate\Http\Request $request)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );
        $data_v    = $this->_validateCheckStaus($request);
        if ( $data_v !== TRUE )
        {
            $data_send['display_msg'] = $data_v;
            return response()->json($data_send);
        }
        $mobile    = $request->input('mobileNumber');
        $actionName    = 'CHECKKYCSTATUS';
        $requestParams = array( 'p1' => $mobile );
        $extraParams = array('requestId' => $mobile);
        $response      = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams, $extraParams);
        $responseData  = json_decode($response, TRUE);
        //print_r($responseData);exit;
        return $output = \App\Libraries\Wallet\Yesbank::handleResponse($responseData);        
    }
    /**
     * Validate check user status
     * @param type $request
     * @return type
     */
    private function _validateCheckStaus($request)
    {
       $validate = array(
            'mobileNumber' => 'numeric|required|digits:10',           
        );

        $validate_response = \Validator::make($request->all(), $validate, array());
        $valid_response    = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }
    /**
     * convert gender info
     * @param type $genderString
     * @return string
     */
    private static function _convertGender($genderString = NULL)
    {    
        $genderStr = strtolower($genderString);
        if ( $genderStr == 'male' )
        {
            $gender = 'M';
        }
        elseif ( $genderStr == 'female' )
        {
            $gender = 'F';
        }
        else
        {
            $gender = 'N';
        }
        return $gender;
    }

}
