<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Transaction Details on recharge
 * 
 * 
 */

$array = [
    'email' => [
        'subject' => '',
        'text'    => '',
    ],
    'sms'   => [
        'text' => $otpCode . '',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file transaction_details_on_recharge */
