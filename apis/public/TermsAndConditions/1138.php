<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Use BUS10 to get INR 10  cashback on minimum booking of INR 100  </li>
        <li>Offer can be redeemed multiple times during the Offer Period, with one registered mobile number. User can perform 10 transactions using the code with cumulative cashback  is INR 200  </li>
        <li>Offer is valid till 30th June, 2018 </li>
        <li>Limited period offer. </li>
        <li>The offer is applicable on Bus Booking only.</li>
    </ul> 
</div>
<?php include 'footer.php';?>
