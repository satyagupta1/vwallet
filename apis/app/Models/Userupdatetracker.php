<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userupdatetracker
 * 
 * @property int $UserWalletID
 * @property string $UpdateFieldName
 * @property string $OldValue
 * @property string $NewValue
 * @property string $SystemIpAddress
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userupdatetracker extends Eloquent
{
	protected $table = 'userupdatetracker';
	protected $primaryKey = 'UserWalletID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UpdateFieldName',
		'OldValue',
		'NewValue',
		'SystemIpAddress',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
