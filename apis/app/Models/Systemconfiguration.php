<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Systemconfiguration
 * 
 * @property int $timestamp
 * @property boolean $data
 *
 * @package App\Models
 */
class Systemconfiguration extends Eloquent
{
        protected $table = 'systemconfiguration';
	public $incrementing = false;
	public $timestamps = false;
	
	protected $fillable = [
		'Section',
		'SecKey',
                'SecTitle',
                'SecDescription'
	];
}
