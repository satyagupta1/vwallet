<?php

namespace App\Http\Controllers\Pays;

/**
 * SendMoneyToBankController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class SendMoneyUPIController extends \App\Http\Controllers\Controller {

    // send Money Wallet to Bank
    public function sendMoneyUsingUPI(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateSendMoneyUPI($request);
        //$checkValidate = TRUE;
        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId         = $request->input('partyId');
        $amount          = $request->input('amount');
        $businessModelid = $request->input('businessModelId');
        $partyVPA        = $request->input('partyVPA');
        $beneficiaryVPA  = $request->input('beneficiaryVPA');
        $serviceType = $request->input('serviceType');
        $productEventId  = 10;

        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        // session Check
        $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sessionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return $output_arr;
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);

        $externalBeneficiaryIds = self::insertExternalBeneficiary($partyInfo,$partyVPA, $beneficiaryVPA);

        $transDetails = self::insertTransDetails($request, $partyId, $externalBeneficiaryIds, $paymentProduct, $fee, $sessionId, $amount);
        if ($transDetails === FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Something went wrong. Please try after sometime.');
            return $output_arr;
        }
        
       
        
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg'] = array('info' => 'Transaction successfully.');
        $output_arr['res_data'] = $transDetails;
        return $output_arr;
    }

    private function _validateSendMoneyUPI($request)
    {
        $validate = array(
            'partyId'         => 'required|numeric|digits_between:1,10',
            'amount'          => 'required|numeric',
            'businessModelId' => 'required|in:2',
        );

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private static function insertTransDetails($request, $partyId, $externalBeneficiaryIds, $paymentProduct, $fee, $sessionId, $amount)
    {
        $transDetail = \App\Models\Transactiondetail::where([['UserWalletId', $partyId], ['TransactionMode', 'P2P'], ['TransactionStatus', 'Success']])->get();
        $discountAmount = NULL;
        $promoCode = NULL;
        if ($transDetail->count() < 1)
        {
            if ($amount >= 50 )
            {
                $requestArray          = \App\Libraries\TransctionHelper::upiPromoCodeInfo($request);
                $verifieyData          = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
                $verifiedPromoCodeData = json_decode($verifieyData);
                if ( $verifiedPromoCodeData->is_error == FALSE )
                {
                    $offerData = $verifiedPromoCodeData->res_data;

                    if ( ! empty($offerData) )
                    {
                        $offerPamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
                        $promoCode = $offerPamentInfo['promoCode'];           
                        $discountAmount = $offerPamentInfo['offerAmount'];
                    }
                }
            }
            
        }
        
        
        $transDate       = date('Y-m-d');
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        $transctionID    = \App\Libraries\TransctionHelper::getTransctionId();
        $transStausMain  = config('vwallet.transactionStatus.pending');
        $useName         = '';
        $transDetails    = array(
            'TransactionOrderID'       => $transctionID,
            'PaymentsProductID'        => $paymentProduct->PaymentsProductID,
            'UserWalletId'             => $partyId, // Wallet user id of from wallet account
            'FromAccountId'            => $externalBeneficiaryIds['partyVPAId'],
            'FromAccountType'          => 'ExternalCardID',
            'ToAccountType'            => 'ExternalCardID',
            'ToAccountId'              => $externalBeneficiaryIds['beneficiaryVPAId'],
            'Amount'                   => $fee['transAmount'],
            'TransactionTypeCode'      => 'Transfer',
            'TransactionDate'          => $dateTimeOfTrans,
            'TransactionStatus'        => $transStausMain,
            'TransactionMode'          => 'P2P',
            'UserSessionUsageDetailID' => $sessionId,
            'TransactionType'          => $paymentProduct->ProductName,
            'CreatedDatetime'          => $dateTimeOfTrans,
            'CreatedBy'                => 'User',
            'TransactionVendor'        => $fee['transctionVendor'],
            'FeeAmount'                => $fee['feeAmount'],
            'TaxAmount'                => $fee['taxAmount'],
            'PromoCode'                => $promoCode,
            'DiscountAmount'           => $discountAmount,
            'UpdatedBy'                => 'User',
        );

        $transactionDetailID = \App\Models\Transactiondetail::insertGetId($transDetails);
        if (!$transactionDetailID)
        {
            return FALSE;
        }

        $transctionLog = \App\Libraries\TransctionHelper::_transctionEventLog($transactionDetailID, $paymentProduct, $transDate, $useName, $dateTimeOfTrans, $amount);

        // preparing notification log details
        $notificationLog = \App\Libraries\TransctionHelper::_notificationLog($transactionDetailID, $paymentProduct, $partyId, '', $amount, $dateTimeOfTrans);

        // insert data into respective tables
        \App\Models\Transactioneventlog::insert($transctionLog);
        \App\Models\Notificationlog::insert($notificationLog);
        $transDetails['transactionId'] = $transactionDetailID;
        return $transDetails;
    }

    private static function insertExternalBeneficiary($partyInfo, $partyVPA, $beneficiaryVPA)
    {

        $externalBeneficiaryIDs = array(
            'partyVPAId'       => 0,
            'beneficiaryVPAId' => 0
        );

        $insertPartyExtDetails = array(
            'UserWalletID' => $partyInfo->UserWalletID,
            'BeneficiaryType'   => 'UPI',
            'isTripleClickUser' => 'N',
            'BeneficiaryStatus' => 'Y',
            'BeneficiaryVPA'    => $partyVPA,
            'SelfBenficiary'    => 'Y'
        );
        
        

        $externalBeneficiaryIDs['partyVPAId'] = \App\Models\Userexternalbeneficiary::insertGetId($insertPartyExtDetails);

        $insertBenefExtDetails = array(
            'UserWalletID' => $partyInfo->UserWalletID,
            'BeneficiaryType'   => 'UPI',
            'isTripleClickUser' => 'N',
            'BeneficiaryStatus' => 'Y',
            'BeneficiaryVPA'    => $beneficiaryVPA,
            'SelfBenficiary'    => 'N'
        );

        $externalBeneficiaryIDs['beneficiaryVPAId'] = \App\Models\Userexternalbeneficiary::insertGetId($insertBenefExtDetails);

        return $externalBeneficiaryIDs;
    }

    public function updatesendMoneyUPIStatus(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateUpdateSendMoneyUPIStatus($request);
        //$checkValidate = TRUE;
        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId       = $request->input('partyId');
        $transactionId = $request->input('transactionId');
        $response      = $request->input('response');
        $yblTxnId      = $request->input('yblTxnId');
        $custRefNo     = $request->input('custRefNo');
        $transStatus   = $request->input('transStatus');
        $partyVPA        = $request->input('partyVPA');
        $beneficiaryVPA  = $request->input('beneficiaryVPA');
        $amount = $request->input('amount');
        // get Logged in User data if Exists.
        $partyInfo     = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        // Created log file for Response
        $messageType = 'yesBank';
        $paramsData  = array('transDetailId' => $transactionId, 'messageType' => $messageType, 'uniqueId' => $yblTxnId);
        \App\Libraries\Vwallet::ResponseLog($response, $paramsData);
        
        $transDetails = \App\Models\Transactiondetail::find($transactionId);
        if (!$transDetails)
        {
            $output_arr['display_msg'] = array('info' => 'Please enter valid transaction ID');
            return response()->json($output_arr);
        }
        
        // Update VPA details
        $updatePartyExternalData = array(
            'BeneficiaryVPA' => $partyVPA
        );
        \App\Models\Userexternalbeneficiary::where('BeneficiaryID',$transDetails->FromAccountId)->update($updatePartyExternalData);
        $updateBenefExternalData = array(
            'BeneficiaryVPA' => $beneficiaryVPA
        );
        \App\Models\Userexternalbeneficiary::where('BeneficiaryID',$transDetails->ToAccountId)->update($updateBenefExternalData);

        $transDetails->RespPGTrackNum    = $yblTxnId;
        $transDetails->RespBankRefNum    = $custRefNo;
        $transDetails->TransactionStatus = $transStatus;
        $transDetails->save();
        
        $upiDetails = \App\Models\Userbeneficiary::where('UserWalletID',$partyInfo->UserWalletID)
                ->where('BeneficiaryVPA',$partyVPA)
                ->first();
        
        if(!$upiDetails){
            $insertUPI = array(
                'UserWalletID' => $partyInfo->UserWalletID,
                'BeneficiaryType' => 'N',
                'BeneficiaryWalletID' => $partyInfo->WalletAccountId,
                'BeneficiaryStatus' => 'N',
                'BeneficiaryVPA' => $partyVPA,
                'SelfBenficiary' => 'Y'
            );
            \App\Models\Userbeneficiary::insert($insertUPI);
        }
        if($transStatus == 'Success') {
            $transDetail = \App\Models\Transactiondetail::where([['UserWalletId', $partyId], ['TransactionMode', 'P2P'], ['TransactionStatus', 'Success']])->get();
            
            $discountAmount = NULL;
            $promoCode = NULL;
            if ($transDetail->count() == 1)
            {
                if ($amount >= 50 )
                {
                    $requestArray          = \App\Libraries\TransctionHelper::upiPromoCodeInfo($request);
                    $verifieyData          = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
                    $verifiedPromoCodeData = json_decode($verifieyData);
                    if ( $verifiedPromoCodeData->is_error == FALSE )
                    {
                        // $cashBackStatus = \App\Libraries\TransctionHelper::cashBackTransaction($request, $verifiedPromoCodeData);
                        $cashBackStatus = \App\Libraries\OfferBussness::addbenefitTrack($transactionId);
                    }
                }
            }
        }
        $transDetails->TransactionStatus = $transStatus;
        $transDetails->ProductName = 'P2P';
        $output_arr['display_msg'] = array('info' => 'Transaction '.$transStatus);
        $output_arr['is_error']    = FALSE;
        $output_arr['res_data']    = $transDetails;
        return $output_arr;
    }

    private function _validateUpdateSendMoneyUPIStatus($request)
    {
        $validate = array(
            'partyId'       => 'required|numeric|digits_between:1,10',
            'transactionId' => 'required|numeric',
            'response'     => 'required',
            'yblTxnId'     => 'required',
            'custRefNo'    => 'required',
            'transStatus'  => 'required',
            'partyVPA'        => 'required',
            'beneficiaryVPA'  => 'required',
        );

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
