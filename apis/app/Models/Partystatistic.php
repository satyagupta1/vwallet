<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partystatistic
 * 
 * @property string $PartyType
 * @property int $TotalNumberOfUsers
 * @property \Carbon\Carbon $ReportDate
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 *
 * @package App\Models
 */
class Partystatistic extends Eloquent
{
	protected $primaryKey = 'PartyType';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TotalNumberOfUsers' => 'int'
	];

	protected $dates = [
		'ReportDate',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TotalNumberOfUsers',
		'ReportDate',
		'UpdatedDateTime',
		'UpdatedBy'
	];
}
