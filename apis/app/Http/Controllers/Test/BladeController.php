<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * BladeController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */
class BladeController extends Controller {

    public function test(Request $request)
    {
        $arr     = view('email', [ 'site_name' => 'James', 'mobile' => 9123456789, 'otp_code' => 123456 ]);
        $templet = json_decode($arr);
//        print_r($templet);
        return $templet->email->subject;
    }

    public function testBlade(Request $request)
    {
//        $arr = view()->make('email', ['site_name' => 'James', 'mobile' => 9123456789, 'otp_code' => 123456]);   
//        $templet = json_decode($arr);
//        print_r($templet);
        return view()->make('email', [ 'site_name' => 'James', 'mobile' => 9123456789, 'otp_code' => 123456 ]);
    }

    public function CommunicateTest(Request $request)
    {
        
        $partyId       = $request->input('partyId');
        $templateKey   = $request->input('templateKey');
        $email         = $request->input('emailId');
        $mobile        = $request->input('mobileNumber');
        $linkedTransId = $request->input('linkedTransId');
        $templateData  = [
            'partyId'        => $partyId,
            'linkedTransId'  => 0, // if transction related notification
            'templateKey'    => $templateKey,
            'senderEmail'    => $email,
            'senderMobile'   => $mobile,
            'reciverId'      => 542, // if reciver / sender exsists
            'reciverEmail'   => 'anand.akurathi1@violamoney.com',
            'reciverMobile'  => '831138015',
            'templateParams' => array( // template utalisation parameters
                'fullname'     => 'Anand Kumar',
                'otpCode'      => 123456,
                'email'        => 'anand.akurathi1@violamoney.com',
                'deviceName'   => 'MOTO XS',
                'receiverName' => 'Ragava',
                'mobileNumber' => '831138015',
                    'cardnumber' => '555656565',
                    'amount' => 100,
                    'totalamount' => 100,
                    'date' => date('d-m-y'),
                    'time' => date('h:a:'),
            )
        ];
       $output =  \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
       print_r($output);
       die();
    }

    public function testMaasRequest(Request $request)
    {
        $prirority = array( 'high', 'low', 'medium', 'normal' );
        $array     = array(
            'comChannel'     => "sms",
            'product'        => 'VC',
            'message'        => 'Hello Anand, Welcome to Viola Group ',
            'priority'       => $prirority[array_rand($prirority, 1)],
            'to'             => 9885109781,
            'countryCode'    => 'IND',
            'notificationId' => 130
        );

        $data   = json_encode($array);
        $url    = 'http://192.168.101.30/maas/api/sendNotification';
//        $url    = 'http://localhost/maas/api/sendNotification';
        $encKey = config('vwallet.ENCRYPT_SALT');

        $responseData = \App\Libraries\Helpers::encryptData($data, $encKey, TRUE);

        $encData = http_build_query(array( 'encryptedData' => $responseData )) . "\n";
        \Log::info('Request:  ' . $encData);

        $res = \App\Libraries\Helpers::curlSend($url, $encData);
        \Log::info('Response:  ' . $res);

        $dataRes     = json_decode($res);
        $decryptData = \App\Libraries\Helpers::decryptData1($dataRes->encryptedData, $encKey, TRUE);
        \Log::info('Response:  ' . $decryptData);

        return $decryptData;
    }

}
