<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 16 Nov 2017 06:22:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Circlemaster
 * 
 * @property int $Circle_Id
 * @property string $Master
 *
 * @package App\Models
 */
class Circlemaster extends Eloquent
{
	protected $table = 'circlemaster';
	protected $primaryKey = 'Circle_Id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'Circle_Id' => 'int'
	];

	protected $fillable = [
		'Master'
	];
}
