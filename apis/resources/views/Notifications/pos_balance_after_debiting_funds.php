<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Balance details after debiting funds (Success Cases) template 
 *  param $orderId varchar 
 *  param $balanceAmount numeric 
 *  param $posMerchantName varchar 
 *  param $dateTime varchar  
 *  param $amount varchar  
 */

$array = [
    'email' => [
        'subject' => '',
        'text'    => ''
    ],
    'sms'   => [
        'text' => 'Paid Rs.' . $amount . '  to ' . $posMerchantName . ' at time '.$dateTime.'. Order ID: ' . $orderId . ' . Current balance on ViolaWallet Rs ' . $balanceAmount],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file pos_balance_after_debiting_funds.php */
