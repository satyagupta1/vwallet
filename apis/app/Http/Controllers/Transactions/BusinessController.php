<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;

/**
 * BusinessController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class BusinessController extends Controller {

    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');          
        $this->userTypeKey     = config('vwallet.userTypeKey');
        $this->adminTypeKey     = config('vwallet.adminTypeKey');
        $this->defaultWalletPlanId     = config('vwallet.walletPlanTypeId');        
        //$this->lang = apdefaultWalletPlanIdp('translator')->getLocale();
    }

    /*
     *  Search Business - Advanced Business Search - View Business
     *  @Desc This is used to get results with given params, orderby, limit and pagination.
     *  @param Request $request
     *  @return JSON
     */

    public function getFeePlans(\Illuminate\Http\Request $request)
    {
        

        //$currency = \App\Models\Business::find(1)->currency()->get(); 
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateFeePlan($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }    
        $planDetails = $this->_resultFeePlans($request);
        $countResults = count($planDetails);
        
        $output_arr['is_error']    = FALSE; 
        if ( $countResults > 0 )
        {           
            $output_arr['display_msg'] = array();
            $output_arr['res_data']    = $planDetails;
            return response()->json($output_arr);
        }  
        
        //if no results and no walletPlanId then we have to apply default plan type
        if ( $request->input('walletPlanId') == NULL )
        {   
            $request->request->add(['walletPlanId' => $this->defaultWalletPlanId]);//default user wallet type
            $planDetails = $this->_resultFeePlans($request);
            $countResults = count($planDetails);
            if ( $countResults > 0 )
            {           
                $output_arr['display_msg'] = array();
                $output_arr['res_data']    = $planDetails;
                return response()->json($output_arr);
            }  
        }        
       
          
        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);

    }

    /**
     * Validate Business request data
     * @param $request
     * @return String
     */
    private function _validateFeePlan($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',            
                //'device_type' => 'required|in:W,I,A,M',
        ];
        if ( $request->input('walletBusinessId') !== NULL )
        {
            $validate['walletBusinessId'] = 'required|numeric';
        }
        if ( $request->input('paymentProductId') !== NULL )
        {
            $validate['paymentProductId'] = 'required|numeric';
        }
        if ( $request->input('walletPlanId') !== NULL )
        {
            $validate['walletPlanId'] = 'required|numeric';
        }
        

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);
        
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    /**
     * Return Business fee plan types and data
     * @param $request
     * @return String
     */
    private function _resultFeePlans($request){
        
        $orderBy            = $request->input('orderBy');
        $orderByColumn      = $request->input('orderColumn');
        $limit              = $request->input('limit'); 
        $partyId            = $request->input('partyId');
        $walletPlanId      = $request->input('walletPlanId');
        $walletBusinessId      = $request->input('walletBusinessId');
        $paymentProductId      = $request->input('paymentProductId');
        

        $selectedColumns = [            
            'productplanconfig.ProductPlanChargeDetailsid',
            'productplanconfig.WalletBusinessModelid',
            'productplanconfig.PaymentsProductID',
            'productplanconfig.WalletPlanTypeId',
            'productplanconfig.MinTransAmt',
            'productplanconfig.MaxTransAmt',
            'productplanconfig.FeeFreqCycle',
            'productplanconfig.FreeTransLimit',
            'productplanconfig.IntFeePct',
            'productplanconfig.IntFeeFixAmt',
            'productplanconfig.RevFeePerc',
            'productplanconfig.RevFixFeeAmt',
            'productplanconfig.TaxGSTPerc',
            'productplanconfig.TaxSGSTPerc',
            'productplanconfig.StartDate',
            'productplanconfig.EndDate',
            'productplanconfig.ActiveFlag as ppcActiveFlag',            
            'paymentsproduct.ProductName',
            'paymentsproduct.ProductType',
            'paymentsproduct.EffectiveDate',
            'paymentsproduct.OnlineVendorName',
            'walletplantype.PlanName',
            'walletplantype.PlanDescription',
            'walletplantype.ActiveFlag as wptActiveFlag',
            'walletbusinessmodel.BusinessModelName',
            'walletbusinessmodel.ActiveFlag as wbmActiveFlag'            
        ];
        
        $query = \App\Models\Productplanconfig::leftjoin('walletplantype', 'productplanconfig.WalletPlanTypeId', '=', 'walletplantype.WalletPlanTypeId')                
                ->leftjoin('paymentsproduct', 'productplanconfig.PaymentsProductID', '=', 'paymentsproduct.PaymentsProductID')
                ->leftjoin('walletbusinessmodel', 'productplanconfig.WalletBusinessModelid', '=', 'walletbusinessmodel.WalletBusinessModelid');                
        
        //filter by walletPlanTypeId type
        if ( $walletPlanId !== NULL )
        {           
            $query->where('productplanconfig.WalletPlanTypeId', '=', $walletPlanId);
        }
        else // filter by user subscription wallet type
        {
            array_push($selectedColumns, 'userwallet.WalletPlanTypeId as UserWalletPlanType', 'userwallet.UserWalletID' );
            $query->leftjoin('userwallet', 'productplanconfig.WalletPlanTypeId', '=', 'userwallet.WalletPlanTypeId')
                    ->where('userwallet.UserWalletID', '=', $partyId);
        }

        //filter by WalletBusinessModelid type
        if ( $walletBusinessId !== NULL )
        {
            $query->where('productplanconfig.WalletBusinessModelid', '=', $walletBusinessId);
        }
        //filter by PaymentsProductID type
        if ( $paymentProductId !== NULL )
        {
            $query->where('productplanconfig.PaymentsProductID', '=', $paymentProductId);
        }
        
        $date = date('Y-m-d H:i:s');
        $query->where('productplanconfig.StartDate', '<', $date)
                ->where('productplanconfig.EndDate', '>', $date)
                ->where('productplanconfig.ActiveFlag', '=', 'Y')
                ->where('walletplantype.ActiveFlag', '=', 'Y')
                ->where('walletbusinessmodel.ActiveFlag', '=', 'Y');
        
        
        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectedColumns) )
        {            
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {            
            $query->orderBy('productplanconfig.ProductPlanChargeDetailsid', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }
        
        return  $query->get($selectedColumns)->toArray();        
    }

}
