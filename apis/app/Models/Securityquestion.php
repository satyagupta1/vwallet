<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Securityquestion
 * 
 * @property int $QuestionId
 * @property string $QuestionDetails
 * @property string $QuestionCategory
 * @property string $IsStatusActive
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partysecurities
 *
 * @package App\Models
 */
class Securityquestion extends Eloquent
{
	protected $table = 'securityquestion';
	protected $primaryKey = 'QuestionId';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'QuestionDetails',
		'QuestionCategory',
		'IsStatusActive',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partysecurities()
	{
		return $this->hasMany(\App\Models\Partysecurity::class, 'QuestionId');
	}
}
