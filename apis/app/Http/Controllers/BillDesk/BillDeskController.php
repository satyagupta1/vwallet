<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\BillDesk;

class BillDeskController extends \App\Http\Controllers\Controller {
    /*
     *  billDesk - This method is used to connect with bill desk api to get billers data
     *  
     * @param $request 
     * 
     *  @return string
     */

    public function planDetails($circleId)
    {

//        $circleList = $this->getCirclesList();
//        if($circleList === FALSE)
//        {
//            $output_arr['display_msg'] = array( 'info' => trans('messages.no_Circles_billdesk') );
//            return json_encode($output_arr);
//        }

        $billersList = $this->getBillersList();
        if ( $billersList === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.no_Billers_billdesk') );
            return json_encode($output_arr);
        }

//        foreach($circleList as $mainCricleid)
//        {
//            
        foreach ( $billersList as $mainBillersList )
        {


            $sourceid    = config('vwallet.sourceId');
            $txnId       = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
            $dateTime    = config('vwallet.bdTimeStamp'); // gen of dateTime
            $messageCode = 'U03081';

            $arrayparams      = array(
                'MessageCode'   => $messageCode,
                'TraceID'       => $txnId,
                'SourceID'      => $sourceid,
                'TimeStamp'     => $dateTime,
                'mobilenumber'  => 'NA',
                'BillerId'      => $mainBillersList->BillerId,
                // 'BillerId' => 'AIRCELPRE',
                'CircleID'      => $circleId,
                // 'CircleID' => '4',
                'Extra Field 2' => 'Y',
                'Category'      => 'ALL',
                'FetchPlanType' => 'A',
                'Filler1'       => 'NA',
                'Filler2'       => 'NA',
                'Filler3'       => 'NA',
            );
            $implodeArraykeys = implode('~', $arrayparams);
            // Genartion of HMAC key form library
            $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

            $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

            // url for required api
            $url = config('vwallet.billdeskApiUrl') . $stringKeys;


            // get return response from curl method
            $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url);
            // check reponse if server is sending html reponse that their sever is down
            if ( $response != strip_tags($response) )
            {

                $output['is_error']            = TRUE;
                $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
                return json_encode($output);
            }

            $responseDecode = json_decode($response);


            $rechargeDetails = $responseDecode->RechargeDetails;
            $coutuntRecharge = count($rechargeDetails);


            // return response when data is empty
            if ( $coutuntRecharge <= 0 )
            {
                $output_arr['display_msg'] = array( 'info' => 'No Data Found' );
                return json_encode($output_arr);
            }


            // checking opertor name exists or not
            if ( ! empty($rechargeDetails->operatorName) )
            {
                $operatorName = $this->getBillersId($rechargeDetails->operatorName);
                $circleName   = $this->getCirclesId($rechargeDetails->circleName);

                foreach ( $rechargeDetails->RechargeCategoryList as $rechargekey => $rechrgeCategories )
                {
                    $rechargePlanCategory = $rechargekey;


                    foreach ( $rechrgeCategories as $mainValuerecharge )
                    {

                        $rechargeAmount      = $mainValuerecharge->rechargeAmount;
                        $talkTime            = $mainValuerecharge->talkTime;
                        $validity            = $mainValuerecharge->validity;
                        $rechargePlanId      = $mainValuerecharge->rechargePlanId;
                        $rechargeDescription = $mainValuerecharge->rechargeDescription;

                        $insertPlanDetails                  = new \App\Models\Operatorfeeplan;
                        $insertPlanDetails->FeePlanID       = $rechargePlanId;
                        $insertPlanDetails->Operator        = $operatorName->BillerId;
                        $insertPlanDetails->Circle          = $circleId;
                        $insertPlanDetails->CategoryName    = $rechargePlanCategory;
                        $insertPlanDetails->Mrp             = $rechargeAmount;
                        $insertPlanDetails->TalkTime        = $talkTime;
                        $insertPlanDetails->ServiceTax      = 0;
                        $insertPlanDetails->ProcessingFees  = 0;
                        $insertPlanDetails->AccessFees      = 0;
                        $insertPlanDetails->Validity        = $validity;
                        $insertPlanDetails->Description     = $rechargeDescription;
                        $insertPlanDetails->CreatedDateTime = date('Y-m-d h:s:i');
                        $insertPlanDetails->CreatedBy       = 'Admin';
                        $insertPlanDetails->save();
                    }
                }

                if ( $insertPlanDetails )
                {
                    $output_arr['display_msg'] = array( 'info' => 'Data Uploaded Successfully' );
                    return json_encode($output_arr);
                }
            }

// echo $response->RechargeDetails;
        }
//           
//        } // ends of circle foreach
        //return $response;
    }

    // code for get circle master data
    public function getCirclesList()
    {

        $selcircles = \App\Models\Circlemaster::select('Circle_Id', 'Master')->get();
        return ( ! empty($selcircles)) ? $selcircles : FALSE;
    }

    // code for get Billers List master data
    public function getBillersList()
    {

        $selBillers = \App\Models\Billerlist::select('BillerId')->where('BillerStatus', 'Y')->get();
        return ( ! empty($selBillers)) ? $selBillers : FALSE;
    }

    // code for get circle master data
    public function getCirclesId($circleName)
    {

        $selcircles = \App\Models\Circlemaster::select('Circle_Id', 'Master')->where('Master', $circleName)->first();
        return ( ! empty($selcircles)) ? $selcircles : FALSE;
    }

    // code for get Billers List master data
    public function getBillersId($billerName)
    {

        $selBillers = \App\Models\Billerlist::select('BillerId')->where('BillerName', $billerName)->first();
        return ( ! empty($selBillers)) ? $selBillers : FALSE;
    }

    /**
     * U03011 Recharge Query Details request message specifications
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function rechargeQuery(\Illuminate\Http\Request $request)
    {
        $checkValidate = $this->_validateRechargeQuery($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }

        $output           = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $sourceid         = config('vwallet.sourceId');
        $txnId            = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
        $dateTime         = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode      = 'U03011';
        $partyId          = $request->input('partyId');
        $transactionID    = $request->input('transactionId');
        $arrayparams      = array(
            'MessageCode'   => $messageCode,
            'TraceID'       => $txnId,
            'SourceID'      => $sourceid,
            'TimeStamp'     => $dateTime,
            'UserID'        => $partyId,
            'CustomerId'    => $partyId,
            'TransactionID' => $transactionID, //if Source ID is ABC and Bank Ref number is 123456, then Transaction ID will be ABC123456                       
            'Filler1'       => 'NA',
            'Filler2'       => 'NA',
            'Filler3'       => 'NA',
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

        $insertData = json_encode($arrayparams);
        // url for required api
        $url        = config('vwallet.billdeskApiUrl') . $stringKeys;

        // get return response from curl method
        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url);

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }
        $responseArray = explode('~', $response);

        $responseKeys = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'RechargeBillerId',
            'Authenticator1',
            'Authenticator2',
            'Authenticator3',
            'TransactionID',
            'PaymentTransActionDate',
            'RechargeAmount',
            'PaymentID',
            'PaymentStatus',
            'RechargeStatus',
            'RechargeStatusDescription',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );
        if ( count($responseKeys) != count($responseArray) )
        {
            $output['res_data'] = $responseArray;
            return json_encode($output);
        }
        $combine                       = array_combine($responseKeys, $responseArray);
        $billerName                    = \App\Libraries\TransctionHelper::getBilldeskName($combine['RechargeBillerId']);
        $combine['RechargeBillerName'] = ($billerName) ? $billerName : $billerId;
        //store log file
        $insertData                    = json_encode($combine);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $combine);
        $output['is_error']            = FALSE;
        $output['res_data']            = $combine;
        return json_encode($output);
    }

    /**
     * U03013 Payment and Recharge Details request message
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function rechargePayment(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateRechargePayment($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }
        $partyId          = $request->input('partyId');
        $mobileNumber     = $request->input('mobileNumber');
        $billerId         = $request->input('billerId');
        $paymentID        = $request->input('paymentId');
        $paymentChannelID = $request->input('paymentChannelId');
        $rechargeAmount   = $request->input('rechargeAmount');
        $paymentType      = $request->input('paymentType');
        $bankRefNo        = $request->input('bankRefNo');
        $bankMessage      = $request->input('bankMessage');
        $rechargePlanId   = $request->input('rechargePlanId');

        $offerData = NULL;
        if ( trim($request->input('promoCode')) != NULL )
        {
            $requestArray['aplicableChannel'] = $request->header('device-type');
            $requestArray['promoCode']        = $request->input('promoCode');
            //$requestArray['offerCategoryId'] = 0;
            // $requestArray['offerUserTypeId'] = 1;
            //$requestArray['benefitCategoryId'] = 0;
            if ( $request->input('offerCategoryId') != NULL )
            {
                $requestArray['offerCategoryId'] = $request->input('offerCategoryId');
            }
            if ( $request->input('offerUserTypeId') != NULL )
            {
                $requestArray['offerUserTypeId'] = $request->input('offerUserTypeId');
            }

            $requestArray['amount']           = $request->input('rechargeAmount');
            $requestArray['partyId']          = $request->input('partyId');
            $requestArray['paymentProductId'] = $request->input('paymentProductId');
            $verifyJsonData                   = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
            $verifyData                       = json_decode($verifyJsonData);

            if ( $verifyData->is_error == TRUE )
            {
                return $verifyJsonData;
            }
            $offerData  = $verifyData->res_data;
            $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
            if ( ! empty($pamentInfo) )
            {
                $offerPamentInfo = array( 'offerPamentInfo' => $pamentInfo );
                $request->request->add($offerPamentInfo);
            }
        }
        if ( $request->input('superCash') > 0 )
        {
            $requestArray         = array( 'partyId' => $request->input('partyId'), 'amount' => $request->input('rechargeAmount') );
            $superCashPaymentInfo = \App\Libraries\OfferRules::getSuperCashPaymentInfo($requestArray);
            if ( ! empty($superCashPaymentInfo) )
            {
                $superCashInput = array( 'superCashPamentInfo' => $superCashPaymentInfo );
                $request->request->add($superCashInput);
            }
        }

        $sourceid    = config('vwallet.sourceId');
        $txnId       = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
        $dateTime    = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode = 'U03013';

        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ( $partyInfo === FALSE )
        {
            $output['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output;
        }

        $billerInfo = \App\Models\Billerlist::select('BillerName', 'Category', 'BillerType')->where('BillerId', $billerId)->first();


        $amount                 = $rechargeAmount;
        $entityId               = 1;
        $transStaus             = config('vwallet.transactionStatus.pending');
        //trans_type whether it is WW or WB
        $paymentsProductEventID = $this->getEventId($billerId . 'INI')['PaymentProductEventID'];
        $mainProdId             = $this->getProductId($billerId . 'INI')['PaymentsProductID'];
        $bankRefId              = ( ! empty($request->input('bankRefNo'))) ? $request->input('bankRefNo') : 0;
        $payementId             = ( ! empty($request->input('paymentId'))) ? $request->input('paymentId') : 0;
        $sesionId               = \App\Libraries\TransctionHelper::getUserSessionId($request);


        $partyTransLimit   = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductEventID);


        // fee calculation 
        $fee       = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $entityId, $partyId);
        $transInfo = $this->_getTransactionInfo($fee, $request);
        if ( $request->input('checkout') === 'FALSE' )
        {
            $output['is_error']    = FALSE;
            $output['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
            $output['res_data']    = $transInfo;
            return response()->json($output);
        }

        if ( $partyInfo->CurrentAmount < $transInfo['totalAmount'] )
        {
            $output['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output;
        }

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, TRUE, $transStaus, $sesionId);
        $orderId  = $transRes['response']['TransactionDetails']['TransactionOrderID'];

        //\DB::enableQueryLog();
        $updateRefIdMain                  = \App\Models\Transactiondetail::select('TransactionDetailID')->where('TransactionOrderID', $orderId)->first();
        //    print_r(\DB::getQueryLog());
        $updateTxnDetails                 = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
        $updateTxnDetails->RespPGTrackNum = ( ! empty($payementId)) ? $payementId : 0;
        $updateTxnDetails->RespBankRefNum = ( ! empty($bankRefId)) ? $bankRefId : 0;
        $updateTxnDetails->save();

        $arrayparams      = array(
            'MessageCode'             => $messageCode,
            'TraceID'                 => $txnId,
            'SourceID'                => $sourceid,
            'TimeStamp'               => $dateTime,
            // 'UserID'                  => $partyId,
            'UserID'                  => config('vwallet.billdeskUserId'),
            'CustomerID'              => $partyId,
            'RechargeBillerId'        => $billerId,
            'ShortName'               => 'NA',
            'RechargeBillerAccountID' => 'NA',
            'Authenticators'          => $mobileNumber,
            'PaymentID'               => $paymentID,
            'PaymentChannelID'        => $paymentChannelID, //ATM/MOBILE/N ETB/KIOSK           
            'RechargeAmount'          => $rechargeAmount,
            'PaymentType'             => $paymentType, //RNP, PNY
            'BankRefNo'               => $bankRefNo,
//            'BankMessage'             => $bankMessage, //message
            'BankMessage'             => config('vwallet.billdeskBankMessage'),
            'Filler1'                 => config('vwallet.billdeskFiller1'),
            'Filler2'                 => config('vwallet.billdeskFiller2'),
            'Filler3'                 => config('vwallet.billdeskFiller3'),
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

        //store log file       
        $insertData = json_encode($arrayparams);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $arrayparams);
        // url for required api
        $url        = config('vwallet.billdeskApiUrl') . $stringKeys;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $txnId,
            'thirdPartyName' => 'Bill Desk Payment',
            'thirdPartyCall' => $messageCode,
            'referenceId'    => 0,
            'request'        => $arrayparams,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);

        // get return response from curl method
        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url, $logId);

        if ( $response === FALSE )
        {
            $output['display_msg'] = array( 'info' => "Operator is taking more time than expected. Don't worry if the transaction fails, money will be back to your wallet." );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Desk Payment',
                'response'       => $combine,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }
        $responseArray = explode('~', $response);
        $responseKeys  = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'PaymentID',
            'TransactionID',
            'BankRefNo',
            'PaymentStatus',
            'PaymentStatusDescription',
            'RechargeStatus',
            'RechargeStatusDescription',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );

        if ( count($responseKeys) != count($responseArray) )
        {
            $responseData = $responseArray;
            if ( count($responseArray) == 2 )
            {
                $responseKeys = array( 'errorCode', 'errorMsg' );
                $responseData = array_combine($responseKeys, $responseArray);
            }
            $output['res_data'] = $responseData;

            $logRequest = array(
                'thirdPartyName' => 'Bill Desk Payment',
                'response'       => $responseData,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            return json_encode($output);
        }

        $combine = array_combine($responseKeys, $responseArray);

        $logRequest = array(
            'thirdPartyName' => 'Bill Desk Payment',
            'response'       => $combine,
            'status'         => 'Success'
        );
        \App\Libraries\LogHelper::createLog($logRequest, $logId);

        // calling billdesk query starts
        $messageCode        = 'U03011';
        $transactionIDquery = $combine['TransactionID'];

        $arrayparamsQuery      = array(
            'MessageCode'   => $messageCode,
            'TraceID'       => $txnId,
            'SourceID'      => $sourceid,
            'TimeStamp'     => $dateTime,
            'UserID'        => $partyId,
            'CustomerId'    => $partyId,
            'TransactionID' => $transactionIDquery, //if Source ID is ABC and Bank Ref number is 123456, then Transaction ID will be ABC123456                       
            'Filler1'       => 'NA',
            'Filler2'       => 'NA',
            'Filler3'       => 'NA',
        );
        $implodeArraykeysQuery = implode('~', $arrayparamsQuery);
        // Genartion of HMAC key form library
        $hmacKeyQury           = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeysQuery);

        $stringKeysQuery = $implodeArraykeysQuery . '~' . strtoupper($hmacKeyQury);

        //store log file       
        //  $insertData = json_encode($arrayparams);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $arrayparams);
        // url for required api
        $urlQuery = config('vwallet.billdeskApiUrl') . $stringKeysQuery;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $txnId,
            'thirdPartyName' => 'Bill Desk Query',
            'thirdPartyCall' => $messageCode,
            'referenceId'    => 0,
            'request'        => $arrayparamsQuery,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);

        // get return response from curl method
        $responseQuery = \App\Libraries\BilldeskHelper::billDeskGETCURL($urlQuery, $logId);

        if ( $responseQuery === FALSE )
        {
            $output['display_msg'] = array( 'info' => "Operator is taking more time than expected. Don't worry if the transaction fails, money will be back to your wallet." );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $responseQuery != strip_tags($responseQuery) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Desk Query',
                'response'       => $responseQuery,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }
        $responseArrayQuery = explode('~', $responseQuery);

        $responseKeysQury = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'RechargeBillerId',
            'Authenticator1',
            'Authenticator2',
            'Authenticator3',
            'TransactionID',
            'PaymentTransActionDate',
            'RechargeAmount',
            'PaymentID',
            'PaymentStatus',
            'RechargeStatus',
            'RechargeStatusDescription',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );
        if ( count($responseKeysQury) != count($responseArrayQuery) )
        {
            $logRequest         = array(
                'thirdPartyName' => 'Bill Desk Query',
                'response'       => $responseArrayQuery,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);
            $output['res_data'] = $responseArrayQuery;
            return json_encode($output);
        }
        $combinequery = array_combine($responseKeysQury, $responseArrayQuery);
        $logRequest   = array(
            'thirdPartyName' => 'Bill Desk Query',
            'response'       => $combinequery,
            'status'         => 'Success'
        );
        \App\Libraries\LogHelper::createLog($logRequest, $logId);
        // recharge Query Ends
        //store log file
        $insertData   = json_encode($combine);

        // insert into user operator fee plan
        $insertFeePlanArray = array( 'Operator' => $billerId, 'FeePlanID' => 0, 'Circle' => 0, 'Mrp' => $rechargeAmount );
        $getOperatorplanId  = $this->_insertUserPlan($rechargePlanId, $insertFeePlanArray);

        // insertion in user recharge details
        $subCategory = $this->getBillerCategory($billerId)['SubCategory'];
        if ( $subCategory == 'PREPAID MOBILE' || $subCategory == 'PREPAID DTH' )
        {
            $status = ($combine['RechargeStatus'] == 'SUCCESS' ) ? 'Success' : 'Failed';
            $this->insertRechargeDetails($partyId, $updateRefIdMain->TransactionDetailID, $status, $mobileNumber, $getOperatorplanId, $rechargeAmount, $combine['PaymentID'], $combine['TransactionID'], $billerId, $billerInfo->BillerName, $billerInfo->Category, $billerInfo->BillerType, $combine['Valid']);
        }


        $updatetxneentLog           = \App\Models\Transactioneventlog::find($this->getTxnNotifLog($updateRefIdMain->TransactionDetailID)['TransactionEventDetailID']);
        $updatetxneentLog->Comments = 'Recharge For ' . $mobileNumber . '(' . \App\Libraries\TransctionHelper::getBilldeskName($billerId) . ')';
        $updatetxneentLog->save();
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $combine);

        if ( $combine['Valid'] == 'Y' && $combine['RechargeStatus'] == 'SUCCESS' && $combinequery['Valid'] == 'Y' && $combinequery['RechargeStatus'] == 'SUCCESS' )
        {

            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = config('vwallet.transactionStatus.success');
            $updateTxnDetails->save();

            if ( $request->input('offerPamentInfo') != NULL )
            {
                $offerPamentInfo = $request->input('offerPamentInfo');
                \App\Libraries\OfferBussness::addbenefitTrack($updateRefIdMain->TransactionDetailID);
            }
            $superCashPaymentInfo = $request->input('superCashPamentInfo');
            if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 )
            {
                //if(!empty($superCashPaymentInfo) && $transStausMain == 'Success') {
                $trackRequest                        = [];
                $trackRequest['partyId']             = $partyId;
                $trackRequest['amount']              = $superCashPaymentInfo['offerAmount'];
                $trackRequest['transactionDetailId'] = $updateRefIdMain->TransactionDetailID;
                \App\Libraries\OfferBussness::updateUsedAmount($trackRequest);
            }
            $statusRecharge = 'Success';
            $msg            = 'Recharged successfully with Rs. ' . $rechargeAmount;
        }
        else if ( $combinequery['RechargeStatus'] == 'PENDING' )
        {
            $statusRecharge                      = 'Pending';
            $msg                                 = 'Pending to Pay Billpayment';
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = 'Pending';
            $updateTxnDetails->save();
        }
        else
        {

            $statusRecharge                      = 'Failed';
            $msg                                 = 'Failed to recharge';
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = config('vwallet.transactionStatus.failed');
            $updateTxnDetails->save();

            \App\Libraries\PaymentRefund::paymentRefund($updateRefIdMain->TransactionDetailID);
        }


        $userBalanceInfo = \App\Libraries\TransctionHelper::getWalletInfo($partyId);

        $combine['TransactionID']             = $orderId;
        $combine['txnStatus']                 = $combine['RechargeStatus'];
        $combine['amount']                    = $rechargeAmount;
        // $combine['availableBalance']          = (number_format((float) $this->getuserBalance($partyId)['AvailableBalance'], 2, '.', ''));
        //$combine['currentBalance']            = (number_format((float) $this->getuserBalance($partyId)['CurrentAmount'], 2, '.', ''));
        $combine['txnMsg']                    = 'Recharge For ' . $mobileNumber . '(' . \App\Libraries\TransctionHelper::getBilldeskName($billerId) . ')';
        $billerName                           = \App\Libraries\TransctionHelper::getBilldeskName($billerId);
        $combine['RechargeBillerName']        = ( ! empty($billerName)) ? $billerName : $billerId;
        $combine['BillerId']                  = $billerId;
        $combine['PaymentTransActionDate']    = $combinequery['PaymentTransActionDate'];
        $combine['billdeskPaymentId']         = $combinequery['PaymentID'];
        $combine['MessageCode']               = $combinequery['MessageCode'];
        $combine['TraceID']                   = $combinequery['TraceID'];
        $combine['SourceID']                  = $combinequery['SourceID'];
        $combine['TimeStamp']                 = $combinequery['TimeStamp'];
        $combine['UserID']                    = $combinequery['UserID'];
        $combine['CustomerID']                = $combinequery['CustomerID'];
        $combine['Valid']                     = $combinequery['Valid'];
        $combine['ErrorCode']                 = $combinequery['ErrorCode'];
        $combine['ErrorMessage']              = $combinequery['ErrorMessage'];
        $combine['RechargeBillerId']          = $combinequery['RechargeBillerId'];
        $combine['Authenticator1']            = $combinequery['Authenticator1'];
        $combine['Authenticator2']            = $combinequery['Authenticator2'];
        $combine['Authenticator3']            = $combinequery['Authenticator3'];
        $combine['PaymentTransActionDate']    = $combinequery['PaymentTransActionDate'];
        $combine['RechargeAmount']            = $combinequery['RechargeAmount'];
        $combine['PaymentID']                 = $combinequery['PaymentID'];
        $combine['PaymentStatus']             = $combinequery['PaymentStatus'];
        $combine['RechargeStatus']            = $combinequery['RechargeStatus'];
        $combine['RechargeStatusDescription'] = $combinequery['RechargeStatusDescription'];
        $combine['Filler1']                   = $combinequery['Filler1'];
        $combine['Filler2']                   = $combinequery['Filler2'];
        $combine['Filler3']                   = $combinequery['Filler3'];
        $combine['BBPSReferencenumber']       = $combinequery['Filler3'];
        $combine['rechargePlanId']            = $rechargePlanId;
        $combine['FeeAmount']                 = $transRes['response']['TransactionDetails']['FeeAmount'];
        $combine['TaxAmount']                 = $transRes['response']['TransactionDetails']['TaxAmount'];
        $combine['WalletAmount']              = $transRes['response']['TransactionDetails']['WalletAmount'];
        $combine['DiscountAmount']            = $transRes['response']['TransactionDetails']['DiscountAmount'];
        $combine['CashbackAmount']            = $transRes['response']['TransactionDetails']['CashbackAmount'];

        $returnData = array_merge($combine, $userBalanceInfo);

        $offerAmount = '0.00';
        if ( isset($superCashPaymentInfo['offerAmount']) && ($superCashPaymentInfo['offerAmount'] <= 0) )
        {
            $offerAmount = $superCashPaymentInfo['offerAmount'];
        }

        $billerName = $this->getbillerNameRecharge($billerId)['BillerName'];
        //$getBalance = $this->getuserBalance($partyId);
        // Communication Start

        $templateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW016',
            'senderEmail'    => $partyInfo->EmailID,
            'senderMobile'   => $partyInfo->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullname'           => ($partyInfo->FirstName) ? $partyInfo->FirstName : '',
                'amount'             => $rechargeAmount,
                'rechargedate'       => date('d/m/Y'),
                'date'               => date('d/m/Y'),
                'time'               => date('H:i A'),
                'rechargeid'         => $updateRefIdMain->TransactionDetailID,
                'operator'           => $billerName,
                'offeramount'        => $offerAmount,
                'cashbackPercentage' => $offerAmount,
                'mobileNumber'       => $mobileNumber,
                'balanceamount'      => $returnData['CurrentAmount'],
                'supportURL'         => "violamoney.com",
            )
        );

        if ( $statusRecharge == 'Failed' )
        {
            $templateData['templateKey'] = 'VW017';
        }

        if ( $subCategory == 'PREPAID DTH' )
        {
            $templateData['templateKey'] = 'VW020';

            if ( $statusRecharge == 'Failed' )
            {
                $templateData['templateKey'] = 'VW021';
            }
        }

        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        /*
          if ( ($statusRecharge == 'Success' ) )
          {
          $templateData['templateKey'] = 'VW018';
          if ( $subCategory == 'PREPAID DTH' )
          {
          $templateData['templateKey'] = 'VW022';
          }

          \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
          } */

        // Communication End

        $output['is_error']    = FALSE;
        $output['res_data']    = $returnData;
        $output['display_msg'] = array( 'info' => '' );
        return json_encode($output);
    }

    public function getEventId($eventCode)
    {
        return $selEventGetId = \App\Models\Paymentproductevent::select('PaymentProductEventID')
                        ->where('EventCode', $eventCode)->first();
    }

    public function getProductId($eventCode)
    {
        return $selEventGetId = \App\Models\Paymentsproduct::select('PaymentsProductID')
                        ->where('ProductName', $eventCode)->first();
    }

    public function getBillerCategory($eventCode)
    {
        return $selEventGetId = \App\Models\Billerlist::select('SubCategory')
                        ->where('BillerId', $eventCode)->first();
    }

    // update recharge details
    public function insertRechargeDetails($partyId, $txnId, $status, $subNumber, $planId, $amount, $paymentId, $billdeskTxnId, $billId, $billerName, $billCategory, $billerType, $valid)
    {
        // insert user recharge details
        $insertUserRHdetails                        = new \App\Models\Userrechargedetail;
        $insertUserRHdetails->UserWalletID          = $partyId;
        $insertUserRHdetails->TransactionDetailID   = $txnId;
        $insertUserRHdetails->RechargeCategory      = $billCategory;
        $insertUserRHdetails->RechargeType          = $billerType;
        $insertUserRHdetails->SubscriberNumber      = $subNumber;
        $insertUserRHdetails->SubScriberName        = $billerName;
        $insertUserRHdetails->UseroperatorfeeplanID = $planId;
        $insertUserRHdetails->Amount                = $amount;
        $insertUserRHdetails->RechargeCategory      = $this->getBillerCategoryList($billId)['SubCategory'];
        $insertUserRHdetails->Status                = $status;
        $insertUserRHdetails->Valid                 = $valid;
        $insertUserRHdetails->CreatedBy             = 'User';
        $insertUserRHdetails->PaymentID             = ( ! empty($paymentId)) ? $paymentId : 0;
        $insertUserRHdetails->BillDeskTransactionID = ( ! empty($billdeskTxnId)) ? $billdeskTxnId : 0;
        $insertUserRHdetails->save();
    }

    // get bIller category
    public function getBillerCategoryList($billId)
    {
        $selBillerCateg = \App\Models\Billerlist::select('SubCategory', 'BillerType', 'BillerName')->where('BillerId', $billId)->first();
        return (empty($selBillerCateg)) ? TRUE : $selBillerCateg;
    }

    /**
     * U03005 Validate Recharge Request Details request message 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function rechargeValidate(\Illuminate\Http\Request $request)
    {
        $output        = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateRecharge($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }
        $partyId        = $request->input('partyId');
        $mobileNumber   = $request->input('mobileNumber');
        $billerId       = $request->input('billerId');
        $rechargeAmount = $request->input('rechargeAmount');
        $circleName     = $request->input('circleId');

        // check biller info 
        $billInfoCheck = $this->validateBillerInfo($request);
        if ( ! is_bool($billInfoCheck) )
        {
            $output['is_error']    = TRUE;
            $output['display_msg'] = array( 'mobileNumber' => $billInfoCheck );
            return $output;
        }

        $insertFeePlan  = \App\Models\Operatorfeeplan::
                select('PlanID', 'FeePlanID', 'Operator', 'Circle', 'CategoryName', 'Mrp', 'TalkTime', 'ServiceTax', 'ProcessingFees', 'AccessFees', 'Validity', 'Remarks', 'Type', 'Description')
                ->where('Circle', $circleName)
                ->where('Mrp', $rechargeAmount)
                ->where('Operator', $billerId)
                ->first();
        $rechargePlanId = 0;
        if ( count($insertFeePlan) > 0 )
        {
            $rechargePlanId = $insertFeePlan->FeePlanID;
        }

        $sourceid      = config('vwallet.sourceId');
        $txnId         = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
        $dateTime      = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode   = 'U03005';
        $superCashData = \App\Libraries\OfferRules::getSuperCash($partyId, $rechargeAmount);

        $arrayparams      = array(
            'MessageCode'             => $messageCode,
            'TraceID'                 => $txnId,
            'SourceID'                => $sourceid,
            'TimeStamp'               => $dateTime,
            // 'UserID'                  => config('vwallet.billdeskUserId'),
            'UserID'                  => $partyId,
            'CustomerID'              => $partyId,
            'RechargeBillerAccountID' => 'NA',
            'BillerID'                => $billerId,
            'Authenticator1'          => $mobileNumber,
            'Authenticator2'          => 'NA',
            'Authenticator3'          => 'NA',
            'RechargeAmount'          => $rechargeAmount,
            'Filler1'                 => 'NA',
            'Filler2'                 => 'NA',
            'Filler3'                 => config('vwallet.billdeskFiller3'),
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);
        //store log file       
        //$insertData = json_encode($arrayparams);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $arrayparams);
        // url for required api
        $url        = config('vwallet.billdeskApiUrl') . $stringKeys;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $txnId,
            'thirdPartyName' => 'Biller Validate',
            'thirdPartyCall' => $messageCode,
            'referenceId'    => 0,
            'request'        => $arrayparams,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);
        // get return response from curl method
        $response   = \App\Libraries\BilldeskHelper::billDeskGETCURL($url, $logId);

        if ( $response === FALSE )
        {
            $output['display_msg'] = array( 'info' => 'Seems operator is taking more time. Try again later.' );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Biller Validate',
                'response'       => $response,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }
        $responseArray = explode('~', $response);
        $responseKeys  = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'RechargeBillerAccountID',
            'BillerID',
            'Authenticator1',
            'Authenticator2',
            'Authenticator3',
            'PaymentID',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );
        if ( count($responseKeys) != count($responseArray) )
        {
            $logRequest         = array(
                'thirdPartyName' => 'Biller Validate',
                'response'       => $responseArray,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);
            $output['res_data'] = $responseArray;
            return json_encode($output);
        }

        $combine    = array_combine($responseKeys, $responseArray);
        $logRequest = array(
            'thirdPartyName' => 'Biller Validate',
            'response'       => $combine,
            'status'         => 'Success'
        );
        \App\Libraries\LogHelper::createLog($logRequest, $logId);

        $combine['rechargePlanId']       = $rechargePlanId;
        $output['is_error']              = FALSE;
        $output['res_data']              = $combine;
        $output['res_data']['superCash'] = ($superCashData['res_data']) ? $superCashData['res_data']['superCash'] : 0;
        return json_encode($output);
    }

    /*
     *  billDesk - This method is used to connect with bill desk api to get billers data
     *  
     * @param $request 
     * 
     *  @return string
     */

    public function billpayValidate(\Illuminate\Http\Request $request)
    {
        $httpMethod  = 'POST';
        $Apiendpoint = '/customers/CUSTTEST001/billpay/validate';
        echo $txnId       = config('vwallet.billdeskTxnId') . '----'; // gen of transaction Id
        echo $dateTime    = config('vwallet.bdTimeStamp') . '----';  // gen of dateTime
        // Genartion of HMAC key form library
        echo $hmacKey     = \App\Libraries\BilldeskHelper::generateHMAC($request, $Apiendpoint, $httpMethod, $txnId, $dateTime);

        die();
        // url for required api
        $url = "https://uat.billdesk.com/mpayv2/mpay/v2_1/VOL/customers/CUSTTEST001/billpay/validate";

        // passing headers to required api
        $headersArray = array(
            "accept: application/json",
            "authorization: HMACSignature voluat:$hmacKey",
            "bd-timestamp: $dateTime",
            "bd-traceid: $txnId",
            "cache-control: no-cache",
            "content-type: application/json"
        );


        // get return response from curl method
        return \App\Libraries\BilldeskHelper::billDeskPOSTCURL($url, $httpMethod, $headersArray);
    }

    /*
     *  circleList - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function circleList(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $selectColumns = array(
            'BillerId',
            'BillerName',
        );

        $dbResults = \App\Models\Billerlist::select($selectColumns);

        if ( $dbResults )
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
            $output_arr['res_data']            = $dbResults->get()->toArray();
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }

    /*
     *  circleList - This method returns all the states list.
     * @param $request 
     *  @return string
     */

    public function operatorsList(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $selectColumns = array(
            'Circle_Id',
            'Master',
        );

        $dbResults = \App\Models\Circlemaster::select($selectColumns);

        if ( $dbResults )
        {
            $output_arr['is_error']            = FALSE;
            $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
            $output_arr['res_data']            = $dbResults->get()->toArray();
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }

    /**
     * Cron Jobs
     * U03085 JIO Plan Details Request
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function getJioPlans(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateJioPlans($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }

        $sourceid         = config('vwallet.sourceId');
        $txnId            = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
        $dateTime         = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode      = 'U03085';
        $mobileNumber     = $request->input('mobileNumber');
        $billerId         = $request->input('billerId');
        $arrayparams      = array(
            'MessageCode'  => $messageCode,
            'TraceID'      => $txnId,
            'SourceID'     => $sourceid,
            'TimeStamp'    => $dateTime,
            'MobileNumber' => $mobileNumber,
            'BillerId'     => $billerId,
            'ExtraField1'  => 'NA',
            'ExtraField2'  => 'NA',
            'ExtraField3'  => 'NA',
            'Filler1'      => 'NA',
            'Filler2'      => 'NA',
            'Filler3'      => 'NA',
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

        // url for required api
        $url = config('vwallet.billdeskApiUrl') . $stringKeys;

        // get return response from curl method
        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url);
        if ( $response === FALSE )
        {
            $output['display_msg'] = array( 'info' => 'Seems operator is taking more time. Try again later.' );
            $output['res_data']    = array();
            return $output;
        }

        if ( $response != strip_tags($response) )
        {
            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }

        $rechargeDetails = json_decode($response);
        $coutuntRecharge = count($rechargeDetails->RechargeDetails);

        // return response when data is empty
        if ( $coutuntRecharge <= 0 )
        {
            $output_arr['display_msg'] = array( 'info' => 'No Data Found' );
            return json_encode($output_arr);
        }
        $plansList = array();
        // checking opertor name exists or not
        if ( $rechargeDetails->ErrorCode == 0 )
        {
            if ( isset($rechargeDetails->RechargeDetails) && ($rechargeDetails->RechargeDetails !== NULL) )
            {
                foreach ( $rechargeDetails->RechargeDetails as $rechargekey => $mainValuerecharge1 )
                {
                    foreach ( $mainValuerecharge1 as $mainValuerecharge )
                    {
                        $plansList[$rechargekey][] = array(
                            'Mrp'         => $mainValuerecharge->rechargeAmout,
                            'TalkTime'    => $mainValuerecharge->talktime,
                            'Validity'    => $mainValuerecharge->validity,
                            'Description' => $mainValuerecharge->description,
                            'FeePlanID'   => $mainValuerecharge->rec_plan_id,
                        );
                    }
                }
            }
        }

        $output['is_error'] = FALSE;
        $output['res_data'] = $plansList;
        return json_encode($output);
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validateJioPlans($request)
    {
        $messages = [];

        $validate['mobileNumber'] = 'required|numeric|digits:10';
        $validate['billerId']     = 'required';
        $validate_response        = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validateRecharge($request)
    {
        $messages                   = [];
        //$validate['command'] = 'required|in:confirmOrder,cancelOrder,refundOrder,orderStatusTracker,orderLookup,getPendingOrders';
        $validate['partyId']        = 'required|numeric';
        $validate['mobileNumber']   = 'required|numeric';
        $validate['rechargeAmount'] = 'required|numeric';
        $validate['billerId']       = 'required';
        $validate['circleId']       = 'required|numeric';
        $validate_response          = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validateRechargePayment($request)
    {
        $messages                     = [];
        //$validate['command'] = 'required|in:confirmOrder,cancelOrder,refundOrder,orderStatusTracker,orderLookup,getPendingOrders';
        $validate['partyId']          = 'required|numeric';
        $validate['mobileNumber']     = 'required|numeric';
        $validate['billerId']         = 'required';
        $validate['paymentId']        = 'required';
        $validate['paymentChannelId'] = 'required|in:ATM,MOBILE,NETB,KIOSK';
        $validate['rechargeAmount']   = 'required|numeric||min:0.1';
        $validate['paymentType']      = 'required|in:RNP,PNY';
        $validate['bankRefNo']        = 'required';
        $validate['bankMessage']      = 'required';
        $validate['rechargePlanId']   = 'required';
        if ( trim($request->input('promoCode')) != NULL )
        {
            $validate['paymentProductId'] = 'required|numeric';
            $validate['offerCategoryId']  = 'required|string';
        }
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validateRechargeQuery($request)
    {
        $messages                  = [];
        $validate['partyId']       = 'required|numeric';
        $validate['transactionId'] = 'required';
        //}
        $validate_response         = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    // code for get Bill for services providers 

    public function getBillPaymentDetails(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validategetBilldetails($request, $type          = 'getBill');
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }
        $partyId     = $request->input('partyId');
        $arrayparams = \App\Libraries\BilldeskHelper::getBillPayment($request);

        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);
        $stringKeys       = $implodeArraykeys . '~' . strtoupper($hmacKey);

        // url for required api
        $url = config('vwallet.billdeskApiUrl') . $stringKeys;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $arrayparams['TraceID'],
            'thirdPartyName' => 'Bill Fech',
            'thirdPartyCall' => $arrayparams['MessageCode'],
            'referenceId'    => 0,
            'request'        => $arrayparams,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);

        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url, $logId);

        if ( $response === FALSE )
        {
            $output['display_msg'] = array( 'info' => 'Seems operator is taking more time. Try again later.' );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Fech',
                'response'       => $response,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }
//\Log::debug($response);
//echo $response; exit;
        $responseArray = explode('~', $response);
        $responseKeys  = \App\Libraries\BilldeskHelper::responseKeysU07009($request);

        if ( count($responseKeys) != count($responseArray) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Fech',
                'response'       => $responseArray,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['res_data'] = $responseArray;
            return json_encode($output);
        }

        $outputArray = \App\Libraries\BilldeskHelper::responseCombineU07009($responseKeys, $responseArray);

        $logRequest = array(
            'thirdPartyName' => 'Bill Fech',
            'response'       => $outputArray,
            'status'         => 'Success'
        );

        \App\Libraries\LogHelper::createLog($logRequest, $logId);

        if ( (trim($outputArray['BillAmount']) == 0) || ($outputArray['BillAmount'] == 'NA' ) )
        {
            $output['display_msg'] = array( 'info' => 'You have either entered invalid details or bill is not payable now' );
            $output['res_data']    = array();
            return $output;
        }

        // done by raghava If you have any change please ask me
        // getting response data from ResponseAuthenticators from Database
        $billerIdMain         = $request->input('billerId');
        $responseDataArranged = \App\Libraries\BilldeskHelper::arrangeResponseDataInorder($billerIdMain,$outputArray);

        // showing all arranged data with Db and Response 
        $outputArray['billerInformation'] = $responseDataArranged;


        $output['is_error']              = FALSE;
        $output['res_data']              = $outputArray;
        $rechargeAmount                  = $outputArray['BillAmount'];
        $superCashData                   = \App\Libraries\OfferRules::getSuperCash($partyId, $rechargeAmount);
        $output['res_data']['superCash'] = ($superCashData['res_data']) ? $superCashData['res_data']['superCash'] : 0;

        return json_encode($output);
    }

    // code to request payment
    // code for test electricity bills
    public function makeBillpayment(\Illuminate\Http\Request $request)
    {

        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validategetBilldetails($request, $type          = 'makepayment');
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }

        $partyId        = $request->input('partyId');
        $rechargeAmount = $request->input('rechargeAmount');
        $partyInfo      = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ( $partyInfo === FALSE )
        {
            $output['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return response()->json($output);
        }


        $billerId = $request->input('billerId');

        $amount                 = $request->input('rechargeAmount');
        $entityId               = 1;
        $transStaus             = config('vwallet.transactionStatus.pending');
        //trans_type whether it is WW or WB
        $paymentsProductEventID = $this->getEventId($billerId . 'INI')['PaymentProductEventID'];
        $mainProdId             = $this->getProductId($billerId . 'INI')['PaymentsProductID'];
        $bankRefId              = ( ! empty($request->input('bankRefNo'))) ? $request->input('bankRefNo') : 0;
        $payementId             = ( ! empty($request->input('paymentId'))) ? $request->input('paymentId') : 0;
        $sesionId               = \App\Libraries\TransctionHelper::getUserSessionId($request);

        $offerData = NULL;
        if ( trim($request->input('promoCode')) != NULL )
        {
            $requestArray['aplicableChannel'] = $request->header('device-type');
            $requestArray['promoCode']        = $request->input('promoCode');
            $requestArray['paymentProductId'] = $request->input('paymentProductId');
            //$requestArray['offerCategoryId'] = 0;
            // $requestArray['offerUserTypeId'] = 1;
            //$requestArray['benefitCategoryId'] = 0;
            if ( $request->input('offerCategoryId') != NULL )
            {
                $requestArray['offerCategoryId'] = $request->input('offerCategoryId');
            }
            if ( $request->input('offerUserTypeId') != NULL )
            {
                $requestArray['offerUserTypeId'] = $request->input('offerUserTypeId');
            }

            $requestArray['amount']  = $request->input('rechargeAmount');
            $requestArray['partyId'] = $request->input('partyId');
            $verifyJsonData          = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
            $verifyData              = json_decode($verifyJsonData);
            if ( $verifyData->is_error == TRUE )
            {
                return $verifyJsonData;
            }
            $offerData  = $verifyData->res_data;
            $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
            if ( ! empty($pamentInfo) )
            {
                $offerPamentInfo = array( 'offerPamentInfo' => $pamentInfo );
                $request->request->add($offerPamentInfo);
            }
        }
        if ( $request->input('superCash') > 0 )
        {
            $requestArray         = array( 'partyId' => $request->input('partyId'), 'amount' => $request->input('rechargeAmount') );
            $superCashPaymentInfo = \App\Libraries\OfferRules::getSuperCashPaymentInfo($requestArray);
            if ( ! empty($superCashPaymentInfo) )
            {
                $superCashInput = array( 'superCashPamentInfo' => $superCashPaymentInfo );
                $request->request->add($superCashInput);
            }
        }

        $partyTransLimit   = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductEventID);


        // fee calculation 
        $fee       = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $entityId, $partyId);
        $transInfo = $this->_getTransactionInfo($fee, $request);
        if ( $request->input('checkout') === 'FALSE' )
        {
            $output['is_error']    = FALSE;
            $output['display_msg'] = array( 'info' => trans('messages.requestSuccessfull') );
            $output['res_data']    = $transInfo;
            return response()->json($output);
        }
        if ( $partyInfo->CurrentAmount < $transInfo['totalAmount'] )
        {
            $output['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output;
        }

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, TRUE, $transStaus, $sesionId);
        $orderId  = $transRes['response']['TransactionDetails']['TransactionOrderID'];

        //\DB::enableQueryLog();
        $updateRefIdMain                  = \App\Models\Transactiondetail::select('TransactionDetailID')->where('TransactionOrderID', $orderId)->first();
        //    print_r(\DB::getQueryLog());
        $updateTxnDetails                 = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
        $updateTxnDetails->RespPGTrackNum = ( ! empty($payementId)) ? $payementId : 0;
        $updateTxnDetails->RespBankRefNum = ( ! empty($bankRefId)) ? $bankRefId : 0;
        $updateTxnDetails->save();

        $sourceid = config('vwallet.sourceId');
        $txnId    = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id

        $dateTime         = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode      = 'U05003';
        $arrayparams      = array(
            'MessageCode'      => $messageCode,
            'TraceID'          => trim($txnId),
            'SourceID'         => trim($sourceid),
            'TimeStamp'        => $dateTime,
            'UserID'           => config('vwallet.billdeskUserId'),
            'CustomerID'       => trim($request->input('partyId')),
            'RechargeBillerId' => trim($request->input('billerId')),
            'BillerAccountId'  => 'NA',
            'ShortName'        => 'homemobi',
            'Authenticators'   => preg_replace('/\s+/', '', $request->input('authenticator1')) . '!' . preg_replace('/\s+/', '', $request->input('authenticator2')) . '!' . preg_replace('/\s+/', '', $request->input('authenticator3')) . '!' . preg_replace('/\s+/', '', $request->input('filler3')),
            'BillId'           => 'NA',
            'BillDueDate'      => trim($request->input('billDuedate')),
            'BillDate'         => trim($request->input('billdate')),
            'BillNumber'       => trim($request->input('billNumber')),
            'PaymentAmount'    => trim($request->input('rechargeAmount')),
            'PaymentType'      => 'PNY',
            'PaymentId'        => 'NA',
            'BankRefNo'        => trim($txnId),
            // 'MessageDetail'    => '192.168.100.83!b126fa8bb6adec5b!IOS!VIOLAWALLET',
            'MessageDetail'    => trim($request->input('filler2')),
            'CardType'         => 'WP!NA',
            'CardNumber'       => 'NA',
            'Filler1'          => config('vwallet.billdeskFiller1'),
            'Filler2'          => config('vwallet.billdeskFiller2'),
            'Filler3'          => config('vwallet.billdeskFiller3'),
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

        //store log file       
        $insertData = json_encode($arrayparams);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $arrayparams);
        // url for required api
        $url        = config('vwallet.billdeskApiUrl') . $stringKeys;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $txnId,
            'thirdPartyName' => 'Bill Payment',
            'thirdPartyCall' => $messageCode,
            'referenceId'    => 0,
            'request'        => $arrayparams,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);

        // get return response from curl method
        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url, $logId);

        if ( $response === FALSE )
        {
            $output['display_msg'] = array( 'info' => "Operator is taking more time than expected. Don't worry if the transaction fails, money will be back to your wallet." );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Payment',
                'response'       => $response,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return response()->json($output);
        }

        $responseArray = explode('~', $response);

        $responseKeys = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'TransactionId',
            'PaymentStatus',
            'Reason',
            'Filler1',
            'Filler2',
            'BBPSReferencenumber',
            'Checksum'
        );
        if ( count($responseKeys) != count($responseArray) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Payment',
                'response'       => $responseArray,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['res_data'] = $responseArray;
            return json_encode($output);
        }

        $combine = array_combine($responseKeys, $responseArray);

        $logRequest = array(
            'thirdPartyName' => 'Bill Desk',
            'response'       => $combine,
            'status'         => 'Success'
        );
        \App\Libraries\LogHelper::createLog($logRequest, $logId);

        // calling billdesk query starts
        $messageCode        = 'U03011';
        $transactionIDquery = $combine['TransactionId'];

        $arrayparamsQuery      = array(
            'MessageCode'   => $messageCode,
            'TraceID'       => $txnId,
            'SourceID'      => $sourceid,
            'TimeStamp'     => $dateTime,
            'UserID'        => $partyId,
            'CustomerId'    => $partyId,
            'TransactionID' => $transactionIDquery, //if Source ID is ABC and Bank Ref number is 123456, then Transaction ID will be ABC123456                       
            'Filler1'       => 'NA',
            'Filler2'       => 'NA',
            'Filler3'       => 'NA',
        );
        $implodeArraykeysQuery = implode('~', $arrayparamsQuery);
        // Genartion of HMAC key form library
        $hmacKeyQury           = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeysQuery);

        $stringKeysQuery = $implodeArraykeysQuery . '~' . strtoupper($hmacKeyQury);

        // url for required api
        $urlQuery = config('vwallet.billdeskApiUrl') . $stringKeysQuery;

        $logRequest = array(
            'partyId'        => $partyId,
            'requestId'      => $txnId,
            'thirdPartyName' => 'Bill Pay Confirm',
            'thirdPartyCall' => $messageCode,
            'referenceId'    => 0,
            'request'        => $arrayparamsQuery,
            'status'         => 'Initiated'
        );
        $logId      = \App\Libraries\LogHelper::createLog($logRequest);

        // get return response from curl method
        $responseQuery = \App\Libraries\BilldeskHelper::billDeskGETCURL($urlQuery, $logId);

        if ( $responseQuery === FALSE )
        {
            $output['display_msg'] = array( 'info' => "Operator is taking more time than expected. Don't worry if the transaction fails, money will be back to your wallet." );
            $output['res_data']    = array();
            return $output;
        }

        // check reponse if server is sending html reponse that their sever is down
        if ( $responseQuery != strip_tags($responseQuery) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Pay Confirm',
                'response'       => $responseQuery,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['is_error']            = TRUE;
            $output['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return response()->json($output);
        }
        $responseArrayQuery = explode('~', $responseQuery);

        $responseKeysQury = array(
            'MessageCode',
            'TraceID',
            'SourceID',
            'TimeStamp',
            'UserID',
            'CustomerID',
            'Valid',
            'ErrorCode',
            'ErrorMessage',
            'RechargeBillerId',
            'Authenticator1',
            'Authenticator2',
            'Authenticator3',
            'TransactionID',
            'PaymentTransActionDate',
            'RechargeAmount',
            'PaymentID',
            'PaymentStatus',
            'RechargeStatus',
            'RechargeStatusDescription',
            'Filler1',
            'Filler2',
            'Filler3',
            'Checksum'
        );
        if ( count($responseKeysQury) != count($responseArrayQuery) )
        {
            $logRequest = array(
                'thirdPartyName' => 'Bill Pay Confirm',
                'response'       => $responseQuery,
                'status'         => 'Failed'
            );
            \App\Libraries\LogHelper::createLog($logRequest, $logId);

            $output['res_data'] = $responseArrayQuery;
            return json_encode($output);
        }
        $combinequery = array_combine($responseKeysQury, $responseArrayQuery);

        $logRequest = array(
            'thirdPartyName' => 'Bill Pay Confirm',
            'response'       => $combinequery,
            'status'         => 'Success'
        );
        \App\Libraries\LogHelper::createLog($logRequest, $logId);

        // recharge Query Ends

        if ( $combine['Valid'] == 'Y' && $combine['Filler1'] == 'SUCCESS' && $combinequery['Valid'] == 'Y' && $combinequery['RechargeStatus'] == 'SUCCESS' )
        {
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = 'Success';
            $updateTxnDetails->RespBankRefNum    = trim($txnId);
            $updateTxnDetails->RespPGTrackNum    = $combinequery['PaymentID'];
            $updateTxnDetails->save();
            if ( $request->input('offerPamentInfo') != NULL )
            {
                $offerPamentInfo = $request->input('offerPamentInfo');
                \App\Libraries\OfferBussness::addbenefitTrack($updateRefIdMain->TransactionDetailID);
            }
            $superCashPaymentInfo = $request->input('superCashPamentInfo');
            if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 )
            {
                //if(!empty($superCashPaymentInfo) && $transStausMain == 'Success') {
                $trackRequest                        = [];
                $trackRequest['partyId']             = $partyId;
                $trackRequest['amount']              = $superCashPaymentInfo['offerAmount'];
                $trackRequest['transactionDetailId'] = $updateRefIdMain->TransactionDetailID;
                \App\Libraries\OfferBussness::updateUsedAmount($trackRequest);
            }
            $statusRecharge = 'Success';
            $msg            = 'Billpayment Done successfully with Rs. ' . $rechargeAmount;
            $templateKey    = 'VW080';
        }
        else if ( $combinequery['RechargeStatus'] == 'PENDING' )
        {
            $statusRecharge                      = 'Pending';
            $msg                                 = 'Pending to Pay Billpayment';
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = 'Pending';
            $updateTxnDetails->RespBankRefNum    = trim($txnId);
            $updateTxnDetails->RespPGTrackNum    = $combinequery['PaymentID'];
            $updateTxnDetails->save();
            $templateKey                         = 'VW082';
        }
        else
        {
            $statusRecharge                      = 'Failed';
            $msg                                 = 'Failed to Pay Billpayment';
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->TransactionStatus = 'Failed';
            $updateTxnDetails->RespBankRefNum    = trim($txnId);
            $updateTxnDetails->RespPGTrackNum    = $combinequery['PaymentID'];
            $updateTxnDetails->save();
            \App\Libraries\PaymentRefund::paymentRefund($updateRefIdMain->TransactionDetailID);
            $templateKey                         = 'VW081';
        }

        // insert into txns
        // Insert into Billdesk txn 

        $authdata      = $request->input('authenticator1') . '!' . $request->input('authenticator2') . '!' . $request->input('authenticator3') . '!' . $request->input('filler3');
        $getAuthObject = $this->getBillerAuthenticators($billerId, $authdata);

        $respData = \App\Libraries\BilldeskHelper::userBillerTransactions($arrayparams, $combine, $request, $updateRefIdMain->TransactionDetailID, $request->input('authenticator1'), $combine['BBPSReferencenumber'], $getAuthObject, $combine['Valid']);

        $updatetxneentLog           = \App\Models\Transactioneventlog::find($this->getTxnNotifLog($updateRefIdMain->TransactionDetailID)['TransactionEventDetailID']);
        $updatetxneentLog->Comments = 'Paid For ' . $request->input('authenticator1') . '(' . \App\Libraries\TransctionHelper::getBilldeskName($billerId) . ')';
        $updatetxneentLog->save();

        // Communication Start

        $billerDetails  = \App\Models\Billdeskbiller::where('BillerID', '=', $combinequery['RechargeBillerId'])->first();
        $billerFullName = $combinequery['RechargeBillerId'];
        if ( $billerDetails )
        {
            $billerFullName = $billerDetails->BillerName;
        }
        $templateData = array(
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => $templateKey,
            'senderEmail'    => $partyInfo->EmailID,
            'senderMobile'   => $partyInfo->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullname'      => ($partyInfo->FirstName) ? $partyInfo->FirstName : "",
                'amount'        => $rechargeAmount,
                'rechargedate'  => date('d/m/Y'),
                'date'          => date('d/m/Y'),
                'time'          => date('H:i A'),
                'transactionid' => $updateRefIdMain->TransactionDetailID,
                'billerName'    => $billerFullName,
                'customerid'    => $combinequery['Authenticator1'],
                'supportURL'    => "violamoney.com",
            )
        );
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);

        //
//store log file
        //$combine['userTxnId'] = $respData['res_data']['transactionDetailId'];
        $insertData            = json_encode($combine);
        //\App\Libraries\Vwallet::BilldeskAPIMessages($insertData, $combine);
        $output['is_error']    = FALSE;
        $output['display_msg'] = array( 'info' => '' );
        $userBalanceInfo       = \App\Libraries\TransctionHelper::getWalletInfo($partyId);

        $combine['TransactionID']          = $orderId;
        $combine['txnStatus']              = $combine['Filler1'];
        $combine['amount']                 = $request->input('rechargeAmount');
        //$combine['AvailableBalance']       = (number_format((float) $this->getuserBalance($partyId)['AvailableBalance'], 2, '.', ''));
        //$combine['CurrentBalance']         = (number_format((float) $this->getuserBalance($partyId)['CurrentAmount'], 2, '.', ''));
        $combine['txnMsg']                 = 'Paid For ' . $request->input('authenticator1') . '(' . \App\Libraries\TransctionHelper::getBilldeskName($billerId) . ')';
        $billerName                        = \App\Libraries\TransctionHelper::getBilldeskName($billerId);
        $combine['RechargeBillerName']     = ( ! empty($billerName)) ? $billerName : $billerId;
        $combine['BillerId']               = $billerId;
        $combine['PaymentTransActionDate'] = $combinequery['PaymentTransActionDate'];
        $combine['billdeskPaymentId']      = $combinequery['PaymentID'];

        $combine['MessageCode']               = $combinequery['MessageCode'];
        $combine['TraceID']                   = $combinequery['TraceID'];
        $combine['SourceID']                  = $combinequery['SourceID'];
        $combine['TimeStamp']                 = $combinequery['TimeStamp'];
        $combine['UserID']                    = $combinequery['UserID'];
        $combine['CustomerID']                = $combinequery['CustomerID'];
        $combine['Valid']                     = $combinequery['Valid'];
        $combine['ErrorCode']                 = $combinequery['ErrorCode'];
        $combine['ErrorMessage']              = $combinequery['ErrorMessage'];
        $combine['RechargeBillerId']          = $combinequery['RechargeBillerId'];
        $combine['Authenticator1']            = $combinequery['Authenticator1'];
        $combine['Authenticator2']            = $combinequery['Authenticator2'];
        $combine['Authenticator3']            = $combinequery['Authenticator3'];
        $combine['PaymentTransActionDate']    = $combinequery['PaymentTransActionDate'];
        $combine['RechargeAmount']            = $combinequery['RechargeAmount'];
        $combine['PaymentID']                 = $combinequery['PaymentID'];
        $combine['PaymentStatus']             = $combinequery['PaymentStatus'];
        $combine['RechargeStatus']            = $combinequery['RechargeStatus'];
        $combine['RechargeStatusDescription'] = $combinequery['RechargeStatusDescription'];
        $combine['Filler1']                   = $combinequery['Filler1'];
        $combine['Filler2']                   = $combinequery['Filler2'];
        $combine['Filler3']                   = $combinequery['Filler3'];
        $combine['FeeAmount']                 = $transRes['response']['TransactionDetails']['FeeAmount'];
        $combine['TaxAmount']                 = $transRes['response']['TransactionDetails']['TaxAmount'];
        $combine['WalletAmount']              = $transRes['response']['TransactionDetails']['WalletAmount'];
        $combine['DiscountAmount']            = $transRes['response']['TransactionDetails']['DiscountAmount'];
        $combine['CashbackAmount']            = $transRes['response']['TransactionDetails']['CashbackAmount'];

        $returnData = array_merge($combine, $userBalanceInfo);

        $remindData = \App\Libraries\TransctionHelper::remindTransactionInfo($request->input('partyId'), $request->input('authenticator1'), $request->input('billerId'));

        $output['res_data'] = array_merge($returnData, $remindData);
        return json_encode($output);
    }

    public function getBillerAuthenticators($billerId, $authdata)
    {
        $billerAuth = \App\Models\Billdeskbiller::select('ResponseAuthenticators')
                        ->where('BillerID', $billerId)->first();
        $arrayData  = array();

        if ( ! empty($billerAuth) )
        {
            $explodeResponseAuth = explode("#", $billerAuth['ResponseAuthenticators']);
            $explodeAuthdata     = explode('!', $authdata);
//    taking input from response of get bill details parameters

            $countNew = count($explodeResponseAuth);
            for ( $i = 0; $i < $countNew; $i ++ )
            {
                if ( ! empty($explodeResponseAuth[$i]) )
                {
                    $arrayData[$explodeResponseAuth[$i]] = $explodeAuthdata[$i];
                }
            }
            return json_encode($arrayData);
        }
        return json_encode($arrayData);
    }

    public function getTxnNotifLog($txnDetailsId)
    {
        $getNotifTxnid = \App\Models\Transactioneventlog::select('TransactionEventDetailID')->where('TransactionDetailID', $txnDetailsId)->first();
        return ( ! empty($getNotifTxnid)) ? $getNotifTxnid : TRUE;
    }

    // code for user balance
    public function getuserBalance($partyId)
    {
        $sqlFetch = \App\Models\Walletaccount::select('AvailableBalance', 'CurrentAmount')
                        ->where('UserWalletId', $partyId)->first();
        return (empty($sqlFetch)) ? '' : $sqlFetch;
    }

    public function getbillerName($billerId)
    {
        $sqlFetch = \App\Models\Billdeskbiller::select('BillerName')
                        ->where('BillerID', $billerId)->first();
        return (empty($sqlFetch)) ? '' : $sqlFetch;
    }

    public function getbillerNameRecharge($billerId)
    {
        $sqlFetch = \App\Models\Billerlist::select('BillerName')
                        ->where('BillerID', $billerId)->first();
        return (empty($sqlFetch)) ? '' : $sqlFetch;
    }

    public function getBillerValidationRegx($billerId)
    {
        $sqlFetch = \App\Models\Billdeskbiller::select('KeyAuthenticaotrs', 'Ref1Label', 'Ref1RegEx', 'Ref1Message', 'Ref2Label', 'Ref2RegEx', 'Ref2Message', 'Ref3Label', 'Ref3RegEx', 'Ref3Message')
                        ->where('BillerID', $billerId)->first();
        return (empty($sqlFetch)) ? '' : $sqlFetch;
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validategetBilldetails($request, $type)
    {
        $messages             = [];
        //$validate['command'] = 'required|in:confirmOrder,cancelOrder,refundOrder,orderStatusTracker,orderLookup,getPendingOrders';
        $validate['partyId']  = 'required|numeric';
        $validate['billerId'] = 'required';

        //   if ( $request->input('typeProd') == 'broadband' && $request->input('billerId') == 'ACTOB' && $type == 'getBill' )
        if ( $type == 'getBill' )
        {

            if ( $request->input('billerId') != '' )
            {
                $getBillvalidDetails = $this->getBillerValidationRegx($request->input('billerId'));


                if ( ! empty($getBillvalidDetails) )
                {
                    $authenticatorsMain = explode('#', $getBillvalidDetails['KeyAuthenticaotrs']);
                    $i                  = 1;
                    foreach ( $authenticatorsMain as $mainAuth )
                    {

                        if ( $request->input('billerId') == 'MSEBOB' )
                        {
                            //$auth2Input = 
                            $haystack                   = '';
                            $needle                     = '';
                            $validate['authenticator1'] = array( "required" );
                            $validate['authenticator2'] = array( "required" );


                            if ( $request->input('authenticator2') != '' && $i == 2 )
                            {
                                $haystack     = $getBillvalidDetails['Ref' . $i . 'RegEx'];
                                $needle       = $request->input('authenticator2');
                                $replaceData1 = str_replace("-", "", str_replace("/", "", $haystack));


                                $explodeData  = explode('~', $replaceData1);
                                $arrayRequest = ( array ) $request->input('authenticator2');

                                if ( ! in_array($explodeData, $arrayRequest) )
                                {
                                    $messages['authenticator' . $i . '.regex'] = $getBillvalidDetails['Ref' . $i . 'Message'];
                                }
                            }
                        }
                        else
                        {

                            $validate['authenticator' . $i]            = array( "required", "regex:" . $getBillvalidDetails['Ref' . $i . 'RegEx'] );
                            $messages['authenticator' . $i . '.regex'] = $getBillvalidDetails['Ref' . $i . 'Message'];
                        }
                        $i ++;
                    }
                }
            }
        }

        if ( $type == 'makepayment' )
        {

            $validate['rechargeAmount'] = 'required|numeric|min:0.1';
            $validate['authenticator1'] = 'required';
            $validate['authenticator2'] = 'required';
            $validate['authenticator3'] = 'required';
            $validate['billDuedate']    = 'required';
            $validate['billdate']       = 'required';
            $validate['billNumber']     = 'required';
            $validate['filler1']        = 'required';
            $validate['filler2']        = 'required';
            $validate['filler3']        = 'required';
        }

        if ( $type == 'reconfile' )
        {

            $validate['date'] = 'required';
        }

        if ( trim($request->input('promoCode')) != NULL )
        {
            $validate['paymentProductId'] = 'required|numeric';
            $validate['offerCategoryId']  = 'required|string';
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _insertUserPlan($rechargePlanId, $insertFeePlanArray)
    {

        $insertFeePlan = \App\Models\Operatorfeeplan::
                select('PlanID', 'FeePlanID', 'Operator', 'Circle', 'CategoryName', 'Mrp', 'TalkTime', 'ServiceTax', 'ProcessingFees', 'AccessFees', 'Validity', 'Remarks', 'Type', 'Description')
                ->where('FeePlanID', $rechargePlanId)
                ->first();

        if ( count($insertFeePlan) > 0 )
        {
            $insertFeePlanArray = array(
                'PlanID'          => $insertFeePlan->PlanID,
                'FeePlanID'       => $insertFeePlan->FeePlanID,
                'Operator'        => $insertFeePlan->Operator,
                'Circle'          => $insertFeePlan->Circle,
                'CategoryName'    => $insertFeePlan->CategoryName,
                'Mrp'             => $insertFeePlan->Mrp,
                'TalkTime'        => $insertFeePlan->TalkTime,
                'ServiceTax'      => $insertFeePlan->ServiceTax,
                'ProcessingFees'  => $insertFeePlan->ProcessingFees,
                'AccessFees'      => $insertFeePlan->AccessFees,
                'Validity'        => $insertFeePlan->Validity,
                'Remarks'         => $insertFeePlan->Remarks,
                'Type'            => $insertFeePlan->Type,
                'Description'     => $insertFeePlan->Description,
                'CreatedDateTime' => date('Y-m-d H:i:s'),
                'CreatedBy'       => config('vwallet.adminTypeKey')
            );
        }
        else
        {
            $insertFeePlanArray['CreatedDateTime'] = date('Y-m-d H:i:s');
            $insertFeePlanArray['CreatedBy']       = config('vwallet.adminTypeKey');
        }

        return $insertId = \App\Models\Useroperatorfeeplan::insertGetId($insertFeePlanArray);
    }

    private function _getTransactionInfo($transactionInfo, $request)
    {
        $transAmount          = $transactionInfo['transAmount'];
        $superCashPaymentInfo = $request->input('superCashPamentInfo');
        $superCashAmount      = 0.00;
        $discountAmount       = 0.00;
        if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 )
        {
            $superCashAmount = $superCashPaymentInfo['offerAmount'];
            $transAmount     -= $superCashAmount;
        }
        $offerPamentInfo = $request->input('offerPamentInfo');
        if ( ! empty($offerPamentInfo) )
        {
            if ( $offerPamentInfo['event'] == 'DISCOUNT' )
            {
                $discountAmount = $offerPamentInfo['offerAmount'];
                $transAmount    -= $discountAmount;
            }
        }

        $transactionInfo['superCashAmount'] = $superCashAmount;
        $transactionInfo['discountAmount']  = $discountAmount;
        $transactionInfo['totalAmount']     = $transAmount;
        return $transactionInfo;
    }

    private function validateBillerInfo($request)
    {
        $biller = \App\Models\Billerlist::select('BillerName', 'Ref1FieldValidation', 'Ref1FieldMessage')
                        ->where('BillerId', $request->input('billerId'))->first();
        if ( ! $biller )
        {
            return trans('messages.billerInactive');
        }
        else
        {
            if ( preg_match('/' . $biller->Ref1FieldValidation . '/', $request->input('mobileNumber')) )
            {
                return TRUE;
            }
            else
            {
                return $biller->Ref1FieldMessage;
            }
        }
    }

}
