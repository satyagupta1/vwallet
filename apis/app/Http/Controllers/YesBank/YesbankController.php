<?php

namespace App\Http\Controllers\YesBank;

class YesbankController extends \App\Http\Controllers\Controller {

    public function mePayServerReq(\Illuminate\Http\Request $request)
    {
        $request->merge(array('pgMerchantId' => config('vwallet.pgMerchantId')));
        $requestMsgArray     = $request->input();
        $requestMsgParams    = implode('|', $requestMsgArray);
        $requestMsgJson      = array(
            'requestMsg'   => $requestMsgParams,
            'pgMerchantId' => config('vwallet.pgMerchantId')
        );
        $requestMsgJson      = json_encode($requestMsgJson);
        $securityKey         = config('vwallet.merchantKey');
        $requestMsgEncrypted = \App\Libraries\yesbank\VtransactSecurity::encryptValue($requestMsgJson, $securityKey);

        $mePayServerReq = config('vwallet.yesBankApiUrl') . "mePayServerReq";

        $reponse = \App\Libraries\Helpers::curl_send($mePayServerReq, $requestMsgEncrypted);
        return $reponse;
    }

    public function meTransCollectSvc(\Illuminate\Http\Request $request)
    {
        $request->merge(array('pgMerchantId' => config('vwallet.pgMerchantId')));
        $requestMsgArray     = $request->input();
        $requestMsgParams    = implode('|', $requestMsgArray);
        $requestMsgJson      = array(
            'requestMsg'   => $requestMsgParams,
            'pgMerchantId' => config('vwallet.pgMerchantId')
        );
        $requestMsgJson      = json_encode($requestMsgJson);
        $securityKey         = config('vwallet.merchantKey');
        $requestMsgEncrypted = \App\Libraries\yesbank\VtransactSecurity::encryptValue($requestMsgJson, $securityKey);
    

        $mePayServerReq = config('vwallet.yesBankApiUrl') . "meTransCollectSvc";

        $reponse = \App\Libraries\Helpers::curl_send($mePayServerReq, $requestMsgEncrypted);
        return $reponse;
    }

    public function transactionStatusQuery(\Illuminate\Http\Request $request)
    {
        $request->merge(array('pgMerchantId' => config('vwallet.pgMerchantId')));
        $requestMsgArray     = $request->input();
        $requestMsgParams    = implode('|', $requestMsgArray);
        $requestMsgJson      = array(
            'requestMsg'   => $requestMsgParams,
            'pgMerchantId' => config('vwallet.pgMerchantId')
        );
        $requestMsgJson      = json_encode($requestMsgJson);
        $securityKey         = config('vwallet.merchantKey');
        $requestMsgEncrypted = \App\Libraries\yesbank\VtransactSecurity::encryptValue($requestMsgJson, $securityKey);

        $mePayServerReq = config('vwallet.yesBankApiUrl') . "transactionStatusQuery";

        $reponse = \App\Libraries\Helpers::curl_send($mePayServerReq, $requestMsgEncrypted);
        return $reponse;
    }

    public function meRefundServerReq(\Illuminate\Http\Request $request)
    {
        $request->merge(array('pgMerchantId' => config('vwallet.pgMerchantId')));
        $requestMsgArray     = $request->input();
        $requestMsgParams    = implode('|', $requestMsgArray);
        $requestMsgJson      = array(
            'requestMsg'   => $requestMsgParams,
            'pgMerchantId' => config('vwallet.pgMerchantId')
        );
        $requestMsgJson      = json_encode($requestMsgJson);
        $securityKey         = config('vwallet.merchantKey');
        $requestMsgEncrypted = \App\Libraries\yesbank\VtransactSecurity::encryptValue($requestMsgJson, $securityKey);

        $mePayServerReq = config('vwallet.yesBankApiUrl') . "meRefundServerReq";

        $reponse = \App\Libraries\Helpers::curl_send($mePayServerReq, $requestMsgEncrypted);
        return $reponse;
    }

    public function checkVirtualAddressME(\Illuminate\Http\Request $request)
    {
        $request->merge(array('pgMerchantId' => config('vwallet.pgMerchantId')));
        $requestMsgArray     = $request->input();
        $requestMsgParams    = implode('|', $requestMsgArray);
        $requestMsgJson      = array(
            'requestMsg'   => $requestMsgParams,
            'pgMerchantId' => config('vwallet.pgMerchantId')
        );
        $requestMsgJson      = json_encode($requestMsgJson);
        $securityKey         = config('vwallet.merchantKey');
        $requestMsgEncrypted = \App\Libraries\yesbank\VtransactSecurity::encryptValue($requestMsgJson, $securityKey);

        $mePayServerReq = config('vwallet.yesBankApiUrl') . "CheckVirtualAddress/checkVirtualAddressME";

        $reponse = \App\Libraries\Helpers::curl_send($mePayServerReq, $requestMsgEncrypted);
        return $reponse;
    }

}
