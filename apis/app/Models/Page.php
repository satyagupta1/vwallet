<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 10:33:36 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Page
 * 
 * @property int $PageId
 * @property string $Title
 * @property string $PageType
 * @property string $Content
 * @property string $Status
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 *
 * @package App\Models
 */
class Page extends Eloquent
{
	protected $primaryKey = 'PageId';
	public $timestamps = false;

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'Title',
		'PageType',
		'Content',
		'Status',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDateTime'
	];
}
