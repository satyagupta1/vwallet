<?php
    /**
     * A Send Message with Template file
     * @category  PHP
     * @package   Communication
     * @author    Viola Services (India) PVT LTD <development@violamoney.com>
     * @license   https://violamoney.com Licence
     */
     
    namespace App\Http\Controllers\Communication;
    
    
    // use validation and reqeust class
    use Validator;
    use Illuminate\Http\Request;

    // use libraries
    // use App\Libraries\Functions;
    use App\Libraries\Communication\Sendemail_lib;
    use App\Libraries\Communication\Sendsms_lib;
    use App\Libraries\Communication\Sendpush_lib;
    use App\Libraries\Communication\Sendweb_lib;

    /**
     * A Sendemail_lib class
     * @category  PHP
     * @package   Communication
     * @author    Viola Services (India) PVT LTD <development@violamoney.com>
     * @license   https://violamoney.com Licence
     */

class SendMessageController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    // Actual Controller

    /**
     * Send now is the main function which is used to send notifications to the users
     * on this function we will get the following parameters in post format and according
     * to the data we will send the messages.
     *
     * @param
     * application_name = The name of the application through which we are sending request.
     *  - V-Wallet
     *  - V-Business
     * notification_for = This will the key for the notification template. With the help of this filed
     *                      We will fetch the data from the language and template file.
     *                      This may has the section or page name.
     *  - nreg = Normal Registration Section.
     *  - qreg = quick Registration Section.
     *  - breg = Business Registration Section.
     *  - aaact = Activate email Account.
     *  - forpas = forgot password.
     *  - etc......
     * notification_type = It may be one of the following. If need to send multiple messages
     *                      Please send multiple things in comma separation format. The value
     *                      may be one of the following data. (all small case)
     *  - email = Email ID
     *  - sms = SMS
     *  - push = Push Notification
     *  - web = Web Notification
     *
     * In case if we need to send email and sms both we can send notification_type = email,sms
     * In the same way if we need to send email, sms and push notification then email,sms,push
     *
     * email_id = if we have email in notification_type then need to send email_id otherwise it should be blank.
     * email_data = if we have email in notification_type then need to send email_data, we will replace this data
     *              in the template otherwise it should be blank.
     * mobile_no = Mobile number if we want to send sms.
     * mobile_data = The data for the sms which need to send in SMS template will be pick from language.
     * device_type = The device type may be windows, android, iphone
     *  - android
     *  - iphone
     * device_token = The token of the device to send the puch notification.
     * device_data = The data which we need to send via push notification, the template will be pick from language file.
     * web_data = the web data which we need to send as web notification, the template will be pick from language file.
     *
    */
    public function sendMsg(Request $request)
    {
        // check validations
        $notification_type = trim($request->input('notification_type'));
        // check if notification type has more the a value
        if (strpos($notification_type, ',') !== FALSE)
        {
            $notification_type_arr = explode(',', preg_replace('/\s/', '', $notification_type));
        }
        else
        {
            $notification_type_arr = array($notification_type);
        }

        $valid = $this->_sendMsgValid($request, $notification_type_arr);

        if ($valid === TRUE)
        {

            $partyId = trim($request->input('partyid'));
            $linkedid = trim($request->input('linkedid'));

            $notification_for = trim($request->input('notification_for'));
            
            $application_name = trim($request->input('application_name'));
            
            $email_id = trim($request->input('email_id'));
            $email_data = trim($request->input('email_data'));           
            $mobile_no = trim($request->input('mobile_no'));
            $mobile_data = trim($request->input('mobile_data'));
            
            $device_type = trim($request->input('device_type'));
            $device_token = trim($request->input('device_token'));
            $device_data = trim($request->input('device_data'));

            $web_data = trim($request->input('web_data'));

            $def_noti_data = array(
                'application_name' => $application_name,
                'party_id'  => $partyId,
                'linked_id' => $linkedid,
            );

            $return_response = array(
                'status'=>'success',
                'error' => FALSE,
            );
            
            // check if we need to send email with the request
            if (in_array('email', $notification_type_arr))
            {
                // send email go to library
                $email_notification_lib_data = array(
                    'notification_for' => $notification_for,
                    'email_id' => $email_id,
                    'email_data' => $email_data,
                );

                $lib_array = array_merge($def_noti_data, $email_notification_lib_data);
                
                $email_response = Sendemail_lib::sendNow($lib_array);
                $return_response['email'] = $email_response;
            }

            // check if we need to send sms with the request
            if (in_array('sms', $notification_type_arr))
            {
                // send sms go to library
                $sms_notification_lib_data = array(
                    'notification_for' => $notification_for,
                    'mobile_no' => $mobile_no,
                    'mobile_data' => $mobile_data,
                );

                $sms_lib_array = array_merge($def_noti_data, $sms_notification_lib_data);
               
                $sms_response = Sendsms_lib::sendNow($sms_lib_array);
                $return_response['sms'] = $sms_response;
            }

            // check if we need to send push with the request
            if (in_array('push', $notification_type_arr))
            {

                $push_notification_lib_data = array(
                    'notification_for' => $notification_for,
                    'device_type' => $device_type,
                    'device_token' => $device_token,
                    'push_data' => $device_data,
                    'other_details' => array(),
                );
                $push_lib_array = array_merge($def_noti_data, $push_notification_lib_data);
                $push_response = Sendpush_lib::sendNow($push_lib_array);
                $return_response['push'] = $push_response;
            }

            // check if we need to send web notification
            if (in_array('web', $notification_type_arr))
            {
                $web_notification_lib_data = array(
                    'notification_for' => $notification_for,
                    'web_data' => $web_data,
                    'other_details' => array(
                        'mobile_no'=> $mobile_no,
                        ''
                    ),
                );
                $web_lib_array = array_merge($def_noti_data, $web_notification_lib_data);
                $web_response = Sendweb_lib::sendNow($web_lib_array);
                $return_response['web'] = $web_response;


            }
            return $return_response;
        }
        else
        {
            // return error message in the response.
            return $valid;
        }
    }

    private function _sendMsgValid($request, $noti_arr)
    {
        $validat = [
            'notification_for'  => 'required',
            'notification_type' => 'required|address_char|min:3',
            'application_name'  => 'required|alpha_dash|min:4',
        ];
        
        $messages = [
            'notification_for.required'         => trans('communication'.DIRECTORY_SEPARATOR.'validation.notification_for_req'),
            
            'notification_type.required'        => trans('communication'.DIRECTORY_SEPARATOR.'validation.notification_type_req'),
            'notification_type.address_char'    => trans('communication'.DIRECTORY_SEPARATOR.'validation.notification_type_ach'),
            'notification_type.min'             => trans('communication'.DIRECTORY_SEPARATOR.'validation.notification_type_min'),
            
            'application_name.required'         => trans('communication'.DIRECTORY_SEPARATOR.'validation.application_name_req'),
            'application_name.alpha_dash'       => trans('communication'.DIRECTORY_SEPARATOR.'validation.application_name_ad'),
            'application_name.min'              => trans('communication'.DIRECTORY_SEPARATOR.'validation.application_name_min'),
        ];        

        if ( in_array('email', $noti_arr))
        {
            $validat['email_id'] = 'required|email';
            $validat['email_data'] = 'required|min:10';

            $messages['email_id.required'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.email_id_req');
            $messages['email_id.email'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.email_id_valid');

            $messages['email_data.required'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.email_data_req');
            $messages['email_data.min'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.email_data_min');
        }

        if ( in_array('sms', $noti_arr))
        {
            $validat['mobile_no'] = 'required|numerics|min:9';
            $validat['mobile_data'] = 'required|min:10';

            $messages['mobile_no.required'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.mobile_no_req');
            $messages['mobile_no.numerics'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.mobile_no_valid');
            $messages['mobile_no.min'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.mobile_no_min');

            $messages['mobile_data.required'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.mobile_data_req');
            $messages['mobile_data.min'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.mobile_data_min');
        }

        if ( in_array('push', $noti_arr))
        {
            $validat['device_type'] = 'required|alpha|min:1|max:10';
            $validat['device_token'] = 'required|max:200';
            $validat['device_data'] = 'required|min:10';

            $messages['device_type.required'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.device_type_req');
            $messages['device_type.alpha'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.device_type_alpha');
            $messages['device_type.max'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.device_type_max');
            $messages['device_type.min'] = trans('communication'.DIRECTORY_SEPARATOR.'validation.device_type_min');

            $messages['device_token.required'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.device_token_req');
            $messages['device_token.max'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.device_token_max');

            $messages['device_data.required'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.device_data_req');
            $messages['device_data.min'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.device_data_min');
        }
        if ( in_array('web', $noti_arr))
        {
            $validat['web_data'] = 'required';
            $messages['web_data.required'] =  trans('communication'.DIRECTORY_SEPARATOR.'validation.web_data_req');
        }

        $validator = Validator::make($request->all(), $validat, $messages);

        $errs = array();
        $hasError = 'no';

        foreach ($validator->messages()->toArray() as $key => $error) {
            $errs[$key] = $error[0];
            $hasError = 'yes';
        }

        if ($hasError === 'no') {
            return TRUE;
        } else {
            return $errs;
        }
    }

    private static function _createCommunicationLog($notification_log, $notitbl = 1)
    {
        if ($notitbl === 1)
        {
            $insert_id = Notificationlog::insertGetId($notification_log);
        }
        else if ($notitbl === 1)
        {
            $insert_id = Notificationlogparameters::insertGetId($notification_log);
        }
        return $insert_id;
    }
}
