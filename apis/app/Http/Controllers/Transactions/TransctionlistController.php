<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;

/**
 * TransctionlistController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TransctionlistController extends Controller {

    public function __construct()
    {
        $this->perpage  = config('vwallet.perpage');
        $this->kycLimit = config('vwallet.MONTH_LIMIT_KYC');
        $this->lang     = app('translator')->getLocale();
    }

    /*
     *  Search Transaction - Advanced Transaction Search - View Transaction
     *  @Desc This is used to get results with given params, orderby, limit and pagination.
     *  @param Request $request
     *  @return JSON
     */

    public function transctionList(\Illuminate\Http\Request $request)
    {

        //$currency = \App\Models\Transaction::find(1)->currency()->get(); 
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateMethodTTransaction($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId            = $request->input('partyId');
        $transactionId      = $request->input('transactionId');
        $TransactionOrderID = $request->input('transactionOrderId');
        $tranTypeCode       = $request->input('transactionTypeCode');
        $tranType           = $request->input('transactionType');
        $tranMode           = $request->input('transactionMode');

        $tranStatus = $request->input('transactionStatus');
        $fromDate   = $request->input('fromDate');
        $toDate     = $request->input('toDate');
        $fromAmount = $request->input('fromAmount');
        $toAmount   = $request->input('toAmount');


        $searchField = $request->input('searchField');
        $mobileNo    = $request->input('mobileNumber');
        $fullName    = $request->input('fullName');
        $emailId     = $request->input('email');
        $violaId     = $request->input('violaId');

        $orderBy       = $request->input('orderBy');
        $orderByColumn = $request->input('orderColumn');
        $limit         = $request->input('limit');
        $paginate      = $request->input('paginate');

        $selectedColumns = [
            'transactiondetail.TransactionOrderID',
            'transactiondetail.FromAccountName',
            'transactiondetail.TransactionDate',
            'Amount',
            'TransactionTypeCode',
            'TransactionVendor',
            'TransactionStatus',
            'TransactionMode',
            'TransactionType',
            'FirstName',
            'LastName',
            'MobileNumber',
            'ViolaID',
            'EmailID',
            'partypreferredpaymentmethod.CardDisplayNumber',
            'paymentsproduct.ProductName',
            'paymentsproduct.ProductType',
            'transactioneventlog.Comments'
        ];

        //$query = \App\Models\Userbeneficiary::select($selectedColumns);
        //$query->where('UserWalletID', '=', $partyId);
        $query = \App\Models\Transactiondetail::join('userwallet', 'userwallet.UserWalletID', '=', 'transactiondetail.ToWalletId')
                //join('dailytransactionlog', 'transactiondetail.TransactionDetailID', '=', 'dailytransactionlog.TransactionDetailID')
                //->join('userwallet', 'dailytransactionlog.PartyId', '=', 'userwallet.UserWalletID')                
                ->leftjoin('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                ->join('paymentsproduct', 'transactiondetail.PaymentsProductID', '=', 'paymentsproduct.PaymentsProductID')
                ->join('transactioneventlog', 'transactiondetail.TransactionDetailID', '=', 'transactioneventlog.TransactionDetailID')
                ->where('transactiondetail.UserWalletId', '=', $partyId);

        //filter by beneficiary type
        if ( $transactionId !== NULL )
        {
            $query->where('transactiondetail.TransactionDetailID', '=', $transactionId);
        }

        //filter by beneficiary type
        if ( $TransactionOrderID !== NULL )
        {
            $query->where('transactiondetail.TransactionOrderID', '=', $TransactionOrderID);
        }

        //filter by Transaction Type Code
        if ( $tranTypeCode !== NULL )
        {
            $query->where('transactiondetail.TransactionTypeCode', '=', $tranTypeCode);
        }

        //filter by Transaction Mode
        if ( $tranMode !== NULL )
        {
            $query->where('transactiondetail.TransactionMode', '=', $tranMode);
        }
       
        //filter by triple click
        if ( $tranType !== NULL )
        {
            $query->where('transactiondetail.TransactionType', '=', $tranType);
        }


        if ( $tranStatus !== NULL )
        {
            $query->where('transactiondetail.TransactionStatus', '=', $tranStatus);
        }

        //common search field check
        if ( $searchField !== NULL )
        {

            $query->where(function ($query1) use ($searchField) {
                $query1->where('userwallet.FirstName', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.LastName', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.MobileNumber', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.EmailID', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('userwallet.ViolaID', 'LIKE', '%' . $searchField . '%')
                        ->orWhere('transactiondetail.FromAccountName', 'LIKE', '%' . $searchField . '%');
            });
        }
        else
        {

            //filter by recepent info
            if ( $mobileNo !== NULL )
            {
                $query->where('userwallet.MobileNumber', '=', $mobileNo);
            }

            if ( $fullName !== NULL )
            {
                $query->where(function ($query1) use ($fullName) {
                    $query1->where('userwallet.FirstName', 'LIKE', '%' . $fullName . '%')
                            ->orWhere('userwallet.LastName', 'LIKE', '%' . $fullName . '%');
                });
            }

            if ( $emailId !== NULL )
            {
                $query->where('userwallet.EmailID', '=', $emailId);
            }
            if ( $violaId !== NULL )
            {
                $query->where('userwallet.ViolaID', '=', $violaId);
            }
        }

        //Date filters 
        if ( $fromDate !== NULL && $toDate !== NULL )
        {
            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($fromDate));
            $ymdToDate   = date('Y-m-d' . ' 23:59:59', strtotime($toDate));
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        elseif ( $fromDate !== NULL )
        {

            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($fromDate));
            $ymdToDate   = date('Y-m-d' . ' 23:59:59', strtotime($fromDate));
            $query->whereBetween('transactiondetail.TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }
        elseif ( $toDate !== NULL )
        {
            $ymdToDate   = date('Y-m-d' . ' 23:59:59', strtotime($toDate));
            $ymdfromDate = date('Y-m-d' . ' 00:00:00', strtotime($toDate));
            $query->whereBetween('transactiondetail..TransactionDate', [ $ymdfromDate, $ymdToDate ]);
        }

        //Amout filters 
        if ( $fromAmount !== NULL && $toAmount !== NULL )
        {
            $query->whereBetween('transactiondetail.Amount', [ $fromAmount, $toAmount ]);
        }

        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }

        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectedColumns) )
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('transactiondetail.TransactionDetailID', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $userDetails  = $query->select($selectedColumns)->paginate($this->perpage)->toArray();
            $countResults = $userDetails['total'];
        }
        else
        {
            $userDetails  = $query->get($selectedColumns)->toArray();
            $countResults = count($userDetails);
        }

        if ( $countResults > 0 )
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array();
            $output_arr['res_data']    = $userDetails;
            return response()->json($output_arr);
        }

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateMethodTTransaction($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
                //'device_type' => 'required|in:W,I,A,M',
        ];

        if ( $request->input('searchField') !== NULL )
        {
            $validate['searchField'] = 'required|min:3';
        }
        if ( $request->input('transactionId') !== NULL )
        {
            $validate['transactionId'] = 'required|numeric';
        }
        if ( $request->input('transactionType') !== NULL )
        {
            $validate['transactionType'] = 'required|in:TOPUP,TRIPLECLICK';
        }
        if ( $request->input('transactionMode') !== NULL )
        {
            $validate['transactionMode'] = 'required|in:Credit,Debit';
        }

        if ( $request->input('transactionTypeCode') !== NULL )
        {
            $validate['transactionTypeCode'] = 'required|alpha';
        }

        if ( $request->input('transactionOrderId') !== NULL )
        {
            $validate['transactionOrderId'] = 'required|numeric';
        }

        if ( $request->input('mobileNumber') !== NULL )
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
        }

        if ( $request->input('fullName') !== NULL )
        {
            $validate['fullName'] = 'required|min:3|max:70|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/';
        }

        if ( $request->input('violaId') !== NULL )
        {
            $validate['violaId'] = 'required|alpha_num';
        }

        if ( $request->input('email') !== NULL )
        {
            $validate['email'] = 'required|email|max:100';
        }

        if ( $request->input('transactionStatus') !== NULL )
        {
            $validate['transactionStatus'] = 'required|alpha';
        }
        if ( $request->input('fromAmount') !== NULL && $request->input('toAmount') !== NULL )
        {
            $validate['fromAmount'] = 'required|numeric|between:1,' . $this->kycLimit;
            $validate['toAmount']   = 'required|numeric|between:' . $request->input('fromAmount') . ',' . $this->kycLimit;

            $messages['from_amount.between'] = trans('messages.fromAmount.between');
            $messages['to_amount.between']   = trans('messages.toAmount.between');
        }

        if ( $request->input('fromDate') !== NULL && $request->input('toDate') !== NULL )
        {
            $validate['fromDate'] = 'required|date_format:"d-m-Y"|before:toDate';
            $validate['toDate']   = 'required|date_format:"d-m-Y"|after:fromDate';
        }
        elseif ( $request->input('fromDate') !== NULL )
        {
            $validate['fromDate'] = 'required|date_format:"d-m-Y"|before:' . date('d-m-Y');
        }
        elseif ( $request->input('toDate') !== NULL )
        {
            $validate['toDate'] = 'required|date_format:"d-m-Y"|before:' . date('d-m-Y');
        }



        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.         



        /* $messages = [
          'user_id.required' => trans($langfolder . '/validation.userId'),
         */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function getTransactionWalletData(\Illuminate\Http\Request $request)
    {

        //$currency = \App\Models\Transaction::find(1)->currency()->get(); 
        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateTransactionsWalletData($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId            = $request->input('partyId');
        $transactionId      = $request->input('walletAccountId');
        $transactionOrderID = $request->input('transactionOrderId');
        //current balance
        $update_userWallet  = \App\Models\Walletaccount::find($request->input('walletAccountId'));
        //get transaction details
        $getTransactionList = app('App\Http\Controllers\Transactions\TransactionsController')->getTransactionsList($request);
        if ( empty($getTransactionList->original['res_data']) )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            $output_arr['res_data']            = array( 'AvailableBalance' => number_format(( float ) $update_userWallet->AvailableBalance, 2, '.', ''), 'CurrentAmount' => number_format(( float ) $update_userWallet->CurrentAmount, 2, '.', ''), 'TransactionDetails' => NULL );
            return json_encode($output_arr);
        }
        $transactionDetails     = $getTransactionList->original['res_data']['0'];
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = array( 'AvailableBalance' => number_format(( float ) $update_userWallet->AvailableBalance, 2, '.', ''), 'CurrentAmount' => number_format(( float ) $update_userWallet->CurrentAmount, 2, '.', ''), 'TransactionDetails' => $transactionDetails );
        return json_encode($output_arr);
    }

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateTransactionsWalletData($request)
    {
        $messages = [];
        $validate = [
            'partyId'            => 'required|numeric',
            'walletAccountId'    => 'required|numeric',
            'transactionOrderId' => 'required|numeric',
        ];

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
