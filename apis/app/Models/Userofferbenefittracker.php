<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:15 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userofferbenefittracker
 * 
 * @property int $UserOfferBenefitTrackerID
 * @property int $UserWalletID
 * @property int $WalletAccountId
 * @property int $PromotionalOfferID
 * @property int $OfferBenefitID
 * @property int $ParentTransactionDetailID
 * @property int $ParentPaymentsProductID
 * @property \Carbon\Carbon $ParentTransactionDate
 * @property int $BenefitCategoryRefID
 * @property string $OfferPromoCode
 * @property float $EarnedAmount
 * @property \Carbon\Carbon $EarnedAmountDate
 * @property \Carbon\Carbon $EarnedAmountExpiryDate
 * @property string $EarnedProviderName
 * @property \Carbon\Carbon $ExtendedValidityDate
 * @property float $UsedAmount
 * @property float $ExpiredAmount
 * @property string $ApplicableProviderName
 * @property int $ApplicablePaymentProductEventID
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Walletaccount $walletaccount
 * @property \App\Models\Offerbenefit $offerbenefit
 * @property \App\Models\Paymentproductevent $paymentproductevent
 * @property \App\Models\Promotionaloffer $promotionaloffer
 * @property \App\Models\Transactiondetail $transactiondetail
 *
 * @package App\Models
 */
class Userofferbenefittracker extends Eloquent
{
	protected $table = 'userofferbenefittracker';
	protected $primaryKey = 'UserOfferBenefitTrackerID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserOfferBenefitTrackerID' => 'int',
		'UserWalletID' => 'int',
		'WalletAccountId' => 'int',
		'PromotionalOfferID' => 'int',
		'OfferBenefitID' => 'int',
		'ParentTransactionDetailID' => 'int',
		'ParentPaymentsProductID' => 'int',
		'BenefitCategoryRefID' => 'int',
		'EarnedAmount' => 'float',
		'UsedAmount' => 'float',
		'ExpiredAmount' => 'float',
		'ApplicablePaymentProductEventID' => 'int'
	];

	protected $dates = [
		'ParentTransactionDate',
		'EarnedAmountDate',
		'EarnedAmountExpiryDate',
		'ExtendedValidityDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'WalletAccountId',
		'PromotionalOfferID',
		'OfferBenefitID',
		'ParentTransactionDetailID',
		'ParentPaymentsProductID',
		'ParentTransactionDate',
		'BenefitCategoryRefID',
		'OfferPromoCode',
		'EarnedAmount',
		'EarnedAmountDate',
		'EarnedAmountExpiryDate',
		'EarnedProviderName',
		'ExtendedValidityDate',
		'UsedAmount',
		'ExpiredAmount',
		'ApplicableProviderName',
		'ApplicablePaymentProductEventID',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'WalletAccountId');
	}

	public function offerbenefit()
	{
		return $this->belongsTo(\App\Models\Offerbenefit::class, 'OfferBenefitID');
	}

	public function paymentproductevent()
	{
		return $this->belongsTo(\App\Models\Paymentproductevent::class, 'ApplicablePaymentProductEventID');
	}

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'PromotionalOfferID');
	}

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'ParentPaymentsProductID', 'PaymentsProductID')
					->where('transactiondetail.PaymentsProductID', '=', 'userofferbenefittracker.ParentPaymentsProductID')
					->where('transactiondetail.TransactionDetailID', '=', 'userofferbenefittracker.ParentTransactionDetailID');
	}
}
