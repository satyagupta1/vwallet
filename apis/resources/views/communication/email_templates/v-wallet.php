<!--violacard-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>
            p,
            span,
            font,
            td,
            div {
                line-height: 100%;
                /* Force Hotmail to display normal line spacing */
            }

            body {
                font-family: Arial, sans-serif;
                font-weight: 100;
                -webkit-font-smoothing: antialiased;
                letter-spacing: .5px;
                color: #888;
            }

            body,
            table,
            td,
            a {
                /* Prevent WebKit and Windows mobile changing default text sizes */
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                mso-line-height-rule: exactly !important;
            }

            p {
                font-size: 12px;
                font-weight: 100;
                line-height: 1.5;
                letter-spacing: 1px;
                mso-line-height-rule: exactly !important;
            }

            table,
            td {
                /* Remove spacing between tables in Outlook 1007 and up */
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }

            img {
                /* Allow smoother rendering of resized image in Internet Explorer */
                -ms-interpolation-mode: bicubic;
            }

            b {
                font-weight: 500;
            }

            @media screen and (max-width: 600px) {
                td[class="down"] img {
                    height: auto !important;
                    max-width: 70px;
                }
                td[class="down"] {
                    padding: 5px 10px 8px !important;
                }
                td[class="min-pad"] {
                    padding: 5px 10px 8px !important;
                }
                td[class="min-pad"] img {
                    height: 25px !important;
                }
                td[class="min-100"] {
                    width: 100% !important;
                }
            }
        </style>
    </head>
    <body style="margin:0px;padding:0px;">
        <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;padding:0;width:100%;font-family: Arial, sans-serif;background-color: #f7f7f7; padding: 15px;">
            <tr>
                <td>
                    <table style="max-width:540px; margin:0 auto; background-color:#fff;border-collapse: collapse;border: 1px solid #ddd;" align="center" width="540">
                        <tr style="margin:0;padding:0px;">
                            <td style="padding: 15px;border-bottom:1px solid #ddd"><img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/vwallet.png" alt="" width="100"></td>
                            <td style="text-align:right;padding: 15px;border-bottom:1px solid #ddd"><img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/vwallet3.png" alt="" width="100"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 15px; ">
                                <?php
                                if ( isset($text) )
                                {
                                    $index    = 1;
                                    $text_arr = explode('\n\n', $text);
                                    foreach ( $text_arr as $data )
                                    {
                                        if ( $index === 2 )
                                        {
                                            ?>
                                            <p style="margin:10px 0px; color:#888;font-family: Arial, sans-serif; font-size: 12px;"><b><?php echo $data; ?></b></p>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <p style="margin:10px 0px; color:#888;font-family: Arial, sans-serif; font-size: 12px;"><?php echo $data; ?></p>
                                            <?php
                                        }
                                        $index++;
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div  style="background: #f9f9f9; padding:20px 15px;" >
                                    <p style="font-weight:600; color:#888; margin-bottom: 0px;margin-top: 0px;">Believe it or Not?</p>
                                    <ol style="list-style: none; padding-left: 0px;margin-bottom: 0px;">
                                        <li style="margin-top:0px;margin-bottom:0px;font-family: Arial, sans-serif;color:#888;font-weight:100;font-size: 12px;"> ViolaWallet gives you a cool Virtual Card for all your online transactions.</li>
                                    </ol>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 15px;">
                                <p style="font-weight:500; color:#888; margin-bottom: 0px;margin-top:0px;">Thank you,</p>
                                <p style="font-weight:600; color:#888; margin-bottom: 0px;margin-top: 0px;">Team <span style="color:#68c5df;">Viola</span></p>
                                <p style="margin-top:10px;margin-bottom:3px;font-family: Arial, sans-serif;color:#888;font-weight:100;font-size: 10px;">For any queries please contact us <a href="mailto:support@violawallet.com" style="color:#68c5df;text-decoration:none;font-weight:500;font-size: 10px;">here.</a></p>
                                <p style="margin-top: 0px;margin-bottom:0px; font-family: Arial, sans-serif;color:#888;font-weight:100;font-size: 10px;">Giving you the best service always! </p>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/strip.jpg" alt="" style="max-width:540px;width:540px; ">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px 15px 8px;" class="down">
                                <a href="<?php echo config('vwallet.appleItuneUrl'); ?>" style="text-decoration:none;" target="_blank">
                                    <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/26_Apple_Store.jpg" title="Apple Store" alt="Apple Store" style="height: 30px;">
                                </a>&nbsp;
                                <a href="<?php echo config('vwallet.googlePlayUrl'); ?>" style="text-decoration:none;" target="_blank">
                                    <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/27_Google_Store.jpg" title="Google Store" alt="Google Store" style="height:30px;">
                                </a>
                            </td>
                            <td style="text-align: right; padding: 5px 15px 8px;" class="min-pad">
                                <a href="<?php echo config('vwallet.facebookUrl'); ?>" style="text-decoration:none; display: inline-block;" target="_blank">
                                    <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/Facebook.jpg" title="Facebook" alt="Facebook" height="30">
                                </a>&nbsp;
                                <a href="<?php echo config('vwallet.twitterUrl'); ?>" style="text-decoration:none;display: inline-block;" target="_blank">
                                    <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/Twiter.jpg" title="Twiter" alt="Twiter" height="30">
                                </a>&nbsp;
                                <a href="<?php echo config('vwallet.linkedinUrl'); ?>" style="text-decoration:none;display: inline-block;" target="_blank">
                                    <img src="<?php echo config('vwallet.baseUrl') ?>public/images/email-templates/Linkedin.jpg" title="Linkedin" alt="Linkedin" height="30">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; padding-bottom: 10px;border-top: 2px solid #f28b00;">
                                <p style="font-family: Arial, sans-serif; font-size:9px;font-weight: 100; margin: 0px;line-height:1.5;margin-top: 5px;"> <span style="margin-bottom:2px;display:inline-block;">&copy; 2017-ViolaWallet. All rights reserved </span>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>