<?php
    namespace App\Libraries\Communication\Smss\Smshub;
    
    class SmsHubLib {

        static protected $smshub_url = 'http://smshub.co.in/';

        static protected $smshub_userid = 'vwalletdev';

        static protected $smshub_password = 'vDlx1%20Z3C5n3$@D';

        static protected $smshub_sender_id = 'VIOLAD';

	/** send sms
	 * @name type
	 * @param string $sms_data array witch is send to api
	 * @return void
	 */
	public static function sendSmsNow($sms_data)
	{

	    $api_element = 'websms/sendsms.aspx';
	    
	    $mobile_no = $sms_data['mobile_no'];
	    $message = urlencode($sms_data['message']);
	    
	    if ( strlen($mobile_no) == 10 && preg_match('/^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$/', $mobile_no))
	    {
		$send_message = SmsHubLib::$smshub_url.$api_element.'?userid='.SmsHubLib::$smshub_userid.'&password='.SmsHubLib::$smshub_password.'&sender='.SmsHubLib::$smshub_sender_id.'&mobileno='.$mobile_no.'&msg='.$message;
		$output = SmsHubLib::_curl_send_sms($send_message);
			return $output;
	    }
	    else
	    {
			return '200';
	    }
	}
    
	/** send curl method
	 * @name         type
	 *
	 * @param string $url array witch is send to api
	 *
	 * @return void
	 */
	private static function _curl_send_sms($url)
	{
	    $ch = curl_init();                       // initialize CURL
	    curl_setopt($ch, CURLOPT_POST, FALSE);    // Set CURL Post Data
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $output = curl_exec($ch);
	    curl_close($ch);                         // Close CURL
	
	    return $output;
	}
    
	/** get balance
	 * @name type
	 * @return void
	 */
	public static function get_balance()
	{
	    $url = SmsHubLib::$smshub_url;
	    $api_element = 'websms/getbalance.aspx';
	    $userid = SmsHubLib::$smshub_userid;
	    $password = SmsHubLib::$smshub_password;
	    
	    $send_request = $url.$api_element.'?userid='.$userid.'&password='.$password;
	    
	    $output = SmsHubLib::_curl_send_sms($send_request);
	    return $output;
	}
    
	/** get message status
	 * @name type
	 * @param string $msg_id message id
	 * @return void
	 */
	public static function message_status($msg_id)
	{
	    $url = SmsHubLib::$smshub_url;
	    $api_element = 'websms/messagestatus.aspx';
	    $send_request = $url.$api_element.'?msgid='.$msg_id;
	
	    $output = SmsHubLib::_curl_send_sms($send_request);
	    return $output;
	}
	
    }
    
    /* End of file Api_send_sms.php */
    /* Location: ./system/application/libraries/Api_send_sms.php */
