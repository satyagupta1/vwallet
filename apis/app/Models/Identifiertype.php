<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Identifiertype
 * 
 * @property int $IdentifierTypeID
 * @property string $IdentifierTypeDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partyidentifers
 *
 * @package App\Models
 */
class Identifiertype extends Eloquent
{
	protected $table = 'identifiertype';
	protected $primaryKey = 'IdentifierTypeID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdentifierTypeID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'IdentifierTypeDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partyidentifers()
	{
		return $this->hasMany(\App\Models\Partyidentifer::class, 'IdentifierTypeID');
	}
}
