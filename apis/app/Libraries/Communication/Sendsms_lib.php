<?php

/**
 * A Send email with Template file
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */

namespace App\Libraries\Communication;

// use libraries
use App\Libraries\Communication\Functions;
use App\Libraries\Communication\Smss\SmsNow;

/**
 * A Sendemail_lib class
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */
class Sendsms_lib {

    /**
     * ---------------------------------------------------------------------
     * send_new method.
     * ---------------------------------------------------------------------
     * The method send new is used for sending email to user with a common
     * email template.
     *
     * @param array $email_data ;
     *                          $email_data = array(
     *                          'email_type_key', // the key of the language file.
     *                          'email_id', // the id on which we have to send message.
     *                          'fullname', // the name of user for which we have to send the email.
     *                          '...', // other array according to language file.
     *                          );
     *
     * @return boolean $email_status;
     * */
    public static function sendNow($smsData = array())
    {
        $dateTime     = date('Y-m-d H:i:s');
        $sms_template = trans('communication' . DIRECTORY_SEPARATOR . 'smstemp.' . $smsData['notification_for'] . '.text');
        $replace_data = trans('communication' . DIRECTORY_SEPARATOR . 'smstemp.' . $smsData['notification_for'] . '.text_vari');
        // var_dump($replace_data);
        if ( ! is_array($replace_data) )
        {
            $replace_data = array();
        }
        $replace_data_length = count($replace_data);
        $replace_count       = 0;
        $replace_with        = array();

        $sms_replace_data = json_decode($smsData['mobile_data']);

        // site_name occurrence
        $site_name_occu = strpos($sms_template, '{{site_name}}');
        $site_name      = $smsData['application_name'];
        if ( $site_name_occu !== FALSE )
        {
            $sms_template = str_replace('{{site_name}}', $site_name, $sms_template);
        }

        if ( in_array('fullname', $replace_data) )
        {
            $replace_with[0]     = ucwords(strtolower($sms_replace_data->fullname));
            $replace_data_length = $replace_data_length - 1;
            $replace_count       = $replace_count + 1;
        }
/*
        for ( $i = 0; $i < $replace_data_length; $i ++  )
        {
            $sms_data_key = ( int ) $replace_count;
            $key          = $replace_data{$sms_data_key};

            $replace_with{$replace_count} = $sms_replace_data->$key;
            $replace_count ++;
        } */
        
        foreach( $replace_data as $key => $val )
        {
            if( $sms_replace_data->$val )
            {
                $replace_with[] = $sms_replace_data->$val;
            }
            $replace_data[$key] = '{{' . $val . '}}';
        }
        
        $final_sms_content       = str_ireplace($replace_data, $replace_with, $sms_template);
    //    $final_sms_content = str_ireplace(array( '{{', '}}' ), array( '', '' ), $sms_content);

        $send_sms_array = array( 'mobile_no' => $smsData['mobile_no'], 'message' => $final_sms_content, );
        //$sms_status     = SmsNow::sendSms($send_sms_array);
        $sms_status = 'Disabled sms';
        $sms_res        = explode($sms_status, ':');

        if ( trim($sms_res[0]) === 'Success' )
        {
            $ReceipentAcknowledged = 'Y';
        }
        else
        {
            $ReceipentAcknowledged = 'N';
        }
        // add records into database for log
        $sms_notification_log = [
            'PartyID'                 => $smsData['party_id'],
            'LinkedTransactionID'     => $smsData['linked_id'],
            'NotificationType'        => 'SMS',
            'SentDateTime'            => $dateTime,
            'ReceipentAcknowledged'   => $ReceipentAcknowledged,
            'ReceipentReceivedDate'   => $dateTime,
            'ReceipentDeliveryReport' => $sms_status,
            'CreatedBy'               => config('communication/config_v.CreatedBy'),
            'CreatedDateTime'         => $dateTime,
            'ToAddress'               => $smsData['mobile_no'],
        ];

        // add records into database for log parameters
        $sms_notificationlogparameters = [
            'NotificationID'    => '',
            'TemplateParameter' => $smsData['mobile_data'],
            'TemplateValue'     => $smsData['notification_for'], //$final_sms_content,
            'CreatedDateTime'   => $dateTime,
            'CreatedBy'         => config('communication/config_v.CreatedBy'),
        ];
        Functions::createCommunicationLog($sms_notification_log, $sms_notificationlogparameters);

        return $sms_status;
    }

}

/* End of file Sendsms_lib.php */