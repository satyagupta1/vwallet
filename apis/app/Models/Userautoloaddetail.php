<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userautoloaddetail
 * 
 * @property int $UserWalletId
 * @property string $IsActive
 * @property float $AutoLoadAmount
 * @property int $BeneficiaryID
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userautoloaddetail extends Eloquent
{
	protected $primaryKey = 'UserWalletId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletId' => 'int',
		'AutoLoadAmount' => 'float',
		'BeneficiaryID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'IsActive',
		'AutoLoadAmount',
		'BeneficiaryID',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletId');
	}
}
