<?php

namespace App\Http\Controllers\Topup;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TopUpUPIController extends \App\Http\Controllers\Controller {

    public function addTopUpUPI(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $checkValidate = $this->_validateSendMoneyUPI($request);
        //$checkValidate = TRUE;
        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId         = $request->input('partyId');
        $amount          = $request->input('amount');
        $businessModelid = $request->input('businessModelId');
        $partyVPA        = $request->input('partyVPA');
        $beneficiaryVPA  = $request->input('beneficiaryVPA');
        $serviceType     = $request->input('serviceType');
        $productEventId  = 1;

        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $beneficiaryInfo = \App\Libraries\TransctionHelper::_userIdValidate(4);
        if ($beneficiaryInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        // session Check
       $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sessionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return $output_arr;
        }
        
        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);

        $externalBeneficiaryIds = self::insertExternalBeneficiary($partyId, $partyVPA, $beneficiaryVPA);

        $transDetails = self::insertTransDetails($partyInfo, $beneficiaryInfo, $externalBeneficiaryIds, $paymentProduct, $fee, $sessionId, $amount,$request);
        if ($transDetails === FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Something went wrong. Please try after sometime.');
            return $output_arr;
        }
        
        $retunInfo = \App\Libraries\TransctionHelper::getWalletInfo($partyId);
        $retunInfo['TransactionDetails'] = $transDetails;
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array('info' => 'Transaction successfully.');
        $output_arr['res_data']    = $retunInfo;
        return $output_arr;
    }

    private static function insertTransDetails($partyInfo, $beneficiaryInfo, $externalBeneficiaryIds, $paymentProduct, $fee, $sessionId, $amount,$request)
    {
        $transDate       = date('Y-m-d');
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        $transctionID    = \App\Libraries\TransctionHelper::getTransctionId();
        $transStausMain  = config('vwallet.transactionStatus.pending');
        $useName         = '';
        $transDetails    = array(
            'TransactionOrderID'       => $transctionID,
            'PaymentsProductID'        => $paymentProduct->PaymentsProductID,
            'UserWalletId'             => $partyInfo->UserWalletID, // Wallet user id of from wallet account
            'FromAccountId'            => $externalBeneficiaryIds['partyVPAId'],
            'FromAccountType'          => 'ExternalCardID',
            'ToAccountType'            => 'WalletAccount',
            'ToAccountId'              => $externalBeneficiaryIds['beneficiaryVPAId'],
            'ToWalletId'               => $beneficiaryInfo->UserWalletID,
            'Amount'                   => $amount,
            'TransactionTypeCode'      => 'Deposit',
            'TransactionDate'          => $dateTimeOfTrans,
            'TransactionStatus'        => $transStausMain,
            'TransactionMode'          => 'Credit',
            'UserSessionUsageDetailID' => $sessionId,
            'TransactionType'          => $paymentProduct->ProductName,
            'CreatedDatetime'          => $dateTimeOfTrans,
            'CreatedBy'                => 'User',
            'TransactionVendor'        => $fee['transctionVendor'],
            'FeeAmount'                => $fee['feeAmount'],
            'TaxAmount'                => $fee['taxAmount'],
            'UpdatedBy'                => 'User',
        );

        $transactionDetailID = \App\Models\Transactiondetail::insertGetId($transDetails);
        if (!$transactionDetailID)
        {
            return FALSE;
        }

        $dailyTransctionLog = self::_dailyTransctionEventDetailsBtoW($transactionDetailID, $paymentProduct, $partyInfo, $beneficiaryInfo, $partyInfo->UserWalletID, $amount, $fee, $dateTimeOfTrans);
        
        $transctionLog = \App\Libraries\TransctionHelper::_transctionEventLog($transactionDetailID, $paymentProduct, $transDate, $useName, $dateTimeOfTrans, $amount,'VPA');

        // preparing notification log details
        $notificationLog = \App\Libraries\TransctionHelper::_notificationLog($transactionDetailID, $paymentProduct, $partyInfo->UserWalletID, $beneficiaryInfo->UserWalletID, $amount, $dateTimeOfTrans);
    
        // insert data into respective tables
        \App\Models\Transactioneventlog::insert($transctionLog);
         \App\Models\Dailytransactionlog::insert($dailyTransctionLog);
        \App\Models\Notificationlog::insert($notificationLog);
        $transDetails['transactionId'] = $transactionDetailID;
        return $transDetails;
    }

    private function _validateSendMoneyUPI($request)
    {
        $validate = array(
            'partyId'         => 'required|numeric|digits_between:1,10',
            'amount'          => 'required|numeric',
            'businessModelId' => 'required|in:2',
            'walletacntId'    => 'required|numeric|digits_between:1,10',
        );

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private static function insertExternalBeneficiary($partyId, $partyVPA, $beneficiaryVPA)
    {
        $encKey      = sha1(config('vwallet.ENCRYPT_STATIC'));
        $encryVPA = \App\Libraries\Crypt\Encryptor::encrypt($partyVPA, $encKey);
        $encryBenVPA = \App\Libraries\Crypt\Encryptor::encrypt($beneficiaryVPA, $encKey);
        
        $externalBeneficiaryIDs = array(
            'partyVPAId'       => 0,
            'beneficiaryVPAId' => 0
        );

        $insertPartyExtDetails = array(
            'UserWalletID'      => $partyId,
            'BeneficiaryType'   => 'UPI',
            'isTripleClickUser' => 'N',
            'BeneficiaryStatus' => 'Y',
            'BeneficiaryVPA'    => $encryVPA,
            'SelfBenficiary'    => 'Y'
        );

        $externalBeneficiaryIDs['partyVPAId'] = \App\Models\Userexternalbeneficiary::insertGetId($insertPartyExtDetails);

        $insertBenefExtDetails = array(
            'UserWalletID'      => $partyId,
            'BeneficiaryType'   => 'UPI',
            'isTripleClickUser' => 'N',
            'BeneficiaryStatus' => 'Y',
            'BeneficiaryVPA'    => $encryBenVPA,
            'SelfBenficiary'    => 'N'
        );

        $externalBeneficiaryIDs['beneficiaryVPAId'] = \App\Models\Userexternalbeneficiary::insertGetId($insertBenefExtDetails);

        return $externalBeneficiaryIDs;
    }
    
    public static function _dailyTransctionEventDetailsBtoW($transactionDetailID, $paymentProduct, $partyInfo, $banificiaryInfo, $partyId, $amount, $fee, $dateTimeOfTrans)
    {
        $dailyTransctionLog       = array(
            array(
                'TransactionLineNumber'      => 1,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 2,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $banificiaryInfo->WalletAccountId,
                'PartyId'                    => $banificiaryInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $banificiaryInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        $dailyTransctionLogFeeTax = array();
        if ( ( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ) )
        {
            $dailyTransctionLogFeeTax = self::feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans);
        }
        $dailyLog = array_merge($dailyTransctionLog, $dailyTransctionLogFeeTax);

        return $dailyLog;
    }
    
    public static function feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans)
    {
        $feeWalletAcc             = self::getAdminWalletInfo(2);
        $taxWalletAcc             = self::getAdminWalletInfo(3);
        $dailyTransctionLogFeeTax = array(
            array(
                'TransactionLineNumber'      => 3,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $feeWalletAcc->WalletAccountId,
                'PartyId'                    => $feeWalletAcc->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $feeWalletAcc->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['feeAmount'],
                'LocalCcyAmount'             => $fee['feeAmount'],
                'AcctCcyAmount'              => $fee['feeAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'FEE_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 4,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['feeAmount'],
                'LocalCcyAmount'             => $fee['feeAmount'],
                'AcctCcyAmount'              => $fee['feeAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'FEE_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 5,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $taxWalletAcc->WalletAccountId,
                'PartyId'                    => $taxWalletAcc->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $taxWalletAcc->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['taxAmount'],
                'LocalCcyAmount'             => $fee['taxAmount'],
                'AcctCcyAmount'              => $fee['taxAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TAX_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 6,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['taxAmount'],
                'LocalCcyAmount'             => $fee['taxAmount'],
                'AcctCcyAmount'              => $fee['taxAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TAX_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
        );
        return $dailyTransctionLogFeeTax;
    }
    
    public static function getAdminWalletInfo($userId)
    {
        $userExsist = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.PreferredPrimaryCurrency', 'walletaccount.WalletAccountId', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $userId)
                ->first();
        return( ! $userExsist ) ? FALSE : $userExsist;
    }

}
