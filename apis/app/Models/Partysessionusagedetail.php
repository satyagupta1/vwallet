<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partysessionusagedetail
 * 
 * @property int $SessionUsageDetailID
 * @property int $PartyID
 * @property string $AppType
 * @property string $SessionID
 * @property string $IPAddress
 * @property \Carbon\Carbon $SessionStartTime
 * @property \Carbon\Carbon $SessionEndTime
 * @property string $DeviceType
 * @property string $DeviceUniqueID
 * @property string $DeviceToken
 * @property string $DeviceInfo
 * 
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $transactiondetails
 *
 * @package App\Models
 */
class Partysessionusagedetail extends Eloquent
{
	protected $table = 'partysessionusagedetail';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'Id' => 'int'
	];

	/*protected $dates = [
		'SessionStartTime',
		'SessionEndTime'
	];*/

	protected $fillable = [
		'SessionID',
		'EntryDateTime',
		'ModuleID',
		'OtherComments',		
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function transactiondetails()
	{
		return $this->hasMany(\App\Models\Transactiondetail::class, 'UserSessionUsageDetailID');
	}
}
