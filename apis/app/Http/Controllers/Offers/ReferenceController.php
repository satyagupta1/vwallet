<?php

namespace App\Http\Controllers\Offers;

use App\Http\Controllers\Controller;

class ReferenceController extends Controller
{    
    
    /**
     * Display a listing of the reference.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getReferenceData(\Illuminate\Http\Request $request)
    {
        // Default Response
       $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateReference($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }        
        $referenceId            = $request->input('referenceId');       
        $referenceCode            = $request->input('referenceCode');    
        $referenceType       = $request->input('referenceType');
        $activeIndicator       = $request->input('activeIndicator');
          
        
        $clauses = [];
        if($activeIndicator !== null) {
            $clauses = array_merge($clauses,['ActiveIndicator' => $activeIndicator]);
        }
        if($referenceType !== null) {
            $clauses = array_merge($clauses,['ReferenceType' => $referenceType]);
        }
        if ($referenceCode !== null)
        {
           $clauses = array_merge($clauses,['ReferenceCode' => $referenceCode]);
        }
        if ($referenceId !== null)
        {
            $clauses = array_merge($clauses,['ReferenceId' => $referenceId]);
        }       

       $dbResults = \App\Models\Reference::select('ReferenceID', 'ReferenceCode', 'ReferenceDesc', 'ActiveIndicator' )
            ->orderBy('ReferenceId')             
            ->where($clauses)
            ->get();
         if (!$dbResults)
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return $output_arr;
        }     
        
        $output_arr['is_error']            = FALSE;       
        $output_arr['res_data']            = $dbResults;
        return response()->json($output_arr);
    }
    
    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateReference($request)
    {
        $messages = [];
        $validate = [];  
        if ($request->input('referenceType') !== null)
        {
            $validate['referenceType'] = 'required|alpha_num';
        }        
        
        if ($request->input('referenceCode') !== null)
        {
            $validate['referenceCode'] = 'required|alpha_num';
        }
        //if params empy reference id required.
        if($request->input('referenceId') !== null)
        {
            $validate['referenceId'] = 'required|numeric';
        }
        if($request->input('activeIndicator') !== null)
        {
            $validate['activeIndicator'] = 'required|in:Y,N';
        }
        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }   
    
}
