<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Notificationlog
 * 
 * @property int $NotificationID
 * @property int $PartyID
 * @property int $LinkedTransactionID
 * @property string $NotificationType
 * @property \Carbon\Carbon $SentDateTime
 * @property string $ReceipentAcknowledged
 * @property \Carbon\Carbon $ReceipentReceivedDate
 * @property string $ReceipentDeliveryReport
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $ToAddress
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Transaction $transaction
 * @property \App\Models\Notificationlogparameter $notificationlogparameter
 *
 * @package App\Models
 */
class Notificationlog extends Eloquent
{
	protected $table = 'notificationlog';
        protected $primaryKey = 'NotificationID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'NotificationID' => 'int',
		'PartyID' => 'int',
		'LinkedTransactionID' => 'int'
	];

	protected $dates = [
		'SentDateTime',
		'ReceipentReceivedDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'LinkedTransactionID',
		'NotificationType',
		'SentDateTime',
		'ReceipentAcknowledged',
		'ReceipentReceivedDate',
		'ReceipentDeliveryReport',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedBy',
		'UpdatedDateTime',
		'ToAddress'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'LinkedTransactionID');
	}

	public function notificationlogparameter()
	{
		return $this->hasOne(\App\Models\Notificationlogparameter::class, 'NotificationID');
	}
}
