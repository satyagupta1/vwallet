<?php

namespace App\Http\Controllers\Cron;

//use App\Libraries\Communication\Sendemail_lib;
//use App\Libraries\Communication\Sendsms_lib;
//use App\Libraries\Communication\Sendpush_lib;
//use App\Libraries\Communication\Sendweb_lib;

class RemindersCronJob extends \App\Http\Controllers\Controller {

    public function __construct() {
        $this->createdBy = config('vwallet.adminTypeKey');
    }

    public function reminderCronJob(\Illuminate\Http\Request $request) {  
        ini_set('max_execution_time', 180);//3 mins
        \Log::debug('Remind BillPayments Cron Started!');
        $requestInputData = array();
        $params['title']       = 'Remind BillPayments (remindBillPayments)';
        $output = array('is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array());
        $currentDate = date('Y-m-d');
        $joinReminders = \App\Models\Remindernotification::select('reminders.PartyId', 'reminders.RemindTransactionId', 'remindernotification.Title', 'remindernotification.Description', 'reminders.NotificationCycleType', 'reminders.NotificationCyclePeriod', 'remindernotification.ReminderNotificationId', 'reminders.ReminderId')
                        ->join('reminders', 'reminders.ReminderId', '=', 'remindernotification.RemiderId')
                        ->where([['remindernotification.Status', 'Y'],
                            ['reminders.Status', 'Y'] ])
                ->whereDate( 'remindernotification.DateTime', $currentDate )->get();
        
        if ($joinReminders->count() > 0) {
           
        $baseUrl = config('vwallet.baseUrl');        
        
        foreach ($joinReminders as $mainReminders) {
            $billerId = NULL;
            $subscriberNumber = NULL;
            $billerCategory = NULL;
            $actionUrl1 = NULL;
            $transRemind = \App\Models\Remindtransaction::select('BillerId', 'SubscriberNumber', 'BillerCategory','Category')->find($mainReminders->RemindTransactionId);
            if($transRemind) {
                $billerId = $transRemind->BillerId;
                $subscriberNumber = $transRemind->SubscriberNumber;
                $billerCategory = $transRemind->BillerCategory;
                $category = $transRemind->Category;
                    if($category == 'Prepaid') {
                        $actionUrl1 = 'rechargeValidate';
                    }
                    else if ( $category == 'Postpaid' )
                    {
                        $actionUrl1 = 'getBillpaymentDetails';
                    }                
            } 
            // send notification with partyId
            $partyId = $mainReminders->PartyId;
            $getUserData = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'EmailID')->where('UserWalletID', $partyId)->first();
            
            $userMobileNumber = $getUserData->MobileNumber;
            $userEmail = $getUserData->EmailID;           
            $msgTitle = $mainReminders->Title;
            $msgDesc = $mainReminders->Description;
            $reminderNotifyId = $mainReminders->ReminderNotificationId;
            
            // send notification message
            // Communication Start
            $templateData = array(
                'partyId' => $partyId,
                'linkedTransId' => '0', // if transction related notification
                'templateKey' => 'VW065',
                'senderEmail' => $userEmail,
                'senderMobile' => $userMobileNumber,
                'reciverId' => '', // if reciver / sender exsists
                'reciverEmail' => '',
                'reciverMobile' => '',
                'templateParams' => array(
                    'customerName' => ($getUserData['FirstName']) ? $getUserData['FirstName']:'',
                    'description' => $mainReminders->Description,
                    'title' => $mainReminders->Title,
                    'loginLink' => $baseUrl,
                    'reminderNotificationId' => $reminderNotifyId,
                    'billerId' => $billerId,
                    'subscriberNumber' => $subscriberNumber,
                    'billerCategory' => $billerCategory,
                    'actionUrl1' => $actionUrl1,
                )
            );      
            $requestInputData[] = $reminderNotifyId;
            $status = \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
            if($status !== TRUE) {
                $output['display_msg']['info'] = trans('messages.wrong');
                $params['title']       = 'Faild - Remind BillPayments (remindBillPayments)';
                $params['description'] = $output['display_msg']['info'] . ' <br/> Faild Remind Bill Payment Notification:<br/> ' . print_r($templateData, TRUE);
                \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
                return response()->json($output);
            }            
            // update data
            $dataUpdate   = array(
                'Status'          => 'N',
                'UpdatedDateTime' => date('Y-m-d H:i:a'),
                'UpdatedBy'       => 'Admin',
            );

            \App\Models\Remindernotification::where('ReminderNotificationId', $mainReminders->ReminderNotificationId)->update($dataUpdate);

            // End Notification Message
            // notifications end
            // if notification sent then will insert next reminder by calculating with Year Or Month
            $getNotificationType = $mainReminders->NotificationCycleType;   // notif cycle type Y->Year,M->month
            $getNotificationPeriod = $mainReminders->NotificationCyclePeriod;  // period Of Notification 1,2 etc
            // Insert only if reminder is periodic
            if ($getNotificationType == 'Y') {
                // change formats 
                if ($getNotificationType == 'Y') {
                    $timeType = 'years';
                }
                if ($getNotificationType == 'M') {
                    $timeType = 'month';
                }
                if ($getNotificationType == 'D') {
                    $timeType = 'day';
                }

                // calculate next schedule reminder date here
                $NextDate = date('Y-m-d H:00:00', strtotime('+' . $getNotificationPeriod . ' ' . $timeType));

                // insertion of data
                $dataInsert = array(
                    'RemiderId' => $mainReminders->ReminderId,
                    'Status' => 'Y',
                    'DateTime' => $NextDate,
                    'CreatedDateTime' => date('Y-m-d H:i:a'),
                    'CreatedBy' => 'Admin',
                    'UpdatedDateTime' => date('Y-m-d H:i:a'),
                    'UpdatedBy' => 'Admin',
                );

                // insert query to insert next schedule dateTime
                $insertNextRecord = \App\Models\Remindernotification::insert($dataInsert);
            }
        }
        \Log::debug('Remind BillPayments Cron Success!');
        $output['is_error'] = FALSE;
        $output['display_msg']['info'] = trans('messages.requestSuccessfull');
        }
        $params['description'] = $output['display_msg']['info'] . ' <br/> Remind BillPayments Transactions Data : <br/>' . print_r($requestInputData, TRUE);
        \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
        return response()->json($output);
    }

}
