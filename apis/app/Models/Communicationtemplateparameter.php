<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Communicationtemplateparameter
 * 
 * @property int $TemplateID
 * @property string $Parameter
 * @property string $ParameterValue
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Communicationtemplate $communicationtemplate
 *
 * @package App\Models
 */
class Communicationtemplateparameter extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TemplateID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ParameterValue',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function communicationtemplate()
	{
		return $this->belongsTo(\App\Models\Communicationtemplate::class, 'TemplateID');
	}
}
