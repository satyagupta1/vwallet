<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantwalletregistration
 * 
 * @property int $MerchantID
 * @property string $ReferralCode
 * @property int $ReferralMerchantID
 * @property string $EmailVerified
 * @property string $MobileVerified
 * @property string $TermsAccepted
 * @property \Carbon\Carbon $TermsAcceptedDatetime
 * @property string $IPAddress
 * @property string $CompanyName
 * @property string $BusinessOwnership
 * @property string $KindofBusiness
 * @property string $DateofBirth
 * @property string $PanCardName
 * @property string $PanCardNumber
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 * @property \Illuminate\Database\Eloquent\Collection $merchanttransactiondetails
 *
 * @package App\Models
 */
class Merchantwalletregistration extends Eloquent
{
	protected $table = 'merchantwalletregistration';
	protected $primaryKey = 'MerchantID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int',
		'ReferralMerchantID' => 'int'
	];

	protected $dates = [
		'TermsAcceptedDatetime',
		'CreatedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'ReferralCode',
		'ReferralMerchantID',
		'EmailVerified',
		'MobileVerified',
		'TermsAccepted',
		'TermsAcceptedDatetime',
		'IPAddress',
		'CompanyName',
		'BusinessOwnership',
		'KindofBusiness',
		'DateofBirth',
		'PanCardName',
		'PanCardNumber',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDatetime'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'ReferralMerchantID');
	}

	public function merchanttransactiondetails()
	{
		return $this->hasMany(\App\Models\Merchanttransactiondetail::class, 'MerchantID');
	}
}
