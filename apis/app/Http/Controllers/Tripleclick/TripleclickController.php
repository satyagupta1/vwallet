<?php

/**
 * TripleclickController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\Tripleclick;

/**
 * TripleclickController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TripleclickController extends \App\Http\Controllers\Controller {
    /*
     *  intiateTransction - Intiating Triple click Transction
     *  @param object $request
     *  @return JSON
     */

    public function intiateTransction(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->_checkRequestInfo($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }

        $partyId           = $request->input('partyId');
        //$beneficiaryID   = $request->input('beneficiaryId');
        $amount            = $request->input('amount');
        $businessModelid   = $request->input('businessModelid');
        $transStaus        = config('vwallet.transactionStatus.pending');
        $transType         = $request->input('transType');
        $saveBenf          = $request->input('saveBenf');
        $productEventId    = 3;
        $beneficiarymobile = $request->input('mobileNumber');
        $transBool         = TRUE;
        $benfType          = 'eWallet';
        $nickName          = $request->input('nickName');

        // user exsistens test
        $partyInfo = \App\Libraries\TransctionHelper::_userValidate($partyId);

        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        // Beneficiary Check
        $checkBeneficiary = \App\Libraries\TransctionHelper::_userIdValidate($beneficiarymobile, 'mobile');

        if ( $checkBeneficiary === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        if ( $partyId == $checkBeneficiary->UserWalletID )
        {
            $message                   = trans('messages.partySame');
            $output_arr['display_msg'] = array( 'info' => $message );
            return json_encode($output_arr);
        }

        $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiarymobile, $benfType);

        $AccountName          = $checkBeneficiary->FirstName;
        $userWalletIdBenf     = $checkBeneficiary->WalletAccountId;
        $partyIDBenf          = $checkBeneficiary->UserWalletID;
        $yesBankBeneficiaryId = isset($banificiaryInfo->yesBankBeneficiaryId) ? $banificiaryInfo->yesBankBeneficiaryId : '';
        $senderMobile         = $partyInfo->MobileNumber;
        if ( ($banificiaryInfo == FALSE) || ($yesBankBeneficiaryId == '') )
        {
            // calling add benenficiary 
            $IdentifierType = 'mobile';
            $identifier1    = $beneficiarymobile;
            $benId          = '';
            $extraParams    = array(
                'partyId'        => $partyId,
                'referenceId'    => $benId,
                'referenceTable' => 'userbeneficiary',
                'status'         => 'Initiated'
            );

            $ybkReq        = array(
                'mobile'         => $senderMobile,
                'benifisaryName' => ( ! $nickName) ? $nickName : '',
                'IdentifierType' => 'mobile',
                'identifier1'    => $identifier1,
                'identifier2'    => '',
            );
            $outputRespo   = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);
            $reqForYesBank = json_decode($outputRespo);

            if ( isset($reqForYesBank->beneficiary) )
            {
                $YbBeneficiaryId = $reqForYesBank->beneficiary->id;
            }
            else
            {
                if ( $reqForYesBank->status_code == 'ERROR' )
                {
                    $output_arr['display_msg'] = array( 'info' => $reqForYesBank->message );
                    return $output_arr;
                }
                $YbBeneficiaryId = $reqForYesBank->beneficiary_id;
            }


            $this->addBenficiary($request, $AccountName, $partyId, $partyIDBenf, 'eWallet', $YbBeneficiaryId, $saveBenf);

            $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiarymobile, $benfType);
        }


        // calling YesBank Api
        $SendermobileNumber = $partyInfo->MobileNumber;

        $yesBankApi = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($SendermobileNumber);

        if ( $yesBankApi['is_error'] == TRUE )
        {
            return response()->json($yesBankApi);
        }
        $yesBankApiRes = $yesBankApi['res_data'];
        if ( $yesBankApiRes['remaining_load_limit'] < $amount )
        {
            $output_arr['display_msg'] = array( 'info' => 'You Exceeded Your Monthly Limit' );
            return $output_arr;
        }

        if ( $benfType == 'eWallet' )
        {
            $yesBankApiBenficiary = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($beneficiarymobile);

            if ( $yesBankApiBenficiary['is_error'] == TRUE )
            {
                return response()->json($yesBankApiBenficiary);
            }
            $yesBankApiBenfData = $yesBankApiBenficiary['res_data'];
            if ( $yesBankApiBenfData['remaining_load_limit'] < $amount )
            {
                $output_arr['display_msg'] = array( 'info' => 'You Benificiary Exceeded Your Monthly Limit' );
                return $output_arr;
            }
        }

        // check Balance
        if ( $partyInfo->CurrentAmount < $amount )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output_arr;
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ( $partyTransLimit['error'] )
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // check Benifisary transction or wallet credit limits 
        if ( $transType == 'WW' ) // it is wallet to wallet
        {
            $benificaryTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($checkBeneficiary->UserWalletID, $checkBeneficiary->WalletPlanTypeId, $amount, TRUE);
            if ( $benificaryTransLimit['error'] )
            {
                $output_arr['display_msg'] = $benificaryTransLimit;
                return $output_arr;
            }
        }

        // check tripleclik amounts are not 
        $tripleAmounts = $banificiaryInfo->TripleClickAmount;

        $amountCheck = $this->_checkTripleClickAmout($tripleAmounts, $amount, $banificiaryInfo->BeneficiaryID);
        if ( $amountCheck === FALSE )
        {
            $tripleClick               = trans('messages.attributes.tripleClick');
            $output_arr['display_msg'] = array( 'info' => trans('messages.amountNotTripleClik', [ 'name' => $tripleClick ]) );
            return $output_arr;
        }

        //  $sesionId       = 'sddd';
        // session Check
        $sesionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if ( is_bool($sesionId) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidSession') );
            return $output_arr;
        }
//        
        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo(3);

        // fee calculation 
        $fee      = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);
//        print_r($fee); exit;
        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelid, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transBool, $sesionId);

        $transactionDetails                            = $transRes['response']['TransactionDetails'];
        $transactionDetails['AccountName']             = $banificiaryInfo->FirstName . ' ' . $banificiaryInfo->LastName;
        $transactionDetails['BeneficiaryMobileNumber'] = $request->input('mobileNumber');
        \App\Libraries\Helpers::_sendMoneyCommunication($request, $partyInfo, $banificiaryInfo, $transactionDetails);

        if ( $transRes['error'] )
        {
            $output_arr['display_msg'] = array( 'info' => $transRes['info'] );
            return $output_arr;
        }
        $output_arr['display_msg'] = array( 'info' => $transRes['info'] );
        $output_arr['is_error']    = FALSE;
        $output_arr['res_data']    = $transRes['response'];
        return $output_arr;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _beneficiaryValidate($BeneficiaryID)
    {
        $selectArray       = array(
            'userwallet.FirstName',
            'userwallet.LastName',
            'userwallet.UserWalletID',
            'userbeneficiary.BeneficiaryWalletID',
            'userwallet.MobileNumber',
            'walletaccount.WalletAccountId',
            'walletaccount.WalletAccountId',
            'walletaccount.WalletAccountNumber',
            'walletaccount.AvailableBalance',
            'walletaccount.CurrentAmount',
            'userwallet.PreferredPrimaryCurrency',
            'userwallet.IsVerificationCompleted',
            'userwallet.WalletPlanTypeId',
            'userbeneficiary.BeneficiaryWalletID',
            'userbeneficiary.TripleClickAmount',
            'userbeneficiary.isTripleClickUser'
        );
        $beneficiaryExsist = \App\Models\Userbeneficiary::select($selectArray)
                ->join('userwallet', 'userwallet.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                ->where('userbeneficiary.BeneficiaryID', $BeneficiaryID)
                ->where('userbeneficiary.BeneficiaryType', 'eWallet')
                ->where('userbeneficiary.isTripleClickUser', 'Y')
                ->first();

        return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
    }

    private function _checkTripleClickAmout($tripleAmounts, $amount, $benfId)
    {
        if ( $tripleAmounts )
        {

            $trpAmounts = \App\Models\Userbeneficiary::select('TripleClickAmount')->where('BeneficiaryID', $benfId)->first();
            $tripleAmounts = $trpAmounts->TripleClickAmount;
        }
        else
        {
            $trpAmounts    = \App\Models\Systemconfiguration::select('SecDescription')->where('SecKey', 'tcAmounts')->first();
            $tripleAmounts = $trpAmounts->SecDescription;
        }
        $tripleClickAmounts = explode(',', $tripleAmounts);

        return ( in_array($amount, $tripleClickAmounts) ) ? TRUE : FALSE;
    }

    /*
     *  _checkRequestInfo    - Input validation Method
     *  @param object $request
     *  @return Boolean
     */

    private function _checkRequestInfo($request)
    {
        $validations = array(
            'partyId'         => 'required|integer|digits_between:1,10',
            // 'beneficiaryId'   => 'required|numeric|digits_between:1,10',
            'mobileNumber'    => 'required|numeric',
            'amount'          => 'required|numeric',
            'businessModelid' => 'required|numeric',
            'transType'       => 'required|in:WW',
        );

        return $this->_validate_method($request, $validations);
    }

    private function _validate_method($request, $validations)
    {
        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function addBenficiary($request, $AccountName, $userWalletId, $beneficiaryUserId, $benfType, $YbBneficiaryId, $saveBenf)
    {
        $insertBeneficiryData = array(
            'BeneficiaryWalletID'      => $beneficiaryUserId,
            'UserWalletID'             => $userWalletId,
            'BeneficiaryStatus'        => 'N',
            'BeneficiaryType'          => $benfType,
            'BeneficiaryMobileNumber'  => ($request->input('mobileNumber')) ? $request->input('mobileNumber') : '',
            'WalletBeneficiaryName'    => $request->input('nickName'),
            'isTripleClickUser'        => 'N',
            'BankIFSCCode'             => ($request->input('ifscCode')) ? $request->input('ifscCode') : '',
            'MMID'                     => '',
            'AccountName'              => ($AccountName == '') ? $AccountName : '',
            'AccountNumber'            => ($request->input('accountNo')) ? $request->input('accountNo') : '',
            'BeneficiaryVPA'           => '',
            'CreatedDateTime'          => date('Y-m-s H:i:s'),
            'yesBankBeneficiaryId'     => $YbBneficiaryId,
            'yesBankBeneficiaryStatus' => 'Y',
            'BeneficiaryStatus'        => $saveBenf
        );

        $insertBeneficiry = \App\Models\Userbeneficiary::insertGetId($insertBeneficiryData);
        return $benId            = $insertBeneficiry;
    }

}

/* End of file TripleclickController.php */ /* Location: ./Http/Test/TripleclickController.php */