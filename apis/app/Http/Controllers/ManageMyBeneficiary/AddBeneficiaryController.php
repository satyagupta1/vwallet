<?php

namespace App\Http\Controllers\ManageMyBeneficiary;

use App\Http\Controllers\Controller;

/**
 * AddBeneficiryController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class AddBeneficiaryController extends Controller {

    public function __construct()
    {
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
    }

    /*
     *  searchBeneficiry - Validate Beneficiry with mobile number
     *  @param Request $request
     *  @return JSON
     */

    public function addBeneficiary(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr          = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $communicationParams = array();

        $checkValidate = $this->_validatesearchBeneficiary($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return $output_arr;
        }

        $partyId          = $request->input('partyId');
        $NickName         = $request->input('nickName');
        $beneficieryType  = $request->input('beneficiaryType');
        $mobileNumber     = $request->input('mobileNumber');
        $benificaryNumber = $request->input('walletMobile');

        $params     = array( 'partyId' => $partyId, 'vPin' => $request->input('vPin') );
        $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
        if ( $verifyVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
            return response()->json($output_arr);
        }

        if ( $mobileNumber == $benificaryNumber )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.userMobileAsBenificiary') );
            return response()->json($output_arr);
        }

        $checkBenficiary = array();
        if ( $benificaryNumber != NULL )
        {
            // check benifisary is a viola wallet user or not
            $checkBenficiary = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'MiddleName', 'LastName', 'EmailID', 'MobileNumber')
                    ->where('MobileNumber', '=', $benificaryNumber)
                    ->where('ProfileStatus', '=', 'A')
                    ->first();
            if ( ! $checkBenficiary )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.benifisaryNotViolaUser') );
                return response()->json($output_arr);
            }
        }

        $beneficirycheckNickname = \App\Models\Userbeneficiary::
                        where('WalletBeneficiaryName', $request->input('nickName'))
                        ->where('BeneficiaryStatus', '=', 'Y')
                        ->where('yesBankBeneficiaryStatus', '=', 'Y')
                        ->where('UserWalletID', $request->input('partyId'))->count();

        if ( $beneficirycheckNickname > 0 )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNickNameExists') );
            return response()->json($output_arr);
        }

        $userDetails = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'LastName', 'EmailID', 'MobileNumber', 'ViolaID', 'MasterUserId', 'ProfileStatus', 'WalletPlanTypeId', 'IsSubAccount', 'PreferredPrimaryCurrency')
                ->where('UserWalletID', '=', $partyId)
                ->where('ProfileStatus', '=', 'A')
                ->first();

        if ( ! $userDetails )
        {
            $output_arr['display_msg'] = array( 'info' => 'User doesn`t exist.' );
            return response()->json($output_arr);
        }

        $userExist      = FALSE;
        $IdentifierType = 'mobile';
        if ( $beneficieryType == 'eWallet' )
        {
            if ( $userDetails )
            {
                if ( ($userDetails->IsSubAccount == 'Y') && ($userDetails->MasterUserId != $partyId) )
                {
                    $output_arr['display_msg'] = array( 'info' => trans('messages.cantAddThisBenificiary') );
                    return response()->json($output_arr);
                }
                $userExist = TRUE;
            }
        }

        if ( $beneficieryType == 'Bank' OR $beneficieryType == 'Vcard' )
        {
            $userExist = TRUE;
        }

        if ( $beneficieryType == 'UPI' )
        {
            $userExist = TRUE;
        }


        if ( $userExist === TRUE )
        {
            $where                      = array( 'UserWalletID' => $partyId, 'yesBankBeneficiaryStatus' => 'Y' );
            $where['BeneficiaryType']   = $beneficieryType;
            $where['BeneficiaryStatus'] = 'Y';
            $identifier2                = '';
            if ( $beneficieryType == 'eWallet' || $beneficieryType == 'Vcard' )
            {
                $IdentifierType                   = 'mobile';
                $identifier1                      = $benificaryNumber;
                $where['BeneficiaryMobileNumber'] = $benificaryNumber;
            }
            else if ( $beneficieryType == 'Bank' )
            {
                $IdentifierType         = 'bank';
                $identifier1            = $request->input('accountNumber');
                $identifier2            = $request->input('ifscCode');
                $where['AccountNumber'] = $request->input('accountNumber');
            }
            else if ( $beneficieryType == 'UPI' )
            {
                $IdentifierType          = 'VPA';
                $identifier1             = $request->input('benificiaryVPA');
                $identifier2             = $request->input('ifscCode');
                $where['BeneficiaryVPA'] = $request->input('benificiaryVPA');
            }

            $beneficiryDetails = \App\Models\Userbeneficiary::where($where)->get();

            $beneficiryData = $beneficiryDetails->count();
            if ( $beneficiryData > 0 )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryExists') );
            }
            else
            {
                $addBeneficiaryResult = $this->_adduserBeneficiary($request, $checkBenficiary, 'add');
                \Log::debug(json_encode($addBeneficiaryResult));
                if ( $addBeneficiaryResult['is_error'] === FALSE )
                {
                    $benId       = $addBeneficiaryResult['res_data']['BeneficiaryID'];
                    $extraParams = array(
                        'partyId'        => $partyId,
                        'referenceId'    => $benId,
                        'referenceTable' => 'userbeneficiary',
                        'status'         => 'Initiated'
                    );

                    $ybkReq        = array(
                        'mobile'         => $mobileNumber,
                        'benifisaryName' => ( ! $NickName) ? $NickName : '',
                        'IdentifierType' => $IdentifierType,
                        'identifier1'    => $identifier1,
                        'identifier2'    => ($IdentifierType != 'mobile') ? $identifier2 : '',
                    );
                    $reqForYesBank = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);

                    $templetKey                = 'VW087';
                    $response                  = json_decode($reqForYesBank);
                    $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryAdded') );
                    if ( ( isset($response->code)) && $response->code != '00' )
                    {
                        $templetKey                = 'VW086';
                        $output_arr['display_msg'] = $response->message;
                    }
                    else
                    {
                        $output_arr['is_error'] = FALSE;
                        $update                 = array();
                        $yesBenId               = 0;
                        if ( isset($response->beneficiary) )
                        {
                            $yesBenId = $response->beneficiary->id;
                            $update   = array(
                                'yesBankBeneficiaryStatus' => 'Y',
                                'yesBankBeneficiaryId'     => $response->beneficiary->id,
                                'BeneficiaryStatus'        => 'Y'
                            );
                        }
                        else
                        {
                            $yesBenId = $response->beneficiary_id;
                            $update   = array(
                                'yesBankBeneficiaryStatus' => 'Y',
                                'yesBankBeneficiaryId'     => $response->beneficiary_id,
                                'BeneficiaryStatus'        => 'Y'
                            );
                        }
                        \Log::debug(json_encode($update));
                        /*
                          $extraParams1 = array(
                          'partyId'        => $partyId,
                          'referenceId'    => $benId,
                          'referenceTable' => 'userbeneficiarylimits',
                          'status'         => 'Initiated'
                          );

                          $ybkReq1 = array(
                          'mobile'                 => $mobileNumber,
                          'beneficiaryId'          => $identifier1,
                          'maxMonthlyAllowedLimit' => config('vwallet.beneficiaryLimit'),
                          );

                          $reqToUpdateLimit = \App\Libraries\Wallet\Beneficiary::editBenificiary($ybkReq1, $extraParams1);
                          $limitResponse    = json_decode($reqToUpdateLimit);
                          if ( $response->code != '00' )
                          {
                          $output_arr['display_msg'] = $response->message;
                          }
                          else
                          {

                          }
                         * 
                         */

                        $beneficiaryLimitArray = array(
                            'BeneficiaryID'        => $benId,
                            'yesBankBeneficiaryId' => $yesBenId,
                            'UserWalletID'         => $partyId,
                            'maxPermissibleLimit'  => 10000,
                            'monthlyLimitAvailed'  => 0
                        );
                        \App\Models\Userbeneficiarylimit::insert($beneficiaryLimitArray);
                        \App\Models\Userbeneficiary::where('BeneficiaryID', $benId)->update($update);
                    }

                    $templateData = array(
                        'partyId'        => $partyId,
                        'linkedTransId'  => '0', // if transction related notification
                        'templateKey'    => $templetKey,
                        'senderEmail'    => $userDetails->EmailID,
                        'senderMobile'   => $userDetails->MobileNumber,
                        'reciverId'      => '', // if reciver / sender exsists
                        'reciverEmail'   => '',
                        'reciverMobile'  => '',
                        'templateParams' => array(
                            'fullname'       => ($userDetails->FirstName) ? $userDetails->FirstName : '',
                            'IdentifierType' => $IdentifierType,
                            'identifier1'    => $identifier1,
                            'supportURL'     => "violamoney.com",
                        )
                    );
                    \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);

                    return $output_arr;
                }
                else
                {
                    $output_arr['display_msg'] = $addBeneficiaryResult['display_msg'];
                }
            }
        }
        else
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
        }


        return response()->json($output_arr);
    }

    public function fetchBenificiary(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr          = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $communicationParams = array();

        $checkValidate = $this->_validateYBLBeneficiary($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return $output_arr;
        }

        $partyId       = $request->input('partyId');
        $beneficiaryId = $request->input('beneficiaryId');
//        $amount        = $request->input('limit');
        // check user is a viola wallet user or not
        $checkUser     = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'MiddleName', 'LastName', 'EmailID', 'MobileNumber')
                ->where('UserWalletID', '=', $partyId)
                ->where('ProfileStatus', '=', 'A')
                ->first();
        if ( ! $checkUser )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.userNotFound') );
            return response()->json($output_arr);
        }

        // Beneficiary Check
        $userBeneficiaryId = \App\Models\Userbeneficiary::select('BeneficiaryType', 'AccountName', 'yesBankBeneficiaryId', 'yesBankBeneficiaryStatus')
                ->where('UserWalletID', $partyId)
                ->where('BeneficiaryID', $beneficiaryId)
                ->first();
        if ( $userBeneficiaryId === FALSE || ($userBeneficiaryId->yesBankBeneficiaryId == NULL) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
            return $output_arr;
        }

        $extraParams = array(
            'partyId'        => $partyId,
            'referenceId'    => $beneficiaryId,
            'referenceTable' => 'userbeneficiary',
            'status'         => 'Initiated'
        );

        $ybkReq = array(
            'mobile'        => $checkUser->MobileNumber,
            'BeneficiaryId' => $userBeneficiaryId->yesBankBeneficiaryId
        );

        $reqForYesBank             = \App\Libraries\Wallet\Beneficiary::fetchBenificiary($ybkReq, $extraParams);
        print_r($reqForYesBank);
        exit;
        $templetKey                = 'VW087';
        $response                  = json_decode($reqForYesBank);
        $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryAdded') );
        if ( $response->code != '00' )
        {
            $templetKey                = 'VW086';
            $output_arr['display_msg'] = $response->message;
        }
        else
        {
            
        }
    }

    private function _validateYBLBeneficiary($request)
    {
        $validations = array(
            'partyId'       => 'required|integer|digits_between:1,10',
            'beneficiaryId' => 'required|numeric|digits_between:1,10',
//            'limit'         => 'required'
        );

        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _validatesearchBeneficiary($request)
    {

        $validate = array(
//            'mobileNumber'    => 'required|numeric|digits:10',
            'nickName'        => 'required|regex:/^[a-zA-z0-9]+([a-zA-Z0-9\ ]+)?+$/',
            'partyId'         => 'required|numeric|digits_between:1,10',
            'beneficiaryType' => 'required|in:eWallet,Bank,Vcard,UPI'
        );

        if ( $request->input('beneficiaryType') == 'eWallet' || $request->input('beneficiaryType') == 'Vcard' )
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
            $validate['walletMobile'] = 'required|numeric|digits:10';
        }

        if ( $request->input('beneficiaryType') == 'Bank' )
        {
            $validate['ifscCode']      = 'required';
            $validate['accountName']   = 'required|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/';
            $validate['accountNumber'] = 'required|min:10|max:20|regex:/^[a-zA-Z0-9]+$/';
        }

        if ( $request->input('beneficiaryType') == 'UPI' )
        {
            $validate['ifscCode']       = 'required';
            $validate['benificiaryVPA'] = 'required|min:5|max:45|regex:/^[a-zA-Z0-9@.]+$/';
        }

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _adduserBeneficiary($request, $userDetails, $type = 'edit')
    {
        // Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateAddUpdateBeneficiry($request, 'add');

        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
        }
        else
        {
            $partyID    = $request->input('partyId');
            $params     = array( 'partyId' => $partyID, 'vPin' => $request->input('vPin') );
            $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
            if ( $verifyVpin === FALSE )
            {
                $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
                return $output_arr;
            }

            if ( $request->input('beneficiaryType') == 'eWallet' )
            {
                $userWalletId = $userDetails->UserWalletID;
            }
            if ( $request->input('beneficiaryType') == 'Vcard' )
            {
                $userWalletId = '0';
            }
            if ( ( $request->input('beneficiaryType') == 'Bank' ) || ($request->input('beneficiaryType') == 'UPI') )
            {
                $ifscValidate = \App\Models\Ifsccode::select('code')
                                ->where('code', '=', $request->input('ifscCode'))->first();
                if ( ! $ifscValidate )
                {
                    $ifscLang                  = trans('messages.attributes.ifsc');
                    $output_arr['display_msg'] = array( 'ifscCode' => trans('messages.invalidIFSCCode', [ 'name' => $ifscLang ]) );
                    return $output_arr;
                }
                $userWalletId = 0;
            }

            if ( ($request->input('tripleClik') == '') || ($request->input('tripleClik') == 'N') )
            {
                $tripleClik      = 'N';
                $tipleClikAmount = '';
            }
            else
            {
                $tripleClik      = 'Y';
                $tipleClikAmount = implode(',', [ 50, 100, 500, 1000, 2000, 5000 ]);
            }


            if ( $request->input('beneficiaryType') == 'Bank' OR $request->input('beneficiaryType') == 'Vcard' OR $request->input('beneficiaryType') == 'UPI' )
                $AccountName = $request->input('accountName');
            else
                $AccountName = $userDetails->FirstName . (($userDetails->MiddleName) ? ' ' . $userDetails->MiddleName : '') . ' ' . $userDetails->LastName;

            $insertBeneficiryData = array(
                'BeneficiaryWalletID'     => $userWalletId,
                'UserWalletID'            => $partyID,
                'BeneficiaryStatus'       => 'N',
                'BeneficiaryType'         => $request->input('beneficiaryType'),
                'BeneficiaryMobileNumber' => ($request->input('walletMobile')) ? $request->input('walletMobile') : '',
                'WalletBeneficiaryName'   => $request->input('nickName'),
                'isTripleClickUser'       => $tripleClik,
                'TripleClickAmount'       => $tipleClikAmount,
                'BankIFSCCode'            => ($request->input('ifscCode')) ? $request->input('ifscCode') : '',
                'MMID'                    => ($request->input('mmid')) ? $request->input('mmid') : '',
                'AccountName'             => $AccountName,
                'AccountNumber'           => ($request->input('accountNumber')) ? $request->input('accountNumber') : '',
                'BeneficiaryVPA'          => ($request->input('benificiaryVPA')) ? $request->input('benificiaryVPA') : '',
                'CreatedDateTime'         => date('Y-m-s H:i:s'),
            );


            if ( $type == 'add' )
            {
                $insertBeneficiry = \App\Models\Userbeneficiary::insertGetId($insertBeneficiryData);
                $benId            = $insertBeneficiry;
            }
            else
            {
                $insertBeneficiry->save($insertBeneficiryData);
                $benId = $insertBeneficiry->id;
            }
            if ( $insertBeneficiry )
            {
                $output_arr['is_error'] = FALSE;
//                $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryAdded') );
                $output_arr['res_data'] = array( 'BeneficiaryID' => $benId );
            }
            else
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.wrong') );
            }
        }

        return $output_arr;
    }

    private function _validateAddUpdateBeneficiry($request, $validate_type)
    {
        $validate = array(
            'vPin' => 'required|numeric|digits:4',
        );

        $messages = array(
            'vPin'         => 'Please enter V-PIN',
            'vPin.numeric' => 'V-PIN Should be numeric',
            'vPin.digits'  => 'V-PIN should be 4 digits',
        );

        if ( $request->input('beneficiaryType') == 'add' )
        {
            $validate['tripleClik'] = 'required|in:Y,N';

            $messages['tripleClik.required'] = 'Triple-Clik is required';
            $messages['tripleClik.in']       = 'Triple-Clik should be Y or N';
        }

        if ( $validate_type == 'update' )
        {
            $validate['partyId']         = 'required|numeric|digits_between:1,10';
            $validate['beneficiaryId']   = 'required|numeric';
            $validate['nickName']        = 'required|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/';
//            $validate['transctionLimit'] = 'required|numeric';

            $messages['partyId.required']       = 'Please enter valid Party ID';
            $messages['partyId.numeric']        = 'Enter valid Party ID';
            $messages['partyId.digits_between'] = 'Enter valid Party ID';
            $messages['beneficiaryId.required'] = 'Please Enter Beneficiary ID';
            $messages['beneficiaryId.numeric']  = 'Beneficiary ID should be numeric';
            $messages['nickName.required']      = 'Please enter nick name';
            $messages['nickName.regex']         = 'Nick name should be Characters';
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /*
     * edit beneficiary with yes bank limit change
      public function editBeneficiary(\Illuminate\Http\Request $request)
      {
      $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
      $checkValidate = $this->_validateAddUpdateBeneficiry($request, 'update');
      //$checkValidate = TRUE;
      if ( $checkValidate !== TRUE )
      {
      $output_arr['display_msg'] = $checkValidate;
      return response()->json($output_arr);
      }

      $partyId    = $request->input('partyId');
      $params     = array( 'partyId' => $partyId, 'vPin' => $request->input('vPin') );
      $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
      if ( $verifyVpin === FALSE )
      {
      $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
      return response()->json($output_arr);
      }

      $selectArray       = array(
      'userbeneficiary.BeneficiaryID',
      'userbeneficiary.WalletBeneficiaryName',
      'userbeneficiary.yesBankBeneficiaryId',
      'userbeneficiarylimits.maxPermissibleLimit',
      'userbeneficiarylimits.beneficiaryLimitId',
      'userwallet.MobileNumber'
      );
      $insertBeneficiary = \App\Models\Userbeneficiary::select($selectArray)
      ->join('userwallet', 'userbeneficiary.UserWalletID', '=', 'userwallet.UserWalletID')
      ->join('userbeneficiarylimits', 'userbeneficiary.BeneficiaryID', '=', 'userbeneficiarylimits.BeneficiaryID')
      ->where('userbeneficiary.BeneficiaryID', $request->input('beneficiaryId'))->first();

      if ( ! $insertBeneficiary )
      {
      $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
      return response()->json($output_arr);
      }

      if ( $insertBeneficiary->WalletBeneficiaryName != $request->input('nickName') )
      {
      $updateArray = array(
      'WalletBeneficiaryName' => $request->input('nickName'),
      'UpdatedDateTime'       => date('Y-m-d H:i:s'),
      'UpdatedBy'             => 'User'
      );
      \App\Models\Userbeneficiary::where('BeneficiaryID', $insertBeneficiary->BeneficiaryID)->update($updateArray);
      }

      if ( $insertBeneficiary->maxPermissibleLimit == $request->input('transctionLimit') )
      {
      $output_arr['display_msg'] = array( 'info' => trans('messages.limitAlreadySet') );
      return response()->json($output_arr);
      }
      else
      {
      $transLimit   = $request->input('transctionLimit');
      $extraParams1 = array(
      'partyId'        => $partyId,
      'referenceId'    => $insertBeneficiary->BeneficiaryID,
      'referenceTable' => 'userbeneficiarylimits',
      'status'         => 'Initiated'
      );

      $ybkReq1 = array(
      'mobile'                 => $insertBeneficiary->MobileNumber,
      'beneficiaryId'          => $insertBeneficiary->yesBankBeneficiaryId,
      'maxMonthlyAllowedLimit' => $transLimit,
      );

      $reqToUpdateLimit = \App\Libraries\Wallet\Beneficiary::editBenificiary($ybkReq1, $extraParams1);
      $limitResponse    = json_decode($reqToUpdateLimit);
      \Log::debug($reqToUpdateLimit);
      if ( $limitResponse->code != '00' )
      {
      $output_arr['display_msg'] = $limitResponse->message;
      }
      else
      {
      $updateLimitArray = array(
      'maxPermissibleLimit' => $transLimit,
      'UpdatedDateTime'     => date('Y-m-d H:i:s'),
      'UpdatedBy'           => 'User'
      );
      \App\Models\Userbeneficiarylimit::where('beneficiaryLimitId', $insertBeneficiary->beneficiaryLimitId)->update($updateLimitArray);
      }
      }

      $output_arr['is_error']    = FALSE;
      $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryUpdated') );



      return response()->json($output_arr);
      }
     * 
     */

    public function editBeneficiary(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateAddUpdateBeneficiry($request, 'update');
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
        }
        else
        {
            $partyID    = $request->input('partyId');
            $params     = array( 'partyId' => $partyID, 'vPin' => $request->input('vPin') );
            $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
            if ( $verifyVpin === FALSE )
            {
                $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
            }
            else
            {
                $insertBeneficiary = \App\Models\Userbeneficiary::find($request->input('beneficiaryId'));
                if ( $insertBeneficiary != FALSE )
                {
                    $insertBeneficiary->first();
                    if ( $insertBeneficiary->WalletBeneficiaryName == $request->input('nickName') )
                    {
                        $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryExists') );
                        return response()->json($output_arr);
                    }
                    $insertBeneficiary->WalletBeneficiaryName = $request->input('nickName');
                    $insertBeneficiary->UpdatedDateTime       = date('Y-m-d H:i:s');
                    $insertBeneficiary->save();

                    $output_arr['is_error']    = FALSE;
                    $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryUpdated') );
                }
                else
                {
                    $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
                }
            }
        }

        return response()->json($output_arr);
    }

    /*
     *  updateTripleClikAmount - to update benifisary custom amounts
     *  @param Request $request
     *  @return JSON
     */

    public function updateTripleClikAmount(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->_validateBenificary($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }

        // user exsistens test
        $partyId       = $request->input('partyId');
        $beneficiaryId = $request->input('beneficiaryId');
        $amountList    = $request->input('amountList');

        if ( $request->header('device-type') == 'A' )
        {
            $amtList    = json_decode($amountList);
            $amountList = $amtList->Values;
        }

        $userCheck = $this->_userIdValidate($partyId);
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        // Beneficiary Check
        $userBeneficiaryId = $this->_beneficiaryValidate($beneficiaryId, $partyId);
        if ( $userBeneficiaryId === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
            return $output_arr;
        }

        if ( ( ! is_array($amountList)) || (count($amountList) != 6) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.amountsInvalid') );
            return $output_arr;
        }
        $amountList  = ( array ) $amountList;
        // check all array values are numerics
        $all_numeric = true;
        foreach ( $amountList as $key )
        {
            if ( is_float($key) )
            {
                $all_numeric = false;
                break;
            }
        }
        if ( ! $all_numeric )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.amountsNotInteger') );
            return $output_arr;
        }

        // check array has unique 6 values
        if ( count($amountList) !== count(array_unique($amountList)) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.amountsDuplicate') );
            return $output_arr;
        }

        $per_txn_max = config('vwallet.PER_TXN_MAX');
        // Max amount allowed
        if ( max($amountList) > $per_txn_max )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.maxTransferPerTransaction', [ 'name' => $per_txn_max ]) );
            return $output_arr;
        }

        // Min amount allowed
        if ( min($amountList) <= 0 )
        {
            $tripleClick               = trans('messages.attributes.tripleClick');
            $output_arr['display_msg'] = array( 'info' => trans('messages.tripleclikGreaterThanZero', [ 'name' => $tripleClick ]) );
            return $output_arr;
        }

        $tripleClikAmounts = implode(',', $amountList);

        \App\Models\Userbeneficiary::where('BeneficiaryID', $beneficiaryId)->update([ 'TripleClickAmount' => $tripleClikAmounts ]);
        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.benificiaryAmountUpdated') ), 'status_code' => 200, 'res_data' => array( 'TripleClickAmount' => $tripleClikAmounts ) );

        return $output_arr;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _userIdValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', '=', $partyId)->where('ProfileStatus', '=', 'A');
        if ( ! $userExsist )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    private function _beneficiaryValidate($BeneficiaryID, $partyId, $beneficiaryType = '')
    {
        $beneficiaryExsist = \App\Models\Userbeneficiary::select('BeneficiaryWalletID')
                ->where('UserWalletID', $partyId)
                ->where('BeneficiaryID', $BeneficiaryID)
                ->where('BeneficiaryType', 'eWallet')
                ->first();
        return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist->BeneficiaryWalletID;
    }

    private function _validateBenificary($request)
    {
        $validations = array(
            'partyId'       => 'required|integer|digits_between:1,10',
            'beneficiaryId' => 'required|numeric|digits_between:1,10',
            'amountList'    => 'required'
        );

        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function deleteBeneficiary(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->_validateDeleteBeneficiary($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }
        $partyId         = $request->input('partyId');
        $beneficiaryId   = $request->input('beneficiaryId');
        $beneficaryType  = $request->input('beneficaryType');
        $checkBeneficary = \App\Models\Userbeneficiary::select('BeneficiaryStatus')
                        ->where('BeneficiaryID', '=', $beneficiaryId)->first();
        if ( ! $checkBeneficary )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
            return $output_arr;
        }

        $params     = array( 'partyId' => $partyId, 'vPin' => $request->input('vPin') );
        $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
        if ( $verifyVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'vPin' => trans('messages.vpinNotValid') );
            return $output_arr;
        }

        $beneficiaryDetails                    = \App\Models\Userbeneficiary::find($beneficiaryId);
        $beneficiaryDetails->BeneficiaryStatus = 'N';
        $beneficiaryDetails->save();

        $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryDeleted') );
        $output_arr['is_error']    = FALSE;
        return $output_arr;
    }

    private function _validateDeleteBeneficiary($request)
    {
        $validations = array(
            'partyId'       => 'required|integer|digits_between:1,10',
            'beneficiaryId' => 'required|numeric|digits_between:1,10',
            'vPin'          => 'required|numeric|digits:4',
        );

        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function addBeneficiaryToTripleClick(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->_validateBeneficiaryInfo($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }
        $partyId         = $request->input('partyId');
        $beneficiaryId   = $request->input('beneficiaryId');
        $beneficaryType  = $request->input('beneficiaryType');
        $status          = $request->input('tripleClickStatus');
        $checkBeneficary = $this->_beneficiaryValidate($beneficiaryId, $partyId, $beneficaryType);
        if ( $checkBeneficary === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryNotFound') );
            return $output_arr;
        }

        $beneficiaryDetails = \App\Models\Userbeneficiary::where('BeneficiaryID', $beneficiaryId)
                ->where('UserWalletID', $partyId)
                ->update([ 'isTripleClickUser' => $status ]);

        $statusCheck = "Added to";
        if ( $status == 'N' )
        {
            $statusCheck = 'Deleted from';
        }
        $tripleClick               = trans('messages.attributes.tripleClick');
        $output_arr['display_msg'] = array( 'info' => trans('messages.benificiaryStatusAddOrDelete', [ 'status' => $statusCheck, 'name' => $tripleClick, ]) );
        $output_arr['is_error']    = FALSE;
        return $output_arr;
    }

    private function _validateBeneficiaryInfo($request)
    {
        $validations = array(
            'partyId'           => 'required|integer|digits_between:1,10',
            'beneficiaryId'     => 'required|numeric|digits_between:1,10',
            'beneficiaryType'   => 'required|in:eWallet',
            'tripleClickStatus' => 'required|in:Y,N',
        );

        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function vpaListUpdate(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->_validateVPAListUpdate($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }
        $partyId             = $request->input('partyId');
        $beneficiaryWalletID = $request->input('beneficiaryWalletID');
        $vpaList             = $request->input('vpaList');

        $userCheck = $this->_userIdValidate($partyId);
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }

        $vpaList = json_decode($vpaList);
        if ( $vpaList === NULL )
        {
            $output_arr['display_msg'] = array( 'vpaList' => "Invalid Format" );
            return $output_arr;
        }

        $beneficiryVpaDetails = \App\Models\Userbeneficiary::select('BeneficiaryVPA')
                ->where("UserWalletID", $partyId)
                ->where("BeneficiaryType", "UPI")
                ->get();
        $beneficiryVpaCount   = $beneficiryVpaDetails->count();
        if ( $beneficiryVpaCount > 0 )
        {
            $beneficiryVpaDetails = $beneficiryVpaDetails->toArray();

            $existingVpa = [];
            foreach ( $beneficiryVpaDetails as $singleRow )
            {
                $existingVpa[] = $singleRow['BeneficiaryVPA'];
            }

            // Check for existing vpa and remove from inserting list.
            foreach ( $vpaList as $key => $singleRow )
            {
                if ( in_array($singleRow->fvaddr, $existingVpa) )
                {
                    unset($vpaList[$key]);
                }
            }
        }

        // Case if all vpa's already exists.
        if ( count($vpaList) == 0 )
        {
            $output_arr['display_msg'] = array( 'vpaList' => "List up to date." );
            return $output_arr;
        }

        $finalVpaList = $vpaList;
        $date_added   = date('Y-m-d H:i:s');
        $insertArray  = [];

        foreach ( $finalVpaList as $vpaDetail )
        {
            $insertArray[] = [
                "UserWalletID"        => $partyId,
                "BeneficiaryWalletID" => $beneficiaryWalletID,
                "BeneficiaryType"     => "UPI",
                //  "BankCode" => $vpaDetail->bankCode,
                "AccountNumber"       => $vpaDetail->accountNumber,
                "AccountName"         => $vpaDetail->accountName,
                "BeneficiaryVPA"      => $vpaDetail->fvaddr,
                "isTripleClickUser"   => "N",
                "BeneficiaryStatus"   => "Y",
                "SelfBenficiary"      => "Y",
                'CreatedDateTime'     => $date_added,
                'CreatedBy'           => 'User',
                'UpdatedDateTime'     => $date_added,
                'UpdatedBy'           => $request->input('UpdatedBy'),
            ];
        }

        \App\Models\Userbeneficiary::insert($insertArray);

        $output_arr['display_msg'] = array( 'info' => "List Updated" );
        $output_arr['is_error']    = FALSE;
        return $output_arr;
    }

    private function _validateVPAListUpdate($request)
    {
        $validations = array(
            'partyId'             => 'required|integer|digits_between:1,10',
            'beneficiaryWalletID' => 'required|numeric|digits_between:1,10',
            'vpaList'             => 'required',
        );

        $validate_response = \Validator::make($request->all(), $validations);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
