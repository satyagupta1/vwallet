<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |       
    */
    
    'accepted'             => 'The :attribute must be accepted.',    
    'insert_faild'               => 'Insert data faild.',
    'update_faild'               => 'Update data faild.',
    'update_success'               => 'Update data success.',
    'insert_success'               => 'Inserting data success.',
    
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'from_amount' => [
        'between' => 'raashi se vaidh seema hona chaahie.',
    ],
    'to_amount' => [
        'between' => 'uchit seema ke beech raashi ke lie hona chaahie',
    ],
   
            
];
