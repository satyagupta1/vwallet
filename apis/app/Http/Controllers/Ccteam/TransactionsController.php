<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Ccteam;

use App\Http\Controllers\Controller;

/**
 * UserProfileController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-wallet.com Licence
 * 
 */
class TransactionsController extends Controller {

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function listTransactions(\Illuminate\Http\Request $request)
    {

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $userid     = $request->input('custid');
        $violaid    = $request->input('violaid');
        $phone      = $request->input('phone');
        
         if ( empty($userid) && empty($violaid) && empty($phone) )
        {
            $data_send = array( 'is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => [] );
            return $data_send;
        }

       
// check validation
        $chk_valid = $this->_validationChecktransactions($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        else
        {
             $where = [ 'UserWalletID', '=', $userid ];
              if ( ! empty($violaid) )
        {
            $data  = explode('@', $violaid);
            $where = [ 'ViolaID', '=', $data[0] ];
        }
        elseif ( ! empty($phone) )
        {
            $where = [ 'MobileNumber', '=', $phone ];
        }
        
        $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
            if ( empty($result->UserWalletID) )
            {
                $data_send = array( 'is_error' => TRUE, 'message' => 'No User Found', 'code' => http_response_code(), 'res_data' => [] );
            return $data_send;
            }
            $custId = $result->UserWalletID;
            $output_arr['is_error'] = FALSE;

            if ( (count($this->_getUserTransactions($request,$custId))) <= 0 )
            {
                $output_arr['display_msg'] = array( 'info' => 'No Records Found' );
            }
            
            $fullList = $this->_getUserTransactions($request,$custId);
           
            foreach ( $fullList as $datalist )
            {
               
                  $fromAccountNumber = $datalist['FromAccountId'];
                    $toAccountNumber = $datalist['ToAccountId'];
                if($datalist['ProductName'] === 'TRIPLECLIK' || $datalist['ProductName'] === 'PAY W2W')
                {
                  
         $getFromAccountNumbers = \App\Models\Walletaccount::select('WalletAccountNumber','UserWalletId')->where('WalletAccountId',$fromAccountNumber)->first();          
         $getToAccountNumbers = \App\Models\Walletaccount::select('WalletAccountNumber','UserWalletId')->where('WalletAccountId',$toAccountNumber)->first();          
                    $fromMainAcntNum = (!empty($getFromAccountNumbers->WalletAccountNumber))?$getFromAccountNumbers->WalletAccountNumber:'';
                    $toMainAcntNum = (!empty($getToAccountNumbers->WalletAccountNumber))?$getToAccountNumbers->WalletAccountNumber:'';    
         
                }
                 else if($datalist['ProductName'] == 'TOPUP')
                {
                     
         $getFromAccountNumbers1 = \App\Models\Partypreferredpaymentmethod::select('CardHolderName','CardDisplayNumber','CardNumber')->where('PaymentMethodID',$fromAccountNumber)->first();          
         $getToAccountNumbers1 = \App\Models\Walletaccount::select('WalletAccountNumber','UserWalletId')->where('WalletAccountId',$toAccountNumber)->first();          
         $fromMainAcntNum = (!empty($getFromAccountNumbers1->CardDisplayNumber))?$getFromAccountNumbers1->CardDisplayNumber:$fromAccountNumber;
                    $toMainAcntNum = (!empty($getToAccountNumbers1->WalletAccountNumber))?$getToAccountNumbers1->WalletAccountNumber:'';    
             
                }
                  else if($datalist['ProductName'] === 'PAY W2B')
                {
           $getFromAccountNumbers = \App\Models\userexternalbeneficiary::select('WalletAccountNumber','UserWalletId')->where('WalletAccountId',$fromAccountNumber)->first();          
         $getToAccountNumbers = \App\Models\userexternalbeneficiary::select('AccountNumber','BeneficiaryID')->where('BeneficiaryID',$toAccountNumber)->first();          
                    $fromMainAcntNum = (!empty($getFromAccountNumbers->WalletAccountNumber))?$getFromAccountNumbers->WalletAccountNumber:'';
                    $toMainAcntNum = (!empty($getToAccountNumbers->AccountNumber))?$getToAccountNumbers->AccountNumber:'';
                }
                
              
               $txnlist[] = array(
                   'PartyID' => $datalist['PartyID'],
                   'transactionId' => $datalist['transactionId'],
                   'fromAccountNumber' => $fromMainAcntNum,
                   'fromCustId' => (!empty($getFromAccountNumbers->UserWalletId))?$getFromAccountNumbers->UserWalletId:'',
                   'fromName' => $datalist['From_name'],
                   'toAccountNumber' => $toMainAcntNum,
                    'toCustId' => (!empty($getToAccountNumbers->UserWalletId))?$getToAccountNumbers->UserWalletId:'',
                   'toName' => $datalist['To_name'],
                   'fromMobileNumber' => $datalist['fromMobileNumber'],
                   'toMobileNumber' => $datalist['toMobileNumber'],
                   'TransactionOrderID' => $datalist['TransactionOrderID'],
                   'TransactionStatus' => $datalist['TransactionStatus'],
                   'Amount' => $datalist['Amount'],
                   'Message' => $datalist['Message'],
                   'MessageStatus' => $datalist['MessageStatus'],
                   'notificationSent' => $datalist['notificationSent'],
                   'ReceipentReceivedDate' => $datalist['ReceipentReceivedDate'],
                   'TransactionDate' => $datalist['TransactionDate'],
                   'TransactionMode' => ($datalist['TransactionMode']=='C')?'Credit':'Debit',
                   'TransactionCurrency' => $datalist['TransactionCurrency'],
                   'AccountCurrency' => $datalist['AccountCurrency'],
                   'ProductName' => $datalist['ProductName'],
                   'Comments' => $datalist['Comments'],
                   );
                
                
            }
            
            $output_arr['res_data'] = (!empty($txnlist))?$txnlist:array();
            return json_encode($output_arr);
        }
    }

    /**
     * This used to get all Notification details
     * @desc  This used to get all Notification details
     * @param PartyId integer
     * @param limit integer
     * @param offset integer
     *  
     * @return Array
     */
    private function _getUserTransactions($request,$custId)
    {
        
//\DB::connection()->enableQueryLog();
        
       
        
        
        
        $paginate               = $request->input('paginate');
        $limit = $request->input('limit');
        $offset = $request->input('offset');
        //$offset                 = $request->input('offset');
        $first                  = \App\Models\Promotion::select('PromotionID', 'PromoCode');
        $select_wallet_accounts = \App\Models\Notificationlog::
                join('transactiondetail', 'transactiondetail.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->join('dailytransactionlog', 'transactiondetail.TransactionDetailID', '=', 'dailytransactionlog.TransactionDetailID')
                ->leftjoin('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                ->join('paymentsproduct', 'transactiondetail.PaymentsProductID', '=', 'paymentsproduct.PaymentsProductID')
                ->join('transactioneventlog', 'transactiondetail.TransactionDetailID', '=', 'transactioneventlog.TransactionDetailID')
                ->join('userwallet', 'userwallet.UserWalletID', '=', 'transactiondetail.ToWalletId')
                ->leftjoin('userwallet As fromWallet', 'fromWallet.UserWalletID', '=', 'transactiondetail.UserWalletId')
                ->select('notificationlog.PartyID', 
                        'notificationlog.LinkedTransactionID as transactionId',
                         'transactiondetail.FromAccountName As From_name',
                        'userwallet.FirstName As To_name', 
                        'fromWallet.MobileNumber As fromMobileNumber', 
                        'userwallet.MobileNumber As toMobileNumber',
                          'transactiondetail.TransactionOrderID', 
                        'transactiondetail.Amount', //'transactiondetail.TransactionTypeCode as txnType',
                        'notificationlog.Message', 'notificationlog.MessageStatus', 
                         'notificationlog.SentDateTime as notificationSent', 
                        'notificationlog.ReceipentReceivedDate', 
                        'transactiondetail.TransactionDate', 
                        'transactiondetail.TransactionStatus','transactiondetail.UserWalletId', 
                         'transactiondetail.FromAccountId','transactiondetail.ToAccountId',
                         'transactiondetail.ToWalletId', 
                        'dailytransactionlog.CrDrIndicator As TransactionMode', 
                        'dailytransactionlog.TransactionCurrency', 
                        'dailytransactionlog.AccountCurrency', 
                        'paymentsproduct.ProductName', 'transactioneventlog.Comments','transactiondetail.TransactionStatus'
                        //'partypreferredpaymentmethod.CardNumber'
                )->where('notificationlog.PartyID', '=', $custId);
                $select_wallet_accounts->where('dailytransactionlog.PartyID', '=', $custId);
        // 'transactiondetail.FromAccountName As From_name',
//        if ( $paginate !== NULL && $paginate == 'Y' )
//        {
//            $select_wallet_accounts->paginate($this->perpage)->toArray();
//        }
        
         if ( ( ! empty($request->input('from_date')) ) OR ( ! empty($request->input('to_date')) ) )
            {
                // For date range
                if ( ( ! empty($request->input('from_date')) ) && ( ! empty($request->input('to_date')) ) )
                {
                    $select_wallet_accounts->whereBetween('transactiondetail.TransactionDate', [ $request->input('from_date') . ' 00:00:00', $request->input('to_date') . ' 23:59:59' ]);
                }
                elseif ( ! empty($request->input('from_date')) )
                {
                    $select_wallet_accounts->whereBetween('transactiondetail.TransactionDate', [ $request->input('from_date') . ' 00:00:00', $request->input('from_date') . ' 23:59:59' ]);
                    
                }
                elseif ( ! empty($request->input('to_date')) )
                {
                    $select_wallet_accounts->whereBetween('transactiondetail.TransactionDate', [ $request->input('to_date') . ' 00:00:00', $request->input('to_date') . ' 23:59:59' ]);
                }
            }
            if ( ! empty($request->input('status')) )
            {
                $select_wallet_accounts->where([ [ 'transactiondetail.TransactionStatus', '=', $request->input('status') ] ]);
            }
            if ( ! empty($request->input('amount')) )
            {
                $select_wallet_accounts->where([ [ 'transactiondetail.Amount', '=', $request->input('amount') ] ]);
            }
            if ( ! empty($request->input('transaction_id')) )
            {
                $select_wallet_accounts->where([ [ 'transactiondetail.TransactionOrderID', '=', $request->input('transaction_id') ] ]);
            }
            if ( ! empty($request->input('transaction_mode')) )
            {
               if($request->input('transaction_mode') == 'Credit')
               {
                   $txnMode = 'C';
               }
               else if($request->input('transaction_mode') == 'Debit')
               {
                   $txnMode = 'D';
               }
                else{
                     $txnMode = '';
                }
                    
                $select_wallet_accounts->where([ [ 'dailytransactionlog.CrDrIndicator', '=', $txnMode ] ]);
            }


        if ( ! empty($request->input('accountnumber')) )
        {
             $acntId = $this->getWalletAccountId($request->input('accountnumber'));
            $select_wallet_accounts->where([ ['transactiondetail.FromAccountId', '=', (!empty($acntId))?$acntId:0] ]);
          
        }
            


            if ( ! empty($request->input('currency')) )
            {
                $select_wallet_accounts->where([ [ 'dailytransactionlog.TransactionCurrency', '=', $request->input('currency') ] ]);
            }
        
        
        $select_wallet_accounts->limit($limit)->offset($offset);
        $select_wallet_accounts->groupBy('transactiondetail.TransactionOrderID');
        $select_wallet_accounts->orderBy('notificationlog.MessageStatus', 'DESC');
        $select_wallet_accounts->orderBy('notificationlog.NotificationID', 'DESC');
        return $select_wallet_accounts->get();
    }

    
 public function getWalletAccountId($waltAcntnumber)
 {
     $getFromAccountNumbers = \App\Models\Walletaccount::select('WalletAccountId')->where('WalletAccountNumber',$waltAcntnumber)->first();          
     return (!empty($getFromAccountNumbers->WalletAccountId))?$getFromAccountNumbers->WalletAccountId:'';
 }
    
    /**
     * Validate before showing notification list
     * @desc  this is used to check validation for Notification list
     * @param Request $request
     * @param PartyId integer
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _validationChecktransactions($request)
    {
        // validations
        $validate1 = array(
            'custid'  => 'numerics',
            'phone'   => 'numerics|digits:10',
            'violaid' => 'min:8',
            'limit'   => 'required|integer|min:1',
            'offset'  => 'required|integer|min:0',
        );


        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate1, $extraParamValidate);
        $messages           = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
