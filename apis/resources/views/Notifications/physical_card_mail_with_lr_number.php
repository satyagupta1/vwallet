<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Mail with LR Number (Parcel Tracking Number) and Transporter details (Complete shipment details) template  
 *  param $fullname varchar 
 *  param $orderId int   
 *  param $trackingNumber int  
 *  param $dateTime varchar 
 *  param $shipmentType varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaCard Dispatched!',
        'text'    => $salutationName . ',<br/><br/>  
Plastic Card Dispatched! 
We are happy to inform you that your ViolaCard is dispatched to your address as per your request.<br/><br/>   
Order No: '.$orderId.'  <br/>  
Tracking ID: '.$trackingNumber.'  <br/>  
Date & Time: '.$dateTime],
    'sms'   => [
        'text' => '
            Dear '.$fullname.', 
Your ViolaCard has been dispatched to your address. 
Tracking number :  '.$trackingNumber.'      Date & Time: '.$dateTime.'
    shipped Via: '.$shipmentType.'                
 
',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Physical card dispatched',
    ],
];

echo json_encode($array);

/* End of file physical_card_mail_with_lr_number.php */
