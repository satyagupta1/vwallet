<?php

namespace App\Http\Controllers\Party;

use App\Http\Controllers\Controller;

/**
 * PartySecurityController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class PartySecurityController extends Controller {

    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');
    }

    /*
     *  List Party Security Question Controller
     *  @Desc It will give all Party Security Questions list, 
     *  with filter by security question Question Category, IsStatusActive.and Question Id.
     *  @param Request $request
     *  @return JSON
     */

    public function getPartySecurityList(\Illuminate\Http\Request $request)
    {

        //Default Response
        $output_arr    = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $checkValidate = $this->_validateFields($request);
        if ($checkValidate !== TRUE)
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId       = $request->input('partyId');
        $questionId    = $request->input('questionId');
        $questionRank  = $request->input('questionRank');
        $orderBy       = $request->input('orderBy');
        $orderByColumn = $request->input('orderColumn');
        $limit         = $request->input('limit');
        $paginate      = $request->input('paginate');


        $selectColumns = array(
            'PartyID',
            'QuestionId',
            'QuestionRankNumber',
            'Answer',
            'CreatedDateTime',
        );

        //$query = \App\Models\Userbeneficiary::select($selectColumns);
        //$query->where('UserWalletID', '=', $partyId);
        $query = \App\Models\Partysecurity::select($selectColumns)->where('PartyID', '=', $partyId);
        //filter by beneficiary type
        if ($questionId !== NULL)
        {
            $query->where('QuestionId', '=', $questionId);
        }

        //filter by triple click
        if ($questionRank !== NULL)
        {
            $query->where('QuestionRankNumber', '=', $questionRank);
        }


        //Order Specified default DESC        
        if ($orderBy == NULL)
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ($orderByColumn !== NULL && in_array($orderByColumn, $selectColumns))
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('QuestionId', $orderBy);
        }

        //Limit records
        if ($limit !== NULL)
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ($paginate !== NULL && $paginate == 'Y')
        {
            $finalRecords = $query->paginate($this->perpage)->toArray();
            $countResults = $finalRecords['total'];
        }
        else
        {
            $finalRecords = $query->get()->toArray();
            $countResults = count($finalRecords);
        }

        if ($countResults > 0)
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = '';
            $output_arr['res_data']    = $finalRecords;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    /**
     * Validate Form Fields Data
     * @param $request
     * @return String
     */
    private function _validateFields($request)
    {
        $validate = [
            'partyId' => 'required|numeric',
        ];
        if ($request->input('questionId') !== NULL)
        {
            $validate['questionId'] = 'required|numeric';
        }

        if ($request->input('questionRank') !== NULL)
        {
            $validate['questionRank'] = 'required|numeric';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function getEncryptedKey()
    {
        //Default Response
        $output_arr    = array('is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $key_root_path = base_path() . '/.primaryKey';

        // get part 1 from file.
        $fileData = fopen($key_root_path, 'r');
        $fileKey  = fread($fileData, filesize($key_root_path));
        fclose($fileData);
        $output_arr['res_data']    = array(
            'primaryKey' => $fileKey
        );
        return response()->json($output_arr);
    }

}
