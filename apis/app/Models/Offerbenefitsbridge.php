<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Offerbenefitsbridge
 * 
 * @property int $OfferBenefitsBridgeID
 * @property int $OfferBusinessID
 * @property int $BenefitBusinessID
 * @property string $ActiveInd
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Offerbenefit $offerbenefit
 * @property \App\Models\Promotionaloffer $promotionaloffer
 *
 * @package App\Models
 */
class Offerbenefitsbridge extends Eloquent
{
	protected $table = 'offerbenefitsbridge';
	protected $primaryKey = 'OfferBenefitsBridgeID';
	public $timestamps = false;

	protected $casts = [
		'OfferBusinessID' => 'int',
		'BenefitBusinessID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'OfferBusinessID',
		'BenefitBusinessID',
		'ActiveInd',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function offerbenefit()
	{
		return $this->belongsTo(\App\Models\Offerbenefit::class, 'BenefitBusinessID', 'BenefitBusinessID');
	}

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'OfferBusinessID', 'OfferBusinessID');
	}
}
