<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Get upto Rs.150 Cashback on Mobile and DTH Recharges using Viola Wallet.</li>
        <li>Minimum amount spend to redeem the discount is Rs.50</li>
        <li>The code can be redeemed only once per user.</li>
        <li>Limited period offer.</li>
        <li>The offer is applicable on Mobile/DTH recharges only.</li>
        <li>Cashback will be auto-credited to User's ViolaWallet within 72 hours from the date of successful transaction.</li>
        <li>In case User does not receive the cashback within 48 hours from the time of transaction or for any cashback related query, User should write to support@violamoney.com</li>
        <li>Cashback amount cannot be transferred to any other person or to any bank account.</li>
        <li>Users shall take sole responsibility for privacy and confidentiality of their ViolaWallet account and password.</li>
        <li>Viola reserves the right to add, modify, or discontinue the terms and conditions of this offer at any time without prior notice, at its sole discretion.</li>
        <li>In no event the aggregate liability of Viola shall exceed the amount of cashback, the User is entitled for, under this Offer.</li>
        <li>Unlawful modification or abuse of the Offer is prohibited.</li>
        <li>Viola disclaims all third party liabilities that may arise from or in connection with this offer. This Offer is subject to force majeure terms.</li>
    </ul> 
</div>
<?php include 'footer.php';?>