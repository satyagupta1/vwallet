<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Ccteam;

use App\Libraries\Helpers;
use App\Libraries\Crypt\Encryptor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

/**
 * UserProfileController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-wallet.com Licence
 */
class UserAuthenticateController extends Controller {

    public function __construct()
    {
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
    }

    public function LoginHistory(Request $request)
    {

        // $enc_key = sha1(Helpers::secureEncryption());

        $userid  = $request->input('custid');
        $violaid = $request->input('violaid');
        $phone   = $request->input('phone');
        $offset  = $request->input('offset');
        $limit   = $request->input('limit');

        if ( ! empty($violaid) )
        {
            $data  = explode('@', $violaid);
            $where = [ 'cardusers.ViolaCardId', '=', $data[0] ];
        }
        elseif ( ! empty($phone) )
        {
            $where = [ 'cardusers.mobileNumber', '=', $phone ];
        }

        $errs             = array();
        $errs['is_error'] = FALSE;
        $validator        = $this->_validateLoginHistory($request, $type             = '');

        if ( empty($userid) && empty($violaid) && empty($phone) )
        {
            $data_send = array( 'is_error' => TRUE, 'message' => 'Enter custid / phone / violaid', 'code' => http_response_code(), 'res_data' => [] );
            return $data_send;
        }

        if ( $validator === TRUE )
        {
            $where = [ 'UserWalletID', '=', $userid ];
            if ( ! empty($violaid) )
            {
                $data  = explode('@', $violaid);
                $where = [ 'ViolaID', '=', $data[0] ];
            }
            elseif ( ! empty($phone) )
            {
                $where = [ 'MobileNumber', '=', $phone ];
            }


// check user valid or not !
            $result = \App\Models\Userwallet::select('UserWalletID', 'MobileNumber')->Where([ $where, [ 'ProfileStatus', '=', 'A' ] ])->first();
            if ( ! empty($result->UserWalletID) )
            {
                $lhistory         = \App\Models\Partyloginhistory::select('SessionID', 'PartyLoginHistoryId', 'PartyID', 'Email', 'SessionStartTime', 'SessionEndTime', 'IPAddress', 'BrowserType', 'LoginSuccess', 'ExistingUser', 'DeviceType')->Where('PartyID', '=', $result->UserWalletID)->orderBy('SessionStartTime', 'DESC')->limit($limit)->offset($offset)->get();
                $loghistory_array = array();
                $lcoation         = '';
                if ( $lhistory )
                {

                    foreach ( $lhistory as $history )
                    {

                        $getSessionDetails = \App\Models\Partysessionusagedetail::
                                        select('PartySessionUsageDetailId', 'SessionID', 'ModuleID', 'OtherComments', 'EntryDateTime')
                                        ->where('SessionID', $history['SessionID'])->get();

                        $sessionArray = array();
                        foreach ( $getSessionDetails as $resession )
                        {
                            $sessionArray[] = array( 'eventModule' => $resession['ModuleID'], 'OtherComments' => $resession['OtherComments'], 'InsertDateTime' => $resession['EntryDateTime'] );
                        }


                        $lcoation = "{'lat': '17.3753', 'lon': '78.4744'}";

                        $device = '';
                        if ( $history['DeviceType'] === 'W' )
                        {
                            $device = 'Website';
                        }
                        elseif ( $history['DeviceType'] === 'I' )
                        {
                            $device = 'iPhone';
                        }
                        else if ( $history['DeviceType'] === 'A' )
                        {
                            $device = 'Android';
                        }
                        $loghistory_array[] = array(
                            'custid'           => $history['PartyID'],
                            'loginby'          => $history['Email'],
                            'LoginTime'        => $history['SessionStartTime'],
                            'LogoutTime'       => $history['SessionEndTime'],
                            'Detail'           => ($sessionArray),
                            'IP'               => $history['IPAddress'],
                            'latLong'          => $lcoation,
                            'existingCustomer' => ($history['ExistingUser'] === 'Y') ? 'Yes' : 'No',
                            'success'          => ($history['LoginSuccess'] === 'Y') ? 'Yes' : 'No',
                            'loggedin_on'      => $device,
                            'agentinfo'        => ($history['BrowserType']),
                            'sessionId'        => $history['SessionID'],
                            'totalevents'      => count($sessionArray) );
                    }
                }

                $data_send = array( 'is_error' => FALSE, 'message' => '', 'code' => 200, 'totalRecords' => count($lhistory), 'res_data' => $loghistory_array );
                return $data_send;
            }
            else
            {
                $validator = 'Invalid User!';
            }
        }
        $data_send = array( 'is_error' => TRUE, 'message' => $validator, 'code' => 200, 'res_data' => array() );
        return $data_send;
    }

    public function LogHisEventDet(Request $request)
    {
        // $enc_key = sha1(Helpers::secureEncryption());

        $offset  = $request->input('offset');
        $limit   = $request->input('limit');

        $errs             = array();
        $errs['is_error'] = FALSE;
        $validator        = $this->_validateLoginHistory($request, $type = 'event');

        if ( $validator === TRUE )
        {

            $sessId = $request->input('sessId');
// check user valid or not !
            $result = \App\Models\Partysessionusagedetail::
                                select('PartySessionUsageDetailId', 'SessionID', 'ModuleID', 'OtherComments', 'EntryDateTime')
                                ->where('SessionID', $sessId)->first();
            if ( ! empty($result->PartySessionUsageDetailId) )
            {
                $lhistory         = \App\Models\Partysessionusagedetail::
                                select('PartySessionUsageDetailId', 'SessionID', 'ModuleID', 'OtherComments', 'EntryDateTime')
                                ->where('SessionID', $sessId)->orderBy('PartySessionUsageDetailId', 'DESC')->limit($limit)->offset($offset)->get();
                $loghistory_array = array();
                $lcoation         = '';
                if ( $lhistory )
                {

                    foreach ( $lhistory as $history )
                    {

                        $loghistory_array[] = array(
                            'sessionId'     => $history['SessionID'],
                            'entryDateTime' => $history['EntryDateTime'],
                            'eventModule'   => $history['ModuleID'],
                            'eventBy'       => $history['OtherComments'] );
                    }
                }

                $data_send = array( 'is_error' => FALSE, 'message' => '', 'code' => 200, 'totalRecords' => count($lhistory), 'res_data' => $loghistory_array );
                return $data_send;
            }
            else
            {
                $validator = 'No Events Found';
            }
        }
        $data_send = array( 'is_error' => TRUE, 'message' => $validator, 'code' => 200, 'res_data' => array() );
        return $data_send;
    }

    /*
     *  profileDetails - To Read User Information
     *  @param Request $request
     *  @return JSON
     */

    private function _UserVerify($request, $validMethod)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = $this->$validMethod($request);


        if ( $validationCheck !== TRUE )
        {
            $output_arr['display_msg'] = $validationCheck;
            return $output_arr;
        }

        // user exsistens test
        $partyId   = $request->input('partyId');
        $userCheck = $this->_userIdValidate($partyId);
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;

        return $output_arr;
    }

    /*
     *  forgotVpin / set vpin - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function forgotVpin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_forgetPinValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $userInfo = \App\Models\Userwallet::select('MobileNumber')->find($partyId);

        $getMobile = $userInfo->MobileNumber;
        // check last 3 passwords for uniqueness
        // send Otp for varification 
        $params      = array(
            'UserWalletID' => $partyId,
            'mobileNumber' => $getMobile,
            'typeCode'     => 'PIN',
            'attempts'     => 0
        );
        $otp_success = \App\Libraries\Helpers::OtpSend($params);

        if ( $otp_success === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.otpSentFaild') );
            return $output_arr;
        }
        $sendMobileData = json_encode(array(
            //'fullname' => $results->FirstName.' '.$results->LastName,
            'site_name' => $this->applicationName,
            'otp_code'  => $otp_success['otpNumber'] ));


        $communicationParams = array(
            'party_id'          => $partyId,
            'linked_id'         => '0',
            'notification_for'  => 'otp_verify',
            'notification_type' => 'sms',
            'mobile_no'         => $getMobile,
            'mobile_data'       => $sendMobileData,
            //'email_id' =>   $results->EmailID, 
            //'email_data' => $sendEmailData,
            'application_name'  => $this->applicationName
        );

        //common function to send notification_type msgs.
        $response = \App\Libraries\Helpers::sendMsg($communicationParams);

        $res = $otp_success;
        unset($res['otpMssage']);

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => $otp_success['otpMssage'] ), 'status_code' => 200, 'res_data' => $res );
        return $output_arr;
    }

    /*
     *  change /  - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function changeVpin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_changePinValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $vPin     = $request->input('vPin');
        $userInfo = \App\Models\Userwallet::select('MobileNumber', 'TPin')->find($partyId);

        $getMobile = $userInfo->MobileNumber;
        // check last 3 passwords for uniqueness
        $secureKey = sha1($vPin);
        $keyType   = 'PIN';
        $key       = trans('messages.attributes.vpin');
        $new       = $this->_checkSecureKey($partyId, $secureKey, $keyType);

        if ( $new === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $key ]) );
            return $output_arr;
        }

        // send Otp for varification 
        $params      = array(
            'UserWalletID' => $partyId,
            'mobileNumber' => $getMobile,
            'typeCode'     => $keyType,
            'attempts'     => 0
        );
        $otp_success = \App\Libraries\Helpers::OtpSend($params);
        if ( $otp_success === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.otpSentFaild') );
            return $output_arr;
        }
        $sendMobileData = json_encode(array(
            //'fullname' => $results->FirstName.' '.$results->LastName,
            'site_name' => $this->applicationName,
            'otp_code'  => $otp_success['otpNumber'] ));


        $communicationParams = array(
            'party_id'          => $partyId,
            'linked_id'         => '0',
            'notification_for'  => 'otp_verify',
            'notification_type' => 'sms',
            'mobile_no'         => $getMobile,
            'mobile_data'       => $sendMobileData,
            //'email_id' =>   $results->EmailID, 
            //'email_data' => $sendEmailData,
            'application_name'  => $this->applicationName
        );

        //common function to send notification_type msgs.
        $response = \App\Libraries\Helpers::sendMsg($communicationParams);

        $res = $otp_success;
        unset($res['otpMssage']);

        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => $otp_success['otpMssage'] ), 'status_code' => 200, 'res_data' => $res );
        return $output_arr;
    }

    /*
     *  forgotVpin / set vpin - Get nominee Details
     *  @param Party Id $partyId
     *  @return JSON
     */

    public function setPin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = $this->_UserVerify($request, '_forgetOtpPinValidate');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        // request Variables
        $partyId  = $request->input('partyId');
        $otp      = $request->input('otpNumber');
        $otpId    = $request->input('otpId');
        $otp_Type = $request->input('otpType');
        $vPin     = $request->input('vPin');

        // validate OTP
        $otpPramas    = array( 'partyId' => $partyId, 'otpNumber' => $otp, 'otpId' => $otpId, 'otpType' => $otp_Type );
        $validateVpin = \App\Libraries\Helpers::validateOtp($otpPramas);

        if ( $validateVpin === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.validOtp') );
            return $output_arr;
        }

        $secureKey = sha1($vPin);
        $keyType   = 'PIN';
        $key       = trans('messages.attributes.vpin');
        $new       = $this->_checkSecureKey($partyId, $secureKey, $keyType);

        if ( $new === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.passPinUsed', [ 'name' => $key ]) );
            return $output_arr;
        }

        $walletDetails = \App\Models\Userwallet::find($partyId);

        if ( ! $walletDetails )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }
        $walletDetails->TPin = sha1($request->input('vPin'));
        $walletDetails->save();

        // update password history
        $this->_updatePasswordHistory($partyId, $secureKey, $keyType);

        // Send Notification
        $sendMobileData = json_encode(array());

        $sendEmailData       = json_encode(array(
            'fullname'  => $walletDetails->FirstName . ' ' . $walletDetails->LastName,
            'img_url'   => $this->baseUrl,
            'front_url' => $this->baseUrl,
            'date'      => date('Y-m-d'),
            'time'      => date('H:i:s'),
        ));
        $communicationParams = array(
            'party_id'          => $walletDetails->UserWalletID,
            'linked_id'         => '0',
            'notification_for'  => 'login_chnge_pin',
            'notification_type' => 'email,sms',
            'mobile_no'         => $walletDetails->MobileNumber,
            'mobile_data'       => $sendMobileData,
            'email_id'          => $walletDetails->EmailID,
            'email_data'        => $sendEmailData,
        );
        // Push notification parameters.          
        $device_type         = $request->header('device-type');
        if ( $device_type != 'W' )
        {
            $pushData                                 = array();
            $communicationParams['notification_type'] = $communicationParams['notification_type'] . ",push";
            $communicationParams['device_type']       = $device_type;
            $communicationParams['device_token']      = $request->header('device-token');
            $communicationParams['push_data']         = json_encode($pushData);
        }
        //common function to send notification_type msgs.
        $response = \App\Libraries\Helpers::sendMsg($communicationParams);


        $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.setSuccessfully', [ 'name' => $key ]) ), 'status_code' => 200, 'res_data' => array() );

        return $output_arr;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _userIdValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', '=', $partyId)->where('ProfileStatus', '=', 'A');
        if ( ! $userExsist )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    private function _changePinValidation($request)
    {
        $validations['vPin']    = 'required|numeric|digits:4';
        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _forgetPinValidation($request)
    {
        // common input fiels
        $validations['partyId'] = 'required|integer|digits_between:1,10';
        $messages               = [];

        return $this->_validate_method($request, $validations, $messages);
    }

    private function _validate_method($request, $validations, $messages)
    {
        $validate_response = \Validator::make($request->all(), $validations, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /*
     *  checkSecureKey - check password & pin is already used or not
     *  @param int $partyId
     *  @param string $secureKey
     *  @param string $keyType 
     *  @return JSON
     */

    private function _checkSecureKey($partyId, $secureKey, $keyType)
    {
        // check last 3 passwords for uniqueness
        $lastPass = \App\Models\Partypasswordhistory::select('SecretKey')
                        ->where('PartyID', $partyId)
                        ->where('SecretKeyType', $keyType)
                        ->orderBy('PartyPasswordHistoryID', 'desc')
                        ->take(3)->get();
        $new      = TRUE;
        foreach ( $lastPass as $value )
        {
            if ( $value->SecretKey == $secureKey )
            {
                $new = FALSE;
                break;
            }
            else
            {
                $new = TRUE;
            }
        }
        return $new;
    }

    private function _validateLoginHistory($request, $type)
    {
        // $langfolder = Helpers::languages();
        $validat  = [
            'custid'  => 'numerics',
            'phone'   => 'numerics|digits:10',
            'violaid' => 'min:8',
            'limit'   => 'required|integer|min:1',
            'offset'  => 'required|integer|min:0',
        ];
        $messages = [
            'custid.numerics' => 'Enter valid user ID',
            'phone.numerics'  => 'Enter valid Mobile Number',
            'phone.digits'    => 'Enter valid Mobile Number',
            'violaid.min'     => 'Enter valid Viola ID',
            'limit.required'  => 'Limit Is required',
            'limit.integer'   => 'Limit must be an Interger',
            'limit.min'       => 'Limit Minimum is 1',
            'offset.required' => 'Offeset is required',
            'offset.integer'  => 'Offset must be an integer',
            'offset.min'      => 'Minimum is required',
        ];

        if ( $type == 'event' )
        {
            $validat                     = [
                'sessId' => 'required|numerics',
                'limit'  => 'required|integer|min:1',
                'offset' => 'required|integer|min:0',
            ];
            $messages                    = [
                'limit.required'  => 'Limit Is required',
                'limit.integer'   => 'Limit must be an Interger',
                'limit.min'       => 'Limit Minimum is 1',
                'offset.required' => 'Offeset is required',
                'offset.integer'  => 'Offset must be an integer',
                'offset.min'      => 'Minimum is required',
                'sessId.required' => 'Enter sessionId',
                'sessId.numerics' => 'Session id must be in numeric',
            ];
        }

        $validator = validator::make($request->all(), $validat, $messages);
        $has_error = TRUE;
        if ( $validator->messages()->toArray() )
        {
            $errs = array();
            foreach ( $validator->messages()->toArray() as $key => $error )
            {
                $errs[$key] = $error[0];
            }
            $has_error = $errs;
        }
        return $has_error;
    }

}
