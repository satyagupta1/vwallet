<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Update Mobile Number template
 * 
 *  param $fullName varchar 
 *  param $otpCode varchar 
 *  *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = (isset($fullname)) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Personal Information updated',
        'text'    => $salutationName . ',<br/><br/> 
Thanks for updating your mobile number.  Your changes have been successfully submitted.<br/>
If you didn`t make this request, your account might be compromised. To get back into your account, you`ll need to reset your credentials.'],
    'sms'   => [
        'text' => $otpCode . ' is your OTP to change your mobile number. Did not make this request, please contact our Support.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Alert! Mobile number changed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Your mobile number has been changed from ' . $oldMobileNo . ' to ' . $newMobileNo,
    ],
];

echo json_encode($array);

/* End of file update-mobile_number.php */
