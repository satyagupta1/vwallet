<?php
// All Section Validations
return [
 'notification_for_req'   => 'Notification is required',
 
 'notification_type_req'   => 'Notification type is required',
 'notification_type_ach'   => 'Notification type must contain valid characters',
 'notification_type_min'   => 'Notification type should contain at least 3 characters',
 
 'application_name_req'   => 'Application name is required',
 'application_name_ad'    => 'Application name should contain characters with dash',
 'application_name_min'   => 'Application name should contain at least 4 characters',
 
 'email_id_req'       => 'email is required',
 'email_id_valid'      => 'Please give a valid email id',
 'email_data_req'      => 'Email data is required',
 'email_data_min'      => 'Email data should be minimum 10 characters in length',

 'mobile_no_req'       => 'Mobile number is required',
 'mobile_no_valid'      => 'Please enter a valid mobile number',
 'mobile_no_min'       => 'Mobile number should have atleast 9 digits',
 'mobile_data_req'      => 'Mobile data is required',
 'mobile_data_min'      => 'Mobile data should be minimum 10 characters in length',
 
 'device_type_req'      => 'Device type is required',
 'device_type_alpha'     => 'Device type should be alphabets only',
 'device_type_max'      => 'Device type should be maximum 10 characters in length',
 'device_type_min'      => 'Device type should be minimum 2 characters in length',
 
 'device_token_req'     => 'Device token is required',
 'device_token_max'     => 'Device token should be maximum 200 characters in length',

 'device_data_req'      => 'Device data is required',
 'device_data_min'      => 'Device data should be minimum 10 characters in length',

 'web_data_req'       => 'Web Data is required'
];
