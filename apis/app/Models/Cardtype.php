<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Cardtype
 * 
 * @property int $idCardType
 * @property string $CardTypeName
 * 
 * @property \Illuminate\Database\Eloquent\Collection $virtualcards
 *
 * @package App\Models
 */
class Cardtype extends Eloquent
{
	protected $table = 'cardtype';
	protected $primaryKey = 'idCardType';
	public $timestamps = false;

	protected $fillable = [
		'CardTypeName'
	];

	public function virtualcards()
	{
		return $this->hasMany(\App\Models\Virtualcard::class, 'idCardType');
	}
}
