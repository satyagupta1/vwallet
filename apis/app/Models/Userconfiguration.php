<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userconfiguration
 * 
 * @property int $ConfigurationID
 * @property int $UserWalletID
 * @property string $ConfigType
 * @property string $Details
 * @property string $Parameter1
 * @property string $Value1
 * @property string $Parameter2
 * @property string $Value2
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Userconfiguration extends Eloquent
{
	protected $table = 'userconfiguration';
        protected $primaryKey = 'ConfigurationID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ConfigurationID' => 'int',
		'UserWalletID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
                'UserWalletID',
		'ConfigType',
		'Details',
		'Parameter1',
		'Value1',
		'Parameter2',
		'Value2',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
