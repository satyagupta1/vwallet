<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback on hold (bus ticket booking)
 *  param $salutationName varchar
 *  param $nearestAgentURL varchar
 *  param $KycURLvarchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($primaryName) ? $salutation.' '.$primaryName : $salutation;
$array = [
    'email' => [
        'subject' => 'Wallet Credit on Hold!',
        'text'    => $salutationName . ',<br/><br/> 
        Your cashback is on hold as wallet limit is exceeded. 
        Dont worry, cashback will be credited on 1st of next month. 
        Too long? Do KYC for instant credit. 
        To Submit KYC online, Click <a href="'.$KycURL.'" >here</a>. Want to visit nearest agent, Click <a href="'.$nearestAgentURL.'" >here</a>',
    ],
    'sms'   => [
        'text' => $salutationName . ', Your cashback is on hold as wallet limit is exceeded. 
    Dont worry, cashback will be credited on 1st of next month. Too long? Do KYC for instant credit.'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => $salutationName . ', Your cashback is on hold as wallet limit is exceeded. Dont worry, cashback will be credited on 1st of next month. Too long? Do KYC for instant credit.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'busbooking'
    ],
    'web'   => [
        'text' => $salutationName . ', Your cashback is on hold as wallet limit is exceeded. Dont worry, cashback will be credited on 1st of next month. Too long? Do KYC for instant credit.',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
