<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\BillDesk;

class BillDeskReconController extends \App\Http\Controllers\Controller {

    public function generateReconFile(\Illuminate\Http\Request $request)
    {
        $output = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validaterecon($request);
        if ( $checkValidate !== TRUE )
        {
            $output['display_msg'] = $checkValidate;
            return response()->json($output);
        }

        $dateMain = $request->input('date');
        $fileName = 'recon/'.date("d-m-Y", strtotime($dateMain)) . '.txt';
        $handle   = fopen($fileName, "w");

        $selectArray = array(
            'transactiondetail.UserWalletId',
            'userrechargedetails.BillDeskTransactionID',
            'transactiondetail.Amount',
            'transactiondetail.RespBankRefNum',
            'transactiondetail.TransactionDetailID'
        );

        $selArray = array(
            'transactiondetail.UserWalletId',
            'userbillertransactions.BillDeskTransactionID',
            'transactiondetail.Amount',
            'transactiondetail.RespBankRefNum',
            'transactiondetail.TransactionDetailID'
        );

        $getRconRecharge = \App\Models\Transactiondetail::select($selectArray)
                        ->join('billerlist', 'billerlist.BillerId', '=', 'transactiondetail.TransactionType')
                        ->join('userrechargedetails', 'userrechargedetails.TransactionDetailID', '=', 'transactiondetail.TransactionDetailID')
                        ->whereBetween('transactiondetail.TransactionDate', [ $dateMain . ' 00:00:00', $dateMain . ' 23:59:59' ])
                        ->orderBy('transactiondetail.TransactionDetailID', 'ASC')
                        ->get()->toArray();

        $getRconBillPayments = \App\Models\Transactiondetail::select($selArray)
                        ->join('billdeskbillers', 'billdeskbillers.BillerID', '=', 'transactiondetail.TransactionType')
                        ->join('userbillertransactions', 'userbillertransactions.TransactionDetailID', '=', 'transactiondetail.TransactionDetailID')
                        ->whereBetween('transactiondetail.TransactionDate', [ $dateMain . ' 00:00:00', $dateMain . ' 23:59:59' ])
                        ->orderBy('transactiondetail.TransactionDetailID', 'ASC')
                        ->get()->toArray();

        $getRconData = array_merge($getRconRecharge, $getRconBillPayments);

        usort($getRconData, array( $this, 'sortArray' ));

        foreach ( $getRconData as $resultData )
        {
            // removing TransactionDetailID
            unset($resultData["TransactionDetailID"]);
            // Adding space
            array_splice($resultData, 3, 0, array( '' ));
            // formating amount value
            $resultData["Amount"] = number_format($resultData["Amount"], 2, '.', '');
            // import values with saparater
            $mainData             = implode('|', $resultData) . '|VOL' . "\r\n";
            // echo $mainData = $resultData->UserWalletId . '|' . $resultData->BillDeskTransactionID . '|' . $resultData->Amount . '||' . $resultData->RespBankRefNum . '|VOL\n';
            fwrite($handle, "$mainData");
        }

        fclose($handle);
        /*
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename=' . basename($fileName));
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($fileName));
          readfile($fileName);
          exit();
         * 
         */
    }

    /**
     * Sort Array based on transction Id
     * 
     * @param type $a
     * @param type $b
     * @return type
     */
    public static function sortArray($a, $b)
    {
        return ($a["TransactionDetailID"] <= $b["TransactionDetailID"]) ? -1 : 1;
    }

    /**
     * Validate Recharge request data
     * @param $request
     * @return String
     */
    private function _validaterecon($request)
    {
        $messages         = [];
        $validate['date'] = 'required';

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
