<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyotp
 * 
 * @property int $UserOTPID
 * @property int $PartyID
 * @property string $OTP
 * @property string $TransactionTypeCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partyotp extends Eloquent
{
	protected $table = 'partyotp';
	protected $primaryKey = 'OTPRequestID';
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyID',
		'OTP',
		'TransactionTypeCode',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
}
