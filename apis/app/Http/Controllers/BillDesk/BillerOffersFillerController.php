<?php

namespace App\Http\Controllers\BillDesk;

class BillerOffersFillerController extends \App\Http\Controllers\Controller {

    public function inserBillersByCircle(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $chk_valid = $this->_validateRequest($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }

        $billerId = $request->input('billerId');
        $circleId = $request->input('circleId');

        $billers = $this->billersCheck($billerId);
        if ( $billers === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.no_Billers_billdesk') );
            return json_encode($output_arr);
        }

        $circle = $this->circlesCheck($circleId);
        if ( $circle === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.no_Billers_billdesk') );
            return json_encode($output_arr);
        }

        // check for this biller circle offer details exsists are not
        $dataExsist = $this->dataCheck($billerId, $circleId);
        if ( $dataExsist > 0 )
        {
            $output_arr['display_msg'] = array( 'info' => trans('For this Biller circle offer details exsists.') );
            return json_encode($output_arr);
        }

        $sourceid    = config('vwallet.sourceId');
        $txnId       = \App\Libraries\BilldeskHelper::gettraceId(); // gen of transaction Id
        $dateTime    = config('vwallet.bdTimeStamp'); // gen of dateTime
        $messageCode = 'U03081';

        $arrayparams      = array(
            'MessageCode'   => $messageCode,
            'TraceID'       => $txnId,
            'SourceID'      => $sourceid,
            'TimeStamp'     => $dateTime,
            'mobilenumber'  => 'NA',
            'BillerId'      => $billerId,
            // 'BillerId' => 'AIRCELPRE',
            'CircleID'      => $circleId,
            // 'CircleID' => '4',
            'Extra Field 2' => 'Y',
            'Category'      => 'ALL',
            'FetchPlanType' => 'A',
            'Filler1'       => 'NA',
            'Filler2'       => 'NA',
            'Filler3'       => 'NA',
        );
        $implodeArraykeys = implode('~', $arrayparams);
        // Genartion of HMAC key form library
        $hmacKey          = \App\Libraries\BilldeskHelper::generateHMAC($implodeArraykeys);

        $stringKeys = $implodeArraykeys . '~' . strtoupper($hmacKey);

        // url for required api
        $url = config('vwallet.billdeskApiUrl') . $stringKeys;

        // get return response from curl method
        $response = \App\Libraries\BilldeskHelper::billDeskGETCURL($url);

        // check reponse if server is sending html reponse that their sever is down
        if ( $response != strip_tags($response) )
        {
            $output_arr['is_error']            = TRUE;
            $output_arr['display_msg']['info'] = 'Sorry we are unable to process your request.Please try after some time.';
            return json_encode($output);
        }

        $responseDecode  = json_decode($response);
        $rechargeDetails = $responseDecode->RechargeDetails;
        $coutuntRecharge = count($rechargeDetails);

        // return response when data is empty
        if ( $coutuntRecharge <= 0 )
        {
            $output_arr['display_msg'] = array( 'info' => 'No Data Found' );
            return json_encode($output_arr);
        }

        // checking opertor name exists or not
        if ( ! empty($rechargeDetails->operatorName) )
        {
            if ( isset($rechargeDetails->RechargeCategoryList) && ($rechargeDetails->RechargeCategoryList !== NULL) )
            {
                foreach ( $rechargeDetails->RechargeCategoryList as $rechargekey => $rechrgeCategories )
                {
                    foreach ( $rechrgeCategories as $mainValuerecharge )
                    {

                        $rechargeAmount      = $mainValuerecharge->rechargeAmount;
                        $talkTime            = $mainValuerecharge->talkTime;
                        $validity            = $mainValuerecharge->validity;
                        $rechargePlanId      = $mainValuerecharge->rechargePlanId;
                        $rechargeDescription = $mainValuerecharge->rechargeDescription;

                        $insertPlanDetails                  = new \App\Models\Operatorfeeplan;
                        $insertPlanDetails->FeePlanID       = $rechargePlanId;
                        $insertPlanDetails->Operator        = $billerId;
                        $insertPlanDetails->Circle          = $circleId;
                        $insertPlanDetails->CategoryName    = $rechargekey;
                        $insertPlanDetails->Mrp             = $rechargeAmount;
                        $insertPlanDetails->TalkTime        = $talkTime;
                        $insertPlanDetails->ServiceTax      = 0;
                        $insertPlanDetails->ProcessingFees  = 0;
                        $insertPlanDetails->AccessFees      = 0;
                        $insertPlanDetails->Validity        = $validity;
                        $insertPlanDetails->Description     = $rechargeDescription;
                        $insertPlanDetails->CreatedDateTime = date('Y-m-d h:s:i');
                        $insertPlanDetails->CreatedBy       = 'Admin';
                        $insertPlanDetails->save();
                        break;
                    }
                }

                if ( $insertPlanDetails )
                {
                    $output_arr['display_msg'] = array( 'info' => 'Data Uploaded Successfully' );
                    return json_encode($output_arr);
                }
            }
            else
            {
                $output_arr['display_msg'] = array( 'info' => 'No offers available' );
                return json_encode($output_arr);
            }
        }
        else
        {
            $output_arr['display_msg'] = array( 'info' => 'No offers available' );
            return json_encode($output_arr);
        }
    }

    /**
     *  Get biller Info from billerlist table
     * 
     * @return type
     */
    public function billersCheck($BillerId)
    {
        $selBillers = \App\Models\Billerlist::select('BillerId')
                        ->where('BillerStatus', 'Y')
                        ->where('BillerId', $BillerId)->first();
        return ( $selBillers ) ? $selBillers : FALSE;
    }

    /**
     *  Get biller Info from billerlist table
     * 
     * @return type
     */
    public function circlesCheck($circleId)
    {
        $selBillers = \App\Models\Circlemaster::select('Circle_Id')->where('Circle_Id', $circleId)->first();
        return ( $selBillers ) ? $selBillers : FALSE;
    }

    /**
     *  to check for this biller circle offer details inserted or not
     * @param type $billerId
     * @param type $circleId
     */
    public function dataCheck($billerId, $circleId)
    {
        $feePlan = \App\Models\Operatorfeeplan::select('PlanID')
                        ->where('Circle', $circleId)
                        ->where('Operator', $billerId)->count();
        return $feePlan;
    }

    private function _validateRequest($request)
    {
        $validate = array(
            'billerId' => 'required',
            'circleId' => 'required',
        );
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
