<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Querycontact
 * 
 * @property int $idQueryContact
 * @property int $partyId
 * @property string $fullname
 * @property string $mobileno
 * @property string $emailid
 * @property string $message
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property string $status
 *
 * @package App\Models
 */
class Querycontact extends Eloquent
{
	protected $table = 'querycontact';
	protected $primaryKey = 'idQueryContact';
	public $timestamps = false;

	protected $casts = [
		'partyId' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'partyId',
		'fullname',
		'mobileno',
		'emailid',
		'message',
		'CreatedDateTime',
		'CreatedBy',
		'status'
	];
}
