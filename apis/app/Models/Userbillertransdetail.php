<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Dec 2017 07:38:37 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userbillertransdetail
 * 
 * @property int $UserBillerTransrDetailId
 * @property int $UserBillerTransrid
 * @property string $TagName
 * @property string $TagValue
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * 
 * @property \App\Models\Userbillertransaction $userbillertransaction
 *
 * @package App\Models
 */
class Userbillertransdetail extends Eloquent
{
    protected  $table = 'userbillertransdetails';
    protected $primaryKey = 'UserBillerTransrDetailId';
	public $timestamps = false;

	protected $casts = [
		'UserBillerTransrid' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'UserBillerTransrid',
		'TagName',
		'TagValue',
		'CreatedDateTime',
		'CreatedBy'
	];

	public function userbillertransaction()
	{
		return $this->belongsTo(\App\Models\Userbillertransaction::class, 'UserBillerTransrid');
	}
}
