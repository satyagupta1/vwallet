<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userrechargedetail
 * 
 * @property int $RechargeDetailID
 * @property int $UserWalletID
 * @property int $TransactionDetailID
 * @property string $RechargeCategory
 * @property string $RechargeType
 * @property string $SubscriberNumber
 * @property string $SubScriberName
 * @property int $UseroperatorfeeplanID
 * @property float $Amount
 * @property string $Status
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * @property string $PaymentID
 * @property string $BillDesckTransactionID
 * @property string $ValidationID
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Useroperatorfeeplan $useroperatorfeeplan
 * @property \Illuminate\Database\Eloquent\Collection $billdeskapimessages
 * @property \Illuminate\Database\Eloquent\Collection $userrechargetrackers
 *
 * @package App\Models
 */
class Userrechargedetail extends Eloquent
{
	protected $primaryKey = 'RechargeDetailID';
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'TransactionDetailID' => 'int',
		'UseroperatorfeeplanID' => 'int',
		'Amount' => 'float'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'UserWalletID',
		'TransactionDetailID',
		'RechargeCategory',
		'RechargeType',
		'SubscriberNumber',
		'SubScriberName',
		'UseroperatorfeeplanID',
		'Amount',
		'Status',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDatetime',
		'PaymentID',
		'BillDesckTransactionID',
		'ValidationID'
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function useroperatorfeeplan()
	{
		return $this->belongsTo(\App\Models\Useroperatorfeeplan::class, 'UseroperatorfeeplanID');
	}

	public function billdeskapimessages()
	{
		return $this->hasMany(\App\Models\Billdeskapimessage::class, 'RechargeDetailID');
	}

	public function userrechargetrackers()
	{
		return $this->hasMany(\App\Models\Userrechargetracker::class, 'RechargeDetailID');
	}
}
