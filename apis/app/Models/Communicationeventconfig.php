<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Communicationeventconfig
 * 
 * @property int $EventID
 * @property string $EventTag
 * @property string $SMSNotification
 * @property string $EmailNotification
 * @property string $ScreenNotification
 * @property int $SMSTemplateID
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $EmailTemplateID
 * @property string $ScreenNotificationTemplateID
 * 
 * @property \App\Models\Communicationtemplate $communicationtemplate
 *
 * @package App\Models
 */
class Communicationeventconfig extends Eloquent
{
	protected $table = 'communicationeventconfig';
	protected $primaryKey = 'EventID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'EventID' => 'int',
		'SMSTemplateID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'EventTag',
		'SMSNotification',
		'EmailNotification',
		'ScreenNotification',
		'SMSTemplateID',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'EmailTemplateID',
		'ScreenNotificationTemplateID'
	];

	public function communicationtemplate()
	{
		return $this->belongsTo(\App\Models\Communicationtemplate::class, 'SMSTemplateID');
	}
}
