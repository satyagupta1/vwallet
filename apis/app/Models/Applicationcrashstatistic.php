<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Applicationcrashstatistic
 * 
 * @property int $ApplicationCrashStatisticsID
 * @property string $Module
 * @property string $ScreenName
 * @property string $LoginUserID
 * @property int $StepNumber
 * @property string $ActionName
 * @property string $SessionID
 * @property string $DeviceID
 * @property string $ExternalLogParth
 * @property string $ReportDate
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 *
 * @package App\Models
 */
class Applicationcrashstatistic extends Eloquent
{
	protected $primaryKey = 'ApplicationCrashStatisticsID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ApplicationCrashStatisticsID' => 'int',
		'StepNumber' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'Module',
		'ScreenName',
		'LoginUserID',
		'StepNumber',
		'ActionName',
		'SessionID',
		'DeviceID',
		'ExternalLogParth',
		'ReportDate',
		'CreatedDateTime',
		'CreatedBy'
	];
}
