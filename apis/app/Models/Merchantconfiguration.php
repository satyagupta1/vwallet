<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantconfiguration
 * 
 * @property int $MerchantID
 * @property string $Section
 * @property string $SecKey
 * @property string $SecTitle
 * @property string $SecDescription
 * @property string $SecValue
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 *
 * @package App\Models
 */
class Merchantconfiguration extends Eloquent
{
	protected $table = 'merchantconfiguration';
	protected $primaryKey = 'Section';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int'
	];

	protected $fillable = [
		'MerchantID',
		'SecKey',
		'SecTitle',
		'SecDescription',
		'SecValue'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}
}
