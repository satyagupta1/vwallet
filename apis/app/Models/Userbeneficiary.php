<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userbeneficiary
 * 
 * @property int $BeneficiaryID
 * @property int $UserWalletID
 * @property string $BeneficiaryType
 * @property string $BankIFSCCode
 * @property string $MMID
 * @property string $AccountName
 * @property string $AccountNumber
 * @property int $WalletBeneficiaryMobileNumber
 * @property string $WalletBeneficiaryName
 * @property string $TripleClickAmount
 * @property string $Comments
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \Illuminate\Database\Eloquent\Collection $transactionschedules
 *
 * @package App\Models
 */
class Userbeneficiary extends Eloquent
{
	protected $table = 'userbeneficiary';
	protected $primaryKey = 'BeneficiaryID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BeneficiaryID' => 'int',
		'UserWalletID' => 'int',
		'WalletBeneficiaryMobileNumber' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'BeneficiaryType',
		'BankIFSCCode',
		'MMID',
		'AccountName',
		'AccountNumber',
		'WalletBeneficiaryMobileNumber',
		'WalletBeneficiaryName',
		'TripleClickAmount',
		'Comments',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function transactionschedules()
	{
		return $this->hasMany(\App\Models\Transactionschedule::class, 'BeneficiaryID');
	}
}
