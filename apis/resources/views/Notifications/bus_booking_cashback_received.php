 <?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Topup success with Transaction Details device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $cardnumber varchar
 *  param $date varchar
 *  param $time varchar
 *  param $totalamount varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation ;
$array = [
    'email' => [
        'subject' => 'Money Credited into your ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
                        Top up of INR ' . $amount . ' successful<br/>
                        Transaction Details<br/>
                        Credit / Debit Card number: ' . $cardnumber . '<br/>
                        Total Amount: ' . $totalamount . '<br/>
                        Time: ' . $time . '<br/>
                        Date: ' . $date,
    ],
    'sms'   => [
        'text' => 'TKT NO '.$ticketNumber.'
Name:'.$primaryName.','.$from.'-'.$to.' ,'.$operatorName.', 
Boarding '.$boarding.'
<seat No.>
<original Fare> Amount
<Discounted Fare> Amount
<your Savings> amount
< Boarding Point> Location
<Landmark>'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Top Up Successful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Rs ' . $amount . ' Topped Up into your ViolaWallet',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
