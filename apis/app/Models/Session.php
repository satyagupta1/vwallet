<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Session
 * 
 * @property string $id
 * @property string $ip_address
 * @property int $timestamp
 * @property boolean $data
 *
 * @package App\Models
 */
class Session extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'timestamp' => 'int',
		'data' => 'boolean'
	];

	protected $fillable = [
		'timestamp',
		'data'
	];
}
