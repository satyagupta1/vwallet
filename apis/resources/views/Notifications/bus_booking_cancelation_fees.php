<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * BusTicket Booking Cancelation Fees
 * 
 *  param $ticketNumber varchar
 *  param $placeOne varchar
 *  param $placeTwo varchar 
 *  param $customerName varchar 
 *  param $operatorName varchar 
 *  param $amount decimal
 *  param $cancellationFeeAmount decimal
 *  param $leviedAmount decimal
 *  param $vwalletEmail varchar
 */

// get salutation from language file
$salutation = trans('messages.salutation');
$salutationName = ($customerName) ? $salutation.' '.$customerName : $salutation;
$array = [
    'email' => [
        'subject' => 'Cancellation Fee -Ticket number: '.$ticketNumber,
        'text'    => $salutationName.',<br/><br/>
                        
This is to let you know that your '.$placeOne.' to '.$placeTwo.' bus booking has been cancelled by you. we have charged a fee of Rs '.$cancellationFeeAmount.'  as per the cancellation policy<br/><br/>

Book Again',
    ],
    'sms'   => [
        'text' => 'Dear '.$customerName.', A fee of INR '.$amount.' has been charged for cancelling ticket no '.$ticketNumber.' with  bus operator '.$operatorName.'.  Please contact customer support for further help.'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cancellation fee of Rs '.$cancellationFeeAmount.' has been charged. Ticket no. '.$ticketNumber,
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Ticket Number '.$ticketNumber.' cancelled and Rs '.$leviedAmount.' levied as cancellation fee'
    ],
];

echo json_encode($array);

/* End of file bus_booking_cancelation_fees */
