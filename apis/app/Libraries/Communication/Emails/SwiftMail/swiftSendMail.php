<?php

namespace App\Libraries\Communication\Emails\SwiftMail;

class swiftSendMail {

    static protected $smtp_host = 'smtp.gmail.com';
    static protected $smtp_port = '587';
    static protected $smtp_crypto = 'tls';
    static protected $smtp_user = 'logs@vwallet.in';
    static protected $smtp_unme = 'No Reply V-Wallet';
    static protected $smtp_pass = 'Vwallet@123';

    public static function sendSmtpMailNow($final_data) {
        error_reporting(E_ALL);

        $path = __DIR__.'/swift_mailer/swift_required.php';
        require_once $path;
        
        // $swifte = new Swift_Mailer($app['swift.transport']);
        // return $swifte;
        try {
            // Your code to send the email
            //Create the Transport
            $transport = \Swift_SmtpTransport::newInstance(swiftSendMail::$smtp_host, swiftSendMail::$smtp_port)->setUsername(swiftSendMail::$smtp_user)->setPassword(swiftSendMail::$smtp_pass);
            if (swiftSendMail::$smtp_crypto != '') {
                $transport->setEncryption(swiftSendMail::$smtp_crypto);
            }
            // ADD THIS LINE ON LOCAL
            if ($_SERVER['SERVER_NAME'] == 'localhost') {
                $transport->setLocalDomain('[127.0.0.1]');
            }
            //dump_exit($transport);
            //Create the Mailer using your created Transport
            $mailer = \Swift_Mailer::newInstance($transport);

            //Create the message
            $message = \Swift_Message::newInstance();

            //Give the message and subject
            $message->setSubject($final_data['subject'])
            ->setFrom(array(swiftSendMail::$smtp_user => swiftSendMail::$smtp_unme))
            ->setTo(array($final_data['to_email'] => $final_data['to_name']))
            ->setBody($final_data['mail_body'], 'text/html');
            if(!empty($final_data['cc_email']))
            {
                $message->setCc(array($final_data['cc_email'] => $final_data['cc_email']));
            }

            if ($final_data['attachment'] != '') {
                // Create the attachment with your data
                if ($final_data['filetype'] == 'pdf') {
                    $attach = \Swift_Attachment::newInstance($final_data['attachment'], $final_data['attname'], 'application/pdf');
                }
                // Attach it to the message
                $message->attach($attach);
            }
            // var_dump($message);

            //Send the message
            $result = $mailer->send($message);
            //dump_exit($result);
            if ($result) {
                return $result;
            }
            else {
                return 'Error Code: 011. Sorry! We can\'t send email right now. Please try again later.';
            }
        } catch (Exception $e) {
            return 'Error Code: 012. An error occurred:' . $e->getMessage();
        }
    }
}

?>
