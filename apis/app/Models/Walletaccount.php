<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletaccount
 * 
 * @property int $WalletAccountId
 * @property string $WalletAccountNumber
 * @property string $WalletAccountPrefix
 * @property int $UserWalletId
 * @property string $PrimaryAccountIndicator
 * @property string $AccountLinked
 * @property \Carbon\Carbon $AccountDelnikedDate
 * @property string $WalletCurrencyCode
 * @property float $MaximumBalanceLimit
 * @property int $FeePercentage
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property float $AvailableBalance
 * @property string $IsAdminWallet
 * @property string $AssetLiabilityFlag
 * @property float $CurrentAmount
 * @property float $NonWithdrawAmount
 * @property float $HoldAmount
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \Illuminate\Database\Eloquent\Collection $dailytransactionlogs
 * @property \Illuminate\Database\Eloquent\Collection $transactiondetails
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 * @property \Illuminate\Database\Eloquent\Collection $walletcards
 *
 * @package App\Models
 */
class Walletaccount extends Eloquent
{
	protected $table = 'walletaccount';
	public $timestamps = false;
        protected $primaryKey = 'WalletAccountId';

	protected $casts = [
		'UserWalletId' => 'int',
		'MaximumBalanceLimit' => 'float',
		'FeePercentage' => 'int',
		'AvailableBalance' => 'float',
		'CurrentAmount' => 'float',
		'NonWithdrawAmount' => 'float',
		'HoldAmount' => 'float'
	];

	protected $dates = [
		'AccountDelnikedDates',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
                'UserWalletId',
		'WalletAccountPrefix',
                'WalletAccountNumber',
		'PrimaryAccountIndicator',
		'AccountDelnikedDate',
		'WalletCurrencyCode',
		'MaximumBalanceLimit',
		'FeePercentage',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'AvailableBalance',
		'IsAdminWallet',
		'AssetLiabilityFlag',
		'CurrentAmount',
		'NonWithdrawAmount',
		'HoldAmount',
           
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletId');
	}

	public function dailytransactionlogs()
	{
		return $this->hasMany(\App\Models\Dailytransactionlog::class, 'TransactionMerchantWalletAccountId');
	}

	public function transactiondetails()
	{
		return $this->hasMany(\App\Models\Transactiondetail::class, 'ToWalletId', 'UserWalletId');
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class, 'UserWalletId', 'UserWalletId');
	}

	public function walletcards()
	{
		return $this->hasMany(\App\Models\Walletcard::class, 'WalletAccountID');
	}
}
