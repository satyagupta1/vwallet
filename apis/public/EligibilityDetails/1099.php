<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction system should identify if it is a UPI transaction of value >= INR 50.</li>
        <li>System should then identify the counterparty UPI ID of the transaction.</li>
        <li>System should credit INR 50 cashback into the users wallet only if the counterparty UPI ID identified above has appeared for the first time ever in the transaction history of ther user.</li>
    </ul> 
</div>
<?php include 'footer.php';?>