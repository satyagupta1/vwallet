<?php

namespace App\Http\Controllers\Topup;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TopUpController extends \App\Http\Controllers\Controller {

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function addMoney(\Illuminate\Http\Request $request)
    {
        // session Check
        $sesionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sesionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return response()->json($output_arr);
        }
        
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        // check validation
        $chk_valid  = $this->_validationCheck($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }
        
        $offerData = NULL;
        $requestArray =  [];
        if ($request->input('promoCode') != NULL)
        {
            $requestArray['aplicableChannel'] = $request->header('device-type');
            $requestArray['promoCode']        = $request->input('promoCode');
            //$requestArray['offerCategoryId'] = 0;
            // $requestArray['offerUserTypeId'] = 1;
            $requestArray['benefitCategoryId'] = 7; //cashback only
            if ($request->input('offerCategoryId') != NULL)
            {
                $requestArray['offerCategoryId'] = $request->input('offerCategoryId');
            }
            if ($request->input('offerUserTypeId') != NULL)
            {
                $requestArray['offerUserTypeId'] = $request->input('offerUserTypeId');
            } 
            $requestArray['amount']  = $request->input('amount');
            $requestArray['partyId'] = $request->input('partyId');
            $requestArray['paymentProductId'] = $request->input('paymentProductId');
            $verifyJsonData          = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
            $verifyData              = json_decode($verifyJsonData);
           
            if ($verifyData->is_error == TRUE)
            {
                return $verifyJsonData;
            }
            $offerData  = $verifyData->res_data;
            $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
            if (!empty($pamentInfo))
            {                 
                $offerPamentInfo = array('offerPamentInfo' => $pamentInfo);
                $request->request->add($offerPamentInfo);
            }
        }

        if ($request->input('savedcardId'))
        {
            $isUserCard = \App\Libraries\TransctionHelper::checkUserCard($request);
            if ($isUserCard == FALSE)
            {
                $attrIfsc                            = trans('messages.attributes.cardInfo');
                $message                             = trans('messages.nameInvalid', ['name' => $attrIfsc]);
                $output_arr['display_msg']['cardNo'] = $message;
                return response()->json($output_arr);
            }
        }

        if ($request->input('cardNo') != NULL)
        {
            $attrIfsc = trans('messages.attributes.cardNo');
            $message  = trans('messages.nameInvalid', ['name' => $attrIfsc]);
            // check which type of card it is
            $card     = \App\Libraries\CreditCard::validCreditCard($request->input('cardNo'));
            if ($card['valid'] == FALSE)
            {
                $output_arr['display_msg']['cardNo'] = $message;
                return response()->json($output_arr);
            }
            //if valid card take card vendor type /visa/mastercard
            $request->request->add(['cardVendor' => $card['type']]);

            //Check for exp validation
            $year            = date('Y');
            $yearFirstTwoDig = trim(substr($year, 0, 2));
            $cardExp         = $request->input('cardExp');
            $expiryMonth     = trim(substr($cardExp, 0, 2));
            $expiryYear      = $yearFirstTwoDig . trim(substr($cardExp, 2, 4));
            $expValid        = \App\Libraries\CreditCard::validDate($expiryYear, $expiryMonth);
            if ($expValid == FALSE)
            {
                $attrCardExp                          = trans('messages.attributes.cardExp');
                $output_arr['display_msg']['cardExp'] = trans('messages.nameInvalid', ['name' => $attrCardExp]);
                return response()->json($output_arr);
            }
        }

        $partyId  = $request->input('partyId');
        $amount   = $request->input('amount');
        $entityId = $request->input('businessModelid'); // id for credit card/debit card/netbanking/upi...
        $cardId   = $request->input('savedcardId');
       
        $transBool         = TRUE;
        $paymentsProductID = 1;
       
        // user exsistens and kyc status check
        $partyInfo = \App\Libraries\TransctionHelper::_userValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return response()->json($output_arr);
        }
        else if ($partyInfo->WalletStatus === 'inactive')
        {
            $output_arr['display_msg'] = array('info' => trans('messages.inactiveKyc'));
            return response()->json($output_arr);
        }

        // code for money transfer from Not saved card starts
        if (( $request->input('savecardList') == 'Y' ) && ($cardId != ''))
        {
            $select_wallet_savedcards = \App\Models\Userexternalcard::select('MoneySourceCode', 'CardVendor', 'CardNumber', 'CardExpiryDate')
                            ->where('UserExternalCardID', '=', $cardId)
                            ->where('PartyID', '=', $request->input('partyId'))->count();

            if ($select_wallet_savedcards <= 0)
            {
                $output_arr['display_msg'] = array('info' => trans('messages.noSavedCards'));
                return response()->json($output_arr);
            }
        }
 
        // get config parameter based on payment entity id and product id and wallet plane id
        $productParams = \App\Libraries\TransctionHelper::paymentEntity($entityId, $paymentsProductID, $partyInfo->WalletPlanTypeId);
        if (!$productParams)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return response()->json($output_arr);
        }

        $entityPrams = \App\Libraries\TransctionHelper::productPlanParams($productParams->ConfigParamNum);
        $feeInfo = array();
        if ($entityPrams)
        {
            foreach ($entityPrams as $value)
            {
                $feeInfo[$value->ParameterTagName] = $value->ParameterTagValue;
            }
        }

        // code to check min amount for transaction
        if ($request->input('amount') < $feeInfo['MinTransAmt'])
        {
            $output_arr['display_msg'] = array('info' => trans('messages.topupMinRequired'));
            return response()->json($output_arr);
        }
        
        // get topup limits
        $topupLimits = config('vwallet.topupWalletLimits');
        
        $kycLimits = $topupLimits['03'];
        if ( isset($topupLimits[$partyInfo->KycStatus]))
        {
            $kycLimits = $topupLimits[$partyInfo->KycStatus];
        }
        
        $transactionLimit = $kycLimits['walletLimit'] - $partyInfo->CurrentAmount;
        // code to per limit amount per transaction
        if ($request->input('amount') > $transactionLimit)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.canNotAddMoreMoney', ['name' => $transactionLimit]));
            return response()->json($output_arr);
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionCreditLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
        
        if ($partyTransLimit['is_error'])
        {
            $output_arr['display_msg'] = $partyTransLimit['display_msg'];
            return response()->json($output_arr);
        }
        $partyWalletPlanId = $partyTransLimit['res_data']['WalletPlanTypeId'];

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductID);

        // fee calculation 
        $fee = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $entityId, $partyId);

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, TRUE, $transBool, $sesionId);
        if ($transRes['error'])
        {
            $output_arr['display_msg'] = array('info' => $transRes['info']);
            return response()->json($output_arr);
        }

        $request->request->add(['transactionId' => $transRes['response']['TransactionDetails']['TransactionOrderID']]);
        $request->request->add(['transactionOrderId' => $transRes['response']['TransactionDetails']['TransactionDetailID']]);
        $returnData = \App\Libraries\Payments\Ccavenue::kotakApiRequest($request);

        $resArray = array(
            'pageStatus'         => 'openBrowser',
            'transactionOrderId' => $transRes['response']['TransactionDetails']['TransactionOrderID'],
            'result'             => $returnData
        );

        $output_arr['display_msg'] = array('info' => $transRes['info']);
        $output_arr['is_error']    = FALSE;
        $output_arr['res_data']    = $resArray;
        //return $output_arr;
        return response()->json($output_arr);
    }

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param PartyId integer
     * @param WalletAcntId integer
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * @param card_no integer
     * @param card_exp integer
     * @param card_hname string
     * @param money_source string
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _validationCheck($request)
    {

        // validations        
        $validateCard       = $validatenetBanking = array();
        $validateArray      = array(
            'partyId'         => 'required|numeric',
            'walletacntId'    => 'required|numeric',
            'amount'          => 'required',
            'currencyCode'    => 'required|in:INR',
            'savecardList'    => 'required|in:Y,N',
            'moneySource'     => 'required|in:CRDC,DBCRD,NBK,CASHC,MOBP,EMI,WLT,UPI',
            'businessModelid' => 'required|numeric',
            'saveMyCard'      => 'required|in:Y,N',
            'paymentProductId' => 'required|numeric',
        );
        $businessModelid    = $request->input('businessModelid');
        if ($request->input('savecardList') == 'N')
        {
            $nonCard = array(2, 7);
            if (!in_array($businessModelid, $nonCard))
            {
                $validateCard = array(
                    'cardNo'         => 'required|digits_between:11,17',
                    'cardExp'        => 'required|min:4|max:4',
                    'cardholderName' => 'required|min:3|max:50',
                    'cardCvv'        => 'required|min:3|max:4',
                );
            }

            if ($businessModelid == 2)
            {
                $validateCard = array(
                    'vpa' => 'required'
                );
            }
        }
        if (( $request->input('savecardList') == 'Y' ) && ($request->input('businessModelid') != 7))
        {
            $validateCard = array(
                'savedcardId' => 'required|numeric',
                'cardCvv'     => 'required|min:3|max:4',
            );
        }

        if ($request->input('businessModelid') == 7)
        {
            $validatenetBanking = array(
                'netBankingCode' => 'required',
                'cardVendor'     => 'required',
            );
        }
        if ( trim($request->input('promoCode')) != NULL )
        {
            $validateArray['paymentProductId'] = 'required|numeric';
            $validateArray['offerCategoryId']  = 'required|string';
        }
        $validate = array_merge($validateArray, $validateCard, $validatenetBanking);
        $messages = array();

        // validation for mobile device
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function updateUpiTransaction(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $chk_valid  = self::_validateUpdateUpiTrans($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }
        $transctionId      = $request->input('transactionId');
        $partyId           = $request->input('partyId');
        $response          = $request->input('response');
        $yblTxnId          = $request->input('yblTxnId');
        $custRefNo         = $request->input('custRefNo');
        $transStatus       = $request->input('transStatus');
        $partyVPA          = $request->input('partyVPA');
        $beneficiaryVPA    = $request->input('beneficiaryVPA');
        $businessModelid   = 2;
        $paymentsProductID = 1;

        // user exsistens test
        $partyInfo = \App\Libraries\TransctionHelper::_userValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $select            = array(
            'TransactionDetailID',
            'TransactionOrderID',
            'PaymentsProductID',
            'UserWalletId',
            'FromAccountId',
            'ToAccountId',
            'TransactionDetailID',
            'FeeAmount',
            'TaxAmount',
            'TransactionType',
            'Amount',
            'ToWalletId',
            'TransactionTypeCode',
            'TransactionDate',
            'TransactionStatus',
            'TransactionMode'
        );
        $transactionDetail = \App\Models\Transactiondetail::select($select)
                        ->where('TransactionDetailID', $transctionId)->first();
        if (!$transactionDetail)
        {
            $output_arr['display_msg']['transId'] = 'Transaction details doesn`t found.';
            return response()->json($output_arr);
        }

        $updatePartyExternalData = array(
            'BeneficiaryVPA' => $partyVPA
        );
        \App\Models\Userexternalbeneficiary::where('BeneficiaryID', $transactionDetail->FromAccountId)->update($updatePartyExternalData);
        $updateBenefExternalData = array(
            'BeneficiaryVPA' => $beneficiaryVPA
        );
        \App\Models\Userexternalbeneficiary::where('BeneficiaryID', $transactionDetail->ToAccountId)->update($updateBenefExternalData);

        $upiDetails = \App\Models\Userbeneficiary::where('UserWalletID', $partyInfo->UserWalletID)
                ->where('BeneficiaryVPA', $partyVPA)
                ->first();

        if (!$upiDetails)
        {
            $insertUPI = array(
                'UserWalletID'        => $partyInfo->UserWalletID,
                'BeneficiaryType'     => 'UPI',
                'BeneficiaryWalletID' => $partyInfo->WalletAccountId,
                'BeneficiaryStatus'   => 'Y',
                'BeneficiaryVPA'      => $partyVPA,
                'SelfBenficiary'      => 'Y'
            );
            \App\Models\Userbeneficiary::insert($insertUPI);
        }

        $transactionDetail->ProductName = 'TOPUP';
        $transAmount                    = $transactionDetail->Amount;

        // Updated Transaction details table
        \App\Models\Transactiondetail::where('TransactionDetailID', $transctionId)
                ->update(['TransactionStatus' => $transStatus,
                    'RespPGTrackNum'    => $yblTxnId,
                    'RespBankRefNum'    => $custRefNo]);

        // Created log file for Response
        $messageType                          = 'yesBank';
        $paramsData                           = array('transDetailId' => $transctionId, 'messageType' => $messageType, 'uniqueId' => $yblTxnId);
        \App\Libraries\Vwallet::ResponseLog($response, $paramsData);
        $transactionDetail->TransactionStatus = $transStatus;
        if ($transStatus != 'Success')
        {
            $transStatus               = 'Transaction ' . $transStatus;
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array('info' => $transStatus);
            $output_arr['res_data']    = $transactionDetail;
            return $output_arr;
        }

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductID);

        // fee calculation 
        $fee = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyInfo->WalletPlanTypeId, $transAmount, $businessModelid, $partyId);

        if (( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ))
        {
            $feeWalletAcc                   = self::getAdminWalletInfo(2);
            $taxWalletAcc                   = self::getAdminWalletInfo(3);
            // update Fee wallet balance
            $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
            $updateFeeBal->AvailableBalance += $fee['feeAmount'];
            $updateFeeBal->CurrentAmount    += $fee['feeAmount'];
            $updateFeeBal->save();

            // update Tax wallet Balance
            $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
            $updateTaxBal->AvailableBalance += $fee['taxAmount'];
            $updateTaxBal->CurrentAmount    += $fee['taxAmount'];
            $updateTaxBal->save();
        }

        // update Benificiary Balance
        $updateAdminBal                   = \App\Models\Walletaccount::find(1);
        $updateAdminBal->AvailableBalance -= $transAmount;
        $updateAdminBal->CurrentAmount    -= $transAmount;
        $updateAdminBal->save();

        // update Party balance
        $updateUserBal                   = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);
        $updateUserBal->AvailableBalance += $transAmount;
        $updateUserBal->CurrentAmount    += $transAmount;
        $updateUserBal->save();

        $retunInfo = \App\Libraries\TransctionHelper::getWalletInfo($partyId);
        $retunInfo['TransactionDetails'] = $transactionDetail;  
        $retunInfo['TransactionDetails']['Comments'] = "TOPUP of Rs.".$transAmount." from Viola Wallet(UPI)";
        $output_arr['res_data']    = $retunInfo;
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array('info' => 'Transaction successfully.');        
        return $output_arr;
    }

    private static function _validateUpdateUpiTrans($request)
    {
        $validateArray = array(
            'partyId'        => 'required|numeric',
            'transactionId'  => 'required',
            'response'       => 'required',
            'yblTxnId'       => 'required',
            'custRefNo'      => 'required',
            'transStatus'    => 'required',
            'partyVPA'       => 'required',
            'beneficiaryVPA' => 'required',
        );

        $messages = array();

        // validation for mobile device
        $validate_response = \Validator::make($request->all(), $validateArray, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public static function getAdminWalletInfo($userId)
    {
        $userExsist = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.PreferredPrimaryCurrency', 'walletaccount.WalletAccountId', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $userId)
                ->first();
        return(!$userExsist ) ? FALSE : $userExsist;
    }

}
