<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Get flat 5% Discount on bill payments of ACT Broadband done through Viola Wallet.</li>
        <li>Minimum amount spend to redeem the discount is Rs.500</li>
        <li>The code can be redeemed only once per user.</li>
        <li>Limited period offer.</li>
        <li>The offer is applicable on operator ACT Broadband only.</li>
    </ul> 
</div>
<?php include 'footer.php';?>