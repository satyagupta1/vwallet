<?php

namespace App\Libraries;

class OfferBussness {
    
    static protected $staticBenefitCriteria = array( '28' => 'Fixed', '29' => 'Percentage', '30' => 'Least', '31' => 'Highest' );
    
    /**
     * Insert data in offerbenifittrack table
     * @param Request $request
     * @return type
     */
    public static function benefitTrack($request)
    {
        $startDateTime = date('Y-m-d 00:00:00');
        $dateTime = date('Y-m-d H:i:s');
        $defaltAmount = '0.00';
        $JobName = 'addOfferBenefitTrack';
        $batchJobMaster = \App\Models\Batchjobmaster::where('JobName', $JobName)->orderBy('BatchJobMasterId', 'desc')->first();
        if ($batchJobMaster)
        {
            $startDateTime = $batchJobMaster->ExtractionEndDTTM;
        }
        
        $transData = \App\Models\Transactiondetail::select('transactiondetail.TransactionDetailID', 'transactiondetail.TransactionOrderID', 'transactiondetail.PaymentsProductID', 'transactiondetail.UserWalletId', 'transactiondetail.FromAccountId', 'transactiondetail.ToWalletId', 'transactiondetail.ToAccountId', 'transactiondetail.TransactionMode', 'transactiondetail.Amount', 'transactiondetail.TransactionDate', 'transactiondetail.EventCode', 'transactiondetail.PromoCode', 'transactiondetail.CreatedDatetime', 'offerbenefitsbridge.BenefitBusinessID', 'offerbenefits.OfferBenefitID', 'offerbenefits.BenefitName', 'offerbenefits.BenefitEffectiveDate', 'offerbenefits.BenefitExpiryDate', 'offerbenefits.BenefitCategoryRefID', 'offerbenefits.BenefitCriteriaRefID', 'offerbenefits.BenefitFixedAmount', 'offerbenefits.BenefictPercentageValue', 'promotionaloffers.PromotionalOfferID', 'promotionaloffers.OfferCategoryRefID')
                ->where('TransactionStatus', 'Success')->where('EventCode', 'CASHBACK')
                ->whereBetween('transactiondetail.CreatedDatetime', [$startDateTime, $dateTime])
                ->join('promotionaloffers', 'promotionaloffers.PromoCode', 'transactiondetail.PromoCode')
                ->join('offerbenefitsbridge', 'offerbenefitsbridge.OfferBusinessID', 'promotionaloffers.OfferBusinessID')
                ->join('offerbenefits', 'offerbenefits.BenefitBusinessID', 'offerbenefitsbridge.BenefitBusinessID')
                ->get();
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        if ($transData->count() > 0)
        {
            $totalCount = $transData->count();
            $StartDTTM = $EndDTTM = $dateTime;
            foreach($transData as $val => $value)
            {
                if ($val == 0)
                {
                    $StartDTTM = $value->CreatedDatetime;
                }
                if ($val == $totalCount - 1)
                {
                    $EndDTTM = $value->CreatedDatetime;
                }
                    
                $wallId = $value->UserWalletId;
                $accId = $value->FromAccountId;
                if ($value->TransactionMode === 'Credit')
                {
                    $wallId = $value->ToWalletId;
                    $accId = $value->ToAccountId;
                }
                //$requestData             = [ 'ReferenceID' => $value->OfferCategoryRefID ];
                
                $array = array(
                    'promoCode' => $value->PromoCode,
                    'amount' => $value->Amount,
                    'isVerified' => TRUE
                );
                $response = \App\Libraries\OfferRules::getBenefitAmount($array, $value);
                
                $insertData = array(
                    'UserWalletID'      => $wallId,
                    'WalletAccountId'   => $accId,
                    'PromotionalOfferID'=> $value->PromotionalOfferID,
                    'OfferBenefitID'    => $value->OfferBenefitID,
                    'ParentTransactionDetailID' => $value->TransactionDetailID,
                    'ParentPaymentsProductID'   => $value->PaymentsProductID,
                    'ParentTransactionDate'     => $value->TransactionDate,
                    'BenefitCategoryRefID'      => $value->BenefitCategoryRefID,
                    'OfferPromoCode'    => $value->PromoCode,
                    'EarnedAmount'      => $response['res_data']['benifitAmount'],
                    'EarnedAmountDate'  => $dateTime,
                    'EarnedAmountExpiryDate'    => $value->BenefitExpiryDate,
                    'EarnedProviderName'        => 'VIOLA',
                    'ExtendedValidityDate'      => $value->BenefitExpiryDate,
                    'UsedAmount'        => $defaltAmount,
                    'ExpiredAmount'     => $defaltAmount,
                    'ApplicableProviderName'    => '',
                    'ApplicablePaymentProductEventID'=> '1',
                    'CashbackTransactionDetailID'=> '',
                    'BenefitUsedTansactionDetailID'=> '',
                    'ActivateDate'      => $value->BenefitEffectiveDate,
                    'CreatedDatetime'   => $dateTime,
                    'CreatedBy'         => config('vwallet.userTypeKey'),
                    'UpdatedDateTime'   => $dateTime,
                    'UpdatedBy'         => config('vwallet.userTypeKey'),
                    'VoucherCode'       => '',
                );
                \App\Models\Userofferbenefittracker::insertGetId($insertData);
            }
            if ($batchJobMaster === NULL)
            {
                $BatchData = array(
                    'JobOwner'              => 'Wallet',
                    'JobName'               => $JobName,
                    'ExtractionStartDTTM'   => $StartDTTM,
                    'ExtractionEndDTTM'     => $EndDTTM,
                    'JobStartDTTM'          => $dateTime,
                    'JobEndDTTM'            => date('Y-m-d H:i:s'),
                    'JobStatus'             => 'Completed',
                    'SourceTableName'       => 'transactiondetail',
                    'TargetTableName'       => 'userofferbenefittracker',
                    'CreatedDateTime'       => $dateTime,
                    'CreatedBy'             => config('vwallet.userTypeKey')
                );
                \App\Models\Batchjobmaster::insertGetId($BatchData);
            }
            else
            {
                $updateData = array(
                    'ExtractionStartDTTM'  => $StartDTTM,
                    'ExtractionEndDTTM'  => $EndDTTM,
                    'JobStartDTTM'  => $startDateTime,
                    'JobEndDTTM'    => date('Y-m-d H:i:s'),
                    'UpdatedDateTime'=> date('Y-m-d H:i:s'),
                    'UpdatedBy'     => config('vwallet.userTypeKey')
                );
                \App\Models\Batchjobmaster::where('BatchJobMasterId', $batchJobMaster->BatchJobMasterId)->update($updateData);
            }
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        }
        return $output_arr;
    }
    
    /**
     * Insert data in offerbenifittrack table
     * @param Request $request
     * @return type
     */
    public static function addbenefitTrack($transactionId)
    {
        $dateTime = date('Y-m-d H:i:s');
        $defaltAmount = '0.00';
        
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array('info' => trans('messages.emptyRecords')), 'status_code' => 200, 'res_data' => array() );
        $transData = \App\Models\Transactiondetail::where('TransactionDetailID', $transactionId)->where('TransactionStatus', 'Success')->first();
        if ($transData === NULL)
        {
            return $output_arr;
        }
        $promoCode = $transData->PromoCode;
        $clauses = [ 'offerbenefitsbridge.ActiveInd' => 'Y', 'offerbenefits.ActiveInd' => 'Y', 'promotionaloffers.ActiveInd' => 'Y', 'promotionaloffers.PromoCode' => $promoCode ];
        $selectedColumns        = [
            'offerbenefitsbridge.ActiveInd AS BridgeStatus',
            'offerbenefitsbridge.OfferBusinessID',
            'offerbenefitsbridge.BenefitBusinessID',
            'promotionaloffers.OfferVersion',
            'promotionaloffers.OfferName',
            'promotionaloffers.OfferDescription',
            'promotionaloffers.OfferCategoryRefID',
            'promotionaloffers.OfferTypeRefID',
            'promotionaloffers.PromoCode',
            'promotionaloffers.OfferUserTypeRefID',
            'promotionaloffers.BusinessEffectiveDate',
            'promotionaloffers.BusinessExpiryDate',
            'promotionaloffers.ApplicableChannel',
            'promotionaloffers.TermsConditionsFilePath',
            'promotionaloffers.EligibilityDetailsFilePath',
            'promotionaloffers.ActiveInd as PromotionStatus',
            'promotionaloffers.PromotionalOfferID',
            'offerbenefits.BenefitVersion',
            'offerbenefits.BenefitName',
            'offerbenefits.BenefitDescription',
            'offerbenefits.BenefitEffectiveDate',
            'offerbenefits.BenefitExpiryDate',
            'offerbenefits.BenefitCategoryRefID',
            'offerbenefits.BenefitCriteriaRefID',
            'offerbenefits.AdminWalletAccountId',
            'offerbenefits.ActiveInd AS BenefitStatus',
            'offerbenefits.BenefitApplicableChannel',
            'offerbenefits.BenefitFixedAmount',
            'offerbenefits.BenefictPercentageValue',
            'offerbenefits.OfferBenefitID',
            'offerbenefits.CashBackDelayPeriod',
            'offerbenefits.CashBackExpiryPeriod',
        ];
        $dbResults = \App\Models\Offerbenefit::join('offerbenefitsbridge', 'offerbenefits.BenefitBusinessID', '=', 'offerbenefitsbridge.BenefitBusinessID')
                        ->rightjoin('promotionaloffers', 'promotionaloffers.OfferBusinessID', '=', 'offerbenefitsbridge.OfferBusinessID')
                        ->orderBy('offerbenefits.BenefitCategoryRefID', 'ASC')
                        ->where($clauses)
                        ->get($selectedColumns);
        if ($dbResults->count() <= 0)
        {
            return $output_arr;
        }
        foreach ($dbResults as $val => $value)
        {
            $wallId = $transData->UserWalletId;
            $accId = $transData->FromAccountId;
            if ($transData->TransactionMode === 'Credit') {
                $wallId = $transData->ToWalletId;
                $accId = $transData->ToAccountId;
            }

            $array = array(
                'promoCode' => $transData->PromoCode,
                'amount' => $transData->Amount,
                'isVerified' => TRUE
            );
            $response = \App\Libraries\OfferRules::getBenefitAmount($array, $value);
            $benifitAmount = $response['res_data']['benifitAmount'];
            $requestData = ['ReferenceType' => 'BenefitCategory'];
            $ruleNames = \App\Libraries\OfferRules::offerReferenceData($requestData);
            $ruleFlipData = array_flip($ruleNames);

            if (array_key_exists($value->BenefitCategoryRefID, $ruleNames)) {
                if ($ruleNames[$value->BenefitCategoryRefID] === 'Cashback' || $ruleNames[$value->BenefitCategoryRefID] === 'DelayedCashback') {
                    $activateDate = date('Y-m-d H:s:i', strtotime('+'.$value->CashBackDelayPeriod.' day'));
                    $EarnedAmountExpiryDate = date('Y-m-d H:s:i', strtotime('+'.$value->CashBackExpiryPeriod.' day'));
                    $insertData = array(
                        'UserWalletID' => $wallId,
                        'WalletAccountId' => $accId,
                        'PromotionalOfferID' => $value->PromotionalOfferID,
                        'OfferBenefitID' => $value->OfferBenefitID,
                        'ParentTransactionDetailID' => $transactionId,
                        'ParentPaymentsProductID' => $transData->PaymentsProductID,
                        'ParentTransactionDate' => $transData->TransactionDate,
                        'BenefitCategoryRefID' => $value->BenefitCategoryRefID,
                        'OfferPromoCode' => $transData->PromoCode,
                        'EarnedAmount' => $benifitAmount,
                        'EarnedAmountDate' => $dateTime,
                        'EarnedAmountExpiryDate' => $EarnedAmountExpiryDate,
                        'EarnedProviderName' => 'VIOLA',
                        'ExtendedValidityDate' => $EarnedAmountExpiryDate,
                        'UsedAmount' => $defaltAmount,
                        'ExpiredAmount' => $defaltAmount,
                        'ApplicableProviderName' => '',
                        'ApplicablePaymentProductEventID' => '1',
                        'CashbackTransactionDetailID' => '',
                        'BenefitUsedTansactionDetailID' => '',
                        'ActivateDate' => $activateDate,
                        'CreatedDatetime' => $dateTime,
                        'CreatedBy' => config('vwallet.userTypeKey'),
                        'UpdatedDateTime' => $dateTime,
                        'UpdatedBy' => config('vwallet.userTypeKey'),
                        'VoucherCode' => '',
                    );
                    \App\Models\Userofferbenefittracker::insertGetId($insertData);                   
                }               
                //insert to user offer stats table
                $pamentInfo = array( 'offerBusinessId' => $value->OfferBusinessID, 'promoCode' => $value->PromoCode, 'offerAmount' => $benifitAmount );
                \App\Libraries\TransctionHelper::userOfferStatsInsert($wallId, $pamentInfo, 'Used');
            }
        }
        $output_arr = array('is_error' => FALSE, 'display_msg' => array('info' => trans('messages.requestSuccessfull')), 'status_code' => 200, 'res_data' => array());
        return $output_arr;
    }
    
    /**
     * Insert data in offerbenifittrack table
     * @param Request $request
     * @return type
     */
    public static function addrefferbenefitTrack($request, $promoCodeData = '')
    {
        // return $PromoCodeData;
        $dateTime = date('Y-m-d H:i:s');
        $defaltAmount = '0.00';
        $partyId = $request->input('partyId');
        $transactionId = $request->input('transactionId');
        $PaymentsProductID = $request->input('PaymentsProductID');
        $transactionAmount = $request->input('transactionAmount');
        $userData = self::_getUserData($partyId);         
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array('info' => trans('messages.wrongUser')), 'status_code' => 200, 'res_data' => array() );
        if ($userData === NULL)
        {
            return (object) $output_arr;
        }
                
        if(!isset($promoCodeData['partyId'])) {
            $promoCodeData['partyId'] = $partyId;
        }
        
        $verifyPromoData = \App\Libraries\OfferRules::verifyPromoCode($promoCodeData);
       
        $PromoData = json_decode($verifyPromoData);        
        if ($PromoData->is_error === FALSE)
        {
            $wallId = $userData->UserWalletID;
            $accId = $userData->WalletAccountId;
            
            $benefitArray = array(                
                'amount' => $transactionAmount,
            );
            $response = \App\Libraries\OfferRules::getBenefitAmount($benefitArray, $PromoData->res_data->offerInfo);
            $activateDate = date('Y-m-d H:s:i', strtotime('+'.$PromoData->res_data->offerInfo->CashBackDelayPeriod.' day'));
            $EarnedAmountExpiryDate = date('Y-m-d H:s:i', strtotime('+'.$PromoData->res_data->offerInfo->CashBackExpiryPeriod.' day'));
            $insertData = array(
                'UserWalletID' => $wallId,
                'WalletAccountId' => $accId,
                'PromotionalOfferID' => $PromoData->res_data->offerInfo->PromotionalOfferID,
                'OfferBenefitID' => $PromoData->res_data->offerInfo->OfferBenefitID,
                'ParentTransactionDetailID' => $transactionId,
                'ParentPaymentsProductID' => $PaymentsProductID,
                'ParentTransactionDate' => '',
                'BenefitCategoryRefID' => $PromoData->res_data->offerInfo->BenefitCategoryRefID,
                'OfferPromoCode' => $PromoData->res_data->offerInfo->PromoCode,
                'EarnedAmount' => $response['res_data']['benifitAmount'],
                'EarnedAmountDate' => $dateTime,
                'EarnedAmountExpiryDate' => $EarnedAmountExpiryDate,
                'EarnedProviderName' => 'VIOLA',
                'ExtendedValidityDate' => $EarnedAmountExpiryDate,
                'UsedAmount' => $defaltAmount,
                'ExpiredAmount' => $defaltAmount,
                'ApplicableProviderName' => '',
                'ApplicablePaymentProductEventID' => '1',
                'CashbackTransactionDetailID' => '',
                'BenefitUsedTansactionDetailID' => '',
                'ActivateDate' => $activateDate,
                'CreatedDatetime' => $dateTime,
                'CreatedBy' => config('vwallet.userTypeKey'),
                'UpdatedDateTime' => $dateTime,
                'UpdatedBy' => config('vwallet.userTypeKey'),
                'VoucherCode' => '',
            );
            \App\Models\Userofferbenefittracker::insertGetId($insertData);
            $output_arr = array('is_error' => FALSE, 'display_msg' => array('info' => trans('messages.referralCodeVerified')), 'status_code' => 200, 'res_data' => array());
        }
        return (object) $output_arr;
    }
    
    private static function _getUserData($partyId)
    {
        $userWalletData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.Gender', 'userwallet.DateofBirth', 'userwallet.ProfileStatus', 'userwallet.IsVerificationCompleted', 'party.PartyType', 'partyaddress.State', 'walletaccount.WalletAccountId', 'walletaccount.CurrentAmount')
                ->join('party', 'party.PartyID', '=', 'userwallet.UserWalletID')
                ->join('partyaddress', 'partyaddress.PartyID', '=', 'userwallet.UserWalletID')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $partyId)->first();
        return $userWalletData;
    }
    
    public static function cashBackTrans($request)
    {
        \Log::debug('Cash Back Transactions Cron Started!');
        $date = date('Y-m-d');
        $startDateTime = date('Y-m-d 00:00:00');
        $DateTime = date('Y-m-d H:i:s');
        $JobName = 'addCashBack';
        $transIds = array();
        $faildTransIds = array();
        $batchJobMaster = \App\Models\Batchjobmaster::where('JobName', $JobName)->orderBy('BatchJobMasterId', 'desc')->first();
        if ($batchJobMaster)
        {
            $startDateTime = $batchJobMaster->ExtractionEndDTTM;
        }
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array('info' => trans('messages.emptyRecords')), 'status_code' => 200, 'res_data' => array() );
        $benefitTrackData = \App\Models\Userofferbenefittracker::whereBetween('ActivateDate', [$startDateTime, $DateTime])->where('CashbackTransactionDetailID', 0)->get();
        if ($benefitTrackData->count() > 0)
        {
            $totalCount = $benefitTrackData->count();
            $StartDTTM = $EndDTTM = $DateTime;
            foreach ($benefitTrackData as $val => $value)
            {
                
                if ($val == 0)
                {
                    $StartDTTM = $value->ActivateDate;
                }
                if ($val == $totalCount - 1)
                {
                    $EndDTTM = $value->ActivateDate;
                }
                $array = array(
                    'partyId'   => $value->UserWalletID,
                    'promoCode' => $value->OfferPromoCode,
                    'isVerified' => TRUE
                );
                $request->request->add(['partyId' => $value->UserWalletID]);
                $promoCodeData = \App\Libraries\OfferRules::verifyPromoCode($array); 
                $verifyPromoData = json_decode($promoCodeData);               
                if($verifyPromoData->is_error == TRUE) {  
                    $faildTransIds[$value->UserOfferBenefitTrackerID] = $value->ParentTransactionDetailID;
                    $faildTransIds['error_'.$value->UserOfferBenefitTrackerID] = $verifyPromoData->display_msg->info;
                    continue;
                }
                //success transactions
                $transIds[] = $value->ParentTransactionDetailID;
                //update with earned amount
                $verifyPromoData->res_data->benifitAmount = $value->EarnedAmount;
                $transactionResponse = \App\Libraries\TransctionHelper::cashBackTransaction($request, $verifyPromoData, $value);
            }
            
            $BatchData = array(
                'JobOwner' => 'Wallet',
                'JobName' => $JobName,
                'ExtractionStartDTTM' => $StartDTTM,
                'ExtractionEndDTTM' => $EndDTTM,
                'JobStartDTTM' => $DateTime,
                'JobEndDTTM' => date('Y-m-d H:i:s'),
                'JobStatus' => 'Completed',
                'SourceTableName' => 'userofferbenefittracker',
                'TargetTableName' => '',
                'CreatedDateTime' => $DateTime,
                'CreatedBy' => config('vwallet.userTypeKey')
            );
            \App\Models\Batchjobmaster::insertGetId($BatchData);
            
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array('info' => trans('messages.cashBackTransSuccess')), 'status_code' => 200, 'res_data' => array() );
        }
        \Log::debug('Cash Back Transactions Cron Finished!');
        $params['title'] = 'Cash Back Transactions ('.$JobName.')';
        $params['description'] = $output_arr['display_msg']['info'].' <br/> Parent Transactions : '. print_r($transIds, TRUE);
        \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
        if(!empty($faildTransIds)) {
            $params1 = array();
            $params1['title'] = 'Faild!! Cash Back Transactions ('.$JobName.')';
            $params1['description'] = 'Allert!! Faild Cash Back Transactions Details : <br/>'. print_r($faildTransIds, TRUE);
            \App\Libraries\TransctionHelper::sendCronMessage($params1, $request);
        }
        return $output_arr;
    }

    /**
     * Update used amount
     * @param Request $request
     * @return type
     */
    public static function updateUsedAmount($request)
    {
        $dateTime = date('Y-m-d H:i:s');
        $partyId  = isset($request['partyId']) ? $request['partyId'] : 0;
        $amount   = isset($request['amount']) ? $request['amount'] : 0;        
        $transactionDetailId   = isset($request['transactionDetailId']) ? $request['transactionDetailId'] : NULL;
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
 
        if($partyId == 0){
           return $output_arr; 
        }
        $partyInfo = \App\Libraries\TransctionHelper::_userValidate($partyId);
        
        $update_userWallet                   = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);
        $update_userWallet->NonWithdrawAmount    = $partyInfo->NonWithdrawAmount - $amount;
        $update_userWallet->save();
               
        $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount', 'UsedAmount')->where('UserWalletID', $partyId)->where('EarnedAmount', '>', '0.00')->where('CashbackTransactionDetailID', '>', '0')->where('ExtendedValidityDate', '>', $dateTime)->orderBy('UserOfferBenefitTrackerID', 'desc')->get();

        if ( ! $bnftTrackData )
        {
            return $output_arr;
        }
        foreach ( $bnftTrackData as $data )
        {
            if ( $amount > $data->EarnedAmount )
            {
                $amount     = $amount - $data->EarnedAmount;
                $updateData = array(
                    'EarnedAmount'    => '0.00',
                    'UsedAmount'      => $data->UsedAmount + $data->EarnedAmount,
                    'UpdatedDateTime' => $dateTime,
                    'BenefitUsedTansactionDetailID' => $transactionDetailId,
                    'UpdatedBy'       => config('vwallet.userTypeKey')
                );
                \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $data->UserOfferBenefitTrackerID)->update($updateData);
            }
            else
            {
                $earnAmount = $data->EarnedAmount - $amount;
                $usedAmount = $data->UsedAmount + $amount;
                $updateData = array(
                    'EarnedAmount'    => $earnAmount,
                    'UsedAmount'      => $usedAmount,
                    'UpdatedDateTime' => $dateTime,
                    'BenefitUsedTansactionDetailID' => $transactionDetailId,
                    'UpdatedBy'       => config('vwallet.userTypeKey')
                );
                \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $data->UserOfferBenefitTrackerID)->update($updateData);
                break;
            }
        }
        return $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.requestSuccessfull') ), 'status_code' => 200, 'res_data' => array() );
    }

    /**
     * Update expiry amount
     * @param Request $request
     * @return type
     */
    public static function updateExpiryAmount($request)
    {
        \Log::debug('Expiry Cash Cron Started!');
        $dateTime   = date('Y-m-d H:i:s');
        $JobName    = 'updateExpiryAmount';
        $transIds   = array();
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );

        $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount', 'CashbackTransactionDetailID', 'CreatedDatetime')->where('EarnedAmount', '>', '0.00')->where('ExtendedValidityDate', '<', $dateTime)->orderBy('ExtendedValidityDate', 'desc')->get();
// $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount', 'CashbackTransactionDetailID', 'CreatedDatetime')->where('EarnedAmount', '>', '0.00')->orderBy('ExtendedValidityDate', 'desc')->get();
        if ( $bnftTrackData->count() > 0 )
        {

            $transactionId = 0;
            $totalCount    = $bnftTrackData->count();
            $StartDTTM     = $EndDTTM       = $dateTime;
            foreach ( $bnftTrackData as $val => $data )
            {
                $transIds[] = $data->UserOfferBenefitTrackerID;
                if ( $val == 0 )
                {
                    $StartDTTM = $data->CreatedDatetime;
                }
                if ( $val == $totalCount - 1 )
                {
                    $EndDTTM = $data->CreatedDatetime;
                }
                $refundData        = \App\Libraries\PaymentRefund::paymentRefundCB($data->CashbackTransactionDetailID, $data->EarnedAmount);
                $paymentRefundData = json_decode($refundData);
                if ( $paymentRefundData->is_error === FALSE )
                {
                    $transactionId = $paymentRefundData->res_data->transactionId;
                }
                $updateData = array(
                    'EarnedAmount'                  => '0.00',
                    'ExpiredAmount'                 => $data->EarnedAmount,
                    'BenefitUsedTansactionDetailID' => $transactionId,
                    'UpdatedDateTime'               => $dateTime,
                    'UpdatedBy'                     => config('vwallet.userTypeKey')
                );
                \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $data->UserOfferBenefitTrackerID)->update($updateData);
            }
            $BatchData  = array(
                'JobOwner'            => 'Wallet',
                'JobName'             => $JobName,
                'ExtractionStartDTTM' => $StartDTTM,
                'ExtractionEndDTTM'   => $EndDTTM,
                'JobStartDTTM'        => $dateTime,
                'JobEndDTTM'          => date('Y-m-d H:i:s'),
                'JobStatus'           => 'Completed',
                'SourceTableName'     => 'userofferbenefittracker',
                'TargetTableName'     => 'userofferbenefittracker',
                'CreatedDateTime'     => $dateTime,
                'CreatedBy'           => config('vwallet.userTypeKey')
            );
            \App\Models\Batchjobmaster::insertGetId($BatchData);
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => 'Expiry Cash Back Success' ), 'status_code' => 200, 'res_data' => array() );
            \Log::debug('Expiry Cash Cron Finished!');
        }
        $params['title']       = 'Expiry Cash Back (' . $JobName . ')';
        $params['description'] = $output_arr['display_msg']['info'] . ' <br/> User Offer Benefit Tracker Ids : ' . print_r($transIds, TRUE);
        \App\Libraries\TransctionHelper::sendCronMessage($params, $request);
        return $output_arr;
    }

    public static function referralFirstTranscation($request)
    {
        $partyId = $request->input('partyId');
        $transactionId = $request->input('transactionId');
        $PaymentsProductID = $request->input('productId');
        $transactionAmount = $request->input('transactionAmount');
        $transDetail = \App\Models\Transactiondetail::where([['UserWalletId', $partyId], ['TransactionType', 'RECHARGE'], ['TransactionStatus', 'Success']])->get();
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ($transDetail->count() != 1)
        {
            $output_arr['display_msg']['info'] = 'Not applicable.';
            // return $output_arr;
        }
        $request->request->add([ 'transactionAmount' => $transactionAmount ]);
        $request->request->add([ 'transactionId' => $transactionId ]);
        $request->request->add([ 'PaymentsProductID' => $PaymentsProductID ]);
        
        $refresults      = \App\Models\Partyreferral::select('PartyID', 'ReferralCode', 'ReferralMediumCode', 'RefereeName', 'RefereeMobileNumber', 'RefereeEmailId')->where('PartyID', $partyId)->first();
        if($refresults && $refresults->ReferralCode!=NULL)
        {
            if ($refresults->ReferralCode === config('vwallet.walletReferral'))
            {
                $usrWallet      = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', $partyId)->where('SocialMediaRegistration', 'W')->first();
                if ($usrWallet !== NULL)
                {
                    $request->request->add([ 'partyId' => $partyId ]);
                    $requestArray          = \App\Libraries\TransctionHelper::violaSignupPromoCodeInfo($request);
                    \App\Libraries\OfferBussness::addrefferbenefitTrack($request, $requestArray);
                    $output_arr['is_error'] = FALSE;
                    $output_arr['display_msg']['info'] = 'success';
                }
            }
            else
            {
                $request->request->add([ 'usedReferralCode' => $refresults->ReferralCode ]);            
            
                //$referralCode = $request->input('usedReferralCode');
                $referralCashBackStats = \App\Libraries\OfferRules::referralCashBack($request);
                $referralInfo = json_decode($referralCashBackStats);
                if ( $referralInfo->is_error == FALSE )
                {
                    $output_arr['is_error'] = FALSE;
                    $output_arr['display_msg']['cashBackMsg'] = $referralInfo->display_msg->info;
                }
            }
            
        }
        return $output_arr;
    }
    
    /**
     * getBenefitAmount
     * @param json $dbRecords
     * @return type
     */
   /* public static function getBenefitAmount($dbRecords)
    {
        $promoCode       = $dbRecords->PromoCode;
        $amount          = $dbRecords->Amount;
        $getAmountStatus = TRUE;
        $output_arr      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $benefitCriteriaRefID       = $dbRecords->BenefitCriteriaRefID;
        $staticBenefitCriteriaArray = self::$staticBenefitCriteria;
        $benefitCriteriaCode        = NULL;
        if ( isset($staticBenefitCriteriaArray[$benefitCriteriaRefID]) )
        {
            $benefitCriteriaCode = $staticBenefitCriteriaArray[$benefitCriteriaRefID];
        }
        
        switch ( trim($benefitCriteriaCode) )
        {

            case 'Fixed':
                //BenefitFixedAmount
                $benifitAmount = $dbRecords->BenefitFixedAmount;
                break;
            case 'Percentage':
                //BenefictPercentageValue
                $benifitAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                break;
            case 'Least':
                //Least Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount < $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            case 'Highest':
                //Heighest Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / $amount) * 100;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount > $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            default:
                $benifitAmount = '0.00';
        }

        $benifitAmount          = number_format(( float ) $benifitAmount, 2, '.', '');
        $output_arr['res_data'] = array( 'benifitAmount' => $benifitAmount );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }
    
    public static function offerReferenceData($requestData = array())
    {
        if ( empty($requestData) )
        {
            $requestData = [];
            //$requestData = [ 'ActiveIndicator' => 'Y' ];
        }
        $dbResults  = \App\Models\Reference::select('ReferenceID', 'ReferenceCode', 'ReferenceDesc', 'ActiveIndicator')
                ->orderBy('ReferenceId')
                ->where($requestData)
                ->get();
        $returnData = [];
        if ( $dbResults->count() > 0 )
        {
            foreach ( $dbResults as $eachReference )
            {
                $returnData[$eachReference->ReferenceID] = $eachReference->ReferenceCode;
            }
        }
        return $returnData;
    }*/
}

?>
