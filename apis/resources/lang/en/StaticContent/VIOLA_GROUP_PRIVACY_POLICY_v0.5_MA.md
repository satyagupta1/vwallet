# PRIVACY POLICY

This Privacy Policy relates to the electronic products and services issuer and identity verification services provided by Viola Group, which is part of Viola Group.  Please read this document in conjunction with Viola Group - Terms of Use, Cookie and Refund Policy

## **Introduction**
This policy describes the information we collect and how we use them.  We recommend that you read our Privacy Policy if you are considering applying for any of our products or services so you fully understand that we take very seriously the privacy of the personal information you entrust us with.  Read this together with our Terms and Conditions of Use.  However, if there is any conflict between the two, the Terms and Conditions for the particular product or service will be followed.  We need to make you aware that we need to collect, use and disclose personal information to allow us to operate our business along with the services we provide.

When you provide us with your personal information, when you create your account, we shall assume that you have read this Privacy Policy in full and by doing so, you have given us your consent to collect, use, retain and disclose your personal information in accordance with the terms set out in this Privacy Policy.

**Personal Information**
For us to identify you, we will collect personal information, which means any information you provide we collect, from which you can be identified by and that can be attributed to you.  By completing the application form you have given us consent to collect, use, retain and disclose your personal information in accordance with the terms set out in this policy.  By providing information about yourself we will assume that you are providing your consent for us to collect, use, retain and disclose personal information as authorised by law.  Entering your personal details on the Viola Group application form will provide us with your consent electronically.  By completing and confirming your wish to register for an account by ticking Terms of Use, will imply your wish to proceed.  Considering these circumstances, we accept reasonable expectations that you are wishing to proceed, the sensitivity of the information provided and confirmation that your personal information will be treated with utmost care.  If we need to collect, use, retain or disclose any information for any purpose not set out in this Privacy Policy, we will provide you with the reasons and give you appropriate notification to enable you to refuse such action.

Viola Group will hold and transmit your personal information in a safe, confidential, and secure environment.  However, Viola Group will not be responsible for the privacy practices or the content of third party referrals, websites or web site links.

 **Notification of Changes**
We reserve the right to change or amend this Privacy Policy at any time and for whatever reason.  We will provide you with the notice on our website along with notice within our Viola Group site that our Privacy Policy has been amended.  Please check for the most current version of our Privacy Policy to ensure you are fully up to date.

Viola Group will not be held responsible for our registered merchants, for their services as they are governed by their own Terms and Conditions of Use and Privacy Policy.  Viola Group clients submitting information to or through these third-party websites should review the privacy statement of these sites prior to providing them with personal information.

 **Collection of Information**
For you to use our Viola Group Account, which holds electronic money, you must provide us with your name, address, phone number, date of birth, e-mail address, government issued identification, credit/debit card and/or bank account information.  We will also ask you to choose security questions to answer.  We require this information for us to process transactions, issue a new password if you forget or lose your password, to protect you online against identity theft, credit card and bank account fraud and other criminal acts online.  Should the need arise; the ability to contact you will be quick to ensure your Viola Group information remains secure and to administer your Viola Group Account.

**Transaction Information**

Should you decide to close your Viola Group Account or, if there is a need for us to close your Viola Group Account, to assist us in detecting any possible fraud all your transactions and information received by us through your Viola Group will need to be retained for security and fraud purposes.  For us to detect possible unauthorised transactions, we also need to collect your Internet Address (IP Address) of the device you are using to access your Viola Group Account.

 **Information Obtained from Third Parties**
For us to protect you from fraud, we use a third-party verification service to check your personal information that you have given to us.  We may also receive personal information about you from them. If you register your credit card or bank account within your Viola Group Account, we will use card authorisation and fraud screening services to verify your address and card information to ensure it matches the information you have supplied us and to ensure the card is not lost or stolen.

**Information about Using Our Website**
When you visit or leave the Viola Group website, we will automatically receive the web address of the site that you came from and/or are going to.  We will also retain information about where you visited within our website, the times you accessed our website along with the type of browser you use.  To ensure we understand, develop and improve our services to you and by collating information and managing our servers to work with speed and efficiency, will in turn enhance our service to everyone using Viola Group.

 **Our Use of &quot;Cookies&quot;**
For us to recognize you as a client of our Viola Group service, we will use &#39;Cookies&#39; within your computer if you use the same computer and browser.  We may send &#39;Session Cookies&#39; to your computer when you log into your Viola Group Account by using your email address and password.  We will then recognize you if you visit our site and look at multiple pages during your time on our website.  You don&#39;t need to re-enter your password multiple times whilst moving around the site.  However, once you log out these &#39;Session Cookies&#39; are no longer effective.

We need to make you aware that should you decide to turn off the &#39;Cookies&#39; by changing your browser settings, this may severely restrict your use of some services on our Viola Group site.

Please review the Viola Group Cookie Policy for further information.

 **Communication**
Any communication from you to the Viola Group site will be retained within your Viola Group Account.  We may also retain any correspondence, monitor and record your phone calls to and from us.  This will help us improve our customer service, our security against fraud and violations of our Terms of Use.

From time to time, we may send you marketing or promotional correspondence or ask you to complete questionnaires or surveys to understand Clients&#39; interests, needs, demographic information plus experience of using the Viola Group site so that we can improve and offer further services in the future. Within these surveys, we may collect personal information from you as our Client and in doing so we will provide you with notice of how this information will be collated and used.

By agreeing to our Viola Group Terms and Conditions, you will also be indicating your consent to receiving promotional emails; therefore, you can choose to opt-out of receiving information on questionnaires and/or surveys.  If you require opting-out, please go to &quot;Menu&quot; and select &quot;My Security Settings&quot;.

 **Information**
To use your personal information, we will obtain your prior consent.  The following details below show how this information may be obtained and used.

- Verifying your personal identity and financial information
- Analysing your personal information and transaction history to help us improve our services and provide you with special offers on products, features and promotions
- Helping us improve all our services including our website, and your eligibility to receive any special offers, promotions and features, we will use your personal information and transaction history to develop an understanding of you
- Conducting market analysis to assist us with strategic planning for the future improvements on our Viola Group site.
- Any correspondence from you, including any questions from live chat, emails, phone calls which may be recorded and used for training purposes, may be stored along with our response for future reference.

To reassure you, we will only give access to your personal information to our employees who require it to provide our services.  They will have completed their training and signed a declaration of confidentiality to not divulge any Clients&#39; details.

 **Third Party Disclosure Other Than Viola Group Employees**
Your personal information with third parties will not be shared except in the limited circumstances described below and only with your express permission or as permitted by law.  These third parties are limited by law, or by contract, from using the information for secondary purposes beyond the purposes for which the information is shared.  Viola Group will not sell or rent any of your personal information to third parties.

1. We may disclose your personal information to third parties to assist us in our investigations of fraud or other illegal activities and investigate any suspected violations of our Terms and Conditions if appropriate to do so.
2. In response to a subpoena, warrant, court order, levy, attachment, order of a court appointed receiver or other comparable legal process, including subpoenas from private parties in a civil action; we will cooperate with the legal entities and provide personal information about you.
3. Should you need to use an agent or legal representative (a guardian appointed by you or the holder of a power of attorney that you grant), we will provide them with information required
4. Information may be provided to Merchants to or from whom you transfer funds: name, account number, date of birth, jurisdiction, zip/postal code &amp; address, e-mail address, and/or IP address to allow us to identify and verify you.
5. For certain marketing purposes, we may disclose and transfer your information to a service provider in another country.  We will ensure that the service provider agrees to apply the same levels of protection as we are required to apply the information held here in the UK and to use your information only for providing the service to us.
6. In future, Viola Group could amalgamate or merge with or be acquired by another company.  If such an acquisition occurs, the successor company would have access to the personal information maintained by Viola Group, including customer account information, but would continue to be bound by this Privacy Policy unless and until it is amended, and you would be advised accordingly at that time.
7. To help us enforce our Viola Group Terms and Conditions, we may share your information with our parent company, subsidiaries and any joint ventures to help us provide our services to you.

 **Client Communication**
When you create your account with Viola Group, we will use your phone number and e-mail address to confirm that you have opened a Viola Group account.  For you to send or receive payments, we need to verify your personal information and are required by law to send information to you about important changes to our products and services along with notices and any disclosures.  Clients cannot opt out of these communications, but they will be primarily informational in nature rather than promotional.  We communicate by phone to resolve customer complaints or investigate suspicious transactions.  We will also communicate with users on a regular basis via the Viola Group App, e-mail and live chat to provide requested services.

Upon opening a Viola Group account, every user can choose to receive SMS or email confirming their successful registration.

**Internet Address Information**
We will analyse the type of browser you use, the times you access the site, and using your IP address this will help us administer, prevent fraud, gather information about the demographic area in which you live and help us improve our service to you.

 **Information Security and Protection**
Here at Viola Group we are committed to handling your personal information with high standards of information security.  Our employees receive training and only have access to your personal information on a need to know basis to ensure they provide excellent customer service and fully understand the products available to you.  Those that have access to your information are made aware of how to keep it confidential and must sign an agreement stating that confidentiality is a condition of employment with Viola Group to ensure that we are protected against unauthorised access, error or loss.  To comply with the strict procedures which we have put in place to safeguard your personal information, we maintain physical and electronic processes applicable to the regulations to protect your non-public personal information.  We do not store your card and bank account information on computers that are connected to the Internet.  Our security systems and processes are stress tested regularly, along with any third-party organisation that we are partnered with.

You must also protect your Viola Group Account and maintain your security by not sharing with anyone your password, PIN code or answers to your security questions.  We will not ask you to email your security details to us.  We may ask you to enter your personal details or other information onto the Viola Group website, the URL will begin with http://www.violacard.com.  Should you be requested to forward any information whether it is by email or telephone or a link that does not begin with the above website address please treat it as suspicious and unauthorised.  Report this to us here at Viola Group immediately.

Should you have shared your security details with a third party for whatever reason including if the third party has promised to provide you with additional services, the third party will then have access to your account, your personal information and may therefore be responsible for the movement of funds and any actions in altering your account details.  Therefore, if someone other than you has obtained access to your account along with any security details or your identification, please block your card within the App immediately then log into your account via the App or website http://www.violacard.com and change your password within the &quot;Menu&quot; settings, and contact us directly using contact@viola.group

Any information you have provided us with may be used and accessed in countries outside of the UK. By you supplying your personal information, you are therefore agreeing to this transfer, storing or processing.  To ensure that your information is treated securely, and in accordance with this Privacy Policy, we will take all the steps necessary to protect your personal information.

 **Updating Your Information**
Should you wish to review, amend or change any of your personal information you have provided go to the settings for your Viola Group account.  You can do this at any time by logging into your Viola Group App account or log on through the http://www.violacard.comwebsite and changing your preferences in the &quot;Menu&quot; section of your account.

You also have the right to access any additional personal information that we hold about you or to obtain a copy of it, where it is not stored on your Viola Group account.  Any access request may be subject to a fee towards our costs in providing you with details of the personal information that we hold about you.  We may ask you to verify your identity and check for more information about your request.  To obtain a copy of the personal information that we hold about you, please write to Viola Group Limited, 3 Waterton Park, Bridgend, CF31 3PH.

 **Closing Your Account**
Should you decide to close your Viola Group account, you can do this at any time, however we will mark it in our database as &quot;Closed,&quot; but your Viola Group account information will remain on our database.  This is a necessity to deter fraud, by ensuring that people who try to commit fraud will not be able to avoid detection simply by closing their Viola Group account and opening a new Viola Group account.  However, if you do decide to close your Viola Group account, your personal information will not be used by us for any further purposes, nor sold or shared with third parties, except as necessary to prevent fraud and assist law enforcement, or as required by law.

We can provide information about what we hold about you and where it is stored, who we have shared this with, and for what purposes we used it for.

 **Contacting Us**
If you have any questions about this privacy statement, our information policies or practices, or any other queries you have with Viola Group please use contact@viola.group

Viola Group Privacy Policy was last amended on 18/08/2017.