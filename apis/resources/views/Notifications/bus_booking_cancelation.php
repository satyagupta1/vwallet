<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * BusTicket Booking Cancelation Fees
 * 
 *  param $ticketNumber varchar
 *  param $placeOne varchar
 *  param $placeTwo varchar 
 *  param $customerName varchar 
 *  param $operatorName varchar 
 *  param $amount decimal
 *  param $cancellationFeeAmount decimal
 *  param $leviedAmount decimal
 *  param $vwalletEmail varchar
 */

// get salutation from language file
$salutation = trans('messages.salutation');
$array = [
    'email' => [
        'subject' => 'Seat Cancellation',
        'text'    => $salutation . ',<br/><br/>
    Your seats on the the travels , '.$seatNumbers.' have been cancelled. Please find details below: <br/>
    Original Fare: '.$orginalAmount.'
    Cancellation Charges: '.$cancellationCharges.'
    Discount: '.$refundAmt],
    'sms'   => [
        'text' => 'Your seat numbers '.$seatNumbers.' have been cancelled on '.$operator.', '.$servicenumber.'. Your refund amount is '.$refundAmt.'. Team Viola'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your seat numbers '.$seatNumbers.' have been cancelled on '.$operator.', '.$servicenumber.'. Your refund amount is '.$refundAmt.'. Team Viola',
        'summaryText'        => 'Seat Cancellation',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'busbooking'
    ],
    'web'   => [
        'text' => 'Your seat numbers '.$seatNumbers.' have been cancelled on '.$operator.', '.$servicenumber.'. Your refund amount is '.$refundAmt.'. Team Viola'
    ],
];

echo json_encode($array);

/* End of file bus_booking_cancelation_fees */
