<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Topup success with Transaction Details device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $cardnumber varchar
 *  param $date varchar
 *  param $time varchar
 *  param $totalamount varchar
 *  param $paymentMode varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$payMode = ($paymentMode) ? ' from your ' . $paymentMode : '';
$cardNum = ($cardnumber) ? ' number ' . $cardnumber : '';
$array = [
    'email' => [
        'subject' => 'Money Credited into your ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
            Money Loaded!!<br/>
                        Top Up of Rs '.$amount.' has been credited onto your ViolaWallet'.$payMode . $cardNum.'.<br/> Current Balance: '.$totalamount.'.'],
    'sms'   => [
        'text' => $salutationName.' Top up of INR '.$amount.' successful. Transaction Details Total Amount:'.$amount.' Time:'.$time.' Date:'.$date.'',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Rs. '.$amount.' Topped Up in your ViolaWallet',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Rs '.$amount.' Topped Up into your ViolaWallet',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
