<?php

  namespace App\Libraries\Communication\Emails;

  use App\Libraries\Communication\Emails\SwiftMail\swiftSendMail;
  use App\Libraries\Communication\Emails\Mailgun\MailgunSendmail;


/**
 * Created by PhpStorm.
 * User: mahendravikhar
 * Date: 11/02/17
 * Time: 11:53 PM
 */
class emailNow
{

    public static function sendEmail($email_data){
        $email_status = swiftSendMail::sendSmtpMailNow($email_data);
        return $email_status;

        /*$email_status = MailgunSendmail::sendSmtpMailNow($email_data);
        return $email_status;*/

        /*$CI->load->library('emails/sendin_blue/sendin_sendmail');
        $email_status = $CI->sendin_sendmail->ssmtp_mail($email_data);
        return $email_status;*/
    }
}
