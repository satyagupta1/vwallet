<?php
namespace App\Libraries\Crypt;

use App\Libraries\Crypt\Cryptor;
use \stdClass;

//require_once __DIR__ . '/Cryptor.php';

/**
 * RNDecryptor for PHP
 *
 * Decrypt data interchangeably with Rob Napier's Objective-C implementation
 * of RNCryptor
 */
class Decryptor extends Cryptor
{
    static protected $components;
    /**
     * Decrypt RNCryptor-encrypted data
     *
     * @param string $base64EncryptedData Encrypted, Base64-encoded text
     * @param string $password Password the text was encoded with
     * @throws Exception If the detected version is unsupported
     * @return string|false Decrypted string, or false if decryption failed
     */
    public static function decrypt($encryptedBase64Data, $password)
    {
        $components = Decryptor::unpackEncryptedBase64Data($encryptedBase64Data);

        if (is_string($components))
        {
            return 'error˜'.$components;
        }

        if ( !Decryptor::hmacIsValid($components, $password)) {
            return false;
        }

        $key = Decryptor::makeKey($components->headers->encSalt, $password);
        if (Cryptor::$config->mode == 'ctr') {
            return Cryptor::aesCtrLittleEndianCrypt($components->ciphertext, $key, $components->headers->iv);
        }

        $iv = (string)$components->headers->iv;
        $method = Cryptor::$config->algorithm . 'cbc';

        return openssl_decrypt($components->ciphertext, $method, $key, OPENSSL_RAW_DATA, (string)$iv);
    }

    private static function unpackEncryptedBase64Data($encryptedBase64Data, $isPasswordBased = true)
    {
        $binaryData = base64_decode($encryptedBase64Data);

        $components = new stdClass;
        $components->headers = Decryptor::parseHeaders($binaryData, $isPasswordBased);

        if (is_string($components->headers))
        {
            return $components->headers;
        }

        $components->hmac = substr($binaryData, -self::$config->hmac->length);

        $offset = $components->headers->length;
        $length = strlen($binaryData) - $offset - strlen($components->hmac);

        $components->ciphertext = substr($binaryData, $offset, $length);

        return $components;
    }

    private static function parseHeaders($binData, $isPasswordBased = true)
    {
        $offset = 0;

        $versionChr = $binData[0];
        $offset += strlen($versionChr);

        $configure_reutrn = Cryptor::configure(ord($versionChr));

        if (is_string($configure_reutrn))
        {
            return $configure_reutrn;
        }

        $optionsChr = $binData[1];
        $offset += strlen($optionsChr);

        $encSalt = null;
        $hmacSalt = null;
        if ($isPasswordBased) {
            $encSalt = substr($binData, $offset, self::$config->saltLength);
            $offset += strlen($encSalt);

            $hmacSalt = substr($binData, $offset, self::$config->saltLength);
            $offset += strlen($hmacSalt);
        }

        $iv = substr($binData, $offset, self::$config->ivLength);
        $offset += strlen($iv);

        $headers = (object)[
            'version' => $versionChr,
            'options' => $optionsChr,
            'encSalt' => $encSalt,
            'hmacSalt' => $hmacSalt,
            'iv' => $iv,
            'length' => $offset
        ];

        return $headers;
    }

    private static function hmacIsValid($components, $password)
    {
        $hmacKey = Cryptor::makeKey($components->headers->hmacSalt, $password);

        return hash_equals($components->hmac, Cryptor::makeHmac($components, $hmacKey));
    }
}
