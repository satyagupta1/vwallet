<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Promotion
 * 
 * @property int $PromotionID
 * @property string $PromoCode
 * @property string $PromoType
 * @property float $AmountValue
 * @property string $OneTimeUsageIndicator
 * @property \Carbon\Carbon $EffectiveDate
 * @property \Carbon\Carbon $ExpiryDate
 * @property string $ActiveIndicator
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $transactionpromotions
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 *
 * @package App\Models
 */
class Promotion extends Eloquent
{
	protected $primaryKey = 'PromoCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PromotionID' => 'int',
		'AmountValue' => 'float'
	];

	protected $dates = [
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PromotionID',
		'PromoType',
		'AmountValue',
		'OneTimeUsageIndicator',
		'EffectiveDate',
		'ExpiryDate',
		'ActiveIndicator',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transactionpromotions()
	{
		return $this->hasMany(\App\Models\Transactionpromotion::class, 'PromoCode');
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class, 'PromoCode');
	}
}
