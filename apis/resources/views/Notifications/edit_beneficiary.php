<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * add beneficiary
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */
$msg = '';
if ( $IdentifierType == 'mobile' )
{
    $msg = 'Mobile Number: ' . $identifier1;
}
else if ( $IdentifierType == 'bank' )
{
    $accShortForm = $msg          = 'Account Number: ' . substr($identifier1, -4);
}
else if ( $IdentifierType == 'VPA' )
{
    $msg = 'VPA: ' . $identifier1;
}

$salutation     = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array          = [
    'email' => [
        'subject' => 'Edit Beneficiary Successful',
        'text'    => $salutationName . ',<br/><br/>
                    Sorry, your request for editing ' . $msg . ' as beneficiary is Successful.<br/>
                    Please check and try again later.<br/>'
    ],
    'sms'   => [
        'text' => 'Sorry,your request for editing ' . $msg . ' as beneficiary is Successful.'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Edit Beneficiary Successful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'genral'
    ],
    'web'   => [
        'text' => 'Sorry, your request for editing ' . $msg . ' as beneficiary is Successful.',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
