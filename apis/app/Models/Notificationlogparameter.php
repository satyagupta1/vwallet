<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Notificationlogparameter
 * 
 * @property int $NotificationID
 * @property string $TemplateParameter
 * @property string $TemplateValue
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Notificationlog $notificationlog
 *
 * @package App\Models
 */
class Notificationlogparameter extends Eloquent
{
	protected $primaryKey = 'NotificationID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'NotificationID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TemplateParameter',
		'TemplateValue',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function notificationlog()
	{
		return $this->belongsTo(\App\Models\Notificationlog::class, 'NotificationID');
	}
}
