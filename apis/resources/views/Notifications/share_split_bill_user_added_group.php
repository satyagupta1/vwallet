<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * When user added to a group template
 *  param $fullname varchar
 *  param $memberName varchar 
 */

$split = "We have added you to ViolaWallet's “Split & Share bill”";
$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Added to Share & Spilit of ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
Welcome ' . $memberName . ' !
 ' . $split . ' . 
This helps you to transfer money, track expenses and split and share your spends.
Enjoy easy and hassle free payment split.'],
    'sms'   => [
        'text' => '
Hello  ' . $fullname . ',
Welcome ' . $memberName . ' ! ' . $split,
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file share_split_bill_user_added_group.php */
