<?php

  namespace App\Libraries\Emails\Mailgun;

/**
 * Created by PhpStorm.
 * User: mahendravikhar
 * Date: 20/02/17
 * Time: 3:40 PM
 */

// namespace Mailgun;

class MailgunSendmail
{

    /**
     * @var null|string
     */
    static protected $apiKey = 'key-42a88a54bfd310ea999ffcd9516969d4';
    // protected $apiKey = 'key-eda08366727d41eeb5289030c85d3261';

    /**
     * @var null|string
     */
    static protected $domain = 'sandbox9c9e9d28377f4c2c99370cec364f9f84.mailgun.org';
    // protected $domain = array('V-Business' => 'hemophilia.ml', 'V-Wallet' => 'indorehemophilia.org.in');


    /**
     *  This function allows the sending of a fully formed message OR a custom
     *  MIME string. If sending MIME, the string must be passed in to the 3rd
     *  position of the function call.
     *
     * @param array $final_data
     *
     * @return string;
     */



    public static function sendSmtpMailNow($final_data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:'.MailgunSendmail::$apiKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/'.MailgunSendmail::_get_domain($final_data['site_name']).'/messages');
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            array('from' => MailgunSendmail::_get_from_id($final_data['site_name']),
                'to' => $final_data['to_name'].' <'.$final_data['to_email'].'>',
                // 'cc' => 'himanshu.mandloi@vwallet.in, navin.khapre@vwallet.in' ,
                // 'bcc' => 'Dharmendra.baghel@vwallet.in',
                'subject' => $final_data['subject'],
                'html' => $final_data['mail_body'],
                //'o:testmode' => TRUE,
                'attachment' => '/Users/mahendravikhar/dumps/Dump20170220.sql',
            ));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private static function _get_from_id($site_name){

        // select domain for which we need to create from email id.
        $current_domain = MailgunSendmail::_get_domain($site_name);

        return 'NoReply '.$site_name. '<no-reply@'.$current_domain.'>';
    }

    private static function _get_domain($site_name){
        if (is_array(MailgunSendmail::$domain))
        {
            $my_domain = MailgunSendmail::$domain{$site_name};
        }
        else
        {
            $my_domain = MailgunSendmail::$domain;
        }
        return $my_domain;
    }


}