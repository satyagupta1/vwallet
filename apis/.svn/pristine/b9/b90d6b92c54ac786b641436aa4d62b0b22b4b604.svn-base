<?php

namespace App\Libraries;

class GeoAddress {
    /*
     * Given longitude and latitude return the address using The Google Geocoding API V3
     *
     */

    public static function getAddressFromGoogleMaps($lat, $lon)
    {

        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false";

// Make the HTTP request
        $data     = @file_get_contents($url);
// Parse the json response
        $jsondata = json_decode($data, true);

// If the json data is invalid, return empty array
        $address = array();
        if ( self::checkStatus($jsondata) )
        {
            $address = array(
                'country'           => self::googleGetCountry($jsondata),
                'province'          => self::googleGetProvince($jsondata),
                'city'              => self::googleGetCity($jsondata),
                'street'            => self::googleGetStreet($jsondata),
                'postalCode'       => self::googleGetPostalCode($jsondata),
                'countryCode'      => self::googleGetCountryCode($jsondata),
                'formattedAddress' => self::googleGetAddress($jsondata),
            );
        }
        return $address;
    }

    /*
     * Check if the json data from Google Geo is valid 
     */

    public static function checkStatus($jsondata)
    {
        if ( $jsondata["status"] == "OK" )
            return TRUE;
        return FALSE;
    }

    /*
     * Given Google Geocode json, return the value in the specified element of the array
     */

    public static function googleGetCountry($jsondata)
    {
        return self::getAddressName("country", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetProvince($jsondata)
    {
        return self::getAddressName("administrative_area_level_1", $jsondata["results"][0]["address_components"], true);
    }

    public static function googleGetCity($jsondata)
    {
        return self::getAddressName("locality", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetStreet($jsondata)
    {
        return self::getAddressName("street_number", $jsondata["results"][0]["address_components"]) . ' ' . self::getAddressName("route", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetPostalCode($jsondata)
    {
        return self::getAddressName("postal_code", $jsondata["results"][0]["address_components"]);
    }

    public static function googleGetCountryCode($jsondata)
    {
        return self::getAddressName("country", $jsondata["results"][0]["address_components"], true);
    }

    public static function googleGetAddress($jsondata)
    {
        return $jsondata["results"][0]["formatted_address"];
    }

    /*
     * Searching in Google Geo json, return the long name given the type. 
     * (If short_name is true, return short name)
     */

    public static function getAddressName($type, $array, $short_name = false)
    {
        foreach ( $array as $value )
        {
            if ( in_array($type, $value["types"]) )
            {
                if ( $short_name )
                    return $value["short_name"];
                return $value["long_name"];
            }
        }
    }

}

?>
