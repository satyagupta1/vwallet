<?php
namespace App\Libraries;

class XSSProtection {
    /**
     * The following method loops through all request input and strips out all tags from
     * the request. This to ensure that users are unable to set ANY HTML within the form
     * submissions, but also cleans up input.
     *
     * @param Request $request
     * @param callable $next
     * @return mixed
     */
    public static function xssCleanUp( $request )
    {       
       
        if (!in_array(strtolower($request->method()), ['get', 'post'])) {
            return FALSE;
        }        
        $input = $request->input();
        array_walk_recursive($input, function(&$input) {
            $input = strip_tags($input);
        });
        $request->merge($input);
        return $request;
    }
}