<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Contacttype
 * 
 * @property int $ContactTypeID
 * @property string $ContactDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partycontacts
 *
 * @package App\Models
 */
class Contacttype extends Eloquent
{
	protected $table = 'contacttype';
	protected $primaryKey = 'ContactTypeID';
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ContactDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partycontacts()
	{
		return $this->hasMany(\App\Models\Partycontact::class, 'ContactTypeId');
	}
}
