# COOKIE POLICY

### **Your Privacy**

You can visit our website without telling us who you are or revealing any information about yourself. However, if you give us any personal information about yourself or others, we will treat it securely, fairly and lawfully.  We are committed to protect your privacy.

When we ask you for personal information online it will only be in response to you actively applying for or using one of our online products or services.  If you are giving us information for the first time we shall explain the purposes for which we shall use it at that time.  If you are already a customer, this will be in your Terms of Use

Please read this document in conjunction with Viola Group - Terms of Use, Privacy Policy and Refund Policyfor more information.

### **What are Cookies?**

Cookies are small text files stored on your computer, tablet or phone when you visit a website.  Some cookies are deleted when you close your browser.  These are known as **session cookies.**  Others remain on your device until they expire or you delete them from your cache.  These are known as **persistent cookies** and enable us to view streamed history as a returning visitor.

To find out more about cookies - to see how it has been set and how to manage and delete them, visit www.allaboutcookies.org.  Alternatively, you can search the internet for other independent information on cookies.

### **How to use Cookies?**

If you delete cookies relating to this website, your search history will be removed including your cookie preferences.

Some features within Viola Group depend on &#39;Persistent Cookies&#39;.  For instance, your account Quick View relies on cookies to enable it to display your account balances prior to your entering your PIN code.  This feature enables you to quickly check your account balance without the need to fully log into your account.  Your account Quick View is only enabled after you have fully logged in for the first time from a cookie-enabled device.  If you log in from a new device the information such as your account balance will not be displayed.

Additionally, we may use cookies and other similar technologies to:

- Provide a secure online environment
- Manage marketing relationships
- Give you a better online experience
- Track website performance
- Help us make our website more relevant to you
- To identify you when returning from the same device
- Provide products and services that you request

### **Importance of Cookies**

Without cookies, we are unable to provide some products or services that you might request.  Other &#39;essential&#39; cookies keep our website secure.

Essential cookies are used to:

- Deliver interactive services, such as:
  - Product Applications
  - Online Access
  - Web Chat
  - Call-Backs
- Maintain online security and protect against online fraud
- Record your preference regarding our use of cookies on your device

### **Marketing Relationship Management**

Our marketing partners may set cookies before you reach our site.  If you wish to prevent this type of cookie (sometimes known as a &#39;third party&#39; or &#39;analytical&#39; cookie), you may do so through your device&#39;s browser security settings.

You may sometimes see our advertisements or special offers on other websites.  Cookies provided by the other website or advertising network can tell us how effective this is.  For example, if you click on one of our advertisements, the website owner might use a cookie to tell us that you came from their site.

### **Online Experience and Tracking Website Performance**

You will still be able to use most of our online services, but some things might not work as you may expect if you do not allow us to store and access these cookies on your device.

These cookies make our online services easier to use and help us to understand how people use our website.  For example, amongst other things, these cookies:

- Make online login faster
- Simplify online transactions
- Display of mini-statements as you like them
- Remember relevant information as you browse from page to page to save you from re-entering the same information repeatedly

We also use this type of cookie to understand how visitors use our online services and look for ways to improve them.  A cookie might tell us that lots of people give up on an application process at a certain point – so we can try to make that section much easier to use.

### **Website Relevance**

Enabling us to customise your online experience based upon your individual circumstances, existing product holdings or activities during previous visits to this website.  If you select &quot;No&quot;, we won&#39;t be able to tailor your experience.

These cookies are used to ensure that we present you with product promotions and offers that are relevant to you.  Some features that are designed to give you a better online experience only work if we use cookies.

### **Warning**

By disabling location services on your mobile device, you may be limited to use some services that you may rely on as part of your Viola Group application.

Viola Group Cookie Policy was last updated 18/08/2017