<?php

namespace App\Libraries;

class Statistics {

    /**
     * getUserTransactionStats
     * @param type $partyId
     * @param type $amount
     */
    public static function getUserTransactionStats($partyId)
    {
        $userInfo           = \App\Models\Walletaccount::join('userwallet', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')->select('walletaccount.CurrentAmount', 'walletaccount.MaximumBalanceLimit','userwallet.WalletPlanTypeId')->where('userwallet.UserWalletId', $partyId)->first();
        
        // user wallet subscription status
        //$userWalletId       = \App\Libraries\TransctionHelper::checkUserwalletTypeState($userInfo->WalletPlanTypeId);
        //$walletMonthlyLimit = \App\Libraries\TransctionHelper::walletPlanConfigration($userWalletId->WalletPlanTypeId);

        $monthUtilityTotal = self::getUserMonthlyDebitTransactionsTotal($partyId);
        $monthCrditTotal   = self::getWalletYearlyBalanceTotal($partyId);
        //$monthUtilityCount = self::getUserMonthlyDebitTransactionsCount($partyId);
        //$monthCrditTotal = self::getUserMonthlyCreditTransactionsTotal($partyId);                
        //$monthCrditCount = self::getUserMonthlyCreditTransactionsCount($partyId);

        if ( $monthUtilityTotal == FALSE )
        {
            $monthUtilityTotal['Value'] = '0.00';
        }
        if ( $monthCrditTotal == FALSE )
        {
            $monthCrditTotal['Value'] = $userInfo->CurrentAmount;
        }

        //$criditLimit = $walletMonthlyLimit->ConfValue - $monthCrditTotal['Value'];
        $criditLimit = $userInfo->MaximumBalanceLimit - $monthCrditTotal['Value'];
        if ( $criditLimit < 0 )
        {
            $criditLimit = 0.00;
        }

        $userStst = array(
            //'monthUtilityCount' => ($monthUtilityCount)?$monthUtilityCount['Value']:0,
            //'monthCrditCount' => ($monthCrditCount)?$monthCrditCount['Value']:0,
            'monthUtilityAmount' => number_format($monthUtilityTotal['Value'], 2, '.', ''),
            'monthCrditLimitAmount'   => number_format($criditLimit, 2, '.', '')
        );

        return $userStst;
    }

    /* public static function getUserTransactionStats($partyId)
    {
        $userTransactionStats = \App\Models\Usertransactionstat::select('DayTransactionCount', 'DayAmount')
                        ->where('UserWalletID', $partyId)
                        ->where('TransactionDate', date('Y-m-d'))
                        ->orderBy('DayTransactionCount', 'desc')->first();
        $monthTransTotal      = \App\Models\Usertransactionstat::where('UserWalletID', $partyId)
                        ->whereMonth('TransactionDate', '=', date('m'))->whereYear('TransactionDate', '=', date('Y'))->sum('DayAmount');

        $monthTransCount = \App\Models\Usertransactionstat::where('UserWalletID', $partyId)
                        ->whereMonth('TransactionDate', '=', date('m'))->whereYear('TransactionDate', '=', date('Y'))->sum('DayTransactionCount');

        $userStst = array(
            'dayTransctionCount'   => ($userTransactionStats) ? $userTransactionStats->DayTransactionCount : 0,
            'dayTransAmount'       => ($userTransactionStats) ? $userTransactionStats->DayAmount : 0,
            'monthTransctionCount' => $monthTransCount,
            'monthTransAmount'     => $monthTransTotal
        );

        return $userStst;
    }*/
    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function monthlyCreditTransactionsUpdate($partyId, $amount, $refund = FALSE)
    {  
        $tagKey    = 'CreditTransaction';
        $periodKey = date('Y-m');
         //Aggregate with forward wallet balance         
         \App\Libraries\Statistics::walletMonthBalanceUpdate($partyId, $amount);
        return self::userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund);
    }

    /**
     * user yearly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function yearlyCreditTransactionsUpdate($partyId, $amount, $refund = FALSE)
    {  
        $tagKey    = 'CreditTransaction';
        $periodKey = date('Y');
         //Aggregate with forward wallet balance
         \App\Libraries\Statistics::walletYearlyBalanceUpdate($partyId, $amount);
        return self::userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund);
    }
    
    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function monthlyDebitTransactionsUpdate($partyId, $amount, $refund = FALSE)
    {
        $tagKey    = 'DebitTransaction';
        $periodKey = date('Y-m');
        //Aggregate with forward wallet balance for first transaction 
        $monthCrditTotal   = self::getWalletMonthBalanceTotal($partyId);
        if ($monthCrditTotal == FALSE )
        {
            \App\Libraries\Statistics::walletMonthBalanceUpdate($partyId, $amount);
        }
        return self::userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund);
    }

    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getUserMonthlyCreditTransactionsTotal($partyId)
    {
        $periodKey                     = date('Y-m');
        $tagNameAmount                 = 'CreditTransactionAmount';
        return $userTransactionStatsAggregate = self::_getAggregates($partyId, $tagNameAmount, $periodKey);
    }
    
    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getWalletMonthBalanceTotal($partyId)
    {
        $periodKey                     = date('Y-m');
        $tagNameAmount                 = 'walletMonthTotalAmount';
        return $userTransactionStatsAggregate = self::_getAggregates($partyId, $tagNameAmount, $periodKey);
    }
    
    /**
     * user yearly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getWalletYearlyBalanceTotal($partyId)
    {
        $periodKey                     = date('Y');
        $tagNameAmount                 = 'walletYearlyTotalAmount';
        return $userTransactionStatsAggregate = self::_getAggregates($partyId, $tagNameAmount, $periodKey);
    }
    /**
     * user yearly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function walletYearlyBalanceUpdate($partyId, $amount)
    {
        $tagKey    = 'walletYearlyTotal';
        $periodKey = date('Y');
        return self::userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund = FALSE);
    }
    
    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function walletMonthBalanceUpdate($partyId, $amount)
    {
        $tagKey    = 'walletMonthTotal';
        $periodKey = date('Y-m');
        return self::userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund = FALSE);
    }

    /**
     * user monthly credit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getUserMonthlyCreditTransactionsCount($partyId)
    {
        $periodKey                     = date('Y-m');
        $tagNameCount                  = 'CreditTransactionCount';
        return $userTransactionCountAggregate = self::_getAggregates($partyId, $tagNameCount, $periodKey);
    }

    /**
     * user monthly debit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getUserMonthlyDebitTransactionsTotal($partyId)
    {
        $periodKey                     = date('Y-m');
        $tagNameAmount                 = 'DebitTransactionAmount';
        return $userTransactionStatsAggregate = self::_getAggregates($partyId, $tagNameAmount, $periodKey);
    }

    /**
     * user monthly debit transactions aggregate
     * @param type $partyId
     * @param type $amount
     */
    public static function getUserMonthlyDebitTransactionsCount($partyId)
    {
        $periodKey                     = date('Y-m');
        $tagNameCount                  = 'DebitTransactionCount';
        return $userTransactionCountAggregate = self::_getAggregates($partyId, $tagNameCount, $periodKey);
    }

    /**
     * Common function for all aggregates 
     * @param string $partyId
     * @param int $amount
     * @param type $tagKey as CreditTransaction
     * @param type $period like month as date('Y-m'), Year as date('Y')
     * @return boolean
     */
    private static function userTransactionAgrregates($partyId, $amount, $tagKey, $periodKey, $refund)
    {
        $tagKeyAmount                  = $tagKey . 'Amount';
        $tagKeyCount                   = $tagKey . 'Count';
        $userTransactionAmountAggregate = self::_getAggregates($partyId, $tagKeyAmount, $periodKey);
        $userTransactionCountAggregate = self::_getAggregates($partyId, $tagKeyCount, $periodKey);        
        
        if ( $tagKey == 'walletYearlyTotal' && $userTransactionAmountAggregate['Value'] == 0 )
        {
            $userBalance        = \App\Models\Walletaccount::select('CurrentAmount')->where('UserWalletId', $partyId)->first();
            $dayTransAmount     = $userBalance->CurrentAmount;
            $dayTransctionCount = 1;
        }
        elseif ( $tagKey == 'walletMonthTotal' && $userTransactionAmountAggregate['Value'] == 0 )
        {
            $userBalance        = \App\Models\Walletaccount::select('CurrentAmount')->where('UserWalletId', $partyId)->first();
            $dayTransAmount     = $userBalance->CurrentAmount;
            $dayTransctionCount = 1;
        }
        else
        {
            if ( $refund == TRUE )
            {
                // add up the details
                $dayTransctionCount = $userTransactionCountAggregate['Value'] - 1;
                $dayTransAmount     = $userTransactionAmountAggregate['Value'] - $amount;
            }
            else
            {
                $dayTransctionCount = $userTransactionCountAggregate['Value'] + 1;
                $dayTransAmount     = $userTransactionAmountAggregate['Value'] + $amount;
            }
        }

        $dateTimeOfTrans          = date('Y-m-d H:i:s');
        $usertransactionAggregate = array(
            array(
                'UserWalletID'    => $partyId,
                'TagName'         => $tagKeyAmount,
                'Key'             => $periodKey,
                'Value'           => $dayTransAmount,
                'CreatedDatetime' => $dateTimeOfTrans,
                'CreatedBy'       => 'User'
            ),
            array(
                'UserWalletID'    => $partyId,
                'TagName'         => $tagKeyCount,
                'Key'             => $periodKey,
                'Value'           => $dayTransctionCount,
                'CreatedDatetime' => $dateTimeOfTrans,
                'CreatedBy'       => 'User'
            ),
        );
        if ( $userTransactionCountAggregate )
        {
            foreach ( $usertransactionAggregate as $key => $value )
            {
                \App\Models\Usertransactionstatsaggregate::where('UserWalletID', $partyId)
                        ->where('TagName', $value['TagName'])
                        ->where('Key', $value['Key'])
                        ->update($usertransactionAggregate[$key]);
            }
        }
        else
        {
            \App\Models\Usertransactionstatsaggregate::insert($usertransactionAggregate);
        }
        return TRUE;
    }

    /*
     *  monthlyAggregate - check mounthly user transction amount aggregate
     *  @param integer $partyId
     *  @return object
     */

    private static function _getAggregates($partyId, $tagName, $key)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('Id', 'TagName', 'Key', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', $tagName)
                        ->where('Key', $key)->orderby('Id', 'DESC')->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

}

?>
