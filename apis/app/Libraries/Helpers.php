<?php

namespace App\Libraries;

class Helpers {
    /*
     *  generateOtp - Genrate Randum Number for OTP
     *  @param None
     *  @return Number
     */

    public static function generateOtp()
    {
        return mt_rand(100000, 999999);
    }

    /*
     *  quickRandom - Generate a random string of the given length.
     *  @param None
     *  @return String
     */

    public static function quickRandom()
    {
        return str_random(8);
    }

    /*
     *  getOrderId - Generate Order ID.
     *  @param None
     *  @return Numeric
     */

    public static function getOrderId()
    {
// $timestamp = strftime('%Y%m%d%H%M%S');
// $orderid   = $timestamp . mt_rand(1, 9999);
        $timestamp = DateTime::createFromFormat('U.u', number_format(microtime(TRUE), 4, '.', ''));
        return $timestamp->format('dmyyu');
    }

    /*
     *  getApiTransactionId - Generate API Transction ID.
     *  @param None
     *  @return Numeric
     */

    public static function getApiTransactionId()
    {
        $timestamp = strftime("%Y%m%d%H%M%S");
        $getId     = $timestamp . mt_rand(1, 200);
        return $getId;
    }

    /*
     *  getUniqueId - Generate Unique ID.
     *  @param None
     *  @return Numeric
     */

    public static function getUniqueId()
    {
        $timestamp = strftime("%Y%m%d%H%M%S");
        $uniqueid  = $timestamp . mt_rand(1, 200);
        return $uniqueid;
    }

    /*
     *  dateCompare - Date Comparission
     *  @param fromdate $fromdate
     *  @param todate $todate
     *  @return String / Time
     */

    public static function dateCompare($fromdate, $todate)
    {
        $fromDate = strtotime(str_replace('/', '-', $fromdate));
        $toDate   = strtotime(str_replace('/', '-', $todate) . '+ 5 minute');
        if ($fromDate > $toDate)
        {
            return 'greater';
        }
        else if ($fromDate < $toDate)
        {
            return date('H:i', ($toDate - $fromDate));
        }
        else if ($fromDate == $toDate)
        {
            return 'equal';
        }
    }

    /*
     *  dateDiffMin - Date Diff in Minutes
     *  @param fromdate $fromdate
     *  @param todate $todate
     *  @return numeric
     */

    public static function dateDiffMin($fromdate, $todate)
    {
        $fromDate = strtotime(str_replace('/', '-', $fromdate));
        $toDate   = strtotime(str_replace('/', '-', $todate));
        return round(abs($toDate - $fromDate) / 60);
    }

    /*
     *  OtpSend - Send / Resend OTP
     *  @param pram Array $params
     *  @return Boolean / Array
     */

    public static function OtpSend($params, $request = array()) //remove array once done for all cases.
    {
        //$otp        = Helpers::generateOtp();
        $attempts = 3;
        $otp      = "123456"; //for testing
        $otp_msg  = 'Your OTP is ' . $otp . '. Please verify your ViolaWallet by entering OTP.';
        $sms_arr  = array(
            'mobile_no' => $params['mobileNumber'],
            'message'   => $otp_msg,
        );
        //$msg_id_str = \App\Libraries\Smshub::send_message($sms_arr);

        if (isset($params['attempts']) && $params['attempts'] > 0)
        {
            $attempts       = $params['attempts'];
            $otpData        = array(
                'OTP'       => json_encode(array('otp' => $otp, 'attempts' => $attempts)),
                'CreatedBy' => 'User',
                'UpdatedBy' => 'User',
            );
            \App\Models\Partyotp::where('OTPRequestID', $params['OTPRequestID'])->update($otpData);
            $inserytOTPData = $params['OTPRequestID'];
        }
        else
        {
            $otpData             = array(
                'PartyID'             => $params['UserWalletID'],
                'OTP'                 => json_encode(array('otp' => $otp, 'attempts' => $attempts)),
                'TransactionTypeCode' => $params['typeCode'],
                'CreatedDateTime'     => date('Y-m-d H:i:s'),
                'CreatedBy'           => 'User'
            );
            $inserytOTPData      = \App\Models\Partyotp::insertGetId($otpData);
            $params['otpNumber'] = $otp;
        }

        if ($inserytOTPData)
        {
            $params['otpNumber'] = $otp;
            if (isset($params['deviceName']))
            {
                $optStatus = self:: _sendOtp($params, $request);
            }
            else
            {
                \Log::debug('headers' . json_encode($request->header()));
                $optStatus = self::_sendOtpMessage($params, $request);
            }
            if ($optStatus == FALSE)
            {
                return FALSE;
            }
            $data_v = array(
                'otpMssage' => 'We have sent an OTP to ' . $params['mobileNumber'] . ' mobile number.Please verify by entering OTP',
                'otpNumber' => $otp,
                'otpId'     => $inserytOTPData,
                'otpType'   => $params['typeCode']
            );
            return $data_v;
        }
        return FALSE;
    }

    /**
     * Communication method to send otp
     * @param type $params
     * @param type $request
     * @return type
     */
    private static function _sendOtpMessage($params, $request)
    {
        $templateData = array(
            'partyId'        => $params['UserWalletID'],
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW001',
            'senderEmail'    => $params['emailId'],
            'senderMobile'   => $params['mobileNumber'],
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullname'         => $params['userName'],
                'otpCode'          => $params['otpNumber'],
                'otpExpiryMinutes' => 2,
                'date'             => date('d/m/Y'),
                'time'             => date('H:i A'),
                'supportURL'       => "violamoney.com",
            )
        );
        $notifyStatus = \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        if (!$notifyStatus)
        {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Send Otp to user mobile.
     *
     * @param  Request $params
     * @return Json Response 
     */
    private static function _sendOtp($params, $request)
    {
        $templateData = array(
            'partyId'        => $params['UserWalletID'],
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW003',
            'senderEmail'    => $params['emailId'],
            'senderMobile'   => $params['mobileNumber'],
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullname'         => $params['userName'],
                'otpCode'          => $params['otpNumber'],
                'otpExpiryMinutes' => 2,
                'deviceName'       => $params['deviceName'],
                'email'            => $params['emailId']
            )
        );
        $notifyStatus = \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        \Log::info('New Login:' . json_encode($notifyStatus, JSON_PRETTY_PRINT));
        if (!$notifyStatus)
        {
            return FALSE;
        }
        return TRUE; //sending otp and return response message
    }

    /*
     *  validateOTP - Validate OTP
     *  @param Request $request
     *  @return JSON
     */

    public static function validateVPin($params)
    {
        $validateVpin = \App\Models\Userwallet::select('TPin')->where('UserWalletID', '=', $params['partyId'])->first();
        if (\Illuminate\Support\Facades\Hash::check($params['vPin'], $validateVpin->TPin) === FALSE)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /*
     *  validateOTP - Validate OTP
     *  @param Request $request
     *  @return JSON
     */

    public static function validateOtp($params)
    {
        $validateOtp = \App\Models\Partyotp::select('OTP')
                ->find($params['otpId']);
        if (!$validateOtp)
        {
            return FALSE;
        }
        else
        {
            $otp = json_decode($validateOtp->OTP);
            return ( $otp->otp == $params['otpNumber'] ) ? TRUE : FALSE;
        }
    }

    /*
     *  deviceValid - Extra Params Validation Rules
     *  @params post data.
     *  @return Array
     */

    public static function extraParamValid($request)
    {
        $validate = array();

        if ($request->input('orderBy') !== null)
        {
            $validate['orderBy'] = 'required|in:ASC,DESC';
        }

        if ($request->input('limit') !== null)
        {
            $validate['limit'] = 'required|int';
        }

        if ($request->input('paginate') !== null)
        {
            $validate['paginate'] = 'required|in:Y,N';
        }
        return $validate;
    }

    /*
     *  validateErrorResponse - Validation Check Up
     *  @param Validation Object $validator
     *  @return Boolean / Array
     */

    public static function validateErrorResponse($validator)
    {
        $error_msges = array();
        if ($validator->fails())
        {
            foreach ($validator->messages()->toArray() as $key => $error)
            {
                $error_msges[$key] = $error[0];
            }
            return $error_msges;
        }

        return TRUE;
    }

    /*
     *  deviceValid - Device Validation Rules
     *  @param session token not required for login, registration.
     *  @return Array
     */

    public static function deviceValid($checkSessionToken = 1)
    {

        $valid = array(
            'device_info'   => 'required',
            'device_type'   => 'required|in:W,I,A',
            'device_uid'    => 'required|min:15|max:100',
            'device_token'  => 'required|min:50|max:250',
            'session_token' => 'required|min:15|max:100',
        );
        if ($checkSessionToken == 0)
        {
            unset($valid['session_token']);
        }
        return $valid;
    }

    /*
     *  deviceValidMsg - Device Validation Messages
     *  @param None
     *  @return Array
     */

    public static function deviceValidMsg()
    {
        $messages = array(
            'device_type.required'   => 'Enter Device Type',
            'device_type.in'         => 'Enter Valid device Type',
            'device_info.required'   => 'Enter Valid device Info',
            'device_uid.required'    => 'Enter Valid device Uid',
            'device_uid.min'         => 'Enter Valid device Uid',
            'device_uid.max'         => 'Enter Valid device Uid',
            'device_token.required'  => 'Enter Valid Device Token',
            'device_token.min'       => 'Enter Valid Device Token',
            'device_token.max'       => 'Enter Valid Device Token',
            'session_token.required' => 'Enter Session Token',
            'session_token.min'      => 'Enter Valid Session Token',
            'session_token.max'      => 'Enter Valid Session Token',
        );
        return $messages;
    }

    /*
     *  deviceSSO - Check User Device has session
     *  @param Request $request
     *  @return Boolean
     */

    public static function deviceSSO($request)
    {
        exit;
        $deviceLogin = \App\Models\Partyloginhistory::select('Id', 'SessionStartTime', 'SessionEndTime')
                ->where('PartyID', '=', $request->input('partyId'))
                ->where('DeviceType', '=', $request->input('device_type'))
                ->where('DeviceUniqueID', '=', $request->input('device_uid'))
                ->where('DeviceToken', '=', $request->input('device_token'))
                ->where('SessionID', '=', $request->input('session_token'))
                ->first();

        if (!$deviceLogin)
        {
            return FALSE;
        }
        else
        {
            if ($deviceLogin->SessionEndTime != '')
            {
                return TRUE;
            }
            return FALSE;
        }
    }

    /*
     *  userdata - Common respon for login
     *  @param Results $results
     *  @return Array
     */

    public static function userdata($results)
    {
        $availableBalance       = number_format($results->AvailableBalance, 2, '.', '');
        $availableCurrentAmount = number_format($results->CurrentAmount, 2, '.', '');
        $NonWithdrawAmount      = number_format($results->NonWithdrawAmount, 2, '.', '');
        $HoldAmount             = number_format($results->HoldAmount, 2, '.', '');

        $monthlyKycLimit = config('vwallet.MONTH_LIMIT_NON_KYC');
        if ($results->IsVerificationCompleted === 'Y')
        {
            $monthlyKycLimit = config('vwallet.MONTH_LIMIT_KYC');
        }
        $userTransactionStats = \App\Libraries\Statistics::getUserTransactionStats($results->UserWalletID);
        $userAdd              = \App\Models\Partyaddress::select('partyaddress.AddressLine1', 'partyaddress.AddressLine1', 'partyaddress.City', 'partyaddress.State', 'partyaddress.Country', 'partyaddress.PostalZipCode')
                        ->where('partyaddress.PartyID', '=', $results->UserWalletID)->where('partyaddress.AddressTypeCode', '=', 'HOM')->first();

        $array['UserWalletID']            = $results->UserWalletID;
        $array['violaId']                 = $results->ViolaID;
        $array['PartyID']                 = $results->UserWalletID;
        $array['EmailID']                 = $results->EmailID;
        $array['FirstName']               = $results->FirstName;
        $array['LastName']                = $results->LastName;
        $array['MiddileName']             = $results->MiddleName;
        $array['Gender']                  = ($results->Gender) ? $results->Gender : '';
        $array['MobileNumber']            = $results->MobileNumber;
        $array['DateofBirth']             = ($results->DateofBirth) ? $results->DateofBirth : 'N/A';
        $array['WalletAccountId']         = ($results->WalletAccountId) ? $results->WalletAccountId : 'N/A';
        $array['WalletCountryCode']       = ($results->WalletCountryCode) ? $results->WalletCountryCode : 'N/A';
        $array['MonthlyKycLimit']         = $monthlyKycLimit;
        $array['monthUtilityAmount']      = $userTransactionStats['monthUtilityAmount'];
        $array['monthCrditLimitAmount']   = $userTransactionStats['monthCrditLimitAmount'];
        $array['AvailableBalance']        = ($results->AvailableBalance) ? $availableBalance : '0.00';
        $array['CurrentAmount']           = ($results->CurrentAmount) ? $availableCurrentAmount : '0.00';
        $array['NonWithdrawAmount']       = ($results->NonWithdrawAmount) ? $NonWithdrawAmount : '0.00';
        $array['HoldAmount']              = ($results->HoldAmount) ? $HoldAmount : '0.00';
        $array['RegisteredReferralCode']  = ($results->RegisteredReferralCode) ? $results->RegisteredReferralCode : 'N/A';
        $array['ProfileStatus']           = $results->ProfileStatus;
        $array['PartyUniqueKey']          = $results->PartyUniqueKey;
        $array['ProfileImagePath']        = $results->ProfileImagePath;
        $array['IsVerificationCompleted'] = ($results->IsVerificationCompleted) ? $results->IsVerificationCompleted : 'N';
        $array['AddressLine1']            = '';
        $array['City']                    = '';
        $array['State']                   = '';
        $array['Country']                 = '';
        $array['PostalZipCode']           = '';
        if (isset($userAdd))
        {
            $array['AddressLine1']  = $userAdd->AddressLine1;
            $array['City']          = $userAdd->City;
            $array['State']         = $userAdd->State;
            $array['Country']       = $userAdd->Country;
            $array['PostalZipCode'] = $userAdd->PostalZipCode;
        }
        $array['btn_sequance'] = '';
        return $array;
    }

    /*
     * get user preferance data
     */

    public static function userConfigData($partyId)
    {
        $defaultOder    = config('vwallet.FASTTRACK_MENU');
        $fastTrackKeys  = array_keys($defaultOder);
        $fastTrackOrder = implode(",", $fastTrackKeys);
        $configArray    = array('balanceView' => 'N', 'fastTrack' => $fastTrackOrder);
        $userConfig     = \App\Models\Userconfiguration::select('ConfigType', 'Details')->where('UserWalletID', $partyId)->get();
        if ($userConfig)
        {
            foreach ($userConfig as $configData)
            {
                $configArray[$configData['ConfigType']] = $configData['Details'];
            }
        }

        return $configArray;
    }

    // Api Url
    public static function crmData()
    {
        $apidata['actionurl']               = 'http://139.59.12.188/violacarddev/index.php?';
        $apidata['product']                 = 'Viola_Wallet';
        $apidata['ticket_priority']         = 'Low';
        $apidata['ticket_status']           = 'Open';
        $apidata['ticket_type']             = 'External';
        $apidata['ticket_source']           = 'Portal';
        $apidata['non_login_ticket_source'] = 'Portal1';
        return $apidata;
    }

    public static function curl_send($url, $data)
    {
        $cert_file = dirname(__FILE__) . '/Yesbank/certificate.pem';
        $ch        = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSLCERT, $cert_file);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-IBM-Client-Secret: N8pK2hJ5cD2iD7sM3hS8eV0qN4bT5vH1sY1pB4yH1lK2vW7mE2',
            'X-IBM-Client-Id: 0968025c-397a-45af-8183-f97e4d511b9d',
            'Content-Type: application/json'
        ));

        $output = curl_exec($ch);
        if (!$output)
        {
            $output = curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }

    public static function curlSend($url, $data)
    {
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $output = curl_exec($ch);
        $error  = curl_error($ch);
        curl_close($ch);
        return $output;
    }

    public static function apiUrl()
    {
        $apiurl = 'http://localhost/Vwallet_services/';
        return $apiurl;
    }

    public static function splitFullName($string)
    {
        //Trim multiple spaces
        $stringFilter = preg_replace('!\s+!', ' ', trim($string));
        $first_name   = $middle_name  = $last_name    = NULL;
        $arr          = explode(' ', $stringFilter, 3); //max 3 splits
        $num          = count($arr);
        if (strlen($arr[0]) < 3)
        {
            $arr = array_reverse($arr);
        }
        if ($num == 1)
        {
            list($first_name) = $arr;
        }
        else if ($num == 2)
        {
            list($first_name, $last_name) = $arr;
        }
        else if ($num == 3)
        {
            list($first_name, $middle_name, $last_name) = $arr;
        }
        else
        {
            $first_name = $stringFilter;
        }
        return array(
            'FirstName'  => $first_name,
            'MiddleName' => $middle_name,
            'LastName'   => $last_name,
        );
    }

    // code to get admin data
    public static function GetAdminData()
    {
        $admin_data = \App\Models\Userwallet::join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletId')
                ->select('userwallet.UserWalletID', 'userwallet.FirstName'
                        , 'userwallet.MobileNumber', 'userwallet.ViolaID', 'userwallet.EmailID', 'walletaccount.WalletAccountNumber', 'walletaccount.UserWalletId', 'walletaccount.PrimaryAccountIndicator'
                        , 'walletaccount.WalletCurrencyCode', 'walletaccount.MaximumBalanceLimit', 'walletaccount.FeePercentage', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'walletaccount.HoldAmount', 'walletaccount.WalletAccountId')
//                 ->where('walletaccount.IsAdminWallet', '=', 'Y')
                ->where('walletaccount.UserWalletID', 1)
                ->first();
        return ($admin_data) ? $admin_data : 0;
    }

    /**
     * 
     * @param type $walletId
     * @return type
     */
    public static function GetWalletUserData($walletId)
    {
        $admin_data = \App\Models\Userwallet::join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletId')
                ->select('userwallet.UserWalletID', 'userwallet.FirstName'
                        , 'userwallet.MobileNumber', 'userwallet.ViolaID', 'userwallet.EmailID', 'walletaccount.WalletAccountNumber', 'walletaccount.UserWalletId', 'walletaccount.PrimaryAccountIndicator'
                        , 'walletaccount.WalletCurrencyCode', 'walletaccount.MaximumBalanceLimit', 'walletaccount.FeePercentage', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'walletaccount.HoldAmount', 'walletaccount.WalletAccountId')
//                 ->where('walletaccount.IsAdminWallet', '=', 'Y')
                ->where('walletaccount.WalletAccountId', $walletId)
                ->first();
        return ($admin_data) ? $admin_data : 0;
    }

    /*
     *  getUserSessionId - Get logged in user sessionId
     *  @param object $results
     *  @return Array
     */

    public static function getUserSessionId($request)
    {
        $where    = array(
            'PartyID'   => $request->input('partyId'),
            'IPAddress' => $request->header('ip-address'),
            'SessionID' => $request->header('session-token'),
        );
        $selParty = \App\Models\Partyloginhistory::select('PartyLoginHistoryId')
                ->where($where)
                ->orderBy('PartyLoginHistoryId', 'desc')
                ->first();
        return ($selParty) ? $selParty->PartyLoginHistoryId : FALSE;
    }

    public static function getPartyLatestLoginHistory($partyId, $deviceUID = '', $deviceType = 'M')
    {
        $whereData = array(
            'PartyID'    => $partyId,
            'DeviceType' => $deviceType
        );
        if ($deviceUID)
        {
            $whereData['DeviceUniqueID'] = $deviceUID;
        }
        $loginHistoryData = \App\Models\Partyloginhistory::select('PartyLoginHistoryId', 'PartyID', 'SessionID', 'Email', 'SessionStartTime', 'SessionEndTime', 'IPAddress', 'DeviceUniqueID', 'DeviceType', 'MobileType', 'ExistingUser', 'DeviceToken')
                ->where($whereData)
                ->orderBy('PartyLoginHistoryId', 'DESC')
                ->first();

        return ($loginHistoryData) ? $loginHistoryData : FALSE;
    }

    /**
     * Send now is the main function which is used to send notifications to the users
     * on this function we will get the following parameters in post format and according
     * to the data we will send the messages.
     *
     * @param
     * application_name = The name of the application through which we are sending request.
     *  - V-Wallet
     *  - V-Business
     * notification_for = This will the key for the notification template. With the help of this filed
     *                      We will fetch the data from the language and template file.
     *                      This may has the section or page name.
     *  - nreg = Normal Registration Section.
     *  - qreg = quick Registration Section.
     *  - breg = Business Registration Section.
     *  - aaact = Activate email Account.
     *  - forpas = forgot password.
     *  - etc......
     * notification_type = It may be one of the following. If need to send multiple messages
     *                      Please send multiple things in comma separation format. The value
     *                      may be one of the following data. (all small case)
     *  - email = Email ID
     *  - sms = SMS
     *  - push = Push Notification
     *  - web = Web Notification
     *
     * In case if we need to send email and sms both we can send notification_type = email,sms
     * In the same way if we need to send email, sms and push notification then email,sms,push
     *
     * email_id = if we have email in notification_type then need to send email_id otherwise it should be blank.
     * email_data = if we have email in notification_type then need to send email_data, we will replace this data
     *              in the template otherwise it should be blank.
     * mobile_no = Mobile number if we want to send sms.
     * mobile_data = The data for the sms which need to send in SMS template will be pick from language.
     * device_type = The device type may be windows, android, iphone
     *  - android
     *  - iphone
     * device_token = The token of the device to send the puch notification.
     * device_data = The data which we need to send via push notification, the template will be pick from language file.
     * web_data = the web data which we need to send as web notification, the template will be pick from language file.
     *
     */
    public static function sendMsg($notificationParams)
    {
        $return_response = TRUE;
        if (config('vwallet.communicateUser') === TRUE)
        {
            $notificationParams['application_name'] = config('vwallet.applicationName');

            $notificationType = $notificationParams['notification_type'];
            // check if notification type has more the a value
            if (strpos($notificationType, ',') !== FALSE)
            {
                $notificationTypeArray = explode(',', preg_replace('/\s/', '', $notificationType));
            }
            else
            {
                $notificationTypeArray = array($notificationType);
            }

            $return_response = array(
                'status' => 'success',
                'error'  => FALSE,
            );

            // check if we need to send email with the request
            if (in_array('email', $notificationTypeArray))
            {
                // Error while sending email sometimes so blocking this 
                // for demo purpose.
                // send email go to library               
                $email_response           = \App\Libraries\Communication\Sendemail_lib::sendNow($notificationParams);
                $return_response['email'] = $email_response;
            }

            // check if we need to send sms with the request
            if (in_array('sms', $notificationTypeArray))
            {
                $sms_response           = \App\Libraries\Communication\Sendsms_lib::sendNow($notificationParams);
                $return_response['sms'] = $sms_response;
            }

            // check if we need to send push with the request
            if (in_array('push', $notificationTypeArray))
            {
                $notificationParams['other_details'] = array();
                $push_response                       = \App\Libraries\Communication\Sendpush_lib::sendNow($notificationParams);
                $return_response['push']             = $push_response;
            }

            // check if we need to send web notification
            if (in_array('web', $notificationTypeArray))
            {
                $web_data                            = json_decode($notificationParams['web_data']);
                $notificationParams['other_details'] = array('mobile_no' => $web_data->mobile);
                $web_response                        = \App\Libraries\Communication\Sendweb_lib::sendNow($notificationParams);
                $return_response['web']              = $web_response;
            }
        }
        return $return_response;
    }

    public static function encryptData($response, $encryKey, $respType = FALSE)
    {
        if ($respType === FALSE)
        {
            $responseData = $response->getContent();
        }
        if ($respType === TRUE)
        {
            $responseData = $response;
        }
        $responseEncrypt = \App\Libraries\Crypt\Encryptor::encrypt($responseData, $encryKey);
        if ($respType === TRUE)
        {
            return $responseEncrypt;
        }
        $response->setContent($responseEncrypt);
    }

    public static function decryptData($request, $encryKey, $respType = FALSE)
    {
        if ($respType === TRUE)
        {
            $encryptedData = $request;
        }
        else
        {
            if (!$request->input('encryptedData') && ($respType === FALSE))
                return FALSE;

            $encryptedData = $request->input('encryptedData');
        }
        $encryptedData = str_replace(' ', '+', $encryptedData);
        $requestParam = \App\Libraries\Crypt\Decryptor::decrypt($encryptedData, $encryKey);
        if ($respType === TRUE)
        {
            return $requestParam;
        }

        // query format to array 
        $encryptJsondecode                = json_decode($requestParam, true);
        $encryptJsondecode['requestData'] = $encryptJsondecode;
        $request->merge($encryptJsondecode);
    }

    public static function decryptDataWithoutInput($request, $encryKey, $respType = FALSE)
    {
        if (!$request['encryptedData'] && ($respType === FALSE))
        {
            return FALSE;
        }
        $encryptedData = str_replace(' ', '+', $request['encryptedData']);

        $requestParam = \App\Libraries\Crypt\Decryptor::decrypt($encryptedData, $encryKey);
        if ($respType === TRUE)
        {
            return $requestParam;
        }

        // query format to array 
        $encryptJsondecode                = json_decode($requestParam, true);
        $encryptJsondecode['requestData'] = $encryptJsondecode;
        $request->merge($encryptJsondecode);
    }

    public static function checkwalletTypeState($walletId)
    {
        $walletType = \App\Models\Walletplantype::select('ActiveFlag')
                        ->where('WalletPlanTypeId', $walletId)
                        ->where('EndDate', '>=', date('Y-m-d H:i:s'))->first();
    }

    public static function monthlyAggregate($partyId)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('UserTransactionStatsAggregateId', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', 'MonthlyTransactionAmount')
                        ->where('Key', date('Y-m'))->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

    public static function walletPlanConfigration($walletPlanTypeId)
    {
        $monthlyTrnAmtLimit = \App\Models\Walletplanconfiguration::select('ConfValue')
                        ->where('WalletPlanTypeId', $walletPlanTypeId)
                        ->where('ConfKey', 'MONTHLY_TRANS_AMOUNT_lIMIT')->first();
        return ($monthlyTrnAmtLimit) ? $monthlyTrnAmtLimit : FALSE;
    }

    public static function userTransctionLimitChecker($partyId, $walletPlanTypeId, $amount, $benificary = '')
    {
        $response                = array('error' => TRUE, 'info' => '', 'benificary' => FALSE, 'response' => '');
        $response ['benificary'] = ( $benificary ) ? TRUE : FALSE;
        $walletMonthlyLimit      = self::walletPlanConfigration($walletPlanTypeId);
        if ($walletMonthlyLimit === FALSE)
        {
            $response['info'] = 'Something Went Wrong. Please try Again.';
            return $response;
        }
        $partyMonthlyLimit = self::monthlyAggregate($partyId);
        // if no recordfound for user for that month first transction for the month.
        if (($partyMonthlyLimit === TRUE) && (($partyMonthlyLimit->Value + $amount) > $walletMonthlyLimit->ConfValue))
        {
            $response['info']     = 'User Limit Exceed.';
            $response['response'] = $partyMonthlyLimit;
            return $response;
        }
        $response['error'] = FALSE;
        return $response;
    }

    /*
     *  Publlic Path
     *  @return public path
     */

    public static function publicPath($path = NULL)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }

    /*
     *  Profile Image Url
     *  @return public path
     */

    public static function profileUrl($partyId)
    {
        $baseUrl    = config('vwallet.baseUrl');
        $userFolder = sha1($partyId);
        return $baseUrl . "public/Party/" . $userFolder . '/profile/';
    }

    /*
     *  Banner Image Url
     *  @return public path
     */

    public static function bannerUrl()
    {
        $baseUrl = config('vwallet.baseUrl');
        return $baseUrl . "public/banners/";
    }

    /**
     * Wallet Number Check
     * @return int
     */
    public static function _check_wallet_id()
    {
        $walletid = \App\Models\Walletaccount::where('WalletAccountNumber', '!=', '')->orderBy('WalletAccountId', 'DESC')->value('WalletAccountNumber');
        if (empty($walletid))
        {
            $account_num = '9100000001';
        }
        else
        {
            $account_num = floatval($walletid) + 1;
        }
        return $account_num;
    }

    /**
     * Create Party Login History
     * @return Boolean
     * @param $request Requested data
     * @param $status Login Status
     * @param $partyId Login Party Id
     */
    public static function _addLoginHistory($request, $partyId, $status = 'N', $checkFailCount = TRUE)
    {
        $isUser = 'Y';
        if ($partyId == 0)
        {
            $isUser = 'N';
        }
        $idealTime  = config('vwallet.login_wait_min');
        $idealTime  = '-' . $idealTime . ' minutes';
        $MobileType = $request->header('device-type');
        $deviceType = $request->header('device-type');
        $sessid     = \App\Libraries\Helpers::getUniqueId();
        if ($deviceType != 'W')
            $deviceType = 'M';

        $dateTime       = new \DateTime;
        $dateTime->modify($idealTime);
        $formatted_date = $dateTime->format('Y-m-d H:i:s');

        $party = \App\Models\Partyloginhistory::select('PartyLoginHistoryId', 'PartyID', 'LoginSuccess', 'FailureCount', 'SessionID')
                ->where('SessionStartTime', '>=', $formatted_date)
                ->where('PartyID', '=', $partyId)
                ->where('DeviceUniqueID', '=', $request->header('device-uid'))
                ->where('LoginSuccess', '=', 'N')
                ->orderBy('PartyLoginHistoryId', 'DESC')
                ->first();

        //if FailureCount >=2 no need to do next steps. 
        if (!$party)
        {
            $email      = ($request->input('email')) ? $request->input('email') : $request->input('mobileNumber');
            $insertData = array(
                'PartyID'          => $partyId,
                'SessionID'        => $sessid,
                'Email'            => $email,
                'SessionStartTime' => date('Y-m-d H:i:s'),
                'IPAddress'        => $request->header('ip-address'),
                'LoginSuccess'     => $status,
                'ExistingUser'     => $isUser,
                'MobileType'       => $MobileType,
                'FailureCount'     => 1,
                'DeviceType'       => $deviceType,
                'DeviceToken'      => $request->header('device-token'),
                'DeviceInfo'       => $request->header('device-info'),
                'DeviceUniqueID'   => $request->header('device-uid'),
                'CreatedDateTime'  => date('Y-m-d H:i:s'),
                'CreatedBy'        => 'User'
            );
            if (( $request->header('device-type') == 'W' ) && ($request->header('user-agent') != NULL))
            {
                $insertData['BrowserType'] = $request->header('user-agent');
            }
            $logHistoryId                      = \App\Models\Partyloginhistory::insertGetId($insertData);
            $insertData['PartyLoginHistoryId'] = $logHistoryId;
            return (object) $insertData;
        }

        if ($checkFailCount === TRUE)
        {
            if ($party->FailureCount >= 3)
            {
                return FALSE;
            }
        }

        $update = \App\Models\Partyloginhistory::find($party->PartyLoginHistoryId);
        if ($checkFailCount == TRUE)
        {
            $update->FailureCount = ($party->FailureCount + 1);
        }
        if ($status == 'Y')
        {
            $update->LoginSuccess = 'Y';
            $party->LoginSuccess  = 'Y';
        }
        $update->save();
        return $party;
    }

    public static function updatePartyLoginHistoryStatus($logHisId, $status)
    {
        $update               = \App\Models\Partyloginhistory::find($logHisId);
        $update->LoginSuccess = $status;
        $update->save();

        return $update;
    }

    public static function calculateReminderNextExecution($startDateTime, $notificationCycleType, $notificationCyclePeriod)
    {
        $nextDateString = "";
        if ($notificationCycleType == 'H')
        {
            $nextDateString = "+" . $notificationCyclePeriod . " hours";
        }
        else if ($notificationCycleType == 'D')
        {
            $nextDateString = "+" . $notificationCyclePeriod . " days";
        }
        else if ($notificationCycleType == 'W')
        {
            $nextDateString = "+" . $notificationCyclePeriod . " week";
        }
        else if ($notificationCycleType == 'M')
        {
            $nextDateString = "+" . $notificationCyclePeriod . " month";
        }
        else if ($notificationCycleType == 'Y')
        {
            $nextDateString = "+" . $notificationCyclePeriod . " year";
        }

        $nextDate = date('Y-m-d h:m:i', strtotime($nextDateString, strtotime($startDateTime)));

        return $nextDate;
    }

    // Offer Apply on Payment
    public static function initiateOffer($request)
    {
        $offerData = NULL;
        if (trim($request->input('promoCode')) != NULL)
        {
            $requestArray['aplicableChannel'] = $request->header('device-type');
            $requestArray['promoCode']        = $request->input('promoCode');

            if ($request->input('offerCategoryId') != NULL)
            {
                $requestArray['offerCategoryId'] = $request->input('offerCategoryId');
            }
            if ($request->input('offerUserTypeId') != NULL)
            {
                $requestArray['offerUserTypeId'] = $request->input('offerUserTypeId');
            }

            $requestArray['amount']           = $request->input('amount');
            $requestArray['partyId']          = $request->input('partyId');
            $requestArray['paymentProductId'] = $request->input('paymentProductId');
            $verifyJsonData                   = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
            $verifyData                       = json_decode($verifyJsonData);

            if ($verifyData->is_error == TRUE)
            {
                return json_decode($verifyJsonData);
            }
            $offerData  = $verifyData->res_data;
            $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
            if (!empty($pamentInfo))
            {
                $offerPamentInfo = array('offerPamentInfo' => $pamentInfo);
                $request->request->add($offerPamentInfo);
            }
        }
        if ($request->input('superCash') > 0)
        {
            $requestArray         = array('partyId' => $request->input('partyId'), 'amount' => $request->input('amount'));
            $superCashPaymentInfo = \App\Libraries\OfferRules::getSuperCashPaymentInfo($requestArray);
            if (!empty($superCashPaymentInfo))
            {
                $superCashInput = array('superCashPamentInfo' => $superCashPaymentInfo);
                $request->request->add($superCashInput);
            }
        }

        return TRUE;
    }

    public static function updateOfferDetails($request, $transactionDetailId)
    {
        if ($request->input('offerPamentInfo') != NULL)
        {
            \App\Libraries\OfferBussness::addbenefitTrack($transactionDetailId);
        }
        $superCashPaymentInfo = $request->input('superCashPamentInfo');
        if (isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0)
        {
            $partyId                             = $request->input('partyId');
            $trackRequest                        = [];
            $trackRequest['partyId']             = $partyId;
            $trackRequest['amount']              = $superCashPaymentInfo['offerAmount'];
            $trackRequest['transactionDetailId'] = $transactionDetailId;
            \App\Libraries\OfferBussness::updateUsedAmount($trackRequest);
        }
    }

    public static function debitOfferAmount($transactionDetailId, $partyInfo)
    {
        $transactionDetails = \App\Models\Transactiondetail::select('Amount', 'TransactionType', 'EventCode', 'PromoCode', 'DiscountAmount', 'WalletAmount', 'CashbackAmount', 'VoucherDiscount')
                        ->where('TransactionDetailID', '=', $transactionDetailId)
                        ->orWhere('RefTransactionDetailID', '=', $transactionDetailId)
                        ->orderBy('TransactionDetailID', 'ASC')->get();
        if (!$transactionDetails || ($transactionDetails[0]->PromoCode == ''))
        {
            return FALSE;
        }
        $debitOfferAmountDetails              = array();
        $debitOfferAmountDetails['promoCode'] = $transactionDetails[0]->PromoCode;
        $debitOfferAmountDetails['superCash'] = $transactionDetails[0]->CashbackAmount;
        if (count($transactionDetails) > 1)
        {
            $debitOfferAmountDetails['offerType']   = $transactionDetails[1]->EventCode;
            $debitOfferAmountDetails['offerAmount'] = $transactionDetails[1]->Amount;
        }
        else
        {
            $debitOfferAmountDetails['offerType']   = 'DISCOUNT';
            $debitOfferAmountDetails['offerAmount'] = $transactionDetails[0]->DiscountAmount;
        }

        $userWalletAccount                    = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);
        $userWalletAccount->NonWithdrawAmount = ($userWalletAccount->NonWithdrawAmount + $transactionDetails[0]->CashbackAmount);
        $userWalletAccount->save();

        return $debitOfferAmountDetails;
    }

    public static function requestMoney($request, $partyInfo, $banificiaryInfo)
    {

        $requestArray = array(
            'SenderPartyID'   => $partyInfo->UserWalletID,
            'Amount'          => $request->input('amount'),
            'ReceiverMobNum'  => $partyInfo->MobileNumber,
            'ReceiverPartyID' => $banificiaryInfo->UserWalletID,
            'RequestType'     => $request->input('requestType'),
            'Comments'        => $request->input('remarks'),
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'UpdatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'user',
            'UpdatedBy'       => 'user',
        );

        $requestId = \App\Models\Partyqrcode::insertGetId($requestArray);

        $dateTimeOfTrans   = date('Y-m-d H:i:s');
        $requestMoney      = array(
            'userType' => 'sender',
            'status'   => 'request'
        );
        $notificationLog   = array();
        $notificationLog[] = array(
            'PartyID'               => $partyInfo->UserWalletID,
            'LinkedTransactionID'   => '',
            'NotificationType'      => 'Alert',
            'SentDateTime'          => $dateTimeOfTrans,
            'ReceipentAcknowledged' => 'Y',
            'Message'               => 'Requested '.$request->input('amount').' from '.$banificiaryInfo->FirstName,
            'MessageStatus'         => 'UnRead',
            'CreatedBy'             => 'User',
            'CreatedDateTime'       => $dateTimeOfTrans,
            'ReceipentReceivedDate' => $dateTimeOfTrans,
            'RequestMoneyStatus'    => 'request',
            'RequestMoneyID'        => $requestId,
            'RequestMoneyUser'      => 'sender',
            'RequestMoney'          => $request->input('amount')
        );

        $requestMoney['userType'] = 'receiver';
        $notificationLog[]        = array(
            'PartyID'               => $banificiaryInfo->UserWalletID,
            'LinkedTransactionID'   => '',
            'NotificationType'      => 'Alert',
            'SentDateTime'          => $dateTimeOfTrans,
            'ReceipentAcknowledged' => 'Y',
            'Message'               => $partyInfo->FirstName.' has requested '.$request->input('amount'),
            'MessageStatus'         => 'UnRead',
            'CreatedBy'             => 'User',
            'CreatedDateTime'       => $dateTimeOfTrans,
            'ReceipentReceivedDate' => $dateTimeOfTrans,
            'RequestMoneyStatus'    => 'request',
            'RequestMoneyID'        => $requestId,
            'RequestMoneyUser'      => 'receiver',
            'RequestMoney'          => $request->input('amount')
        );

        \App\Models\Notificationlog::insert($notificationLog);
    }

    public static function _sendMoneyCommunication($request, $partyInfo, $benfInfo, $transDetails)
    {
        $partyId      = $partyInfo->UserWalletID;
        $senderName   = $partyInfo->FirstName . ' ' . $partyInfo->LastName;
        $receiverName = $benfInfo->FirstName . ' ' . $benfInfo->LastName;
        $templateData = [
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW042',
            'senderEmail'    => $partyInfo->EmailID,
            'senderMobile'   => $partyInfo->MobileNumber,
            'reciverId'      => isset($benfInfo->UserWalletID) ? $benfInfo->UserWalletID : '', // if reciver / sender exsists
            'receiverEmail'  => $benfInfo->EmailID,
            'receiverMobile' => $benfInfo->MobileNumber,
            'templateParams' => array(// template utalisation parameters
                'senderName'    => $senderName,
                'receiverName'  => $receiverName,
                'accountName'   => $transDetails['AccountName'],
                'mobileNumber'  => $transDetails['BeneficiaryMobileNumber'],
                'transactionId' => $transDetails['TransactionOrderID'],
                'amount'        => $transDetails['Amount'],
                'balanceAmount' => $transDetails['AvailableBalance'],
                'date'          => date('d/m/Y'),
                'time'          => date('h:i A'),
            )
        ];

        if (($request->input('transType') == 'WB') && ($transDetails['TransactionStatus'] == 'Failed'))
        {
            $templateData['templateKey'] = 'VW043';
        }

        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);

        if (($request->input('transType') == 'WW') && ($request->path() == 'sendMoneyW2W_W2B'))
        {
            $templateData['templateKey']  = 'VW045';
            $templateData['partyId']      = $benfInfo->UserWalletID;
            $templateData['senderEmail']  = $benfInfo->EmailID;
            $templateData['senderMobile'] = $benfInfo->MobileNumber;
            \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        }
    }

    public static function _requestMoneyCommunication($request, $partyInfo, $benfInfo)
    {
        $partyId      = $benfInfo->UserWalletID;
        $senderName   = $partyInfo->FirstName . ' ' . $partyInfo->LastName;
        $receiverName = $benfInfo->FirstName . ' ' . $benfInfo->LastName;
        $senderEmail  = $benfInfo->EmailID;
        $senderMobile = $benfInfo->MobileNumber;

        $templateData = [
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW089',
            'senderEmail'    => $senderEmail,
            'senderMobile'   => $senderMobile,
            'reciverId'      => '', // if reciver / sender exsists
            'receiverEmail'  => '',
            'receiverMobile' => '',
            'templateParams' => array(// template utalisation parameters
                'senderName'   => $senderName,
                'receiverName' => $receiverName,
                'amount'       => $request->input('amount'),
                'date'         => date('d/m/Y'),
                'time'         => date('h:i A'),
            )
        ];

        if ($request->input('serviceType') == 'reject')
        {
            $templateData['templateKey'] = 'VW090';
        }

        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
    }

}

?>
