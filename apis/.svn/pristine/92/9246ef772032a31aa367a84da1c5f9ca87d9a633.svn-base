<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider {

    public function boot()
    {
        Validator::extend('phone', function($attribute, $value, $parameters) {
            return substr($value, 0, 3) == '+91';
        });
        Validator::extend('address_char', function($attribute, $value, $parameters) {
            return preg_match('/^[a-zA-Z#, - ]*$/', $value);
        });
        Validator::extend('alpha_space', function($attribute, $value, $parameters) {
            return preg_match('/^[a-zA-Z ]*$/', $value);
        });

        Validator::extend('numerics', function($attribute, $value, $parameters) {
            return ( bool ) preg_match('/^[0-9]*$/', $value);
        });

        Validator::extend('numeric_comma', function($attribute, $value, $parameters) {
            return ( bool ) preg_match('/^[0-9,"]*$/', $value);
        });

        Validator::extend('numeric_dot', function($attribute, $value, $parameters) {
            return ( bool ) preg_match('/^[0-9.]*$/', $value);
        });

        Validator::extend('alpha_numeric_space', function($attribute, $value, $parameters) {
            return ( bool ) preg_match('/^([a-z0-9 ])+$/i', $value);
        });

        Validator::extend('mobile_number', function($attribute, $value, $parameters) {
            if ( strlen($value) == 16 && preg_match("/^(?:(?:\+|0{0,2})91) [0-9]{3} [0-9]{3} [0-9]{4}$/", $value) )
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        });

        Validator::extend('valid_amount', function($attribute, $value, $parameters) {
            if ( $value <= 0 )
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        });

        Validator::extend('before_date', function($attribute, $value, $parameters) {
            $date      = date('d-m-Y');
            $sys_date  = strtotime($date);
            $user_date = strtotime($value);
            if ( $user_date < $sys_date )
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        });

        Validator::extend('password', function($attribute, $value, $parameters) {

            $uppercase = preg_match('#[A-Z]#', $value);
            $lowercase = preg_match('#[a-z]#', $value);
            $number    = preg_match('#[0-9]#', $value);
            $special   = preg_match('#[\W]{1,}#', $value);
            $length    = strlen($value) >= 8;

            if ( ! $uppercase OR ! $lowercase OR ! $number OR ! $special OR ! $length )
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        });
    }

    public function register()
    {
        
    }

}
