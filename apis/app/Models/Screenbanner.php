<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Screenbanner
 * 
 * @property int $ScreenBannerID
 * @property string $BannerName
 * @property string $BannerImageLocation
 * @property \Carbon\Carbon $EffectiveDate
 * @property \Carbon\Carbon $ExpiryDate
 * @property string $ActiveIndicator
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $Filler1
 * @property string $Filler2
 * @property string $Filler3
 * 
 * @property \Illuminate\Database\Eloquent\Collection $promotionaloffers
 *
 * @package App\Models
 */
class Screenbanner extends Eloquent
{
	protected $primaryKey = 'ScreenBannerID';
	public $timestamps = false;

	protected $dates = [
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BannerName',
		'BannerImageLocation',
		'EffectiveDate',
		'ExpiryDate',
		'ActiveIndicator',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'Filler1',
		'Filler2',
		'Filler3'
	];

	public function promotionaloffers()
	{
		return $this->hasMany(\App\Models\Promotionaloffer::class, 'OfferBannerImageID');
	}
}
