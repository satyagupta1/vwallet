<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * When a ticket raised on call with Ticket info template
 *  param $fullname varchar
 *  param $queryName varchar
 *  param $ticketNumber varchar
 *  param $dateTime varchar
 *  param $supportURL varchar 
 * 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Ticket Number : ' . $ticketNumber . ' from ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
We are sorry to hear about your problem and will do everything to resolve it accordingly. 
To ensure the query is well taken care, we have created a ticket number ' . $ticketNumber . '. 
    This will help you track all the communication and work associated with your query. 
    Please use this ticket number in all associated calls or emails<br/>
To see your query and to add any further comments, click on the link below
<a href="' . $supportURL . '" > here</a>
<br/><br/>Team ViolaWallet',
    ],
    'sms'   => [
        'text' => '
 Hi ' . $fullname . ',
Your query has been registered with us and is being reviewed by our Support Team 
Ticket No: ' . $ticketNumber . '
Date & Time: ' . $dateTime . ' 
 ',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Ticket raised. Ticket number :' . $ticketNumber,
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Query ' . $queryName . ' raised. Ticket number:' . $ticketNumber . '',
    ],
];

echo json_encode($array);

/* End of file support_call_ticket_info.php */
