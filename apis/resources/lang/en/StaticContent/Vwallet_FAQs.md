**FAQ&#39;s**

**About V-Wallet**

**1) How can I contact V-Wallet?**
 &gt;We&#39;re available 24/7 for our customers and you can drop us a line on care@vwallet.in You can also give us a call at 040-66495534 between 9AM – 9PM all 7 days (excluding National Holidays). For potential business partners, we can be reached at info@vwallet.in

**2) Where do I spend the money on my V-Wallet account?**
        &gt;You can spend the money loaded onto your V-Wallet account for a variety of things. You can send money to or request money from friends and family. You can split bills with your friends when you dine out, collect money for movie tickets from your friends, remind friends that they owe you money and more! You can of course also use it to pay with select merchants online.

**3) What can I do with V-Wallet?**
&gt;Deposit money
&gt;Send money to any mobile number
&gt;Send money to any bank account
&gt;Prepaid Recharge
&gt;Postpaid bill payments
&gt;DTH Recharge
&gt;Utility Payments
&gt;Merchant Payment
&gt;Withdraw money

**4) What are the benefits of using V-Wallet?**
        &gt;By registering for V-Wallet, you would gain access to a variety of services, which include:
&gt;Secure cash-free transactions anytime from anywhere
&gt;Securely store all your credit/debit cards and bank accounts for convenient and faster payments
&gt;Make in-store and online payments across a variety of merchants
&gt;Transfer funds to other V-Wallet users and to bank accounts
&gt;Pay bills and recharge mobile/DTH using a single app
&gt;Get great offers, deals and coupons from top brands and also from your neighbourhood stores

**5) How do I dispute an error on my V-Wallet account?**
 &gt;We have built stringent checks at our end to ensure that there is no error/dispute while you transact through V-Wallet. In fact, after each transaction you will receive a SMS and/or email alert from us and a monthly e-statement detailing your transactions for the given month.
        &gt;We also recommend that you periodically keep checking your wallet summary for your transaction history. However, if you suspect an error on your statement or receipt or you need more information about a transfer listed on your statement, we request you to let us know about the probable anomaly within sixty (60) days after we sent the FIRST statement on which the problem or error appeared. In order to reach us, please call us at 040-66495534 or write to us at [care@vwallet.in](mailto:care@vwallet.in) .

You must include the following when disputing an issue:
        &gt;Your Name, User ID &amp; the amount you think is at stake

The issue and why you believe it is an error. In other words, tell us why you need more information.
        &gt;If you choose to call our Customer Support and tell us the issue orally, also send us your complaint/query in writing within ten (10) business days. We will investigate and determine whether an error occurred within ten (10) business days after we hear from you, and will promptly correct any errors found. If we need more time, however, we may take up to forty-five (45) days to investigate your complaint or question. If we ask you to send us your question or complaint in writing, and we do not receive it within ten (10) business days, we may not proceed with the investigation. If we find that there was no error, we will also send you a written communication stating the same.

**5) How can my customer download the V-Wallet App to pay me?**
        &gt;Anyone can download the V-Wallet App using any Android or Apple Smartphone from the Google PlayStore or iOS App store
&gt;Just click on the Google PlayStore or iOS App store icon
&gt;Search for &#39;V-Wallet&#39;
&gt;Click on Install
&gt;User would have to do a quick one time registration before they can start paying using V-Wallet

**6)Money deducted from wallet, but not reflecting in your bank account?**
 &gt;V-Wallet customers can transfer money from their V-Wallets to a bank account at any time. We process this transaction instantly and in most cases money is reflected in user&#39;s Bank accounts within few minutes. However, when the banking systems are going through stress due load spikes, banks could take typically upto 7 working days to credit the amount in your bank account.

**7)Money deducted from bank account but not reflected in your V-Wallet wallet?**
 &gt;The next time you receive a notification from the bank saying money has been deducted, but the amount doesn&#39;t reflect in your V-Wallet account, don&#39;t worry. Your money is absolutely safe in the banking system.
        &gt;Transaction goes in pending when we don&#39;t receive the final confirmation from your bank or V-Wallet gateway. These exceptions happen when banking payment gateways are facing some technical outages or load spikes.
    
        In such cases, our systems continue to follow-up with the bank/gateway for the next 4 hrs. In case we don&#39;t get confirmation for transaction success from your bank/gateway within the next 4 hrs, the transaction fails. In case money was deducted from your account, your bank could take 7–14 days to credit the refund in your account. Since this time is taken by your bank, V-Wallet has little capability in expediting it. Hence, we suggest you to please wait for this duration. You may also contact your bank for any delays.

We hope that we are able to address your concern through the concern above. If you have a different concern, you can go through our blogs on other topics as well. In case your problem is still not resolved, you can write to us on V-Wallet.com/care or register your complaint by calling our IVR number: 040-66495534

**8)Money deducted from your account, but recharge is unsuccessful**
 &gt;Withdrawal to bank account is allowed post 45 days of adding amount to the wallet.
        &gt;You have not yet crossed the minimum waiting period.
        &gt;Promotional cash given out by merchants can&#39;t be withdrawn to bank
        &gt;V-Wallet may restrict withdrawal of money in case we suspect any misuse of bank withdrawal functionality- For any assistance please reach out to us at care@vwallet.in

**9)Unable to withdraw money from account?**
 &gt;You may be unable to withdraw money from your V-Wallet account to your bank account due to any of the following reasons,

-  Withdrawal to bank account is allowed post 45 days of adding amount to the wallet.
-  You have not yet crossed the minimum waiting period.
-  Promotional cash given out by merchants can&#39;t be withdrawn to bank
-  V-Wallet may restrict withdrawal of money in case we suspect any misuse of bank withdrawal functionality- For any assistance please reach out to us at care@vwallet.in

**10)how to withdraw money from account?**
 &gt;Withdrawal to bank account will be allowed post 45 days of adding amount to the wallet.
&gt;This includes add cash, P2P, refund/reversal to wallet (excludes cashback).
&gt;Kindly note that 45 days is calculated from the date of wallet amount added/refunded from any source.
&gt;You can continue to use the wallet amount for all your day to day payment needs right from recharges/bill payments/utilities.

**11)how do I close my account?**
&gt;In order to close your V-Wallet account, simply drop a mail at V-Walletsupport@\*\*\*\*\*.com and we will revert as soon as possible.

**12) Is there a limit to receiving payments in V-Wallet?**
 &gt; You can accept payments up to Rs. 20,000 per month in your non-KYC V-Wallet account. After the limit exceeds, the payments will automatically start flowing into your linked Bank account. You can also upgrade to a full KYC Wallet with Rs. 100K monthly limit. To know more, reach us at 040-66495534 or [care@V-Wallet.in](mailto:care@V-Wallet.in) .

**13)How do I load Money on to my account?**
 &gt;To add money to your account

- Click on &quot;Add Money&quot; after logging in to your account.
- Choose the mode of transfer (debit/card/credit card or net banking).
- Enter the required card/net banking details.

The amount will reflect in your account in no time.

**Sign-up/Login Page**
**1) **What are the modes/channels to register on V-Wallet?**
        &gt;V-Wallet Users
        &gt;V-Wallet Mobile App
        &gt;V-Wallet Call Centre (\*\*\*\*)

**2) I have not turned 10 yet. Can I sign up for V-Wallet?**
 &gt;To sign up for V-Wallet, your minimum age should be 18 years (on your last birthday).

**3) Do you verify my e-mail ID?**
 &gt;Yes. To verify your email, we send you a link. If it has not arrived, check your spam or junk mail folder. If it has not arrived, click on resend tab on sign up page to get new link.

**4) Do you verify my mobile number?**
 &gt;Yes. We send you a mobile verification code on your mobile number, the one you shared with us while signing up. If you do not receive a mobile verification code within 10 minutes, click on the resend verification tab on the sign up page to get a new mobile verification code.

**5) Which phones are supported by V-Wallet?**
Currently V-Wallet is available only for ios devices having ios version 9.3 or higher.

If you are on an older version, please make sure you upgrade to your phone before you install V-Wallet application. Else, you can always go to our website vwallet.in-to access our services via mobile.

**My Account**
**1) How should I activate my V-Wallet Merchant Account?**
        &gt;Once you have submitted your application and uploaded all the documents, your account will be instantly activated
        &gt;You will receive an SMS with your Merchant ID and Terminal ID on completion of sign-up
        &gt;However, we will verify all documents uploaded by you
        &gt;If the documents are verified successfully, you would be upgraded to a full KYC customer and your monthly limits for transactions will be increased
        &gt;You will also receive a notification via SMS/Email from us the moment your documents are verified

**2) Can I have multiple Merchant accounts?**
  **&gt;** Yes, you can have multiple accounts under one mobile number registered in different merchant hierarchy with V-Wallet
        &gt;For example, you can have 1 account whereby you are the master merchant, another account where you are a terminal user under some other master merchant
        &gt;You cannot have 2 master merchant accounts with the same mobile number
        &gt;If you have different merchant accounts under one mobile number registered, after logging in to the App/Web portal, you will be asked to choose from different accounts, of which you wish to login

**3) How should I block or unblock my account?**
        &gt;There are different status types for a blocked V-Wallet account which can enable certain functionalities for the account as below:
        &gt;Terminate account: If the account is terminated, you will not be able to use or login for any purpose. Also, if you have any sub users in your account, all such user accounts will also be terminated. At any given point, if you want to start using V-Wallet again, you will have to signup again.
        &gt;You can Visit the V-Wallet Merchant web portal and block your account or call V-Wallet Merchant Care.

**4) How can I view my account limits**

**5) How can I accept payments using Credit or Debit Cards?**

**6)How can I stop receiving emails or messages from V-Wallet?**
        &gt;It is possible to unsubscribe our promotional emails. Just write to us at [care@vwallet.in](mailto:care@vwallet.in)  or click on the unsubscribe link at the bottom of the promotional emails. However, you will still receive transactional emails or messages from V-Wallet.

**7) Forgot Password?**
 &gt;To set a new password, click on &quot;Forgot Password&quot; . Once you enter your credentials, a one-time Verification Code will be sent to your email through which you&#39;ll be able to set a new password.

**8) How can I check the status of a transaction?**
 &gt;In order to check the status of your transaction, log in to your V-Wallet and select &#39;Transaction History&#39; tab from the menu.

**9)I am unable to access my account because of security reasons, what should I do ?**
 &gt;In such cases, please contact [care@vwallet.in](mailto:care@vwallet.in) .

**10) Is my account information safe?**
        &gt;Don&#39;t worry! V-Wallet use your mobile number for identification and correspondence and does not share any of your bank details with anyone. All the information transmitted between you and V-Wallet is guarded by our advanced encryption which ensures that any unauthorized access (hacking) is rejected at the entry level. For additional security, whenever you try to send money through [www.V-wallet.](http://www.V-wallet.com/)in; an OTP (one Time Password) is sent to your registered mobile number.

**11) Is there an expiry date on the account?**
        &gt;Yes, if your account remains inactive for 6 months, we hold the right to make your account inactive. We will be sending you an email notifying you about your account inactivity as well two more communications before your account expires.

**12) How do I set a particular account as my default account?**
        &gt;Tap the app drawer on the top right corner of your app screen and go to &#39;Accounts &amp; Profiles&#39;. Once there, simply select an account as your default account by tapping on it anywhere in the row.

**13) What are my monthly limits?**
        &gt;If you are a non KYC customer, you have a monthly incoming limit of Rs.20, 000. Your balance limit is also Rs. 20,000.

**14) What are the modes of loading money into V-Wallet?**
        &gt;Net Banking
        &gt;Debit Card
        &gt;Credit card

**Wallet Upgrade**
**1) Who is a V-Wallet KYC Customer?**
        &gt;When you Sign Up on V-Wallet, a wallet gets created for you in which you can add upto Rs. 20,000/- in a calendar month. This is as per RBI regulations. As a result there is a cap on how much you can spend. Also, there can be delays in getting cashback if the monthly limit of adding Rs. 20,000 gets exceeded.
        &gt;You can upgrade your wallet for FREE by getting your KYC (Know Your Customer) done and get rid of the Rs. 20,000 monthly limit. Additionally, you will get the following benefits:
        &gt;Balance Limit increases to 100,000
&gt;Special offers and more cashbacks

**2) How do I become a V-Wallet KYC customer?**
        &gt;In order to become a V-Wallet KYC customer you need to get your KYC (Know Your Customer) done which simply means that you need to get your documents of address proof and identity proof verified. You can initiate the process from your V-Wallet app only.
&gt;V-Wallet App
        &gt;You can find the &quot;V-Wallet KYC&quot; section on the Profile Page.
        &gt;On V-Wallet app, you can complete your KYC in any one of the following ways:
        &gt;Request a visit from our agent at your home/office
        &gt;Verify documents at your nearest V-Wallet KYC centre
        &gt;You can also follow the URL:

**3) What are the documents needed to complete my KYC?**
 &gt;In order to complete your KYC, we need to verify original document of your Aadhaar card

**4) Are there any limits on an upgraded wallet?**
        &gt;The balance in your wallet at any point in time cannot exceed Rs. 1, 00, 000.
        &gt;Also, the total amount that you can send to other V-Wallet customers or to any bank account is capped at Rs 25,000 for a calendar month.

**Add money/Request money**
**1) What is add money?**
        &gt;It simply means adding money to the wallet. Whenever you add money, we refer to it as &quot;add money&quot; and the amount you add reflects as your V-Wallet balance.

**2) Why is it necessary to add money to the wallet?**
        &gt;Since V-Wallet is a prepaid wallet, you can make transactions quickly only when there is money in your wallet. So it is important that you add money in your wallet.

**3) What is Balance?**
        &gt;Your &#39;Balance&#39; is the money that is available in your V-Wallet after all pending transactions, if any, are processed.

**4) How do I request money from V-Wallet?**
        &gt;Go to &#39;Request Money&#39; tab. Enter the amount of money that you wish to request. Select a friend &amp; send an &#39;request money&#39; request.

**5) I received an &#39;request money&#39; request. Now what?**
        &gt;All you have to do is accept the request and the money will be instantly credited to your friend&#39;s V-wallet.

**Send money/Transfer money**
**1) How do I send money via V-Wallet?**
        It&#39;s a simple two-step process:
                Step 1:Simple tap on the &#39;Send Money&#39; option displayed on the default screen of the app.
                Step 2: Now select/enter the mobile number of the person you want to send the money to and then hit &#39;Send&#39;. Voila! You&#39;re done!

**2) Is there a limit on the amount I can send?**
        &gt;As per RBI guidelines you cannot send more that Rs.5,000 in one transaction, however there is no limit to the number of transactions you can do.

**3) How do I send money to a friend?**
        &gt;Using V-Wallet, you can send money to a friend over his mobile number/email or bank a/c. Select the desired route. Enter the details and you&#39;re done.

\*Note: You can send money to friends who do not have an V-Wallet.

**4) My friend is not on my social contact list? Can I still send him send/re money requests?**
        &gt;Yes! The request for send/request money could be sent to a friend (in form of a link) on his/her:
Mobile number
E-mail ID

**5) Is it mandatory for my friend to have an V-Wallet to receive money sent by me?**
        You can send money to any of your friends whether or not they have V-Wallet. However, to access the funds sent by you, your friend will have to register for V-Wallet within 7 days or else the money will be credited back to your wallet.

**6) What if my friend does not register with V-Wallet?**
        &gt;When you send money but your friend does not open an V-Wallet account within 7 days, the amount will be credited back to your V-Wallet.

**7) How do I know when money has been received by my friend?**
        &gt; We will send you SMS &amp; email notification (if email is registered with us) stating that the beneficiary has received the amount &amp; your V-Wallet has been debited.

**8) I am trying to send money but unable to send. Why?**
        &gt;If you are unable to send money it could be that you are trying to send more than the limit that is allowed for money transfer. For more information, please refer to section Money Transfer Limits or drop us a line on [care@vwallet.in](mailto:care@vwallet.in)  stating your problem and we&#39;ll make sure your issue is resolved on priority.

**Sub Accounts**
**1.  What is sub accounts &amp; how can I create?**
&gt;Sub Accounts means creating accounts with parental control.
 &gt;On V-Wallet customer should have an ability to create multiple sub         accounts by  this customer can transfer funds to the Sub-accounts. There         won&#39;t be any Physical card request for sub account holders all the         transactions performed in this account&#39;s will be virtual cash only. And         funds can be transferred only by the customer who created that respective sub account.

** 2. Whom can I add in sub accounts &amp; what is the criteria?**
&gt;

** 3. I am unable to add sub accounts?**
  &gt; Please check your internet connection on your device.

 **4. Can I edit/delete the existing sub accounts?**
 &gt;Yes, you can edit/delete existing sub accounts from the list.

 **5. I am unable to view the existing sub accounts?**
  &gt;Please check your internet connection on your device.

 **6. Can I see all transactions in sub accounts?**
  &gt;Yes, you can view all transactions from sub account&#39;s transaction  list.

 **7.  What is the transaction limit for sub account?**
&gt;Cumulative transactions for all sub accounts in primary account cannot exceed statutory limits.

**withdrawals**
**1) Are there any charges involved for withdrawal transactions?**
  **&gt;** There are absolutely no charges for withdrawal transactions on V-Wallet.

**2) What is the maximum amount of money I can withdraw from my V-Wallet account at any time?**
        &gt; Money can be withdrawn from ATM only by the customers who have full KYC completed.
        &gt;User can always transfer full money back to the source of funds (DC/CC/NB) .
        &gt;User can transfer money upto INR 5000 to bank account through IMPS/NEFT

**3) What is a source account?**
        &gt;The source account is where you loaded the money from in the first place. It could be your debit card/credit card or your Net-Banking Account. Money is sent to you source account for safety reasons, to protect you from fraud.

**4) Will my withdrawal to bank account be reversed if I enter the wrong account details?**
 &gt;If the account number or IFSC are found to be incorrect by the bank, we will reverse the amount into your V-Wallet Account. The bank rejects transactions only on the next day so this will take 2 business days from when you raised the original withdrawal request.

**5) How long will it take to process a withdrawal?**
 &gt;The withdrawal to your bank account should happen within 2 business / working days.
        &gt;The withdrawal to your source account will take between 7 to 10 working days to process.

**6) Can someone withdraw on my behalf?**
        No, the money cannot be withdrawn on your behalf as the withdrawal process is initiated by you on your handset.

**My Benefeciaries**
        What is beneficiary &amp; how I can add?
        &gt;Benefeciaries are the people who would be receiving payments from the user
        &gt;You can go to Payments and proceed to add beneficiary through mobile number or email.

        I am unable to add beneficiary?
                &gt;Please check your internet settings on your device.

        Can I edit/delete the existing beneficiaries?
                &gt;Yes. You can delete existing benefeciaries from the list.

        I am unable to view the existing beneficiaries?
                &gt;Please check your internet settings on your device.

        Can I transfer the money to non-beneficiaries?
                &gt;Yes, you can transfer money to non benefeciaries.



**Warranties**
**TripleClik**
**1) What is TripleClik?**
 &gt;TripleClik is a patented feature of Viola. As the terms says, we aim to simplify and process transfer in just 3 clicks.

**2) How can I add beneficiaries to TripleClik?**
        &gt;You can add benefeciaries either through Manage my Benefeciary-Add new Beneficiary-Add new Benefeciary or Manage TripleClik-Add someone new to my list.

        Can I transfer money without adding to TripleClik?
                &gt;Yes. You can transfer money without adding to TripleClik.

        I am unable to view the beneficiary list?
                &gt;Please check your internet connection on your device.

        Can I edit/delete the beneficiary list in TripleClik?
                &gt;Yes, you can edit/delete the beneficiary list in TripleClik.

        How much time it will take to add beneficiary in tripleclick?
                &gt;Beneficiaries in TripleClik are added instantly.

        How much time it will take to transfer money to beneficiary account?
                &gt;Money is transferred securely and instantly to your benefeciary account.

        Where can I see the TripleClik transaction history?
                &gt;TripleClik transaction are like any other transactions, user can see transactions through TripleClik in transaction history of the account.

**Virtual cards**
**1)  Is it safer than the credit card I carry in my wallet?**
 &gt;The key details of your VCC like the card number, expiry date, etc. are visible online. You can use these details to transact online and never be worried about losing your card or having to carry it &#39;safely&#39; in your wallet. Besides, you don&#39;t have to wait for a physical card and ATM PIN to make their way to your residence.

**2) How do I use the virtual card?**
        &gt;Since no physical plastic card is issued, it can be used for online transactions only.

**3) What limit do I get for a virtual card? Can I alter the limit depending on my needs?**
        &gt;The initial credit limit of your virtual credit card will be Re.1/-. You can change the maximum limit on your VCC within the overall limit of your primary card.

**4) How do I make payments for my virtual card transactions?**
        &gt;The transactions made with the virtual card will be visible online in your primary credit card statement. You can make payment through Infinity.

**5) Can I place request to block the virtual card online?**
        &gt;Yes, you can block your VCC instantaneously online.

**6) How about the CVV number? Does it change every time I use the card for an online transaction?**
        &gt;The CVV remains unchanged as long as the card is in the active stage. The CVV will change only if you hot-list the current card for some reason and ask for a new card.

**7) How do I apply for a prepaid virtual card?**
        &gt;It is so simple, just click on apply button and follow the easy and simple instructions.

**8) Is prepaid virtual card has any physical shape?**
        &gt;No, it does not have any physical presence.

**9) How many time I can apply for a prepaid virtual card?**
        &gt;There are no restrictions, you can get as much as you want.

**10) Can I gift prepaid virtual card to my friends and family?**
        &gt;Yes, you can gift it to anyone you want.

**11) Can I use prepaid virtual card on ATM machine?**
        &gt;No, it will not work on ATM because it requires a physical card.

**Gift Cards**
**1) can I transfer funds from one gift card to any other gift card?**
        &gt;No, you cannot transfer funds from one Gift Card to any other card.

**2) what is the validity of gift card?**
 &gt;The validity of the card is 1 year from the data of issue i.e. &quot;Valid From&quot; as printed on the card

**3) What is the estimated delivery time for gift card?**
 &gt;Electronic: Delivery is instant. On successful completion of transaction the customer will receive the voucher instantly on his mobile via SMS or on his email address.
        &gt;Physical: All orders are delivered in 4 to 5 working days from the time the payment is received and verified. Working days exclude public holidays and Sundays. For Special Offers &amp; Promotions please add 2 more working Days to the Delivery Timeline. These special offers mean a Seasonal, Combo deals or short term promotional Shopping vouchers.

**4) How do I order gift cards?**
 &gt;Find step-by-step instructions on ordering gift cards here.

**5) Can I transfer funds from one Gift Card to any other card?**
&gt;No, you cannot transfer funds from one Gift Card to any other card.

**Favourites**
**Recharges and Bill Payments**
**1) How can I recharge through SMS?**
 &gt;Use SMS to do a recharge using V-Wallet.

**2) What should I do if my recharge is not done?**
 &gt;Wait for 5 minutes after the payment is successfully done. If your mobile number is still not recharged, write to us at [care@vwallet.in](mailto:care@vwallet.in) . We will respond to it as soon as possible.

**3) Does the recharge happen immediately?**
        &gt;Yes, you get your recharge immediately.
        &gt;As soon as the payment is made, you will get a confirmation mail from V-Wallet.in and a recharge message from your mobile operator. Generally, it takes less than 10 seconds for the transaction to complete.

**4) Can I recharge without selecting coupons?**
        &gt;Although we would like you to reap maximum benefits of the coupons available, yes, you can also choose to recharge without opting for it.

**5) What are the fees for making a payment?**
 &gt;For most of our services, such as prepaid recharges, postpaid bill payments, paying online merchants etc there are no charges. However, in case of utility bills (Electricity,  Gas, Broadband, Landline), you are charged a minimum fee to make this payment.

**6) How Do I make payments?**
 &gt; Lookout for the V-Wallet sign which you will come across while browsing several merchant sites. Keep your account loaded to enjoy a seamless and pleasurable payment experience.

**7) Can I recharge any mobile, DTH, Datacard etc.?**
        &gt;V-Wallet Wallet supports operators&#39; recharges across all circles in India. For the full list of operators, please click here.

**8) Can I pay bills through V-Wallet?**
        With V-Wallet, you can pay postpaid bills without having to look for a parking space to drop cheques or chase someone to make the payments. All this convenience comes to you for a minimal convenience charge. For the full list of operators, please click here.

**9) Can I pay a bill of amount &gt;10k on V-Wallet?**
        &gt;Yes, you can use V-Wallet&#39;s app or website to pay any amount. If you don&#39;t have enough balance in the  V-Wllet then you can use debit card, netbanking, credit card or IMPS as the modes of payment to pay the remaining balance.

**10) Can I transfer the cashback amount to my bank account?**
Yes, you can transfer cashback amount to your bank by paying a small fee. 1% in case you have a KYC wallet and 4% in case you don&#39;t have a KYC wallet. You can get a KYC done by requesting to &quot;Upgrade&quot; your wallet from the V-Wallet app.

**Foreign Exchange**
**Coupons/Deals/promo code**
**1)What is promo codes?**
        &gt;Promo code is a promotional code directly from online store to promote the products and using promo code you can get discounts from the respective store on your checkout cart.

**2) What is coupon?**
        &gt;Coupon is nothing but a code, which you can use on the shopping site to avail a discount.

**3)  How do I use a promo code?**
 &gt;If you have a promo code for your purchase or recharge, you can add it on the checkout page.

**4) Can I use the coupon multiple times?**
        &gt;It depends on T&amp;C and policy of online store. But most of the coupons can be used for multiple times.

**5) How often do you update your offers?**
        &gt;We keep adding Coupons &amp; Deals on every day in our DesiPromoCode and Updates happen round the clock as well.

**6) How promo code will useful for me?**
        &gt;Yes, because when you apply promo code while check out will get decent amount of discount on your actual purchase amount

**7) Will I be charged extra for the coupons?**
 &gt;There is no extra charge for the coupons. However, we do have exclusive coupons that are paid. The price will be mentioned on the individual coupons. You will not be charged anything apart from the recharge amount if you do not opt for any coupons.

**8) What should I do if coupons have not been received after a successful recharge?**
 &gt;You can write us an email at [care@vwallet.in](mailto:care@vwallet.in)  with the order ID and we&#39;ll re-send the coupons to you. Before that, please check your &quot;Spam&quot; and &quot;Promotions&quot; folder.

**9) Can I give these coupons to anybody?**
        &gt;Absolutely, these coupons are transferable. You can give these coupons to your friends and relatives for them to redeem.

**10) What is the validity of the coupons selected?**
 &gt;Different coupons have different validity and this is mentioned on the Terms &amp; Conditions on each coupon. You can go through the same and redeem the coupons within the respective validity period.

**Referral Code**
**1) What is the V-Wallet Refer and Earn Program ?**

**2) Can I invite anyone?**
 &gt;Yes, you can invite any of your friends or acquaintances. However, to ensure that users don&#39;t get spammed, please ensure that you only send invites to people you are acquainted with.

**3) How long is the referral link valid for?**
        Your referral links are valid for a period of 60 days from the time you share them. As long as a non-member joins V-Wallet through your link, and successfully completes their first purchase, you will receive a referral bonus. After the order ships, the Gift Card will take 30 days to appear in your account

**4)Is there a limit on the number of email invites I can send?**
 &gt;Yes, you can send up to 100 e-mail invites.

**Text and Email Notifications**
**1) What are V-Wallet email and text alerts?**
 &gt;These are notifications about updates to your account or confirmations of payments or changes that are sent to you by email and/or text message.

**2) What type of information can I receive with V-Wallet alerts?**
        &gt;There are three categories of alerts that you can choose to receive:
                &gt;Bill ready
                &gt;Payment confirmations (includes when payments and credits are applied)
                &gt;Account activity confirmations (includes when plan and features changes are made)

Go to the Email and Text Alert Preferences page in V-Wallet for more details on these categories.

**3) Can I set multiple email addresses to receive alerts about my account?**
        &gt;No, you cannot enter multiple email addresses to receive email alerts in V-Wallet.

**4) I&#39;m no longer a V-Wallet customer, but I&#39;m still receiving notifications by text and/or email. Why is this?**
        &gt;If you&#39;ve cancelled your service but are still receiving alerts, this could mean that you have an outstanding payment balance. Contact Customer Service.

**Account ID, Secure ID,Passwords and Security Questions**

**1) Is my credit/debit card, net banking information secure on VWallet?**
        &gt;V-Wallet uses Verisign certified 128-bit encryption technology and is PCI-DSS (Payment Card Industry Data Security Standard) certified to ensure the security of your credit/debit card, net banking information at all times. No matter what the transaction, no one can access the information but you. So… yes your information is probably more secure on V-Wallet than it is anywhere else!

**2) Why am I receiving the new device alert even if I have used the device before?**
        &gt;We might send you an alert for a device that you&#39;ve signed in from before. This can happen if you&#39;ve recently deleted cookies, updated your browser or an app, or used incognito mode to log in to your V-Wallet account.

**3) When does one get these new device alerts?**
 &gt;This alert can be sent in case you have signed in for the first time on a new computer, phone or browser, when you use your browser&#39;s incognito or private browsing mode or clear your cookies, or when somebody else is accessing your account.

**4) My account was just locked because of a suspicious activity. What should I do?**
 &gt; If you have received an email or sms from V-Wallet stating that your account has been locked because of security reasons , we strongly recommend that you change your password. Changing password will secure your account and log you out from all other platforms where you are logged in with your V-Wallet account.

**5) I am unable to access my account because of security reasons, what should I do?**
 &gt; In such cases, please contact [care@vwallet.in](mailto:care@vwallet.in)

**6) I am receiving many alerts. What should I do ?**
 &gt;In  such a scenario, we recommend that you write to [care@vwallet.in](mailto:care@vwallet.in) . We will help you resolve your issue.

**7) I am receiving many alerts. What should I do ?**
 &gt;In  such a scenario, we recommend that you write to [care@vwallet.in](mailto:care@vwallet.in) . We will help you resolve your issue.

**General queries**
**1) How many prepaid accounts can I have?**
        &gt;Each user can have only one prepaid account:It&#39;s an RBI policy.

**2) How do I set a particular account as my default account?**
        &gt;Tap the app drawer on the top right corner of your app screen and go to &#39;Accounts &amp; Profiles&#39;. Once there, simply select an account as your default account by tapping on it anywhere in the row.

**3) How do I know about the new offers from V-Wallet?**
  **&gt;** Please follow us on our official Facebook ( [www.facebook.com/V-Wallet\*\*](http://www.facebook.com/V-Wallet**)) and Twitter ( [https://twitter.com/V-Wallet\*\*](https://twitter.com/V-Wallet**)) page for new offers.

**4) Other than care@vwallet.in is there any other way to contact you?**
 &gt;Yes, you can reach us through Facebook and Twitter.

**Customer FAQ&#39;s**
**1) How do I protect my V-Wallet account?**
 &gt;DO NOT SHARE your V-Wallet Password with anyone. Still if you feel, your V-Wallet password has been compromised at any time, please write to us at [care@vwallet.in](mailto:care@vwallet.in)  or simply SMS RESET to 987\*\*\*. You will receive a new password on your registered mobile number, which you can change to your liking.

Please Note: We never ask for your password when you call us.

**2) Do I need to maintain a minimum balance in my V-Wallet?**
        &gt;No. You can add as much money as you wish to and use it the way you want. After all it&#39;s yours! We just help you better manage it and pay the smarter way.

**3) I forgot my vPIN, what should I do?**
 &gt; Please write to us at [care@vwallet.in](mailto:care@vwallet.in)  with transaction details (Account email of FC GO card, Merchant Name, Mobile number). We will investigate and get back to you.

**4) Who is eligible to hold a V-Wallet account?**
 &gt; Any person aged above 10 years is eligible to hold a V-Wallet account.

**5) Is my money safe if I lose my phone?**
 &gt; Even if you lose your phone, your money is 100% safe. Nobody can access your V-Wallet account, since only you can do so through your password and 4-digit vPIN. V-Wallet locks your account for your own safety after 3 consecutive incorrect vPIN attempts. If you suspect unauthorized activity in your account, you can also suspend your V-Wallet account and block payments by calling our customer service at 1800-891-9999 (toll-free) or write to [care@vwallet.in](mailto:care@vwallet.in) .

**6) I mistakenly recharged on wrong number! Can I get my money back?**
 &gt; If you accidentally recharged to an incorrect mobile number, then please contact our customer care team &amp; the money will be sent back to your account. However if you send money to a correct but unknown person&#39;s number, not much can be done about it.

**7) How a Merchant is different from a Retail Outlet?**
        &gt;Retail Outlet (RO) is a business partner who does Recharges/Bill payments/Money transfers for their customers paying in cash, using a prepaid trading advance account of V-Wallet. But a Merchant could be any business who registers and uses V-Wallet to accept payments from his/customers. It could be a grocery/Kirana store, a pharmacy, a doctor clinic, a hawker or a service provider selling goods or services.

**8) How do I use Chat &amp; Pay?**
        &gt;It&#39;s simple. In the latest V-Wallet app, just click on the chat icon/second tab and select a contact from your contact list