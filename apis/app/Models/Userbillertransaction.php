<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Dec 2017 07:38:37 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userbillertransaction
 * 
 * @property int $UserBillerTransid
 * @property int $UserWalletID
 * @property int $WalletAccountId
 * @property string $BillerCategory
 * @property string $Status
 * @property int $TransactionDetailID
 * @property int $PaymentProductID
 * @property int $BillDeskTransactionID
 * @property string $ValidationID
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \Illuminate\Database\Eloquent\Collection $userbillertransdetails
 *
 * @package App\Models
 */
class Userbillertransaction extends Eloquent
{
    protected  $table = 'userbillertransactions';
	protected $primaryKey = 'UserBillerTransid';
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'WalletAccountId' => 'int',
		'TransactionDetailID' => 'int',
		'PaymentProductID' => 'int',
		'BillDeskTransactionID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'UserWalletID',
		'WalletAccountId',
		'BillerCategory',
		'Status',
		'TransactionDetailID',
		'PaymentProductID',
		'BillDeskTransactionID',
		'ValidationID',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedBy',
		'UpdatedDatetime'
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID')
					->where('transactiondetail.TransactionDetailID', '=', 'userbillertransactions.TransactionDetailID')
					->where('transactiondetail.PaymentsProductID', '=', 'userbillertransactions.PaymentProductID');
	}

	public function userbillertransdetails()
	{
		return $this->hasMany(\App\Models\Userbillertransdetail::class, 'UserBillerTransrid');
	}
}
