<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

// Default route
$app->get('/', function () use ($app) {
    return '<h1 style="text-align:center">Wellcome To Viola Wallet</h1>';
});

/*
  |--------------------------------------------------------------------------
  | Login & Registration Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('registration', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@registrationOne' ]);
$app->post('registrationVerify', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@registrationTwo' ]);
$app->post('regAadhaar', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@regAadhaar' ]);
$app->post('regAadhaarVerify', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@regAadhaarVerify' ]);
$app->post('registerVpa', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@regVpa' ]);
$app->post('submitkyc', 'User\RegisterController@kycDocumentUpload');
$app->post('checkKycStatus', 'User\RegisterController@checkKycStatus');
//$app->post('checkEmail', 'User\RegisterController@checkEmail');
//$app->post('checkMobile', 'User\RegisterController@checkMobile');
$app->get('referralVerify', [ 'middleware' => 'headers',
    'as'         => 'referralVerify', 'uses'       => 'User\RegisterController@referralVerify'
]);
$app->post('resendOtp', [ 'middleware' => 'headers', 'uses' => 'User\LoginController@resendOtp' ]);
$app->post('forgotPassword', [ 'middleware' => 'headers', 'uses' => 'User\ForgotPasswordController@mobileValidate' ]);
$app->post('otpValidate', [ 'middleware' => 'headers', 'uses' => 'User\LoginController@validOtp' ]);
$app->post('passwordChange', [ 'middleware' => 'headers', 'uses' => 'User\ForgotPasswordController@vPinChange' ]);
$app->post('login', [ 'middleware' => 'headers', 'uses' => 'User\LoginController@checkLogin' ]);
$app->post('completeSocialRegistration', [ 'middleware' => 'headers', 'uses' => 'User\LoginController@completeSocialRegistration' ]);
$app->post('logout', [ 'middleware' => 'headers', 'uses' => 'User\LogOutController@partyLogout' ]);

/*
  |--------------------------------------------------------------------------
  | My Account Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('userInfo', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@profileDetails' ]);
$app->post('userInfoUpdate', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@updateUserProfile' ]);
//$app->post('changeMobileNumber', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@updateMobileNumber' ]);
//$app->post('changeMobileSendOtp', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@changeMobileResendOtp' ]);
//$app->post('validateUpdateMobile', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@validateUpdateMobile' ]);
$app->post('changeEmailId', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@changeUniqueValue' ]);
$app->post('changePassword', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@changeSecureKeys' ]);
$app->post('changeVpin', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@changeSecureKeys' ]);
$app->post('updateTranVpinLimit', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@updateTranVpinLimit' ]);
$app->post('getUserBalance', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@userBalance' ]);
$app->get('nomineInfo', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@nomineDetails' ]);
$app->post('balancePreference', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@balanceViewPrefarence' ]);
$app->post('fastTrackUpdate', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@fastTrackSetting' ]);
$app->post('validateVpin', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@validateVpin' ]);
$app->post('profileUpdate', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@updateProfileImage' ]);
$app->post('getProfileImage', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@getProfileImage' ]);
$app->post('preferenceProfileImage', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@preferenceProfileImage' ]);
$app->post('deleteProfileImage', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@deleteProfileImage' ]);
// user configuration rules
$app->post('UpdatePreferences', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@UpdatePreferences' ]);
$app->post('viewPreferences', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@MyPreferences' ]);
// Card Management
$app->post('saveCardDetails', [ 'middleware' => 'headers', 'uses' => 'User\PaymentCardController@saveCardDetails' ]);
$app->post('deleteCardDetails', [ 'middleware' => 'headers', 'uses' => 'User\PaymentCardController@deleteCardDetails' ]);
$app->post('listSavedcards', [ 'middleware' => 'headers', 'uses' => 'User\PaymentCardController@listSavedcards' ]);
$app->post('userFeedBackUpdate', [ 'middleware' => 'headers', 'uses' => 'Feedback\FeedbackController@updateFeedback' ]);

/*
  |--------------------------------------------------------------------------
  | Beneficiary Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('addBeneficiary', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@addBeneficiary' ]);
$app->post('editBeneficiary', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@editBeneficiary' ]);
$app->post('deleteBeneficiary', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@deleteBeneficiary' ]);
$app->post('getBeneficiaryList', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\BeneficiaryController@getBeneficiaryList' ]);
$app->post('vpaListUpdate', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@vpaListUpdate' ]);
$app->post('fetchBenificiary', 'ManageMyBeneficiary\AddBeneficiaryController@fetchBenificiary');
$app->post('checkUserWithUsOrNot', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\BeneficiaryController@checkUserWithUsOrNot' ]);
$app->post('delecteWallet', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\BeneficiaryController@deleteWallet' ]);
/*
  |--------------------------------------------------------------------------
  | Add Money to Wallet (Topup) Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('topup', [ 'middleware' => 'headers', 'uses' => 'Topup\TopUpController@addMoney' ]);
$app->post('addTopupUPI', [ 'middleware' => 'headers', 'uses' => 'Topup\TopUpUPIController@addTopUpUPI' ]);
//$app->post('addMoneyTopup', [ 'middleware' => 'headers', 'uses' => 'Topup\AddMoneyTopup@addMoney' ]);
$app->post('refundInitiation', 'Refunds\RefundsController@refundInitiation');
$app->post('updateTopupUPITransaction', [ 'middleware' => 'headers', 'uses' => 'Topup\TopUpController@updateUpiTransaction' ]);
$app->post('walletsAndSavedCardsList', [ 'middleware' => 'headers', 'uses' => 'Topup\AddMoneyTopup@walletsAndSavedCardsList' ]);
$app->post('listwallets', [ 'middleware' => 'headers', 'uses' => 'Topup\AddMoneyTopup@listWallets' ]);

/*
  |--------------------------------------------------------------------------
  | Kotak Bank (CCAvenue) API integration
  |--------------------------------------------------------------------------
  |
 */
$app->post('paymentNetbankOptions', [ 'middleware' => 'headers', 'uses' => 'Payments\CcavenueController@paymentNetbankOptions' ]);
$app->post('paymentResponse', 'Payments\CcavenuResponseController@paymentResponse');
$app->get('transctionComplete', function() {
    
});
$app->post('kotakWebTransaction', 'Payments\CcavenueMerchantController@kotakWebTransaction');
$app->post('refundBillDesk', 'Cron\CronJobRefundController@refundBillDesk');

/*
  |--------------------------------------------------------------------------
  | Transction Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('transactionList', [ 'middleware' => 'headers', 'uses' => 'Transactions\TransactionsController@transactionList' ]);
$app->get('notificationsList', [ 'middleware' => 'headers', 'uses' => 'Support\NotificationsController@getNotifcationsPushNotif' ]);
$app->post('getTransactionWalletData', [ 'middleware' => 'headers', 'uses' => 'Transactions\TransactionsController@getTransactionWalletData' ]);
$app->post('getSecurityQuestionList', [ 'middleware' => 'headers', 'uses' => 'Party\SecurityQuestionController@getSecurityQuestionList' ]);
$app->post('getPartySecurityList', [ 'middleware' => 'headers', 'uses' => 'Party\PartySecurityController@getPartySecurityList' ]);
$app->post('addPartySecurity', [ 'middleware' => 'headers', 'uses' => 'Party\AddPartySecurityController@addPartySecurity' ]);
$app->post('addToTripleClick', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@addBeneficiaryToTripleClick' ]);

/*
  |--------------------------------------------------------------------------
  | Notification Services
  |--------------------------------------------------------------------------
  |
 */
$app->get('notificationsCount', [ 'middleware' => 'headers', 'uses' => 'Support\NotificationsController@notificationsCount' ]);
$app->post('updateNotificationStatus', [ 'middleware' => 'headers', 'uses' => 'Support\NotificationsController@updateNotificationStatus' ]);

/*
  |--------------------------------------------------------------------------
  | Support (Contact Us) Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('ContactusInsertBeforeLogin', 'Support\ContactusController@ContactusInsert');
$app->post('ContactusInsertAfterLogin', 'Support\ContactusController@ContactusInsert');

$app->post('UpdateCrmCustomerRecord', 'Support\ContactusController@UpdateUserStatusCrm');

/*
  |--------------------------------------------------------------------------
  | Triple Click Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('intiateTripleClickTransction', [ 'middleware' => 'headers', 'uses' => 'Tripleclick\TripleclickController@intiateTransction' ]);
$app->post('updateTripleClikAmount', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\AddBeneficiaryController@updateTripleClikAmount' ]);
$app->post('getFeePlans', [ 'middleware' => 'headers', 'uses' => 'Transactions\BusinessController@getFeePlans' ]);



/*
  |--------------------------------------------------------------------------
  | Send money using UPI services
  |--------------------------------------------------------------------------
  |
 */
$app->post('sendMoneyUPI', [ 'middleware' => 'headers', 'uses' => 'Pays\SendMoneyUPIController@sendMoneyUsingUPI' ]);
$app->post('updatesendMoneyUPI', [ 'middleware' => 'headers', 'uses' => 'Pays\SendMoneyUPIController@updatesendMoneyUPIStatus' ]);

/*
  |--------------------------------------------------------------------------
  | Send money pay w2w Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('sendMoneyW2W_W2B', 'Pays\SendMoneyController@sendMoneyWB');


$app->post('sendMoneywalletToBank', [ 'middleware' => 'headers', 'uses' => 'Pays\SendMoneyToBankController@sendMoneyWB' ]);
$app->get('pincodeDetails', [ 'middleware' => 'headers', 'uses' => 'User\PincodeDetailsController@getPincodeDetails' ]);
$app->get('getIfscBankDetails', [ 'middleware' => 'headers', 'uses' => 'ManageMyBeneficiary\BeneficiaryController@getIfscBankDetails' ]);
$app->post('createQrCode', [ 'middleware' => 'headers', 'uses' => 'QrManagement\QrController@createQrCode' ]);
$app->post('scanQrCode', [ 'middleware' => 'headers', 'uses' => 'QrManagement\QrController@scanQrCode' ]);
$app->post('sendMoneyQrCode', [ 'middleware' => 'headers', 'uses' => 'QrManagement\QrController@sendMoneyQrCode' ]);
$app->post('viewQrTransactions', [ 'middleware' => 'headers', 'uses' => 'QrManagement\QrController@viewQrTransactions' ]);

/*
  |--------------------------------------------------------------------------
  | Log Monitoring Service
  |--------------------------------------------------------------------------
  |
 */
$app->group([ 'namespace' => '\Rap2hpoutre\LaravelLogViewer' ], function() use ($app) {
    $app->get('logs', 'LogViewerController@index');
});

/*
  |--------------------------------------------------------------------------
  | Content Management System Service
  |--------------------------------------------------------------------------
  |
 */
$app->get('getTermsandConditions', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getTermsandConditions' ]);
$app->get('getAntiBriberyandCorruptionPolicy', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getAntiBriberyandCorruptionPolicy' ]);
$app->get('getCoockiePolicy', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getCoockiePolicy' ]);
$app->get('getPrivacyPolicy', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getPrivacyPolicy' ]);
$app->get('getRefundPolicy', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getRefundPolicy' ]);
$app->get('getRiskPolicy', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getRiskPolicy' ]);
$app->get('getAMLPoliciesandProcedures', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getAMLPoliciesandProcedures' ]);
$app->get('getAMLPoliciesandProceduresBase', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getAMLPoliciesandProceduresBase' ]);
$app->get('getAMLPoliciesandProceduresDynamic', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getAMLPoliciesandProceduresDynamic' ]);
$app->post('getFaqs', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getFaqs' ]);
$app->post('getWebFaqs', [ 'middleware' => 'headers', 'uses' => 'StaticContent\StaticContentController@getWebFaqs' ]);
$app->post('createPage', [ 'middleware' => 'headers', 'uses' => 'StaticContent\PagesController@createPage' ]);
$app->post('getPage', [ 'middleware' => 'headers', 'uses' => 'StaticContent\PagesController@getPage' ]);
$app->post('updatePage', [ 'middleware' => 'headers', 'uses' => 'StaticContent\PagesController@updatePage' ]);
$app->post('deletePage', [ 'middleware' => 'headers', 'uses' => 'StaticContent\PagesController@deletePage' ]);
$app->post('createBanner', [ 'middleware' => 'headers', 'uses' => 'StaticContent\BannersController@createBanner' ]);
$app->post('getBanner', [ 'middleware' => 'headers', 'uses' => 'StaticContent\BannersController@getBanner' ]);
$app->post('updateBanner', [ 'middleware' => 'headers', 'uses' => 'StaticContent\BannersController@updateBanner' ]);
$app->post('deleteBanner', [ 'middleware' => 'headers', 'uses' => 'StaticContent\BannersController@deleteBanner' ]);
$app->get('listFees', [ 'middleware' => 'headers', 'uses' => 'Payments\FeeParamsController@getFeesDetails' ]);
$app->get('monthlyAggregrate', [ 'middleware' => 'headers', 'uses' => 'Payments\FeeParamsController@monthlyAggregate' ]);

/*
  |--------------------------------------------------------------------------
  | Bill Desk Payment Services
  |--------------------------------------------------------------------------
  |
 */
// recharges
$app->post('insertBillerOffers', 'BillDesk\BillerOffersFillerController@inserBillersByCircle');
$app->post('getJioPlans', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@getJioPlans' ]);
$app->get('planDetails/{circleId}', 'BillDesk\BillDeskController@planDetails');
$app->get('operatorList', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@operatorsList' ]);
$app->post('circlesList', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@circlesList' ]);
$app->post('planscategoriesList', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@planCategoriesList' ]);
$app->post('getoperatorByMobile', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@getoperatorByMobilenumber' ]);
$app->post('plansDetailsUserList', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@getPlanListUserData' ]);
$app->post('rechargeValidate', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@rechargeValidate' ]);
$app->post('rechargePayment', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@rechargePayment' ]);
// $app->post('rechargeQuery', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@rechargeQuery' ]);
// bill payments
// $app->post('rechargeBilldesk', 'BillDesk\BillDeskRecharge@BillDeskRecharge');
$app->post('getBillerDetailsBillpayments', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@getBillersPostBillpay' ]);
$app->get('getBillerDropdownLookup', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskUserApisController@getBillersDropdownData' ]);
$app->post('getBillpaymentDetails', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@getBillPaymentDetails' ]);
$app->post('makeBillpayment', [ 'middleware' => 'headers', 'uses' => 'BillDesk\BillDeskController@makeBillpayment' ]);
$app->get('genReconFile', 'BillDesk\BillDeskReconController@generateReconFile');

/*
  |--------------------------------------------------------------------------
  | CRM (SugarCRM) Integration Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('documentUpload', 'Ccteam\UploadController@ImgUpload');
$app->get('crmActregDet', 'Ccteam\UserProfileController@actregDetail');
$app->get('crmLeadregDet', 'Ccteam\UserProfileController@leadregDetail');
//$app->post('crmforgotVpin', 'Ccteam\UserAuthenticateController@forgotVpin');
//$app->post('crmchangeVpin', 'Ccteam\UserAuthenticateController@changeVpin');
//$app->post('crmsetVpin', 'Ccteam\UserAuthenticateController@setPin');
$app->get('crmLoghistory', 'Ccteam\UserAuthenticateController@LoginHistory');
$app->get('crmLoghisEveDet', 'Ccteam\UserAuthenticateController@LogHisEventDet');
$app->get('crmwalletAccounts', 'Ccteam\UserProfileController@walletAccountsList');
$app->get('crmtransactionsList', 'Ccteam\TransactionsController@listTransactions');
$app->get('crmKycDocsList', 'Ccteam\UserProfileController@getKycDocuments');
$app->post('crmDeleteKyc', 'Ccteam\UserProfileController@deleteKycDocuments');
$app->post('crmChangeUserStatus', 'Ccteam\UserProfileController@updateUserStatus');
$app->post('crmChangeKycStatus', 'Ccteam\UserProfileController@ApproveKycDocuments');

/*
  |--------------------------------------------------------------------------
  | Yes Bank Integration Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('mePayServerReq', 'Yesbank\YesbankController@mePayServerReq');
$app->post('meTransCollectSvc', 'Yesbank\YesbankController@meTransCollectSvc');
$app->post('transactionStatusQuery', 'Yesbank\YesbankController@transactionStatusQuery');
$app->post('meRefundServerReq', 'Yesbank\YesbankController@meRefundServerReq');
$app->post('checkVirtualAddressME', 'Yesbank\YesbankController@checkVirtualAddressME');

$app->post('testbilldetails', 'BillDesk\TestbillsData@billDetails');

/*
  |--------------------------------------------------------------------------
  | Offers & Promotional Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('getPromotionalOffers', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@getPromotionalOffers' ]);
$app->post('getBenefits', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@getBenefitsData' ]);
$app->post('verifyPromoCode', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@verifyPromoCode' ]);
$app->post('getReference', [ 'middleware' => 'headers', 'uses' => 'Offers\ReferenceController@getReferenceData' ]);
// $app->post('getSuperCash', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@getBeniftTrack' ]);
$app->post('getSuperCash', 'Offers\PromotionalOffersController@getBeniftTrack');
$app->post('getPromoTermsElegibility', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@getPromoTermsElegibility' ]);
$app->post('saveFavouritePromoCode', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@favouritePromoCode' ]);
$app->post('getFavouritePromos', [ 'middleware' => 'headers', 'uses' => 'Offers\PromotionalOffersController@getFavouritePromos' ]);
//$app->post('getPromoTermsElegibility', 'Offers\PromotionalOffersController@getPromoTermsElegibility');

/*
  |--------------------------------------------------------------------------
  | CronJob
  |--------------------------------------------------------------------------
  |
 */
// $app->post('addcashback', 'Cron\CronJobController@cronAddCashback');
$app->get('cronCashback', 'Cron\CronJobController@cronCashBack');
$app->get('updateExpiryAmount', 'Cron\CronJobController@updateExpiryAmount');
$app->get('findBill', 'Reminder\BillController@getBillPayment');
$app->get('remindBillPayments', 'Cron\RemindersCronJob@reminderCronJob');

/*
  |--------------------------------------------------------------------------
  | IVR Services
  |--------------------------------------------------------------------------
  |
 */
$app->post('ivrUserValidate', 'Ivr\ProfileController@validateUser');
$app->post('ivrGetUserBalance', 'Ivr\ProfileController@getUserBalance');
$app->post('ivrResetPassword', 'Ivr\ProfileController@forgotPassword');
$app->post('ivrRecentTransctions', 'Ivr\TransctionController@recentTransction');
$app->post('ivrRecentDeposits', 'Ivr\TransctionController@recentDeposits');
$app->post('ivrRecentWithdrawal', 'Ivr\TransctionController@recentWithdrawal');
$app->post('validateHash', [ 'middleware' => 'headers', 'uses' => 'Ivr\ProfileController@validateHashKey' ]);
$app->post('valodateOtpIvr', 'Ivr\ProfileController@validateHashOtp');

/*
  |--------------------------------------------------------------------------
  | Money Exchange Services Services (Prototype project)
  |--------------------------------------------------------------------------
  |
 */
$app->post('getRate', 'Payments\ExchangeMoneyController@getRate');
$app->post('getCurrencyExchange', 'Payments\ExchangeMoneyController@buySellCurrency');
$app->post('getNewUserBalance', 'User\MyAccountController@userBalance');
$app->post('addNewBeneficiary', 'ManageMyBeneficiary\AddBeneficiaryController@addBeneficiary');
$app->post('deleteNewBeneficiary', 'ManageMyBeneficiary\AddBeneficiaryController@deleteBeneficiary');
$app->post('getNewBeneficiaryList', 'ManageMyBeneficiary\BeneficiaryController@getBeneficiaryList');
$app->post('getNewTransactionsList', 'Transactions\TransactionsController@getTransactionsList');
$app->post('sendMoneywalletToCard', 'Pays\SendMoneyToCardController@sendMoneyWC');
$app->post('reciveMoneyApi', 'Pays\SendMoneyToCardController@reviceMoneyCW');

/*
  |--------------------------------------------------------------------------
  | Viola Chat Services Services
  |--------------------------------------------------------------------------
  |
 */
//$app->post('checkEmail', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@checkEmail' ]);
//$app->post('verifyAuthToken', [ 'middleware' => 'headers', 'uses' => 'User\RegisterController@verifyService' ]);





/*
  |--------------------------------------------------------------------------
  | Reminder services
  |--------------------------------------------------------------------------
  |
 */
$app->post('getReminders', [ 'middleware' => 'headers', 'uses' => 'Reminder\ReminderController@getUserReminders' ]);
$app->post('createReminder', [ 'middleware' => 'headers', 'uses' => 'Reminder\ReminderController@createReminder' ]);
$app->post('updateReminder', [ 'middleware' => 'headers', 'uses' => 'Reminder\ReminderController@updateReminder' ]);
$app->post('deleteReminder', [ 'middleware' => 'headers', 'uses' => 'Reminder\ReminderController@deleteReminder' ]);
$app->post('addReminderTransaction', [ 'middleware' => 'headers', 'uses' => 'Reminder\ReminderController@addReminderTransactions' ]);




/*
  |--------------------------------------------------------------------------
  | Code need to clean up
  |--------------------------------------------------------------------------
  |
 */

// test controller
//$app->post('countryCode', [ 'middleware' => 'headers', 'uses' => 'Test\TestController@countryCode' ]);
$app->post('testMethod', 'Test\TestController@testMethod');


// create methods for offerbenefittracker
$app->post('addBenefitTrack', 'Test\TestController@addbenefittrack');
$app->post('addrefferTrack', 'Test\TestController@addreferbenefittrack');
$app->post('referalcashback', 'Test\TestController@referalcashback');
// Bill Desk Key
//$app->get('billDeskKey', 'BillDesk\BillsController@billDesk');
//$app->get('testBills', 'BillDesk\Testbills@billDesk');
//$app->post('ValidateBillPay', 'BillDesk\Testbills@billpayValidate');
// Communication New 
$app->post('testab', 'Test\TestController@testAB');
$app->post('test1', 'Test\BladeController@test');
$app->post('test2', 'Test\BladeController@testBlade');
$app->post('test3', 'Test\BladeController@CommunicateTest');
$app->post('maas', 'Test\BladeController@testMaasRequest');

// *********************************************************************
// *********************************************************************
// *********************************************************************
// *********************************************************************
// *********************************************************************
// actual routes
$app->post('sendCommunication', [ 'middleware' => 'headers', 'uses' => 'Communication\SendMessageController@sendMsg' ]);
// End of the communication routes


/*
  |--------------------------------------------------------------------------
  | RedBus (SeatSeller) Services
  |--------------------------------------------------------------------------
  |
 */


$app->get('getCities', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getCities' ]);
$app->get('getCitiesStore', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getCitiesStore' ]);
$app->post('getBusList', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getBusList' ]);
$app->post('getBusLayout', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getBusLayout' ]);
$app->post('blockTicket', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@blockTicket' ]);
$app->post('confirmTicket', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@confirmTicket' ]);
$app->post('transactionInitiation', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@transactionInitiation' ]);
$app->post('confirmTicket', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@confirmTicket' ]);
$app->post('cancelTicketDetails', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@ticketCancellationCharges' ]);
$app->post('getBusbookingsList', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getBusbookings' ]);
$app->post('resendTicket', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@resendTicket' ]);
$app->post('cancelTicket', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@cancelTicket' ]);
$app->post('getBpDpList', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@getBpDpList' ]);
$app->post('blockTicket1', [ 'middleware' => 'headers', 'uses' => 'RedBus\SeatsellerController@blockTicket' ]);

$app->get('generate-pdf/{str}', 'RedBus\PdfRedbusTicketController@busTicketPdfDownload');

$app->post('YesbankUserBalanceCheck', [ 'middleware' => 'headers', 'uses' => 'User\MyAccountController@CheckYesBankWalletBalance' ]);


// Job for Billdesk To export and Create All Master Data
$app->get('uploadExcelSheet/{str}', 'BilldeskBillersExportJobs\BilldeskBillpaymentsExport@readExcelsheet');

/*
  |--------------------------------------------------------------------------
  | Wallet Configration
  |--------------------------------------------------------------------------
  |
 */

$app->get('walletPlanConfiguration', 'WalletConfiguration\WalletConfigController@setPlanConfigs');

/* End of file routes.php */ /* Location: ./Http/routes.php */