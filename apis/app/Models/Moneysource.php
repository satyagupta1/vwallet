<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Moneysource
 * 
 * @property string $MoneySourcesCode
 * @property string $SourceDescription
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partypreferredpaymentmethods
 * @property \Illuminate\Database\Eloquent\Collection $walletrecharges
 *
 * @package App\Models
 */
class Moneysource extends Eloquent
{
	protected $primaryKey = 'MoneySourcesCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'SourceDescription',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function partypreferredpaymentmethods()
	{
		return $this->hasMany(\App\Models\Partypreferredpaymentmethod::class, 'MoneySourceCode');
	}

	public function walletrecharges()
	{
		return $this->hasMany(\App\Models\Walletrecharge::class, 'MoneySourceCode');
	}
}
