<?php
 namespace App\Libraries\Communication\Push\Android;

    class PushNowA {

      static protected $keys = array('V-Card' => 'AIzaSyA2Usjt43dSpbRpFpzHr-h6BiHmBZMPN0Q', 'V-Pay' => '', 'V-Wallet' => 'AAAAS_SmiNU:APA91bEtxylxjRUQq0mWVQ3u8i5g3JL_fRKvLmHeRB0nHFyImHfl4b50UP00RSjK2Pm8PjNBAHX0oakgdZeiGfhDaCApN4fzS_H5xetqgjzKCig33pTHH5cag8sL-bzW7Xywdd6lZ2bf');

      public static function sendPush($pushData){
          
        $url = 'https://fcm.googleapis.com/fcm/send';

        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = PushNowA::$keys{$pushData['applicationNameKey']};
        
        $fields = array();
        $fields['notification'] = array(
          'body' => $pushData['message'],
          'title' => $pushData['application_name'],
          'icon' => 'https://cdn1.iconfinder.com/data/icons/business-and-finance-20/200/vector_65_07-64.png',
          'click_action' => '',
          'sound' => 'default',
          'color' => '',
          'tag' => '',
         );
        $fields['to'] = $pushData['deviceToken'];
        $fields['priority'] = 'normal';

        //header with content_type api key
        $headers = array(
         'Content-Type:application/json',
        'Authorization:key='.$server_key
        );
       
        //CURL request to route notification to FCM connection server (provided by Google)			
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        if ($result === FALSE) {
          return 'Oops! FCM Send Error: ' . curl_error($ch);
        }
      
        curl_close($ch);
        return $result;
        }
      }