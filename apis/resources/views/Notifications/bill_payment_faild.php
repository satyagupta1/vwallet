<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Recharge Fails
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction Failed',
        'text'    => $salutationName.',<br/><br/>
                        
This is to let you know that your bill payment for INR '.$amount.' could not be confirmed. We apologise for the inconvenience caused to you. Please note your Transaction ID: '.$transactionid.' for reference. Use this for all future communication related to this transaction.
In case money has been deducted from your wallet , rest assured that your money is safe and will be refunded automatically and should reflect in your wallet/card/account within 10 days depending on your bank.',
    ],
    'sms'   => [
        'text' => 'Your payment of INR '.$amount.' for '.$billerName.' did not go through. Time: '.$date . ' ' . $time.', TranID: '.$transactionid.'.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Bill Payment for '.$billerName.' is failed.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Bill Payment Failed.',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
