<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Transaction details when payment done through Bar code (Merchant and Viola User) template  
 *  param $cahsbackPercentage numeric 
 *  param $posMerchantName varchar 
 *  param $amount varchar  
 *  param $tracnsactionId varchar  
 */

$array = [
    'email' => [
        'subject' => '',
        'text'    => ''
    ],
    'sms'   => [
        'text' => '
            Received Rs.'.$amount.' from <'.$posMerchantName.' in your ViolaWallet. 
                Transaction ID: '.$tracnsactionId.' Bar code to Pay at the merchant name and receive 
                '.$cahsbackPercentage.'% cashback.'],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file pos_transaction_done_bar_code.php */
