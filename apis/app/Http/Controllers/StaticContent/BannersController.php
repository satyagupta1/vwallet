<?php

namespace App\Http\Controllers\StaticContent;


/**
 * BannersController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class BannersController extends \App\Http\Controllers\Controller {  
    
    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');         
        $this->lang = app('translator')->getLocale();
        $this->createdBy = config('vwallet.adminTypeKey');       
        
    }
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function createBanner(\Illuminate\Http\Request $request)
    {
        
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        
        $checkValidate = $this->_validateBanner($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        //$bannerImage = '';
        if ( $request->input('bannerImage') !== NULL )
        {
            $bannerImage = $this->_uploadBanner($request);
            $bannerImage = $bannerImage['bannerImage'];
        }
        $date_added                        = date('Y-m-d H:i:s');
        $data                              = array(
                        'Title' => $request->input('title'),
                        'Category' => $request->input('category'),
                        'Content' => $request->input('content'),
                        'Link' => $request->input('link'),
                        'Active' => 'Y',            
                        'BannerPath' => $bannerImage,            
                        'CreatedDateTime' => $date_added,
                        'UpdatedDateTime' => $date_added,
                        'CreatedBy'       => $this->createdBy,
                        'UpdatedBy'       => $this->createdBy
                    );
        
        $bannerInsert  = \App\Models\Banner::create($data);   
       
        
        $attrBanner = trans('messages.attributes.banner');
        $message = trans('messages.insertedSuccessfully', ['name' => $attrBanner]);
        
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
    /*
     *  uploadBanner - upload banner Image
     *  @param Request $request
     *  @return string
     */
    private function _uploadBanner($request)
    {
        $attachment_url = NULL;
        \Intervention\Image\ImageManagerStatic::configure([ 'driver' => 'gd' ]);
        $imageType      = $request->input('bannerImageType');

        // upload image
        $image = $request->input('bannerImage');
        if ( ! empty($image) )
        {
            $fileName = md5(time()) . '.' . $imageType;
            $bannersPath     = config('vwallet.BANNERS_PATH');
            $NewPath  = \App\Libraries\Helpers::publicPath($bannersPath);
            $NPath    = $NewPath . '/' . $fileName;

            if ( ! is_dir($NewPath) )
            {
                mkdir($NewPath, 0777, TRUE);
            }

            $is_saved = \Intervention\Image\ImageManagerStatic::make(base64_decode($image))->save($NPath);
            if ( $is_saved )
            {
                $attachment_url['oldBannerImage'] = NULL;                
             /*  
                // check info exsist 
                $configId        = \App\Models\Userwallet::select(array( 'UserWalletID', 'FirstName', 'ProfileImagePath' ))->where('UserWalletID', $partyId)->first();
                if ( $configId->ProfileImagePath != NULL )
                {
                   $attachment_url['oldProfileImage'] = $configId->ProfileImagePath;
                } 
                // update profile Image
                \App\Models\Userwallet::where('UserWalletID', $partyId)->update($userConfigArray);             
               */
                $userConfigArray['UpdatedDateTime'] = date('Y-m-d H:i:s');
                $userConfigArray['UpdatedBy']       = 'User';
                $userConfigArray['BannerImagePath']       = $fileName;
                
                $attachment_url['bannerImage'] = $fileName;
            }
        }
        return $attachment_url;
    }

    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function getBanner(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateFields($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $bannerId          = $request->input('bannerId');
        $status          = $request->input('status');
        $orderBy         = $request->input('orderBy');
        $orderByColumn   = $request->input('orderColumn');
        $limit           = $request->input('limit');
        $paginate        = $request->input('paginate');

        $selectColumns = array(
            'BannerId',
            'Title',
            'Category',
            'Content',
            'BannerPath',
            'Link',
            'Active',
            'CreatedDateTime',
            'UpdatedDateTime',
        );

        
        $query = \App\Models\Banner::select($selectColumns);
        //filter by security question type
        if ( $bannerId !== NULL )
        {
            $query->where('BannerId', '=', $bannerId);
        }

        //filter by triple click
        if ( $status !== NULL )
        {
            $query->where('Active', '=', $status);
        }


        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectColumns) )
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('BannerId', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $finalRecords = $query->paginate($this->perpage)->toArray();
            $countResults = $finalRecords['total'];
        }
        else
        {
            $finalRecords = $query->get()->toArray();
            $countResults = count($finalRecords);
        }

        if ( $countResults > 0 )
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
            $output_arr['res_data']    = $finalRecords;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);       
    }
    
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function updateBanner(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
       
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        $checkValidate = $this->_validateUpdateBanner($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $date_added                        = date('Y-m-d H:i:s');   
        
        $bannerInsert = \App\Models\Banner::find($request->input('bannerId'));
        if(!$bannerInsert) {
           $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
           return response()->json($output_arr);
        }
        $bannerImage = '';
        if ( $request->input('bannerImage') !== NULL )
        {
            $bannerImage = $this->_uploadBanner($request);
            if ( $bannerImage != NULL )
            {
                $bannerUrl              = \App\Libraries\Helpers::bannerUrl();
                $bannerImage  = $bannerImage['bannerImage'];
               
            }
        }
        $bannerInsert->Title = $request->input('title');
        $bannerInsert->Category = $request->input('category');
        $bannerInsert->Content = $request->input('content');
        $bannerInsert->Link = $request->input('link');
        if($bannerImage)
        {
            $bannerInsert->BannerPath = $bannerImage;
        }
        $bannerInsert->UpdatedDateTime       = $date_added;
        $bannerInsert->save();
        
        $attrBanner = trans('messages.attributes.banner');
        $message = trans('messages.updatedSuccessfully', ['name' => $attrBanner]);
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
    
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function deleteBanner(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        $checkValidate = $this->_validateDeleteBanner($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $dateAdded                        = date('Y-m-d H:i:s');
        $bannerInsert = \App\Models\Banner::find($request->input('bannerId'));
        if(!$bannerInsert) {
           $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
           return response()->json($output_arr);
        }       
        $bannerInsert->Active = 'N';
        $bannerInsert->UpdatedDateTime       = $dateAdded;
        $bannerInsert->save();
        
        $attrBanner = trans('messages.attributes.banner');
        $message = trans('messages.deletedSuccessfully', ['name' => $attrBanner]);
        
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
     /**
     * Validate insert page request data
     * @param $request
     * @return String
     */
    private function _validateBanner($request)
    {
        $messages = [];
        $validate = [
            //'partyId' => 'required|numeric',
              'title' =>'required',
              'content' =>'required',
              'category' =>'required',            
        ];
        
        $validate['bannerImage']     = 'required';
        $validate['bannerImageType'] = 'required|in:gif,jpeg,jpg,pjpeg,png,x-png';
        
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.  
       /*$messages = [
        'partyId.required' => trans($langfolder . '/validation.userId'),        
        ];
       */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
     /**
     * Validate update page request data
     * @param $request
     * @return String
     */
    private function _validateUpdateBanner($request)
    {
        $messages = [];
        $validate = [
              'bannerId' => 'required|numeric',
              'title' =>'required',
              'content' =>'required',
              'category' =>'required',
             // 'Active' => 'required|in:Active,Hide,Trash'
        ];
        if( $request->input('bannerImage') !== NULL )
        {
            $validate['bannerImage']     = 'required';
            $validate['bannerImageType'] = 'required|in:gif,jpeg,jpg,pjpeg,png,x-png';
        }
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.  
       /*$messages = [
        'partyId.required' => trans($langfolder . '/validation.userId'),        
        ];
       */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
     /**
     * Validate update page request data
     * @param $request
     * @return String
     */
    private function _validateDeleteBanner($request)
    {
        $messages = [];
        $validate = [
              'bannerId' => 'required|numeric',              
        ];
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    /**
     * Validate Form Fields Data
     * @param $request
     * @return String
     */
    private function _validateFields($request)
    {
        $validate = [];
        if ( $request->input('pageId') !== NULL )
        {
            $validate['questionCategory'] = 'required|numeric';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    /*
     *  @Desc check if logged in user is admin
     *  @param Request $request
     *  @return JSON
     */
    private function _checkAdmin($request)
    {
       $partyId = $request->input('partyId');
       $userParty = '';
       if( $partyId )
       {
           $selectedColumns = [
               'PartyID',
               'PartyType',
               ];
           $userParty = \App\Models\Party::select($selectedColumns)
                   ->where('PartyId', '=', $partyId)
                   ->where('PartyType', '=', 'ADMIN')->first();
           
           return $userParty;
       }
       return $userParty;
    }
    
}
