<?php

namespace App\Http\Controllers\Support;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class NotificationsController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->perpage         = config('vwallet.perpage');
        $this->mdmUrl          = config('vwallet.genrateViolaId');
        $this->userTypeKey     = config('vwallet.userTypeKey');
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
        $this->nonKycLimit     = config('vwallet.MONTH_LIMIT_NON_KYC');
        $this->prefix          = config('vwallet.prefix');
    }

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function listNotifications(\Illuminate\Http\Request $request)
    {

        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        // check validation
        $chk_valid  = $this->_validationCheckNotifications($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        else
        {
            $output_arr['is_error'] = FALSE;

            if ((count($this->_getUserNotifications($request))) <= 0)
            {
                $output_arr['display_msg'] = array('info' => trans('messages.no_records'));
            }

            $fullList = $this->_getUserNotifications($request);
            foreach ($fullList as $datalist)
            {

                $fromAccountNumber = $datalist['FromAccountId'];
                $toAccountNumber   = $datalist['ToAccountId'];
                if ($datalist['ProductName'] === 'TRIPLECLIK' || $datalist['ProductName'] === 'PAY W2W')
                {

                    $getFromAccountNumbers = \App\Models\Walletaccount::select('WalletAccountNumber', 'UserWalletId')->where('WalletAccountId', $fromAccountNumber)->first();
                    $getToAccountNumbers   = \App\Models\Walletaccount::select('WalletAccountNumber', 'UserWalletId')->where('WalletAccountId', $toAccountNumber)->first();
                    $fromMainAcntNum       = (!empty($getFromAccountNumbers->WalletAccountNumber)) ? $getFromAccountNumbers->WalletAccountNumber : '';
                    $toMainAcntNum         = (!empty($getToAccountNumbers->WalletAccountNumber)) ? $getToAccountNumbers->WalletAccountNumber : '';
                }
                else if ($datalist['ProductName'] == 'TOPUP')
                {

                    $getFromAccountNumbers1 = \App\Models\Partypreferredpaymentmethod::select('CardHolderName', 'CardDisplayNumber', 'CardNumber')->where('PaymentMethodID', $fromAccountNumber)->first();
                    $getToAccountNumbers1   = \App\Models\Walletaccount::select('WalletAccountNumber', 'UserWalletId')->where('WalletAccountId', $toAccountNumber)->first();
                    $fromMainAcntNum        = (!empty($getFromAccountNumbers1->CardDisplayNumber)) ? $getFromAccountNumbers1->CardDisplayNumber : $fromAccountNumber;
                    $toMainAcntNum          = (!empty($getToAccountNumbers1->WalletAccountNumber)) ? $getToAccountNumbers1->WalletAccountNumber : '';
                }
                else if ($datalist['ProductName'] === 'PAY W2B')
                {

                    $getFromAccountNumbers = \App\Models\userexternalbeneficiary::
                                    join('walletaccount', 'walletaccount.UserWalletId', 'userexternalbeneficiary.UserWalletID')
                                    ->select('walletaccount.WalletAccountNumber', 'userexternalbeneficiary.UserWalletId')->where('walletaccount.WalletAccountId', $fromAccountNumber)->first();
                    $getToAccountNumbers   = \App\Models\userexternalbeneficiary::select('AccountNumber', 'BeneficiaryID')->where('BeneficiaryID', $toAccountNumber)->first();
                    $fromMainAcntNum       = (!empty($getFromAccountNumbers->WalletAccountNumber)) ? $getFromAccountNumbers->WalletAccountNumber : '';
                    $toMainAcntNum         = (!empty($getToAccountNumbers->AccountNumber)) ? $getToAccountNumbers->AccountNumber : '';
                }
                else
                {

                    $getFromAccountNumbers = \App\Models\Walletaccount::select('WalletAccountNumber', 'UserWalletId')->where('WalletAccountId', $fromAccountNumber)->first();

                    $getToAccountNumbers = \App\Models\Userrechargedetail::
                                    join('transactiondetail', 'transactiondetail.TransactionDetailID', 'userrechargedetails.TransactionDetailID')
                                    ->join('useroperatorfeeplan', 'userrechargedetails.TransactionDetailID', 'useroperatorfeeplan.UseroperatorfeeplanID')
                                    ->join('billerlist', 'billerlist.BillerId', 'useroperatorfeeplan.Operator')
                                    ->select('billerlist.BillerName')->where('transactiondetail.TransactionDetailID', $datalist['transactionId'])->first();
                    $fromMainAcntNum     = (!empty($getFromAccountNumbers->WalletAccountNumber)) ? $getFromAccountNumbers->WalletAccountNumber : '';
                    $toMainAcntNum       = (!empty($getToAccountNumbers->BillerName)) ? $getToAccountNumbers->BillerName : '';
                }


                $txnlist[] = array(
                    'NotificationID'        => $datalist['NotificationID'],
                    'PartyID'               => $datalist['PartyID'],
                    'LinkedTransactionID'   => $datalist['LinkedTransactionID'],
                    'NotificationType'      => $datalist['NotificationType'],
                    'SentDateTime'          => $datalist['SentDateTime'],
                    'ReceipentAcknowledged' => $datalist['ReceipentAcknowledged'],
                    'Message'               => $datalist['Message'],
                    'MessageStatus'         => $datalist['MessageStatus'],
                    'ReceipentReceivedDate' => $datalist['ReceipentReceivedDate'],
                    'fromAccountNumber'     => $fromMainAcntNum,
                    'fromCustId'            => (!empty($getFromAccountNumbers->UserWalletId)) ? $getFromAccountNumbers->UserWalletId : '',
                    'fromName'              => $datalist['From_name'],
                    'toAccountNumber'       => $toMainAcntNum,
                    'toCustId'              => (!empty($getToAccountNumbers->UserWalletId)) ? $getToAccountNumbers->UserWalletId : '',
                    'toName'                => $datalist['To_name'],
                    'fromMobileNumber'      => $datalist['fromMobileNumber'],
                    'toMobileNumber'        => $datalist['toMobileNumber'],
                    'TransactionOrderID'    => $datalist['TransactionOrderID'],
                    'TransactionStatus'     => $datalist['TransactionStatus'],
                    'TransactionTypeCode'   => $datalist['TransactionTypeCode'],
                    'Amount'                => $datalist['Amount'],
                    'TransactionDate'       => $datalist['TransactionDate'],
                    'TransactionMode'       => ($datalist['TransactionMode'] == 'C') ? 'Credit' : 'Debit',
                    'TransactionCurrency'   => $datalist['TransactionCurrency'],
                    'AccountCurrency'       => $datalist['AccountCurrency'],
                    'ProductName'           => $datalist['ProductName'],
                    'Comments'              => $datalist['Comments'],
                );
            }


            $output_arr['res_data'] = $txnlist;
            return json_encode($output_arr);
        }
    }

    /**
     * This used to get all Notification details
     * @desc  This used to get all Notification details
     * @param PartyId integer
     * @param limit integer
     * @param offset integer
     *  
     * @return Array
     */
    private function _getUserNotifications($request)
    {
//\DB::connection()->enableQueryLog();
        $paginate               = $request->input('paginate');
        //$offset                 = $request->input('offset');
        $first                  = \App\Models\Promotion::select('PromotionID', 'PromoCode');
        $select_wallet_accounts = \App\Models\Notificationlog::
                join('transactiondetail', 'transactiondetail.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->join('dailytransactionlog', 'transactiondetail.TransactionDetailID', '=', 'dailytransactionlog.TransactionDetailID')
                ->leftjoin('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                ->join('paymentsproduct', 'transactiondetail.PaymentsProductID', '=', 'paymentsproduct.PaymentsProductID')
                ->join('transactioneventlog', 'transactiondetail.TransactionDetailID', '=', 'transactioneventlog.TransactionDetailID')
                ->join('userwallet', 'userwallet.UserWalletID', '=', 'transactiondetail.ToWalletId')
                ->leftjoin('userwallet As fromWallet', 'fromWallet.UserWalletID', '=', 'transactiondetail.UserWalletId')
                ->select('notificationlog.NotificationID', 'notificationlog.PartyID', 'notificationlog.LinkedTransactionID', 'notificationlog.NotificationType', 'notificationlog.SentDateTime', 'notificationlog.ReceipentAcknowledged', 'notificationlog.ReceipentReceivedDate', 'notificationlog.Message', 'notificationlog.MessageStatus', 'transactiondetail.TransactionOrderID', 'transactiondetail.Amount', 'transactiondetail.TransactionTypeCode', 'transactiondetail.TransactionDate', 'transactiondetail.TransactionStatus', 'dailytransactionlog.CrDrIndicator As TransactionMode', 'dailytransactionlog.TransactionCurrency', 'dailytransactionlog.AccountCurrency', 'paymentsproduct.ProductName', 'transactioneventlog.Comments', 'partypreferredpaymentmethod.CardNumber', 'transactiondetail.FromAccountName As From_name', 'userwallet.FirstName As To_name', 'fromWallet.MobileNumber As fromMobileNumber', 'userwallet.MobileNumber As toMobileNumber'
                )->where('notificationlog.PartyID', '=', $request->input('partyId'))
                ->where('dailytransactionlog.PartyID', '=', $request->input('partyId'));
        // 'transactiondetail.FromAccountName As From_name',
        if ($paginate !== NULL && $paginate == 'Y')
        {
            $select_wallet_accounts->paginate($this->perpage)->toArray();
        }
        $select_wallet_accounts->groupBy('transactiondetail.TransactionOrderID');
        //  $select_wallet_accounts->orderBy('notificationlog.MessageStatus', 'DESC');
        $select_wallet_accounts->orderBy('notificationlog.NotificationID', 'DESC');
        return $select_wallet_accounts->get();
    }

    /**
     * This used to get all Notification details for push notifications
     * @desc  This used to get all Notification details
     * @param PartyId integer
     *  
     * @return Array
     */
    public function getNotifcationsPushNotif(\Illuminate\Http\Request $request)
    {
//\DB::connection()->enableQueryLog();
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        // check validation
        $chk_valid  = $this->_validationCheckNotifications($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }

        $partyId                = $request->input('partyId');
        //$offset                 = $request->input('offset');
        $select_wallet_accounts = \App\Models\Notificationlog::
                leftJoin('transactioneventlog', 'transactioneventlog.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->leftJoin('transactiondetail', 'transactiondetail.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->select('notificationlog.NotificationID', 'notificationlog.NotificationType', 'transactioneventlog.Comments', 'notificationlog.ReceipentReceivedDate', 'notificationlog.SentDateTime', 'notificationlog.ReceipentAcknowledged', 'notificationlog.MessageStatus', 'notificationlog.RequestMoneyStatus', 'notificationlog.Message', 'notificationlog.RequestMoneyID', 'notificationlog.RequestMoneyUser', 'notificationlog.RequestMoney', 'transactiondetail.TransactionOrderID')
                ->where('notificationlog.PartyID', '=', $partyId)
                ->where('notificationlog.MessageStatus', '!=', 'Hide');
        if ($request->header('device-type') != 'W')
        {
            $select_wallet_accounts->where('notificationlog.NotificationType', '!=', 'Web');
        }

        $select_wallet_accounts->orderBy(\DB::raw('RequestMoneyStatus <> "request"'))
                ->orderBy('RequestMoneyUser','ASC')
                ->orderBy('NotificationID','DESC');
        // 'transactiondetail.FromAccountName As From_name',
        $countTotalData = count($select_wallet_accounts->get());
        $resData        = $select_wallet_accounts->paginate($this->perpage)->toArray();

        //   $countResults = $select_wallet_accounts['total'];
        // $resData = $select_wallet_accounts->get();


        if ($countTotalData > 0)
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array();
            //$output_arr['total'] = $countTotalData;
            $output_arr['res_data']    = array('notifyCount' => array('totalRecords' => $countTotalData, 'unReadRecords' => $this->getUnreadmsgsCount($partyId)), 'notifData' => $resData);
            return response()->json($output_arr);
        }

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);
    }

    public function getUnreadmsgsCount($partyId)
    {
        $select_notifLogs = \App\Models\Notificationlog::
                join('transactioneventlog', 'transactioneventlog.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->join('transactiondetail', 'transactiondetail.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                ->select('notificationlog.NotificationID', 'notificationlog.NotificationType', 'transactioneventlog.Comments', 'notificationlog.ReceipentReceivedDate', 'notificationlog.SentDateTime', 'notificationlog.ReceipentAcknowledged', 'notificationlog.MessageStatus', 'transactiondetail.TransactionOrderID')
                ->where('notificationlog.PartyID', '=', $partyId)
                ->where('notificationlog.MessageStatus', '=', 'UnRead');

        $select_notifLogs->orderBy('notificationlog.NotificationID', 'DESC');
        // 'transactiondetail.FromAccountName As From_name',
        return $unreadCount = count($select_notifLogs->get());
    }

    /**
     * Validate before showing notification list
     * @desc  this is used to check validation for Notification list
     * @param Request $request
     * @param PartyId integer
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _validationCheckNotifications($request)
    {
        // validations
        $validate1          = array(
            'partyId' => 'required|numeric',
        );
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate1, $extraParamValidate);
        $messages           = [];
        /* $messages = array(
          'PartyId.required'  => trans('validation.required'),
          'PartyId.numeric'   => trans('validation.numeric'),
          'paginate.required' => trans('validation.required'),
          'paginate.alpha'    => trans('validation.alpha'),
          'offset.required'   => trans('validation.required'),
          'offset.numeric'    => trans('validation.numeric'),
          ); */

        // validation for mobile device

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate _validateBellCount before showing notification Count
     * @desc  this is used to check validation for Notification Count
     * @param Request $request
     * @param PartyId integer
     * @param notificationType integer
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return JSON
     */
    public function notificationsCount(\Illuminate\Http\Request $request)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $chk_valid  = $this->_validateBellCount($request, 'count');
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }
        else
        {
            $user_id           = $request->input('partyId');
            $notification_type = $request->input('notificationType');

//            $notificationDetailsQuery = \App\Models\Notificationlog::select('NotificationID', 'LinkedTransactionID', 'NotificationType', 'SentDateTime', 'ReceipentAcknowledged', 'ReceipentReceivedDate', 'ReceipentDeliveryReport', 'ToAddress')
//                    ->where('PartyID', '=', $user_id)
//                    ->where('MessageStatus', '=', 'UnRead');
            $notificationDetailsQuery = \App\Models\Notificationlog::
                    join('transactioneventlog', 'transactioneventlog.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                    ->join('transactiondetail', 'transactiondetail.TransactionDetailID', '=', 'notificationlog.LinkedTransactionID')
                    ->select('notificationlog.NotificationID', 'notificationlog.NotificationType', 'transactioneventlog.Comments', 'notificationlog.ReceipentReceivedDate', 'notificationlog.SentDateTime', 'notificationlog.ReceipentAcknowledged', 'notificationlog.MessageStatus', 'transactiondetail.TransactionOrderID')
                    ->where('notificationlog.PartyID', '=', $user_id)
                    ->where('notificationlog.MessageStatus', '=', 'UnRead');
            if ($notification_type)
                $notificationDetailsQuery->where('notificationlog.NotificationType', '=', $notification_type)
                        ->orderBy('notificationlog.NotificationID', 'desc')
                        ->get();

            $notificationCount = $notificationDetailsQuery->count();

            //$notificationCount = $this -> _getUserNotifications($request);

            $output_arr['is_error'] = FALSE;
            $output_arr['res_data'] = array(
                'count' => $notificationCount,
            );
        }

        return response()->json($output_arr);
    }

    /**
     * Validate _validateBellCount before showing notification Status
     * @desc  this is used to check validation for Notification Status
     * @param Request $request
     * @param PartyId integer
     * @param notificationId integer
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return JSON
     */
    public function updateNotificationStatus(\Illuminate\Http\Request $request)
    {

        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $chk_valid  = $this->_validateBellCount($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }
        else
        {
            $user_id         = $request->input('partyId');
            $notification_id = $request->input('notificationId');

            if ($notification_id == 0)
            {
                $notiUpdate = array(
                    'MessageStatus' => 'Read'
                );
                \App\Models\Notificationlog::where('PartyID', $user_id)
                        ->update($notiUpdate);

                $output_arr['is_error'] = FALSE;
                $output_arr['res_data'] = 'Success';
            }

            if ($notification_id > 0)
            {
                $notifications = \App\Models\Notificationlog::find($notification_id);
                if ($notifications)
                {
                    $notifications->MessageStatus = 'Read';
                    $notifications->save();
                    $output_arr['is_error']       = FALSE;
                    $output_arr['res_data']       = 'Success';

                    $results = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                                    ->where('UserWalletId', $request->input('partyId'))->first();
                    /* // Send Notification
                      $sendMobileData = json_encode(array());
                      $sendEmailData = json_encode(array(
                      'fullname' => $results->FirstName.' '.$results->LastName,
                      'img_url' => $this->baseUrl,
                      'front_url' => $this->baseUrl,

                      ));
                      $communicationParams = array(
                      'party_id' => $results->UserWalletID,
                      'linked_id' => '0',
                      'notification_for' => 'profile_changePass',
                      'notification_type' => 'email,sms',
                      'mobile_no' => $results->MobileNumber,
                      'mobile_data' => $sendMobileData,
                      'email_id' => $results->EmailID,
                      'email_data' => $sendEmailData,

                      );
                      // Push notification parameters.
                      $device_type      = $request->header('device-type');
                      if($device_type!='W') {
                      $pushData =  array();
                      $communicationParams['notification_type'] = $communicationParams['notification_type'].",push";
                      $communicationParams['device_type'] = $device_type;
                      $communicationParams['device_token'] = $request->header('device-token');
                      $communicationParams['push_data'] = json_encode($pushData);
                      }
                      //common function to send notification_type msgs.
                      $response = \App\Libraries\Helpers::sendMsg($communicationParams);
                     */
                }
                else
                {
                    $output_arr['display_msg'] = array('info' => 'Please enter valid Notification Id');
                }
            }
        }

        return response()->json($output_arr);
    }

    /**
     * Validate _validateBellCount is used to check validations
     * @desc  this is used to check validation for _validateBellCount
     * @param Request $request
     * @param PartyId integer
     * @param notificationId integer
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _validateBellCount($request, $type = '')
    {
        $validations = array(
            'partyId' => 'required|integer|digits_between:1,10',
        );



        if ($type == 'count')
        {
            $validations['notificationType']    = 'regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/';
            $messages['notificationType.regex'] = 'Enter valid Notification Type';
        }
        else
        {
            $validations['notificationId'] = 'required';
        }
        $messages          = array();
        $validate_response = \Validator::make($request->all(), $validations, $messages);
        $validate_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);

        return $validate_response;
    }

}
