<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bookingmaster
 * 
 * @property int $Id
 * @property int $TransactionId
 * @property \Carbon\Carbon $TransactionDate
 * @property string $RedbusBookingId
 * @property string $BlockingId
 * @property int $Source
 * @property int $Destination
 * @property \Carbon\Carbon $JourneyDate
 * @property int $NoOfBookedSeats
 * @property int $NoOfCancelledSeats
 * @property int $ServiceId
 * @property float $TotalFare
 * @property int $BoardingPointId
 * @property string $BoardingPoint
 * @property string $BoardingAddress
 * @property string $DroppingAddress
 * @property int $DroppingPointId
 * @property string $DroppngPoint
 * @property string $TransactionStatus
 * @property string $TicketNum
 * @property string $PnrNum
 * @property string $BoardingTime
 * @property string $DroppingTime
 * @property string $BoardingLandmark
 * @property string $DroppingLandmark
 * @property string $IsCancellable
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \Illuminate\Database\Eloquent\Collection $bookingcancellations
 * @property \Illuminate\Database\Eloquent\Collection $bookingdetails
 * @property \Illuminate\Database\Eloquent\Collection $bookingfaredetails
 * @property \Illuminate\Database\Eloquent\Collection $bookingpayments
 * @property \Illuminate\Database\Eloquent\Collection $serviceinfos
 *
 * @package App\Models
 */
class Bookingmaster extends Eloquent
{
	protected $table = 'bookingmaster';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'TransactionId' => 'int',
		'Source' => 'int',
		'Destination' => 'int',
		'NoOfBookedSeats' => 'int',
		'NoOfCancelledSeats' => 'int',
		'ServiceId' => 'int',
		'TotalFare' => 'float',
		'BoardingPointId' => 'int',
		'DroppingPointId' => 'int'
	];

	protected $dates = [
		'TransactionDate',
		'JourneyDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionId',
		'TransactionDate',
		'RedbusBookingId',
		'BlockingId',
		'Source',
		'Destination',
		'JourneyDate',
		'NoOfBookedSeats',
		'NoOfCancelledSeats',
		'ServiceId',
		'TotalFare',
		'BoardingPointId',
		'BoardingPoint',
		'BoardingAddress',
		'DroppingAddress',
		'DroppingPointId',
		'DroppngPoint',
		'TransactionStatus',
		'TicketNum',
		'PnrNum',
		'BoardingTime',
		'DroppingTime',
		'BoardingLandmark',
		'DroppingLandmark',
		'IsCancellable',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionId');
	}

	public function bookingcancellations()
	{
		return $this->hasMany(\App\Models\Bookingcancellation::class, 'BookingMasterId');
	}

	public function bookingdetails()
	{
		return $this->hasMany(\App\Models\Bookingdetail::class, 'BookingMasterId');
	}

	public function bookingfaredetails()
	{
		return $this->hasMany(\App\Models\Bookingfaredetail::class, 'BookingMasterId');
	}

	public function bookingpayments()
	{
		return $this->hasMany(\App\Models\Bookingpayment::class, 'BookingMasterId');
	}

	public function serviceinfos()
	{
		return $this->hasMany(\App\Models\Serviceinfo::class, 'BookingMasterId');
	}
}
