<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;

class UpdateCBExpiryAmountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updateExpiryAmount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $request = '';
        $response = \App\Libraries\OfferBussness::updateExpiryAmount($request);
        $this->info("Application run successfull.");
    }
}