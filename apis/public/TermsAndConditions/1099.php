<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>You have to complete a UPI transaction by sending money to any of your friend’s UPI address to get Rs.50 cashback.</li>
        <li>Offer is valid till 25th January 2018.</li>
        <li>You can earn cashback for every UPI transaction with a unique UPI ID upto a total of INR 200.</li>
        <li>Cashback will be credited within 2 working days of completing the transaction.</li>
    </ul> 
</div>
<?php include 'footer.php';?>