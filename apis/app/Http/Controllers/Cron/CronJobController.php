<?php

namespace App\Http\Controllers\Cron;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CronJobController extends Controller {
    
    /*
    public function cronAddCashback(\Illuminate\Http\Request $request)
    {
        $response = \App\Libraries\OfferBussness::benefitTrack($request);
        return $response;
    }
     * 
     */
    
    public function cronCashBack(\Illuminate\Http\Request $request)
    {       
        $response = \App\Libraries\OfferBussness::cashBackTrans($request);
        return $response;
    }
    
    /*public function updateUsedAmount(\Illuminate\Http\Request $request)
    {
        $dateTime = date('Y-m-d H:i:s');
        
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        $bnftTrackData = \App\Models\Userofferbenefittracker::select('UserOfferBenefitTrackerID', 'EarnedAmount')->where('EarnedAmount', '>', '0.00')->where('ExtendedValidityDate', '>', $dateTime)->orderBy('ExtendedValidityDate', 'desc')->first();
        if ($bnftTrackData)
        {
            $updateData = array(
                'EarnedAmount'  => '0.00',
                'UsedAmount'    => $bnftTrackData->EarnedAmount,
                'UpdatedDateTime'=> $dateTime,
                'UpdatedBy'     => config('vwallet.userTypeKey')
            );
            \App\Models\Userofferbenefittracker::where('UserOfferBenefitTrackerID', $bnftTrackData->UserOfferBenefitTrackerID)->update($updateData);
            $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        }
        
        return $output_arr;
    }*/
    
    public function updateExpiryAmount(\Illuminate\Http\Request $request)
    {
        $response = \App\Libraries\OfferBussness::updateExpiryAmount($request);
        return $response;
    }
}
