<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Balance details after Topup Reversal device template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $amount varchar
 *  param $date varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Top Up reversal',
        'text'    => $salutationName . ',<br/><br/>
Top Up of INR ' . $amount . ' has been reversed on your ViolaWallet on ' . $date . ' as per your request. Available Balance' . $amount,
    ],
    'sms'   => [
        'text' => 'Top Up Reversal Alert! Dear ' . $fullname . ' ,Top Up of INR ' . $amount . ' has been reversed on your ViolaWallet on ' . $date . ' as per your request. Available Balance' . $amount . '
',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Top Up reversed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Rs ' . $amount . ' top Up reversed',
    ],
];

echo json_encode($array);

/* End of file balance_details_after_topup_reversal.php */