<?php
/**
 * A language file for text and sms alert
 * @category PHP
 * @package  Viola wallet
 * @author  Viola Services (India) PVT LTD
 */

return [
  'nreg'     => [
    'text_vari'    => array('fullname', 'otp_code'),
    'text'      => 'Dear {{fullname}}, Thank you for registering with {{site_name}}, Please use {{otp_code}} as OTP to verify your mobile number.',
  ],
  'after_nreg'     => [
    'text_vari'    => array(),
    'text'      => 'Welcome to Viola Wallet. We are delighted you joined us!',
  ],
  'non_kycUser'     => [
    'text_vari'    => array(),
    'text'      => 'Welcome to Viola Wallet. We are delighted you joined us! Submit your KYC documents for verification.',
  ],
  'login_otp'     => [
    'text_vari'    => array('otp_code'),
    'text'      => 'Enter OTP {{otp_code}} to login into your account.',
  ],
  'otp_verify'     => [
    'text_vari'    => array('otp_code'),
    'text'      => 'Enter OTP {{otp_code}} to verify your account.',
  ],
  'login_forgotpass'     => [
    'text_vari'    => array('otp_code'),
    'text'      => '{{otp_code}} is your One Time Passcode(OTP). Please use this code to proceed to reset your password.',
  ],
  'profile_changePass'     => [
    'text_vari'    => array(),
    'text'      => 'Your password is changed successfully.',
  ],
  'dash_blockcards'     => [
    'text_vari'    => array('card_number'),
    'text'      => 'Your Card Number {{card_number}} is blocked. Contact Customer Support Team for further queries.',
  ],
   'dash_restrctcards'     => [
    'text_vari'    => array('card_number'),
    'text'      => 'You have restricted Card Number {{card_number}} for online shopping .',
  ],
   'mypref_chngemobile'     => [
    'text_vari'    => array('mobile'),
    'text'      => 'As per your request, we have changed your mobile number to {{mobile}}.',
  ],
   'kyc_idproof'     => [
    'text_vari'    => array(),
    'text'      => 'Thank you for updating your KYC. We will verify your documents and send a confirmation mail.',
  ],
   'kyc_addresproof'     => [
    'text_vari'    => array(),
    'text'      => 'Thank you for updating your Address Proof. We will verify your documents and send a confirmation mail.',
  ],
   'topup_autodebit'     => [
    'text_vari'    => array(),
    'text'      => 'Your monthly Top Up feature is deactivated. Please update your KYC to restart your monthly planned payments..',
  ],
  'topup_creditamount'     => [
    'text_vari'    => array('amount','card_number'),
    'text'      => '{{amount}} successfully credited to your Card Number {{card_number}}.',
  ],
  'txn_card_debitamount'     => [
    'text_vari'    => array('amount','card_number'),
    'text'      => '{{amount}} Debited towards Card Number {{card_number}}.',
  ],
  'txn_declined'     => [
    'text_vari'    => array('txn_id','card_number','reason'),
    'text'      => 'Your transaction {{txn_id}} from the card {{card_number}} is declined due to {{reason}}.',
  ],
   'topup_debtcard_creditamount'     => [
    'text_vari'    => array('amount','card_number'),
    'text'      => '{{amount}} successfully credited into your Card Number {{card_number}}.',
  ],
   'topup_bank_amountcredit'     => [
    'text_vari'    => array('amount','card_number'),
    'text'      => '{{amount}} successfully credited into your Card Number {{card_number}}.',
  ],
  'topup_paypoint_amountcredit'     => [
    'text_vari'    => array('amount','card_number','paypoint_location'),
    'text'      => '{{amount}} successfully credited into your Card Number {{card_number}} through {{paypoint_location}}.',
  ],
   'topup_monthlyplan'     => [
    'text_vari'    => array('amount','card_number'),
    'text'      => '{{amount}} gets Auto Top-UP every 12 of the month into your Card Number {{card_number}} .',
  ],
   'help_assist'     => [
    'text_vari'    => array(),
    'text'      => 'Thank you for contacting Viola Wallet. Your query is registered with us and we will contact you shortly.',
  ],
  'help_phone'     => [
    'text_vari'    => array(),
    'text'      => 'Thank you for contacting Viola Wallet. Your query is registered with us and we will contact you shortly.',
  ],
  'help_livechat'    => [
    'text_vari'    => array(),
    'text'      => 'Thank you.',
  ],
  'frm_trpleclk_txn_suc'  => [
    'text_vari'    => array('fullname','amount'),
    'text'      => 'Sent {{amount}} to {{fullname}} through TripleClik.',
  ],
  'rec_trpleclk_txn_suc'  => [
    'text_vari'    => array('fullname', 'mobile_no', 'amount'),
    'text'      => 'Received {{amount}} from {{fullname}} {{mobile_no}}.',
  ],
  'trplclk_unsuc'    => [
    'text_vari'    => array('fullname','amount'),
    'text'      => 'Your payment of {{amount}} to payee {{fullname}} did not go through due to insufficient funds / Network Error/ Set limit exceeded.',
  ],
   'frm_psp_suc_txn'  => [
    'text_vari'    => array('fullname', 'amount', 'viola_id'),
    'text'      => '{{amount}} transferred to {{fullname}} ({{viola_id}}).',
  ],
   'rec_psp_suc_txn'  => [
    'text_vari'    => array('fullname', 'amount', 'viola_id'),
    'text'      => '{{amount}} received from {{fullname}} ({{viola_id}}).',
  ],
  'psp_unsuc_txn'    => [
    'text_vari'    => array(),
    'text'      => 'Payment declined due to Insufficient funds/ Network Error / Invalid Card / Limit Exceeded.',
  ],
  'frm_rqst_amt'  => [
    'text_vari'    => array('fullname', 'amount'),
    'text'      => '{{amount}} request sent to {{fullname}}.',
  ],
  'rec_rqst_amt'  => [
    'text_vari'    => array('fullname', 'amount'),
    'text'      => '{{amount}} request received form {{fullname}}.',
  ],
  'login_chnge_pin'     => [
    'text_vari'    => array(),
    'text'      => 'We changed your Viola PIN as per your request. If you did not make this change? Contact us immediately.',
  ],
  'add_payee'     => [
    'text_vari'    => array('fullname', 'viola_id', 'mobile'),
    'text'      => 'Payee request sent to {{fullname}}, {{viola_id}} & {{mobile}}.',
  ],
  'pay_suc_txn'     => [
    'text_vari'    => array('fullname','viola_id','amount'),
    'text'      => '{{amount}} transferred to {{fullname}} & {{viola_id}} .',
  ],
  'pay_unsuc_txn'     => [
    'text_vari'    => array(),
    'text'      => '',
  ],
  'pay_unreg_user'     => [
    'text_vari'    => array('amount','mobile'),
    'text'      => '<sender name> transferred {{amount}} to {{mobile}}.As you are not a Viola Wallet user, click on the link below to receive money..',
  ],
  'rqst_cards_cnfmrtn'     => [
    'text_vari'    => array('card_name',''),
    'text'      => 'Your Plastic Card request for {{card_name}} is in process. ',
  ],
   'rqst_cards_sent'     => [
    'text_vari'    => array('card_name',''),
    'text'      => 'Your Plastic Card request for {{card_name}} is dispatched.',
  ],
   'acnt_close'     => [
    'text_vari'    => array('card_name',''),
    'text'      => 'Your request to close your account is in process. We will get back to you shortly.',
  ],
    'send_money_wtow' => [
        'text_vari'       => array('amount','fromName','toName','transId','dateTime'),
        'text'            => '',
    ],
    'send_money_wtob' => [
        'text_vari'       => array('amount','fromName','toName','transId','dateTime'),
        'text'            => '',
    ],
    'scan_qr_send' => [
        'text_vari'       => array('amount','fromName','toName','transId','dateTime'),
        'text'            => '',
    ],
    'send_money_request' => [
        'text_vari'       => array('amount','fromName','toName','transId','dateTime'),
        'text'            => '',
    ]
];

/*1 email text for normal personal register nreg = normal register*/
$lang['nreg_sms_text'] = 'Dear {{fullname}}, Thank you for registering with {{site_name}}, Please use {{otp_code}} as OTP to verify your mobile number.';
$lang['nreg_sms_text_vari'] = array('fullname', 'otp_code');;

/* 2 email text for quick register qreg = quick register*/
$lang['qreg_sms_text'] = '';
$lang['qreg_sms_text_veri'] = '';

/* 3 email text for business register breg = business register*/
$lang['breg_sms_text'] = '';
$lang['breg_sms_text_veri'] = '';

/* 4 email text for actication of all account aaact = activate all account*/
$lang['aaact_sms_text'] = '';
$lang['aaact_sms_text_veri'] = '';

/* 5 email text for forgot Password forpas = forgot password*/
$lang['forpas_sms_text'] = '';
$lang['forpas_sms_text_veri'] = '';

/* 6 email text for forgot Password forpin = forgot pin*/
$lang['forpin_sms_text'] = '';
$lang['forpin_sms_text_veri'] = '';

/* 7 email text for forgot PIN forpin = forgot pin*/
$lang['forpin_sms_text'] = '';
$lang['forpin_sms_text_veri'] = '';

/* 8 email text for Change PIN cpin = Change pin*/
$lang['cpin_sms_text'] = '';
$lang['cpin_sms_text_veri'] = '';

/* 9 email text for Change Password cpass = Change Password*/
$lang['cpass_sms_text'] = '';
$lang['cpass_sms_text_veri'] = '';

/* 10 email text for Change Mobile cmob = Change Mobile*/
$lang['cmob_sms_text'] = '';
$lang['cmob_sms_text_veri'] = '';

/* 11 email text for Change Email cmail = Change Email*/
$lang['cmail_sms_text'] = '';
$lang['cmail_sms_text_veri'] = '';

/* 12 email text for Change Address cadd = Change Address*/
$lang['cadd_sms_text'] = '';
$lang['cadd_sms_text_veri'] = '';

/* 13 email text for Change Telephone ctel = Change Telephone*/
$lang['ctel_sms_text'] = '';
$lang['ctel_sms_text_veri'] = '';

/* 14 email text for Change Name Request cnreq = Change name request*/
$lang['cnreq_sms_text'] = '';
$lang['cnreq_sms_text_veri'] = '';

/* 15 email text for Change Currency Watch ccur = Change Currency Watch*/
$lang['ccur_sms_text'] = '';
$lang['ccur_sms_text_veri'] = '';

/* 16 email text for Add Beneficiary aben = Add Beneficiary*/
$lang['aben_sms_text'] = '';
$lang['aben_sms_text_veri'] = '';

/* 17 email text for Add Second Mobile amob = Add Second Mobile*/
$lang['amob_sms_text'] = '';
$lang['amob_sms_text_veri'] = '';

/* 18 email text for Add Additional Email email = Add Additional Email*/
$lang['amail_sms_text'] = '';
$lang['amail_sms_text_veri'] = '';

/* 19 email text for Wallet to Wallet Receive wtwr = Wallet to Wallet Receive*/
$lang['wtwr_sms_text'] = '';
$lang['wtwr_sms_text_veri'] = '';

/* 20 email text for Wallet to Wallet Send wtws = Wallet to Wallet Send*/
$lang['wtws_sms_text'] = '';
$lang['wtws_sms_text_veri'] = '';

/* 21 email text for Move Money Within My Wallet mmmw = Move Money Within My Wallet*/
$lang['mmmw_sms_text'] = '';
$lang['mmmw_sms_text_veri'] = '';

/* 22 email text for Move Money Within My Wallet wtw = Wallet to wallet transaction.*/
$lang['wtw_sms_text'] = '';
$lang['wtw_sms_text_veri'] = '';

/* 23 email text for Move Money Within My Wallet wtwe = Wallet to wallet exchange transaction.*/
$lang['wtwe_sms_text'] = '';
$lang['wtwe_sms_text_veri'] = '';

/* 25 email text for Move Money Within My Wallet wtwt = Wallet to wallet TripleClik transaction.*/
$lang['wtwt_sms_text'] = '';
$lang['wtwt_sms_text_veri'] = '';

/* 26 email text for Move Money Within My Wallet wtwto = Wallet to wallet TripleClik Other beneficiary transaction.*/
$lang['wtwto_sms_text'] = '';
$lang['wtwto_sms_text_veri'] = '';

/* 27 email text for Move Money Within My Wallet wtwte = Wallet to wallet TripleClik exchange transaction.*/
$lang['wtwte_sms_text'] = '';
$lang['wtwte_sms_text_veri'] = '';

/* 28 email text for Move Money Within My Wallet wtwtoe = Wallet to wallet TripleClik Other beneficiary exchange transaction.*/
$lang['wtwtoe_sms_text'] = '';
$lang['wtwtoe_sms_text_veri'] = '';

/* End of file textemail_alerts_lang.php */
