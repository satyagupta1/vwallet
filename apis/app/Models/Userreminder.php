<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 26 Feb 2018 10:39:42 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userreminder
 * 
 * @property int $UserReminderId
 * @property int $SourcedUserBillerTransid
 * @property int $UserWalletID
 * @property int $WalletAccountId
 * @property string $Status
 * @property string $BBPSBiller
 * @property string $BillerID
 * @property string $BillerName
 * @property string $BillerCategory
 * @property float $BillAmount
 * @property string $KeyAuthenticator_1
 * @property string $ResponseAuthenticator_1
 * @property string $KeyAuthenticator_2
 * @property string $ResponseAuthenticator_2
 * @property string $KeyAuthenticator_3
 * @property string $ResponseAuthenticator_3
 * @property string $KeyAuthenticator_4
 * @property string $ResponseAuthenticator_4
 * @property \Carbon\Carbon $ReminderAddedDt
 * @property \Carbon\Carbon $NextReminderDt
 * @property string $BillGenerated
 * @property \Carbon\Carbon $PaymentDueDate
 * @property string $ReminderTimelineUnit
 * @property int $ReminderTimelineValue
 * @property int $ReminderFrequencyRefID
 * @property string $PaidFromOtherServProv
 * @property int $ReminderCategoryRefId
 * @property int $ReminderSubCategoryRefId
 * 
 * @property \App\Models\Remindtransaction $remindtransaction
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Walletaccount $walletaccount
 *
 * @package App\Models
 */
class Userreminder extends Eloquent
{
	protected $primaryKey = 'UserReminderId';
	public $timestamps = false;

	protected $casts = [
		'ReminderId' => 'int',
		'UserWalletID' => 'int',
		'WalletAccountId' => 'int',
		'BillAmount' => 'float',		
	];

	protected $dates = [
		'CreatedDateTime',
		'PaymentDueDate'
	];

	protected $fillable = [
		'ReminderId',
		'UserWalletID',
		'WalletAccountId',
		'Status',
		'BBPSBiller',
		'BillerID',
		'BillerName',
		'BillerCategory',
		'BillAmount',		
		'ResponseAuthenticator_1',		
		'ResponseAuthenticator_2',		
		'ResponseAuthenticator_3',		
		'ResponseAuthenticator_4',
		'PaymentDueDate',
                'CreatedDateTime',
                'CreatedBy'		
	];

	public function remindtransaction()
	{
		return $this->belongsTo(\App\Models\Remindtransaction::class, 'SourcedUserBillerTransid');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'WalletAccountId');
	}
}
