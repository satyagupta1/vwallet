<?php

namespace App\Libraries;

class Vwallet {

    /**
     * Update session info
     *
     * @param Request $request
     * @param UserId 
     * @return None
     */
    public static function sessionDetails($request, $UserId,$loginHistory)
    {
        $sessionId    = $loginHistory->SessionID;
        $session_data = array(
            'SessionID'        => $sessionId,
            'PartyID'          => $UserId,
            'SessionStartTime' => date('Y-m-d H:i:s'),
            'DeviceType'       => $request->header('device-type'),
            'IPAddress'        => $request->header('ip-address'),
            'MobileType'       => 'W',
            'LoginSuccess'     => 'Y'
        );

        if ( $request->header('device-type') != 'W' )
        {
            $session_data['DeviceUniqueID'] = $request->header('device-uid');
            $session_data['DeviceToken']    = $request->header('device-token');
            $session_data['DeviceInfo']     = $request->header('device-info');
            $session_data['MobileType']     = $request->header('device-type');
        }
        return $session_data;
    }
    /**
     * partysessionusagedetail Insert info
     * @Desc Insert praty session usage. It is used to track party module access.
     * @param Request $request
     * @param UserId 
     * @return None
     */
    public static function sessionUsageInsert($insert)
    {
        // code to insert cards details starts
        $insertUsage                = new \App\Models\Partysessionusagedetail;
        $insertUsage->SessionID     = $insert['SessionID'];
        $insertUsage->EntryDateTime = date('Y-m-d H:i:s');
        $insertUsage->ModuleID      = $insert['ModuleID'];
        $insertUsage->OtherComments = $insert['OtherComments'];
        $insertUsage->save();
        return TRUE;
    }
    /**
     * @Desc ResponseLog
     * @param type $request
     * @param type $response
     * @param type $request_id
     * @return boolean
     */
    public static function ResponseLog1($path, $response, $request_id)
    {    
        
        $newPath = rtrim(app()->basePath('public/' . $path), '/');
        if ( ! is_dir($newPath))
        {
            mkdir($newPath, 0777, TRUE);
        }
        $fileName = $request_id.'.log';
        $file = fopen($newPath.'/'.$fileName,'a+');       
        fwrite($file, json_encode($response)."\r\n");
        fclose($file);
        return TRUE;
    }
    /**
     * @Desc BilldeskAPIMessages    
     * @param type $response
     * @param type $params
     * @return boolean
     */
    public static function ResponseLog($response, $params)
    {  
        $transDetailId = $params['transDetailId'];
        $msgType = $params['messageType'];
        $uniqueId = $params['uniqueId'];
        $path                = 'SystemLog/'.$msgType.'/';
        $newPath = rtrim(app()->basePath('public/' . $path), '/');
        $dateTimeOfTrans   = date('Y-m-d H:i:s');
        if ( ! is_dir($newPath))
        {
            mkdir($newPath, 0777, TRUE);
        }
        $fileName = $uniqueId.'.log';
        $storeFullPath = $path.$fileName;
        $file = fopen($newPath.'/'.$fileName,'a+');         
        fwrite($file, json_encode($response)."\r\n");
        fclose($file);        
        $transactionMessages = array('TransactionDetailID' => $transDetailId, 'MessageType' => $msgType, 'MessageFilePath' => $storeFullPath,  'CreatedBy'=>config('vwallet.adminTypeKey'), 'CreatedDateTime'=>$dateTimeOfTrans); 
        $insertId = \App\Models\Transactionapimessage::insertGetId($transactionMessages);
        return ($insertId)?TRUE:FALSE;
    }
    /**
     * @Desc BilldeskAPIMessages    
     * @param type $response
     * @param type $params
     * @return boolean
     */
    public static function BilldeskAPIMessages($response, $params)
    {   
       // print_r($params);
        
       $partyId = (($params['UserID'] == 'NA'))?$params['CustomerID']:$params['UserID'];
        $timeStamp = $params['TimeStamp'];
       $msgType = $params['MessageCode'];
        
        $path                = 'SystemLog/BillDesk/'.$partyId.'/';
        $newPath = rtrim(app()->basePath('public/' . $path), '/');
        $dateTimeOfTrans   = date('Y-m-d H:i:s');
        if ( ! is_dir($newPath))
        {
            mkdir($newPath, 0777, TRUE);
        }
        $fileName = $timeStamp.'.log';
        $storeFullPath = $path.$fileName;
        $file = fopen($newPath.'/'.$fileName,'a+');         
        fwrite($file, json_encode($response)."\r\n");
        fclose($file);        
        $transactionMessages = array( 'UserBillerTransid'=>$params['userTxnId'],'MessageType' => $msgType, 'MessageFilePath' => $storeFullPath, 'CreatedBy' => config('vwallet.adminTypeKey'), 'CreatedDateTime' => $dateTimeOfTrans );
        $insertId = \App\Models\BilldeskAPIMessage::insertGetId($transactionMessages);
        return ($insertId)?TRUE:FALSE;
    }
}

?>
