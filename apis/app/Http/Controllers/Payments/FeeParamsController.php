<?php

namespace App\Http\Controllers\Payments;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FeeParamsController extends \App\Http\Controllers\Controller {

    public function getFeesDetails(\Illuminate\Http\Request $request)
    {
        $partyId       = $request->input('partyId');
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateMethodFees($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $select_wallet_savedcards = \App\Models\Userwallet::select('UserWalletID', 'WalletPlanTypeId')
                ->where('UserWalletID', '=', $partyId)
                ->first();

        if ( empty($select_wallet_savedcards) )
        {

            $output_arr['display_msg'] = array( 'info' => 'No User Found' );
            return response()->json($output_arr);
        }

        $planTypeId = $select_wallet_savedcards->WalletPlanTypeId;

        $selectPlancongif = \App\Models\Productplanconfig::select('productplanconfig.PaymentEntityID', 'productplanconfig.PaymentProductID', 'productplanconfig.WalletPlanTypeId', 'productplanconfig.FeeFreqCycle', 'productplanconfig.ConfigParamNum', 'paymentsproduct.ProductName', 'paymentsproduct.PaymentsProductID', 'walletplantype.PlanName')
                ->join('paymententity', 'paymententity.PaymentEntityID', '=', 'productplanconfig.PaymentEntityID')
                ->join('walletplantype', 'walletplantype.WalletPlanTypeId', '=', 'productplanconfig.WalletPlanTypeId')
                ->join('paymentsproduct', 'paymentsproduct.PaymentsProductID', '=', 'productplanconfig.PaymentProductID')
                ->where('productplanconfig.ActiveFlag', 'Y')
                ->where('productplanconfig.FeeFreqCycle', 'D')
                ->where('productplanconfig.WalletPlanTypeId', $select_wallet_savedcards->WalletPlanTypeId)
                ->where('productplanconfig.ExpiryDate', '>', date('Y-m-d'))
                ->get();

        if ( empty($selectPlancongif) )
        {
            $output_arr['display_msg'] = array( 'info' => 'No Fee Details Found' );
            return response()->json($output_arr);
        }

        $walletCons    = \App\Models\Walletplanconfiguration::select('WalletPlanTypeId', 'ConfTagName', 'ConfKey', 'ConfValue')
                        ->where('activeFlag', 'Y')
                        ->where('endDate', '>', date('Y-m-d H:i:s'))
                        ->get()->toArray();
        $walletConfigs = array();
        foreach ( $walletCons as $walCon )
        {
            $walletConfigs[$walCon['WalletPlanTypeId']] = $walCon;
        }

        $dataArray = array();
        foreach ( $selectPlancongif as $mainconfig )
        {
            $feeAplicability = \App\Models\Paymentproductevent::select('FeeApplicable', 'EffectiveDate')
                            ->where('PaymentsProductID', $mainconfig->PaymentProductID)->first();

            $Array       = array(
                'PaymentsProductID' => $mainconfig->PaymentsProductID,
                'ProductName'       => $mainconfig->ProductName,
                'feeApplicable'     => $feeAplicability->FeeApplicable,
                'paymentEntityId'   => $mainconfig->PaymentEntityID,
                'PaymentEntityName' => $this->GetConfigName($mainconfig->PaymentEntityID),
                'WalletTypeId'      => $mainconfig->WalletPlanTypeId,
                'walletName'        => $mainconfig->PlanName,
                'walletConfigs'     => $walletConfigs[$mainconfig->WalletPlanTypeId],
                'ConfigParameters'  => $this->GetproductPlanData($mainconfig->ConfigParamNum)
            );
            $dataArray[] = $Array;
        }
        
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg'] = array( 'info' => '' );
        $output_arr['res_data']    = $dataArray;
        return response()->json($output_arr);
    }

    public function GetConfigName($id)
    {
        $select_config = \App\Models\Paymententity::select('PaymentEntityName', 'PaymentEntityID')
                ->where('PaymentEntityID', '=', $id)
                ->where('ActiveFlag', '=', 'Y')
                ->where('EndDate', '>', date('Y-m-d H:i:s'))
                ->first();

        return ( ! empty($select_config)) ? $select_config->PaymentEntityName : '';
    }

    public function GetproductPlanData($id)
    {
        $select_config = \App\Models\Productplanconfigparameter::select('ProductPlanConfigParameterId', 'ParameterTagName', 'ParameterTagValue')
                ->where('ConfigParamNum', '=', $id)
                ->where('ActiveFlag', '=', 'Y')
                ->where('ExpiryDate', '>', date('Y-m-d H:i:s'))
                ->get();

        $getresdata = array();
        foreach ( $select_config as $newdata )
        {
            $getresdata[$newdata->ParameterTagName] = $newdata->ParameterTagValue;
        }

        return $getresdata;
    }
    
    
    public function monthlyAggregate(\Illuminate\Http\Request $request)
    {
        $partyId       = $request->input('partyId');
        $productId       = $request->input('productId');
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateMethodFeesProductId($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $checkProductId = \App\Models\Paymentsproduct::select('PaymentsProductID')->where('PaymentsProductID',$productId)->first();
        if(empty($checkProductId))
        {
            $output_arr['display_msg'] = array('info' => 'No Product Id Found');
            return response()->json($output_arr);
        }
        
        $aggregateArray = \App\Libraries\TransctionHelper::monthlyAggregate($partyId);
       $selctTxnDetails = \App\Models\Transactiondetail::select('TransactionDetailID','Amount')
               ->where('TransactionStatus','Success')
               ->where('PaymentsProductID',$productId)
               ->whereBetween('TransactionDate', [ date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59' ])
               ->get();
      
       if(!empty($selctTxnDetails))
       {
           foreach ($selctTxnDetails as $mytxnDetails)
           {
               $totAMountsum [] =$mytxnDetails->Amount;
           }
       }
       
       
     $countTxns = (count($totAMountsum)>0)?count($totAMountsum):0;
     $totAmount = (array_sum($totAMountsum) > 0) ? array_sum($totAMountsum):0;
       $arrayData = ($aggregateArray != FALSE)?array($aggregateArray->Key => $aggregateArray->Value):array('MonthlyTransactionAmount' => 0);
       $arrayData['todayTxns'] = $countTxns;
       $arrayData['todayTxnsTotalAmount'] = $totAmount;
      
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg'] = array( 'info' => '' );
        $output_arr['res_data']    = $arrayData;
        return response()->json($output_arr);
    }
    

    /**
     * Validate Transaction request data
     * @param $request
     * @return String
     */
    private function _validateMethodFees($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
                //'device_type' => 'required|in:W,I,A,M',
        ];


        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.         



        /* $messages = [
          'user_id.required' => trans($langfolder . '/validation.userId'),
         */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    
     private function _validateMethodFeesProductId($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
            'productId' => 'required|numeric',
                //'device_type' => 'required|in:W,I,A,M',
        ];


        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.         



        /* $messages = [
          'user_id.required' => trans($langfolder . '/validation.userId'),
         */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
}
