<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller {

    public function __construct()
    {
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
    }

    public function mobileValidate(\Illuminate\Http\Request $request)
    {
        $output_arr       = array(
            'is_error'    => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data'    => array()
        );
        $validate_reponse = $this->_validateMethod($request, 'mobileValidate');

        if ($validate_reponse !== TRUE)
        {
            $output_arr['display_msg'] = $validate_reponse;
            return json_encode($output_arr);
        }

        $mobileNo    = str_replace('+91', '', $request->input('mobileNumber'));
        $userDetails = \App\Models\Userwallet::select('UserWalletID', 'FirstName', 'LastName', 'MobileNumber', 'EmailID', 'ViolaID', 'ProfileStatus')
                ->Where('MobileNumber', '=', $mobileNo)->Where('ProfileStatus', '=', 'A')
                ->first();
        if (empty($userDetails))
        {
            $output_arr['display_msg'] = array('mobileNumber' => trans('messages.wrongUser'));
        }
        else
        {
            $checkProfileStatus = $this->login_con($userDetails, $request);
            if ($checkProfileStatus['is_error'] === TRUE)
            {
                $output_arr['display_msg'] = $checkProfileStatus;
            }
            else if (!empty($checkProfileStatus['res_data']))
            {
                $output_arr['is_error']    = FALSE;
                $output_arr['status_code'] = 200;
                $output_arr['display_msg'] = array('info' => trans('messages.otpSentMsg'));
                $output_arr['res_data']    = $checkProfileStatus['res_data'];
            }
            else
            {
                $output_arr['display_msg'] = array('info' => trans('messages.wrong'));
            }
        }

        return json_encode($output_arr);
    }

    public function login_con($results, $request)
    {
        $msge_array = array(
            'is_error' => TRUE
        );

        if ($results->ProfileStatus === 'D')
        {
            $msge_array['info'] = trans('messages.accountDeactivated');
        }
        else if ($results->ProfileStatus === 'N')
        {
            $msge_array['info'] = trans('messages.accountDeactivated');
        }
        elseif ($results->ProfileStatus === 'DLE')
        {
            $msge_array['info'] = trans('messages.accountDelete');
        }
        else if ($results->MobileVerified === 'N')
        {

            $msge_array['info'] = trans('messages.accountNotActive');
        }
        else
        {
            $params      = array(
                'UserWalletID'  => $results->UserWalletID,
                'mobileNumber'  => $results->MobileNumber,
                'emailId'       => $results->EmailID,
                'userName'      => ($results->FirstName) ? $results->FirstName : '',
                'typeCode'      => 'FGP',
                'attempts'      => 0,
                'templateKey'   => 'VW007'
            );
            $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
            if ($otp_success !== FALSE)
            {
                $msge_array['res_data'] = $otp_success;
                $msge_array['is_error']  = FALSE;
            }
        }

        return $msge_array;
    }

    public function vPinChange(\Illuminate\Http\Request $request)
    {
        $attrpassword     = trans('messages.attributes.vpin');
        $attrMobileNumber = trans('validation.attributes.mobileNumber');
        $output_arr       = array(
            'is_error'    => TRUE,
            'display_msg' => array(),
            'status_code' => 200,
            'res_data'    => array()
        );
        $validate_res     = $this->_validateMethod($request, 'vPinChange');
        if ($validate_res !== TRUE)
        {
            $output_arr['display_msg'] = $validate_res;
        }
        else
        {

            $mobile_no = $request->input('mobileNumber');
            $passwordType = 'PIN';
            if ($request->header('device-type') == 'W')
            {
                $attrpassword     = trans('messages.attributes.password');
                $passwordType = 'PASSWORD';
                $newVpin = $request->input('password');
                $passType = 'password';
                $vPin         = \Illuminate\Support\Facades\Hash::make($request->input('password'));
            }else{
                $passType = 'vPin';
                $newVpin = $request->input('vPin');
                $vPin         = \Illuminate\Support\Facades\Hash::make($request->input('vPin'));
            }

            $passwordHistory = \App\Models\Userwallet::join('partypasswordhistory', 'partypasswordhistory.PartyID', '=', 'userwallet.UserWalletID', 'left')
                            ->where('userwallet.MobileNumber', '=', $mobile_no)
                            ->select('partypasswordhistory.SecretKey', 'partypasswordhistory.SecretKeyType', 'userwallet.UserWalletID')
                            ->orderBy('partypasswordhistory.CreatedDateTime', 'DESC')
                            ->limit(3)->get();
            if (count($passwordHistory) <= 0)
            {
                $message                   = trans('messages.nameInvalid', ['name' => $attrMobileNumber]);
                $output_arr['display_msg'] = array('info' => $message);
            }
            else
            {
                $user_id = $passwordHistory[0]->UserWalletID;
                foreach ($passwordHistory as $vPinkey => $vPinValue)
                {
                    if (\Illuminate\Support\Facades\Hash::check($newVpin, $vPinValue->SecretKey))
                    {
                        $output_arr['display_msg'] = array($passType => trans('messages.usedAlready'));
                        return $output_arr;
                    }
                }

                if (empty($output_arr['display_msg']))
                {
                    $userWallet = \App\Models\Userwallet::find($user_id);
                    if($request->header('device-type') == 'W'){
                    $userWallet->Password = $vPin;
                    }else{
                    $userWallet->TPin = $vPin;    
                    }
                    $userWallet->save();
                    
                    \App\Models\Partypasswordhistory::where('partypasswordhistory.PartyID', '=', $user_id)->update(['partypasswordhistory.Active' => 'N']);

                    $passHisInsert = array(
                        'PartyID'         => $user_id,
                        'SecretKey'       => $vPin,
                        'SecretKeyType'   => $passwordType,
                        'EffectiveDate'   => date('Y-m-d H:i:s'),
                        'Active'          => 'Y',
                        'CreatedDateTime' => date('Y-m-d H:i:s'),
                        'CreatedBy'       => 'User'
                    );

                    $passHisInsRes = \App\Models\Partypasswordhistory::insert($passHisInsert);

                    if ($passHisInsRes === FALSE)
                    {
                        $output_arr['display_msg'] = array('info' => trans('messages.wrong'));
                    }
                    else
                    {
                        $results = \App\Models\Userwallet::select('FirstName', 'LastName', 'MobileNumber', 'ViolaID', 'UserWalletID', 'EmailID')
                                        ->where('MobileNumber', $mobile_no)->first();

                        $templateData = array(
                            'partyId'        => $results->UserWalletID,
                            'linkedTransId'  => '0', // if transction related notification
                            'templateKey'    => 'VW012',
                            'senderEmail'    => $results->EmailID,
                            'senderMobile'   => $results->MobileNumber,
                            'reciverId'      => '', // if reciver / sender exsists
                            'reciverEmail'   => '',
                            'reciverMobile'  => '',
                            'templateParams' => array(
                                'fullName'   => ($results->FirstName) ? $results->FirstName : '',
                                'otpCode'    => '',
                                'supportURL' => "violamoney.com",
                            )
                        );
                        
                        // send notification_type msgs.
                        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData); 
                        
                        $output_arr['is_error']    = FALSE;
                        $message                   = trans('messages.updatedSuccessfully', ['name' => $attrpassword]);
                        $output_arr['display_msg'] = array('info' => $message);
                    }
                }
            }
        }
        return $output_arr;
    }

    private function _validateMethod($request, $method)
    {
        if ($method == 'vPinChange')
        {
            $validate = array(
                'vPin' => 'required|numeric|digits:4',
            );
            if ($request->header('device-type') == 'W')
            {
                $validate = array(
                    'password' => 'required|min:8|max:21|password',
                );
            }
        }
        $validate['mobileNumber'] = 'required|numeric|digits:10';
        $validate_response        = \Validator::make($request->all(), $validate);
        $validate_response        = \App\Libraries\Helpers::validateErrorResponse($validate_response);

        return $validate_response;
    }

}
