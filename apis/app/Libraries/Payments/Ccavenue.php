<?php

namespace App\Libraries\Payments;

error_reporting(E_ALL ^ E_DEPRECATED);

class Ccavenue {

    private $encKey;

    public function __construct()
    {
        $this->encKey = sha1(config('vwallet.ENCRYPT_SALT'));
    }

    /**
     * payment gateway CCAvenue
     * @Desc The CCAvenue payment integration supports a seamless payment experience on your platform, 
     * while protecting your application from payment frauds and complexity related to various regulations. 
     * @param Request $request
     * @param UserId 
     * @return None
     */
    public static function sedTransaction($insertData)
    {
        $accessCodeCcA = config('vwallet.accessCodeCcA');
        $workingKeyCcA = config('vwallet.workingKeyCcA');
        return "test " . $insertData;
    }

    public static function encrypt($plainText, $key)
    {
        $secretKey  = self::hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode   = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $blockSize  = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad   = self::pkcs5_pad($plainText, $blockSize);
        if ( mcrypt_generic_init($openMode, $secretKey, $initVector) != -1 )
        {
            $encryptedText = mcrypt_generic($openMode, $plainPad);
            mcrypt_generic_deinit($openMode);
        }
        return bin2hex($encryptedText);
    }

    public static function decrypt($encryptedText, $key)
    {
        $secretKey     = self::hextobin(md5($key));
        $initVector    = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = self::hextobin($encryptedText);
        $openMode      = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
    }

    //*********** Padding Function *********************

    private static function pkcs5_pad($plainText, $blockSize)
    {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    //********** Hexadecimal to Binary function for php 4.0 version ********

    private static function hextobin($hexString)
    {
        $length    = strlen($hexString);
        $binString = "";
        $count     = 0;
        while ( $count < $length )
        {
            $subString    = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ( $count == 0 )
            {
                $binString = $packedString;
            }
            else
            {
                $binString .= $packedString;
            }

            $count += 2;
        }
        return $binString;
    }

    public function kotakApiRequest(\Illuminate\Http\Request $request)
    {
        $partyId            = $request->input('partyId');
        $userData           = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.LastName', 'userwallet.MiddleName', 'userwallet.EmailID', 'userwallet.Gender', 'userwallet.DateofBirth', 'partyaddress.AddressLine1', 'partyaddress.AddressLine2', 'partyaddress.City', 'partyaddress.State', 'partyaddress.Country', 'partyaddress.PostalZipCode')
                        ->join('partyaddress', 'partyaddress.PartyID', '=', 'userwallet.UserWalletID', 'LEFT')
                        ->where('userwallet.UserWalletID', $partyId)
                        ->where('userwallet.ProfileStatus', 'A')->first();
        // print_r($request->input());exit;
        $request->input('partyId');
        $orderId            = $request->input('transactionId');
        $transactionOrderId = $request->input('transactionOrderId');
        $ccaMerchantId      = config('vwallet.ccaMerchantId');
        $accessCodeCcA      = config('vwallet.accessCodeCcA');
        $workingKeyCcA      = config('vwallet.workingKeyCcA');
        $ccavenueTransUrl    = config('vwallet.ccavenueTransUrl');
        //$apiUrl = 'https://192.168.100.86/apis/kotakTestApi';
        //$responseUrl = 'https://192.168.100.86/apis/paymentResponse';
        $responseUrl        = config('vwallet.ccavenueResponseUrl');
        //$responseUrl = 'http://192.168.100.86/apis/paymentResponse';
        
        // Default Response
        $response           = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $year     = date("Y");
        $cardType = $request->input('moneySource');
        $cardName = $request->input('cardVendor');
        $cardExp  = $request->input('cardExp');
        $cardNo   = $request->input('cardNo');

        $sendData = [
            'tid'              => $transactionOrderId, //time()*1000,
            'merchant_id'      => $ccaMerchantId,
            'order_id'         => $orderId,
            'amount'           => $request->input('amount'),
            'currency'         => $request->input('currencyCode'),
            'redirect_url'     => $responseUrl,
            'cancel_url'       => $responseUrl,
            'language'         => 'EN',
            'billing_name'     => ($userData->FirstName) ? $userData->FirstName : config('vwallet.applicationName'),
            'billing_address'  => config('vwallet.billingAddress'),
            'billing_city'     => config('vwallet.billingCity'),
            'billing_state'    => config('vwallet.billingState'),
            'billing_zip'      => config('vwallet.billingZip'),
            'billing_country'  => config('vwallet.billingCountry'),
            'billing_tel'      => config('vwallet.billingTel'),
            'billing_email'    => ($userData->EmailID) ? $userData->EmailID : config('vwallet.billingTestEmail'),
            'delivery_name'    => 'Viola',
            'delivery_address' => config('vwallet.billingAddress'),
            'delivery_city'    => config('vwallet.billingCity'),
            'delivery_state'   => config('vwallet.billingState'),
            'delivery_zip'     => config('vwallet.billingZip'),
            'delivery_country' => config('vwallet.billingCountry'),
            'delivery_tel'     => config('vwallet.billingTel'),
            //'merchant_param1' => 'NA',
            //'merchant_param2' => 'NA',
            //'merchant_param3' => 'NA',
            //'merchant_param4' => 'NA',
            //'merchant_param5' => 'NA', 
            'card_type'        => $cardType,
            'card_name'        => $cardName,
            'data_accept'      => 'N',
            'issuing_bank'     => $request->input('cardVendor'),
            'mobile_number'    => config('vwallet.billingTel')
        ];

        
        if ( $cardType == 'CRDC')
        {
            //$cardOneDegit = substr($cardNo, 0, 1);
            //$cardName     = ($cardOneDegit == 4) ? 'Visa' : 'MasterCard';
            $sendData['payment_option'] = 'OPTCRDC';
        }
        else if ($cardType == 'DBCRD' )
        {          
            $sendData['payment_option'] = 'OPTDBCRD';
            if($cardName == 'visa') {
               $cardName = 'Visa Debit Card';
            }
        }
        else if ( $cardType == 'NBK' )
        {
            $sendData['payment_option'] = 'OPTNBK';
        }
        $yearFirstTwoDig          = trim(substr($year, 0, 2));
        $sendData['card_type']    = $cardType;
        $sendData['card_name']    = $cardName;
        $sendData['card_number']  = $request->input('cardNo');
        $sendData['expiry_month'] = trim(substr($cardExp, 0, 2));
        $sendData['expiry_year']  = $yearFirstTwoDig . trim(substr($cardExp, 2, 4));
        $sendData['cvv_number']   = $request->input('cardCvv');


        $merchant_data = NULL;
        foreach ( $sendData as $key => $value )
        {
            $merchant_data .= $key . '=' . urlencode($value) . '&';
        }
        $encryptedData       = \App\Libraries\Payments\Ccavenue::encrypt($merchant_data, $workingKeyCcA);
        if ( $request->header('device-type') == 'W' )
        {
            $responseData = json_encode(array( 'ccavenueUrl' => $ccavenueTransUrl, 'encryptedData' => $encryptedData, 'accessCodeCcA' => $accessCodeCcA ));
        }
        else
        {
            $responseData = view('payments.ccavRequest')
                    ->with('ccavenueUrl', $ccavenueTransUrl)
                    ->with('encryptedData', $encryptedData)
                    ->with('accessCodeCcA', $accessCodeCcA);
        }
        $enKey               = config('vwallet.ENCRYPT_SALT');
        $responseDataEncrypt = \App\Libraries\Helpers::encryptData($responseData, $enKey, TRUE);        
        return $responseDataEncrypt;        
    }

}

?>
