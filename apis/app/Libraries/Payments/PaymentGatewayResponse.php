<?php

namespace App\Libraries;

class PaymentGatewayResponse {

    public static function updateTransctionStaus($status, $pgResponseArray)
    {    
        //print_r($pgResponseArray);exit;
        $transctionId = $pgResponseArray['order_id'];
        $statusMessage = $pgResponseArray['status_message'];
        $orderStatus = $pgResponseArray['order_status'];
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        //we got response from CCAvenue
        $select = array(
            'TransactionDetailID',
            'TransactionOrderID',
            'PaymentsProductID',
            'UserWalletId',
            'FromAccountId',
            'ToAccountId',
            'TransactionDetailID',
            'FeeAmount',
            'TaxAmount',
            'TransactionType',
            'Amount',
            'ToWalletId'
        );
        
        $update_txn_detail                    = \App\Models\Transactiondetail::select($select)
                        ->where('TransactionOrderID', $transctionId)->first(); 
        
        $updateTransction = \App\Models\Transactiondetail::where('TransactionOrderID', $transctionId)
                ->update(['TransactionStatus' => $status,
                    //'PartyMobileNumber' => $pgResponseArray['billing_tel'],
                    //'PartyEmailID' => $pgResponseArray['billing_email'],
                    'RespPGTrackNum' => $pgResponseArray['tracking_id'],
                    'RespBankRefNum' => $pgResponseArray['bank_ref_no']]);
       //print_r($updateTransction);exit;
        //$path = 'SystemLog/CCAresponse/'.date('Y').'/'.date('m').'/'.date('d').'/';
        $transDetailId = $update_txn_detail->TransactionDetailID;
        //$logDetail = \App\Models\Transactionapimessage::where('TransactionDetailID', '=', $transDetailId)->first();
        //if(!$logDetail) {
            $paramsData = array('transDetailId' => $transDetailId, 'messageType'=>'CCAresponse', 'uniqueId'=>$pgResponseArray['tracking_id']);
            \App\Libraries\Vwallet::ResponseLog($pgResponseArray,$paramsData);
           // \App\Libraries\Vwallet::ResponseLog($path, $pgResponseArray, $transDetailId);        
            //$transactionMessages = array('TransactionDetailID' => $transDetailId, 'MessageType' => 'CCAResponse', 'MessageFilePath' => $path,  'CreatedBy'=>config('vwallet.adminTypeKey'), 'CreatedDateTime'=>$dateTimeOfTrans); 
            //$transactionDetailID = \App\Models\Transactionapimessage::insertGetId($transactionMessages);
       // }        
        // update notification Log
        
        $notificationUpdate = \App\Models\Notificationlog::select('Message')
                ->where('PartyID', $update_txn_detail->ToAccountId)
                ->where('LinkedTransactionID', $update_txn_detail->TransactionDetailID)
                ->update(['Message' => $update_txn_detail->TransactionType . " Order Status: ".$orderStatus. ", Order Comment: ".$statusMessage]);
        
        /*$notificationUpdate = \App\Models\Notificationlog::select('Message')
                ->where('PartyID', '!=',$update_txn_detail->ToAccountId)
                ->where('LinkedTransactionID', $update_txn_detail->TransactionDetailID)
                ->update(['Message' => $update_txn_detail->TransactionType . " Order Status: ".$orderStatus. ", Order Comment: ".$statusMessage]);
        */
        return $update_txn_detail;
    }

    public static function updateTransctionwallets($pgResponseArray, $transctionDetails)
    {
        if ( $pgResponseArray['order_status'] === 'Success' )
        {
           
            //save only if success
            // code for admin wallet update starts
            $order_amount = $transctionDetails->Amount;
            $admin_wallet                         = \App\Libraries\TransctionHelper::getAdminWalletInfo(1);
            
            $update_adminWallet                   = \App\Models\Walletaccount::find($admin_wallet->WalletAccountId);
            $update_adminWallet->AvailableBalance -= $order_amount;
            $update_adminWallet->CurrentAmount    -= $order_amount;
            $update_adminWallet->save();
            // code for user wallet update starts    
            $update_userWallet                   = \App\Models\Walletaccount::find($transctionDetails->ToAccountId);
            $update_userWallet->AvailableBalance += $order_amount;
            $update_userWallet->CurrentAmount    += $order_amount;
            $update_userWallet->save();

            $feeWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(2);
            $taxWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(3);
            // update Fee wallet balance
            $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
            $updateFeeBal->AvailableBalance += $transctionDetails->FeeAmount;
            $updateFeeBal->CurrentAmount    += $transctionDetails->FeeAmount;
            $updateFeeBal->save();

            // update Tax wallet Balance
            $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
            $updateTaxBal->AvailableBalance += $transctionDetails->TaxAmount;
            $updateTaxBal->CurrentAmount    += $transctionDetails->TaxAmount;
            $updateTaxBal->save();
        }
        return TRUE;
    }

}
