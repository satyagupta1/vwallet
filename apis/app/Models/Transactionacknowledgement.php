<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactionacknowledgement
 * 
 * @property int $TransactionID
 * @property string $AckReferenceNumber
 * @property string $MoreDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transaction $transaction
 *
 * @package App\Models
 */
class Transactionacknowledgement extends Eloquent
{
	protected $table = 'transactionacknowledgement';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TransactionID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionID',
		'AckReferenceNumber',
		'MoreDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'TransactionID');
	}
}
