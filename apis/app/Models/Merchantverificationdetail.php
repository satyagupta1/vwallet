<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantverificationdetail
 * 
 * @property int $MerchantVerificationDetailID
 * @property int $MerchantVerficationID
 * @property int $DocumentID
 * @property string $DocumentTypeID
 * @property string $DocumentVerificationStatus
 * @property \Carbon\Carbon $CraetedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * @property string $UpadatedBy
 * 
 * @property \App\Models\Merchantverfication $merchantverfication
 * @property \App\Models\Partydocument $partydocument
 *
 * @package App\Models
 */
class Merchantverificationdetail extends Eloquent
{
	protected $table = 'merchantverificationdetail';
	protected $primaryKey = 'MerchantVerificationDetailID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantVerificationDetailID' => 'int',
		'MerchantVerficationID' => 'int',
		'DocumentID' => 'int'
	];

	protected $dates = [
		'CraetedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'MerchantVerficationID',
		'DocumentID',
		'DocumentTypeID',
		'DocumentVerificationStatus',
		'CraetedDatetime',
		'CreatedBy',
		'UpdatedDatetime',
		'UpadatedBy'
	];

	public function merchantverfication()
	{
		return $this->belongsTo(\App\Models\Merchantverfication::class, 'MerchantVerficationID');
	}

	public function partydocument()
	{
		return $this->belongsTo(\App\Models\Partydocument::class, 'DocumentID');
	}
}
