<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property string $BankCode
 * @property string $BankName
 * @property int $PreferencePriority
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $bankbranches
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
	protected $table = 'bank';
	protected $primaryKey = 'BankCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PreferencePriority' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BankName',
		'PreferencePriority',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bankbranches()
	{
		return $this->hasMany(\App\Models\Bankbranch::class, 'BankCode');
	}
}
