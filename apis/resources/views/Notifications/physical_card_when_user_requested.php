<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  When user requested for Physical card (Request Confirmation, Order Id,) template
 *  param $fullname varchar 
 *  param $orderId int   
 *  param $trackingNumber int  
 *  param $dateTime varchar 
 *  param $shipmentType varchar  
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaCard Request!',
        'text'    => $salutationName . ',<br/><br/>  
Plastic Card Request! 
Thank you for contacting us for Plastic Card Request. Your request has been confirmed with <br/><br/>
Order ID: '.$orderId],
    'sms'   => [
        'text' => '
            Dear '.$fullname.', 
            Thank you for contacting us for Plastic Card. Your request has been confirmed and we will send you an email confirmation with a link to track your card.'],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => 'Physical card request activated',
    ],
];

echo json_encode($array);

/* End of file physical_card_when_user_requested.php */
