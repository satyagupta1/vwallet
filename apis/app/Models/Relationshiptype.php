<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Relationshiptype
 * 
 * @property string $RelationshipTypeCode
 * @property string $RelationshipDescription
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $usernomineedetails
 *
 * @package App\Models
 */
class Relationshiptype extends Eloquent
{
	protected $table = 'relationshiptype';
	protected $primaryKey = 'RelationshipTypeCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'RelationshipDescription',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function usernomineedetails()
	{
		return $this->hasMany(\App\Models\Usernomineedetail::class, 'MinorGuardianRelationTypeCode');
	}
}
