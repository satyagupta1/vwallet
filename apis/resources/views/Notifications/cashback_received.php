<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback Received
 * 
 *  param $amount decimal
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Congratulations! Enjoy cashbacks',
        'text'    => $salutationName . ',<br/><br/>
Great news! you received cashback of Rs '.$amount.' on your '.$operator.' recharge.<br/>
Please refer your friends and help them receive cashbacks too. You also earn more cashbacks on each referral.<br/>
Invite Friends Now.',
    ],
    'sms'   => [
        'text' => 'Cashback of Rs '.$amount.' received on '.$operator.' recharge',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cashback of Rs '.$amount.' received on Mobile/ Gas/ DTH recharge',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'offers'
    ],
    'web'   => [
        'text' => 'Enjoy cash back of Rs '.$amount.' on '.$operator,
    ],
];

echo json_encode($array);

/* End of file cashback_received */
