<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Forgot Viola Pin template
 * 
 *  param $pinChangedDate date
 *  param $pinChangedTime time          
 *  param $supportURL varchar 
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'ViolaWallet PIN reset',
        'text'    => $salutationName . ', <br/><br/>
                        '.$otpCode.' is your OTP to reset your ViolaPIN. <br/>
                        If you didn`t make this request, your account might be compromised. To get back into your account, you`ll need to reset your credentials.'],
    'sms'   => [
        'text' => $otpCode . ' is your OTP to reset your ViolaPIN. If you did not make this request please call us at 9030084652.',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file forgot_viola_pin.php */
