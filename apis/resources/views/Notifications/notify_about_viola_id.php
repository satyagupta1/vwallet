<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Notify about Viola ID template
 * 
 *  param $fullname varchar
 *  param $otpCode varchar
 *  param $email varchar
 *  param $deviceName varchar          
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Get started with ViolaWallet!',
        'text'    => $salutationName . ',<br/><br/> 
Welcome to Viola family!  Congratulations your unique  Viola ID has been created for your ViolaWallet. Please login using your unique ID.'],
    'sms'   => [
       'text'    => '',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your unique Viola Id has been created.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file notify_about_viola_id.php */
