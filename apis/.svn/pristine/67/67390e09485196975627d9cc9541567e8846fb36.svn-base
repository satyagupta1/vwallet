<?php

namespace App\Http\Controllers\ManageMyBeneficiary;

use App\Http\Controllers\Controller;

/**
 * BeneficiaryController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class BeneficiaryController extends Controller {

    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');
    }

    /*
     *  searchBeneficiary - ListBeneficiary
     *  @Desc It will give all all beneficiary list, with filter by beneficiary type, triple click and beneficiary mobile
     *  @param Request $request
     *  @return JSON
     */

    public function getBeneficiaryList(\Illuminate\Http\Request $request)
    {

        //Default Response
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateMethodTBeneficiary($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $partyId           = $request->input('partyId');
        $beneficiaryType   = $request->input('beneficiaryType');
        $tripleClick       = $request->input('tripleClik');
        $beneficiaryMobile = $request->input('mobileNumber');
        $orderBy           = $request->input('orderBy');
        $orderByColumn     = $request->input('orderColumn');
        $limit             = $request->input('limit');
        $paginate          = $request->input('paginate');


        $selectColumns = array(
            'userbeneficiary.BeneficiaryID',
            'userbeneficiary.UserWalletID',
            'userbeneficiary.BeneficiaryWalletID',
            'userbeneficiary.BeneficiaryType',
            'userbeneficiary.BankIFSCCode',
            'userbeneficiary.MMID',
            'userbeneficiary.AccountName',
            'userbeneficiary.AccountNumber',
            'userbeneficiary.BeneficiaryMobileNumber',
            'userbeneficiary.WalletBeneficiaryName',
            'userbeneficiary.isTripleClickUser',
            'userbeneficiary.TripleClickAmount',
            'userbeneficiary.Comments',
            'userbeneficiary.CreatedDateTime',
            'userbeneficiary.yesBankBeneficiaryStatus',
            'userbeneficiary.yesBankBeneficiaryId',
            'userbeneficiarylimits.maxPermissibleLimit',
            'userbeneficiarylimits.monthlyLimitAvailed',
        );


        $whereArray = [];
        //filter by beneficiary type
        if ( $beneficiaryType !== NULL )
        {
            $whereArray['userbeneficiary.BeneficiaryType'] = $beneficiaryType;
        }

        //filter by triple click
        if ( $tripleClick !== NULL )
        {
            $whereArray['userbeneficiary.isTripleClickUser'] = $tripleClick;
        }

        //filter by beneficiary mobile
        if ( $beneficiaryMobile !== NULL )
        {
            $whereArray['userbeneficiary.BeneficiaryMobileNumber'] = $beneficiaryMobile;
        }
        $whereArray['userbeneficiary.BeneficiaryStatus']        = 'Y';
        $whereArray['userbeneficiary.yesBankBeneficiaryStatus'] = 'Y';
        $whereArray['userbeneficiary.UserWalletID']             = $partyId;
        $query                                                  = \App\Models\Userbeneficiary::join('userbeneficiarylimits', 'userbeneficiary.BeneficiaryID', '=', 'userbeneficiarylimits.BeneficiaryID')
                ->where($whereArray);

        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }

        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectColumns) )
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('userbeneficiary.BeneficiaryID', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {

            $userDetails  = $query->select($selectColumns)->paginate($this->perpage)->toArray();
            $countResults = $userDetails['total'];
        }
        else
        {
            $userDetails  = $query->get($selectColumns)->toArray();
            $countResults = count($userDetails);
        }

        if ( $countResults > 0 )
        {
            $output_arr['res_data'] = $userDetails;
            if ( $tripleClick == 'Y' )
            {
                $defaultAmounts         = \App\Models\Systemconfiguration::select('SecDescription')->where('SecKey', 'tcAmounts')->first();
                $output_arr['res_data'] = array( 'DefaultTripleClikAmounts' => $defaultAmounts->SecDescription, 'beneficiaryList' => $userDetails );
            }
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array();
            return response()->json($output_arr);
        }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
        return response()->json($output_arr);
    }

    // code to get ifsc code with state city branch location
    public function getIfscBankDetails(\Illuminate\Http\Request $request)
    {
        $attrIfsc   = trans('messages.attributes.ifsc');
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => '', 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateIfsc($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $output_arr['is_error'] = FALSE;

        //Pincode Details
        $getPincodeDetails = \App\Models\Bank:: join('bankbranch', 'bankbranch.BankCode', '=', 'bank.BankCode')
                ->select('bank.BankName', 'bankbranch.BankBranchID', 'bankbranch.BankCode', 'bankbranch.BranchName', 'bankbranch.AddressLine1', 'bankbranch.AddressLine2', 'bankbranch.City', 'bankbranch.State', 'bankbranch.Country', 'bankbranch.PinCode', 'bankbranch.BranchLocalCurrency', 'bankbranch.IFSCCode', 'bankbranch.MICRCode', 'bankbranch.SWIFTCode', 'bankbranch.OperatingCurrencyCode')
                ->where('bankbranch.IFSCCode', $request->input('ifscCode'))
                ->first();
        $message           = trans('messages.nameInvalid', [ 'name' => $attrIfsc ]);

        if ( $getPincodeDetails )
        {
            $output_arr['res_data'] = $getPincodeDetails;
            //  ($getPincodeDetails->count() <= 0) ?  : $output_arr['display_msg'] = array();
            return json_encode($output_arr);
        }

        $output_arr['display_msg'] = array( 'info' => $message );
        return json_encode($output_arr);
    }

    public function checkUserWithUsOrNot(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => '', 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateContact($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $contacts   = explode(',', ($request->input('contacts')));
        $result     = \App\Models\Userwallet::select('MobileNumber')->whereIn('MobileNumber', $contacts)->where('ProfileStatus', 'A')->get();
        $resContact = array();
        if ( $result )
        {
            $resMobiles = array();
            foreach ( $result as $value )
            {
                $resMobiles[] = $value->MobileNumber;
            }
            $removeExistingContact = array_values(array_diff($contacts, $resMobiles));

            $resContact = array( 'existingContact' => $resMobiles, 'nonExistingContact' => $removeExistingContact );
        }
        else
        {
            $resContact = array( 'existingContact' => [], 'nonExistingContact' => $contacts );
        }
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $resContact;
        return json_encode($output_arr);
    }

    private function _validateContact($request)
    {
        $validate = array(
            'partyId'  => 'required|numeric',
//            'contacts' => 'required|regex:^[0-9,]$^',
            'contacts' => 'required',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _validateIfsc($request)
    {
        $validate = array(
            'ifscCode' => 'required|alpha_num',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate Triple Click Beneficiary Data
     * @param $request
     * @return String
     */
    private function _validateMethodTBeneficiary($request)
    {

        $validate = [
            'partyId' => 'required|numeric',
        ];
        //echo $request->input('beneficiary_type');exit;
        if ( $request->input('beneficiary_type') !== NULL )
        {
            $validate['beneficiaryType'] = 'required';
        }

        if ( $request->input('tripleClik') !== NULL )
        {
            $validate['tripleClik'] = 'required|in:Y,N';
        }

        if ( $request->input('mobileNumber') !== NULL )
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
        }
        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function deleteWallet(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => '', 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateParty($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $partyId     = $request->input('partyId');
        $userDetails = \App\Models\Userwallet::select('MobileNumber')->where('UserWalletID', $partyId)->first();

        if ( ! $userDetails )
        {
            $output_arr['display_msg'] = '';
            return response()->json($output_arr);
        }
        $extraParams = array( 'partyId' => '1', 'requestId' => '2', 'referenceId' => '3', 'status' => 'Initiated' );

        $actionName = 'CLOSE';
        $params     = array( 'p1' => '9573398780', 'p2' => 14453 ); //9038213799

        $response = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params, $extraParams);
    }

    private function _validateParty($request)
    {
        $validate = array(
            'partyId' => 'required|numeric',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
