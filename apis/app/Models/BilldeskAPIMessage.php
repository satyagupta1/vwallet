<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 20 Nov 2017 11:57:55 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class BilldeskAPIMessage
 * 
 * @property int $BilldeskAPIMessID
 * @property int $UserWalletID
 * @property string $MessageType
 * @property string $MessageFilePath
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class BilldeskAPIMessage extends Eloquent
{
	public $timestamps = false;
        protected $table = 'billdeskapimessages';

	protected $casts = [
		'UserWalletID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'MessageType',
		'MessageFilePath',
		'CreatedBy',
		'CreatedDateTime'
	];

	public function userwallet()
	{
            return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
