<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 26 Feb 2018 10:39:42 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Remindtransaction
 * 
 * @property int $RemindTransactionId
 * @property int $PartyId
 * @property int $TransactionId
 * @property string $ActionType
 * @property int $DueTimeLine
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \Illuminate\Database\Eloquent\Collection $userreminders
 *
 * @package App\Models
 */
class Remindtransaction extends Eloquent
{
	protected $primaryKey = 'RemindTransactionId';
	public $timestamps = false;

	protected $casts = [
		'PartyId' => 'int',
		'DueTimeLine' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyId',
		'Category',
		'BillerId',
		'SubscriberNumber',
		'ActionType',
		'DueTimeLine',
		'Status',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyId');
	}
}