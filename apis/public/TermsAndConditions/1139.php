<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>        
        <li>Use code DTH10 to earn cashback of INR 10 on minimum recharge of INR 100.  </li>
        <li>Applicable on recharges done during 01st May to 30th June 2018</li>
        <li>Offer can be redeemed multiple times during the Offer Period, with one registered mobile number. User can perform 10 transactions using the code with cumulative cashback  is INR 100 </li>
        <li>Offer is valid on all operators and on all Circles.</li>
        <li>Coupon redemption & Payment should be done only on ViolaWallet App or Website.</li>
        <li>Viola has the right to end or call back any or all of its offer without prior notice.</li>
    </ul> 
</div>
<?php include 'footer.php';?>
