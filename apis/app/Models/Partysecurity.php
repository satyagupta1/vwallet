<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partysecurity
 * 
 * @property int $PartyID
 * @property int $QuestionId
 * @property int $QuestionRankNumber
 * @property string $Answer
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Securityquestion $securityquestion
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partysecurity extends Eloquent
{
	protected $table = 'partysecurity';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int',
		'QuestionId' => 'int',
		'QuestionRankNumber' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'Answer',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function securityquestion()
	{
		return $this->belongsTo(\App\Models\Securityquestion::class, 'QuestionId');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
}
