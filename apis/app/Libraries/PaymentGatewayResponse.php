<?php

namespace App\Libraries;

class PaymentGatewayResponse {

    public static function updateTransctionStaus($status, $pgResponseArray)
    {
        //print_r($pgResponseArray);exit;
        $transctionId    = $pgResponseArray['order_id'];
        $statusMessage   = $pgResponseArray['status_message'];
        $orderStatus     = $pgResponseArray['order_status'];
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        //we got response from CCAvenue
        $select          = array(
            'TransactionDetailID',
            'TransactionOrderID',
            'PaymentsProductID',
            'UserWalletId',
            'FromAccountId',
            'ToAccountId',
            'TransactionDetailID',
            'FeeAmount',
            'TaxAmount',
            'TransactionType',
            'Amount',
            'ToWalletId',
            'YesBankStatusCode',
            'CreatedDatetime',
        );

        $update_txn_detail = \App\Models\Transactiondetail::select($select)
                        ->where('TransactionOrderID', $transctionId)->first();
        
        $updateTrans = [ 'TransactionStatus' => $status,
            //'PartyMobileNumber' => $pgResponseArray['billing_tel'],
            //'PartyEmailID' => $pgResponseArray['billing_email'],
            'RespPGTrackNum'    => $pgResponseArray['tracking_id'],
            'RespBankRefNum'    => $pgResponseArray['bank_ref_no']];
        
        // Add money to Yes Bank wallet
        if ( $status === 'Success' )
        {
            $reqParams = array('UserWalletId' => $update_txn_detail->UserWalletId, 'amount' => $update_txn_detail->Amount, 'partRefNum' => $pgResponseArray['order_id']);
            $yesBankResp = self::_updateYesBankWallet($reqParams);
            $updateTrans = array_merge($updateTrans, $yesBankResp);
            $update_txn_detail->YesBankStatusCode = $updateTrans['YesBankStatusCode'];
        }
        
        \App\Models\Transactiondetail::where('TransactionOrderID', $transctionId)
                ->update($updateTrans);

        $transDetailId = $update_txn_detail->TransactionDetailID;

        $paramsData = array( 'transDetailId' => $transDetailId, 'messageType' => 'CCAresponse', 'uniqueId' => $pgResponseArray['tracking_id'] );
        \App\Libraries\Vwallet::ResponseLog($pgResponseArray, $paramsData);


        \App\Models\Notificationlog::select('Message')
                ->where('PartyID', $update_txn_detail->ToAccountId)
                ->where('LinkedTransactionID', $update_txn_detail->TransactionDetailID)
                ->update([ 'Message' => $update_txn_detail->TransactionType . " Order Status: " . $orderStatus . ", Order Comment: " . $statusMessage ]);


        return $update_txn_detail;
    }

    public static function updateTransctionwallets($pgResponseArray, $transctionDetails)
    {
        if ( $pgResponseArray['order_status'] === 'Success' )
        {
            //save only if success
            // code for admin wallet update starts
            $order_amount = $transctionDetails->Amount;
            $admin_wallet                         = \App\Libraries\TransctionHelper::getAdminWalletInfo(1);
            
            $update_adminWallet                   = \App\Models\Walletaccount::find($admin_wallet->WalletAccountId);
            $update_adminWallet->AvailableBalance -= $order_amount;
            $update_adminWallet->CurrentAmount    -= $order_amount;
            $update_adminWallet->save();
            // code for user wallet update starts    
            $update_userWallet                   = \App\Models\Walletaccount::find($transctionDetails->ToAccountId);
            $update_userWallet->AvailableBalance += $order_amount;
            $update_userWallet->CurrentAmount    += $order_amount;
            $update_userWallet->save();

            $feeWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(2);
            $taxWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(3);
            // update Fee wallet balance
            $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
            $updateFeeBal->AvailableBalance += $transctionDetails->FeeAmount;
            $updateFeeBal->CurrentAmount    += $transctionDetails->FeeAmount;
            $updateFeeBal->save();

            // update Tax wallet Balance
            $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
            $updateTaxBal->AvailableBalance += $transctionDetails->TaxAmount;
            $updateTaxBal->CurrentAmount    += $transctionDetails->TaxAmount;
            $updateTaxBal->save();
        }
        return TRUE;
    }
    
    private function _updateYesBankWallet($reqParams)
    {
        $transStatus = array(
        'TransactionStatus' => 'Pending',
        'YesBankTransNum' => '',
        'YesBankRefNum' => '',
        'YesBankCode' => '',
        'YesBankStatusCode' => '');
        if ($reqParams['UserWalletId'])
        {
            $userDetails = \App\Models\Userwallet::select('MobileNumber')
                        ->where('UserWalletId', $reqParams['UserWalletId'])->first();
            if ($userDetails->MobileNumber)
            {
                $actionName = 'TRANSACTION';
                $extraParams = array('partyId' => $reqParams['UserWalletId'], 'requestId' => $userDetails->MobileNumber);
                $requestParams = array('transaction_type' => 'Load', 'p1'  => $userDetails->MobileNumber, 'p2' => $reqParams['amount'], 'p3' => 'Add money to wallet', 'p4' => $reqParams['partRefNum'], 'p5' => 'test');
                $response      = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams, $extraParams);
                $responseData = json_decode($response, TRUE);
                if ($responseData)
                {
                    $transStatus['YesBankCode'] = $responseData['code'];
                    $transStatus['YesBankStatusCode'] = $responseData['status_code'];
                    
                    if ( $responseData['status_code'] !== 'SUCCESS' )
                    {
                        return $transStatus;
                    }

                    //update Yesbank Balance and limit
                    $arrayUpdate                      = array(
                        'YesBankBalance'            => $responseData['balance'],
                        'YesBankRemainBalanceLimit' => $responseData['remaining_load_limit'],
                    );
                    $transStatus['TransactionStatus'] = 'Success';
                    $transStatus['YesBankTransNum'] = $responseData['transaction_reference_number'];
                    $transStatus['YesBankRefNum'] = $responseData['yes_bank_reference_number'];
                    
                    \App\Models\Walletaccount::where('UserWalletId', $reqParams['UserWalletId'])->update($arrayUpdate);
                }
            }
        }
        return $transStatus;
    }

}
