<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Paymentsproduct
 * 
 * @property int $PaymentsProductID
 * @property string $ProductName
 * @property string $ProductType
 * @property \Carbon\Carbon $EffectiveDate
 * @property string $OnlineVendorName
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \Illuminate\Database\Eloquent\Collection $dailytransactionlogs
 * @property \Illuminate\Database\Eloquent\Collection $paymentproductevents
 * @property \Illuminate\Database\Eloquent\Collection $transactiondetails
 *
 * @package App\Models
 */
class Paymentsproduct extends Eloquent
{
	protected $table = 'paymentsproduct';
	protected $primaryKey = 'PaymentsProductID';
	public $timestamps = false;

	protected $dates = [
		'EffectiveDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ProductName',
                'DisplayName',
		'ProductType',
		'EffectiveDate',
		'OnlineVendorName',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function dailytransactionlogs()
	{
		return $this->hasMany(\App\Models\Dailytransactionlog::class, 'PaymentsProductID');
	}

	public function paymentproductevents()
	{
		return $this->hasMany(\App\Models\Paymentproductevent::class, 'PaymentsProductID');
	}

	public function transactiondetails()
	{
		return $this->hasMany(\App\Models\Transactiondetail::class, 'PaymentsProductID');
	}
}
