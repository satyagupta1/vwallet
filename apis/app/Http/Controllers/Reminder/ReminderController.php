<?php

namespace App\Http\Controllers\Reminder;

class ReminderController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->createdBy = config('vwallet.adminTypeKey');
    }

    public function remindBillPayment(\Illuminate\Http\Request $request)
    {

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $select_bill = \App\Models\Userreminder::select(
                        'SourcedUserBillerTransid', 'UserWalletID', 'BillerID', 'BillerName', 'BillAmount', 'ResponseAuthenticator_1', 'ResponseAuthenticator_2', 'ResponseAuthenticator_3', 'ResponseAuthenticator_4', 'Reminder1SentDt', 'Reminder2SentDt', 'Reminder3SentDt'
                )
                ->where('Status', '=', 'Active')
                ->get();
        if ( empty($select_bill) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords') );
            return response()->json($output_arr);
        }
        $output_arr['is_error']            = FALSE;
        $output_arr['display_msg']['info'] = array( 'info' => trans('messages.requestSuccessfull') );
        $output_arr['res_data']            = $select_bill;
        return response()->json($output_arr);
    }

    /*
     *  @Desc get all remindars for a user
     *  @param Request $request
     *  @return JSON
     */

    public function getUserReminders(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateGetReminder($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $userCheck = $this->_userIdValidate($request->input('partyId'));
        if ( $userCheck !== TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return response()->json($output_arr);
        }
        if ( $request->input('fetchBill') == 'Yes' )
        {
            $finalRecords = $this->_remindFetchBill($request->input('partyId'), $request->input('reminderId'));
        }
        else
        {
            $finalRecords = $this->_remindResults($request->input('partyId'), $request->input('reminderId'));
        }
        $countResults           = count($finalRecords);
        $output_arr['is_error'] = FALSE;
        if ( $countResults > 0 )
        {
            $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
            $output_arr['res_data']            = $finalRecords;
        }
        return response()->json($output_arr);
    }

    /*
     *  @Desc Create new reminder
     *  @param Request $request
     *  @return JSON
     */

    public function createReminder(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateReminder($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $date_added = date('Y-m-d H:i:s');
        //DB transactions start from here.
        \DB::beginTransaction();

        try
        {
            // calculate next reminder datetime
            $nextExectionDateTime = \App\Libraries\Helpers::calculateReminderNextExecution($request->input('dateTime'), $request->input('notificationCycleType'), $request->input('notificationCyclePeriod'));

            $data     = array(
                'PartyId'                 => $request->input('partyId'),
                'NotificationCycleType'   => $request->input('notificationCycleType'),
                'NotificationCyclePeriod' => $request->input('notificationCyclePeriod'),
                'NextExectionDateTime'    => $nextExectionDateTime,
                'Status'                  => 'Y',
                'CreatedDateTime'         => $date_added,
                'UpdatedDateTime'         => $date_added,
                'CreatedBy'               => $this->createdBy,
                'UpdatedBy'               => $this->createdBy
            );
            $remindId = \App\Models\Reminders::insertGetId($data);
            $notify   = array(
                'RemiderId'       => $remindId,
                'Status'          => 'Y',
                'Title'           => $request->input('title'),
                'Description'     => $request->input('description'),
                'DateTime'        => $request->input('dateTime'),
                'CreatedDateTime' => $date_added,
                'UpdatedDateTime' => $date_added,
                'CreatedBy'       => $this->createdBy,
                'UpdatedBy'       => $this->createdBy
            );
            \App\Models\Remindernotification::create($notify);
            \DB::commit();
            // all good 
        }
        catch ( \Exception $e )
        {
            \DB::rollback();
            // something went wrong
            $output_arr['display_msg']['info'] = trans('messages.wrong');
            return response()->json($output_arr);
        }
        $message                           = trans('messages.insertedSuccessfully', [ 'name' => 'Reminder' ]);
        $output_arr['is_error']            = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }

    /*
     *  @Desc update user reminder details
     *  @param Request $request
     *  @return JSON
     */

    public function updateReminder(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateUpdateReminder($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $date_added = date('Y-m-d H:i:s');
        $remindId   = $request->input('reminderId');
        $partyId    = $request->input('partyId');
        //$reminders = \App\Models\Reminders::where('PartyId', '=', $request->input('partyId'))->where('ReminderId', '=', $remindId)->first();
        $reminders  = \App\Models\Reminders::where([ 'PartyId' => $partyId, 'ReminderId' => $remindId ])->first();
        if ( $reminders === NULL )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return response()->json($output_arr);
        }
        // calculate next reminder datetime
        $nextExectionDateTime                 = \App\Libraries\Helpers::calculateReminderNextExecution($request->input('dateTime'), $request->input('notificationCycleType'), $request->input('notificationCyclePeriod'));
        $updateQuery                          = \App\Models\Reminders::find($remindId);
        $updateQuery->DateTime                = $request->input('dateTime');
        $updateQuery->NotificationCycleType   = $request->input('notificationCycleType');
        $updateQuery->NotificationCyclePeriod = $request->input('notificationCyclePeriod');
        $updateQuery->NextExectionDateTime    = $nextExectionDateTime;
        $updateQuery->UpdatedDateTime         = $date_added;
        $updateQuery->save();
        \App\Models\Remindernotification::where('RemiderId', '=', $remindId)->where('Status', '=', 'Y')->delete();
        $notify                               = array(
            'RemiderId'       => $remindId,
            'Status'          => 'Y',
            'Title'           => $request->input('title'),
            'Description'     => $request->input('description'),
            'DateTime'        => $request->input('dateTime'),
            'CreatedDateTime' => $date_added,
            'UpdatedDateTime' => $date_added,
            'CreatedBy'       => $this->createdBy,
            'UpdatedBy'       => $this->createdBy
        );
        \App\Models\Remindernotification::create($notify);
        $message                              = trans('messages.updatedSuccessfully', [ 'name' => 'Reminder' ]);
        $output_arr['is_error']               = FALSE;
        $output_arr['display_msg']['info']    = $message;
        return response()->json($output_arr);
    }

    /*
     *  @Desc Soft delete a reminder
     *  @param Request $request
     *  @return JSON
     */

    public function deleteReminder(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateDeleteReminder($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $dateAdded   = date('Y-m-d H:i:s');
        $deleteQuery = \App\Models\Remindtransaction::find($request->input('reminderId'));
        if ( ! $deleteQuery )
        {
            $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
            return response()->json($output_arr);
        }
        $deleteQuery->Status          = 'N';
        $deleteQuery->UpdatedDateTime = $dateAdded;
        $deleteQuery->save();

        $message                           = trans('messages.deletedSuccessfully', [ 'name' => 'Reminder' ]);
        $output_arr['is_error']            = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }

    /*
     *  @Desc Create new reminder
     *  @param Request $request
     *  @return JSON
     */

    public function addReminderTransactions(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateReminderTransaction($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }


        $date_added = date('Y-m-d H:i:s');
        $partyId    = $request->input('partyId');
        //DB transactions start from here.
        \DB::beginTransaction();

        try
        {
        $billerData     = \App\Models\Billdeskbiller::select('BillerID', 'BillerCategory')
                        ->where(array( 'BillerID' => $request->input('billerId') ))
                        ->orderby('BilldeskBillersid', 'DESC')->first();
        $billerType     = 'Prepaid';
        $billerCategory = 'Biller';
        if ( count($billerData) > 0 )
        {
            $billerType     = 'Postpaid';
            $billerCategory = $billerData->BillerCategory;
        }
        else
        {
            $billerData     = \App\Models\Billerlist::select('BillerID', 'SubCategory')
                            ->where(array( 'BillerID' => $request->input('billerId') ))
                            ->orderby('BillerListId', 'DESC')->first();
            $billerCategory = $billerData->SubCategory;
        }
        $dueTimeLine = 0;
        if ( $request->input('dayPeriod') > 0 )
        {
            $dueTimeLine = $request->input('dayPeriod');
        }
        //check plan validataion NA or valid number
        $feePlanValidity = 0;
        if ( $billerType == 'Prepaid' )
        {
            //$request->input('rechargePlanId') != '0' && 
            $feePlandId                        = $request->input('rechargePlanId');
            $feePlan                           = \App\Models\Operatorfeeplan::select('FeePlanID', 'Validity', 'Description')->where('FeePlanID', $feePlandId)->orderby('PlanID', 'DESC')->first();           
           
            $output_arr['res_data']            = array( 'remindTransId' => 0 );
            $output_arr['display_msg']['info'] = trans('messages.unableToRemind');
            if ( count($feePlan) == 0 )
            {
                return response()->json($output_arr);
            }
            $feePlanValidity = $feePlan->Validity;
            if ( $feePlanValidity == 'NA' )
            {
                return response()->json($output_arr);
            }
        }

        $trasactions = \App\Models\Remindtransaction::select('RemindTransactionId')
                        ->where(array( 'PartyId' => $partyId, 'BillerId' => $request->input('billerId'), 'ActionType' => $request->input('actionType'), 'SubscriberNumber' => $request->input('subscriberNumber') ))
                        ->orderby('RemindTransactionId', 'DESC')->first();
        if ( count($trasactions) > 0 )
        {
            $remindTransId                   = $trasactions->RemindTransactionId;
            $updateReminder                  = \App\Models\Remindtransaction::find($remindTransId);
            $updateReminder->UpdatedDateTime = $date_added;
            $updateReminder->Category        = $billerType;
            $updateReminder->BillerCategory  = $billerCategory;
            $updateReminder->DueTimeLine     = $dueTimeLine;
            $updateReminder->Status          = 'Y';
            $updateReminder->save();
        }
        else
        {

            $notify        = array(
                'Status'           => 'Y',
                'PartyId'          => $partyId,
                'Category'         => $billerType,
                'BillerCategory'   => $billerCategory,
                'BillerId'         => $request->input('billerId'),
                'SubscriberNumber' => $request->input('subscriberNumber'),
                'ActionType'       => $request->input('actionType'),
                'DueTimeLine'      => $dueTimeLine,
                'CreatedDateTime'  => $date_added,
                'UpdatedDateTime'  => $date_added,
                'CreatedBy'        => $this->createdBy,
                'UpdatedBy'        => $this->createdBy
            );
            $remindTransId = \App\Models\Remindtransaction::insertGetId($notify);
        }
        if ( $feePlanValidity > 0 )
        {
            $checkValidate = $this->_planedReminder($remindTransId, $feePlan, $partyId);
        }
        \DB::commit();
        // all good 
        }
        catch ( \Exception $e )
        {
            \DB::rollback();
            // something went wrong
            $output_arr['display_msg']['info'] = trans('messages.wrong');
            return response()->json($output_arr);
        }

        $output_arr['is_error']            = FALSE;
        $output_arr['res_data']            = array( 'remindTransId' => $remindTransId );
        $output_arr['display_msg']['info'] = trans('messages.insertedSuccessfully', [ 'name' => 'Reminder' ]);
        return response()->json($output_arr);
    }

    private function _planedReminder($remindTransId, $feePlan, $partyId)
    {
        $dateTime = date('Y-m-d H:i:s');
        $remindId = 0;
        if ( count($feePlan) > 0 )
        {
            $data     = array(
                'PartyId'                 => $partyId,
                'RemindTransactionId'     => $remindTransId,
                'NotificationCycleType'   => 'N',
                'NotificationCyclePeriod' => 0,
                'NextExectionDateTime'    => $dateTime,
                'Status'                  => 'Y',
                'CreatedDateTime'         => $dateTime,
                'UpdatedDateTime'         => $dateTime,
                'CreatedBy'               => $this->createdBy,
                'UpdatedBy'               => $this->createdBy
            );
            $remindId = \App\Models\Reminders::insertGetId($data);

            $notifyArray = array(
                'RemiderId'       => $remindId,
                'Status'          => 'Y',
                'Title'           => 'Recharge Validity',
                'Description'     => $feePlan->Description,
                'DateTime'        => date('Y-m-d', strtotime($feePlan->Validity . ' days', strtotime($dateTime))),
                'CreatedDateTime' => $dateTime,
                'UpdatedDateTime' => $dateTime,
                'CreatedBy'       => $this->createdBy,
                'UpdatedBy'       => $this->createdBy
            );

            \App\Models\Remindernotification::insert($notifyArray);
        }
        return $remindId;
    }

    /**
     * Validate delete user reminder
     * @param $request
     * @return String
     */
    private function _validateDeleteReminder($request)
    {
        $messages           = [];
        $validate           = [
            'partyId'    => 'required|numeric',
            'reminderId' => 'required|numeric',
        ];
        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);
        $validate_response  = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate update reminder request data
     * @param $request
     * @return String
     */
    private function _validateUpdateReminder($request)
    {
        $messages           = [];
        $validate           = [
            'reminderId'              => 'required|numeric',
            'partyId'                 => 'required|numeric',
            'title'                   => 'required',
            'description'             => 'required',
            'dateTime'                => 'required',
            'notificationCycleType'   => 'required|in:H,D,M,Y',
            'notificationCyclePeriod' => 'required|numeric',
        ];
        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate insert reminder request data
     * @param $request
     * @return String
     */
    private function _validateReminder($request)
    {
        $messages           = [];
        $validate           = [
            'partyId'                 => 'required|numeric',
            'title'                   => 'required',
            'description'             => 'required',
            'dateTime'                => 'required',
            'notificationCycleType'   => 'required|in:H,D,M,Y',
            'notificationCyclePeriod' => 'required|numeric',
        ];
        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate get reminder request data
     * @param $request
     * @return String
     */
    private function _validateGetReminder($request)
    {
        $messages = [];
        $validate = [
            'partyId' => 'required|numeric',
        ];
        if ( $request->input('reminderId') !== NULL )
        {
            $validate['reminderId'] = 'required|numeric';
        }
        if ( $request->input('fetchBill') !== NULL )
        {
            $validate['fetchBill'] = 'required|in:Yes';
        }
        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * Validate transaction reminder request data
     * @param $request
     * @return String
     */
    private function _validateReminderTransaction($request)
    {
        $messages = [];
        $validate = [
            'partyId'          => 'required|numeric',
            'billerId'         => 'required',
            'subscriberNumber' => 'required',
            'rechargePlanId'   => 'required',
            'actionType'       => 'required|in:Reminder,AutoPay',
        ];

        if ( trim($request->input('actionType')) == 'AutoPay' )
        {
            $validate['dayPeriod'] = 'required|numeric';
        }
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    private function _userIdValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('UserWalletID')->where('UserWalletID', '=', $partyId)->where('ProfileStatus', '=', 'A');
        if ( ! $userExsist )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * _remindResults query results
     * @param type $partyId
     * @param type $reminderId
     * @return type
     */
    private function _remindResults($partyId, $reminderId = 0)
    {

        $selectColumns = array(
            'remindernotification.ReminderNotificationId',
            'ReminderId',
            'Title',
            'Description',
            'NotificationCycleType',
            'NotificationCyclePeriod',
            'remindernotification.DateTime',
            'remindernotification.Status',
            'remindernotification.CreatedDateTime',
        );
        $clauses       = [ 'PartyId' => $partyId, 'remindernotification.Status' => 'Y' ];
        if ( $reminderId > 0 )
        {
            $clauses = array_merge($clauses, [ 'remindernotification.ReminderNotificationId' => $reminderId ]);
        }
        $currentDate  = date('Y-m-d');
        return $finalRecords = \App\Models\Remindernotification::join('reminders', 'reminders.ReminderId', '=', 'remindernotification.RemiderId')
                        ->select($selectColumns)
                        ->where($clauses)
                        ->whereDate('remindernotification.DateTime', '<=', $currentDate)
                        ->orderBy('remindernotification.DateTime', 'DESC')->get();
    }

    /**
     * _remindFetchBill query results with bill information
     * @param type $partyId
     * @param type $reminderId
     * @return type
     */
    private function _remindFetchBill($partyId, $reminderId = 0)
    {

        $selectColumns = array(
            'reminders.ReminderId',
            'Title',
            'Description',
            'NotificationCycleType',
            'NotificationCyclePeriod',
            'reminders.Status',
            'userreminders.BillerID',
            'userreminders.BillerName',
            'userreminders.BillerCategory',
            'userreminders.BillAmount',
            'userreminders.CreatedDateTime',
        );
        $clauses       = [ 'PartyId' => $partyId ];
        if ( $reminderId > 0 )
        {
            $clauses = array_merge($clauses, [ 'reminders.ReminderId' => $reminderId ]);
        }
        return $finalRecords = \App\Models\Userreminder::join('reminders', 'reminders.ReminderId', '=', 'userreminders.ReminderId')
                        ->select($selectColumns)
                        ->where($clauses)
                        ->orderBy('userreminders.CreatedDateTime', 'DESC')->get();
    }

}
