<?php

/**
 * A Send email with Template file
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */

namespace App\Libraries\Communication;

// Load Library
use App\Libraries\Communication\Functions;
use App\Libraries\Communication\Push\Iphone\PushNowI;
use App\Libraries\Communication\Push\Android\PushNowA;

/**
 * A Sendemail_lib class
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */
class Sendpush_lib {

    /**
     * ---------------------------------------------------------------------
     * send_new method.
     * ---------------------------------------------------------------------
     * The method send new is used for sending email to user with a common
     * email template.
     *
     * @param array $email_data ;
     *                          $email_data = array(
     *                          'email_type_key', // the key of the language file.
     *                          'email_id', // the id on which we have to send message.
     *                          'fullname', // the name of user for which we have to send the email.
     *                          '...', // other array according to language file.
     *                          );
     *
     * @return boolean $email_status;
     * */
    public static function sendNow($pushData = array())
    {
        $dateTime     = date('Y-m-d H:i:s');
        $appNameKey = config('vwallet.applicationNameKey');
        $pushTemplate = trans('communication' . DIRECTORY_SEPARATOR . 'pushtemp.' . $pushData['notification_for'] . '.text');
        $replace_data = trans('communication' . DIRECTORY_SEPARATOR . 'pushtemp.' . $pushData['notification_for'] . '.text_vari');
        // return $pushTemplate;
        if ( ! is_array($replace_data) )
        {
            $replace_data = array();
        }

        $replace_data_length = count($replace_data);
        $replace_count       = 0;
        $replace_with        = array();

        $push_replace_data = json_decode($pushData['push_data']);

        // site_name occurrence
        $site_name_occu = strpos($pushTemplate, '{{site_name}}');
        $site_name      = $pushData['application_name'];
        if ( $site_name_occu !== FALSE )
        {
            $pushTemplate = str_replace('{{site_name}}', $site_name, $pushTemplate);
        }

        if ( in_array('fullname', $replace_data) )
        {
            $replace_with[0]     = ucwords(strtolower($push_replace_data->fullname));
            $replace_data_length = $replace_data_length - 1;
            $replace_count       = $replace_count + 1;
        }
/*
        for ( $i = 0; $i < $replace_data_length; $i ++  )
        {
            $push_data_key = ( int ) $replace_count;
            $key           = $replace_data{$push_data_key};

            $replace_with{$replace_count} = $push_replace_data->$key;
            $replace_count ++;
        }  */
        foreach( $replace_data as $key => $val )
        {
            if( $push_replace_data->$val )
            {
                $replace_with[] = $push_replace_data->$val;
            }
            $replace_data[$key] = '{{' . $val . '}}';
        }
        $final_push_content = str_ireplace($replace_data, $replace_with, $pushTemplate);

      //  $final_push_content = str_ireplace(array( '{{', '}}' ), array( '', '' ), $push_content);
        $send_push_array    = array( 'deviceToken' => $pushData['device_token'], 'message' => $final_push_content, 'dev_mode' => '2', 'application_name' => $pushData['application_name'], 'other_details' => array() );
        $send_push_array['applicationNameKey'] = $appNameKey;
        $ReceipentAcknowledged = 'N';
        if ( $pushData['device_type'] === 'A' )
        {
            $push_status   = PushNowA::sendpush($send_push_array);
            $push_stat_obj = json_decode($push_status);
            if(count($push_stat_obj))
            {
                if ( trim($push_stat_obj->success) === 1 )
                {
                    $ReceipentAcknowledged = 'Y';
                }
                else
                {
                    $ReceipentAcknowledged = 'N';
                }
            }
        }
        else if ( $pushData['device_type'] === 'I' )
        {
            $push_status = PushNowI::sendpush($send_push_array);
            if(count($push_status))
            {
                if ( trim($push_status) === 'delivered' )
                {
                    $ReceipentAcknowledged = 'Y';
                }
                else
                {
                    $ReceipentAcknowledged = 'N';
                }
            }
        }

        // add records into database for log
        $push_notification_log = [
            'PartyID'                 => $pushData['party_id'],
            'LinkedTransactionID'     => $pushData['linked_id'],
            'NotificationType'        => 'Push',
            'SentDateTime'            => $dateTime,
            'ReceipentAcknowledged'   => $ReceipentAcknowledged,
            'ReceipentReceivedDate'   => $dateTime,
            'ReceipentDeliveryReport' => $push_status,
            'CreatedBy'               => config('config_v.CreatedBy'),
            'CreatedDateTime'         => $dateTime,
            'ToAddress'               => $pushData['device_token'],
        ];

        // add records into database for log parameters
        $push_notificationlogparameters = [
            'NotificationID'    => '',
            'TemplateParameter' => $pushData['push_data'],
            'TemplateValue'     => $pushData['notification_for'], //$final_push_content,
            'CreatedDateTime'   => $dateTime,
            'CreatedBy'         => config('communication/config_v.CreatedBy'),
        ];
        Functions::createCommunicationLog($push_notification_log, $push_notificationlogparameters);

        return $push_status;
    }

}

/* End of file Sendemail_lib.php */