<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction system should identify if it is first ever transactions pertaining to the services such as Bill Payments, Recharges in the respective user wallet.</li>
        <li>System should then check if the user perfoming the above transaction has signed up using Viola Link/QR Code/Direct Download from App store.</li>
        <li>If both 1 and 2 are satisfied, then system should credit 100% of the transaction value back in to the user wallet however the cashback shall never exceed INR 100.</li>
    </ul> 
</div>
<?php include 'footer.php';?>