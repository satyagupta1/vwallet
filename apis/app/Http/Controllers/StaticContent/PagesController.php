<?php

namespace App\Http\Controllers\StaticContent;


/**
 * PagesController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class PagesController extends \App\Http\Controllers\Controller {  
    
    public function __construct()
    {
        $this->perpage = config('vwallet.perpage');         
        $this->lang = app('translator')->getLocale();
        $this->createdBy = config('vwallet.adminTypeKey');
    }
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function createPage(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
       
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        $checkValidate = $this->_validatePage($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $date_added                        = date('Y-m-d H:i:s');
        $data                              = array(
            'Title' => $request->input('title'),
            'PageType' => $request->input('pageType'),
            'Content' => $request->input('content'),
            'Status' => 'Active',            
            'CreatedDateTime' => $date_added,
            'UpdatedDateTime' => $date_added,
            'CreatedBy'       => $this->createdBy,
            'UpdatedBy'       => $this->createdBy
        );
        $pageInsert                        = \App\Models\Page::create($data);   
        $attrPage = trans('messages.attributes.page');
        $message = trans('messages.insertedSuccessfully', ['name' => $attrPage]);
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function getPage(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateFields($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $pageId = $request->input('pageId');
        $status          = $request->input('status');
        $orderBy         = $request->input('orderBy');
        $orderByColumn   = $request->input('orderColumn');
        $limit           = $request->input('limit');
        $paginate        = $request->input('paginate');


        $selectColumns = array(
            'PageId',
            'Title',
            'PageType',
            'Content',
            'Status',
            'CreatedDateTime',
            'UpdatedDateTime',
        );

        
        $query = \App\Models\Page::select($selectColumns);
        //filter by security question type
        if ( $pageId !== NULL )
        {
            $query->where('PageId', '=', $pageId);
        }

        //filter by triple click
        if ( $status !== NULL )
        {
            $query->where('Status', '=', $status);
        }


        //Order Specified default DESC        
        if ( $orderBy == NULL )
        {
            $orderBy = 'DESC';
        }
        //Order by table column
        if ( $orderByColumn !== NULL && in_array($orderByColumn, $selectColumns) )
        {
            $query->orderBy($orderByColumn, $orderBy);
        }
        else
        {
            $query->orderBy('PageId', $orderBy);
        }

        //Limit records
        if ( $limit !== NULL )
        {
            $query->limit($limit);
        }

        //results with pagination, results in array         
        if ( $paginate !== NULL && $paginate == 'Y' )
        {
            $finalRecords = $query->paginate($this->perpage)->toArray();
            $countResults = $finalRecords['total'];
        }
        else
        {
            $finalRecords = $query->get()->toArray();
            $countResults = count($finalRecords);
        }

        if ( $countResults > 0 )
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg']['info'] = trans('messages.requestSuccessfull');
            $output_arr['res_data']    = $finalRecords;
            return response()->json($output_arr);
        }

        $output_arr['display_msg'] = array('info' => trans('messages.emptyRecords'));
        return response()->json($output_arr);       
    }
    
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function updatePage(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
       
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        $checkValidate = $this->_validateUpdatePage($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $date_added                        = date('Y-m-d H:i:s');   
        
        $pageInsert = \App\Models\Page::find($request->input('pageId'));
        if(!$pageInsert) {
           $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
           return response()->json($output_arr);
        }
        $pageInsert->Title = $request->input('title');
        $pageInsert->PageType = $request->input('pageType');
        $pageInsert->Content = $request->input('content');
        $pageInsert->Status = $request->input('status');
        $pageInsert->UpdatedDateTime       = $date_added;
        $pageInsert->save();
        
        $attrPage = trans('messages.attributes.page');
        $message = trans('messages.updatedSuccessfully', ['name' => $attrPage]);
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
    
    /*
     *  @Desc This is create pages.
     *  @param Request $request
     *  @return JSON
     */
    public function deletePage(\Illuminate\Http\Request $request)
    {
        $output_arr    = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        
        $adminStatus = $this -> _checkAdmin($request);
        if(!$adminStatus) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.onlyAdminAccess') );
            return response()->json($output_arr);
        }
        $checkValidate = $this->_validateDeletePage($request);
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $dateAdded                        = date('Y-m-d H:i:s');
        $pageInsert = \App\Models\Page::find($request->input('pageId'));
        if(!$pageInsert) {
           $output_arr['display_msg']['info'] = trans('messages.emptyRecords');
           return response()->json($output_arr);
        }       
        $pageInsert->Status = 'Trash';
        $pageInsert->UpdatedDateTime       = $dateAdded;
        $pageInsert->save();
        
        $attrPage = trans('messages.attributes.page');
        $message = trans('messages.deletedSuccessfully', ['name' => $attrPage]);
        $output_arr['is_error'] = FALSE;
        $output_arr['display_msg']['info'] = $message;
        return response()->json($output_arr);
    }
     /**
     * Validate insert page request data
     * @param $request
     * @return String
     */
    private function _validatePage($request)
    {
        $messages = [];
        $validate = [
            //'partyId' => 'required|numeric',
              'title' =>'required',
              'content' =>'required',
              'pageType' =>'required',            
        ];
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.  
       /*$messages = [
        'partyId.required' => trans($langfolder . '/validation.userId'),        
        ];
       */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
     /**
     * Validate update page request data
     * @param $request
     * @return String
     */
    private function _validateUpdatePage($request)
    {
        $messages = [];
        $validate = [
              'pageId' => 'required|numeric',
              'title' =>'required',
              'content' =>'required',
              'pageType' =>'required',
              'status' => 'required|in:Active,Hide,Trash'
        ];
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.  
       /*$messages = [
        'partyId.required' => trans($langfolder . '/validation.userId'),        
        ];
       */
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
     /**
     * Validate update page request data
     * @param $request
     * @return String
     */
    private function _validateDeletePage($request)
    {
        $messages = [];
        $validate = [
              'pageId' => 'required|numeric',              
        ];
         //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    /**
     * Validate Form Fields Data
     * @param $request
     * @return String
     */
    private function _validateFields($request)
    {
        $validate = [];
        if ( $request->input('pageId') !== NULL )
        {
            $validate['questionCategory'] = 'required|numeric';
        }

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    /*
     *  @Desc check if logged in user is admin
     *  @param Request $request
     *  @return JSON
     */
    private function _checkAdmin($request)
    {
       $partyId = $request->input('partyId');
       $userParty = '';
       if( $partyId )
       {
           $selectedColumns = [
               'PartyID',
               'PartyType',
               ];
           $userParty = \App\Models\Party::select($selectedColumns)
                   ->where('PartyId', '=', $partyId)
                   ->where('PartyType', '=', 'ADMIN')->first();
           
           return $userParty;
       }
       return $userParty;
    }
    
    
}
