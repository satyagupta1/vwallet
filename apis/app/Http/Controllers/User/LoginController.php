<?php

/**
 * LoginController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\User;

// JWT Libraries

//use Illuminate\Support\Facades\Crypt;
//use App\Libraries\Crypt\Encryptor;

/**
 * LoginController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class LoginController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->applicationName = config('vwallet.applicationName');
        $this->mdmUrl          = config('vwallet.genrateViolaId');
        $this->userTypeKey     = config('vwallet.userTypeKey');
        $this->applicationName = config('vwallet.applicationName');
        $this->baseUrl         = config('vwallet.baseUrl');
        $this->nonKycLimit     = config('vwallet.MONTH_LIMIT_NON_KYC');
        $this->prefix          = config('vwallet.prefix');
        $this->encKey          = sha1(config('vwallet.ENCRYPT_STATIC'));
    }

    public function checkLogin(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $chk_valid = $this->_validateCheckLogin($request);

        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }
        // reading request params & defining variables 
        //$email = $request->input('email');
        $pass  = $request->input('vPin');
        if ( $request->header('device-type') == 'W' )
        {
            $pass = $request->input('password');
        }


        $results = $this->userProfileData($request);        
        if ( !($results) || $results->ProfileStatus == 'N' )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return response()->json($output_arr);
        }       
        $password = $results->TPin;
        if ( $request->header('device-type') == 'W' )
        {
            $password = $results->Password;
        }
        if ( \Illuminate\Support\Facades\Hash::check($pass, $password) === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.validCredentials') );
            $loginHistory              = \App\Libraries\Helpers::_addLoginHistory($request, $results->UserWalletID, 'N');
            if ( $loginHistory === FALSE )
            {
                $output_arr['display_msg'] = array( 'info' => trans('messages.accountBloked') );
            }
            return response()->json($output_arr);
        }
        // check user Status
        $response = $this->_userStatusCheck($request, $results);
        if ( $response['is_error'] === TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => $response['msge'] );
            return response()->json($output_arr);
        }
        if($results->KycStatus == '01') {        
            return $this->_mobileRegistration($results->MobileNumber);
        }
        $loginHistory = \App\Libraries\Helpers::_addLoginHistory($request, $results->UserWalletID, 'N');

        if ( ($loginHistory === FALSE) || ($loginHistory->FailureCount >= 3) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.accountBloked') );
            return response()->json($output_arr);
        }

        if ( ( $request->header('device-type') != 'W' ) && ($response['otp'] === TRUE) )
        {
            $otpResponse                 = $response['msge'];
            $output_arr['is_error']      = FALSE;
            $output_arr['display_msg']   = array( 'info' => $otpResponse['otpMssage'] );
            unset($otpResponse['otpMssage']);
            $otpResponse['logHistoryId'] = $loginHistory->PartyLoginHistoryId;
            $output_arr['res_data']      = $otpResponse;

            return response()->json($output_arr);
        }

        if ( ($loginHistory === FALSE) || ($loginHistory->FailureCount >= 3) )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.accountBloked') );
            return response()->json($output_arr);
        }

        $loginHistory = \App\Libraries\Helpers::_addLoginHistory($request, $results->UserWalletID, 'Y', FALSE);
        $sessionData  = \App\Libraries\Vwallet::sessionDetails($request, $results->UserWalletID, $loginHistory);

        $output_arr['is_error']       = FALSE;
        $output_arr['display_msg']    = array( 'info' => trans('messages.loginSuccess') );
        $userConfigData               = \App\Libraries\Helpers::userConfigData($results->UserWalletID);
        $userData                     = \App\Libraries\Helpers::userdata($results);
        $returnData                   = array_merge($userData, $sessionData, $userConfigData);
        $returnData['BeneficiaryVPA'] = '';
        $userBeneficiary              = \App\Models\Userbeneficiary::where('UserWalletID', $results->UserWalletID)->first();
        if ( $userBeneficiary )
        {
            $returnData['BeneficiaryVPA'] = $userBeneficiary->BeneficiaryVPA;
        }
        $output_arr['res_data'] = $returnData;
        return response()->json($output_arr);
    }

    private function _userStatusCheck($request, $results)
    {
        // response array
        $msge_array = array( 'msge' => '', 'is_error' => TRUE, 'otp' => FALSE );

        // user profile status check
        switch ( $results->ProfileStatus )
        {
            case 'D':
            case 'B':
                $msge_array['msge'] = trans('messages.accountDeactivated');
                break;
            case 'N':
                $msge_array['msge'] = trans('messages.accountNotActive');
                break;
            case 'F':
                $msge_array['msge'] = trans('messages.accountFrozen');
                break;
        }

        if ( trim($msge_array['msge']) != "" )
        {
            return $msge_array;
        }

        if ( $results->MobileVerified == 'N' && $results->EmailVerified == 'N' && $results->ProfileStatus = 'A' )
        {
            $params      = array(
                'UserWalletID' => $results->UserWalletID,
                'mobileNumber' => $results->MobileNumber,
                'typeCode'     => 'LOG',
                'emailId'      => $results->EmailID,
                'userName'     => ($results->FirstName) ? $results->FirstName : '',
            );
            $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
            if ( $otp_success !== FALSE )
            {
                $msge_array['msge']     = $otp_success;
                $msge_array['otp']      = TRUE;
                $msge_array['is_error'] = FALSE;
            }
            return $msge_array;
        }

        // for active users mobile check up
        if ( ( $request->header('device-type') != 'W' ) && ($results->ProfileStatus == 'A') )
        {
            $msge_array['is_error'] = FALSE;
            $loginHistory           = \App\Models\Partyloginhistory::select('PartyLoginHistoryId')
                            ->where('PartyID', '=', $results->UserWalletID)
                            ->where('LoginSuccess', 'Y')
                            ->orderby('PartyLoginHistoryId', 'DESC')
                            ->first();    

            if ( ! $loginHistory || ($loginHistory->DeviceUniqueID != $request->header('device-uid')) )
            {
                $params      = array(
                    'UserWalletID' => $results->UserWalletID,
                    'mobileNumber' => $results->MobileNumber,
                    'emailId'      => $results->EmailID,
                    'userName'     => ($results->FirstName) ? $results->FirstName : '',
                    'typeCode'     => 'LOG',
                    'attempts'     => 0,
                    'deviceName'   => ($request->header('device-type') == 'I') ? 'IOS' : 'Android'
                );
                $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
                if ( $otp_success !== FALSE )
                {
                    $msge_array['msge'] = $otp_success;
                    $msge_array['otp']  = TRUE;
                }
            }
        }
        else
        {
            $msge_array['is_error'] = FALSE;
        }
        return $msge_array;
    }

    public function validOtp(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // Checking validations
        $valid_res = $this->_validateOTP($request);

        if ( $valid_res === TRUE )
        {
            $date_added   = date('Y-m-d H:i:s');
            $mobile_no    = $request->input('mobileNumber');
            $valid_otp    = trim($request->input('otpNumber'));
            $OTPId        = $request->input('otpId');
            $OTPType      = $request->input('otpType');
            $OTPUserCheck = $request->input('userStatus');
            $logHistoryId = $request->input('logHistoryId');

            // checking user login by mobile / email
            if ( ! filter_var($mobile_no, FILTER_VALIDATE_EMAIL) )
            {
                $usernameField = 'userwallet.MobileNumber';
            }
            else
            {
                $usernameField = 'userwallet.EmailID';
            }

            if ( $OTPType == 'REG' )
            {
                $actionName = 'VERIFYMOTP';
                $extraParams = array('requestId' => $mobile_no);
                $requestParams     = array( 'p1'  => $mobile_no, 'p2' => $OTPId, 'p3' => $valid_otp);
                $response = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams);
                $responseData = json_decode($response, TRUE);
                if ( $responseData['status_code'] == 'SUCCESS' )
                {
                     $userData  = \App\Models\Userwallet::select('UserWalletID', 'ProfileStatus')
                        ->where('MobileNumber', '=', $mobile_no)
                        ->first();  
                    $userRegUpdate             = array( 'MobileVerified' => 'Y', 'UpdatedDateTime' => $date_added );
                    \App\Models\Userregistration::where('UserWalletID', $userData->UserWalletID)->update($userRegUpdate);
                    $responseData['partyId'] = $userData->UserWalletID; 
                    $responseData['message'] = trans('messages.otpValidateSuccess');
                }
                return $output = \App\Libraries\Wallet\Yesbank::handleResponse($responseData);
            }            

            // Get user information

            if ( $OTPUserCheck != 'N' )
            {
                $results = \App\Models\Partyotp::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime', 'partyotp.OTP')
                        ->join('userwallet', 'userwallet.UserWalletID', '=', 'partyotp.PartyID', 'left')
                        ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                        ->where($usernameField, '=', $mobile_no)
                        ->where('ProfileStatus', '=', 'A')
                        ->where('partyotp.OTPRequestID', '=', $OTPId)
                        ->where('partyotp.TransactionTypeCode', '=', "$OTPType")
                        ->orderBy('partyotp.PartyID', 'desc')
                        ->first();
            }

            if ( $OTPUserCheck == 'N' )
            {
                $results = \App\Models\Partyotp::select('partyotp.OTP', 'partyotp.PartyID as UserWalletID')
                        ->where('partyotp.OTPRequestID', '=', $OTPId)
                        ->first();
            }

            if ( $results )
            {
                $otp = json_decode($results->OTP);
                if ( $otp->otp == $valid_otp )
                {
                    if ( $OTPType == 'LOG' )
                    {
                        $logHistoryId              = \App\Libraries\Helpers::updatePartyLoginHistoryStatus($logHistoryId, 'Y');
                        $sessionData               = \App\Libraries\Vwallet::sessionDetails($request, $results->UserWalletID, $logHistoryId);
                        $output_arr['display_msg'] = array( 'info' => trans('messages.validOtpAndLoginSuccess') );
                        $userConfigData            = \App\Libraries\Helpers::userConfigData($results->UserWalletID);
                        $userData                  = \App\Libraries\Helpers::userdata($results);
                        $returnData                = array_merge($userData, $sessionData, $userConfigData);
                        $output_arr['res_data']    = $returnData;
                    }
                    $userRegUpdate             = array( 'MobileVerified' => 'Y', 'UpdatedDateTime' => $date_added );
                    \App\Models\Userregistration::where('UserWalletID', $results->UserWalletID)->update($userRegUpdate);
                    $output_arr['display_msg'] = array( 'info' => trans('messages.attributes.success') );
                    $output_arr['is_error']    = FALSE;
                }
                else
                {
                    $output_arr['display_msg'] = array( 'otpNumber' => 'Please enter valid OTP.' );
                }
                return $output_arr;
            }
            else
            {
                $output_arr['display_msg'] = array( 'mobileNumber' => 'Invalid Data.' );
            }
        }
        else
        {
            $output_arr['display_msg'] = $valid_res;
        }
        return response()->json($output_arr);
    }

    /**
     * Resend Otp.
     *
     * @param Request $request
     * @return JSON
     */
    public function resendOtp(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_array = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $valid_res = $this->_validateResendOTP($request);
        if ( $valid_res === TRUE )
        {
            $mobile_number = $request->input('mobileNumber');
            $OTPType       = $request->input('otpType');
            $OTPId         = $request->input('otpId');

            if($request->input('otpType') == "REG") {
                return $this->_mobileRegistration($mobile_number);
            }
            

            $results = \App\Models\Partyotp::select('partyotp.OTP', 'userwallet.UserWalletID','userwallet.FirstName','userwallet.LastName', 'userwallet.EmailID', 'partyotp.TransactionTypeCode', 'partyotp.OTPRequestID')
                            ->join('userwallet', 'userwallet.UserWalletID', '=', 'partyotp.PartyID')
                            ->where('userwallet.MobileNumber', '=', $mobile_number)
                            ->where('partyotp.OTPRequestID', '=', $OTPId)->first();


            if ( ! empty($results) )
            {
                $otp = json_decode($results->OTP);

                if ( $otp->attempts === 0 )
                {
                    $output_array['display_msg'] = array( 'info' => trans('messages.callMe') );
                }
                else
                {
                    $attmpt = $otp->attempts - 1;
                    $params = array(
                        'OTPRequestID'    => $results->OTPRequestID,
                        'PartyID'         => $OTPId,
                        'UserWalletID'    => $results->UserWalletID,
                        'mobileNumber'    => $mobile_number,
                        'emailId'         => $results->EmailID,
                        'userName'        => ($results->FirstName) ? $results->FirstName : '',
                        'typeCode'        => $OTPType,
                        'attempts'        => $attmpt,
                        'CreatedDateTime' => date('Y-m-d H:i:s A'),
                        'templateKey'     => 'VW002'
                    );

                    $otp_success = \App\Libraries\Helpers::OtpSend($params, $request);
                    if ( $otp_success !== FALSE )
                    {
                        $output_array['is_error']    = FALSE;
                        $output_array['res_data']    = $otp_success;
                        $output_array['display_msg'] = array( 'info' => trans('messages.otpSent') );
                        $sendMobileData              = json_encode(array(
                            'fullname'  => $results->FirstName . ' ' . $results->LastName,
                            'site_name' => $this->applicationName,
                            'otp_code'  => $otp_success['otpNumber'] ));
                        //emails
                        $communicationParams         = array(
                            'party_id'          => $results->UserWalletID,
                            'linked_id'         => '0',
                            'notification_for'  => 'login_otp',
                            'notification_type' => 'sms',
                            'mobile_no'         => $results->MobileNumber,
                            'mobile_data'       => $sendMobileData,
                            'application_name'  => $this->applicationName
                        );

                        //common function to send notification_type msgs.
                        $response = \App\Libraries\Helpers::sendMsg($communicationParams);
                    }
                    else
                    {
                        $output_array['display_msg'] = array( 'info' => trans('messages.wrong') );
                    }
                }
            }
            else
            {
                $output_array['display_msg'] = array( 'info' => trans('messages.enterValidDetails') );
            }
        }
        else
        {
            $output_array['display_msg'] = $valid_res;
        }

        return response()->json($output_array);
    }

    private function _validateCheckLogin($request)
    {
        $usernameCheck = ( strlen($request->input('email')) > 10) ? substr($request->input('email'), 2, 12) : $request->input('email');
        $validate      = array(
            'vPin' => 'required|numeric|digits:4',
        );
        if ( $request->header('device-type') == 'W' )
        {
            $validate = array(
                'password' => 'required|min:8|max:21|password',
            );
        }
        if ( is_numeric($usernameCheck) )
        {
            $validate['email'] = 'required|numeric|digits:10';
        }
        else
        {
            $validate['email'] = 'required|email';
        }

        return $this->_validationResponse($request, $validate);
    }

    private function _validateOTP($request)
    {

        $validate = array(
            'otpNumber' => 'required|numeric|digits:6',
            'otpType'   => 'required|in:LOG,FGP,REG,PIN,VLD',
            'otpId'     => 'required|numeric',
        );

        if ( $request->input('otpType') == 'LOG' )
        {
            $validate['logHistoryId'] = 'required|numeric';
        }

        $usernameCheck = ( strlen($request->input('mobileNumber')) > 10) ? substr($request->input('mobileNumber'), 2, 12) : $request->input('mobileNumber');

        if ( is_numeric($usernameCheck) )
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
        }
        else
        {
            $validate['mobileNumber'] = 'required|email';
        }

        return $this->_validationResponse($request, $validate);
    }

    private function _validateResendOTP($request)
    {
        $validate = array(
            'mobileNumber' => 'required|numeric|digits:10',
            'otpType'      => 'required|in:LOG,FGP,REG,PIN,VLD',
            'otpId'        => 'required|numeric'
        );

        return $this->_validationResponse($request, $validate);
    }

    private function _validationResponse($request, $validate)
    {
        $validate_response = \Validator::make($request->all(), $validate);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    public function userRegistration($request)
    {
        $date_added  = date('Y-m-d H:i:s');
        $data        = array(
            'PartyType'       => "User",
            'CreatedDateTime' => $date_added,
            'UpdatedDateTime' => $date_added );
        $partyInsert = \App\Models\Party::create($data);
        $partyId     = $partyInsert->PartyID;

        $result_name = \App\Libraries\Helpers::splitFullName($request->firstName);
        $referenceId = '';
        if ( ($request->input('social') != '') && ($request->input('email') == '') )
        {
            $referenceId = $request->input('referenceId');
        }
        $data = array(
            'UserWalletID'               => $partyId,
            'FirstName'                  => $result_name['FirstName'],
            'LastName'                   => ($result_name['LastName']) ? $result_name['LastName'] : $result_name['FirstName'],
            'MiddleName'                 => $result_name['MiddleName'],
            'MobileNumber'               => '',
            'EmailID'                    => $request->email,
            'Password'                   => '',
            'TPin'                       => '',
            'WalletPlanTypeId'           => '1',
            'ProfileStatus'              => 'N',
            'IsVerificationCompleted'    => 'N',
            'CreatedDateTime'            => $date_added,
            'UpdatedDateTime'            => $date_added,
            'PreferredPrimaryCurrency'   => 'INR',
            'PreferredSecondaryCurrency' => 'INR',
            'SocialMediaRegistration'    => $request->social,
        );

        $walletDetails = \App\Models\Userwallet::create($data);

        $data2 = array(
            'UserWalletID'            => $partyId,
            'RegisteredReferralCode'  => "",
            'MobileVerified'          => 'N',
            'EmailVerified'           => 'Y',
            'RegistrationCountryCode' => "IND",
            'WalletCountryCode'       => 'IND',
            'TermsAccepted'           => 'Y',
            'TermsAcceptedDateTime'   => $date_added,
            'CreatedDateTime'         => $date_added,
            'UpdatedDateTime'         => $date_added );

        \App\Models\Userregistration::create($data2);

        $data = array( 'UserWalletID' => $partyId, 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added );
        \App\Models\Userconfiguration::create($data);

        $wallet_num = $this->_check_wallet_id(); // Generate wallet number

        $data = array(
            'WalletAccountNumber'     => $wallet_num,
            'WalletAccountPrefix'     => 'VWALLET',
            'UserWalletId'            => $partyId,
            'PrimaryAccountIndicator' => 'Y',
            'AccountLinked'           => 'N',
            'WalletCurrencyCode'      => 'INR',
            'MaximumBalanceLimit'     => '100000',
            'IsAdminWallet'           => 'N',
            'CreatedDateTime'         => $date_added,
            'UpdatedDateTime'         => $date_added
        );
        \App\Models\Walletaccount::create($data);

        //$passwordHistoryData = array( 'PartyID' => $partyId, 'SecretKey' => '', 'SecretKeyType' => 'PASSWORD', 'EffectiveDate' => $date_added, 'Active' => 'Y', 'CreatedDateTime' => $date_added, 'UpdatedDateTime' => $date_added, 'CreatedBy' => $reg_id, 'UpdatedBy' => $reg_id );
        //\App\Models\Partypasswordhistory::create($data);

        return $partyId;
    }

    /**
     * Wallet Number Check
     * @return int
     */
    private function _check_wallet_id()
    {
        $walletid = \App\Models\Walletaccount::where('WalletAccountNumber', '!=', '')->orderBy('UserWalletID', 'DESC')->value('WalletAccountNumber');

        if ( empty($walletid) )
        {
            $account_num = '9100000001';
        }
        else
        {
            $account_num = floatval($walletid) + 1;
        }
        return $account_num;
    }

    private function userProfileData($request)
    {

        // checking user login by mobile / email
        $email = $request->input('email');

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL) )
        {
            $usernameField = 'userwallet.MobileNumber';
        }
        else
        {
            $usernameField = 'userwallet.EmailID';
        }

        $results = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.IsVerificationCompleted', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userwallet.TPin', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userwallet.KycStatus', 'userwallet.WalletStatus', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                        ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                        ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                        ->where($usernameField, '=', $email)->first();

        if ( $results )
        {
            return $results;
        }
        return FALSE;
    }

    private function _validateSocialLoginData($request)
    {
        $validate = array(
            'email'     => 'email|max:100',
            'firstName' => 'required|min:2|max:30|regex:/^[a-zA-z]+([a-zA-Z\ ]+)?+$/',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $valid_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    public function completeSocialRegistration(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $chk_valid = $this->_validateSocialRegistrationData($request);

        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return response()->json($output_arr);
        }

        $results = FALSE;
        if ( ($request->input('vPin') != '') && ($request->input('mobileNumber') != '') )
        {
            $results = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                            ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                            ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                            ->where('userwallet.MobileNumber', '=', $request->input('mobileNumber'))->first();
            if ( $results )
            {
                $output_arr['is_error']                    = FALSE;
                $output_arr['display_msg']['mobileNumber'] = 'Mobile number already exist.';
            }
        }

        if ( ($request->input('vPin') == '') && ($request->input('email') != '') )
        {
            $results = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime')
                            ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                            ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                            ->where('userwallet.EmailID', '=', $request->input('email'))->first();
            if ( $results )
            {
                $output_arr['is_error']             = FALSE;
                $output_arr['display_msg']['email'] = 'Email already exist.';
            }
        }

        if ( $results )
        {

            return response()->json($output_arr);
        }

        if ( $request->input('otpId') == '' )
        {
            $request->merge(array( 'email' => $request->input('mobileNumber') ));

            if ( ! filter_var($request->input('mobileNumber'), FILTER_VALIDATE_EMAIL) )
            {
                $params              = array(
                    'UserWalletID' => $request->input('partyId'),
                    'mobileNumber' => $request->input('mobileNumber'),
                    'typeCode'     => 'REG',
                );
                $communicationParams = array(
                    'party_id'          => $request->input('partyId'),
                    'linked_id'         => $request->input('partyId'),
                    'fullName'          => $request->input('fullName'),
                    'mobile_no'         => $request->input('mobileNumber'),
                    'notification_for'  => 'nreg',
                    'notification_type' => 'sms'
                );

                $data_json = $this->_sendOtp($params, $communicationParams);

                return $data_json;
            }
        }

        if ( $request->input('otpId') )
        {
            // Get user information
            $results = \App\Models\Partyotp::select('userwallet.UserWalletID', 'userwallet.ViolaID', 'userwallet.Password', 'userwallet.FirstName', 'userwallet.LastName', 'userwallet.Gender', 'userwallet.EmailID', 'userwallet.MobileNumber', 'userwallet.ProfileStatus', 'userregistration.MobileVerified', 'userregistration.EmailVerified', 'userregistration.RegistrationCountryCode', 'userregistration.WalletCountryCode', 'userregistration.WalletTimeZone', 'userwallet.DateofBirth', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsPriorityUser', 'userwallet.IsSubAccount', 'userregistration.RegisteredReferralCode', 'walletaccount.PrimaryAccountIndicator', 'walletaccount.MaximumBalanceLimit', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.WalletAccountId', 'walletaccount.IsAdminWallet', 'userwallet.UpdatedDateTime', 'partyotp.OTP')
                    ->join('userwallet', 'userwallet.UserWalletID', '=', 'partyotp.PartyID', 'left')
                    ->join('userregistration', 'userregistration.UserWalletID', '=', 'userwallet.UserWalletID')
                    ->join('walletaccount', 'walletaccount.UserWalletID', '=', 'userwallet.UserWalletID', 'left')
                    ->where('userwallet.UserWalletID', '=', $request->input('partyId'))
                    ->where('partyotp.OTPRequestID', '=', $request->input('otpId'))
                    ->where('partyotp.TransactionTypeCode', '=', "REG")
                    ->orderBy('partyotp.PartyID', 'desc')
                    ->first();

            if ( ! $results )
            {
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array( 'info' => trans('messages.wrongUser') );
                return response()->json($output_arr);
            }
            $otp = json_decode($results->OTP);

            $validOtp = $request->input('otpNumber');
            if ( $otp->otp != $validOtp )
            {
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array( 'info' => trans('messages.validOtp') );
                return response()->json($output_arr);
            }

            $sessionData                 = \App\Libraries\Vwallet::insLogHisDetails($request, $results->UserWalletID);
            $partyDetails                = \App\Models\Userwallet::find($request->input('partyId'));
            $partyDetails->MobileNumber  = $request->input('mobileNumber');
            $partyDetails->EmailID       = $request->input('email');
            $partyDetails->ProfileStatus = 'A';
            $partyDetails->TPin          = \App\Libraries\Crypt\Encryptor::encrypt($request->input('vPin'), sha1(config('vwallet.ENCRYPT_SALT')));
            $partyDetails->save();

            // Get ViolaID Start
            $vparams = array( 'firstName' => $results->FirstName, 'lastName' => ($results->LastName) ? $results->LastName : $results->FirstName, 'mobileNumber' => $request->input('mobileNumber') );

            $violaIdInfo = \App\Libraries\Helpers::curl_send($this->mdmUrl, $vparams);
            $violaIdInfo = json_decode($violaIdInfo);

            if ( $violaIdInfo->is_error === TRUE )
            {
                $data_send['display_msg']['info'] = trans('messages.wrong');
                return $data_send;
            }
            $violaId              = $violaIdInfo->res_data->violaId;
            // Get ViolaID end
            // Update Viola ID
            $userDetails          = \App\Models\Userwallet::find($request->input('partyId'));
            $userDetails->ViolaID = $violaId;
            $userDetails->save();

            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array( 'info' => trans('messages.loginSuccess') );
            $userConfig                = \App\Models\Userconfiguration::select('Details')->where('UserWalletID', $results->UserWalletID)->first()->toArray();
            $defaultOder               = config('vwallet.FASTTRACK_MENU');
            $fastTrackKeys             = array_keys($defaultOder);
            $fastTrackOrder            = implode(",", $fastTrackKeys);
            $userpreferance            = array( 'balanceView' => 'N', 'fastTrack' => $fastTrackOrder );
            if ( $userConfig['Details'] )
            {
                //$userpreferance = array( 'balanceView' => 'N', 'fastTrack' => '0,1,2,3,4,5' );
                $userprefer = json_decode($userConfig['Details']);
                if ( isset($userprefer->balanceView) )
                {
                    $userpreferance['balanceView'] = $userprefer->balanceView;
                }

                if ( isset($userprefer->fastTrack) )
                {
                    $userpreferance['fastTrack'] = $userprefer->fastTrack;
                }
            }
            $userData               = \App\Libraries\Helpers::userdata($results);
            $returnData             = array_merge($userData, $sessionData, $userpreferance);
            $returnData['violaId']  = $violaId;
            $returnData['EmailID']  = $request->input('email');
            $output_arr['res_data'] = $returnData;

            return response()->json($output_arr);
        }
    }

    private function _validateSocialRegistrationData($request)
    {


        $validate = array(
            'vPin'    => 'required|digits:4',
            'partyId' => 'required|numeric|digits_between:1,10',
        );
        if ( $request->input('referenceId') == '' )
        {
            if ( ! filter_var($request->input('mobileNumber'), FILTER_VALIDATE_EMAIL) )
            {
                $validate['mobileNumber'] = 'required|numeric|digits:10';
            }
            else
            {
                $validate['mobileNumber'] = 'required|email';
            }
        }
        else
        {
            $validate['mobileNumber'] = 'required|numeric|digits:10';
            $validate['email']        = 'required|email';
        }

        if ( $request->input('otpId') )
        {
            $validate['otpId']     = 'required|numeric|digits_between:1,10';
            $validate['otpNumber'] = 'required|numeric|digits_between:5,6';
        }

        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $valid_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $valid_response;
    }

    /**
     * Send Otp to user mobile.
     *
     * @param  Request $params
     * @return Json Response 
     */
    private function _sendOtp($params, $communicationParams)
    {
        $otp_success = \App\Libraries\Helpers::OtpSend($params);
        if ( ! $otp_success )
        {
            $data_send = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.otpSentFaild') ), 'status_code' => '200', 'res_data' => array() );
            return response()->json($data_send);
        }
        $communicationParams['mobile_data']      = json_encode(array(
            'fullname'  => $communicationParams['fullName'],
            'site_name' => $this->applicationName,
            'otp_code'  => $otp_success['otpNumber'] ));
        $communicationParams['application_name'] = $this->applicationName;

        //common function to send notification_type msgs.
        $response = \App\Libraries\Helpers::sendMsg($communicationParams);

        $otp_success['mobileNumber'] = $params['mobileNumber'];
        $otp_success['partyId']      = $params['UserWalletID'];

        $data_send = array(
            'is_error'    => FALSE,
            'display_msg' => array( 'info' => trans('messages.otpSentMsg') ),
            'status_code' => '200',
            'res_data'    => $otp_success
        );

        return response()->json($data_send); //sending otp and return response message
    }

        
    /* KYC Registration
     * 
     */

    private function _mobileRegistration($mobileNumber)
    {
        $data_send = array( 'is_error' => FALSE, 'display_msg' => array( 'info' => trans('messages.requestSuccessfull') ), 'status_code' => '200', 'res_data' => array() );
        $actionName    = 'REGUSER';
        $extraParams = array('requestId' => $mobileNumber);
        $requestParams = array( 'p1' => $mobileNumber, 'p2' => 'Y' );
        $response      = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $requestParams, $extraParams);
        $responseData  = json_decode($response, TRUE);
        if ( $responseData['status_code'] == 'SUCCESS' )
        {
            $data_send['is_error'] = FALSE;
            }
        $responseData['otpType'] = "REG";       
        $data_send['display_msg']['info'] = $responseData['message'];
        $data_send['res_data']            = $responseData;        
        return response()->json($data_send);
        }
    }

/* End of file LoginController.php *//* Location: ./controller/User/LoginController.php */
