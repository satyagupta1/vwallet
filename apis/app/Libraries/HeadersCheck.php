<?php

namespace App\Libraries;

class HeadersCheck {

    public static function checkBeforeAfter($path)
    {
        $beforeLoginArray = array(
            'login',
            'countryCode',
            'passwordChange',
            'otpValidate',
            'forgotPassword',
            'resendOtp',
            'registration',
            'registrationVerify',
            'regAadhaar',
            'regAadhaarVerify',
            'referralVerify',
            'getIfscBankDetails',
            'pincodeDetails',
            'sendCommunication',
            'getTermsandConditions',
            'getAntiBriberyandCorruptionPolicy',
            'getCoockiePolicy',
            'getAMLPoliciesandProcedures',
            'getPrivacyPolicy',
            'getAMLPoliciesandProceduresBase',
            'getAMLPoliciesandProceduresDynamic',
            'getAMLPoliciesandProcedures',
            'getRefundPolicy',
            'getRiskPolicy',
            'completeSocialRegistration',
            'mePayServerReq',
            'meTransCollectSvc',
            'transactionStatusQuery',
            'meRefundServerReq',
            'checkVirtualAddressME',
            'validateHash',
            'refundInitiation',
            'getPromotionalOffers',
            'getFaqs',
            'getWebFaqs',
            'getPromoTermsElegibility',
            'checkEmail',
            'circlesList',
            'getoperatorByMobile',
            'operatorList',
            'planscategoriesList',
            'plansDetailsUserList',
            'getBillerDetailsBillpayments',
            'getBillpaymentDetails',
            'getBusList',
            'getBusLayout',
            'getCities',
            'blockicket',
            'getBillerDropdownLookup',
            'getBpDpList',
            'getCitiesStore',
            'insertBillerOffers',
            'getJioPlans',
            'walletPlanConfiguration',
            'getEncryptedKey'
        );

        if ( in_array($path, $beforeLoginArray) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // method for both after login and before login route names
    public static function bothStatges($path)
    {
        $bothStatgesArray = array(
            'pincodeDetails',
            'getPromotionalOffers',
            'getPromoTermsElegibility',
            'getFaqs',
            'getWebFaqs',
            'circlesList',
            'getoperatorByMobile',
            'operatorList',
            'planscategoriesList',
            'plansDetailsUserList',
            'getBillerDetailsBillpayments',
            'getBillpaymentDetails',
            'getBusList',
            'getBusLayout',
            'getCities',
            'blockicket',
            'getBillerDropdownLookup',
            'getBpDpList',
            'generate-pdf',
            'getJioPlans',
            'getEncryptedKey'
        );

        if ( in_array($path, $bothStatgesArray) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}
