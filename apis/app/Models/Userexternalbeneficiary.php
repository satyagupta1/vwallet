<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 31 Oct 2017 11:24:43 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userexternalbeneficiary
 * 
 * @property int $BeneficiaryID
 * @property int $UserWalletID
 * @property int $BeneficiaryWalletID
 * @property string $BeneficiaryType
 * @property string $BankCode
 * @property string $BankIFSCCode
 * @property string $MMID
 * @property string $AccountName
 * @property string $AccountNumber
 * @property string $BeneficiaryMobileNumber
 * @property string $WalletBeneficiaryName
 * @property string $isTripleClickUser
 * @property string $TripleClickAmount
 * @property string $BeneficiaryStatus
 * @property string $Comments
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * @property string $BeneficiaryVPA
 * @property string $BeneficiaryAadhaar
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Bank $bank
 *
 * @package App\Models
 */
class Userexternalbeneficiary extends Eloquent
{
	protected $table = 'userexternalbeneficiary';
	protected $primaryKey = 'BeneficiaryID';
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'BeneficiaryWalletID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'BeneficiaryWalletID',
		'BeneficiaryType',
		'BankCode',
		'BankIFSCCode',
		'MMID',
		'AccountName',
		'AccountNumber',
		'BeneficiaryMobileNumber',
		'WalletBeneficiaryName',
		'isTripleClickUser',
		'TripleClickAmount',
		'BeneficiaryStatus',
		'Comments',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy',
		'BeneficiaryVPA',
		'BeneficiaryAadhaar'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function bank()
	{
		return $this->belongsTo(\App\Models\Bank::class, 'BankCode');
	}
}
