<?php

/**
 * UploadController file
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-card.com Licence
 */

namespace App\Http\Controllers\Ccteam;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

/**
 * UploadController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   https://www.v-card.com Licence
 */
class UploadController extends Controller {

    /**
     * Image Upload
     * @param Request $request
     * @return JSON
     */
    public function ImgUpload(\Illuminate\Http\Request $request)
    {
        $chekc_data = $this->check_valid($request);
        if ( $chekc_data )
        {
                    $output_arr['is_error']    = TRUE;
                    $output_arr['display_msg'] = $chekc_data;
                    $output_arr['Code']        = 202; // Accepted
                    $output_arr['res_data']    = array();
                    return response()->json($output_arr);
        }


        $user_id     = $request->input('custid');
      
        $checkCustId = \App\Models\Userwallet::select('UserWalletID')
                ->where('UserWalletID', $user_id)
                ->count();
        
        if($checkCustId <=0)
        {
            $output_arr['is_error']    = TRUE;
                    $output_arr['display_msg'] = 'No User Found';
                    $output_arr['Code']        = 202; // Accepted
                    $output_arr['res_data']    = array();
                    return response()->json($output_arr);
        }
        
        if ( $checkCustId > 0 )
        {
//        $langfolder = \App\Libraries\Helpers::languages();
            $main_data     = '';
            \Intervention\Image\ImageManagerStatic::configure([ 'driver' => 'gd' ]);
            $imageType     = $request->input('file_type');
            $service_type  = $request->input('service_type');
            $category_code = $request->input('category_code');
            $image         = \Illuminate\Support\Facades\Input::get('userfile');
            $docType       = $request->input('document_type');
            $tcktNo        = $request->input('ticketNo');

            $CRM_data = \App\Libraries\Helpers::crmData();
            $url      = $CRM_data['actionurl'];

            $attachment_url = '';
            $res_data       = '';
            if ( $request->input('service_type') == 'attachment' )
            {

                if ( ! empty($image) )
                {

                    $documentType = $docType;
                    if ( empty($docType) )
                    {
                        $documentType = 'attachment';
                    }


                    // Document Upload
                    $respons = \App\Libraries\UploadDoc::uploadDoc($user_id, $imageType, $image, $documentType);

                    if ( $respons )
                    {
                        $attachment_url      = \App\Libraries\Helpers::apiUrl() . "public/" . $respons['path'] . '/' . $respons['filename'];
                        $document_insert_arr = array(
                            'entryPoint' => 'API',
                            'action'     => 'create',
                            'module'     => 'Documents',
                            'ticketno'   => trim($tcktNo),
                            'url'        => $attachment_url,
                        );

                        $output_attachment = $this->curl_registration($url, $document_insert_arr);
                        $res_data          = array(
                            'imageUrl' => $attachment_url,
                        );

                        $output_arr['is_error']    = FALSE;
                        $output_arr['display_msg'] = array( 'info' => 'Attachment Uploaded Successfully' );
                        $output_arr['Code']        = 201; // image created
                        $output_arr['res_data']    = $res_data;
                        return response()->json($output_arr);

                        //return $this->returnResponse(FALSE, 'Attachment Uploaded Successfully', '', 201, $res_data);
                    }

                    $output_arr['is_error']    = TRUE;
                    $output_arr['display_msg'] = array( 'info' => 'Failed to upload Attachment' );
                    $output_arr['Code']        = 204; // no content
                    $output_arr['res_data']    = array();
                    return response()->json($output_arr);
                }
            }
            else if ( $request->input('service_type') != 'profile' && $request->input('service_type') != 'attachment' )
            {

                /* Create Document */
                $ImageType       = explode('/', $request->input('file_type'));
                $userdetail      = \App\Models\Userwallet::select('UserWalletID', 'FirstName','LastName')->where('UserWalletID', $user_id)->first();
                $user_name       = ($userdetail->FirstName) ? $userdetail->FirstName.' '.$userdetail->LastName : '';
                $fullname        = explode(' ', $user_name);
                $create_document = array( 'user_id'              => ($userdetail->UserWalletID) ? $userdetail->UserWalletID : '',
                    'first_name'           => $fullname[0],
                    'last_name'            => $fullname[1],
                    'type'                 => '2',
                    'language'             => 'en',
                    'status'               => '',
                    'subject'              => 'Identification',
                    'notes'                => 'User Identity',
                    'file_name'            => 'Passport Copy',
                    'file_extension'       => '.' . $ImageType[1],
                    'content'              => $image,
                    'latitude'             => '31.969848',
                    'longitude'            => '35.864097',
                    'position_description' => 'Viola Money Compnay',
                    'custom_notes'         => $ImageType[1] . ' document',
                    'validate'             => '0' );

//                $response          = \App\Libraries\Accomplish::create_document($create_document);
//                $document_response = json_decode($response['message']);
//                if ( $document_response->result->code !== '0000' )
//                {
//                    $output_arr['is_error']    = TRUE;
//                    $output_arr['display_msg'] = array( 'info' => $document_response->result->message );
//                    $output_arr['Code']        = 204; // no content
//                    $output_arr['res_data']    = array();
//                    return response()->json($output_arr);
//                }

                // upload image
                $documentType = $service_type . '-' . $category_code;
                $respons      = \App\Libraries\UploadDoc::uploadDoc($user_id, $imageType, $image, $documentType);

                $crm_path = \App\Libraries\Helpers::apiUrl() . 'public/' . $respons['path'] . '/' . $respons['filename'];

                $InsertData = array(
                    'PartyID'               => $request->input('custid'),
                    'CategoryCode'          => $request->input('category_code'),
                    'DocumentTypeCode'      => $request->input('document_type'),
                    'DocumentUserName'      => $request->input('name'),
                    'IndentificationNumber' => $request->input('identity_num'),
                    'DocumentImage'         => $respons['path'] . '/' .$respons['filename'],
                    'CreatedDateTime'       => date('Y-m-d H:i:s'),
                    'CreatedBy'             => 'User',
                    'Status'                => 'Uploaded'
                );

                $main_data   = \App\Models\PartyDocument::insertGetId($InsertData);
                // update variable
               // $updateArray = ($request->input('category_code') == 'IDP') ? [ 'kycIDPStatus' => 'Pending' ] : [ 'kycADDStatus' => 'Pending' ];
                
                $catagary = '';
                if ( $InsertData['CategoryCode'] === 'IDP' )
                {
                    $catagary = 'id_proof';
                }
                else if ( $InsertData['CategoryCode'] === 'ADD' )
                {
                    $catagary = 'address_proof';
                }

                $url      = \App\Libraries\Helpers::crmData();
                $array    = array( 'entryPoint' => 'API', 'action' => 'create', 'module' => 'Documents', 'ticketno' => '', 'url' => $crm_path, 'custid' => $InsertData['PartyID'], 'category' => $catagary, 'docid' => $main_data );
                $output   = $this->curl_registration($url['actionurl'], $array);
                $data_api = 'fail';
                if ( $output === '"success"' )
                {
                    $res_data                  = array(
                        'imageUrl' => $crm_path,
                    );
                    $output_arr['is_error']    = FALSE;
                    $output_arr['display_msg'] = array( 'info' => 'Kyc Document Uploaded Successfully' );
                    $output_arr['Code']        = 201; // image created
                    $output_arr['res_data']    = $res_data;
                    return response()->json($output_arr);
                }

                $output_arr['crm_respose'] = $data_api;
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array( 'info' => 'Failed To Upload Kyc Document' );
                $output_arr['Code']        = 204; // image created
                $output_arr['res_data']    = array();
                return response()->json($output_arr);
            }
            else
            {
                // upload image
                $documentType   = $service_type . '-' . $category_code;
                $respons        = \App\Libraries\UploadDoc::uploadDoc($user_id, $imageType, $image, $documentType);
                $attachment_url = \App\Libraries\Helpers::apiUrl() . "public/" . $respons['path'] . '/' . $respons['filename'];
                $res_data       = array(
                    'imageUrl' => $attachment_url,
                );

                $updateArray = array(
                    'ProfileImagePath' => $respons['filename'],
                    'UpdatedDateTime'  => date('Y-m-d H:i:s'),
                    'UpdatedBy'        => 'User'
                );

                $update_profile = \App\Models\Userwallet::where('UserWalletID', '=', $request->input('custid'))->update($updateArray);

                if ( $update_profile )
                {
                    $output_arr['is_error']    = FALSE;
                    $output_arr['display_msg'] = array( 'info' => 'Profile Uploaded Successfully' );
                    $output_arr['Code']        = 201; // image created
                    $output_arr['res_data']    = $res_data;
                    return response()->json($output_arr);
                }

                $output_arr['is_error']    = TRUE;
                $output_arr['display_msg'] = array( 'info' => 'failed to upload profile' );
                $output_arr['Code']        = 204; // no content
                $output_arr['res_data']    = array();
                return response()->json($output_arr);
            }
        }
        $output_arr['is_error']    = TRUE;
        $output_arr['display_msg'] = array( 'info' => 'No User Found' );
        $output_arr['Code']        = 201; // image created
        $output_arr['res_data']    = array();
        return response()->json($output_arr);
    }

    /**
     * Validate Image Upload Data
     * @param $request
     * @return String
     */
    private function check_valid($request)
    {
        // $langfolder = \App\Libraries\Helpers::languages();
        $validat  = [
            'service_type' => 'required',
            'userfile'     => 'required',
            'file_type'    => 'required',
            'custid'       => 'required|numeric',
        ];
        $validat1 = array();
        if ( $request->input('service_type') != 'profile' && $request->input('service_type') != 'attachment' )
        {
            $validat1 = [
                'identity_num'  => 'required',
                'name'          => 'required|min:2',
                'document_type' => 'required',
                'category_code' => 'required',
            ];
        }

        $validat2 = array();
        if ( $request->input('service_type') == 'attachment' )
        {
            $validat2 = [
                'ticketNo' => 'required',
            ];
        }


        $validateMerge = array_merge($validat, $validat1, $validat2);
        $messages      = [
            'custid.required'        => 'Customer Id is required',
            'custid.numeric'         => 'Customer Id must be numeric',
            'identity_num.required'  => 'Identity Number Is required',
            'name.required'          => 'Name is required',
            'name.min'               => 'name minimum letters',
            'category_code.required' => 'Category Code is required',
            'document_type.required' => 'DOcument type is required',
            'userfile.required'      => 'User file is required',
            'userfile.json'          => 'user file is in json format',
            'file_type.required'     => 'File type is required',
            'ticketNo.required'      => 'Ticket Number is required',
        ];

        $validator = \Validator::make($request->all(), $validateMerge, $messages);
        $errs      = array();
        foreach ( $validator->messages()->toArray() as $key => $error )
        {
            $errs[$key] = $error[0];
        }
        return $errs;
    }

    /**
     * Process Request from Curl
     * @param $url, $dat
     * @return JSON
     */
    public function curl_registration($url, $data)
    {
        $ch       = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $response = curl_exec($ch);
        $error    = curl_error($ch);
        curl_close($ch);
        return $response;
    }

}
