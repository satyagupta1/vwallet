<?php
    
    /**
     * A Send email with Template file
     * @category  PHP
     * @package   Communication
     * @author    Viola Services (India) PVT LTD <development@violamoney.com>
     * @license   https://violamoney.com Licence
     */
    namespace App\Libraries\Communication;
	
	// Load Library
	use App\Libraries\Communication\Functions;
	use App\Models\Notificationlog;
	use App\Models\CardNotification;
    /**
     * A Sendemail_lib class
     * @category  PHP
     * @package   Communication
     * @author    Viola Services (India) PVT LTD <development@violamoney.com>
     * @license   https://violamoney.com Licence
     */
    
	class Sendweb_lib {

        static protected $language_name = 'webtemp';
        /**
         * ---------------------------------------------------------------------
         * send_new method.
         * ---------------------------------------------------------------------
         * The method send new is used for sending email to user with a common
         * email template.
         *
         * @param array $email_data ;
         *                          $web_data = array(
         *                              'web_type_key', // the key of the language file.
         *                              'party_id', // the id on which we have to send message.
         *                              'fullname', // the name of user for which we have to send the email.
         *                              '...', // other array according to language file.
         *                          );
         *
         * @return boolean $email_status;
         **/
        public static function sendNow($webData = array())
        {
            $dateTime = date('Y-m-d H:i:s');
            $webTemplate = trans('communication'.DIRECTORY_SEPARATOR.'webtemp.'.$webData['notification_for'].'.text');
            $replace_data = trans('communication'.DIRECTORY_SEPARATOR.'webtemp.'.$webData['notification_for'].'.text_vari');
            if ( ! is_array($replace_data) )
            {
                $replace_data = array();
            }
            $replace_data_length = count($replace_data);
            $replace_count = 0;
            $replace_with = array();

            $web_replace_data = json_decode($webData['web_data']);

            // site_name occurrence
            $site_name_occu = strpos($webTemplate, '{{site_name}}');
            $site_name = $webData['application_name'];
            if ($site_name_occu !== FALSE)
            {
                $webTemplate = str_replace('{{site_name}}', $site_name, $webTemplate);
            }
            if (in_array('fullname', $replace_data))
            {
                $replace_with[0] = ucwords(strtolower($web_replace_data->fullname));
                $replace_data_length = $replace_data_length - 1;
                $replace_count = $replace_count + 1;
            }
            
            for ($i = 0; $i < $replace_data_length; $i++)
            {
                $web_data_key = (int) $replace_count;
                $key = $replace_data{$web_data_key};
                $replace_with{$replace_count} = $web_replace_data->$key;
                $replace_count++;
            }
            
            $web_content = str_replace($replace_data, $replace_with, $webTemplate);
            $final_web_content = str_replace(array('{{','}}'), array('', ''), $web_content);
            // Insert notification records.
           /* $actual_nofification = [
                'message_for_id' => $web_replace_data->message_for_id,
                'FromUsers' => $web_replace_data->to_id,
                'idCardUsers' => $web_replace_data->from_id,
                'PayeeRowId' => $web_replace_data->payee_id,
                'TransactionId' => $web_replace_data->linked_id,
                'giftCardId' => $web_replace_data->gift_id,
                'MobileNumber' => $web_replace_data->mobile_number, // if payee is not register with V-Card
                'NotificationType' => $web_replace_data->notification_type,
                'Message' => $final_web_content,
                'MessageStatus' => '0',
                'CreatedDateTime' => $dateTime,
                'CreatedBy' => 'CardUser',
            ];
            CardNotification::insert($actual_nofification);
*/
            // add records into database for log
            $web_notification_log = [
                'PartyID'                   => $webData['party_id'],
                'LinkedTransactionID'       => $webData['linked_id'],
                'NotificationType'          => 'Web',
                'SentDateTime'              => $dateTime,
                'ReceipentAcknowledged'     => 'Y',
                'ReceipentReceivedDate'     => $dateTime,
                'ReceipentDeliveryReport'   => 'Delivered',
                'CreatedBy'                 => config('communication/config_v.CreatedBy'),
                'CreatedDateTime'           => $dateTime,
                'ToAddress'                 => $webData['party_id'],
            ];
            // add records into database for log parameters
            $web_noti_log_param = [
                'NotificationID'            => '',
                'TemplateParameter'         => $webData['web_data'],
                'TemplateValue'             => $webData['notification_for'], //$final_web_content,
                'CreatedDateTime'           => $dateTime,
                'CreatedBy'                 => config('communication/config_v.CreatedBy'),
            ];
            $web_status = Functions::createCommunicationLog($web_notification_log, $web_noti_log_param);

            return $web_status;
        }
    }
    
    /* End of file Sendweb_lib.php */