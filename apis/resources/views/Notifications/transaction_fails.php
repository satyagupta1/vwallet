<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * transaction fails device template
 * 
 *  param $fullname varchar
 *  param $amount varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction Failed',
        'text'    => $salutationName . ',<br/><br/>
                        This is to let you know that your movie ticket booking could not be confirmed. We apologise for the inconvenience caused to you. Please note your Booking ID for reference. Use this for all future communication related to this booking with your Movie operator. 
                        <br/><br/>In case money has been deducted from your wallet , rest assured that your money is safe and will be refunded automatically and should reflect in your wallet/card/account within 10 days depending on your bank.
                        <br/>Book Again'],
    'sms'   => [
        'text' => 'Your payment of INR ' . $amount . ' did not go through. Date Time Booking Number:Movie Ticket Provider',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Movie Ticket Booking cancelled',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Booking Reference Id cancelled',
    ],
];

echo json_encode($array);

/* End of file transaction_fails.php */
