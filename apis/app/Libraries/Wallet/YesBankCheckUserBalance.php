<?php

namespace App\Libraries\Wallet;

class YesBankCheckUserBalance {

    /**
     * YesBank User Balance Check
     * 
     * @param type $request
     */
    public static function CheckYesBankWalletBalance($mobileNumber)
    {
        $data_send = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => '200', 'res_data' => array() );

        $userData = \App\Models\Userwallet::select('UserWalletID')
                        ->where('MobileNumber', '=', $mobileNumber)->first();

        if ( ! $userData )
        {
            $data_send['display_msg'] = array( 'info' => 'User not Found.' );
            return $data_send;
        }

        $actionName   = 'WALLETBAL';
        $params       = array( 'p1' => $mobileNumber );
        $response     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $params);
        $responseData = json_decode($response, TRUE);

        if ( $responseData['status_code'] == 'ERROR' )
        {
            $data_send['display_msg']['info'] = $responseData['message'];
            return $data_send;
        }
        //update Yesbank Balance and limit
        $arrayUpdate = array(
            'YesBankBalance'            => $responseData['balance'],
            'YesBankRemainBalanceLimit' => $responseData['remaining_load_limit'],
        );
        \App\Models\Walletaccount::where('UserWalletId', $userData['UserWalletID'])->update($arrayUpdate);

        $data_send['is_error'] = FALSE;
        $data_send['res_data'] = $responseData;
        return $data_send;
    }

    public static function fundTransferBetweenAccounts($params)
    {
        $fundTransParams = array(
            'p1' => $params['mobileNumber'],
            'p2' => $params['amount'],
            'p6' => $params['beneficiaryId']
        );
        if ( $params['type'] == 'wallet' )
        {
            $fundTransParams['transaction_type'] = 'FundTransfer';
            $fundTransParams['p3']               = $params['tagName'];
        }
        if ( $params['type'] == 'bank' )
        {
            $fundTransParams['transaction_type'] = 'IMPSTransfer';
            $fundTransParams['p4']               = $params['partnerRefNo'];
            $fundTransParams['p5']               = $params['remarks'];
            $fundTransParams['p7']               = $params['feeRate'];
        }

        $actionName   = 'TRANSACTION';
        $response     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $fundTransParams);
        $responseAObj = json_decode($response);
        return $responseAObj;
    }

    public static function addUserBeneficiary($request, $senderMobile)
    {
        // Default Response
        $output_arr        = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $partyId           = $request->input('partyId');
        $beneficiarymobile = $request->input('mobileNumber');
        $nickName          = $request->input('nickName');
        $saveBenf          = $request->input('saveBenf');

        $banificiaryInfo = \App\Libraries\TransctionHelper::_checkBeneficiary($partyId, $beneficiarymobile);
        if ( $banificiaryInfo['is_error'] === TRUE )
        {
            $output_arr['display_msg'] = array( 'info' => 'Please enter valid Beneficiary Mobile Number.' );
            return $output_arr;
        }

        $AccountName      = $banificiaryInfo['FirstName'];
        $userWalletIdBenf = $banificiaryInfo['WalletAccountId'];
        $partyIDBenf      = $banificiaryInfo['UserWalletID'];

        if ( $banificiaryInfo['yesBankBeneficiaryId'] == '' )
        {
            // calling add benenficiary 
            $identifier1 = $beneficiarymobile;
            $benId       = '';
            $extraParams = array(
                'partyId'        => $partyId,
                'referenceId'    => $benId,
                'referenceTable' => 'userbeneficiary',
                'status'         => 'Initiated'
            );

            $ybkReq        = array(
                'mobile'         => $senderMobile,
                'benifisaryName' => ($nickName) ? $nickName : '',
                'IdentifierType' => 'mobile',
                'identifier1'    => $identifier1,
                'identifier2'    => '',
            );
            $outputRespo   = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);
            $reqForYesBank = json_decode($outputRespo);

            if ( isset($reqForYesBank->beneficiary) )
            {
                $beneficiaryId = $reqForYesBank->beneficiary->id;
            }
            else
            {
                if ( $reqForYesBank->status_code == 'ERROR' )
                {
                    $output_arr['display_msg'] = array( 'info' => $reqForYesBank->message );
                    return $output_arr;
                }
                $beneficiaryId = $reqForYesBank->beneficiary_id;
            }

            if ( $saveBenf == 'Y' )
            {
                $insertBeneficiry       = self::addBenficiary($request, $AccountName, $userWalletIdBenf, $partyIDBenf, 'eWallet', $beneficiaryId);
                $output_arr['is_error'] = FALSE;
                $output_arr['res_data'] = $insertBeneficiry;
                return $output_arr;
            }
        }

        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $banificiaryInfo;
        return $output_arr;
    }

    public static function addBenficiary($request, $AccountName, $userWalletId, $partyID, $benfType, $beneficiaryId)
    {
        $insertBeneficiryData = array(
            'BeneficiaryWalletID'      => $userWalletId,
            'UserWalletID'             => $partyID,
            'BeneficiaryStatus'        => 'N',
            'BeneficiaryType'          => $benfType,
            'BeneficiaryMobileNumber'  => ($request->input('mobileNumber')) ? $request->input('mobileNumber') : '',
            'WalletBeneficiaryName'    => $request->input('nickName'),
            'isTripleClickUser'        => 'N',
            'BankIFSCCode'             => $request->input('ifscCode'),
            'MMID'                     => '',
            'AccountName'              => ($AccountName) ? $AccountName : '',
            'AccountNumber'            => $request->input('accountNumber'),
            'BeneficiaryVPA'           => '',
            'CreatedDateTime'          => date('Y-m-s H:i:s'),
            'yesBankBeneficiaryId'     => $beneficiaryId,
            'yesBankBeneficiaryStatus' => 'Y',
        );

        $insertBeneficiry                        = \App\Models\Userbeneficiary::insertGetId($insertBeneficiryData);
        $insertBeneficiryData['beneficiaryData'] = $insertBeneficiry;
        return $insertBeneficiryData;
    }

    public static function _checkBeneficiary($partyId, $BeneficiaryID, $benfType = 'eWallet', $idType = 'mobile', $isTrpleClik = FALSE)
    {
        if ( $benfType == 'eWallet' )
        {
            $selectArray = array(
                'userwallet.FirstName',
                'userwallet.LastName',
                'userwallet.UserWalletID',
                'userbeneficiary.BeneficiaryWalletID',
                'userbeneficiary.BeneficiaryID',
                'userwallet.MobileNumber',
                'walletaccount.WalletAccountId',
                'walletaccount.WalletAccountId',
                'walletaccount.WalletAccountNumber',
                'walletaccount.AvailableBalance',
                'walletaccount.CurrentAmount',
                'userwallet.PreferredPrimaryCurrency',
                'userwallet.IsVerificationCompleted',
                'userwallet.WalletPlanTypeId',
                'userbeneficiary.BeneficiaryWalletID',
                'userbeneficiary.TripleClickAmount',
                'userbeneficiary.isTripleClickUser',
                'userbeneficiary.yesBankBeneficiaryId',
                'userbeneficiary.yesBankBeneficiaryStatus'
            );

            if ( $idType == 'mobile' )
            {
                $whereArray = array(
                    'userbeneficiary.BeneficiaryMobileNumber' => $BeneficiaryID,
                    'userbeneficiary.UserWalletID'            => $partyId
                );
            }

            if ( $idType == 'partyId' )
            {
                $whereArray = array(
                    'userbeneficiary.BeneficiaryWalletID' => $BeneficiaryID,
                    'userbeneficiary.UserWalletID'        => $partyId
                );
            }

            if ( $isTrpleClik !== FALSE )
            {
                $whereArray['userbeneficiary.isTripleClickUser'] = $isTrpleClik;
            }
            $beneficiaryExsist = \App\Models\Userbeneficiary::select($selectArray)
                    ->join('userwallet', 'userwallet.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                    ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                    ->where($whereArray)
                    ->orderBy('userbeneficiary.BeneficiaryID', 'desc')
                    ->first();

            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }

        if ( $benfType == 'Bank' )
        {
            $beneficiaryExsist = \App\Models\Userbeneficiary::select('BeneficiaryID', 'yesBankBeneficiaryId', 'yesBankBeneficiaryStatus', 'UserWalletID', 'BeneficiaryWalletID')
                    ->where('AccountNumber', $BeneficiaryID)
                    ->where('UserWalletID', $partyId)
                    ->first();
            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }

        if ( $benfType == 'Vcard' )
        {
            $beneficiaryExsist = \App\Models\Userbeneficiary::select('BeneficiaryID', 'UserWalletID', 'BeneficiaryWalletID', 'BeneficiaryMobileNumber', 'WalletBeneficiaryName', 'yesBankBeneficiaryId', 'yesBankBeneficiaryStatus')
                    ->where('BeneficiaryID', $BeneficiaryID)
                    ->where('UserWalletID', $partyId)
                    ->first();
            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }
        return FALSE;
    }

}

?>
