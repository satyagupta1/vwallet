<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\ServiceProvider;
//use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;

class HeadersCheck {

    protected $app;

    public function __construct(Auth $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $encKey = config('vwallet.ENCRYPT_SALT');
        $logId  = rand(100000, 999999);
        if ( app('request')->header('language') )
        {
            $appLang = app('request')->header('language');
            app('translator')->setLocale($appLang);
        }
        $this->startTime = microtime(true);
        $logLeaders = $request->headers->all();
        $sessionExpired = trans('messages.sessionExpired');
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ( app('request')->header('test') != TRUE )
        {

            if ( ($request->path() != 'getTermsandConditions') && ($request->path() != 'getPrivacyPolicy') )
            {
                if ( ($request->path() == 'profileUpdate') && ($request->header('device-type') == 'W') )
                {
                    $mergeData = json_decode($request->getContent(), TRUE);
                    $request->merge($mergeData);
                }
                //Decrypt Request data
                $decryptData = \App\Libraries\Helpers::decryptData($request, $encKey);

                
                unset($logLeaders['auth-token']);

                $request_details = 'Request ID : ' . $logId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ', URL : ' . $request->fullUrl() . ', Method-Type : ' . $request->getMethod() . ', Headers : ' . json_encode($logLeaders, JSON_PRETTY_PRINT) . ', IP : ' . $request->getClientIp() . ', Parameters : ' . json_encode($request->input('requestData'), JSON_PRETTY_PRINT);
                \Log::info('Request:  ' . $request_details);
                if ( $decryptData === FALSE )
                {
                    $output_arr['display_msg']['encryptedData'] = trans('messages.unableToProcess');
                    $output_arr                                 = $responseData                               = json_encode($output_arr);
                    if ( app('request')->header('test') != TRUE )
                    {
                        $responseData = \App\Libraries\Helpers::encryptData($output_arr, $encKey, TRUE);
                    }
                    return response($responseData)->header('session-expired', 'N');
                }
            }
        }
        else
        {
            $request_details = 'Request ID : ' . $logId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ', URL : ' . $request->fullUrl() . ', Method-Type : ' . $request->getMethod() . ', Headers : ' . json_encode($logLeaders, JSON_PRETTY_PRINT) . ', IP : ' . $request->getClientIp() . ', Parameters : ' . json_encode($request->input(), JSON_PRETTY_PRINT);
            \Log::info('Request:  ' . $request_details);
        }

        // Clean up the XSS Code
        $clean = \App\Libraries\XSSProtection::xssCleanUp($request);

        if ( ! $clean )
        {
            $output_arr['display_msg']['info'] = trans('messages.unableToProcess');
            $out_arr_enc                       = $responseData                      = json_encode($output_arr);
            if ( app('request')->header('test') != TRUE )
            {
                $responseData = \App\Libraries\Helpers::encryptData($out_arr_enc, $encKey, TRUE);
            }
            return response($responseData)->header('session-expired', 'N');
        }

        $checkBeforeAfter = \App\Libraries\HeadersCheck::checkBeforeAfter($request->path());
        $device_uid       = $request->header('device-uid');
        $device_info      = $request->header('device-info');
        $device_type      = $request->header('device-type');
        $device_token     = $request->header('device-token');
        $session_token    = $request->header('session-token');
        $JWTToken         = $request->header('auth-token');
        $ip_address       = $request->header('ip-address');
        $language         = $request->header('language');
        $nosniff          = $request->header('X-Content-Type-Options');

        if ( $ip_address == '' )
        {
            $output_arr['display_msg']['session-expired'] = $sessionExpired;
        }

        if ( $language == '' )
        {
            $output_arr['display_msg']['session-expired'] = $sessionExpired;
        }

        if ( $device_type == '' )
        {
            $output_arr['display_msg']['session-expired'] = $sessionExpired;
        }

        if ( $checkBeforeAfter === FALSE )
        {
            $partyId = $request->input('partyId');
            if ( $JWTToken == '' )
            {
                $output_arr['display_msg']['session-expired'] = $sessionExpired;
            }

            $request->merge(array( 'token' => $request->header('auth-token') ));
            $token_explode = explode('.', $request->input('token'));
            if ( count($token_explode) >= 3 )
            {
                $token_explode_one = base64_decode(strtr($token_explode[1], '-_', '+/'));
                $token_explode_one = json_decode($token_explode_one);
                if ( $token_explode_one->sub != $partyId )
                {
                    $output_arr['display_msg']['session-expired'] = $sessionExpired;
                    $output_arr                                   = $responseData                                 = json_encode($output_arr);
                    if ( app('request')->header('test') != TRUE )
                    {
                        $responseData = \App\Libraries\Helpers::encryptData($output_arr, $encKey, TRUE);
                    }
                    return response($responseData)->header('session-expired', 'Y');
                }
            }

            if ( $session_token == '' )
            {
                $output_arr['display_msg']['session-token'] = trans('messages.tokenInvalid');
            }
            $deviceType = $request->header('device-type');       
            if ($deviceType != 'W') {
                $deviceType = 'M';
            }
            $sessionDetails = \App\Models\Partyloginhistory::select('DeviceToken', 'DeviceUniqueID', 'SessionID', 'DeviceType', 'MobileType', 'IPAddress')
                            ->where('PartyID', '=', $partyId)
                            ->where('SessionEndTime', '=', NULL)
                            ->where('DeviceType', '=', $deviceType)
                            ->orderBy('PartyLoginHistoryId', 'desc')->first();

            if ( isset($sessionDetails) && count($sessionDetails) > 0 )
            {
                if ( $session_token != $sessionDetails->SessionID )
                {
                    $output_arr['display_msg']['session-expired'] = $sessionExpired;
                    $output_arr                                   = $responseData                                 = json_encode($output_arr);
                    if ( app('request')->header('test') != TRUE )
                    {
                        $responseData = \App\Libraries\Helpers::encryptData($output_arr, $encKey, TRUE);
                    }
                    return response($responseData)->header('session-expired', 'Y');
                }

                if ( ($device_type == 'W') && ($device_type != $sessionDetails->DeviceType) )
                {
                    $output_arr['display_msg']['session-expired'] = $sessionExpired;
                }

                if ( $device_type != 'W' )
                {
                    if ( $device_uid != $sessionDetails->DeviceUniqueID )
                    {
                        $output_arr['display_msg']['session-expired'] = $sessionExpired;
                    }

                    if ( $device_type != $sessionDetails->MobileType )
                    {
                        $output_arr['display_msg']['session-expired'] = $sessionExpired;
                    }

                    if ( $device_token != $sessionDetails->DeviceToken )
                    {
                        $output_arr['display_msg']['session-expired'] = $sessionExpired;
                    }
                }
            }
            else
            {
                $output_arr['display_msg']['session-expired'] = $sessionExpired;
            }
        }

        if ( array_values($output_arr['display_msg']) )
        {
            $output_arr   = $responseData = json_encode($output_arr);
            if ( app('request')->header('test') != TRUE )
            {
                $responseData = \App\Libraries\Helpers::encryptData($output_arr, $encKey, TRUE);
            }

            return response($responseData)->header('session-expired', 'Y');
        }

        $JWTResult = array();
        $jwToken   = NULL;
        if ( ($checkBeforeAfter === TRUE) && ((($request->path() == 'login') || $request->path() == 'regAadhaarVerify' || (($request->path() == 'otpValidate' && $request->input('otpType') == "LOG")))) )
        {
            //generate new token while login
            $JWTResult = $this->_JWTGenerate($request);
        }
        elseif ( $checkBeforeAfter === FALSE )
        {
            //check jwt for authenticate services only.
            $JWTResult = $this->_JWTCheck($request);
        }

        if ( ! empty($JWTResult) )
        {
            if ( $JWTResult['is_error'] === TRUE )
            {
                $output_arr['display_msg'] = array( 'session-expired' => $JWTResult['error_msg'] );
                $output_arr                = $responseData              = json_encode($output_arr);
                if ( app('request')->header('test') != TRUE )
                {
                    $responseData = \App\Libraries\Helpers::encryptData($output_arr, $encKey, TRUE);
                }
                return response($responseData)->header('session-expired', 'Y');
            }
            $jwToken = $JWTResult['error_msg'];
        }

        $response    = $next($request);
        $bothStatges = \App\Libraries\HeadersCheck::bothStatges($request->path());
        if ( $bothStatges === TRUE )
        {
            $jwToken = $request->header('auth-token');
        }
        $response->headers->set('auth-token', $jwToken);
        $response->headers->set('X-Content-Type-Options', 'nosniff');

        $response->headers->set('session-expired', 'N');
        if ( ($request->path() == 'login') || ($request->path() == 'otpValidate' && $request->input('otpType') != "LOG") || ($request->path() == 'regAadhaarVerify') )
        {
            $userData = json_decode($response->getContent(), TRUE);
            if ( $userData['is_error'] === FALSE )
            {
                $userData['res_data']['authToken'] = $jwToken;
                $response->setContent(json_encode($userData));
            }
        }

        $logLeaders   = $response->headers->all();
        unset($logLeaders['auth-token']);
        $responseData = array(
            'Headers' => $logLeaders,
            'Data'    => json_decode($response->getContent())
        );

        $request_details = 'Request ID : ' . $logId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $this->startTime, 3) . ', URL : ' . $request->fullUrl() . ', Method-Type : ' . $request->getMethod() . ', IP : ' . $request->getClientIp() . ', Response : ' . json_encode($responseData, JSON_PRETTY_PRINT);
        \Log::info('Reponse:  ' . $request_details);
        if ( app('request')->header('test') != TRUE )
        {
            //Encrypt response data
            \App\Libraries\Helpers::encryptData($response, $encKey);
        }
        return $response;
    }

    private function _JWTGenerate($request)
    {
        $is_error  = TRUE;
        $error_msg = '';
        $code      = 200;

        $credentials = array(
            'Password' => $request->input('password')
        );
        $email       = ($request->input('email')) ? $request->input('email') : $request->input('mobileNumber');
        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL) )
        {
            $credentials['MobileNumber'] = $email;
        }
        else
        {
            $credentials['EmailID'] = $email;
        }
        $jwt = app(\Tymon\JWTAuth\JWTAuth::class);

        try
        {
            if ( ! $token = $jwt->attempt($credentials) )
            {
                $error_msg = trans('messages.wrongUser');
                $code      = 404;
            }
            else
            {
                $error_msg = $token;
                $is_error  = FALSE;
            }
        }
        catch ( \Tymon\JWTAuth\Exceptions\TokenExpiredException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 301;
        }
        catch ( \Tymon\JWTAuth\Exceptions\TokenInvalidException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 302;
        }
        catch ( \Tymon\JWTAuth\Exceptions\JWTException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 303;
        }

        $output_arr = array( 'is_error' => $is_error, 'error_msg' => $error_msg, 'code' => $code );

        return $output_arr;
    }

    private function _JWTCheck($request)
    {
        $is_error  = TRUE;
        $error_msg = '';
        $code      = 200;

        $old_token = explode('.', $request->input('token'));
        if ( count($old_token) >= 3 )
        {
            $result       = base64_decode(strtr($old_token[1], '-_', '+/'));
            $result       = json_decode($result);
            $current_time = strtotime(date('Y-m-d H:i:s'));
            $extend_time  = date('Y-m-d H:i:s', strtotime('60 minutes'));
            if ( $current_time >= $result->exp )
            {
                $result->exp   = strtotime($extend_time);
                $exp_change    = rtrim(strtr(base64_encode(json_encode($result)), '+/', '-_'), '=');
                $new_signature = sprintf('%s.%s', $old_token[0], $exp_change);
                $old_token[1]  = $exp_change;
                $old_token[2]  = rtrim(strtr(base64_encode(json_encode($new_signature)), '+/', '-_'), '=');
                $request->merge(array( 'token' => implode('.', $old_token) ));
            }
        }
        // Create Instance of JWTAuth Class
        $jwt = app(\Tymon\JWTAuth\JWTAuth::class);
        try
        {
            if ( ! $token = $jwt->parseToken($request)->authenticate() )
            {
                $error_msg = trans('messages.tokenInvalid');
                $code      = 404;
            }
            else
            {
                $error_msg = $request->input('token');
                $is_error  = FALSE;
            }
        }
        catch ( \Tymon\JWTAuth\Exceptions\TokenExpiredException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 301;
        }
        catch ( \Tymon\JWTAuth\Exceptions\TokenInvalidException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 302;
        }
        catch ( \Tymon\JWTAuth\Exceptions\JWTException $e )
        {
            $error_msg = trans('messages.tokenInvalid');
            $code      = 303;
        }

        $output_arr = array( 'is_error' => $is_error, 'error_msg' => $error_msg, 'code' => $code );

        return $output_arr;
    }

    private static function curl_send_sms($url)
    {
        $ch     = curl_init();                       // initialize CURL
        curl_setopt($ch, CURLOPT_POST, false);    // Set CURL Post Data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);                         // Close CURL

        return $output;
    }

}

/* End of file Authenticate.php */ /* Location: ./Middleware/Authenticate.php */
