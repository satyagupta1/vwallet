<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Change E-Mail address template
 * 
 *  param $fullName varchar
 *  param $otpCode varchar
 *  param $email varchar
 *  param $deviceName varchar
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullName) ? $salutation.' '.$fullName : $salutation;
$array = [
    'email' => [
        'subject' => 'Personal Information updated',
        'text'    => $salutationName . ',<br/><br/>
                Your email ID has been changed. If you did not make this request, please contact our Customer Support Team.'],
    'sms'   => [
        'text' => '',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Email ID changed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => 'Your email ID has been changed as per your request',
    ],
];

echo json_encode($array);

/* End of file change_e_mail_address.php */
