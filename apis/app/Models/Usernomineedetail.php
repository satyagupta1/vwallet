<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Usernomineedetail
 * 
 * @property int $UserWalletID
 * @property string $NomineeRelationTypeCode
 * @property string $NomineeName
 * @property \Carbon\Carbon $NomineeDOB
 * @property int $NomineeAddressID
 * @property int $NomineeContactNumber
 * @property string $IsNomineeMinor
 * @property string $NameOfGuardian
 * @property string $MinorGuardianRelationTypeCode
 * @property \Carbon\Carbon $GaurdianDOB
 * @property int $GaurdianAddressID
 * @property int $GuardianContactNumber
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Partyaddress $partyaddress
 * @property \App\Models\Relationshiptype $relationshiptype
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Usernomineedetail extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'NomineeAddressID' => 'int',
		'NomineeContactNumber' => 'int',
		'GaurdianAddressID' => 'int',
		'GuardianContactNumber' => 'int'
	];

	protected $dates = [
		'NomineeDOB',
		'GaurdianDOB',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'NomineeName',
		'NomineeDOB',
		'NomineeAddressID',
		'NomineeContactNumber',
		'IsNomineeMinor',
		'NameOfGuardian',
		'MinorGuardianRelationTypeCode',
		'GaurdianDOB',
		'GaurdianAddressID',
		'GuardianContactNumber',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partyaddress()
	{
		return $this->belongsTo(\App\Models\Partyaddress::class, 'GaurdianAddressID');
	}

	public function relationshiptype()
	{
		return $this->belongsTo(\App\Models\Relationshiptype::class, 'MinorGuardianRelationTypeCode');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
