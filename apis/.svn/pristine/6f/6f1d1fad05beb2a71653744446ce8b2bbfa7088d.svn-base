<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 31 Oct 2017 11:24:43 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Productplanconfig
 * 
 * @property int $ProductPlanChargeDetailsid
 * @property int $PaymentEntityID
 * @property int $PaymentProductID
 * @property int $WalletPlanTypeId
 * @property string $FeeFreqCycle
 * @property int $ConfigParamNum
 * @property string $ActiveFlag
 * @property \Carbon\Carbon $EffectiveDate
 * @property \Carbon\Carbon $ExpiryDate
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Paymentsproduct $paymentsproduct
 * @property \App\Models\Paymententity $paymententity
 * @property \App\Models\Walletplantype $walletplantype
 * @property \App\Models\Productplanconfigparameter $productplanconfigparameter
 *
 * @package App\Models
 */
class Productplanconfig extends Eloquent
{
	protected $table = 'productplanconfig';
	public $timestamps = false;

	protected $casts = [
		'PaymentEntityID' => 'int',
		'PaymentProductID' => 'int',
		'WalletPlanTypeId' => 'int',
		'ConfigParamNum' => 'int'
	];

	protected $dates = [
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'FeeFreqCycle',
		'ConfigParamNum',
		'ActiveFlag',
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function paymentsproduct()
	{
		return $this->belongsTo(\App\Models\Paymentsproduct::class, 'PaymentProductID');
	}

	public function paymententity()
	{
		return $this->belongsTo(\App\Models\Paymententity::class, 'PaymentEntityID');
	}

	public function walletplantype()
	{
		return $this->belongsTo(\App\Models\Walletplantype::class, 'WalletPlanTypeId');
	}

	public function productplanconfigparameter()
	{
		return $this->belongsTo(\App\Models\Productplanconfigparameter::class, 'ConfigParamNum', 'ConfigParamNum');
	}
}
