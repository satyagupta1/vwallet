<?php
namespace App\Libraries\Crypt;

use App\Libraries\Crypt\Cryptor;
use \stdClass;


/**
 * RNEncryptor for PHP
 *
 * Encrypt data interchangeably with Rob Napier's Objective-C implementation
 * of RNCryptor
 */
class Encryptor extends Cryptor
{
    /**
     * Encrypt plaintext using RNCryptor's algorithm
     *
     * @param string $plaintext Text to be encrypted
     * @param string $password Password to use
     * @param int $version (Optional) RNCryptor schema version to use.
     * @throws \Exception If the provided version (if any) is unsupported
     * @return string Encrypted, Base64-encoded string
     */
    public static function encrypt($plaintext, $password, $version = Cryptor::DEFAULT_SCHEMA_VERSION)
    {
        Cryptor::configure($version);

        $components = Encryptor::makeComponents($version);
        $components->headers->encSalt = Encryptor::makeSalt();
        $components->headers->hmacSalt = Encryptor::makeSalt();
        $components->headers->iv = Encryptor::makeIv(Cryptor::$config->ivLength);

        $encKey = Cryptor::makeKey($components->headers->encSalt, $password);
        $hmacKey = Cryptor::makeKey($components->headers->hmacSalt, $password);

        return Encryptor::encryptFromComponents($plaintext, $components, $encKey, $hmacKey);
    }

    public static function encryptWithArbitrarySalts(
        $plaintext,
        $password,
        $encSalt,
        $hmacSalt,
        $iv,
        $version = Cryptor::DEFAULT_SCHEMA_VERSION
    ) {
        Cryptor::configure($version);

        $components = Encryptor::makeComponents($version);
        $components->headers->encSalt = $encSalt;
        $components->headers->hmacSalt = $hmacSalt;
        $components->headers->iv = $iv;

        $encKey = Cryptor::makeKey($components->headers->encSalt, $password);
        $hmacKey = Cryptor::makeKey($components->headers->hmacSalt, $password);

        return Encryptor::encryptFromComponents($plaintext, $components, $encKey, $hmacKey);
    }

    public static function encryptWithArbitraryKeys(
        $plaintext,
        $encKey,
        $hmacKey,
        $iv,
        $version = Cryptor::DEFAULT_SCHEMA_VERSION
    ) {
        Cryptor::configure($version);

        Cryptor::$config->options = 0;

        $components = Encryptor::makeComponents($version);
        $components->headers->iv = $iv;

        return Encryptor::encryptFromComponents($plaintext, $components, $encKey, $hmacKey);
    }

    private static function makeComponents($version)
    {
        $components = new stdClass;
        $components->headers = new stdClass;
        $components->headers->version = chr($version);
        $components->headers->options = chr(Cryptor::$config->options);

        return $components;
    }

    private static function encryptFromComponents($plaintext, stdClass $components, $encKey, $hmacKey)
    {
        $iv = $components->headers->iv;
        if (Cryptor::$config->mode == 'ctr') {
            $components->ciphertext = Cryptor::aesCtrLittleEndianCrypt($plaintext, $encKey, $iv);
        } else {
            $components->ciphertext = Cryptor::encryptInternal($encKey, $plaintext, 'cbc', $iv);
        }

        return base64_encode(''
            . $components->headers->version
            . $components->headers->options
            . (isset($components->headers->encSalt) ? $components->headers->encSalt : '')
            . (isset($components->headers->hmacSalt) ? $components->headers->hmacSalt : '')
            . $components->headers->iv
            . $components->ciphertext
            . Cryptor::makeHmac($components, $hmacKey));
    }

    private static function makeSalt()
    {
        return Encryptor::makeIv(Cryptor::$config->saltLength);
    }

    private static function makeIv($blockSize)
    {
        return openssl_random_pseudo_bytes($blockSize);
    }
}
