<?php

namespace App\Http\Controllers\Payments;

/**
 * PaymentsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */
class ExchangeMoneyController extends \App\Http\Controllers\Controller {

    public function buySellCurrency(\Illuminate\Http\Request $request)
    {
        $response      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $response      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateInputs($request);
        if ( $checkValidate !== TRUE )
        {
            $response['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
         $toCur   = $request->input('toCurrency');
        $fromCur = $request->input('fromCurrency');
        $amount  = $request->input('amount');
        $rateNow              = \App\Libraries\Constant_GRP::BuySellCurrency($fromCur, $toCur, $amount);
        $response['is_error'] = FALSE;
        $response['res_data'] = $rateNow;
        return $response;
    }
    /*
    public static function ExchangeAmount($cur_one, $cur_sec, $amount)
    {
        
      $response = \App\Libraries\Constant_GRP::BuySellCurrency($cur_sec, $cur_one, $amount);

      $p = xml_parser_create();
      xml_parse_into_struct($p, $response, $vals, $index);
      if(!empty($vals[7]['value']))
      {
        $exchangeRate = $vals[7]['value'];
      
        $result = json_decode(json_encode(simplexml_load_string($exchangeRate, "SimpleXMLElement", LIBXML_NOCDATA)),1);

        $sendArray = array('DealNo' => $result['DealNo'], 'SettelementDate' => $result['SettelementDate'],'BuyCurrency' => $result['BuyCurrency'],
                          'BuyAmount' => $result['BuyAmount'], 'SellCurrency' => $result['SellCurrency'],'SellAmount' => $result['SellAmount'],
                          'ExchangeRate' => $result['ExchangeRate'], 'TradeDirection' => $result['TradeDirection'],'TradeRef' => $result['TradeRef']);

        $Response = array(
                'TotalExAmt'    => $result['SellAmount'],                
                'ExFeeAmt'      => 0,
            );
      }
      else
      {
            $ExchangeRate = CurrencyExchange::select('first_currency', 'second_currency', 'exchange_rate', 'fee_type', 'fee_rate')->where([['first_currency', $cur_one], ['second_currency', $cur_sec], ['status', '1']])->first();
            if ($ExchangeRate === NULL)
            {
                return FALSE;
            }
            $ExAmount = $ExchangeRate->exchange_rate * $amount;

            // exchange fees
            $ExAmtFee = $ExchangeRate->fee_rate;
            if ($ExchangeRate->fee_type != '1')
            {
                $Examt = $ExchangeRate->fee_rate * $amount;
                $ExAmtFee = $Examt / 100;
            }
            
            $Response = array(
                'TotalExAmt'    => $ExAmount,
                'ExRate'        => $ExchangeRate->exchange_rate,
                'ExFeeRate'     => $ExchangeRate->fee_rate,
                'ExFeeAmt'      => $ExAmtFee,
                'ExType'        => $ExchangeRate->fee_type
            );
      }
        
        return $Response;
    }
*/
    public function getRate(\Illuminate\Http\Request $request)
    {
        $response      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $checkValidate = $this->_validateInputs($request);
        if ( $checkValidate !== TRUE )
        {
            $response['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $toCur   = $request->input('toCurrency');
        $fromCur = $request->input('fromCurrency');
        $amount  = $request->input('amount');

        //$toCur = 'GBP';
        //$fromCur = 'USD';
        //$amount = '100';  
        $rateNow              = \App\Libraries\Constant_GRP::get_rate($fromCur, $toCur, $amount);
        $response['is_error'] = FALSE;
        $response['res_data'] = $rateNow;
        return $response;
    }

    private function _validateInputs($request)
    {
        $integration   = config('integration.globalreach');
        $currency      = $integration['curArray'];
        $currencyCodes = implode(',', array_keys($currency));
        $messages      = [];
        $validate      = [
            'toCurrency'   => 'required|in:' . $currencyCodes,
            'fromCurrency' => 'required|in:' . $currencyCodes,
            'amount'       => 'required|numeric',
        ];

        //validate if common params passed.        
        $extraParamValidate = \App\Libraries\Helpers::extraParamValid($request);
        $validate           = array_merge($validate, $extraParamValidate);

        //custom messages if required.    
        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}

/* End of file CcavenueController.php */ /* Location: ./Http/Test/TestController.php */
