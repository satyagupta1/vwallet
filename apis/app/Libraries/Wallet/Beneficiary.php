<?php

namespace App\Libraries\Wallet;

class Beneficiary {

    public static function addBenificiary($ybkRequest, $exraParams)
    {
        $actionName = 'ADDBENE';
        $params     = array(
            'p1', // user Mobile number
            'p2', // Benfisary name (optinal)
            'p3', // identifier type (mobile/bank/VPA)
            'p4', // Identifier1 value (mobile number /account number / VPA)
            'p5', // Identifier1 value IFSC (optional In case identifier1 is mobie)
        );

        $request = array_combine($params, $ybkRequest);
        $res     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $request, $exraParams);
        return $res;
    }
    
    public static function fetchBenificiary($ybkRequest, $exraParams)
    {
        $actionName = 'FETCHBENE';
        $params     = array(
            'p1', // user Mobile number
            'p2', // Benfisary Id
        );

        $request = array_combine($params, $ybkRequest);
        $res     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $request, $exraParams);
        return $res;
    }
    
    public static function editBenificiary($ybkRequest, $exraParams)
    {
        $actionName = 'CHANGEBENE';
        $params     = array(
            'p1', // user Mobile number
            'p2', // Benfisary Id
            'p3'
        );

        $request = array_combine($params, $ybkRequest);
        $res     = \App\Libraries\Wallet\Yesbank::handleRequest($actionName, $request, $exraParams);
        return $res;
    }

}
