<?php

namespace App\Http\Controllers\Party;

use App\Http\Controllers\Controller;

/**
 * PartySecurityController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class AddPartySecurityController extends Controller {

    /**
     * This addPartySecurity method is used to get partyId from database
     * 
     * @param Request $request     
     * @return JSON
     */
    public function addPartySecurity(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validateFields($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }
        $countQuestion = $this->_verifyPartyQuestion($request);
        //IF count more than 1 means duplicates. contact 
        /* if($countQuestion > 1) {
          $output_arr['display_msg'] = trans('messages.xxxx');
          return response()->json($output_arr);
          } */

        $result = FALSE;
        if ( $countQuestion == 1 )
        {
            $result = $this->_updatePartySecurity($request);
            $attrName = trans('messages.attributes.update');
            if ( $result !== TRUE )
            {                              
                $output_arr['display_msg'] = array('info' => trans('messages.nameFaild', ['name' => $attrName]));               
            }
            else
            {
                $output_arr['is_error']    = FALSE;                                 
                $output_arr['display_msg'] = array('info' => trans('messages.nameSuccessful', ['name' => $attrName]));                 
            }
        }
        else
        {
            $result = $this->_savePartySecurity($request);
            $attrName = trans('messages.attributes.insert');
            if ( $result !== TRUE )
            {                                 
                $output_arr['display_msg'] = array('info' => trans('messages.nameFaild', ['name' => $attrName]));                
            }
            else
            {
                $output_arr['is_error']    = FALSE;                                
                $output_arr['display_msg'] = array('info' => trans('messages.nameSuccessful', ['name' => $attrName]));
            }
        }

        return response()->json($output_arr);
    }

    /**
     * This _validateFields method is used to validate inputs
     * 
     * @param Request $request     
     * @return Array
     */
    private function _validateFields($request)
    {
        $validate['partyId']    = 'required|numeric';
        $validate['questionId'] = 'required|numeric';
        $validate['answer']      = 'required|regex:/^[a-zA-z0-9]+([a-zA-z0-9\ ]+)?+$/';
        if ( $request->input('questionRank') !== NULL )
        {
            $validate['questionRank'] = 'required|numeric';
        }
        $messages = [];

        $validate_response = \Validator::make($request->all(), $validate, $messages);


        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * This _savePartySecurity method is used to insert party question data
     * 
     * @param Request $request     
     * @return Boolean
     */
    private function _savePartySecurity($request)
    {
        $questionRank = 0;
        if ( $request->input('questionRank') !== NULL )
        {
            $questionRank = $request->input('questionRank');
        }
        $insertData                     = new \App\Models\Partysecurity;
        $insertData->PartyID            = $request->input('partyId');
        $insertData->QuestionId         = $request->input('questionId');
        $insertData->QuestionRankNumber = $questionRank;
        $insertData->Answer             = $request->input('answer');
        $insertData->CreatedDateTime    = date('Y-m-d H:i:s');
        $insertData->CreatedBy          = $request->input('partyId');
        $insertData->UpdatedBy          = $request->input('partyId');
        $insertData->CreatedDateTime    = date('Y-m-d H:i:s');
        $insertData->UpdatedDateTime    = date('Y-m-d H:i:s');
        $save                           = $insertData->save();
        $insertData->save();
        return ($insertData) ? TRUE : FALSE;
    }

    /**
     * This _updatePartySecurity method is used to update party question data
     * 
     * @param Request $request     
     * @return Boolean
     */
    private function _updatePartySecurity($request)
    {
        $questionRank = 0;
        if ( $request->input('questionRank') !== NULL )
        {
            $questionRank = $request->input('questionRank');
        }
        
        
        $affectedRows = \App\Models\Partysecurity::where('PartyID', '=', $request->input('partyId'))
                ->where('QuestionId', '=', $request->input('questionId'))
                ->update(['Answer' => $request->input('answer'),
                    'UpdatedBy' => $request->input('partyId'),
                    'UpdatedDateTime' => date('Y-m-d H:i:s')]);
        return ($affectedRows) ? TRUE : FALSE;
    }

    /**
     * This _getPartyQuestion method is used to get question belongs to party.
     * 
     * @param Request $request     
     * @return Array
     */
    private function _verifyPartyQuestion($request)
    {
        $query = \App\Models\Partysecurity::select([ 'PartyID', 'QuestionId' ])
                ->where('PartyID', '=', $request->input('partyId'))
                ->where('QuestionId', '=', $request->input('questionId'))
                ->count();
        return $query;
    }

}
