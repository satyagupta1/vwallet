<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantqrcode
 * 
 * @property int $MerchantQRCodeID
 * @property string $CodeStatus
 * @property int $MerchantID
 * @property boolean $QRCodeImage
 * @property string $NameOnPoint
 * @property string $MobileNumber
 * @property string $EmailId
 * @property string $Pincode
 * @property string $City
 * @property string $state
 * @property string $Building
 * @property string $Colony
 * @property string $Landmark
 * @property string $Status
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 *
 * @package App\Models
 */
class Merchantqrcode extends Eloquent
{
	protected $table = 'merchantqrcode';
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int',
		'QRCodeImage' => 'boolean'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CodeStatus',
		'QRCodeImage',
		'NameOnPoint',
		'MobileNumber',
		'EmailId',
		'Pincode',
		'City',
		'state',
		'Building',
		'Colony',
		'Landmark',
		'Status',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}
}
