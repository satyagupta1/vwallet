# ANTI-MONEY LAUNDERING
**1.  COMPANY POLICY**

Money Laundering is the process of disguising the origin of the proceeds of crime.   Terrorist financing provides funds for terrorist activity. The use of products and services by money launderers and terrorists exposes Viola Group Limited to significant criminal, regulatory and reputational risk.

This policy is designed to provide direction to staff on the approach and management of Anti-Money Laundering ( **AML** ) and Counter-Terrorist Financing ( **CTF** ) within Viola Group. This policy supports management&#39;s objective of mitigating the following risks:

- Money laundering;
- Terrorist financing;
- Sanctions;
- Politically exposed persons ( **PEPs** );
- Legal and regulatory risk.

This policy applies to all individuals at all levels of Viola Group, including senior managers, officers, directors, employees, consultants, contractors, trainees, homeworkers, part-time and fixed-term workers, casual and agency staff, all of whom are collectively referred to as &#39;staff&#39; in this document.

Senior management of Viola Group will provide direction to, and oversight of the AML and CTF strategy as well as apply a risk-based approach across the business.

Viola Group will adopt a risk-based approach to managing the risks presented by the business, in line with the current Joint Money Laundering Steering Group ( **JMLSG** ) guidance.

Viola Group senior management will allocate a senior individual to be the Money Laundering Reporting Officer ( **MLRO** ).  The MLRO will have overall responsibility for the establishment and maintenance of Viola Group&#39;s AML/CTF systems and controls and will report to the Viola Group Board.

The Viola Group AML/CTF procedures will be in line with the requirements of the UK:

- The Proceeds of Crime Act 2002,
- Terrorism Act 2000 and 2001,
- Money Laundering Regulations 2007,
- Counter-Terrorism Act 2008,
- HM Treasury Sanction Notices
- FCA Handbook,
- JMLSG Guidance

**1.1. Money laundering**
There are three broad groups of offences related to money laundering that firms need to avoid committing. These are:

- knowingly assisting (in a number of specified ways) in concealing, or entering into arrangements for the acquisition, use, and/or possession of, criminal property;
- failing to report knowledge, suspicion, or where there are reasonable grounds for knowing or suspecting, that another person is engaged in money laundering; and
- tipping off, or prejudicing an investigation.

**1.2. Terrorist financing**
There can be considerable similarities between the movement of terrorist property and the laundering of criminal property: some terrorist groups are known to have well established links with organised criminal activity.  However, there are two major differences between terrorist property and criminal property more generally:

- ften only small amounts are required to commit individual terrorist acts, thus increasing the difficulty of tracking the terrorist property;
- terrorists can be funded from legitimately obtained income, including charitable donations, and it is extremely difficult to identify the stage at which legitimate funds become terrorist property.

**2. Risk-based approach**
The risk-based approach takes the most cost effective and proportionate way to manage and mitigate money laundering and terrorist financing risks.

Viola Group will identify the money laundering and terrorist risks presented by:

- Customers;
- Business type;
- Beneficial owners;
- Products;
- Geographical areas of operation.

Consequently, Viola Group will:

- Design and implement controls to manage and mitigate those risks;
- Monitor and seek to improve the operation of these controls, including those of third party client merchants;
- Record what has been done.
-

The risk-based approach will recognise that the money laundering and terrorist financing risks vary according to customers, jurisdictions, products and method of delivery.

Viola Group will implement procedures to manage and mitigate those risks most efficiently and cost effectively.  The procedures will include, but will not be restricted to:

- identification and verification of customer identity
- identification and verification of the business entity
- monitoring of transactions
- review and of customers&#39; ( **KYC and KYB** ) data according to the risk level presented by the customer and the business
- appropriate training of all staff
- monitoring of arrangements and output by the Compliance Department
- producing appropriate management information
- reporting upward
- effective liaison with third party client merchants and other parts of the business
- maintaining up to date knowledge of regulation and legislation.
-

Viola Group recognises that risks change over time and will continually and regularly update its risk management procedures.

**3. MLRO Role and Responsibilities**

Viola Group has an appointed Money Laundering Reporting Officer ( **MLRO** ) who reports directly to the Board of Directors.  The MLRO, with the support of the Board, is responsible for ensuring that the Company meets the AML compliance requirements set by the FCA.  The MLRO oversees the AML systems and controls.  The main activities of the MLRO comprise, but are not limited to, the following:

- versight of all aspects of the Company&#39;s AML and CTF activities;
- focal point for all activities within the Company relating to AML and CTF;
- establishing the basis on which a risk-based approach to the prevention of money laundering and terrorist financing is put into practice;
- supporting and co-ordinating senior management focus on managing the money laundering/terrorist financing risk in individual business areas; and
- ensuring that the Company&#39;s wider responsibility for AML/CTF is addressed centrally through appropriate training activities.

The MLRO is also required to produce reports for Board meetings including, but not limited to, the following items:

- Confirmation that adequate customer due diligence information is being collected and that ongoing monitoring is taking place;
- Summary data relating to complex or unusual transactions;
- Number of internal consents / Suspicious Activity Reports ( **SARs** ) received from staff members;
- Number of SARs made to the National Crime Agency ( **NCA** );
- Information on status of staff training within the company;
- Confirmation that all business records have been properly stored and are retained according to regulatory requirements;
- Changes in the law/operating environment which are or might impact the business;
- Changes in the risk matrix affecting the business; and
- Contacts with the regulators.

**4. Training**
All employees will be made aware through the annual compulsory training programme of:

- The risks of money laundering and terrorist financing, the relevant legislation, and their obligations under that legislation;
- The identity and responsibilities of Viola Group&#39;s nominated officer (MLRO);
- Viola Group&#39;s procedures in how to recognise and deal with potential money laundering or terrorist financing transactions or activity.

Staff training on anti-money laundering and counter terrorist financing will be carried out annually for all staff, and details recorded.

The MLRO is responsible for oversight of Viola Group&#39;s compliance with its requirements in respect of staff training.

The relevant director or senior manager has overall responsibility for the establishment and maintenance of effective training arrangements.

Training covers, at minimum, the following subjects focusing on the relevance to and implications for the Company:

- the risks of money laundering and terrorist financing, the relevant legislation, and their obligations under that legislation
- the identity and responsibilities of Viola Group&#39;s nominated officer and MLRO
- the firm&#39;s procedures and in how to recognise and deal with potential money laundering or terrorist financing transactions or activity
- Staff training given at regular intervals, and details recorded
- MLRO responsibility for oversight of the firm&#39;s compliance with its requirements in respect of staff training
- the relevant director or senior manager has overall responsibility for the establishment and maintenance of effective training arrangements

**5. Customer Journey**
Below it is displayed a break-down of the wallet&#39;s functionalities for both individual and corporate customers.
![Customer Journey]( BASE_IMAGE_URL/AML_Customer_Journey.png )

**6. Sanctions and PEPs screening**
Viola Group will make use of an e-KYC service provider to screen applicants against high risk business partners, terrorists, and politically exposed persons ( **PEPs** ).  These checks will also involve both senders and recipients of the money remittance service.

Individuals will be screened through the World Check database monthly as well as screened on initial sign up.

**6.1 Sanctions Lists**
Information leading to &quot;fuzzy matches&quot; will be investigated further, where the match was related to a name which can be deemed as popular, and this will be compared against the other information that is collected at point of registration.  The full evaluation of the of customer&#39;s data will provide a result.

Viola Group will take all reasonable steps to ensure that all customers with whom a business relationship is established are screened against relevant notices published by:
- the Office of Foreign Assets Control ( **OFAC** )
- the Financial Action Task Force ( **FATF** )
- Her Majesty&#39;s Treasury Department – UK ( **HMT** )
- European Union sanctions ( **EU** )
- United Nations sanctions ( **UN** )
- Australia&#39;s Department of Foreign Affairs &amp; Trade ( **DFAT** )

**6.2 PEPs Screening**
The definition of a &#39;PEP&#39; is set out below:

- is or has, at any time in the preceding year, been entrusted with prominent public functions;
- is an immediate family member of such a person;
- is a known associate of such a person;
- is or has, at any time in the preceding year, been entrusted with a prominent public function by

- a state other than the UK;
- the European Community; or
- an international body; or

- is an immediate family member or a known close associate of a person referred to in the paragraph immediately above?

If a PEP were identified and it is validated, the customer&#39;s account would be suspended immediately and the funds retained.  Upon identification of a PEP, Enhanced Due Diligence will be applied.  Executive management will decide on a case by case basis whether to open PEP accounts based on the risk to the business. PEPs accounts will not be declassified once their tenure in a public office has expired.  The MLRO will include within the annual MLRO report to the board any cases identifiedduring the year as PEPs

**7. Know Your Customer (KYC)**
The first requirement of knowing your customer for money laundering purposes is to be satisfied that a prospective customer is who they claim to be.

The customer due diligence carried out by Viola Group will be referred to as Know Your Customer ( **KYC** ).  Viola Group will carry out appropriate KYC procedures for all customers (individuals and corporates).  The objective of the KYC process is to ensure Viola Group holds appropriate information to be able to satisfactorily know who Viola Group is dealing with, or can obtain such information from third party client merchants where this process is outsourced.

Viola Group will apply KYC measures at the following points:

- Before it establishes a business relationship;
- Suspects money laundering or terrorist financing;
- Doubts arise about the legitimacy of documents previously supplied for purposes of identification or verification.

Customer information will be periodically reviewed using a risk-based approach to keep customer information up to date.



**7.1 Customer Registration**

**7.1.1 Individual customers**
At point of registration within the website, Viola Group will capture the following customer information:

- Full name
- Address
- Date of birth
- Email address
- Mobile number

At this stage, Viola Group will make use of an e-KYC service provider to screen applicant&#39;s devices which link to the internet, then against high risk business partners, terrorists, and politically exposed persons ( **PEPs** ).  This is also applicable to senders and recipients requesting money remittance service only.

In addition, the system will automatically detect if the phone number/email address entered are already assigned to another account. Viola Group will only permit one main account per person and so the details provided will be checked against the existing member database to confirm that they do not already hold an account, and they were not previously unsuccessful. Viola Group will then use an e-KYC to verify client&#39;s documents.

**7.1.2 Corporate customers**
At the point of registration, Viola Group will perform a rigorous risk assessment and a business pre-acceptance process.  The extent of the Customer Due Diligence ( **CDD** ) is determined according to calculated customers risk levels/groups, turnover values, publicly available information, etc.

In the framework of the CDD, the manual analysis is used along with the full range of internal and external IT solutions.

Viola Group utilizes a pre-boarding application process in which the prospective business provides key business information. This assessment will determine if the business can be under consideration to be on boarded.

At this stage, directors and beneficial owners will be checked against any Sanction and PEPs lists.

**7.2 Simplified Due Diligence (SDD)**
All customers (individual and corporate) who have completed the above registration process maybe assigned with a wallet under SDD. Decision is made based on a risk assessment of the applicant. Viola Group will identify and assess risks associated to each customer (individual or corporate) based on the following factors:

- Type of customer
- Countries or geographic area
- E-wallet functionality
- Transactions (in terms of volume and frequency expected)
- Delivery channels (how the customer has been introduced to Viola Group)

Customers, who successfully pass the risk assessment, are assigned with a wallet and one or more prepaid cards attached. Under SDD, the wallet is still subject to the following regulatory limits in line with the Fourth Anti-Money Laundering Directive (AMLD 4):

- annual turnover limit: equal or less than €250 [£200]
- annual redemption limit: equal or less than €250 [£200]
- single transaction limit: equal or less than €250 [200]

Should the customer reach the value limits of an unverified account prior to completing verification, no further transactions will be permitted until that process is complete.  This will be systematically enforced and will raise an alert for the Risk and Compliance team to contact the individual to discuss completing verification.

Management will decide on a case by case basis whether to open PEP accounts based on the risk to the business.  PEPs accounts will not be declassified once their tenure in a public office has expired. The MLRO will include within the annual MLRO report to the board any cases identified during the year as PEPs.

**7.3 Full Due Diligence (FDD)**

**7.3.1 Individual customers**
A customer can increase the functionality of their account before reaching the threshold limits. Otherwise, if an account reaches one or more of the value limits the account will be &#39;Locked - pending verification&#39; and the individual must complete the verification process successfully to resume the service.  A Verified account is also subject to limits in terms of activity.

A verified individual customer is still subject to limits and ongoing monitoring based on the risk rating.  For the money remittance service, it is planned to verify only senders, who are Viola Group customers, while recipients are subject to Sanctions and PEPs screening only.

To become verified, individual customers will be screening electronically using an e-KYC provider. This will be achieved by requiring details that allow a 2+2 KYC check in accordance with the JMLSG guidance notes as follows:

- ne match on an individual&#39;s full name and current address, and
- a second match on an individual&#39;s full name and his date of birth.

If the customer&#39;s identity cannot be verified electronically, there is a manual process utilizing physical documents which the customer must submit for review.

Where Viola Group needs to verify the customer manually, applicants will be required to provide a proof of identity as well as a proof of address (see Appendix .11.1.).

Documents can be sent in the following formats:

- Photocopy of the original – posted to Viola Group&#39;s office
- Scan and email copy to a secure address
- Photograph copy using the camera on a smartphone and sent to a secure email location. As we are using Accomplish Financials licence – they will retain the customer&#39;s documentation. However, Viola Group need to verify and then forward to Accomplish to approve.

Once the documents are received, Viola Group will verify these via the in-house risk team. Following a detailed review of the copies received, a decision will be made.

If the Customer cannot be verified, further access to Viola Group services will not be permitted. Consequently, the account will be &#39;locked&#39; and future use is therefore prohibited.

Following a verification failure, the Risk and Compliance team will review the reason for failure and take the appropriate course of action, including possible escalation to the MLRO and feedback to Senior Managers, up to submission of a SAR

**7.3.2 Corporate Customers**
Corporate customer&#39;s activity will be still subject to limits and ongoing monitoring based on the risk rating.

To become verified, a corporate customer is required to provide the following documentation:

- Formal Corporate Application.
- Corporate documents:
- Memorandum and Articles of Association,
- Certificate of Incorporation.
- Bank statements for last three (3) months.
- Key contact information within the applicant&#39;s organisation, including telephone numbers and email addresses of key personnel.
- Bank reference letter for the business client dated within the last three (3) months; a voided bank account check.
- Bank reference for all Directors and any Beneficial Owner of more than ten percent (25%) of the company and any manager having significant management control of the merchant.
- Proof of Identity of each director and beneficial owner (if the e-KYC fails)
- Proof of address of each director and beneficial owner (if the e-KYC fails)
- Audited financial statements (latest Balance Sheet and Profit and Loss Statement).  If the merchant is a new business and / or does not have audited financial statements, they must provide a Business Plan including projected transaction numbers and volumes.
- Tax returns for the last two (2) years. As per requirement 12 above, the prospective business must provide a Business Plan including projected transaction numbers and volumes.
- Copy of the AML and policy and KYC procedures in place for on-boarding and monitoring their own customers

Viola Group evaluates all submitted documentation to ensure that it is current and valid. In addition, Viola Group performs many additional checks such as, searching the Internet for information on the prospective business and its directors, owners and management.

**7.4 Limits Under FDD**
FDD for personal customers will allow the following: -

- Annual turnover limit: equal and less than £30,000
- Load limits:  maximum £2500
- Single transaction limit: £2500

FDD will allow the following for Corporate Customers wishing to use of cards for Expenses only:

- Annual turnover limit: equal and less than £75,000
- Load limits: maximum £30,000
- Single transaction limit: £5000
- Maximum load per day: £10,000

**7.5 Enhanced Due Diligence (EDD)**
Enhanced due diligence is required in circumstances giving rise to an overall higher risk.

When assessing the ML/TF risks the following factors will be considered:

- types of customer (PEPs)
- countries or geographic areas (countries identified by credible sources as not having adequate AML/CTF approaches)
- products functionality (unverified source of funding)
- transactions (anomalies or the unexpected use of the product, discrepancies between submitted and detected information)
- delivery channels (customers introduced by third parties)

The Money Laundering Regulations 2007 have specifically identified PEPS as higher risk.

Where a customer is assessed as carrying a higher risk, V-Wallet will seek additional information in respect of the customer, to be better able to judge whether the higher risk that the customer is perceived to present is likely to materialise. Such additional information will include an understanding of where the customer&#39;s funds and wealth have come from. Accomplish Financial will be completing our Sanction and PEP&#39;S verification initially.

**7.6 List of Banned Industries**
Customers will not be allowed to make transactions with certain merchants operating under particular Merchant Category Codes ( **MCCs** ).  Any attempted transaction at these categories of merchants will be blocked based on the MCC.



**7.7 Control Over the Loading Instrument**
Viola Group will carry out the following checks:

- Bank account loads – incoming transfer must come from a bank account registered under the customers&#39; name. In order to validate that the customer is in fact the bank account holder, Viola Group requires the customer to submit a copy of their current bank statement (dated within the past 3 months) for the first load. This verification will be completed by Barclays Bank through Accomplish Finance.
- P2P loads – subject to internal transaction monitoring rules

**7.8 Controls Over The Wallet&#39;s Functionalities**
Viola Group will carry out the following checks:

- P2P transfers – subject to internal transaction monitoring rules
- Online purchases with the Prepaid card – subject to internal transaction monitoring rules where customers will not be allowed to make transactions with certain merchants operating under particular Merchant Category Codes (MCCs). Any attempted transaction at these category of merchants will be blocked based on the MCC.
- Transfers to bank accounts - under Wire Transfers Regulations, Viola Group will make sure that wire transfers are compliant with the relevant information requirement however they will not become involved in the verification of the payee.

**7.9 Controls Over Withdrawals**
Viola Group will carry out the following checks:

- Transfers back to the customer&#39;s bank account – subject to internal transaction monitoring rules (only if refund due to error in loading card)
- ATM withdrawals using the Prepaid Card– subject to internal transaction monitoring rules

In the case of refund requests due to account closure, funds will only be returned to a bank account in the customer&#39;s name, and original souce of funs. In order to validate that the customer is in fact the bank account holder, Viola Group requires the customer to submit a copy of their current bank statement (dated within the past 3 months). Where suspicious grounds are established, a suspicious transaction will be reported to the NCA, as a SAR.







**7.10 Controls Over Money Remittance Service**
Under Wire Transfers Regulations, Viola Group will make sure that wire transfers are compliant with the relevant information requirement however they will not become involved in the verification of the recipients.

Transactions will be subject to internal transaction monitoring rules.

**8. Transaction Monitoring**
Viola Group will use a combination of system alerts and blocks to facilitate effective transaction monitoring.  Viola Group will regularly review the parameters to ensure they remain relevant.  Viola Group will conduct ongoing monitoring of the business relationship which includes:

- Scrutinising transactions, including source of funds;
- Keeping up to date documents held by Viola Group or a third-party client merchant on the customer

Monitoring is designed to identify unusual activity which may be indicative of money laundering or terrorist activity.  Monitoring undertaken by Viola Group will:

- Flag up transactions for further examination;
- Be reviewed promptly;
- Lead to appropriate action.
- Where monitoring is undertaken by a third party, the process is to be checked and approved by Viola Group

**9. Suspicious Activity Reporting**
When any member of staff either knows, suspects or has reasonable grounds for knowing or suspecting that a money laundering offence has been or is being committed they must make a suspicious activity report ( **SAR** ) to the MLRO.

Staff are not required to actively search for indications that money laundering offences are occurring. However, if they become aware of, or suspect that, such offences are occurring during their normal duties then they shall make a SAR to the MLRO.

**9.1 Internal Reporting**
All staff must report any activity that they see during their duties and that they think may be suspicious to the MLRO; all such reports must be documented by the member of staff making the report. Viola Group will provide all necessary training and guidance to ensure that staff is aware of;

- Their obligations in this area
- Possible offences and penalties
- Any internal disciplinary sanctions that may apply for not reporting
- Procedures to be followed
- Documentation to be used to make reports
- Where to access further advice and guidance.

**9.2 Suspicious Activity Report – Internal Procedure**
Viola Group has made a SAR template (see Appendix 11.2.) available to staff, all reports must be made using this template to ensure consistency. All reports must be fully documented and must include at a minimum the following information:

- Name and contact details of person making the report
- The customer&#39;s personal details
- The suspicious transaction details; date, amount and transaction identifier.
- Details of any related transactions (as above)
- The reasons for the suspicions.

The fact that a report has been made and the content of the report must remain confidential always.  A member of staff who forms or is aware of a suspicion of money laundering shall not discuss it with any outside party or any other member of staff unless directly involved in the matter causing suspicion.

No member of staff shall at any time disclose a money laundering suspicion to the person suspected. If circumstances arise that may cause difficulties with a client contact, the member of staff must seek and follow the instructions of the MLRO.

No copies or records of money laundering suspicion reports are to be made, except by the MLRO who will keep such records secure, and separate.

**9.3 MLRO Acknowledgement &amp; Evaluation**
All members of staff, anywhere within Viola Group, shall respond in full to all enquiries made by the MLRO for the purposes of evaluating a SAR.  Information provided to the MLRO in response to such enquiries does not breach client confidentiality or professional privilege, and no member of staff shall withhold information on those grounds.

The MLRO will acknowledge every report made. This confirms to the member of staff who raised the matter that their legal obligation to report has been fulfilled. This response will:

- Acknowledge receipt of the report
- Remind the initiator of the report of their obligation to do nothing that might prejudice an investigation or tip off the client
- Provide feedback on the quality of the report and suggest areas of improvement for future reports.

The MLRO will then conduct his own evaluation of the information provided within the report and decide whether to make a disclosure to NCA. The MLRO&#39;s review, evaluation and decision will be recorded.

Any internal enquiries made in relation to the report will be documented.

**9.4 Tipping-Off**
In some circumstances, it may be necessary to obtain information from the customer before deciding whether to make a disclosure to NCA.  In these cases, the MLRO may request an appropriate person (such as a Fraud and Security, Marketing or Customer Services Supervisor) to make discreet enquiries of a customer.  In such circumstances care, will be taken to ensure that the offence of &#39;tipping-off&#39; will be avoided.

Under Section 7 of the Data Protection Act (DPA) a Customer can request information regarding any of their personal information held or processed by an organisation – a Subject Access Request.

Section 29 of the DPA provides some exemption from responding to such a request when a SAR has been made, whether it has been reported to NCA.

Where a Subject Access Request is received and it is established that a SAR has been issued then the authority of the MLRO must be sought before any information relating to the SAR is released to the Customer.  The MLRO will keep a record of all such referrals.

**9.5 External Reporting**
The MLRO (or, if absent, the named deputy) shall receive and evaluate internal suspicion reports and decide whether a formal disclosure is to be made to the authorities.  If so deciding, the MLRO will make the formal disclosure on behalf of Viola Group using the Suspicious Activity Report Form (see Appendix 11.3.).

Prior to making any such report the MLRO will undertake internal enquiries to satisfy themselves that, based on the information in the report and the result of their enquiries, they know or suspect, or have reasonable grounds to know or suspect, an offence of money laundering. External reports will not be made until all available information has been considered by the MLRO, unless this would render such reports untimely. Such information includes:

- The financial circumstances of a customer or any person on whose behalf the customer has been acting
- The features of all transactions that the firm has entered into with or for the customer or any person on whose behalf the customer has been acting.

Where the MLRO does not have immediate access to such information it will be provided as soon as is practicable by any relevant employee of the firm or any relevant employee of any organisation that provides outsourced services on behalf of Viola Group.  The need to examine any additional information will not unnecessarily delay any report.

The decision whether there is knowledge, suspicion or reasonable grounds for knowledge or suspicion of a money laundering offence will rest with the MLRO or their delegated representative alone. Such a decision will not be subject to the consent of any other person within Viola Group.

From the moment, a suspicion of money laundering arises, no further work will be carried out on the matter that gave rise to the suspicion. Neither commercial considerations nor the difficulty in responding to the client&#39;s enquiries on the matter shall be permitted to take precedence over Viola Group&#39;s legal obligations in this regard.

**9.6 National Crime Agency (NCA)**
The disclosure regime for money laundering and terrorist financing is run by the United Kingdom Financial Intelligence Unit ( **UKFIU** ), within NCA.

A SAR is the name given to the making of a disclosure to NCA under either the Proceeds of Crime Act ( **POCA** ) or the Terrorism Act.  NCA has issued a preferred form which can be found online (www.nationalcrimeagency.gov.uk).  This securely encrypted system provided by NCA allows firms to:

- register the firm and relevant contact persons
- submit a SAR at any time of day
- receive e-mail confirmations of each SAR submitted

SARs can still be submitted in hard copy, although they should be typed and on the preferred form. Firms will not receive acknowledgement of any SARs sent this way.

The national reception point for disclosure of suspicions is the UKFIU within the NCA.  The UKFIU address is PO Box 8000, London, SE11 5EN.

The Financial Intelligence Helpdesk can be contacted on 020 7238 8282.  Firms can contact NCA on this number for:

- help in submitting a SAR or with the SARs online system
- help on consent issues
- assessing the risk of tipping off so you know whether disclosing information about a particular SAR would prejudice an investigation

NCA is required to treat any SARs confidentially. Where information from a SAR is disclosed for the purposes of law enforcement, care is taken to ensure that the identity of the reporter and their firm is not disclosed to other persons.

**9.7 Seek Consent**

Where a report is received prior to a transaction or activity taking place then the MLRO will make the appropriate report to NCA and seek consent to proceed with that transaction or activity.

NCA has up to seven (7) days to confirm whether or not the transaction, for which a consent has been requested, can proceed – until NCA gives consent, the transaction cannot proceed – it is frozen.  In these circumstances, the staff member must be very careful that they do not &#39;tip off&#39; the customer about the reason for the delay in processing the transaction.

Where NCA gives notice that consent to a transaction is refused, a further thirty-one (31) day period (the &quot;Moratorium&quot;) commences on the day that notice is given.  The thirty-one (31) days include Saturdays, Sundays and public holidays.  It is an offence to undertake the transaction during this period as the participant would not have the appropriate consent.  The Moratorium period enables NCA to further their investigation into the reported matter using the powers within the POCA in relation to the criminal property (e.g. imposing a restraint order).  If the Moratorium period expires and no such action has been taken, the reporter is free to proceed with the act(s) detailed in the initial disclosure.

It is Viola Group policy that all requests for consent must be processed through the MLRO – it is expressly forbidden for employees to make a &#39;consent&#39; request direct to NCA.

**10. Record Keeping**

Viola Group will retain:

- Copies of, or references to, the evidence obtained of a customer&#39;s identity for six years after the end of the customer relationship;

- Details of customer transactions for five years from the date of the relevant transaction.
- Records of all AML/CTF training delivered Details of actions taken in respect of internal and external suspicion reports;
- Details of information considered by the MLRO or his nominee in respect of an internal report where no external report is made.

Where documents are to be retained by a third-party client merchant, Viola Group will ensure that the above requirements are adhered to, and that the documents can be produced to Viola Group on request.

**11. Appendices**

**11.1 Acceptable Documents**

**Proof of Identity**

At least one of the following documents:

- Current signed passport
- EEA national ID card
- National ID card (non-EEA)
- Current UK issued travel document
- Home Office Immigration and Nationality Directorate application card
- Full UK driving licence

**Proof of address**

At least one of the following documents issued within the last three (3) months showing the Customer&#39;s name and current address:

- Bank, building society or credit card statement
- Driving licence (if not used as proof of ID)
- Council Tax bill/mortgage statement
- Benefits5. Customer Journey/pension book showing current address
- Inland Revenue correspondence

An official letter from a third party (e.g. employer, solicitor) confirming address

**11.2 Internal Suspicious Activity Report Form**
