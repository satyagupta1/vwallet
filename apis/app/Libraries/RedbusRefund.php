<?php

namespace App\Libraries;

class RedbusRefund {

    public static function paymentRefund($transactionId,$orderAmountBus)
    {
        $amount = 0;
        // get walletAccountId from transactionDetail table
        // check wallet exists with correct user or not
        $transactionDetails = self::_getTransactionDetails($transactionId);
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        if ($transactionDetails == FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Transaction doesn`t found.');
            return json_encode($output_arr);
        }

        
       /* if (($transactionDetails->EventCode != '') || ($transactionDetails->RefTransactionDetailID != ''))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.reversalTransactionAlreadyDone'));
            return json_encode($output_arr);
        }
      */  
       
      $partyId   = $transactionDetails->UserWalletId;
        
      
        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        if ($amount <= 0)
        {
               $amount = $orderAmountBus['finalAmount'];
        }
        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }

        
        $paymentEntity = \App\Models\Paymententity::select('PaymentEntityID')
                ->where('PaymentEntityName', $transactionDetails->TransactionVendor)
                ->first();

        if (!$paymentEntity)
        {
            $output_arr['display_msg'] = array('info' => 'Payment entity doesn`t exist.');
            return json_encode($output_arr);
        }

        $paymentProductEventCode = $transactionDetails->TransactionType . "REV";

        // Payment Product Info
        $paymentProductEvent = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentProductEventCode);

        if (!$paymentProductEvent)
        {
            $output_arr['display_msg'] = array('paymentProductEventId' => 'Please enter valid payment product event ID');
            return json_encode($output_arr);
        }
        
        $adminWalletDetails = self::_adminWalletDetails($paymentProductEvent->FromAccount);
        if ($adminWalletDetails === FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Invalid admin details.');
            return json_encode($output_arr);
        }

        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];
        // fee calculation 
        $fee = array();
        if($amount>0)
        {
        $fee               = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProductEvent, $partyWalletPlanId, $amount, $paymentEntity->PaymentEntityID, $partyId);
 
      $amount = $amount-$fee['taxAmount']-$fee['feeAmount'];
      
        }
      
        $offerDetails = isset($orderAmountBus['offerDetails']) ? $orderAmountBus['offerDetails'] : FALSE;
        $transDetails = self::insertTransDetails($partyId, $adminWalletDetails, $partyInfo, $paymentProductEvent, $amount,$fee, $transactionId,$offerDetails);
        if ($transDetails === FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Something went wrong. Please try after sometime.');
            return $output_arr;
        }
        
        self::updateTransctionwallets($transDetails, $adminWalletDetails);

        $transStatus = 'Success';
        if ($transactionDetails->TransactionStatus != 'Success')
        {
            $transStatus = 'Failed';
        }
        self::_updateTransactionEvent($transactionId, $transStatus, $paymentProductEventCode);

        $transDetails['taxAmount'] = (empty($fee['taxAmount']))?0:$fee['taxAmount'];
        $transDetails['feeAmount'] = (empty($fee['feeAmount']))?0:$fee['feeAmount'];
        // insert notifictaions
    
        return $transDetails;
    }
    
    
    private static function insertTransDetails($partyId, $adminWalletDetails, $partyInfo, $paymentProduct, $amount,$fee, $transactionId,$offerDetails)
    {
        
        $transDate           = date('Y-m-d');
        $dateTimeOfTrans     = date('Y-m-d H:i:s');
        $transctionID        = \App\Libraries\TransctionHelper::getTransctionId();
        $transStausMain      = 'Refund';
        $promoCode = '';
        $discountAmount = '0.00';
        $superCashAmount = '0.00';
        if($offerDetails){
            $promoCode = $offerDetails['promoCode'];
            $discountAmount = $offerDetails['offerAmount']; 
            $superCashAmount = $offerDetails['superCash'];
            $amount = $amount - $discountAmount;
        }
        $useName             = '';
       // $amount              = ($fee['transAmount']!='')?$fee['transAmount']:0;
        $transDetails        = array(
            'TransactionOrderID'       => $transctionID,
            'PaymentsProductID'        => $paymentProduct->PaymentsProductID,
            'UserWalletId'             => $adminWalletDetails->UserWalletID, // Wallet user id of from wallet account
            'FromAccountId'            => $adminWalletDetails->WalletAccountId,
            'FromAccountType'          => 'WalletAccount',
            'ToAccountType'            => 'WalletAccount',
            'ToAccountId'              => $partyInfo->UserWalletID,
            'ToWalletId'               => $partyInfo->WalletAccountId,
            'Amount'                   => $amount,
            'TransactionTypeCode'      => 'Transfer',
            'TransactionDate'          => $dateTimeOfTrans,
            'TransactionStatus'        => $transStausMain,
            'TransactionMode'          => 'Credit',
            'UserSessionUsageDetailID' => '',
            'TransactionType'          => $paymentProduct->ProductName . 'REV',
            'CreatedDatetime'          => $dateTimeOfTrans,
            'CreatedBy'                => 'Admin',
            'TransactionVendor'        => (!empty($fee['transctionVendor']))?$fee['transctionVendor']:0,
            'FeeAmount'                => (!empty($fee['feeAmount']))?$fee['feeAmount']:0,
            'TaxAmount'                => (!empty($fee['taxAmount']))?$fee['taxAmount']:0,
            'UpdatedBy'                => 'Admin',
            'RefTransactionDetailID'   => $transactionId,
            'WalletAmount'             =>  $amount,
            'PromoCode'                => $promoCode,
            'DiscountAmount'           => $discountAmount,
            'CashbackAmount'           => $superCashAmount,
        );
        $transactionDetailID = \App\Models\Transactiondetail::insertGetId($transDetails);
        if (!$transactionDetailID)
        {
            return FALSE;
        }

        $transctionLog = \App\Libraries\TransctionHelper::_transctionEventLog($transactionDetailID, $paymentProduct, $transDate, $useName, $dateTimeOfTrans, $amount);
        $transctionLog['Comments'] = trans('messages.busRefundTitle');
        $dailyTransctionLog = \App\Libraries\TransctionHelper::_dailyTransctionEventDetailsWtoW($transactionDetailID, $paymentProduct, $adminWalletDetails, $partyInfo, $partyId, $amount, $fee, $dateTimeOfTrans);

        $notificationLog = \App\Libraries\TransctionHelper::_notificationLog($transactionDetailID, $paymentProduct, $partyId, '', $amount, $dateTimeOfTrans);
        $transctionLog['Comments'] = 'Refund for Bus Ticket';
        // insert data into respective tables
        \App\Models\Transactioneventlog::insert($transctionLog);
        \App\Models\Dailytransactionlog::insert($dailyTransctionLog);
        \App\Models\Notificationlog::insert($notificationLog);
        $transDetails['transactionId'] = $transactionDetailID;
        return $transDetails;
    }
    
    
    public static function paymentRefundCB($transactionId, $amount = 0)
    {
        $transactionDetails = self::_getTransactionDetails($transactionId);
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        if ($transactionDetails == FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Transaction doesn`t found.');
            return json_encode($output_arr);
        }
        if (($transactionDetails->EventCode != '') || ($transactionDetails->RefTransactionDetailID != ''))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.reversalTransactionAlreadyDone'));
            // return json_encode($output_arr);
        }
        
        $partyId   = $transactionDetails->ToWalletId;
        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }
        if ($amount <= 0)
        {
            $amount = $transactionDetails->Amount;
        }
        $adminWalletDetails = \App\Libraries\TransctionHelper::getAdminWalletInfo($transactionDetails->UserWalletId);
        
        $paymentProductEventCode = 'CASHBACK';
        $paymentProductEvent = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentProductEventCode);
        $transDetails = self::insertTransction($partyId, $partyInfo, $transactionDetails, $amount, $paymentProductEvent, $transactionId, $adminWalletDetails);
        if ($transDetails === FALSE)
        {
            $output_arr['display_msg'] = array('info' => 'Something went wrong. Please try after sometime.');
            return $output_arr;
        }

        self::updatewallets($transDetails, $adminWalletDetails);

        $transStatus = 'Success';
        self::_updateTransactionEvent($transDetails->transactionId, $transStatus, $paymentProductEventCode);

        // insert notifictaions
//        $output_arr['is_error']    = FALSE;
//        $output_arr['display_msg'] = array('info' => trans('messages.amountReversalSuccess'));
//        $output_arr['res_data'] = $transDetails;
        return $transDetails;
    }
    
  
    
    /**
     * code for to check _getUserWalletId in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    //code to check for wallet and user matches found with correct id
    private static function _getTransactionDetails($transactionId)
    {

        // code to get kyc accepted or not from table users
        $check_walletAccount = \App\Models\Transactiondetail::select('UserWalletId', 'FromAccountId', 'ToAccountId', 'ToWalletId', 'PaymentsProductID', 'Amount', 'EventCode', 'TransactionDate', 'TransactionStatus', 'TransactionType', 'TransactionVendor', 'WalletAmount')
                ->where('TransactionDetailId', $transactionId)
                ->first();
        return (!empty($check_walletAccount)) ? $check_walletAccount : FALSE;
        // return $check_limit->IsVerificationCompleted;
    }

    /**
     * code for to check _updateTransactionEvent in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for update transaction detail event code for reversal
    private static function _updateTransactionEvent($transactionId, $status, $event)
    {
        $update_txn_detail                    = \App\Models\Transactiondetail::find($transactionId);
        $update_txn_detail->EventCode         = $event;
        $update_txn_detail->TransactionStatus = $status;
        $update_txn_detail->TransactionDate   = date('Y-m-d H:i:s');
        $update_txn_detail->save();
    }

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private static function _ValidateRefundInitiation($request)
    {
        // validations
        $validate = array(
            'transactionId' => 'required|numeric',
        );
        $messages = array();

        // validation for mobile device
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }
    
    public static function updatewallets($transctionDetails, $adminWalletDetails)
    {
       
        //save only if success
        // code for admin wallet update starts
       $order_amount = $transctionDetails['Amount'];

        $update_adminWallet                   = \App\Models\Walletaccount::find($adminWalletDetails->WalletAccountId);
        $update_adminWallet->AvailableBalance += $order_amount;
        $update_adminWallet->CurrentAmount    += $order_amount;
        $update_adminWallet->save();

        // code for user wallet update starts    
        $update_userWallet                   = \App\Models\Walletaccount::find($transctionDetails['FromAccountId']);
        $update_userWallet->NonWithdrawAmount -= $order_amount;
        // $update_userWallet->AvailableBalance -= $order_amount;
        // $update_userWallet->CurrentAmount    -= $order_amount;
        $update_userWallet->save();
        return TRUE;
    }
    
    public static function updateTransctionwallets($transctionDetails, $adminWalletDetails)
    {
      
     
        //save only if success
        // code for admin wallet update starts
        $order_amount = $transctionDetails['WalletAmount'];
//         echo $order_amount;
//        echo $transctionDetails['ToWalletId'];
//        die();
//        
//     
        $update_adminWallet                   = \App\Models\Walletaccount::find($adminWalletDetails->WalletAccountId);
        $update_adminWallet->AvailableBalance -= $order_amount;
        $update_adminWallet->CurrentAmount    -= $order_amount;
        $update_adminWallet->save();

        // code for user wallet update starts    
        $update_userWallet                   = \App\Models\Walletaccount::find($transctionDetails['ToWalletId']);
        $update_userWallet->AvailableBalance += $order_amount;
        $update_userWallet->CurrentAmount    += $order_amount;
        $update_userWallet->save();
 
        if ($transctionDetails['FeeAmount'] > 0)
        {
            $feeWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(2);
            $taxWalletAcc                   = \App\Libraries\TransctionHelper::getAdminWalletInfo(3);
            // update Fee wallet balance
            $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
            $updateFeeBal->AvailableBalance += $transctionDetails['FeeAmount'];
            $updateFeeBal->CurrentAmount    += $transctionDetails['FeeAmount'];
            $updateFeeBal->save();

            // update Tax wallet Balance
            $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
            $updateTaxBal->AvailableBalance += $transctionDetails['TaxAmount'];
            $updateTaxBal->CurrentAmount    += $transctionDetails['TaxAmount'];
            $updateTaxBal->save();
        }
        //current amount update in aggregate table       
        $refund = TRUE;
        \App\Libraries\Statistics::monthlyDebitTransactionsUpdate($transctionDetails['ToAccountId'], $order_amount, $refund);
        \App\Libraries\Statistics::monthlyCreditTransactionsUpdate($adminWalletDetails->UserWalletID, $order_amount, $refund);
        
            
        return TRUE;
    }

    private static function _adminWalletDetails($fromAccount)
    {
        $adminWalletDetails = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.UserWalletID', 'userwallet.MobileNumber', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsVerificationCompleted', 'userwallet.WalletPlanTypeId')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('walletaccount.WalletAccountNumber', $fromAccount)
                ->first();
        return(!$adminWalletDetails ) ? FALSE : $adminWalletDetails;
    }

}
