<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactioneventlog
 * 
 * @property int $TransactionEventDetailID
 * @property int $TransactionDetailID
 * @property int $PaymentsProductID
 * @property \Carbon\Carbon $EventDate
 * @property string $Comments
 * @property string $EventCode
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 *
 * @package App\Models
 */
class Transactioneventlog extends Eloquent
{
	protected $table = 'transactioneventlog';
	protected $primaryKey = 'TransactionEventDetailID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TransactionEventDetailID' => 'int',
		'TransactionDetailID' => 'int',
		'PaymentsProductID' => 'int'
	];

	protected $dates = [
		'EventDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionDetailID',
		'PaymentsProductID',
		'EventDate',
		'Comments',
		'EventCode',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID')
					->where('transactiondetail.TransactionDetailID', '=', 'transactioneventlog.TransactionDetailID')
					->where('transactiondetail.PaymentsProductID', '=', 'transactioneventlog.PaymentsProductID');
	}
}
