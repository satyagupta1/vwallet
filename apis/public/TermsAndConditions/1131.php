<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Get flat Rs 75 Cashback on Electricity bill payments using Viola Wallet.</li>
        <li>Minimum amount spend to redeem the discount is Rs.500.</li>
        <li>The code can be redeemed only once per user.</li>
        <li>Limited period offer.</li>
        <li>The offer is applicable on Electricity Bill Payments only.</li>
    </ul> 
</div>
<?php include 'footer.php';?>