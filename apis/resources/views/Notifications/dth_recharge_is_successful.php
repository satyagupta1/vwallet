<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Dth Recharge is Successful
 * 
 *  param $amount decimal
 *  param $rechargedate Date
 *  param $time time
 *  param $rechargeid varchar
 *  param $operator varchar
 *  param $offeramount int
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Congrats! DTH recharge successfully done',
        'text'    => $salutationName . ',<br/><br/> 
                        

Smile!<br/>
Your recharge of  Rs '.$amount.' has been successful. Your transaction details are given below<br/><br/>

Recharge Date: '.$rechargedate.'<br/>
Time: '.$time.'<br/>
Recharge ID: '.$rechargeid.'<br/>
Operator: '.$operator.'<br/>
Offer Amount: '.$offeramount.'<br/>

You would also receive a confirmation message (SMS) from the operator.',
    ],
    'sms'   => [
        'text' => 'Hello, Your DTH recharge of '.$amount.' amount has been successful. Available Balance is '.$balanceamount,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'DTH rechage of Rs '.$amount.' successful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'DTH recharge of INR '.$amount.' completed successfully  ',
    ],
];

echo json_encode($array);

/* End of file dth_recharge_is_successful */
