# ANTI-BRIBERY AND CORRUPTION POLICY AND PROCEDURE

**Definition of Bribery**

Bribery is an inducement or reward offered, promised or provided to gain personal, commercial, regulatory or contractual advantage.

**The Bribery Act**

The Bribery Act came into force on 1st July 2011.  The Act introduces four new offences for offering a bribe, accepting a bribe, bribing a foreign official and, for companies, not having in place policies to prevent bribery on its behalf.  The Act seeks to confirm what is and what is not acceptable in these circumstances and sets out penalties for offences.

Viola Group Limited ( **Viola Group** ) has incorporated this policy in order to ensure that the company is not compromised in respect of potential bribery or corruption in its business activities.

The key area of focus will be identifying a potential bribe and the comparison between what is being offered in return for a service and the service itself.

All employees will be made aware of this document as part of their induction if they are in a role which necessitates dealing with external clients or customers.

**Policy Statement**

Bribery is a criminal offence.  Viola Group does not pay bribes or offer improper inducements to anyone for any purpose, nor does Viola Group accept bribes or improper inducements.

Viola Group is committed to bribery prevention.  Viola Group therefore has a zero tolerance approach to bribery, which is achieved through the following:

- Setting out a clear anti-bribery policy.
- Making all employees aware of their responsibilities to adhere strictly to this policy at all times.
- Training staff so that they are aware and can recognize bribery, and avoid use of bribery by themselves and by others.
- Follow up and investigate any alleged or reported bribery.
- Encourage staff to report any bribery suspicion.
- Take firm action against any staff or agents that may become involved in bribery.
- Include appropriate clauses in contracts to prevent bribery.

**Policy**

Viola Group could be liable when someone who is acting on its behalf (such as employees) pays or accepts a bribe specifically to get business, keep business, or gain a business advantage for them.

Viola Group has a responsibility to ensure any business dealings carried out by an individual on its behalf are done so in an honest and open way, and all staff have a responsibility to comply with this policy and to ensure that the reputation of Viola Group is not harmed in any way.

**Accepting Gifts**

Employees are allowed to accept small value gifts (£20 value or below) from customers and suppliers but under no circumstances should a cash gift, or variations such as vouchers, be accepted.  Large value gifts cannot be accepted without first receiving approval from Viola Group&#39;s Compliance Officer.  If the value of a gift is unknown, but may be over £20, approval should be sought before accepting it.

**Offering Gifts**

Individuals are not permitted to offer gifts to customers or suppliers on behalf of Viola Group under any circumstances, unless it is a recognized part of their role within the company and has received prior authorisation, either specifically or generally as part of their contract, by a member of Viola Group&#39;s board.

Any individual found to be in breach of this policy will be dealt with via the Viola Group formal disciplinary procedure.

**Procedure**

To ensure the open and honest approach to hospitality and gifts, Viola Group&#39;s Compliance Officer keeps, maintains and reviews a log of both accepted and offered gifts, regardless of value or who has offered or been offered the gift.  This is to ensure adherence to the policy and to ensure all gifts are appropriate and are an adequate reflection of the services given or received.  It is the responsibility of all staff to ensure the accurate and complete recording of gifts received or given, on the log.

Viola Group has adopted the following procedure to enable it to comply with the requirements in this area.

- All gifts, entertainment or other benefits given and received by any Director or employee above a value of £20 are to be reported to the Compliance Officer who will maintain a record of them
- The record of gifts and benefits will contain details of:-
- the individuals concerned (from both Viola Group and the other party);
- the nature and circumstances of the gift or benefit;
- whether the gift or benefit is given or received;
- the date; and
- the approximate cost, if offered by Viola Group, although if an in-house lunch/dinner a statement to that effect will be sufficient;

All details of gifts and benefits given or received, including their frequency and nature and the names of the individuals concerned, are monitored periodically by the Compliance Officer and his monitoring is evidenced, through the Gift Register, and discussed on occasion of board meetings.

**Conclusion**

Viola Group will, over time, work in multiple territories and recognises that accepted and normal market practices may vary by region.  This policy prohibits any inducement (bribery or corruption) which creates an advantage or personal gain to the receiver or anybody formally associated with them, which is intended to influence them to take action which may not be solely in the interests of the business or of the person or body employing them or whom they represent.

This policy is not however meant to prohibit the following practices, which are deemed normal and customary in a particular market, provided they are not disproportionate in nature:

- The offer of resources to assist the person or body to make the decision more efficiently provided that they are supplied for that purpose only.
- Normal and appropriate hospitality.
- The giving of a ceremonial gift on a festival or at another special time.

Inevitably, decisions as to what is acceptable may not always be easy. If anyone is in doubt as to whether a potential act constitutes bribery, the matter should be referred to the appropriate line manager with responsibility for this policy before proceeding.

The prevention, detection and reporting of bribery is the responsibility of all employees throughout the business.