<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Promotionaloffer
 * 
 * @property int $PromotionalOfferID
 * @property int $OfferBusinessID
 * @property float $OfferVersion
 * @property string $OfferName
 * @property string $OfferDescription
 * @property int $OfferCategoryRefID
 * @property int $OfferTypeRefID
 * @property int $OfferBannerImageID
 * @property int $OfferManagerRefID
 * @property string $PromoCode
 * @property int $OfferUserTypeRefID
 * @property string $BusinessEffectiveDate
 * @property string $BusinessExpiryDate
 * @property string $TermsConditionsFilePath
 * @property string $EligibilityDetailsFilePath
 * @property int $OfferStatusRefID
 * @property int $OfferApprovedStatusRefID
 * @property string $ApprovedBy
 * @property float $Budget
 * @property string $ApplicableChannel
 * @property string $SMSNotification
 * @property string $EmailNotification
 * @property string $WebScreenNotification
 * @property string $MobileScreenNotification
 * @property string $QRCodeGenerated
 * @property string $Comments
 * @property string $ActiveInd
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Screenbanner $screenbanner
 * @property \Illuminate\Database\Eloquent\Collection $offerbenefitsbridges
 * @property \Illuminate\Database\Eloquent\Collection $offerexpiryrules
 * @property \Illuminate\Database\Eloquent\Collection $offersponsorcommercials
 * @property \Illuminate\Database\Eloquent\Collection $offerusereligibilities
 * @property \Illuminate\Database\Eloquent\Collection $useroffersstats
 *
 * @package App\Models
 */
class Promotionaloffer extends Eloquent
{
	protected $primaryKey = 'PromotionalOfferID';
	public $timestamps = false;

	protected $casts = [
		'OfferBusinessID' => 'int',
		'OfferVersion' => 'float',
		'OfferCategoryRefID' => 'int',
		'OfferTypeRefID' => 'int',
		'OfferBannerImageID' => 'int',
		'OfferManagerRefID' => 'int',
		'OfferUserTypeRefID' => 'int',
		'OfferStatusRefID' => 'int',
		'OfferApprovedStatusRefID' => 'int',
		'Budget' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'OfferBusinessID',
		'OfferVersion',
		'OfferName',
		'OfferDescription',
		'OfferCategoryRefID',
		'OfferTypeRefID',
		'OfferBannerImageID',
		'OfferManagerRefID',
		'PromoCode',
		'OfferUserTypeRefID',
		'BusinessEffectiveDate',
		'BusinessExpiryDate',
		'TermsConditionsFilePath',
		'EligibilityDetailsFilePath',
		'OfferStatusRefID',
		'OfferApprovedStatusRefID',
		'ApprovedBy',
		'Budget',
		'ApplicableChannel',
		'SMSNotification',
		'EmailNotification',
		'WebScreenNotification',
		'MobileScreenNotification',
		'QRCodeGenerated',
		'Comments',
		'ActiveInd',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function screenbanner()
	{
		return $this->belongsTo(\App\Models\Screenbanner::class, 'OfferBannerImageID');
	}

	public function offerbenefitsbridges()
	{
		return $this->hasMany(\App\Models\Offerbenefitsbridge::class, 'OfferBusinessID', 'OfferBusinessID');
	}

	public function offerexpiryrules()
	{
		return $this->hasMany(\App\Models\Offerexpiryrule::class, 'OfferBusinessID', 'OfferBusinessID');
	}

	public function offersponsorcommercials()
	{
		return $this->hasMany(\App\Models\Offersponsorcommercial::class, 'OfferBusinessID', 'OfferBusinessID');
	}

	public function offerusereligibilities()
	{
		return $this->hasMany(\App\Models\Offerusereligibility::class, 'OfferBusinessID', 'OfferBusinessID');
	}

	public function useroffersstats()
	{
		return $this->hasMany(\App\Models\Useroffersstat::class, 'OfferBusinessID', 'OfferBusinessID');
	}
}
