<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Carduser
 * 
 * @property int $idCardUsers
 * @property string $fullName
 * @property string $isd_code
 * @property string $mobileNumber
 * @property string $emailId
 * @property string $referralCode
 * @property string $password
 * @property string $tpin
 * @property string $status
 * @property float $WalletBalance
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $cardbeneficiaries
 * @property \Illuminate\Database\Eloquent\Collection $cardnotifications
 * @property \Illuminate\Database\Eloquent\Collection $cardtransactions
 * @property \Illuminate\Database\Eloquent\Collection $virtualcards
 *
 * @package App\Models
 */
class Carduser extends Eloquent
{
	protected $primaryKey = 'idCardUsers';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'idCardUsers' => 'int',
		'WalletBalance' => 'float'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'fullName',
		'isd_code',
		'mobileNumber',
		'emailId',
		'referralCode',
		'password',
		'tpin',
		'status',
		'WalletBalance',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'idCardUsers');
	}

	public function cardbeneficiaries()
	{
		return $this->hasMany(\App\Models\Cardbeneficiary::class, 'CardUsers_id');
	}

	public function cardnotifications()
	{
		return $this->hasMany(\App\Models\Cardnotification::class, 'idCardUsers');
	}

	public function cardtransactions()
	{
		return $this->hasMany(\App\Models\Cardtransaction::class, 'toCardId');
	}

	public function virtualcards()
	{
		return $this->hasMany(\App\Models\Virtualcard::class, 'idCardUsers');
	}
}
