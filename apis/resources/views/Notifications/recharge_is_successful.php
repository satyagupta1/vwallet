<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Recharge is Successful
 * 
 *  param $amount decimal
 *  param $rechargedate Date
 *  param $time time
 *  param $rechargeid varchar
 *  param $operator varchar
 *  param $offeramount int
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation ;
$array = [
    'email' => [
        'subject' => 'Recharge successful',
        'text'    => $salutationName . ',<br/><br/>
Enjoy!<br/>
Your recharge of  Rs '.$amount.' has been successful. Your transaction details are given below<br/><br/>

Mobile Number: '.$mobileNumber.'<br/>
Recharge Date: '.$rechargedate.'<br/>
Time: '.$time.'<br/>
Recharge ID: '.$rechargeid.'<br/>
Operator: '.$operator.'<br/><br/>
You would also receive a confirmation message (SMS) from the operator.',
    ],
    'sms'   => [
        'text' => 'Recharge of INR '.$amount.' Successful on '.$rechargedate.' at '.$time.'. Balance amount '.$amount,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Recharge for '.$operator.' successful',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Your recharge of INR '.$amount.'  completed successfully  ',
    ],
];

echo json_encode($array);

/* End of file recharge_is_successful */
