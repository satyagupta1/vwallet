<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ViolaWallet Bus Ticket</title>
        <style>
            body {
                font-family: Calibri, sans-serif;
                font-size: 14px;
                color: #333;
            }

            span {
                font-family: Arial, sans-serif;
                display: block;
            }

            span p {
                display: inline-block;
            }

            p,
            span {
                margin: 0px;
                padding: 0px;
            }

            i {
                font-style: normal;
                font-family: Arial, sans-serif;
            }

            h4 {
                margin: 0px;
                padding: 0px;
            }
            .passenger-content i{
                display: block;
                padding-bottom: 5px;
                padding-top: 5px;

            }
        </style>
    </head>

    <body>
        <div class="invoice-box">
            <table style="background-color:white; width: 800px" width="80%" cellspacing="3" cellpadding="0" border="0">
                <tr>
                    <td style="padding:0.75pt;">
                        <table style="width:700px; margin-bottom:30px; " cellspacing="3" cellpadding="0" border="0">
<!--                            <tr>
                                <td colspan="5" style="padding:0;">
                                    <a href="#" style="padding:8pt 0;display: block; border-top:1pt solid #CCC; border-bottom: 1px solid #ccc;"> <span style="font-size: 12px;color: #333;">print Ticket</span> </a>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>-->
                            <!-- header section   -->
                            <tr>
                                <td style="width:1%; padding: 20px 0px;">
                                    <div style="display:inline-block;margin-bottom:6pt;padding:0;border-right: 1px solid #ccc;">
                                        <img src="{{ url('public/logo.png') }}" alt="ViolaWalletLogo" id="lourlgo" border="0" style="margin-right: 15px; margin-top: 15px;"> </div>
                                </td>
                                <td style="width:20%;padding:2.25pt;"> <span style="font-size: 24px;">My-eTICKET</span> </td>
                                <td colspan="2" style="padding:0;"></td>
                                <td style="width:50%;padding:5px 0px; text-align:right;">
                                    <span style="font-size: 14px;"><b style="">Need help?</b></span>
                                    <!--<span style="margin-top: 15px;"><b>Boarding Point Contact:</b><p>04033559999</p></span> -->
                                    <span style="margin-top: 15px; margin-bottom: 15px;"><b>{{$OperatorName}}</b></span> <span>Write to us <a href="mailto:support@violawallet.com">support@violawallet.com</a></span> </td>
                            </tr>
                            <!--                                    destination section-->
                            <tr>
                                <td colspan="4" style="padding:15px 0; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; ">
                                    <h4 style="font-size: 20px; font-weight: 100;"> <b>{{$SourceName}} - {{$DestinationName}}</b> {{ date('l, F, j, Y', strtotime($JourneyDate)) }} </h4> </td>
                                <td colspan="2" style="padding:15px 0; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: right;"> <span><b style="font-size:12px; margin-bottom: 10px;">Ticket no: ViolaW1234</b></span>
                                    <p style="font-size: 12px; margin-top: 5px;">PNR no: {{$TicketNum.'/'.$TransactionDetailID.'/'.$ServiceId}}</p>
                                </td>
                            </tr>
                        </table>






                        <!--  bus details  -->
                        <table style="width:700px; margin-bottom: 30px;" cellspacing="3" cellpadding="0" border="0">
                            <tbody>


                                <tr>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$OperatorName}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">{{$BusType}}</i>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$boardingTime}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">Reporting time</i>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$dropingTime}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">Departure time</i>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$NoOfBookedSeats}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">Number of Passengers</i>
                                    </td>
                                </tr>



                                <tr>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>Boarding point details</b></span>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$BoardingPoint}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">Location</i>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>Dropping Point</b></span>
                                    </td>
                                    <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                        <span style="font-size:10.5pt;margin-bottom:5px;"><b>{{$DroppingAddress}}</b></span>
                                        <i style="font-size: 9pt; color: #999;">Address  </i>
                                    </td>
                                </tr>
                        </table>


                        <table style="width:700px; margin-bottom: 30px;" cellspacing="3" cellpadding="0" border="0">
                            <tr class="passenger-content">
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Passenger </b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Seat No.</b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Age</b></span>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <span style="font-size:10.5pt;margin-bottom:5px;"><b>Gender</b></span>
                                </td>
                            </tr>
                            @foreach ($passengerInfo as $pass)
                            <tr class="passenger-content">
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['paxName']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['seatNumber']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['age']}}</i>
                                </td>
                                <td style="width:25%;padding:7.5pt;border-bottom: 1px solid #ddd;">
                                    <i style="font-size: 9pt; color: #999;">{{$pass['gender']}}</i>
                                </td>
                            </tr>
                            @endforeach

                            <tr>				
                                <td colspan="7" style="padding:3.75pt;text-align: right;">
                                    <span style="font-size:9pt; margin-bottom: 5px; margin-top: 40px;"><b>Total Fare :</b></span>
                                    <span style="font-size:13.5pt;margin-bottom: 5px;"><b>Rs. {{number_format($TotalFare, 2)}}</b></span>
                                    <span style="font-size:9pt;color: #999; margin-bottom:15px;" >(Rs. 0 inclusive of GST and service charge, if any)</span>
                                    <span style="font-size:10pt;"><b>Discounted Fare : Rs. {{$DiscountAmount}}</b></span>
                                </td>
                            </tr>
                        </table>




                        <table style="width:100%;margin-bottom: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="padding:0;">
                                        <hr style="width:100%;" color="#333333" size="5" width="100%" align="center">                                                
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table style="width:700px; margin-bottom: 30px;" cellspacing="3" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="width:40%;padding:0;"> <hr style="width:100%;" color="#333333" size="5" width="100%" align="center"></td>
                                    <td style="padding:0;text-align: center;">
                                        <span style="font-size:10.5pt;">Terms and Conditions </span>
                                    </td>
                                    <td style="width:41%;padding:0;">
                                        <hr style="width:100%;" color="#333333" size="5" width="100%" align="center">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <ol start="10" style="margin-top:14pt;margin-bottom:0;">
                                <li style="margin:0;">
                                    <span style="margin-bottom: 15px; font-size: 12px;">Cancellation of this ticket is <b >NOT</b>	allowed after bus departure time. </span>
                                </li>
                            </ol>
                        </div>
                        <table style="width:700px;" cellspacing="3" cellpadding="0" border="0">
                            <tr>
                                <td style="width:47%;padding:0;" valign="top">
                                    <ol style="margin-bottom:0;">
                                        <li style="margin:0;"><span style="font-size:9pt;">ViolaWallet is enabler of online ticketing. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with an aggregator. ViolaWallet's advice to customers is to choose bus operators they are aware of and whose service they are comfortable
                                                with. </span>
                                        </li>
                                    </ol>
                                    <div style="margin:14pt 0 0 36pt;">
                                        <span style="font-size:9pt;"><b>ViolaWallet responsibilities include:</b></span>
                                    </div>
                                    <div style="margin:14pt 0 0 50pt;">
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(1) Issuing a valid ticket (a ticket that will be accepted by the bus operator) for its network of bus operators</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(2) Providing refund and support in the event of cancellation</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(3) Providing customer support and information in case of any delays / inconvenience</span>
                                    </div>
                                    <div style="margin:14pt 0 0 36pt;">
                                        <span style="font-size:9pt;"><b>ViolaWallet responsibilities do not include:</b></span>
                                    </div>
                                    <div style="margin:14pt 0 0 50pt;">
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;" >(1) The bus operator's bus not departing / reaching on time</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(2) The bus operator's employees being rude.</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(3) The bus operator's bus seats etc not being up to the customer's expectation.</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(4) The bus operator canceling the trip due to unavoidable reasons.</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(5) he baggage of the customer getting lost / stolen / damaged.</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(6) The bus operator changing a customer's seat at the last minute to accommodate a lady / child.</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(7) The customer waiting at the wrong boarding point (please call the bus operator to find out the exact boarding point if you are not a regular traveler on that particular bus).</span>
                                        <span style="text-indent:-18pt; font-size: 12px; margin-bottom: 10px;">(8) The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take customers to the bus departure point.</span>

                                    </div>
                                    <ol start="2" style="margin-bottom:0;">
                                        <li style="margin:0;"><span style="font-size:9pt;">The departure time mentioned on the ticket are only tentative timings.However the bus will not leave the source before the time that is mentioned on the ticket.</span>
                                        </li>
                                    </ol>
                                </td>
                                <td style="width:6%;padding:0;"></td>
                                <td style="width:47%;padding:0;" valign="top">
                                    <ol start="3" style="margin-bottom:0;">
                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Passengers are required to furnish the following at the time of boarding the bus: </span>
                                            <span style="text-indent:-15pt; font-size: 12px; margin-bottom: 10px;margin-left: 18px;" >(1) A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail).
                                            </span>
                                            <span style="text-indent:-15pt; font-size: 12px; margin-bottom: 10px;margin-left: 18px;" >(2) A valid identity proof  Failing to do so, they may not be allowed to board the bus.</span>
                                        </li>

                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Change of bus: In case the bus operator changes the type of bus due to some reason, ViolaWallet will refund the differential amount to the customer upon being intimated by the customers in 24 hours of the journey.</span>
                                        </li>
                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Amenities for this bus as shown on ViolaWallet have been configured and provided by the bus provider (bus operator). These amenities will be provided unless there are some exceptions on certain days. Please note that ViolaWallet provides this information in good faith to help passengers to make an informed decision. The liability of the amenity not being made available lies with the operator and not with ViolaWallet.</span>
                                        </li>
                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">n case a booking confirmation e-mail and sms gets delayed or fails because of technical reasons or as a result of incorrect e-mail ID / phone number provided by the user etc, a ticket will be considered 'booked' as long as the ticket shows up on the confirmation page of <a href="www.ViolaWallet.com">www.ViolaWallet.com</a></span>
                                        </li>

                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Grievances and claims related to the bus journey should be reported to ViolaWallet support team within 10 days of your travel date.</span>
                                        </li>

                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Please note the following regarding the luggage policy for your journey:
                                            </span>
                                            <span style="text-indent:-15pt; font-size: 12px; margin-bottom: 10px;margin-left: 18px;" >(1) Each passenger is allowed to carry one bag of upto 10 kgs and one personal item such as a laptop bag, handbag, or briefcase of upto 5 kgs.

                                            </span>
                                            <span style="text-indent:-15pt; font-size: 12px; margin-bottom: 10px;margin-left: 18px;" >(2) Passengers should not carry any goods like weapons, inflammable, firearms, ammunition, drugs, liquor, smuggled goods etc and any other articles that are prohibited under law.

                                            </span>
                                            <span style="text-indent:-15pt; font-size: 12px; margin-bottom: 10px;margin-left: 18px;" >(3) Bus Operator reserves the right to deny boarding or charge additional amount in case passenger is travelling with extra luggage than what is mentioned above.

                                            </span>

                                        </li>
                                        <li style="margin-bottom: 10px; "><span style="font-size:9pt; margin-bottom: 10px;">Partial Cancellation is <b style="font-size:13px;">NOT</b> allowed for this ticket. Charges for complete ticket cancellation are mentioned below.</span>
                                        </li>

                                    </ol>





                                    <table style="width:300pt;margin-bottom:0.75pt;margin-top: 15px;" width="500" cellspacing="0" cellpadding="3" border="1px solid #ddd" align="left">

                                        <tr>
                                            <td style="width:60%;padding:5px 10px;">
                                                <span style="font-size:7.5pt;"><b>Cancellation time</b></span>
                                            </td>
                                            <td style="width:29%;padding:5px 10px;">
                                                <span style="font-size:7.5pt;"><b>Cancellation charges</b></span>
                                            </td>
                                        </tr>
                                        @foreach ($cancelPolicy as $can)
                                        <tr>
                                            <td style="width:60%;padding:5px 10px;">
                                                <span style="font-size:7.5pt;">After <b>{{$can['policy']}}</b></span>
                                            </td>
                                            <td style="width:29%;padding:5px 10px;">
                                                <span style="font-size:7.5pt;"><b>  Rs. {{$can['amont']}} </b></span>
                                            </td>
                                        </tr>
                                        @endforeach


                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding:0;">
                        <div style="text-align:center;margin:0;" align="center">
                            <hr style="width:100%;" color="#333333" size="3" width="100%" align="center"> </div>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>