<?php
/**
 * UploadDoc file
 * Version: 1.0 (1st May 2018)
 * @copyright Viola Group Limited, 3 Waterton Park, Bridgend, CF31 3PH
 * @category Financial App
 * @package Viola Wallet 
 * @author ViolaWallet Limited info@viola.group
 * @license viola.group/codelicense
*/

namespace App\Libraries;

use Intervention\Image\ImageManagerStatic as Image;

/**
 * UploadDoc Class
 */
class UploadDoc {

    /**
     * 
     * @param type $userId
     * @param type $fileType
     * @param type $imageName
     * @param type $documentType value should be one of profile, kyc-idp, kyc-adp, ticketAttach
     * @param type $kycType
     * @return string
     */
    public static function uploadDoc($userId,  $fileType, $fileName, $documentType) {
     
        if ($fileType !== NULL AND $userId !== NULL AND $fileName !== NULL AND $documentType !== NULL) {
            $userFolder = sha1($userId);
            \Intervention\Image\ImageManagerStatic::configure(['driver' => 'gd']);
            $imageType = explode('/', $fileType);
            $fileNamerow = md5(time()) . '.' . $imageType[1];
            $path = $documentType . '/'.$userFolder . '/';

            if (strpos($documentType, '-')) {
                $subpath = explode('-', $documentType);
                $path =  $subpath[0] . '/' .$userFolder . '/' . $subpath[1];
            }

            $NewPath = UploadDoc::public_path($path);
            $NPath = $NewPath . '/' . $fileNamerow;
            if (!is_dir($NewPath)) {
                mkdir($NewPath, 0777, TRUE);
            }
             \Intervention\Image\ImageManagerStatic::make(base64_decode($fileName))->save($NPath);

            $response = array('path' => $path, 'filename' => $fileNamerow);
            return $response;
        }

        return FALSE;
    }

    private static function public_path($path = NULL) {
        return rtrim(app()->basePath('public/' . $path), '/');
        // return '/home/v-wallet/public_html/public/'.$path.'/';
    }

}