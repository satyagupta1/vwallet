<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
        <li>Use code IDEAREC to earn monthly cashback of INR 10 for 5 months on a minimum recharge of INR 250 in South and West circles.</li>
        <li>Applicable on recharges done during 25th Dec to 31st Dec 2017.</li>
        <li>The code can be redeemed three times per user.</li>
        <li>Minimum amount spend to redeem the SuperCash is Rs.10.</li>
        <li>Limited Period Offer.</li>
        <li>Offer is valid on IDEA operators in SOuth and West Circles only.</li>
        <li>Coupon redemption & Payment should be done only on ViolaWallet App or Website.</li>
        <li>Viola has the right to end or call back any or all of its offer without prior notice.</li>
    </ul> 
</div>
<?php include 'footer.php';?>