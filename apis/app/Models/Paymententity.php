<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 31 Oct 2017 11:24:43 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Paymententity
 * 
 * @property int $PaymentEntityID
 * @property string $PaymentEntityName
 * @property string $ActiveFlag
 * @property \Carbon\Carbon $StartDate
 * @property \Carbon\Carbon $EndDate
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $productplanconfigs
 *
 * @package App\Models
 */
class Paymententity extends Eloquent
{
	protected $table = 'paymententity';
	protected $primaryKey = 'PaymentEntityID';
	public $timestamps = false;

	protected $dates = [
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PaymentEntityName',
		'ActiveFlag',
		'StartDate',
		'EndDate',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function productplanconfigs()
	{
		return $this->hasMany(\App\Models\Productplanconfig::class, 'PaymentEntityID');
	}
}
