<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Topup success with Transaction Details device template
 *  param $from varchar
 *  param $to varchar
 *  param $name varchar
 *  param $journeyDateFrom varchar
 *  param $onwardTicket varchar
 *  param $busType varchar
 *  param $boardingFrom varchar
 *  param $seatNumbers varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($primaryName) ? $salutation.' '.$primaryName : $salutation ;
$returnJourny = NULL;
if ( $returnTiket == 'Y' )
{
    $returnJourny = 'Return Journey<br/>     
                 Journey Date:' . $journeyDateTo . 'Ticket number: ' . $returnTicketNumber . ', Bus Type:' . $tobusType . ',Boarding From:' . $returnboardingFrom . ', Seatnumbers:' . $returnseatNumbers . '<br/>';
}
$array = [
    'email' => [
        'subject' => 'ViolaWallet Bus booking: Ticket for '.$fromCity.' -'.$toCity.'',
        'text'    => $salutationName . ',<br/><br/> You Bus ticket booking is confirmed <br/><br/>
                        '.$fromCity.'-'.$toCity.','.$journeyDateFrom.'<br/>
                        Onward Journey<br/>
                       Journey Date:'.$journeyDateFrom.' Ticket number: ' . $onwardTicket . ', Bus Type:'.$frombusType.',Boarding From:'.$boardingFrom.', Seatnumbers:'.$fromseatNumbers.'<br/>
                  '.$returnJourny,
    ],
    'sms'   => [
        'text' => 'TKT NO ' . $onwardTicket . '
Name:' . $primaryName . ',' . $fromCity . '-' . $toCity . ' ,' . $onwardoperatorName . ',
ServiceNo:'.$fromserviceNumber. '    
Boarding Time ' . $boardingTime . ', Boarding Point' . $boardingFromPoint . '
<seat No.>' . $fromseatNumbers . '
<original Fare>' . $amount . '
'
    ],
    'push'  => [
        'title'              => 'Booking Confirmed',
        'body'               => 'Bus Ticket '.$fromCity.'-'.$toCity.' confirmed ',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'busbooking'
    ],
    'web'   => [
        'text' => 'Bus Ticket '.$fromCity.'-'.$toCity.' confirmed ',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
