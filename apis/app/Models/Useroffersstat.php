<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Useroffersstat
 * 
 * @property int $UserUsedOffersId
 * @property int $UserWalletID
 * @property int $OfferBusinessID
 * @property string $PromoCode
 * @property string $UserActivity
 * @property \Carbon\Carbon $ActivityDateTime
 * @property float $AmountUsed
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * 
 * @property \App\Models\Promotionaloffer $promotionaloffer
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Useroffersstat extends Eloquent
{
	protected $primaryKey = 'UserUsedOffersId';
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'OfferBusinessID' => 'int',
		'AmountUsed' => 'float'
	];

	protected $dates = [
		'ActivityDateTime',
		'CreatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'OfferBusinessID',
		'PromoCode',
		'UserActivity',
		'ActivityDateTime',
		'AmountUsed',
		'CreatedDateTime',
		'CreatedBy'
	];

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'OfferBusinessID', 'OfferBusinessID');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
