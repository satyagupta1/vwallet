<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactionschedule
 * 
 * @property int $TransactionID
 * @property \Carbon\Carbon $ScheduledDate
 * @property int $BeneficiaryID
 * @property float $TransactionAmount
 * @property string $ActiveIndicator
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \App\Models\Transaction $transaction
 * @property \App\Models\Userbeneficiary $userbeneficiary
 *
 * @package App\Models
 */
class Transactionschedule extends Eloquent
{
	protected $table = 'transactionschedule';
	protected $primaryKey = 'TransactionID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TransactionID' => 'int',
		'BeneficiaryID' => 'int',
		'TransactionAmount' => 'float'
	];

	protected $dates = [
		'ScheduledDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ScheduledDate',
		'BeneficiaryID',
		'TransactionAmount',
		'ActiveIndicator',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function transaction()
	{
		return $this->belongsTo(\App\Models\Transaction::class, 'TransactionID');
	}

	public function userbeneficiary()
	{
		return $this->belongsTo(\App\Models\Userbeneficiary::class, 'BeneficiaryID');
	}
}
