<?php

namespace App\Libraries;

use App\Libraries\Smshub;

class CommunicationLib {

    public static function _communicationSend($partyId, $beneficiaryId, $transRes, $serviceType)
    {
        if($beneficiaryId){
        $partyInfo       = self::_userDataById($partyId);
        $beneficiaryInfo = self::_userDataById($beneficiaryId);
        }else{
        $beneficiaryInfo = self::_userDataById($partyId);    
        }
        
        $beneficiaryLoginHistory = self::_userLoginHistoryById($beneficiaryInfo->UserWalletID);
        
        //$beneficiaryPrefences = \App\Models\Userconfiguration::where('UserWalletID',$beneficiaryId);
        if(!$beneficiaryLoginHistory){
           return FAlSE;
        }
        
        if($beneficiaryLoginHistory->MobileType != 'W'){
        $benefPref = array('sms','email','web','push');
        }else{
        $benefPref = array('email','web');    
        }
        
        $transData = $transRes['response']['TransactionDetails'];
        $userData = array(
            'partyId'      => $beneficiaryInfo->UserWalletID,
            'mobileNumber' => $beneficiaryInfo->MobileNumber,
            'emailId'      => $beneficiaryInfo->EmailID,
            'deviceType'   => isset($beneficiaryLoginHistory->MobileType) ?  $beneficiaryLoginHistory->MobileType : '',
            'deviceToken'  => isset($beneficiaryLoginHistory->DeviceToken) ?  $beneficiaryLoginHistory->DeviceToken : ''
        );

        $fromName = $transData['TransactionVendor'];
        if($beneficiaryId){
           $fromName = $partyInfo->FirstName . ' ' . $partyInfo->LastName; 
        }
        $smsData   = $emailData = $webData   = $pushData  = array(
            'amount'   => $transData['Amount'],
            'fromName' => $fromName,
            'toName'   => $beneficiaryInfo->FirstName . ' ' . $beneficiaryInfo->LastName,
            'transId'  => $transData['TransactionOrderID'],
            'dateTime' => date('d/m/Y H:i:s', strtotime($transData['CreatedDatetime'])),
            'mobile' => $beneficiaryInfo->MobileNumber
        );

        $notificationData = array(
            'notificationFor'  => $serviceType,
            'notificationType' => implode(',',$benefPref)
        );

        $communicationData = array(
            'userData'         => $userData,
            'emailData'        => json_encode($emailData),
            'smsData'          => json_encode($smsData),
            'pushData'         => json_encode($pushData),
            'webData'          => json_encode($webData),
            'notificationData' => $notificationData
        );

        self::sendCommunicationAlerts($communicationData);
    }

    

    private static function sendCommunicationAlerts($communicationInfo)
    {
        $communicationParams = array(
            'party_id'          => $communicationInfo['userData']['partyId'],
            'linked_id'         => '0',
            'notification_for'  => $communicationInfo['notificationData']['notificationFor'],
            'notification_type' => $communicationInfo['notificationData']['notificationType'],
            'mobile_no'         => $communicationInfo['userData']['mobileNumber'],
            'mobile_data'       => $communicationInfo['smsData'],
            'email_id'          => $communicationInfo['userData']['emailId'],
            'email_data'        => $communicationInfo['emailData'],
            'web_data'        => $communicationInfo['webData'],
            'push_data' => $communicationInfo['pushData'],
            'device_type' => $communicationInfo['userData']['deviceType'],
            'device_token' => $communicationInfo['userData']['deviceToken'],
        );
        //common function to send notification_type msgs.
        \App\Libraries\Helpers::sendMsg($communicationParams);
    }
    
    private static function _userDataById($partyId)
    {
        $userData = \App\Models\Userwallet::
                select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.MiddleName', 'userwallet.LastName', 'userwallet.MobileNumber', 'userwallet.EmailID', 'userwallet.ProfileStatus', 'userconfiguration.Details')
                ->where('userwallet.UserWalletID', $partyId)
                ->orWhere('userwallet.MobileNumber', $partyId)
                ->orWhere('userwallet.EmailID', $partyId)
                ->orWhere('userwallet.ViolaID', $partyId)
                ->leftJoin('userconfiguration', 'userwallet.UserWalletID', '=', 'userconfiguration.UserWalletID')
                ->first();
        if ($userData)
        {
            return $userData;
        }
        return FALSE;
    }
    
    private static function _userLoginHistoryById($partyId)
    {
        $loginHistory = \App\Models\partyloginhistory::
                select('SessionID','Email','DeviceUniqueID','IPAddress','DeviceType','MobileType','DeviceToken','LoginSuccess')
                ->where('PartyID', $partyId)
                ->where('DeviceType','!=','W')
                ->orderby('PartyLoginHistoryId','desc')
                ->first();
        if ($loginHistory)
        {
            return $loginHistory;
        }
        return FALSE;
    }

}
