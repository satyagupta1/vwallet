<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Booking in Progress
 *  param $salutationName varchar
 */
$salutation = trans('messages.salutation');
$salutationName = ($primaryName) ? $salutation.' '.$primaryName : $salutation;
$array = [
    'email' => [
        'subject' => '',
        'text'    => $salutationName . ',<br/><br/> 
        Your booking is in progress. Please open your app to complete booking.',
    ],
    'sms'   => [
        'text' => 'Your booking is in progress. Please open your app to complete booking'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your booking is in progress. Please open your app to complete booking.',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'busbooking'
    ],
    'web'   => [
        'text' => 'Your booking is in progress. Please open your app to complete booking',
    ],
];

echo json_encode($array);

/* End of file topup_success_with_transaction_details.php */
