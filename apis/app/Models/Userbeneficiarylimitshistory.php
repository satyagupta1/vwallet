<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 22 May 2018 09:29:55 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userbeneficiarylimitshistory
 * 
 * @property int $beneficiaryHistoryId
 * @property int $BeneficiaryID
 * @property int $UserWalletID
 * @property string $yesBankBeneficiaryId
 * @property float $oldMaxPermissibleLimit
 * @property float $newMaxPermissibleLimit
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userbeneficiary $userbeneficiary
 *
 * @package App\Models
 */
class Userbeneficiarylimitshistory extends Eloquent
{
	protected $table = 'userbeneficiarylimitshistory';
	protected $primaryKey = 'beneficiaryHistoryId';
	public $timestamps = false;

	protected $casts = [
		'BeneficiaryID' => 'int',
		'UserWalletID' => 'int',
		'oldMaxPermissibleLimit' => 'float',
		'newMaxPermissibleLimit' => 'float'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BeneficiaryID',
		'UserWalletID',
		'yesBankBeneficiaryId',
		'oldMaxPermissibleLimit',
		'newMaxPermissibleLimit',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userbeneficiary()
	{
		return $this->belongsTo(\App\Models\Userbeneficiary::class, 'BeneficiaryID');
	}
}
