<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Send Money Failure with Transaction details template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $receivername varchar
 *  param $number varchar
 *  param $time varchar
 *  param $date varchar
 *  param $medium varchar
 *  param $transctionid varchar
 *  param $accountnumber varchar
 *  param $fullname varchar 

 */

$salutation = trans('messages.salutation');
$salutationName = ($senderName) ? $salutation . ' ' . $senderName : $salutation;
$array = [
    'email' => [
        'subject' => 'Transaction failed',
        'text'    => $salutationName . ',<br/><br/>

Your funds transfer request for Rs. '.$amount.' from account '.$accountName.' / '.$mobileNumber.' on '.$date.' '.$time.' towards IMPS could not be processed due to Beneficiary Account Number is invalid
Please find the details as below:<br/>
Beneficiary Account No: '.$mobileNumber.'<br/>
Comments:<br/>
Incase your account is debited for this transaction the amount will be reversed into your account shortly.'],
    'sms'   => [
        'text' => 'Payment of ' . $amount . 'was declined due to  insufficient funds/Network error/ Technical error/ No response from third party/ Session expired/ Not a Viola Customer.
Transaction ID: ' . $transactionId . '
Time: ' . $time . '
Date: ' . $date
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Transaction declined',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Payment of Rs ' . $amount . ' was declined. Please try again later.',
    ],
];

echo json_encode($array);

/* End of file send_money_faliure_with_transaction_details.php */
