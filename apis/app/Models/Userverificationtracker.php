<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Userverificationtracker
 * 
 * @property int $UserWalletID
 * @property string $VerificationStatus
 * @property int $VerficiationCenterID
 * @property string $Comments
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 * @property \App\Models\Verificationcenter $verificationcenter
 *
 * @package App\Models
 */
class Userverificationtracker extends Eloquent
{
	protected $table = 'userverificationtracker';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'VerficiationCenterID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'VerificationStatus',
		'VerficiationCenterID',
		'Comments',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}

	public function verificationcenter()
	{
		return $this->belongsTo(\App\Models\Verificationcenter::class, 'VerficiationCenterID');
	}
}
