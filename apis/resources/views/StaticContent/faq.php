
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc; 
}

.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
    overflow: hidden;
}
</style>
</head>
<body>

<h2>About ViolaWallet</h2>

  <button class="accordion">
      1.How can I contact ViolaWallet?
  </button>
 <div class="panel">
  <p>
  We're available 24/7 for our customers and you can drop us a line on support@violawallet.com You can also give us a call at +91 90300-84652(Viola) between 9AM – 9PM all 7 days (excluding National Holidays). For potential business partners, we can be reached at info@ViolaWallet.in
  </p>
</div>
       

  <button class="accordion">
      2. Where do I spend the money on my ViolaWallet account?
  </button>
 <div class="panel">
  <p>
      You can spend the money loaded onto your ViolaWallet account for a variety of things. You can send money to or request money from friends and family. You can split bills with your friends when you dine out, collect money for movie tickets from your friends, remind friends that they owe you money and more! You can of course also use it to pay with select merchants online.
  </p>
</div>  

  <button class="accordion">
      3.What can I do with ViolaWallet?
  </button>
 <div class="panel">
  <p>
      Deposit money to Wallet 
>Prepaid Recharge 
>Postpaid bill payments 
>DTH Recharge 
>Utility Payments 
> Payment at Merchants 
  </p>
</div>  

  <button class="accordion">
      4.What are the benefits of using ViolaWallet?
  </button>
 <div class="panel">
  <p>
      >By registering for ViolaWallet, you would gain access to a variety of services, which include:
>Secure cash-free transactions anytime from anywhere
>Securely store all your credit/debit cards and bank accounts for convenient and faster payments
>Make in-store and online payments across a variety of merchants
> Recharge Mobile/DTH using a single app
>Get great offers, deals and coupons from top brands and also from your neighborhood stores
  </p>
</div>  
     

<h2 id="scroll_here">Wallet Upgrade</h2>

  <button class="accordion">
      1)Who is a ViolaWallet KYC Customer? (Question and Answer doesn’t match)
  </button>
 <div class="panel">
  <p>
  >When you Sign Up on ViolaWallet, a wallet gets created for you in which you can add upto Rs. 20,000/- in a calendar month. This is as per RBI regulations. As a result there is a cap on how much you can spend. Also, there can be delays in getting cashback if the monthly limit of adding Rs. 10,000gets exceeded.
	>You can upgrade your wallet for FREE by getting your KYC (Know Your Customer) done and get rid of the Rs. 10,000monthly limit. Additionally, you will get the following benefits:
	>Balance Limit increases to 100,000
>Special offers and more cashbacks
2) How do I become a ViolaWallet KYC customer?
	 >In order to become a ViolaWallet KYC customer you need to get your KYC (Know Your Customer) done which simply means that you need to get your documents of address proof and identity proof verified. You can initiate the process from your ViolaWallet app only. 

>ViolaWallet App

	>You can find the “ViolaWallet KYC” section on the Profile Page.
	>On ViolaWallet app, you can complete your KYC in any one of the following ways:
	>Request a visit from our agent at your home/office
	>Verify documents at your nearest ViolaWallet KYC centre
	>You can also follow the URL: www.violawallet.com
  </p>
</div>
     
  
  <button class="accordion">
      3) What are the documents needed to complete my KYC?
  </button>
 <div class="panel">
  <p>
  >In order to complete your KYC, we need to verify your Aadhaar card electronically or physically.
Other accepted documents are
1.PAN Card
2.Voter ID
3.Passport
4.Driving license
  </p>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
</script>

</body>
</html>
