<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 May 2018 11:36:34 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Ifsccode
 * 
 * @property int $id
 * @property string $code
 * @property string $state
 * @property string $district
 * @property string $city
 * @property string $bank
 * @property string $branch
 * @property string $micr
 * @property string $address
 * @property string $contact
 *
 * @package App\Models
 */
class Ifsccode extends Eloquent {

    protected $table   = 'ifsccodes';
    public $timestamps = false;
    protected $fillable = [
        'code',
        'state',
        'district',
        'city',
        'bank',
        'branch',
        'micr',
        'address',
        'contact'
    ];

}
