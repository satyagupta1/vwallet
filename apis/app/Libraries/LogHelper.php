<?php

/**
 * Class for creating logs for each case
 */

namespace App\Libraries;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LogHelper {

    public static function createLog($request = array(), $logId = '')
    {
        $logFolder   = 'third-party-logs/';
        $logFileName = date('Y') . '/' . date('m') . '/' . date('d') . '/log-' . date('H') . '.log';
        $logFile     = $logFolder . $logFileName;

        // check file size and choose dynamic name
        if ( file_exists(base_path($logFile)) )
        {
            $logSize = filesize(base_path($logFile));
            // if file size more than 2MB will rename current file with random number. So new file will be creatted insted of.
            if ( $logSize > 2000000 )
            {
                $reNameLog = $logFolder . date('Y') . '/' . date('m') . '/' . date('d') . '/log-' . date('H') . '-' . rand(00, 99) . '.log';
                rename(base_path($logFile), base_path($reNameLog));
            }
        }

        $logTitle = (isset($request['thirdPartyName'])) ? $request['thirdPartyName'] : 'thirdPartyLog';

        $thirdPartyLog = new Logger(0);
        $thirdPartyLog->pushHandler(new StreamHandler(base_path($logFile)), Logger::INFO);
        $thirdPartyLog->info($logTitle . ' :: ' . json_encode($request, JSON_PRETTY_PRINT));

        if ( isset($request['response']) )
        {
            $request['response'] = json_encode($request['response'], JSON_PRETTY_PRINT);
        }
        else
        {
            $request['request'] = json_encode($request['request'], JSON_PRETTY_PRINT);
        }

        if ( $logId )
        {
            $insertLogId = \App\Models\Thirdpartylog::where('Id', $logId)->update($request);
        }
        else
        {
            $insertLogId = \App\Models\Thirdpartylog::insertGetId($request);
        }

        return $insertLogId;
    }

}
