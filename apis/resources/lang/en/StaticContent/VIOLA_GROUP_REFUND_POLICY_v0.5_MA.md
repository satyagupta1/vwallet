# REFUND POLICY

## **Your Rights to Redemption of Funds and Refunding of Transactions on your Prepaid Card**

This policy sets out the liability of Viola Group to you (the client) about the redemption and refunding of funds loaded to your Viola Group, in accordance with our Terms of Use.  Please read this document in conjunction with Viola Group - Terms of Use, Cookie and Privacy Policy

**Redeeming the funds on your Prepaid Card**
You have the right to redeem the funds on your Prepaid Card at any time in whole or in part.  You can transfer funds from your Viola Group to the original source of funds.  When we process your redemption request, we may require you to provide us with documents such as identification so that we may process your request in accordance with legal requirements.  We may also charge a redemption fee if one of the following circumstances applies:

- You are requesting redemption before termination or expiry of this agreement;
- You cancel this agreement before any agreed termination or expiry date; or
- You request redemption more than one year after the date of termination or expiry of this agreement

All refunds will be returned to you on the payment instrument that you used to fund the Prepaid Card.  We reserve the right to see proof of your ownership of the payment instrument before transferring the funds to it.  To enable us to comply with our legal obligations, we may ask you to provide us with certain information before we can process your refund request.

Where the currency of the refund amount is different to the currency of the payment instrument that you used to fund the Prepaid Card, we will convert this before we refund you.  Please note that exchange rates vary and contain a buy/sell spread and therefore the exchange rate on any refund you receive will differ from the rate when you loaded onto your Prepaid Card.  We will apply our standard rate on the day and will inform you of the exchange rate at the time of the refund.

**Refunding Transactions**
You may be entitled to claim a refund in relation to transactions where:

- Transactions were not authorised under this agreement;
- We are responsible for a transaction which was incorrectly executed/notified to us
- Pre-authorised transaction did not specify the exact amount at the time of its authorisation and the amount charged by a supplier is more than you could reasonably have expected considering normal spending patterns on the Prepaid Card or the circumstances of the transaction.  A claim for a refund in the circumstances set out above will not be accepted if the amount of the transaction was made available to you at least 4 weeks before the transaction date or if the claim is made more than 8 weeks after being debited to your account.
- We were notified of the unauthorised/incorrectly executed transaction within 13 months of the debit date

**Who to Contact for Refund Request?**
Go to Menu within Viola Group App or log into the website [www.violacard.com](http://www.violacard.com) and complete Refund Request Form located within the Menu section.

State all information necessary for the request which includes:

- Viola Group Account Name
- Viola Group Account Number
- Date and Time of Transaction
- Amount of Transaction
- Transaction Type
- Reason for the Request
- Updated Contact Details

**How Long is the Refund Process?**
Within 7 to 14 days of receiving a refund request or where applicable of receiving any further information we have requested from you.

**How is Refund Received?**
We will refund the applicable amount to the source of funds (payment instrument) originally used by you to load the funds or provide you with an explanation for refusing the request.

**Note:** All requests will be investigated to ensure the Laws of England and Wales have been adhered to along with ensuring no fraudulent activity has taken place, and Viola Group are complying with Anti-Money Laundering Laws.

**Please note that Viola Group at no time will be held responsible for funds being sent to incorrect account numbers.  You will be responsible for ensuring you enter the correct details, as we are unable to retrieve funds sent to the wrong account.**

**Should you purchase items in store, online, face to face and they are faulty please do not contact Viola Group.  You must contact the merchant where you purchased your items, for an exchange or refund as Viola Group will not be responsible.**

Viola Group Refund Policy was last updated 18/08/2017