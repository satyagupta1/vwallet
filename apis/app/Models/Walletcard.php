<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletcard
 * 
 * @property int $CardID
 * @property int $WalletAccountID
 * @property string $CardType
 * @property string $SerialNumber
 * @property int $DeliveryAddressID
 * @property string $Status
 * @property string $RequestDate
 * @property string $DeliveredDate
 * @property string $ReceivedDate
 * @property string $CardEffectiveDate
 * @property string $CardExpiryDate
 * @property string $CardLimit
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Partyaddress $partyaddress
 * @property \App\Models\Walletaccount $walletaccount
 *
 * @package App\Models
 */
class Walletcard extends Eloquent
{
	protected $table = 'walletcard';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'CardID' => 'int',
		'WalletAccountID' => 'int',
		'DeliveryAddressID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'WalletAccountID',
		'SerialNumber',
		'DeliveryAddressID',
		'Status',
		'RequestDate',
		'DeliveredDate',
		'ReceivedDate',
		'CardEffectiveDate',
		'CardExpiryDate',
		'CardLimit',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partyaddress()
	{
		return $this->belongsTo(\App\Models\Partyaddress::class, 'DeliveryAddressID');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'WalletAccountID');
	}
}
