<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a ACT DTH Bill Payment of Transaction Amount >= INR 500.</li>
        <li>System should check if the transaction date is less than 31st Jan, 2018.</li>
        <li>Discount of 5% should be done before processing the payments and debit the required amount only from ViolaWallet.</li>
    </ul> 
</div>
<?php include 'footer.php';?>