<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantwallet
 * 
 * @property int $MerchantID
 * @property string $WalletAccountId
 * @property string $WalletAccountPrefix
 * @property string $isd_code
 * @property string $MobileNumber
 * @property string $MerchantName
 * @property string $MerchantLegalName
 * @property float $AvailableBalance
 * @property string $BusinessDetailText
 * @property string $LineOfBusiness
 * @property string $EmailAddress
 * @property string $Password
 * @property string $TPIN
 * @property string $LegalEntityRegistrationNumber
 * @property int $ParentMerchantID
 * @property int $MerchantWallet_MerchantID
 * @property string $ProfileStatus
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $merchantbeneficiarydetails
 * @property \Illuminate\Database\Eloquent\Collection $merchantconfigurations
 * @property \Illuminate\Database\Eloquent\Collection $merchantfeedetails
 * @property \Illuminate\Database\Eloquent\Collection $merchantpaydevices
 * @property \Illuminate\Database\Eloquent\Collection $merchantqrcodes
 * @property \Illuminate\Database\Eloquent\Collection $merchantverfications
 * @property \Illuminate\Database\Eloquent\Collection $merchantwallets
 * @property \App\Models\Merchantwalletregistration $merchantwalletregistration
 * @property \Illuminate\Database\Eloquent\Collection $merchantwalletregistrations
 * @property \Illuminate\Database\Eloquent\Collection $submerchantreleations
 *
 * @package App\Models
 */
class Merchantwallet extends Eloquent
{
	protected $table = 'merchantwallet';
	protected $primaryKey = 'MerchantID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int',
		'AvailableBalance' => 'float',
		'ParentMerchantID' => 'int',
		'MerchantWallet_MerchantID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'WalletAccountId',
		'WalletAccountPrefix',
		'isd_code',
		'MobileNumber',
		'MerchantName',
		'MerchantLegalName',
		'AvailableBalance',
		'BusinessDetailText',
		'LineOfBusiness',
		'EmailAddress',
		'Password',
		'TPIN',
		'LegalEntityRegistrationNumber',
		'ParentMerchantID',
		'MerchantWallet_MerchantID',
		'ProfileStatus',
		'CreatedBy',
		'CreatedDateTime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantWallet_MerchantID');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'MerchantID');
	}

	public function merchantbeneficiarydetails()
	{
		return $this->hasMany(\App\Models\Merchantbeneficiarydetail::class, 'MerchantID');
	}

	public function merchantconfigurations()
	{
		return $this->hasMany(\App\Models\Merchantconfiguration::class, 'MerchantID');
	}

	public function merchantfeedetails()
	{
		return $this->hasMany(\App\Models\Merchantfeedetail::class, 'MerchantID');
	}

	public function merchantpaydevices()
	{
		return $this->hasMany(\App\Models\Merchantpaydevice::class, 'MerchantID');
	}

	public function merchantqrcodes()
	{
		return $this->hasMany(\App\Models\Merchantqrcode::class, 'MerchantID');
	}

	public function merchantverfications()
	{
		return $this->hasMany(\App\Models\Merchantverfication::class, 'MerchantID');
	}

	public function merchantwallets()
	{
		return $this->hasMany(\App\Models\Merchantwallet::class, 'MerchantWallet_MerchantID');
	}

	public function merchantwalletregistration()
	{
		return $this->hasOne(\App\Models\Merchantwalletregistration::class, 'MerchantID');
	}

	public function merchantwalletregistrations()
	{
		return $this->hasMany(\App\Models\Merchantwalletregistration::class, 'ReferralMerchantID');
	}

	public function submerchantreleations()
	{
		return $this->hasMany(\App\Models\Submerchantreleation::class, 'RequestMerchantID');
	}
}
