<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 05 Oct 2017 13:35:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Walletplanconfiguration
 * 
 * @property int $WalletPlanConfId
 * @property int $WalletPlanTypeId
 * @property string $ConfTagName
 * @property string $ConfKey
 * @property string $ConfValue
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Walletplantype $walletplantype
 *
 * @package App\Models
 */
class Walletplanconfiguration extends Eloquent
{
	protected $table = 'walletplanconfiguration';
	protected $primaryKey = 'WalletPlanConfId';
	public $timestamps = false;

	protected $casts = [
		'WalletPlanTypeId' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'WalletPlanTypeId',
		'ConfTagName',
		'ConfKey',
		'ConfValue',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function walletplantype()
	{
		return $this->belongsTo(\App\Models\Walletplantype::class, 'WalletPlanTypeId');
	}
}
