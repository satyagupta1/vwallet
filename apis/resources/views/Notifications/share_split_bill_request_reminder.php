<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Request reminder template
 *  param $customerName varchar
 *  param $billAmount varchar
 *  param $billName varchar
 *  param $loginLink varchar 
 *  param $balanceAmount varchar  
 */

$array = [
    'email' => [
        'subject' => 'Payment Reminder',
        'text'    => '  
Dear ' . $customerName . ',<br/><br/>      
'.$title.'<br/>
'.$description.' 
To make the payment, kindly login to ' . $loginLink],
    'sms'   => [
        'text' => ' 
'.$title.'<br/>
'.$description.'
    To make the payment, kindly login to ' . $loginLink . ' 
   ',
    ],
    'push'  => [
        'title'              => $title,
        'body'               => $description,
        'summaryText'        => $description,
        'notificationType'   => 'bigTextStyle',
        'imageUrl'           => '', // image Url for imageStyle
        'button1'            => 'OK', // button 1 Title for display for Routing to screen noothing but the navigation screen Id
        'actionUrl1'         => $actionUrl1, // acction url endpoint 1
        'button2'            => 'Cancel', // button 2 Title for display
        'actionUrl2'         => '', // acction url endpoint 2 for Routing to screen
        'screenNavigationId' => $billerCategory, // navigation Id for app possible values: alerts, offers, general, transactions, booking
        'notificationId'     => $reminderNotificationId, // unique notification Id / Transction Id,
        'actionParams'       => json_encode(array('authenticator1' => $subscriberNumber, 'billerId' => $billerId)),
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file share_split_bill_request_reminder.php */
