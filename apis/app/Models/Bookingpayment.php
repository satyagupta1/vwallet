<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:57 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Bookingpayment
 * 
 * @property string $BookingPaymentId
 * @property int $BookingMasterId
 * @property string $PaymentType
 * @property \Carbon\Carbon $PaymentTimestamp
 * @property string $PaymentStatus
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Bookingmaster $bookingmaster
 *
 * @package App\Models
 */
class Bookingpayment extends Eloquent
{
	protected $table = 'bookingpayment';
	protected $primaryKey = 'BookingPaymentId';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BookingMasterId' => 'int'
	];

	protected $dates = [
		'PaymentTimestamp',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BookingMasterId',
		'PaymentType',
		'PaymentTimestamp',
		'PaymentStatus',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bookingmaster()
	{
		return $this->belongsTo(\App\Models\Bookingmaster::class, 'BookingMasterId');
	}
}
