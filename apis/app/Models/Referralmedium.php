<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Referralmedium
 * 
 * @property string $ReferralMediumCode
 * @property string $ReferralMediumDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Partyreferral $partyreferral
 *
 * @package App\Models
 */
class Referralmedium extends Eloquent
{
	protected $table = 'referralmediums';
	protected $primaryKey = 'ReferralMediumCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ReferralMediumDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partyreferral()
	{
		return $this->hasOne(\App\Models\Partyreferral::class, 'ReferralMediumCode');
	}
}
