<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * DTH Cashback Received
 * 
 *  param $amount decimal
 *  param $time time
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation ;
$array = [
    'email' => [
        'subject' => 'DTH cashback received',
        'text'    => $salutationName . ',<br/><br/>
Your recharge of Rs ' . $amount . ' for DTH is awaiting confirmation.<br/> 
We will email/sms you once the payment is processed successfully.<br/> 
Promo Code Upon Success. Your cashback of Rs ' . $amount . ' will be credited in ' . $time . ' hours into your wallet.'],
    'sms'   => [
        'text' => 'Cashback of Rs '.$amount.' received'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cashback on DTH received',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'offers'
    ],
    'web'   => [
        'text' => 'Cashback on DTH being processed.',
    ],
];

echo json_encode($array);

/* End of file dth_cashback_received */
