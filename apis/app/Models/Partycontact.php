<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partycontact
 * 
 * @property int $PartyId
 * @property int $ContactTypeId
 * @property string $ContactDetails
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Contacttype $contacttype
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Partycontact extends Eloquent
{
	protected $table = 'partycontact';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyId' => 'int',
		'ContactTypeId' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
                'PartyId',
                'ContactTypeId',
		'ContactDetails',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function contacttype()
	{
		return $this->belongsTo(\App\Models\Contacttype::class, 'ContactTypeId');
	}

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'PartyId');
	}
}
