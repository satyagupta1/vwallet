<?php

namespace App\Http\Controllers\Pays;

/**
 * SendMoneyController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class SendMoneyController extends \App\Http\Controllers\Controller {

    // send Money Wallet to Wallet
    public function sendMoneyWB(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        $partyId     = $request->input('partyId');
        $serviceMode = $request->input('serviceType');
        $requestId   = $request->input('requestId');
        if ($requestId)
        {
            $requestMoneyData = \App\Models\Partyqrcode::find($requestId);
            if (count($requestMoneyData) <= 0)
            {
                $output_arr['display_msg'] = array('requestId' => "Please enter valid request money Id");
                return response()->json($output_arr);
            }

            if ($partyId != $requestMoneyData->ReceiverPartyID)
            {
                $output_arr['display_msg'] = array('requestId' => "Please enter valid request money Id");
                return response()->json($output_arr);
            }

            if ($serviceMode == 'reject')
            {
                $requestMoneyData->UsedStatus = 'REJ';
                $requestMoneyData->save();
                \App\Models\Notificationlog::where('RequestMoneyID', '=', $requestId)->update(['RequestMoneyStatus' => 'reject']);
                
                $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
                $banificiaryInfo = \App\Libraries\TransctionHelper::_userIdValidate($requestMoneyData->SenderPartyID);
                \App\Libraries\Helpers::_requestMoneyCommunication($request, $partyInfo, $banificiaryInfo);
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array('info' => 'Request rejected successfully');
                return $output_arr;
            }

            $mergeArray = array(
                'mobileNumber' => $requestMoneyData->ReceiverMobNum,
                'amount'       => $requestMoneyData->Amount,
                'requestType'  => $requestMoneyData->RequestType,
            );
            $request->merge($mergeArray);
        }
        else
        {
            $checkValidate = $this->_validateSendMoneyWB($request);
            //$checkValidate = TRUE;
            if ($checkValidate !== TRUE)
            {
                $output_arr['display_msg'] = $checkValidate;
                return response()->json($output_arr);
            }
        }

        // Validate V-PIN if vPin parameter value is not empty
        if ($request->input('vPin'))
        {
            //code to check vpin
            $params     = array('partyId' => $request->input('partyId'), 'vPin' => $request->input('vPin'));
            $verifyVpin = \App\Libraries\Helpers::validateVPin($params);
            if ($verifyVpin === FALSE)
            {
                $output_arr['display_msg'] = array('vPin' => 'Please enter valid V-PIN');
                return response()->json($output_arr);
            }
        }

        $amount            = $request->input('amount');
        $businessModelid   = $request->input('businessModelId');
        $beneficiarymobile = $request->input('mobileNumber');
        $transStaus        = config('vwallet.transactionStatus.pending');

        $transType = $request->input('transType');
        //trans_type whether it is WW or WB
        // will get Y or N as a Input (optionl)
        $saveBenf  = $request->input('saveBenf');

        // will get nickname if saveBenf set as Y
        $nickName = $request->input('nickName');

        $productEventId = 4;
        $benfType       = 'eWallet';
        $serviceType    = 'send_money_wtow';
        if ($transType == 'WB')
        {
            $serviceType    = 'send_money_wtob';
            $benfType       = 'Bank';
            $productEventId = 5;
        }
        $banificiaryInfo = '';
        // get Logged in User data if Exists.
        $partyInfo       = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $senderMobile = $partyInfo->MobileNumber;
        if ($transType == 'WW') // it is wallet to wallet
        {
            $checkBeneficiary = \App\Libraries\TransctionHelper::_userIdValidate($beneficiarymobile, 'mobile');

            if ($checkBeneficiary === FALSE)
            {
                $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
                return $output_arr;
            }

            if ($partyId == $checkBeneficiary->UserWalletID)
            {
                $message                   = trans('messages.partySame');
                $output_arr['display_msg'] = array('info' => $message);
                return json_encode($output_arr);
            }

            if ($serviceMode == 'request')
            {
                $requestMoneyData = \App\Libraries\Helpers::requestMoney($request, $partyInfo, $checkBeneficiary);
                \App\Libraries\Helpers::_requestMoneyCommunication($request, $partyInfo, $checkBeneficiary);
                
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array('info' => 'Request send successfully');
                return $output_arr;
            }

            \App\Models\Notificationlog::where('RequestMoneyID', '=', $requestId)->update(['RequestMoneyStatus' => 'success']);

            $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiarymobile, $benfType);

            $AccountName      = $checkBeneficiary->FirstName;
            $userWalletIdBenf = $checkBeneficiary->WalletAccountId;
            $partyIDBenf      = $checkBeneficiary->UserWalletID;

            $yesBankBeneficiaryId = isset($banificiaryInfo->yesBankBeneficiaryId) ? $banificiaryInfo->yesBankBeneficiaryId : '';
            if (($banificiaryInfo == FALSE) || ($yesBankBeneficiaryId == ''))
            {
                // calling add benenficiary 
                $IdentifierType = 'mobile';
                $identifier1    = $beneficiarymobile;
                $benId          = '';
                $extraParams    = array(
                    'partyId'        => $partyId,
                    'referenceId'    => $benId,
                    'referenceTable' => 'userbeneficiary',
                    'status'         => 'Initiated'
                );

                $ybkReq        = array(
                    'mobile'         => $senderMobile,
                    'benifisaryName' => (!$nickName) ? $nickName : $checkBeneficiary->FirstName,
                    'IdentifierType' => 'mobile',
                    'identifier1'    => $identifier1,
                    'identifier2'    => '',
                );
                $outputRespo   = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);
                $reqForYesBank = json_decode($outputRespo);

                if (isset($reqForYesBank->beneficiary))
                {
                    $YbBeneficiaryId = $reqForYesBank->beneficiary->id;
                }
                else
                {
                    if ($reqForYesBank->status_code == 'ERROR')
                    {
                        $output_arr['display_msg'] = array('info' => $reqForYesBank->message);
                        return $output_arr;
                    }
                    $YbBeneficiaryId = $reqForYesBank->beneficiary_id;
                }

                $this->addBenficiary($request, $AccountName, $partyId, $partyIDBenf, 'eWallet', $YbBeneficiaryId, $saveBenf);

                $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiarymobile, $benfType);
            }
        }

        if ($transType == 'WB') // it is wallet to wallet
        {
            $beneficiaryAcntNum = $request->input('accountNo');
            $ifscCode           = $request->input('ifscCode');
            $beneficiarymobile  = $beneficiaryAcntNum;
            $banificiaryInfo    = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiaryAcntNum, $benfType);
            $AccountName        = $request->input('nickName');
            $userWalletIdBenf   = '';
            $partyIDBenf        = '';

            $yesBankBeneficiaryId = isset($banificiaryInfo->yesBankBeneficiaryId) ? $banificiaryInfo->yesBankBeneficiaryId : '';
            if (($banificiaryInfo == FALSE) || ($yesBankBeneficiaryId == ''))
            {
                // calling add benenficiary 
                $IdentifierType = 'bank';
                $identifier1    = $beneficiarymobile;
                $benId          = '';
                $extraParams    = array(
                    'partyId'        => $partyId,
                    'referenceId'    => $benId,
                    'referenceTable' => 'userbeneficiary',
                    'status'         => 'Initiated'
                );

                $ybkReq        = array(
                    'mobile'         => $senderMobile,
                    'benifisaryName' => (!$nickName) ? $nickName : '',
                    'IdentifierType' => 'bank',
                    'identifier1'    => $beneficiaryAcntNum,
                    'identifier2'    => $ifscCode,
                );
                $outputRespo   = \App\Libraries\Wallet\Beneficiary::addBenificiary($ybkReq, $extraParams);
                $reqForYesBank = json_decode($outputRespo);

                if (isset($reqForYesBank->beneficiary))
                {
                    $YbBeneficiaryId = $reqForYesBank->beneficiary->id;
                }
                else
                {
                    if ($reqForYesBank->status_code == 'ERROR')
                    {
                        $output_arr['display_msg'] = array('info' => $reqForYesBank->message);
                        return $output_arr;
                    }
                    $YbBeneficiaryId = $reqForYesBank->beneficiary_id;
                }
                $this->addBenficiary($request, $AccountName, $partyId, '', 'Bank', $YbBeneficiaryId, $saveBenf);
                $banificiaryInfo = \App\Libraries\Wallet\YesBankCheckUserBalance::_checkBeneficiary($partyId, $beneficiaryAcntNum, $benfType);
            }
        }

        // session Check
        /* $sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
        if (is_bool($sessionId))
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
            return $output_arr;
        }
         */
        
        // calling YesBank Api
        $SendermobileNumber = $partyInfo->MobileNumber;

        $yesBankApi = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($SendermobileNumber);

        if ($yesBankApi['is_error'] == TRUE)
        {
            return response()->json($yesBankApi);
        }
        $yesBankApiRes = $yesBankApi['res_data'];
        if ($yesBankApiRes['remaining_load_limit'] < $amount)
        {
            $output_arr['display_msg'] = array('info' => 'You Exceeded Your Monthly Limit');
            return $output_arr;
        }

        if ($benfType == 'eWallet')
        {
            $yesBankApiBenficiary = \App\Libraries\Wallet\YesBankCheckUserBalance::CheckYesBankWalletBalance($beneficiarymobile);

            if ($yesBankApiBenficiary['is_error'] == TRUE)
            {
                return response()->json($yesBankApiBenficiary);
            }
            $yesBankApiBenfData = $yesBankApiBenficiary['res_data'];
            if ($yesBankApiBenfData['remaining_load_limit'] < $amount)
            {
                $output_arr['display_msg'] = array('info' => 'You Benificiary Exceeded Your Monthly Limit');
                return $output_arr;
            }
        }

        // check Balance
        if ($partyInfo->CurrentAmount < $amount)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.notEnoughBalance'));
            return $output_arr;
        }

        // party transction Limit 
        $partyTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
//        print_r($partyTransLimit);
        if ($partyTransLimit['error'])
        {
            $output_arr['display_msg'] = $partyTransLimit;
            return $output_arr;
        }
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        // check Benifisary transction or wallet credit limits 
        if ($transType == 'WW') // it is wallet to wallet
        {
            $benificaryTransLimit = \App\Libraries\TransctionHelper::userTransctionLimitChecker($checkBeneficiary->UserWalletID, $checkBeneficiary->WalletPlanTypeId, $amount, TRUE);
            if ($benificaryTransLimit['error'])
            {
                $output_arr['display_msg'] = $benificaryTransLimit;
                return $output_arr;
            }
        }

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($productEventId);
        // fee calculation 
        $fee            = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $businessModelid, $partyId);

        if (($transType == 'WB') && ($request->input('checkout') == 'N'))
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array('info' => 'Request Successfully');
            $output_arr['res_data']    = $fee;
            return $output_arr;
        }
        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $businessModelid, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transStaus, 'sdfdsfsdff');

        $transactionDetails = $transRes['response']['TransactionDetails'];

        if ($request->input('transType') == 'WB')
        {
            $transactionDetails['AccountName']             = ($banificiaryInfo->AccountName) ? $banificiaryInfo->AccountName : $banificiaryInfo->WalletBeneficiaryName;
            $transactionDetails['BeneficiaryMobileNumber'] = $request->input('accountNo');
        }
        else
        {
            $transactionDetails['AccountName']             = $banificiaryInfo->FirstName . ' ' . $banificiaryInfo->LastName;
            $transactionDetails['BeneficiaryMobileNumber'] = $request->input('mobileNumber');
        }

        \App\Libraries\Helpers::_sendMoneyCommunication($request, $partyInfo, $banificiaryInfo, $transactionDetails);

        if ($transRes['error'])
        {
            $output_arr['display_msg'] = array('info' => $transRes['info']);
            return $output_arr;
        }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array('info' => 'Request Successfully');
        $output_arr['res_data']    = $transRes['response'];
        return $output_arr;
    }

    public function addBenficiary($request, $AccountName, $userWalletId, $beneficiaryUserId, $benfType, $YbBneficiaryId, $saveBenf)
    {
        $insertBeneficiryData = array(
            'BeneficiaryWalletID'      => $beneficiaryUserId,
            'UserWalletID'             => $userWalletId,
            'BeneficiaryStatus'        => 'N',
            'BeneficiaryType'          => $benfType,
            'BeneficiaryMobileNumber'  => ($request->input('mobileNumber')) ? $request->input('mobileNumber') : '',
            'WalletBeneficiaryName'    => $request->input('nickName'),
            'isTripleClickUser'        => 'N',
            'BankIFSCCode'             => ($request->input('ifscCode')) ? $request->input('ifscCode') : '',
            'MMID'                     => '',
            'AccountName'              => ($AccountName == '') ? $AccountName : '',
            'AccountNumber'            => ($request->input('accountNo')) ? $request->input('accountNo') : '',
            'BeneficiaryVPA'           => '',
            'CreatedDateTime'          => date('Y-m-s H:i:s'),
            'yesBankBeneficiaryId'     => $YbBneficiaryId,
            'yesBankBeneficiaryStatus' => 'Y',
            'BeneficiaryStatus'        => $saveBenf
        );

        $insertBeneficiry = \App\Models\Userbeneficiary::insertGetId($insertBeneficiryData);
        return $benId            = $insertBeneficiry;
    }

    private function _validateSendMoneyWB($request)
    {
        $validate = array(
            'partyId'         => 'required|numeric|digits_between:1,10',
            'walletacntId'    => 'required|numeric|digits_between:1,10',
            'amount'          => 'required|numeric',
            'transType'       => 'required|in:WW,WB',
            'businessModelId' => 'required|numeric',
            'serviceType'     => 'required|in:send,request,reject',
            'requestType'     => 'required|in:qrcode,sendmoney',
        );

        $messages = array();

        if ($request->input('saveBenf') == 'Y')
        {
            $validate['nickName'] = 'required|alpha_num';
        }
        if (($request->input('transType') == 'WW') && ($request->input('requestId') == ''))
        {
            // $validate['beneficiaryId'] = 'required|numeric|digits:10';
            $validate['mobileNumber'] = 'required|numeric|digits:10';
        }
        if ($request->input('transType') == 'WB')
        {
            //  $validate['beneficiaryId'] = 'required|numeric';
            $validate['accountNo'] = 'required|numeric|digits_between:11,16';
            $validate['ifscCode']  = 'required';
        }

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
