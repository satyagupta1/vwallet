<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 05:01:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Merchantbeneficiarydetail
 * 
 * @property int $BeneficiaryID
 * @property int $MerchantID
 * @property string $BeneficiaryType
 * @property string $BankCode
 * @property string $BankIFSCCode
 * @property string $AccountName
 * @property string $AccountNumber
 * @property string $AccountType
 * @property string $Comments
 * @property string $BeneficiaryShortName
 * @property string $BeneficiaryAccountName
 * @property string $BeneficiaryWalletAccountID
 * @property string $BeneficiaryStatus
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * @property string $UpadatedBy
 * @property string $SelfBenficiary
 * 
 * @property \App\Models\Merchantwallet $merchantwallet
 *
 * @package App\Models
 */
class Merchantbeneficiarydetail extends Eloquent
{
	protected $primaryKey = 'BeneficiaryID';
	public $timestamps = false;

	protected $casts = [
		'MerchantID' => 'int'
	];

	protected $dates = [
		'CreatedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'MerchantID',
		'BeneficiaryType',
		'BankCode',
		'BankIFSCCode',
		'AccountName',
		'AccountNumber',
		'AccountType',
		'Comments',
		'BeneficiaryShortName',
		'BeneficiaryAccountName',
		'BeneficiaryWalletAccountID',
		'BeneficiaryStatus',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDatetime',
		'UpadatedBy',
		'SelfBenficiary'
	];

	public function merchantwallet()
	{
		return $this->belongsTo(\App\Models\Merchantwallet::class, 'MerchantID');
	}
}
