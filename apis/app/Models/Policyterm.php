<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Sep 2017 07:38:39 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;;

/**
 * Class Policyterm
 * 
 * @property int $PolicyTermID
 * @property string $PolicyCategory
 * @property string $PolicyType
 * @property string $PolicyVersion
 * @property \Carbon\Carbon $PolicyEffectiveDate
 * @property \Carbon\Carbon $PolicyExpiryDate
 * @property string $ActiveInd
 * @property string $TermsCompleteTextPath
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $parties
 *
 * @package App\Models
 */
class Policyterm extends Eloquent
{
	protected $primaryKey = 'PolicyTermID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PolicyTermID' => 'int'
	];

	protected $dates = [
		'PolicyEffectiveDate',
		'PolicyExpiryDate',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PolicyCategory',
		'PolicyType',
		'PolicyVersion',
		'PolicyEffectiveDate',
		'PolicyExpiryDate',
		'ActiveInd',
		'TermsCompleteTextPath',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function parties()
	{
		return $this->belongsToMany(\App\Models\Party::class, 'partypolicyterms', 'PolicyTermID', 'PartyID')
					->withPivot('PartyPolicyTermID', 'TermsAcceptedDate', 'CreatedDateTime', 'CreatedBy', 'UpdatedDateTime', 'UpdatedBy');
	}
}
