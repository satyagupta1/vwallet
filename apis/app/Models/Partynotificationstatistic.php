<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partynotificationstatistic
 * 
 * @property int $PartyID
 * @property int $TotalBeneficiaries
 * @property int $TotalReferral
 * @property int $TotalNotification
 * @property int $TotalUnreadNotification
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Partynotificationstatistic extends Eloquent
{
	protected $primaryKey = 'PartyID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int',
		'TotalBeneficiaries' => 'int',
		'TotalReferral' => 'int',
		'TotalNotification' => 'int',
		'TotalUnreadNotification' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TotalBeneficiaries',
		'TotalReferral',
		'TotalNotification',
		'TotalUnreadNotification',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}
}
