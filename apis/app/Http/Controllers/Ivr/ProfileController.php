<?php

/**
 * ProfileController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\Ivr;

/**
 * ProfileController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class ProfileController extends \App\Http\Controllers\Controller {
    /*
     *  validateUser - validate user by his mobile number
     *  @param Request $request
     *  @return JSON
     */

    public function validateUser(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        $userInfo = $validateUser['res_data'];

        $output_arr['is_error'] = FALSE;
        $BalMsg                 = 'Thanks for validating your self ' . $userInfo->FirstName;
        $output_arr['res_data'] = array( 'info' => $BalMsg );

        return $output_arr;
    }

    /*
     *  getUserBalance - To get user balance
     *  @param Request $request
     *  @return JSON
     */

    public function getUserBalance(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        $userInfo = $validateUser['res_data'];

        $partyId      = $userInfo->UserWalletID;
        // Get user balances 
        $userBalances = \App\Models\Walletaccount::select('WalletAccountNumber', 'AvailableBalance', 'CurrentAmount', 'HoldAmount', 'NonWithdrawAmount')
                        ->where('UserWalletId', $partyId)->first();
        if ( ! $userBalances )
        {
            $output_arr['res_data'] = array( 'info' => trans('messages.emptyRecords') );
            return $output_arr;
        }

        $output_arr['is_error'] = FALSE;
        $BalMsg                 = $userInfo->FirstName . ', your current balance is ' . $userBalances->CurrentAmount . ' available balance is ' . $userBalances->AvailableBalance;
        $output_arr['res_data'] = array( 'info' => $BalMsg );

        return $output_arr;
    }

    public function forgotPassword(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }

        $mobNumber = $request->input('mobileNumber');
        $userInfo  = $validateUser['res_data'];

        $partyId = $userInfo->UserWalletID;
        $encKey  = config('vwallet.ENCRYPT_SALT');

        // When set to TRUE, the return string will be 23 characters. Result more unique. else 13 characters long
        $getUniqId   = uniqid($partyId . '-', TRUE);
        $setPassHash = $this->setForgotPasswordHash($partyId, $getUniqId);
        if ( $setPassHash !== TRUE )
        {
            $output_arr['res_data'] = array( 'info' => 'Oops! Something Went wrong.' );
            return $output_arr;
        }

        $forgotPasswordHash = base64_encode(\App\Libraries\Helpers::encryptData($getUniqId, $encKey, TRUE));

        // genrate OTP
        $otp        = \App\Libraries\IvrHelper::generateOtp();
        $frontUrl   = config('vwallet.frontUrl');
        $smsMessage = 'Your OTP for reset password is ' . $otp . '. Please open below link and use the OTP for furthue process. Link: ' . $frontUrl . 'validateReset/' . $forgotPasswordHash;

        $sms     = array(
            'otpNumber'    => $otp,
            'mobile_no'    => $mobNumber,
            'message'      => $smsMessage,
            'typeCode'     => 'FGP',
            'UserWalletID' => $partyId
        );
        // send SMS & insert OTP information in DB
        $otpInfo = \App\Libraries\IvrHelper::sendOtp($sms);

        $output_arr['is_error'] = FALSE;
        $msg                    = $userInfo->FirstName . ', Password Rest link sent to mobile number with OTP to validate. Please follow the steps';
        $output_arr['res_data'] = array( 'info' => $msg, 'sms_message' => $smsMessage, 'otp' => $otpInfo );
        return $output_arr;
    }

    private function setForgotPasswordHash($partyId, $uniqId)
    {
        $userConfig = \App\Models\Userconfiguration::select('Details')->where('UserWalletID', $partyId)
                ->where('ConfigType', 'forgotPasswordHash')
                ->first();
        // if already the record exsists
        if ( $userConfig )
        {
            $setHash = \App\Models\Userconfiguration::where('UserWalletID', $partyId)
                    ->where('ConfigType', 'forgotPasswordHash')
                    ->update([ 'Details' => $uniqId ]);

            return ( $setHash ) ? TRUE : FALSE;
        }
        // if no config param exsists
        $insert  = array(
            'ConfigType'   => 'forgotPasswordHash',
            'Details'      => $uniqId,
            'UserWalletID' => $partyId
        );
        $setHash = \App\Models\Userconfiguration::insert($insert);
        return ( $setHash ) ? TRUE : FALSE;
    }

    public function validateHashKey(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validate
        $validationCheck = \App\Libraries\IvrHelper::hashKeyValidate($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['res_data'] = $validationCheck;
            return $output_arr;
        }
        $encKey      = sha1(config('vwallet.ENCRYPT_SALT'));
        // decode fron base64
        $hash        = base64_decode($request->input('hashKey'));
        // decrypt hash key to compare
        $decryptData = \App\Libraries\Crypt\Decryptor::decrypt($hash, $encKey);

        $hashSaparate = explode('-', $decryptData);
        // Get user balances 
        $hashCheck    = \App\Models\Userconfiguration::select('Details')
                ->where('UserWalletID', $hashSaparate[0])
                ->where('ConfigType', 'forgotPasswordHash')
                ->where('Details', $decryptData)
                ->first();

        if ( ! $hashCheck )
        {
            $output_arr['res_data'] = array( 'info' => 'Request is not valid' );
            return $output_arr;
        }

        $output_arr['is_error'] = FALSE;
        $msg                    = 'Enter OTP to verify yourself.';
        $output_arr['res_data'] = array( 'info' => $msg );
        return $output_arr;
    }

    public function validateHashOtp(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validate
        $validationCheck = \App\Libraries\IvrHelper::hashKeyOtp($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['res_data'] = $validationCheck;
            return $output_arr;
        }

        // validate OTP
        $validationOtp = \App\Libraries\IvrHelper::validateOtp($request);
        if ( $validationOtp !== TRUE )
        {
            $output_arr['res_data'] = array( 'info' => 'Not valid OTP' );
            return $output_arr;
        }

        $output_arr['is_error'] = FALSE;
        $msg                    = 'Otp validation Done. Set New Password.';
        $output_arr['res_data'] = array( 'info' => $msg );
        return $output_arr;
    }

    public function functionName(\Illuminate\Http\Request $request)
    {
        
    }

}
