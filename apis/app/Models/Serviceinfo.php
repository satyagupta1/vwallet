<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Mar 2018 10:21:58 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Serviceinfo
 * 
 * @property int $ServiceInfoId
 * @property int $BookingMasterId
 * @property string $OperatorName
 * @property string $ServiceType
 * @property int $ServiceSource
 * @property int $ServiceDest
 * @property int $ServiceRating
 * @property string $Duration
 * @property string $OperatorAddress
 * @property string $OperatorContactNum
 * @property string $SourceDepartureTime
 * @property string $DestArrivalTime
 * @property string $Remarks
 * @property string $TermsConditions
 * @property string $CancellationPolicy
 * @property string $IsPartlCnclAllowed
 * @property string $TermsConditionsContent
 * @property string $CancellationPolicyContent
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Bookingmaster $bookingmaster
 *
 * @package App\Models
 */
class Serviceinfo extends Eloquent
{
	protected $table = 'serviceinfo';
	protected $primaryKey = 'ServiceInfoId';
	public $timestamps = false;

	protected $casts = [
		'BookingMasterId' => 'int',
		'ServiceSource' => 'int',
		'ServiceDest' => 'int',
		'ServiceRating' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'BookingMasterId',
		'OperatorName',
		'ServiceType',
		'ServiceSource',
		'ServiceDest',
		'ServiceRating',
		'Duration',
		'OperatorAddress',
		'OperatorContactNum',
		'SourceDepartureTime',
		'DestArrivalTime',
		'Remarks',
		'TermsConditions',
		'CancellationPolicy',
		'IsPartlCnclAllowed',
		'TermsConditionsContent',
		'CancellationPolicyContent',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bookingmaster()
	{
		return $this->belongsTo(\App\Models\Bookingmaster::class, 'BookingMasterId');
	}
}
