<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Recharge Fails
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Recharge Declined',
        'text'    => $salutationName . ',<br/><br/>
Sorry, your transaction of Rs '.$amount.' failed as the payment is declined by your bank due to one of the following reasons:<br/>
Network error or no response from operator<br/>
Please check and try again later.'],
    'sms'   => [
        'text' => 'Transaction of INR '.$amount.' declined due to <insufficient funds/ Network error/ no response from operator) on '.$date.' at '.$time.'. Balance amount '.$balanceamount,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Recharge Declined',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'Your recharge of INR '.$amount.' was declined',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
