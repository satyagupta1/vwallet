<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * reject request money
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */

$array          = [
    'email' => [
        'subject' => 'Request Money -- Canceled',
        'text'    => 'Hello, '.$receiverName.',<br/>Request you made for '.$amount.' from '.$senderName.' person has been cancelled. If you are in urgent need, Please reraise a request' ],
    'sms'   => [
        'text' => 'Your request for '.$amount.' from '.$senderName.' is cancelled'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Your request for '.$amount.' from '.$senderName.' is cancelled',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'genral'
    ],
    'web'   => [
        'text' => 'Your request for '.$amount.' from '.$senderName.' is cancelled.',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
