<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * add beneficiary failure
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $date Date
 *  param $time time
 *  param $fullname varchar 
 */
$msg = '';
if ( $IdentifierType == 'mobile' )
{
    $msg = 'Mobile Number: ' . $identifier1;
}
else if ( $IdentifierType == 'bank' )
{
    $accShortForm = $msg          = 'Account Number: ' . substr($identifier1, -4);
}
else if ( $IdentifierType == 'VPA' )
{
    $msg = 'VPA: ' . $identifier1;
}

$salutation     = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array          = [
    'email' => [
        'subject' => 'Add Beneficiary Failure',
        'text'    => $salutationName . ',<br/><br/>
Sorry, your request for adding ' . $msg . ' as beneficiary is failed due to one of the following reasons:<br/>
Network error or Beneficiary details are wrong<br/>
Please check and try again later.' ],
    'sms'   => [
        'text' => 'Sorry,your request for adding ' . $msg . ' as beneficiary is failed due to one of the following reasons: Network error or Beneficiary details are wrong'
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Add Beneficiary Failure',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'genral'
    ],
    'web'   => [
        'text' => 'Sorry, your request for adding ' . $msg . ' as beneficiary is failed due to one of the following reasons: Network error or Beneficiary details are wrong',
    ],
];

echo json_encode($array);

/* End of file recharge_fails */
