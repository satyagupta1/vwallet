<?php

namespace App\Libraries;

class OfferRules {

    static protected $staticBenefitCriteria = array( '28' => 'Fixed', '29' => 'Percentage', '30' => 'Least', '31' => 'Highest' );

    public static function verifyPromoCode($requestArray)
    {
        $ruleNames               = \App\Libraries\OfferRules::offerReferenceData();
        $ruleFlipData = array_flip($ruleNames);
        $output_arr             = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $selectedColumns        = [
            'offerbenefitsbridge.ActiveInd AS BridgeStatus',
            'offerbenefitsbridge.OfferBusinessID',
            'offerbenefitsbridge.BenefitBusinessID',
            'promotionaloffers.OfferVersion',
            'promotionaloffers.OfferName',
            'promotionaloffers.OfferDescription',
            'promotionaloffers.OfferCategoryRefID',
            'promotionaloffers.OfferTypeRefID',
            'promotionaloffers.PromoCode',
            'promotionaloffers.OfferUserTypeRefID',
            'promotionaloffers.BusinessEffectiveDate',
            'promotionaloffers.BusinessExpiryDate',
            'promotionaloffers.ApplicableChannel',
            'promotionaloffers.TermsConditionsFilePath',
            'promotionaloffers.EligibilityDetailsFilePath',
            'promotionaloffers.ActiveInd as PromotionStatus',
            'promotionaloffers.PromotionalOfferID',
            'offerbenefits.BenefitVersion',
            'offerbenefits.BenefitName',
            'offerbenefits.BenefitDescription',
            'offerbenefits.BenefitEffectiveDate',
            'offerbenefits.BenefitExpiryDate',
            'offerbenefits.BenefitCategoryRefID',
            'offerbenefits.BenefitCriteriaRefID',
            'offerbenefits.AdminWalletAccountId',
            'offerbenefits.ActiveInd AS BenefitStatus',
            'offerbenefits.BenefitApplicableChannel',
            'offerbenefits.BenefitFixedAmount',
            'offerbenefits.BenefictPercentageValue',
            'offerbenefits.OfferBenefitID',
            'offerbenefits.CashBackDelayPeriod',
            'offerbenefits.CashBackTyp',
            'offerbenefits.CashBackExpiryPeriod',
        ];
        $promoCode              = isset($requestArray['promoCode'])?$requestArray['promoCode']:NULL;
        $offerCategoryId = isset($requestArray['offerCategoryId'])?$requestArray['offerCategoryId']:NULL;
        $offerUserTypeId = isset($requestArray['offerUserTypeId'])?$requestArray['offerUserTypeId']:NULL;
        //$paymentProductId = isset($requestArray['paymentProductId'])?$requestArray['paymentProductId']:NULL;
        $benefitCategoryId = isset($requestArray['benefitCategoryId'])?$requestArray['benefitCategoryId']:NULL;
        $isVerified              = isset($requestArray['isVerified'])?$requestArray['isVerified']:FALSE;

        $requestArray['amount'] = isset($requestArray['amount']) ? $requestArray['amount'] : 0;
        $clauses                = [ 'offerbenefitsbridge.ActiveInd' => 'Y', 'offerbenefits.ActiveInd' => 'Y', 'promotionaloffers.ActiveInd' => 'Y' ];
        if ( $promoCode !== NULL )
        {
            $clauses = array_merge($clauses, [ 'promotionaloffers.PromoCode' => $promoCode ]);
        }
        if ( $offerCategoryId !== NULL )
        {       
            $categoryId = $ruleFlipData[$offerCategoryId];
            $offerCategoryId = self::_verifyGlobalCategory($promoCode, $categoryId);
            $clauses = array_merge($clauses, [ 'promotionaloffers.OfferCategoryRefID' => $offerCategoryId ]);
        }
        if ( $offerUserTypeId !== NULL )
        {
            $offerUserTypeId = $ruleFlipData[$offerUserTypeId];
            $clauses = array_merge($clauses, [ 'promotionaloffers.OfferUserTypeRefID' => $offerUserTypeId ]);
        }
        
        if ( $benefitCategoryId !== NULL )
        {
            $clauses = array_merge($clauses, [ 'offerbenefits.BenefitCategoryRefID' => $benefitCategoryId ]);
        }
         
        //print_r($clauses);exit;
        $dbResults = \App\Models\Offerbenefit::join('offerbenefitsbridge', 'offerbenefits.BenefitBusinessID', '=', 'offerbenefitsbridge.BenefitBusinessID')
                        //->join('userwallet', 'dailytransactionlog.PartyId', '=', 'userwallet.UserWalletID')
                        ->rightjoin('promotionaloffers', 'promotionaloffers.OfferBusinessID', '=', 'offerbenefitsbridge.OfferBusinessID')
                        ->orderBy('offerbenefits.BenefitCategoryRefID', 'ASC')
                        ->where($clauses)
                        ->get($selectedColumns)->first();

        $countResults = count($dbResults);        
        if ( $countResults < 1 )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidPromoCode');
            self::OfferLog($promoCode, $output_arr['display_msg']['info']);
            return json_encode($output_arr);
        }

        $dateTimeOfTrans = date('Y-m-d H:i:s');
        if($isVerified == FALSE) {

        $userOfferStats = array(
            'UserWalletID'       => isset($requestArray['partyId'])?$requestArray['partyId']:0,
            'OfferBusinessID'        => $dbResults->OfferBusinessID,
            'PromoCode'             =>  $dbResults->PromoCode,
            'UserActivity'            => 'View',
            'ActivityDateTime'          => $dateTimeOfTrans,
            'AmountUsed'          => '0.00',
            'CreatedDateTime'            => $dateTimeOfTrans,
            'CreatedBy'              => 'User',
        );
        $insertId = \App\Models\Useroffersstat::insertGetId($userOfferStats);

        //Promotion Eligibility
        $offerElgibility = self::promotionEligibility($requestArray, $dbResults);
        if ( $offerElgibility['is_error'] == TRUE )
        {
            self::OfferLog($promoCode, $offerElgibility['display_msg']['info']);
            $output_arr['display_msg']['info'] = $offerElgibility['display_msg']['info'];
            //$output_arr['res_data']            = $dbResults;
            return json_encode($output_arr);
        }
        //Benifit Eligibility
        /*$benifitElgibility = self::benifitEligibility($requestArray, $dbResults);
        if ( $benifitElgibility['is_error'] == TRUE )
        {
            self::OfferLog($promoCode, $benifitElgibility['display_msg']['info']);
            $output_arr['display_msg']['info'] = $benifitElgibility['display_msg']['info'];
            //$output_arr['res_data']            = $dbResults;
            return json_encode($output_arr);
        }
        //print_r($requestArray);exit;
        //Benifit Rules Eligibility
        $benifitRuleElgibility = self::benifitRulesEligibility($requestArray, $dbResults);
        if ( $benifitRuleElgibility['is_error'] == TRUE )
        {
            self::OfferLog($promoCode, $benifitRuleElgibility['display_msg']['info']);
            $output_arr['display_msg']['info'] = $benifitRuleElgibility['display_msg']['info'];
            //$output_arr['res_data']            = $dbResults;
            return json_encode($output_arr);
        }*/
        
        //Expiry rules eligibility
        $expiryElgibility = self::expiryRulesEligibility($requestArray, $dbResults);
        if ( $expiryElgibility['is_error'] == TRUE )
        {
            self::OfferLog($promoCode, $expiryElgibility['display_msg']['info']);
            $output_arr['display_msg']['info'] = $expiryElgibility['display_msg']['info'];
            //$output_arr['res_data']            = $dbResults;
            return json_encode($output_arr);
        }

        //User rules eligibility
        $userElgibility = self::userRulesEligibility($requestArray, $dbResults);
        if ( $userElgibility['is_error'] == TRUE )
        {
            self::OfferLog($promoCode, $userElgibility['display_msg']['info']);
            $output_arr['display_msg']['info'] = $userElgibility['display_msg']['info'];
            //$output_arr['res_data']            = $dbResults;
            return json_encode($output_arr);
        }
        
    }
        $getBenefitAmout = self::getBenefitAmount($requestArray, $dbResults);
        if ( $getBenefitAmout['is_error'] == TRUE )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidPromoCode');
             return json_encode($output_arr);
        }
        if ( $isVerified == FALSE )
        {
            $userOfferStats['UserActivity'] = 'Verified';
            $userOfferStats['AmountUsed']   = $getBenefitAmout['res_data']['benifitAmount'];
            \App\Models\Useroffersstat::insertGetId($userOfferStats);
        }
        //only for topup category offers
        if ( $offerCategoryId == '4' )
        {
            $selectUserColumn      = [ 'OfferBusinessID', 'OfferVersion', 'RuleCategoryRefID', 'RuleNameRefID', 'RuleOperatorRefID', 'RuleParamValues' ];
            $whereClauses          = [ 'OfferBusinessID' => $dbResults->OfferBusinessID, 'OfferVersion' => $dbResults->OfferVersion, 'RuleNameRefID' => '63', 'RuleCategoryRefID' => '33' ];
            $userTransactionChanel = \App\Models\Offerusereligibility::where($whereClauses)->get($selectUserColumn);
            $output_arr['res_data']['transactionChannel'] = "All";            
            if ( $userTransactionChanel->count() > 0 )
            {
                $output_arr['res_data']['transactionChannel'] = $userTransactionChanel[0]->RuleParamValues;
            }
        }
        $output_arr['is_error']                  = FALSE;
        $output_arr['display_msg']['info'] = trans('messages.promoCodeVerified');
        $output_arr['res_data']['benifitAmount'] = $getBenefitAmout['res_data']['benifitAmount'];
        $output_arr['res_data']['benifitCategory'] = $ruleNames[$dbResults->BenefitCategoryRefID];
        $output_arr['res_data']['offerInfo']     = $dbResults;
        return json_encode($output_arr);

    }
    /**
     * Calculate benefit amount
     * @param type $requestArray
     * @param type $dbRecords
     * @return boolean
     */
    public static function getBenefitAmount($requestArray, $dbRecords)
    {
       
        $amount          = $requestArray['amount'];        
        $output_arr      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $benefitCriteriaRefID       = $dbRecords->BenefitCriteriaRefID;
        $staticBenefitCriteriaArray = self::$staticBenefitCriteria;
        $benefitCriteriaCode        = NULL;
        if ( isset($staticBenefitCriteriaArray[$benefitCriteriaRefID]) )
        {
            $benefitCriteriaCode = $staticBenefitCriteriaArray[$benefitCriteriaRefID];
        }
        switch ( trim($benefitCriteriaCode) )
        {

            case 'Fixed':
                //BenefitFixedAmount
                $benifitAmount = $dbRecords->BenefitFixedAmount;
                break;
            case 'Percentage':
                //BenefictPercentageValue
                $benifitAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                break;
            case 'Least':
                //Least Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / 100) * $amount;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount < $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            case 'Highest':
                //Heighest Amount
                $percentAmount = ($dbRecords->BenefictPercentageValue / $amount) * 100;
                $fixedAmount   = $dbRecords->BenefitFixedAmount;
                $benifitAmount = $percentAmount;
                if ( $fixedAmount > $percentAmount )
                {
                    $benifitAmount = $fixedAmount;
                }
                break;
            default:
                $benifitAmount = '0.00';
        }

        $benifitAmount          = number_format(( float ) $benifitAmount, 2, '.', '');
        $output_arr['res_data'] = array( 'benifitAmount' => $benifitAmount );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    public static function promotionEligibility($requestArray, $dbRecords)
    {
        $output_arr      = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $promoCode       = $requestArray['promoCode'];
        $offerCategoryId = isset($requestArray['offerCategoryId']) ? $requestArray['offerCategoryId'] : 0;
        $offerUserTypeId = isset($requestArray['offerUserTypeId']) ? $requestArray['offerUserTypeId'] : 0;

        if ( $dbRecords->OfferCategoryRefID != '0' && $offerCategoryId != 0 && $offerCategoryId != $dbRecords->OfferCategoryRefID )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidOfferCategory');
            return $output_arr;
        }

        if ( $dbRecords->OfferUserTypeRefID != '0' && $offerUserTypeId != 0 && $offerUserTypeId != $dbRecords->OfferUserTypeRefID )
        {
            //Faild offer category
            $output_arr['display_msg']['info'] = trans('messages.invalidOfferUserType');
            return $output_arr;
        }
        $businessEffectiveDate = strtotime($dbRecords->BusinessEffectiveDate);
        $businessExpiryDate    = strtotime($dbRecords->BusinessExpiryDate);

        $dateTime = strtotime(date('Y-m-d H:i:s'));
        if ( $dateTime < $businessEffectiveDate )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBussinessEffectiveDate');
            return $output_arr;
        }

        if ( $dateTime > $businessExpiryDate )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBussinessExpiaryDate');
            return $output_arr;
        }
        $deviceType = $requestArray['aplicableChannel'];
        if ( $dbRecords->ApplicableChannel != 'B' )
        {
            if ( ($dbRecords->ApplicableChannel != 'W' && $deviceType == 'W') || ($dbRecords->BenefitApplicableChannel == 'W' && $deviceType != 'W') )
            {
                $output_arr['display_msg']['info'] = trans('messages.invalidApplicableChanel');
                return $output_arr;
            }
        }
        return $output_arr = array( 'is_error' => FALSE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
    }

    public static function benifitEligibility($requestArray, $dbRecords)
    {
        $output_arr        = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $promoCode         = $requestArray['promoCode'];
        $benefitCategoryId = isset($requestArray['benefitCategoryId']) ? $requestArray['benefitCategoryId'] : 0;

        if ( $benefitCategoryId != '0' && $benefitCategoryId != $dbRecords->BenefitCategoryRefID )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBenefitCatRef');
            return $output_arr;
        }
        $businessEffectiveDate = strtotime($dbRecords->BenefitEffectiveDate);
        $businessExpiryDate    = strtotime($dbRecords->BenefitExpiryDate);

        $dateTime = strtotime(date('Y-m-d H:i:s'));
        if ( $dateTime < $businessEffectiveDate )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBussinessEffectiveDate');
            return $output_arr;
        }

        if ( $dateTime > $businessExpiryDate )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBussinessExpiaryDate');
            return $output_arr;
        }
        $deviceType = $requestArray['aplicableChannel'];
        if ( $dbRecords->BenefitApplicableChannel != 'B' )
        {
            if ( ($dbRecords->BenefitApplicableChannel != 'W' && $deviceType == 'W') || ($dbRecords->BenefitApplicableChannel == 'W' && $deviceType != 'W') )
            {
                $output_arr['display_msg']['info'] = trans('messages.invalidApplicableChanel');
                return $output_arr;
            }
        }
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    public static function benifitRulesEligibility($requestArray, $dbRecords)
    {
        //print_r($requestArray);exit;
        $promoCode                = $requestArray['promoCode'];
        //$amount                 = $requestArray['amount'];
        //$requestData = ['ReferenceType' => 'OfferCategory', 'ReferenceCode'=>'UPI','ReferenceId' => '3'];
        $requestData              = [ 'ReferenceType' => 'RuleName' ];
        $ruleNames                = OfferRules::offerReferenceData($requestData);
        $requestData              = [ 'ReferenceType' => 'RuleOperator' ];
        $ruleOperators            = OfferRules::offerReferenceData($requestData);
        //print_r($ruleOperators);exit;
        $output_arr               = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $selectUserColumn         = [ 'OfferBenefitEligibilityID', 'BenefitBusinessID', 'BenefitVersion', 'RuleCategoryRefID', 'RuleNameRefID', 'RuleOperatorRefID', 'RuleParamValues' ];
        $whereClauses             = [ 'BenefitBusinessID' => $dbRecords->BenefitBusinessID, 'BenefitVersion' => $dbRecords->BenefitVersion ];
        $benefitEligibility       = \App\Models\Offerbenefiteligibility::where($whereClauses)->get($selectUserColumn);
        //print_r($benefitEligibility);exit;
        $benifitEligibilityStatus = TRUE;
        if ( $benefitEligibility->count() > 0 )
        {
            foreach ( $benefitEligibility as $eachRule )
            {
                $benifitEligibilityCode = NULL;
                if ( isset($ruleNames[$eachRule->RuleNameRefID]) )
                {
                    $benifitEligibilityCode = $ruleNames[$eachRule->RuleNameRefID];
                }
                switch ( trim($benifitEligibilityCode) )
                {
                    case 'PerTransactionAmount':
                        $inputValue     = $requestArray['amount']; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'CardFlag':
                        $inputValue     = 1; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'ProductID':
                        $inputValue     = 1; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                }
                if ( ! $operatorstatus )
                {
                    $benifitEligibilityStatus = FALSE;
                    OfferRules::OfferLog($promoCode, 'Offerusereligibility ' . $benifitEligibilityCode . ' failed.');
                    break;
                };
            }
        }
        if ( ! $benifitEligibilityStatus )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidBenefitRule');
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    public static function userRulesEligibility($requestArray, $dbRecords)
    {

        //print_r($dbRecords);exit;
        $promoCode             = $requestArray['promoCode'];
        $partyId               = isset($requestArray['partyId'])?$requestArray['partyId']:NULL;
        $offerProductId = isset($requestArray['paymentProductId'])?$requestArray['paymentProductId']:0;

        $inputAmount                 = $requestArray['amount'];
        //$requestData = ['ReferenceType' => 'OfferCategory', 'ReferenceCode'=>'UPI','ReferenceId' => '3'];
        $requestData           = [ 'ReferenceType' => 'RuleName' ];
        $ruleNames             = OfferRules::offerReferenceData($requestData);
        $requestOptData           = [ 'ReferenceType' => 'RuleOperator' ];
        $ruleOperators         = OfferRules::offerReferenceData($requestOptData);
        $requesCatgData           = [ 'ReferenceType' => 'RuleCategory' ];
        $ruleCategorys         = OfferRules::offerReferenceData($requesCatgData);
        
        //print_r($ruleOperators);exit;
        $output_arr            = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $selectUserColumn      = [ 'OfferBusinessID', 'OfferVersion', 'RuleCategoryRefID', 'RuleNameRefID', 'RuleOperatorRefID', 'RuleParamValues' ];
        $whereClauses          = [ 'OfferBusinessID' => $dbRecords->OfferBusinessID, 'OfferVersion' => $dbRecords->OfferVersion ];
        $userEligibility       = \App\Models\Offerusereligibility::where($whereClauses)->get($selectUserColumn);

        //print_r($userEligibility);exit;
        $userEligibilityStatus = TRUE;
        if ( $userEligibility->count() > 0 )
        {
            // get userwallet data
            $userWalletData = NULL;
            if ($partyId == NULL)
            {
                $output_arr['display_msg']['info'] = trans('messages.invalidUserRule');
                return $output_arr;

            }

            $userWalletData = self::_getUserData($partyId);
            if ($userWalletData === NULL)
            {
                //$output_arr['is_error'] = FALSE;
                $output_arr['display_msg']['info'] = trans('messages.invalidUserRule');
                return $output_arr;
            }
            
            //get data as for            
            $busData = NULL;
            foreach ( $userEligibility as $eachRuleCategory )
            {
                $ruleCategoryCode = $ruleCategorys[$eachRuleCategory->RuleCategoryRefID];
                switch ( trim($ruleCategoryCode) )
                {
                    case 'BusRules':
                        $busData = self::_getBusInfo($partyId);
                        break;
                }
            }            
            // calculate age
            $dateOfBirth = ($userWalletData->DateofBirth) ? $userWalletData->DateofBirth : date('Y-m-d');
            $diff = date_diff(date_create($dateOfBirth), date_create(date('Y-m-d')));
            $age = $diff->format('%y');

            // get userOffersStatus
            $userOffersStats = self::_getOffersStats($promoCode, $partyId);

            // get UtilizationVlauePerUser
            $userVlaue = self::_getUtilizeUserVlaue($partyId, $promoCode);

            foreach ( $userEligibility as $eachRule )
            {
                $operatorstatus      = TRUE;
                $userEligibilityCode = NULL;
                if ( isset($ruleNames[$eachRule->RuleNameRefID]) )
                {
                    $userEligibilityCode = $ruleNames[$eachRule->RuleNameRefID];
                }

                switch ( trim($userEligibilityCode) )
                {

                    case 'KYCStatus':
                        $inputValue     = $userWalletData->IsVerificationCompleted; 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'MaxUtilizationCountPerUser':
                        $inputValue     = $userOffersStats->count(); 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'MaxUtilizationVlauePerUser':
                        $inputValue     = $userVlaue->sum('AmountUsed'); 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'UserType':
                        $inputValue     = $userWalletData->PartyType; 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'Age':
                        $inputValue     = $age; 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'Gender':
                        $inputValue     = ($userWalletData->Gender) ? $userWalletData->Gender : 'N'; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'State':
                        $inputValue     = $userWalletData->State; 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'WalletTurnover':
                        $inputValue     = ($userWalletData->CurrentAmount) ? $userWalletData->CurrentAmount : '0.00'; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'WalletFrequency':
                        $inputValue     = '1'; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'MinTransactionAmount':
                        $inputValue     = $inputAmount;
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                    case 'ProductID':
                        $inputValue     = $offerProductId; 
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;                        
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);                        
                        break;
                    case 'BusSource':
                        $inputValue     = isset($busData->Source)?$busData->Source:0;
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;                        
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);                        
                        break;
                    case 'BusTwoWay':
                        $inputValue     = isset($busData->ReturnBookingId)?$busData->ReturnBookingId:0; //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;                        
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);                        
                        break;
                }
                if ( ! $operatorstatus )
                {
                    $userEligibilityStatus = FALSE;
                    OfferRules::OfferLog($promoCode, 'Offerusereligibility ' . $userEligibilityCode . ' failed.');
                    break;
                };
            }
        }
        if ( ! $userEligibilityStatus )
        {

            $output_arr['display_msg']['info'] = trans('messages.invalidUserRule');
            //$output_arr['display_msg']['info'] =trans('messages.nameFaild', ['name' => $userEligibilityCode]);
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    private static function _getUserData($partyId)
    {
        $userWalletData = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.Gender', 'userwallet.DateofBirth', 'userwallet.ProfileStatus', 'userwallet.IsVerificationCompleted', 'party.PartyType', 'partyaddress.State', 'walletaccount.CurrentAmount')
                ->join('party', 'party.PartyID', '=', 'userwallet.UserWalletID')
                ->join('partyaddress', 'partyaddress.PartyID', '=', 'userwallet.UserWalletID')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $partyId)->first();
        return $userWalletData;


    }
    /*
     * if promo code defined as gloable override default category.
     */
    private static function _verifyGlobalCategory($promoCode, $catId)
    {        
        $offerData = \App\Models\Promotionaloffer::select('OfferBusinessID', 'OfferCategoryRefID')
                        ->where('PromoCode', $promoCode)->first();
        return (isset($offerData) && $offerData->OfferCategoryRefID == 5)? $offerData->OfferCategoryRefID : $catId;
    }

    private static function _getUtilizeUserVlaue($partyId, $promoCode)
    {
        $whereClauses = [ 'UserWalletID' => $partyId, 'PromoCode' => $promoCode, 'UserActivity' => 'Used' ];
        $UserOfferStatus = \App\Models\Useroffersstat::select('AmountUsed')->where($whereClauses)->get();
        return $UserOfferStatus;
    }
    private static function _getBusInfo($partyId){
        $bookingObj = NULL;       
        if ( $partyId > 0 )
        {
            $whereClauses = [ 'UserWalletID' => $partyId, 'BookingStatus' => 'Blocked' ];
            $bookingObj = \App\Models\Bookingmaster::select('Id', 'UserWalletID', 'ReturnBookingId', 'Source', 'Destination', 'NoOfBookedSeats')->where($whereClauses)->orderBy('Id', 'DESC')->first();
        }
        return $bookingObj;
    }

    public static function expiryRulesEligibility($requestArray, $dbRecords)
    {
        //print_r($dbRecords);exit;
        $promoCode               = $requestArray['promoCode'];
        //$amount                 = $requestArray['amount'];
        //$requestData = ['ReferenceType' => 'OfferCategory', 'ReferenceCode'=>'UPI','ReferenceId' => '3'];
        $requestData             = [ 'ReferenceType' => 'RuleName' ];
        $ruleNames               = OfferRules::offerReferenceData($requestData);
        $requestData             = [ 'ReferenceType' => 'RuleOperator' ];
        $ruleOperators           = OfferRules::offerReferenceData($requestData);
        //print_r($ruleOperators);exit;
        $output_arr              = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $selectUserColumn        = [ 'OfferBusinessID', 'OfferVersion', 'RuleCategoryRefID', 'RuleNameRefID', 'RuleOperatorRefID', 'RuleParamValues' ];
        $whereClauses            = [ 'OfferBusinessID' => $dbRecords->OfferBusinessID, 'OfferVersion' => $dbRecords->OfferVersion ];
        $expiryEligibility       = \App\Models\Offerexpiryrule::where($whereClauses)->get($selectUserColumn);
        //print_r($expiryEligibility);exit;
        $expiryEligibilityStatus = TRUE;
        if ( $expiryEligibility->count() > 0 )
        {
            // get OffersStatus
            $OffersStats = self::_getOffersStats($promoCode);

            foreach ( $expiryEligibility as $eachRule )
            {
                $operatorstatus        = TRUE;
                $expiryEligibilityCode = NULL;
                if ( isset($ruleNames[$eachRule->RuleNameRefID]) )
                {
                    $expiryEligibilityCode = $ruleNames[$eachRule->RuleNameRefID];
                }
                switch ( trim($expiryEligibilityCode) )
                {
                    case 'FirstXTransactions':
                        $inputValue     = $OffersStats->count(); //need dynamic
                        $operator       = $ruleOperators[$eachRule->RuleOperatorRefID];
                        $paramValue     = $eachRule->RuleParamValues;
                        $operatorstatus = OfferRules::queryOperator($inputValue, $operator, $paramValue);
                        break;
                }
                if ( ! $operatorstatus )
                {
                    $expiryEligibilityStatus = FALSE;
                    OfferRules::OfferLog($promoCode, 'Offerusereligibility ' . $expiryEligibilityCode . ' failed.');
                    break;
                };
            }
        }
        if ( ! $expiryEligibilityStatus )
        {
            $output_arr['display_msg']['info'] = trans('messages.invalidExpiryRule');
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

     private static function _getOffersStats($promoCode, $partyId = 0)
    {
         /*
        $fromDate = date('Y-m-d', strtotime('last year'));
        $toDate = date('Y-m-d');
        $UserOfferStatus = \App\Models\Useroffersstat::select('UserUsedOffersId')->where([['UserWalletID', $partyId], ['PromoCode', $promoCode], ['UserActivity', 'Used']])->whereBetween('ActivityDateTime', [$fromDate, $toDate])->get();
          *
          */
        $whereClauses = [ 'UserWalletID' => $partyId, 'PromoCode' => $promoCode, 'UserActivity' => 'Used' ];
        if ($partyId == 0)
        {
            $whereClauses = [ 'PromoCode' => $promoCode, 'UserActivity' => 'Used' ];
        }
        $UserOfferStatus = \App\Models\Useroffersstat::select('UserUsedOffersId')->where($whereClauses)->get();
        return $UserOfferStatus;
    }

    public static function offerReferenceData($requestData = array())
    {
        if ( empty($requestData) )
        {
            $requestData = [];
            //$requestData = [ 'ActiveIndicator' => 'Y' ];
        }
        $dbResults  = \App\Models\Reference::select('ReferenceID', 'ReferenceCode', 'ReferenceDesc', 'ActiveIndicator')
                ->orderBy('ReferenceId')
                ->where($requestData)
                ->get();
        $returnData = [];
        if ( $dbResults->count() > 0 )
        {
            foreach ( $dbResults as $eachReference )
            {
                $returnData[$eachReference->ReferenceID] = $eachReference->ReferenceCode;
            }
        }
        return $returnData;
    }

    private static function queryOperator($inputValue, $operator, $paramValue)
    {
        $result = FALSE;
        switch ( trim($operator) )
        {

            case "=":
                if ( $inputValue == $paramValue )
                {
                    $result = TRUE;
                }
                break;
            case ">":
                if ( $inputValue > $paramValue )
                {
                    $result = TRUE;
                }
                break;
            case "<":
                if ( $inputValue < $paramValue )
                {
                    $result = TRUE;
                }
                break;
            case "IN":
                $value = explode(',', $paramValue);
                if (in_array($inputValue, $value))
                {
                    $result = TRUE;
                }
                break;
            default :
                $result = FALSE;
        }
        //$query = $inputValue . $operator . $paramValue;
        //OfferRules::OfferLog($result, $query);
        return $result;
    }

    public static function getBenifitPaymentInfo($offerData)
    {      
        //$benefitCategoryRef = config('vwallet.benefitCategoryRef');
        $requestData             = [ 'ReferenceType' => 'BenefitCategory' ];
        $benefitCategoryRef               = OfferRules::offerReferenceData($requestData);

        $benifitAmount      = $offerData->benifitAmount;
        $offerInfo          = $offerData->offerInfo;
        // print_r($offerInfo->BenefitCategoryRefID); exit;

        $benefitCategoryRefID = $offerInfo->BenefitCategoryRefID;
        $adminWalletAccountId = $offerInfo->AdminWalletAccountId;
        $offerBusinessID = $offerInfo->OfferBusinessID;
        $promoCode = $offerInfo->PromoCode;
        $benefitBusinessID = $offerInfo->BenefitBusinessID;
        $benefitCashBackDelayPeriod = $offerInfo->CashBackDelayPeriod;
        $benefitCashBackTyp = $offerInfo->CashBackTyp;
        $benefitCategoryCode = NULL;
        if ( isset($benefitCategoryRef[$benefitCategoryRefID]) )
        {
            $benefitCategoryCode = $benefitCategoryRef[$benefitCategoryRefID];
        }
        $pamentInfo = array();
        switch ( trim($benefitCategoryCode) )
        {
            case 'InstantRebate':
                $pamentInfo['PaymentsProductID'] = 7;
                $pamentInfo['productEventId'] = 12;
                $pamentInfo['event']          = 'DISCOUNT';
                $pamentInfo['amountTag']      = 'DISCOUNT_AMT';
                $pamentInfo['txnType']        = 'OFFER';
                $pamentInfo['message']        = 'Discount added successfully';
                $pamentInfo['offerAmount']        = $benifitAmount;
                $pamentInfo['vendor']        = 'Viola Wallet';
                $pamentInfo['offerAdminWalletAccountId'] = $adminWalletAccountId;
                $pamentInfo['offerBusinessId'] = $offerBusinessID;
                $pamentInfo['promoCode'] = $promoCode;
                $pamentInfo['benefitBusinessId'] = $benefitBusinessID;
                $pamentInfo['benefitCashBackDelayPeriod'] = $benefitCashBackDelayPeriod;
                $pamentInfo['benefitCashBackTyp'] = $benefitCashBackTyp;

                break;
            case 'Cashback':
            case 'DelayedCashback':
                $pamentInfo['PaymentsProductID'] = 7;
                $pamentInfo['productEventId'] = 10;
                $pamentInfo['event']          = 'CASHBACK';
                $pamentInfo['amountTag']      = 'CASH_BACK_AMT';
                $pamentInfo['txnType']        = 'OFFER';
                $pamentInfo['message']        = 'Cashback added for using promocode '.$promoCode;
                $pamentInfo['offerAmount']        = $benifitAmount;
                $pamentInfo['vendor']        = 'Viola Wallet';
                $pamentInfo['offerAdminWalletAccountId'] = $adminWalletAccountId;
                $pamentInfo['offerBusinessId'] = $offerBusinessID;
                $pamentInfo['promoCode'] = $promoCode;
                $pamentInfo['benefitBusinessId'] = $benefitBusinessID;
                $pamentInfo['benefitCashBackDelayPeriod'] = $benefitCashBackDelayPeriod;
                $pamentInfo['benefitCashBackTyp'] = $benefitCashBackTyp;
                break;
            default:
                $pamentInfo      = array();
        }
        return $pamentInfo;
    }

    public static function getSuperCashPaymentInfo($request)
    {
        $pamentInfo    = [];
        $partyId       = $request['partyId'];
        $amount        = $request['amount'];
        $superCashData = \App\Libraries\OfferRules::getSuperCash($partyId, $amount);
        if($superCashData['is_error'] == TRUE) {
        return $pamentInfo;
        }
        $benifitAmount = $superCashData['res_data']['superCash'];

        $pamentInfo['PaymentsProductID']         = 8;
        $pamentInfo['productEventId']            = 91;
        $pamentInfo['event']                     = 'SUPERCASH';
        $pamentInfo['amountTag']                 = 'SUPERCASH_AMT';
        $pamentInfo['txnType']                   = 'Super Cash';
        $pamentInfo['message']                   = 'Super cash used.';
        $pamentInfo['offerAmount']               = $benifitAmount;
        $pamentInfo['vendor']                    = 'Cash Back Wallet';
        $pamentInfo['offerAdminWalletAccountId'] = '12';
        return $pamentInfo;
    }

    public static function getSuperCash($partyId, $tranAmount = 0){
        $output_arr            = array( 'is_error' => TRUE, 'display_msg' => array( 'info' => trans('messages.emptyRecords') ), 'status_code' => 200, 'res_data' => array() );
        $maxCashback           = config('vwallet.maxCashback');
        $maxCashback           = number_format(( float ) $maxCashback, 2, '.', '');
        $cashbackRuleMinAmount = config('vwallet.cashbackRuleMinAmount');
        $dateTime              = date('Y-m-d H:i:s');
        if ( $tranAmount < $cashbackRuleMinAmount )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.cashBackNotApplicable') );
            return $output_arr;
        }

        $response        = [];
        //$bnftTrackData = \App\Models\Userofferbenefittracker::where('UserWalletID', $partyId)->where('EarnedAmount', '>', '0.00')->where('ExtendedValidityDate', '>', $dateTime)->where('CashbackTransactionDetailID', '>', 0)->where('ActivateDate', '<=', $dateTime)->orderBy('ExtendedValidityDate', 'desc')->sum('EarnedAmount');
        $walletObj       = \App\Models\Walletaccount::select('NonWithdrawAmount')->where('UserWalletID', $partyId)->first();
        $countResults = count($walletObj);
        if ( $countResults > 0 )
        {
            $caschBackAmount = number_format(( float ) $walletObj->NonWithdrawAmount, 2, '.', '');
            if ( $countResults > 0 )
            {
                $superCash                 = ($caschBackAmount >= $maxCashback) ? $maxCashback : $caschBackAmount;
                $response                  = array( 'EarnedAmount' => $caschBackAmount, 'maxCashback' => $maxCashback, 'superCash' => $superCash );
                $output_arr['is_error']    = FALSE;
                $output_arr['display_msg'] = array( 'info' => trans('messages.cashBackApplicable') );
                $output_arr['res_data']    = $response;
            }
        }
        return $output_arr;
    }

    public static function referralCashBack($request){

        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $requestArray          = \App\Libraries\TransctionHelper::referralPromoCodeInfo($request);
        if(empty($requestArray)) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidPromoCode') );
            return json_encode($output_arr);
        }    
        $regPartyId   = $request->input('partyId');
        $referralCode = $request->input('usedReferralCode');
        
        $results = \App\Models\Userwallet::select('userwallet.FirstName', 'userwallet.LastName',  'userwallet.EmailID', 'userwallet.MobileNumber')
                        ->where('userwallet.UserWalletID', '=', $regPartyId)->first();
        if ( ! $results )
        {
            $data_send['display_msg'] = array( 'info' => trans('messages.wrongUser') );
            return response()->json($data_send);
        }
        $referer = \App\Models\Userwallet::join('userregistration', 'userwallet.UserWalletID', '=', 'userregistration.UserWalletID')
                ->where('userregistration.RegisteredReferralCode', $referralCode)
                ->select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.LastName', 'userwallet.MobileNumber', 'userwallet.EmailID')
                ->first();
        if(!$referer) {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidReferralCode') );
            return json_encode($output_arr);
        }
        //Giving cashback to registerd user
        $request->request->add([ 'partyId' => $regPartyId ]);
        // $cashBackStatus = \App\Libraries\TransctionHelper::cashBackTransaction($request, $verifiedPromoCodeData);
        $cashBackStatus = \App\Libraries\OfferBussness::addrefferbenefitTrack($request, $requestArray);
        if ($cashBackStatus->is_error  == TRUE)
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.cashBackTransFaild'));
            return json_encode($output_arr);
        }
        
         $refPartyId                = $referer->UserWalletID;
        //Giving cashback to refferal user
        $request->request->add([ 'partyId' => $refPartyId ]);      
        
        $cashBackStatus = \App\Libraries\OfferBussness::addrefferbenefitTrack($request, $requestArray);
        
        if ($cashBackStatus->is_error  == TRUE)
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.cashBackTransFaild') );
            return json_encode($output_arr);
        }
        
        $senderName = ($results->FirstName) ? $results->FirstName : '';
        
        $receiverName = $referer->FirstName . ' ' . $referer->LastName;
        if ( $receiverName == ' ' )
        {
            $receiverName = 'Customer';
        }
        $templateData = array(
            'partyId'        => $regPartyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW006',
            'senderEmail'    => $results->EmailID,
            'senderMobile'   => $results->MobileNumber,
            'reciverId'      => $referer->UserWalletID, // if reciver / sender exsists
            'reciverEmail'   => $referer->EmailID,
            'reciverMobile'  => $referer->MobileNumber,
            'templateParams' => array( // template utalisation parameters 
                'fullname'     => $senderName,
                'email'        => $results->EmailID,
                'mobileNumber' => $results->MobileNumber,
                'senderName' => $receiverName,
                'receiverName' => $receiverName,
                'referalCode' => $referer->RegisteredReferralCode,
                'ammount' => $requestArray['amount'],
            )
        );
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);

        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.cashBackTransSuccess'));
        return json_encode($output_arr);
    }

    public static function OfferLog($offerCode, $message, $msgType = 'Offer')
    {
        // $msgType  = 'Offer';
        $path     = 'SystemLog/' . $msgType . '/';
        $newPath  = rtrim(app()->basePath('public/' . $path), '/');
        $dateTime = date('Y-m-d H:i:s');
        if ( ! is_dir($newPath) )
        {
            mkdir($newPath, 0777, TRUE);
        }
        $uniqueId      = date("Ym", strtotime($dateTime));
        $fileName      = $uniqueId . '.log';
        $storeFullPath = $path . $fileName;
        $file          = fopen($newPath . '/' . $fileName, 'a+');
        fwrite($file, date('d/m/Y h:i:s') . ' :: ' . $offerCode . ' :: ' . $message . "\r\n");
        fclose($file);
    }

}

?>
