<?php

namespace App\Libraries;

class TransctionHelper {
    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    public static function _userValidate($partyId)
    {
        $userExsist = \App\Models\Userwallet::select('userwallet.FirstName', 'userwallet.UserWalletID', 'userwallet.MobileNumber', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsVerificationCompleted', 'userwallet.WalletPlanTypeId', 'userwallet.KycStatus', 'userwallet.WalletStatus')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $partyId)
                ->first();
        return( ! $userExsist ) ? FALSE : $userExsist;
    }

    /*
     *  getUserSessionId - Get logged in user sessionId
     *  @param object $results
     *  @return Array
     */

    public static function getUserSessionId($request)
    {
        $where    = array(
            'PartyID'   => $request->input('partyId'),
            'IPAddress' => $request->header('ip-address'),
            'SessionID' => $request->header('session-token'),
        );
        $selParty = \App\Models\Partyloginhistory::select('PartyLoginHistoryId')
                ->where($where)
                ->orderBy('PartyLoginHistoryId', 'desc')
                ->first();
        return ($selParty) ? $selParty->PartyLoginHistoryId : FALSE;
    }

    /*
     *  getUserSessionId - Get logged in user sessionId
     *  @param object $results
     *  @return Array
     */

    public static function checkUserCard($request)
    {
        $where    = array(
            'PartyID'            => $request->input('partyId'),
            'UserExternalCardId' => $request->input('savedcardId'),
            'ActiveIndicator'    => 'Y',
        );
        $selParty = \App\Models\Userexternalcard::select('UserExternalCardId')
                ->where($where)
                ->first();
        return ($selParty) ? TRUE : FALSE;
    }

    /*
     *  getTransctionId - Get unique transction Id
     *  @return integer
     */

    public static function getTransctionId()
    {
        $timestamp = \DateTime::createFromFormat('U.u', number_format(microtime(TRUE), 6, '.', ''));
        $date = $timestamp->format("dmy");
        $mic = number_format(microtime(true), 6, '', ''); 
        return $date . substr($mic, -4);
    }

    /*
     *  getPaymentProductEventInfo - Get getPayment Product Event Info
     *  @return Array
     */

    public static function getPaymentProductEventInfo($id)
    {
        $paymentProduct = \App\Models\Paymentproductevent::select('paymentsproduct.ProductName', 'paymentproductevent.PaymentProductEventID', 'paymentproductevent.EventCode', 'paymentproductevent.PaymentsProductID', 'paymentproductevent.FeeApplicable', 'paymentproductevent.EventSequenceNumber', 'paymentsproducteventdetail.FromAccount')
                        ->join('paymentsproduct', 'paymentsproduct.PaymentsProductID', '=', 'paymentproductevent.PaymentsProductID')
                        ->join('paymentsproducteventdetail', 'paymentsproducteventdetail.PaymentProductEventID', '=', 'paymentproductevent.PaymentProductEventID')
                        ->where('paymentproductevent.PaymentProductEventID', $id)
                        ->orWhere('paymentproductevent.EventCode', $id)->first();
        return $paymentProduct;
    }

    /*
     *  checkUserwalletTypeState - checks userwallet subscription
     *  @param integer $walletId
     *  @return object $walletType
     */

    public static function checkUserwalletTypeState($walletId)
    {
        $walletType = \App\Models\Walletplantype::select('WalletPlanTypeId', 'ActiveFlag')
                        ->where('WalletPlanTypeId', $walletId)
                        ->whereDate('EndDate', '>=', date('Y-m-d H:i:s'))->first();

        if ( ! $walletType )
        {
            $walletType = \App\Models\Walletplantype::select('WalletPlanTypeId', 'ActiveFlag')
                            ->where('WalletPlanTypeId', 1)->first();
        }

        return $walletType;
    }

    /*
     *  monthlyAggregate - check mounthly user transction amount aggregate
     *  @param integer $partyId
     *  @return object
     */

    public static function monthlyAggregate($partyId)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('Id', 'TagName', 'Key', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', 'MonthlyTransactionAmount')
                        ->where('Key', date('Y-m'))->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

    /*
     *  monthlyAggregate - check mounthly user transction amount aggregate
     *  @param integer $partyId
     *  @return object
     */

    public static function monthlyTransctionCountAggregate($partyId)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('Id', 'TagName', 'Key', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', 'MonthlyTransactionCount')
                        ->where('Key', date('Y-m'))->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

    /*
     *  monthlyAggregate - check mounthly user transction amount aggregate
     *  @param integer $partyId
     *  @return object
     */

    public static function monthlyAggregateTotal($partyId, $key)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('Id', 'TagName', 'Key', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', $key)
                        ->where('Key', date('Y-m'))->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

    /*
     *  monthlyAggregate - check mounthly user transction amount aggregate
     *  @param integer $partyId
     *  @return object
     */

    public static function monthlyAggregateCount($partyId, $key)
    {
        $transctionAggregate = \App\Models\Usertransactionstatsaggregate::select('Id', 'TagName', 'Key', 'Value')
                        ->where('UserWalletID', $partyId)
                        ->where('TagName', $key)
                        ->where('Key', date('Y-m'))->first();
        return ($transctionAggregate) ? $transctionAggregate : FALSE;
    }

    /*
     *  walletPlanConfigration - check walletplan based transction limits
     *  @param integer $walletPlanTypeId
     *  @return object
     */

    public static function walletPlanConfigration($walletPlanTypeId)
    {
        $monthlyTrnAmtLimit = \App\Models\Walletplanconfiguration::select('ConfValue')
                        ->where('WalletPlanTypeId', $walletPlanTypeId)
                        ->where('ConfKey', 'MONTHLY_TRANS_AMOUNT_lIMIT')->first();
        return ($monthlyTrnAmtLimit) ? $monthlyTrnAmtLimit : FALSE;
    }

    /*
     *  walletPlanConfigration - check walletplan based transction yearly limits
     *  @param integer $walletPlanTypeId
     *  @return object
     */

    public static function walletPlanConfigrationYearly($walletPlanTypeId)
    {
        $monthlyTrnAmtLimit = \App\Models\Walletplanconfiguration::select('ConfValue')
                        ->where('WalletPlanTypeId', $walletPlanTypeId)
                        ->where('ConfKey', 'YEARLY_TRANS_AMOUNT_lIMIT')->first();
        return ($monthlyTrnAmtLimit) ? $monthlyTrnAmtLimit : FALSE;
    }

    /*
     *  userTransctionLimitChecker - checks user wallet subscription.
     *  based on that will check transction limits of user.
     *  and check for current mounth his transction limits.
     *
     *  @param integer $partyId
     *  @param integer $walletPlanTypeId
     *  @param float $amount
     *  @param boolean $benificary
     *
     *  @return Array $response 
     */

    public static function userTransctionLimitChecker($partyId, $walletPlanTypeId, $amount, $benificary = '')
    {
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // user wallet subscription status
        $userWalletId = self::checkUserwalletTypeState($walletPlanTypeId);

        // get wallet plan configs
        $walletMonthlyLimit = self::walletPlanConfigration($userWalletId->WalletPlanTypeId);
        if ( $walletMonthlyLimit === FALSE )
        {
            $response['display_msg']['info'] = trans('messages.wrong');
            return $response;
        }

        //$partyMonthlyLimit = self::monthlyAggregate($partyId);
        //Debited from user wallet
        $partyMonthlyLimit = \App\Libraries\Statistics::getUserMonthlyDebitTransactionsTotal($partyId);
//        print_r($partyMonthlyLimit); exit;
        // if no recordfound for user for that month first transction for the month.
        if ( ($partyMonthlyLimit != FALSE) && (($partyMonthlyLimit->Value + $amount) > $walletMonthlyLimit->ConfValue) )
        {
            if ( $benificary )
            {
                $response['info'] = 'Beneficiary Limit Exceed.';
            }
            $response['error']    = FALSE;
            $response['response'] = array( 'WalletPlanTypeId' => $userWalletId->WalletPlanTypeId );
        }
        else
        {
            $response['error']    = FALSE;
            $response['response'] = array( 'WalletPlanTypeId' => $userWalletId->WalletPlanTypeId );
        }
        return $response;
    }

    public static function userTransctionCreditLimitChecker($partyId, $walletPlanTypeId, $amount)
    {
        $response = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        // user wallet subscription status
        $userWalletId = self::checkUserwalletTypeState($walletPlanTypeId);

        // get wallet plan configs
        $walletYearlyLimit = self::walletPlanConfigrationYearly($userWalletId->WalletPlanTypeId);
        if ( $walletYearlyLimit === FALSE )
        {
            $response['display_msg']['info'] = trans('messages.wrong');
            return $response;
        }

        //Credited to user wallet
        $partyMonthlyLimit = \App\Libraries\Statistics::getWalletYearlyBalanceTotal($partyId);
        // if no recordfound for user for that month first transction for the month.
        if ( ($partyMonthlyLimit != FALSE) && (($partyMonthlyLimit->Value + $amount) > $walletYearlyLimit->ConfValue) )
        {
            $response['display_msg']['info'] = trans('messages.montlytTansactionLimit');
            return $response;
        }
        $response['is_error'] = FALSE;
        $response['res_data'] = array( 'WalletPlanTypeId' => $userWalletId->WalletPlanTypeId );
        return $response;
    }

    public static function paymentEntity($entityId, $paymentsProductID, $walletPlanTypeId)
    {
        $selectArray = array(
            'ProductPlanChargeDetailsid',
            'ConfigParamNum'
        );

        $planConfig = \App\Models\Productplanconfig::select($selectArray)
                        ->where('PaymentEntityID', $entityId)
                        ->where('PaymentProductID', $paymentsProductID)
                        ->where('WalletPlanTypeId', $walletPlanTypeId)
                        ->where('FeeFreqCycle', 'D')
                        ->where('ActiveFlag', 'Y')
                        ->whereDate('ExpiryDate', '>=', date('Y-m-d H:i:s'))->first();
        return $planConfig;
    }

    /*
     *  addFeeForTransction - calculate fee structure
     *
     *  @param  char    $feeApplicable
     *  @param  integer $walletPlanTypeId
     *  @param  float   $amount
     *
     *  @return Array $feeArray
     */

    public static function addFeeForTransction($paymentProduct, $walletPlanTypeId, $amount, $businessModelid, $partyId)
    {
        $response = array( 'error' => TRUE, 'info' => array(), 'response' => array() );

        $transFixFee             = 0;
        $transPerFee             = 0;
        $transAmount             = $amount;
        $gst                     = 0;
        $transPerFeeAmount       = 0;
        $igst                    = 0;
        $feeAmount               = 0;
        $taxAmount               = 0;
        $transctionVendorDetails = \App\Models\Paymententity::select('PaymentEntityName')->where('PaymentEntityID', $businessModelid)->first();
        $transctionVendor        = $transctionVendorDetails->PaymentEntityName;
        // fee check & calculation
        if ( $paymentProduct->FeeApplicable == 'Y' )
        {
            $transctionVendor = \App\Models\Paymententity::select('PaymentEntityName')->where('PaymentEntityID', $businessModelid)->first();
            $transctionVendor = $transctionVendor->PaymentEntityName;

            $dateTimeOfTrans = date('Y-m-d H:i:s');
            $planConfig      = self::paymentEntity($businessModelid, $paymentProduct->PaymentsProductID, $walletPlanTypeId);

            //$queries = \DB::getQueryLog();
            // print_r($queries);  exit;
            // if no plan configured
            if ( ! $planConfig )
            {
                $feeArray = array(
                    'amount'           => $amount,
                    'taxAmount'        => 0,
                    'feeAmount'        => 0,
                    'transAmount'      => $amount,
                    'transctionVendor' => $transctionVendor
                );
                return $feeArray;
            }


            // get plan parameters
            $entityPrams = self::productPlanParams($planConfig->ConfigParamNum);

            $feeInfo = array();
            foreach ( $entityPrams as $key => $value )
            {
                $feeInfo[$value->ParameterTagName] = $value->ParameterTagValue;
            }

            // transction limits
            if ( ($feeInfo['MinTransAmt'] > $amount ) || ( $feeInfo['MaxTransAmt'] < $amount ) )
            {
                $output_arr['display_msg'] = array( 'amount' => 'Choosen Amount can not been procced.' );
                return $output_arr;
            }
            $freeTransCount = (array_key_exists('FreeTransLimit', $feeInfo)) ? $feeInfo['FreeTransLimit'] : 0;
            // ceck it is a free trasction or not
            $freetrans      = self::countOfProductTransction($partyId, $paymentProduct->PaymentsProductID);


            if ( $freeTransCount > $freetrans )
            {
                $feeArray = array(
                    'amount'           => $amount,
                    'taxAmount'        => 0,
                    'feeAmount'        => 0,
                    'transAmount'      => $amount,
                    'transctionVendor' => $transctionVendor
                );
                return $feeArray;
            }

            $transFixFee = (array_key_exists('TransFixFeeAmt', $feeInfo)) ? $feeInfo['TransFixFeeAmt'] : 0;
            $transPerFee = (array_key_exists('TransFeePerc', $feeInfo)) ? $feeInfo['TransFeePerc'] : 0;

            if ( $paymentProduct->EventSequenceNumber == 2 )
            {
                $transFixFee = (array_key_exists('RevFixFeeAmt', $feeInfo)) ? $feeInfo['RevFixFeeAmt'] : 0;
                $transPerFee = (array_key_exists('RevFeePerc', $feeInfo)) ? $feeInfo['RevFeePerc'] : 0;
            }

            // fee applicabilty type
            $feeMode   = (array_key_exists('FeeMode', $feeInfo)) ? $feeInfo['FeeMode'] : 'P';
            $igst      = 0;
            $feeAmount = 0;
            // fee is percentage
            if ( $feeMode == 'P' )
            {
                $transPerFeeAmount = number_format((($amount * $transPerFee) / 100), 2, '.', '');
                $transFixFee       = 0;
                $feeAmount         = $transPerFeeAmount + $transFixFee;
            }
            // fee is fixed
            if ( $feeMode == 'F' )
            {
                $transPerFeeAmount = 0;
                $transFixFee       = $transFixFee;
                $feeAmount         = $transPerFeeAmount + $transFixFee;
            }
            // fee has both percentage & fixed amounts
            if ( $feeMode == 'B' )
            {
                $transPerFeeAmount = number_format((($amount * $transPerFee) / 100), 2, '.', '');
                $transFixFee       = $transFixFee;
                $feeAmount         = $transPerFeeAmount + $transFixFee;
            }
            // need to take higher value of tax
            if ( $feeMode == 'H' )
            {
                $transPerFeeAmount = number_format((($amount * $transPerFee) / 100), 2, '.', '');
                $transFixFee       = $transFixFee;

                $feeAmount = ($transPerFeeAmount > $transFixFee) ? $transPerFeeAmount : $transFixFee;
            }
            // need to take lower value of tax
            if ( $feeMode == 'L' )
            {
                $transPerFeeAmount = number_format((($amount * $transPerFee) / 100), 2, '.', '');
                $transFixFee       = $transFixFee;

                $feeAmount = ($transPerFeeAmount < $transFixFee) ? $transPerFeeAmount : $transFixFee;
            }
            $gst  = (array_key_exists('TaxIGSTPerc', $feeInfo)) ? $feeInfo['TaxIGSTPerc'] : 0;
            $sgst = (array_key_exists('TaxGSTPerc', $feeInfo)) ? $feeInfo['TaxGSTPerc'] : 0;
            $cgst = (array_key_exists('TaxSGSTPerc', $feeInfo)) ? $feeInfo['TaxSGSTPerc'] : 0;
            $igst = (( float ) $gst != 0) ? $gst : ($cgst + $sgst);

            // tax always on fee collected for amount transfered
            $taxAmount   = number_format((($feeAmount * $igst) / 100), 2, '.', '');
            $transAmount = $amount + $feeAmount + $taxAmount;
        }

        $feeArray = array(
            'amount'             => $amount,
            'taxAmount'          => $taxAmount,
            'feeAmount'          => $feeAmount,
            'transAmount'        => $transAmount,
            'transctionVendor'   => $transctionVendor,
            'CGST'               => $gst,
            'SGST'               => $gst,
            'IGST'               => $igst,
            'transactionFeePerc' => $transPerFee . '%',
            'transFixFeeAmt'     => 'Rs ' . number_format(($transFixFee), 2, '.', ''),
        );

        return $feeArray;
    }

    public static function productPlanParams($ConfigParamNum)
    {
        $dateTimeOfTrans             = date('Y-m-d H:i:s');
        // transction line number
        $productPlanConfigParameters = \App\Models\Productplanconfigparameter::select('ParameterTagName', 'ParameterTagValue')
                ->where('ConfigParamNum', $ConfigParamNum)
                ->where('ActiveFlag', 'Y')
                ->whereDate('EffectiveDate', '<=', $dateTimeOfTrans)
                ->whereDate('ExpiryDate', '>=', $dateTimeOfTrans)
                ->get();
        return $productPlanConfigParameters;
    }

    public static function countOfProductTransction($partyId, $productId)
    {
        // transction line number
        $transactionProcutCount = \App\Models\Transactiondetail::select('TransactionDetailID')
                        ->where(\DB::raw("cast(TransactionDate as DATE)"), date('Y-m-d'))
                        ->where('PaymentsProductID', $productId)
                        ->where('UserWalletId', $partyId)->count();
        return $transactionProcutCount;
    }

    public static function getFromToAccountTypeInvolved($request, $prodctEventId = '', $partyInfo, $banificiaryInfo = '')
    {

        switch ( $prodctEventId )
        {

            case 1:
                $FromAccountType = 'ExternalCardID';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Deposit';
                $transMode       = 'Credit';
                $FromAccountId   = $banificiaryInfo['PreffredPaymentMethodID'];
                $FromAccountName = $banificiaryInfo['CardHolderName'];
                $ToAccountId     = $partyInfo->UserWalletID;
                $ToWalletId      = $partyInfo->WalletAccountId;
                break;
            case 4:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'ExternalBeneficiaryId';
                $trnsTypeCode    = 'Transfer';
                $transMode       = 'Debit';
                $FromAccountId   = $partyInfo->WalletAccountId;
                $FromAccountName = $partyInfo->FirstName;
                //$ToAccountId     = $banificiaryInfo['PreffredPaymentMethodID'];
                $ToAccountId     = $banificiaryInfo['BeneficiaryID'];
                $ToWalletId      = 1;
                break;

            case 2:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Transfer';
                $transMode       = 'Debit';
                $FromAccountId   = $partyInfo->WalletAccountId;
                $FromAccountName = $partyInfo->FirstName;
                //$ToAccountId     = $banificiaryInfo['PreffredPaymentMethodID'];
                $ToAccountId     = $banificiaryInfo['BeneficiaryID'];
                $ToWalletId      = 1;
                break;

            case 3:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Transfer';
                $transMode       = 'Debit';
                $FromAccountId   = $partyInfo->WalletAccountId;
                $FromAccountName = $partyInfo->FirstName;
                //$ToAccountId     = $banificiaryInfo['PreffredPaymentMethodID'];
                $ToAccountId     = $banificiaryInfo['BeneficiaryID'];
                $ToWalletId      = 1;
                break;

            case 6:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Billpay';
                $transMode       = 'Debit';
                $FromAccountId   = $partyInfo->WalletAccountId;
                $FromAccountName = $partyInfo->FirstName;
                $ToAccountId     = $banificiaryInfo->UserWalletID;
                $ToWalletId      = $banificiaryInfo->WalletAccountId;
                break;

            case 8:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Transfer';
                $transMode       = 'Debit';
                $FromAccountId   = $banificiaryInfo->WalletAccountId;
                $FromAccountName = 'CashBack Admin';
                $ToAccountId     = $partyInfo->WalletAccountId;
                $ToWalletId      = $partyInfo->UserWalletID;
                break;

            default:
                $FromAccountType = 'WalletAccount';
                $ToAccountType   = 'WalletAccount';
                $trnsTypeCode    = 'Transfer';
                $transMode       = 'Debit';
                $FromAccountId   = $partyInfo->WalletAccountId;
                $FromAccountName = $partyInfo->FirstName;
                $ToAccountId     = $banificiaryInfo->UserWalletID;
                $ToWalletId      = $banificiaryInfo->WalletAccountId;
                break;
        }
        $accountTypes = array(
            'FromAccountType' => $FromAccountType,
            'ToAccountType'   => $ToAccountType,
            'transTypeCode'   => $trnsTypeCode,
            'transMode'       => $transMode,
            'FromAccountId'   => $FromAccountId,
            'FromAccountName' => $FromAccountName,
            'ToAccountId'     => $ToAccountId,
            'ToWalletId'      => $ToWalletId
        );
        return $accountTypes;
    }

    public static function transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, $banificiaryInfo, $transStaus, $sessionId, $transType = '')
    {

        $response        = array( 'error' => TRUE, 'info' => array(), 'response' => array() );
        $transDate       = date('Y-m-d');
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        $transctionID    = self::getTransctionId();
        $partyId         = $partyInfo->UserWalletID;
        $transStausMain  = config('vwallet.transactionStatus.pending');
        $paymentEntity   = array( 5, 6, 7 );
        $AdminInfo       = array();
        $benfInfo        = $banificiaryInfo;
        if ( $request->input('transType') == 'WB' )
        {
            $useName = "";
            $request->merge(array( 'beneficiaryId' => $banificiaryInfo->BeneficiaryID ));
        }
        else
        {
            $useName = ( ! is_bool($banificiaryInfo) && is_object($banificiaryInfo)) ? $banificiaryInfo->FirstName . ' ' . $banificiaryInfo->LastName : '';
        }


        if ( in_array($entityId, $paymentEntity) )
        {
            $banificiaryInfo = self::topupCardInfo($request);
            $useName         = $banificiaryInfo['CardHolderName'];
            $AdminInfo       = self::getAdminWalletInfo(1);
        }
        // pay W2B

        $payWBEntity = array( 2, 3, 4 );
        if ( in_array($entityId, $payWBEntity) )
        {
            // $banificiaryInfo = self::getAdminWalletInfo(1);
            $walletInfo = self::walletToBankPrefferance($request, $partyInfo, $banificiaryInfo, $fee);
            $useName    = $walletInfo['CardHolderName'];
        }

        // code for recharge
        if ( $request->input('transType') == '' )
        {
            $payWBEntity = array( 1 );
            if ( in_array($entityId, $payWBEntity) )
            {
                $adminProdId     = self::getProductId($paymentProduct['ProductName']);
                $transStausMain  = $transStaus;
                $banificiaryInfo = self::getAdminWalletInfo($adminProdId['UserWalletID']);
            }
        }

        $benificaryUserWalletId = ( $AdminInfo ) ? $AdminInfo->UserWalletID : $banificiaryInfo->UserWalletID;
        $accountType            = self::getFromToAccountTypeInvolved($request, $paymentProduct->PaymentsProductID, $partyInfo, $banificiaryInfo);
        $transAmount            = $fee['transAmount'];
        $discountAmount         = 0.00;
        $promoCode              = NULL;
        $offerPamentInfo        = $request->input('offerPamentInfo');
        if ( ! empty($offerPamentInfo) )
        {
            $promoCode = $offerPamentInfo['promoCode'];
            if ( $offerPamentInfo['event'] == 'DISCOUNT' )
            {
                $discountAmount = $offerPamentInfo['offerAmount'];
                $transAmount    -= $discountAmount;
            }
        }
        $superCashPaymentInfo = $request->input('superCashPamentInfo');
        $superCashAmount      = 0;
        if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 )
        {
            $superCashAmount = $superCashPaymentInfo['offerAmount'];
            $transAmount     -= $superCashAmount;
        }
        $transDetails        = array(
            'TransactionOrderID'       => $transctionID,
            'PaymentsProductID'        => $paymentProduct->PaymentsProductID,
            'UserWalletId'             => ($transType == 'CW') ? $accountType['ToWalletId'] : $partyId, // Wallet user id of from wallet account
            'FromAccountId'            => ($transType == 'CW') ? $accountType['ToAccountId'] : $accountType['FromAccountId'],
            'FromAccountType'          => $accountType['FromAccountType'],
            'FromAccountName'          => ($transType == 'CW') ? 'Admin' : $accountType['FromAccountName'],
            'ToAccountType'            => $accountType['ToAccountType'],
            'ToAccountId'              => ($transType == 'CW') ? $accountType['FromAccountId'] : $accountType['ToWalletId'],
            'ToWalletId'               => ($transType == 'CW') ? $partyId : $accountType['ToAccountId'],
            'Amount'                   => $amount,
            'TransactionTypeCode'      => $accountType['transTypeCode'],
            'TransactionDate'          => $dateTimeOfTrans,
            'TransactionStatus'        => $transStausMain,
            'TransactionMode'          => ($transType == 'CW') ? 'Credit' : $accountType['transMode'],
            'UserSessionUsageDetailID' => $sessionId,
            'TransactionType'          => $paymentProduct->ProductName,
            'CreatedDatetime'          => $dateTimeOfTrans,
            'CreatedBy'                => 'User',
            'TransactionVendor'        => $fee['transctionVendor'],
            'FeeAmount'                => $fee['feeAmount'],
            'TaxAmount'                => $fee['taxAmount'],
            'PromoCode'                => $promoCode,
            'DiscountAmount'           => $discountAmount,
            'WalletAmount'             => $transAmount,
            'CashbackAmount'           => $superCashAmount,
            'UpdatedBy'                => 'User',
        );
        // return $transDetails;
        $transactionDetailID = \App\Models\Transactiondetail::insertGetId($transDetails);

        // transction Event Log details prepare here
        $transctionLog = self::_transctionEventLog($transactionDetailID, $paymentProduct, $transDate, $useName, $dateTimeOfTrans, $amount);
        // preparing Daily trnsction Log details
        switch ( $paymentProduct->PaymentsProductID )
        {
            case 1:
                $dailyTransctionLog = self::_dailyTransctionEventDetailsBtoW($transactionDetailID, $paymentProduct, $partyInfo, $AdminInfo, $partyId, $amount, $fee, $dateTimeOfTrans);
                break;
            case 4:
                $dailyTransctionLog = self::_dailyTransctionEventDetailsWtoB($transactionDetailID, $paymentProduct, $partyInfo, $AdminInfo, $partyId, $amount, $fee, $dateTimeOfTrans);
                break;
            default:
                $dailyTransctionLog = self::_dailyTransctionEventDetailsWtoW($transactionDetailID, $paymentProduct, $partyInfo, $banificiaryInfo, $partyId, $transAmount, $fee, $dateTimeOfTrans);
                break;
        }
        //If offer type is discount, insert only event logs
        if ( ! empty($offerPamentInfo) && $offerPamentInfo['event'] == 'DISCOUNT' )
        {
            $dailyTransctionLog = \App\Libraries\TransctionHelper::_dailyTransctionEventDetailsReversal($transactionDetailID, $offerPamentInfo, $partyInfo, $AdminInfo, $dateTimeOfTrans, $dailyTransctionLog);
        }

        //If offer type is super cash amount, inserting event logs
        if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 )
        {
            $dailyTransctionLog = \App\Libraries\TransctionHelper::_dailyTransctionEventDetailsSuperCash($transactionDetailID, $superCashPaymentInfo, $partyInfo, $dateTimeOfTrans, $dailyTransctionLog);
        }

        // preparing notification log details
        $notificationLog = self::_notificationLog($transactionDetailID, $paymentProduct, $partyId, $benificaryUserWalletId, $amount, $dateTimeOfTrans);

        // insert data into respective tables
        \App\Models\Transactioneventlog::insert($transctionLog);
        \App\Models\Dailytransactionlog::insert($dailyTransctionLog);
        \App\Models\Notificationlog::insert($notificationLog);


        // temp transction details need to remove later
//        $request->request->add([ 'transactionId' => $transactionDetailID ]);
//        $getTransactionList = app('App\Http\Controllers\Transactions\TransactionsController')->getTransactionsList($request);
//        $transactionDetails = $getTransactionList->original['res_data']['0'];
        // if transction Failue due to benificiary transction limit exceed

        $transDetails['TransactionDetailID'] = $transactionDetailID;
        if ( ! $transStaus )
        {
            if ( $paymentProduct->PaymentProductEventID == 7 )
            {
                $userDetails = self::_userValidate($banificiaryInfo->UserWalletID);
            }
            else
            {
                $userDetails = self::_userValidate($partyId);
            }
            $transDetails['AvailableBalance']  = $userDetails->AvailableBalance;
            $transDetails['CurrentAmount']     = $userDetails->CurrentAmount;
            $transDetails['NonWithdrawAmount'] = $userDetails->NonWithdrawAmount;
            $transDetails['HoldAmount']        = $userDetails->HoldAmount;
            $response['response']              = array( 'TransactionDetails' => $transDetails );
            $response['error']                 = FALSE;
            $response['info']                  = trans('messages.transFailedDueToExcess', [ 'name' => $paymentProduct->ProductName, 'status' => trans('messages.attributes.failed') ]);
            return $response;
        }
        // check if the product is not W2W. Hold the transction till PG respond back.
        $escapePG = array( 1, 4, 6 );
        if ( ($request->input('transType') == 'WB') || ($request->input('transType') == 'WW') )
        {
            $tagName = 'WalletToBank';
            $type    = 'bank';
            if ( $request->input('transType') == 'WW' )
            {
                $tagName = 'WalletToWallet';
                $type    = 'wallet';
            }
            $remarks        = $request->input('remarks');
            $params         = array(
                'mobileNumber'  => $partyInfo->MobileNumber,
                'amount'        => $transAmount,
                'beneficiaryId' => $benfInfo->yesBankBeneficiaryId,
                'tagName'       => $tagName,
                'partnerRefNo'  => '',
                'remarks'       => $remarks,
                'feeRate'       => '',
                'type'          => $type
            );
            $fundTransYBRes = \App\Libraries\Wallet\YesBankCheckUserBalance::fundTransferBetweenAccounts($params);

            if ( $fundTransYBRes->status_code != 'SUCCESS' )
            {
                $transctionStats = array(
                    'TransactionStatus' => 'Failed',
                    'UpdatedBy'         => 'Admin',
                    'UpdatedDatetime'   => date('Y-m-d H:i:s')
                );
                \App\Models\Transactiondetail::where('TransactionDetailID', '=', $transactionDetailID)
                        ->update($transctionStats);

                $transDetails['AvailableBalance']  = $partyInfo->AvailableBalance;
                $transDetails['CurrentAmount']     = $partyInfo->CurrentAmount;
                $transDetails['NonWithdrawAmount'] = $partyInfo->NonWithdrawAmount;
                $transDetails['HoldAmount']        = $partyInfo->HoldAmount;
                $transDetails['TransactionStatus'] = 'Failed';
                $response['response']              = array( 'TransactionDetails' => $transDetails );
                $response['error']                 = TRUE;
                $response['info']                  = trans('messages.transFailed');
                return $response;
            }

            $latestTransDetails = self::transctionbetweenWallets($transactionDetailID, $partyId, $transAmount, $partyInfo, $banificiaryInfo, $paymentProduct, $fee, $transDate, $dateTimeOfTrans, $request);

            return $latestTransDetails;
        }
        else if ( ! in_array($paymentProduct->PaymentsProductID, $escapePG) )
        {
            // if service is W2W iniciate second level of transction. credit amount to benifisary

            if ( $transType == 'CW' )
            {
                $secoundLevel = self::transctionbetweenWallets($transactionDetailID, $partyId, $transAmount, $banificiaryInfo, $partyInfo, $paymentProduct, $fee, $transDate, $dateTimeOfTrans, $request);
            }
            else
            {
                $secoundLevel = self::transctionbetweenWallets($transactionDetailID, $partyId, $transAmount, $partyInfo, $banificiaryInfo, $paymentProduct, $fee, $transDate, $dateTimeOfTrans, $request);
            }
            if ( $secoundLevel['error'] === TRUE )
            {
                $transDetails['AvailableBalance']  = $partyInfo->AvailableBalance;
                $transDetails['CurrentAmount']     = $partyInfo->CurrentAmount;
                $transDetails['NonWithdrawAmount'] = $partyInfo->NonWithdrawAmount;
                $transDetails['HoldAmount']        = $partyInfo->HoldAmount;
                $transDetails['TransactionStatus'] = 'Failed';
                $response['response']              = array( 'TransactionDetails' => $transDetails );
                $response['error']                 = TRUE;
                $response['info']                  = trans('messages.transFailed');
                return $response;
            }
            //If offer type is cash back
            if ( ! empty($offerPamentInfo) && $transStausMain == 'Success' )
            {
                //if($offerPamentInfo['event'] == 'CASHBACK') {
                //$insertCashback = self::cashBack($request, $offerPamentInfo);
                //}
                $insertUserOfferStats = self::_userOfferStatsInsert($partyId, $offerPamentInfo, 'Used');
            }
            //If supercash applied update amounts
            if ( isset($superCashPaymentInfo['offerAmount']) && $superCashPaymentInfo['offerAmount'] > 0 && $transStausMain == 'Success' )
            {
                //if(!empty($superCashPaymentInfo) && $transStausMain == 'Success') {
                $trackRequest                        = [];
                $trackRequest['partyId']             = $partyId;
                $trackRequest['amount']              = $superCashPaymentInfo['offerAmount'];
                $trackRequest['transactionDetailId'] = $transactionDetailID;
                \App\Libraries\OfferBussness::updateUsedAmount($trackRequest);
            }
            if ( $paymentProduct->PaymentProductEventID == 7 )
            {
                $userDetails = self::_userValidate($banificiaryInfo->UserWalletID);
            }
            else
            {
                $userDetails = self::_userValidate($partyId);
            }
            $transDetails['AvailableBalance']  = $userDetails->AvailableBalance;
            $transDetails['CurrentAmount']     = $userDetails->CurrentAmount;
            $transDetails['NonWithdrawAmount'] = $userDetails->NonWithdrawAmount;
            $transDetails['HoldAmount']        = $userDetails->HoldAmount;

            $response['response'] = array( 'TransactionDetails' => $transDetails );
            $response['error']    = FALSE;
            $response['info']     = trans('messages.transSuccess', [ 'module' => $paymentProduct->ProductName, 'amount' => $amount, 'username' => $banificiaryInfo->FirstName . ' ' . $banificiaryInfo->LastName ]);
            return $response;
        }
        if ( $paymentProduct->PaymentProductEventID == 7 )
        {
            $userDetails = self::_userValidate($banificiaryInfo->UserWalletID);
        }
        else
        {
            $userDetails = self::_userValidate($partyId);
        }

        $transDetails['AvailableBalance']  = $userDetails->AvailableBalance;
        $transDetails['CurrentAmount']     = $userDetails->CurrentAmount;
        $transDetails['NonWithdrawAmount'] = $userDetails->NonWithdrawAmount;
        $transDetails['HoldAmount']        = $userDetails->HoldAmount;
        $response['response']              = array( 'TransactionDetails' => $transDetails );
        $response['error']                 = FALSE;
        $response['info']                  = $paymentProduct->ProductName . ' Transction ' . trans('messages.attributes.iniciate');
        return $response;
    }

    public static function _transctionEventLog($transactionDetailID, $paymentProduct, $transDate, $useName, $dateTimeOfTrans, $amount, $partyUPI = '')
    {

        $transctionLog = array(
            'TransactionDetailID' => $transactionDetailID,
            'PaymentsProductID'   => $paymentProduct->PaymentsProductID,
            'EventDate'           => $transDate,
            'Comments'            => $paymentProduct->ProductName . ' transfer to ' . $useName,
            'EventCode'           => $paymentProduct->EventCode,
            'CreatedBy'           => 'User',
            'CreatedDateTime'     => $dateTimeOfTrans
        );

        if ( $partyUPI == '' )
        {
            $topupCardnumber = empty(self::getTopupCardNumber($transactionDetailID)[0]) ? '' : ' (' . self::getTopupCardNumber($transactionDetailID)[0] . ')';
            if ( $paymentProduct->PaymentsProductID == 1 )
            {
                $transctionLog['Comments'] = $paymentProduct->ProductName . ' of Rs.' . (number_format(( float ) $amount, 2, '.', '')) . ' from ' . self::getTopupCardNumber($transactionDetailID)[1] . $topupCardnumber;
            }
            elseif ( $paymentProduct->PaymentsProductID == 101 )
            {
                $transctionLog['Comments'] = 'Paid for Bus Ticket';
            }
        }
        else
        {
            $transctionLog['Comments'] = $paymentProduct->ProductName . ' of Rs.' . (number_format(( float ) $amount, 2, '.', '')) . ' from ' . $partyUPI;
        }
        return $transctionLog;
    }

    public static function _dailyTransctionEventDetailsWtoW($transactionDetailID, $paymentProduct, $partyInfo, $banificiaryInfo, $partyId, $amount, $fee, $dateTimeOfTrans)
    {
        $dailyTransctionLog       = array(
            array(
                'TransactionLineNumber'      => 1,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 2,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $banificiaryInfo->WalletAccountId,
                'PartyId'                    => $banificiaryInfo->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $banificiaryInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        $dailyTransctionLogFeeTax = array();
        if ( ( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ) )
        {
            $dailyTransctionLogFeeTax = self::feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans);
        }
        $dailyLog = array_merge($dailyTransctionLog, $dailyTransctionLogFeeTax);

        return $dailyLog;
    }

    public static function _dailyTransctionEventDetailsWtoB($transactionDetailID, $paymentProduct, $partyInfo, $banificiaryInfo, $partyId, $amount, $fee, $dateTimeOfTrans)
    {
        $benWalletAccId           = isset($banificiaryInfo->WalletAccountId) ? $banificiaryInfo->WalletAccountId : 1;
        $benWalletId              = isset($banificiaryInfo->BeneficiaryWalletID) ? $banificiaryInfo->BeneficiaryWalletID : 1;
        $benCurr                  = isset($banificiaryInfo->PreferredPrimaryCurrency) ? $banificiaryInfo->PreferredPrimaryCurrency : "INR";
        $dailyTransctionLog       = array(
            array(
                'TransactionLineNumber'      => 1,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 2,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $benWalletAccId,
                'PartyId'                    => $benWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $benCurr,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        $dailyTransctionLogFeeTax = array();
        if ( ( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ) )
        {
            $dailyTransctionLogFeeTax = self::feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans);
        }
        $dailyLog = array_merge($dailyTransctionLog, $dailyTransctionLogFeeTax);

        return $dailyLog;
    }

    public static function _dailyTransctionEventDetailsBtoW($transactionDetailID, $paymentProduct, $partyInfo, $banificiaryInfo, $partyId, $amount, $fee, $dateTimeOfTrans)
    {
        $dailyTransctionLog       = array(
            array(
                'TransactionLineNumber'      => 1,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 2,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $banificiaryInfo->WalletAccountId,
                'PartyId'                    => $banificiaryInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $banificiaryInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $amount,
                'LocalCcyAmount'             => $amount,
                'AcctCcyAmount'              => $amount,
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        $dailyTransctionLogFeeTax = array();
        if ( ( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ) )
        {
            $dailyTransctionLogFeeTax = self::feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans);
        }
        $dailyLog = array_merge($dailyTransctionLog, $dailyTransctionLogFeeTax);

        return $dailyLog;
    }

    public static function _dailyTransctionEventDetailsReversal($transactionDetailID, $offerPamentInfo, $partyInfo, $adminInfo, $dateTimeOfTrans, $dailyTransctionLog = array())
    {
        //$adminInfo                                         = \App\Libraries\Helpers::GetAdm$offerPamentInfoinData();
        $adminWalletId = $offerPamentInfo['offerAdminWalletAccountId'];
        $adminInfo     = \App\Libraries\Helpers::GetWalletUserData($adminWalletId);
        $dailyLog      = array(
            array(
                'TransactionLineNumber'      => 7,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $offerPamentInfo['event'],
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $offerPamentInfo['offerAmount'],
                'LocalCcyAmount'             => $offerPamentInfo['offerAmount'],
                'AcctCcyAmount'              => $offerPamentInfo['offerAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $offerPamentInfo['PaymentsProductID'],
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 8,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $offerPamentInfo['event'],
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $adminInfo->WalletAccountId,
                'PartyId'                    => $adminInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $adminInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $offerPamentInfo['offerAmount'],
                'LocalCcyAmount'             => $offerPamentInfo['offerAmount'],
                'AcctCcyAmount'              => $offerPamentInfo['offerAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $offerPamentInfo['PaymentsProductID'],
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TRAN_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        if ( ! empty($dailyTransctionLog) )
        {
            $dailyLog = array_merge($dailyTransctionLog, $dailyLog);
        }

        return $dailyLog;
    }

    public static function _dailyTransctionEventDetailsSuperCash($transactionDetailID, $offerPamentInfo, $partyInfo, $dateTimeOfTrans, $dailyTransctionLog = array())
    {
        //$adminInfo                                         = \App\Libraries\Helpers::GetAdminData();
        $adminWalletId = $offerPamentInfo['offerAdminWalletAccountId'];
        $adminInfo     = \App\Libraries\Helpers::GetWalletUserData($adminWalletId);
        $dailyLog      = array(
            array(
                'TransactionLineNumber'      => 9,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $offerPamentInfo['event'],
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $offerPamentInfo['offerAmount'],
                'LocalCcyAmount'             => $offerPamentInfo['offerAmount'],
                'AcctCcyAmount'              => $offerPamentInfo['offerAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $offerPamentInfo['PaymentsProductID'],
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => $offerPamentInfo['amountTag'],
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 10,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $offerPamentInfo['event'],
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $adminInfo->WalletAccountId,
                'PartyId'                    => $adminInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $adminInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $offerPamentInfo['offerAmount'],
                'LocalCcyAmount'             => $offerPamentInfo['offerAmount'],
                'AcctCcyAmount'              => $offerPamentInfo['offerAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $offerPamentInfo['PaymentsProductID'],
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => $offerPamentInfo['amountTag'],
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            )
        );
        if ( ! empty($dailyTransctionLog) )
        {
            $dailyLog = array_merge($dailyTransctionLog, $dailyLog);
        }

        return $dailyLog;
    }

    public static function feeDailyLog($transactionDetailID, $paymentProduct, $partyInfo, $fee, $dateTimeOfTrans)
    {
        $feeWalletAcc             = self::getAdminWalletInfo(2);
        $taxWalletAcc             = self::getAdminWalletInfo(3);
        $dailyTransctionLogFeeTax = array(
            array(
                'TransactionLineNumber'               => 3,
                'TransactionDetailID'                 => $transactionDetailID,
                'EventCode'                           => $paymentProduct->EventCode,
                'TransactionDate'                     => $dateTimeOfTrans,
                'BankReconciliatiofeeDailyLognStatus' => 'N',
                'TransactionWalletAccountId'          => $feeWalletAcc->WalletAccountId,
                'PartyId'                             => $feeWalletAcc->UserWalletID,
                'TransactionCurrency'                 => 'INR',
                'AccountCurrency'                     => $feeWalletAcc->PreferredPrimaryCurrency,
                'LocalCurrency'                       => 'INR',
                'ExchangeRate'                        => '0.00',
                'TransactionCcyAmount'                => $fee['feeAmount'],
                'LocalCcyAmount'                      => $fee['feeAmount'],
                'AcctCcyAmount'                       => $fee['feeAmount'],
                'IsInternalJournalEntry'              => 'N',
                'PaymentsProductID'                   => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'                       => 'C',
                'AmountTag'                           => 'FEE_AMT',
                'CreatedBy'                           => 'User',
                'CreatedDatetime'                     => $dateTimeOfTrans,
                'UpdatedBy'                           => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 4,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['feeAmount'],
                'LocalCcyAmount'             => $fee['feeAmount'],
                'AcctCcyAmount'              => $fee['feeAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'FEE_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 5,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $taxWalletAcc->WalletAccountId,
                'PartyId'                    => $taxWalletAcc->UserWalletID,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $taxWalletAcc->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['taxAmount'],
                'LocalCcyAmount'             => $fee['taxAmount'],
                'AcctCcyAmount'              => $fee['taxAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'C',
                'AmountTag'                  => 'TAX_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
            array(
                'TransactionLineNumber'      => 6,
                'TransactionDetailID'        => $transactionDetailID,
                'EventCode'                  => $paymentProduct->EventCode,
                'TransactionDate'            => $dateTimeOfTrans,
                'BankReconciliationStatus'   => 'N',
                'TransactionWalletAccountId' => $partyInfo->WalletAccountId,
                'PartyId'                    => $partyInfo->UserWalletId,
                'TransactionCurrency'        => 'INR',
                'AccountCurrency'            => $partyInfo->PreferredPrimaryCurrency,
                'LocalCurrency'              => 'INR',
                'ExchangeRate'               => '0.00',
                'TransactionCcyAmount'       => $fee['taxAmount'],
                'LocalCcyAmount'             => $fee['taxAmount'],
                'AcctCcyAmount'              => $fee['taxAmount'],
                'IsInternalJournalEntry'     => 'N',
                'PaymentsProductID'          => $paymentProduct->PaymentsProductID,
                'CrDrIndicator'              => 'D',
                'AmountTag'                  => 'TAX_AMT',
                'CreatedBy'                  => 'User',
                'CreatedDatetime'            => $dateTimeOfTrans,
                'UpdatedBy'                  => 'Admin'
            ),
        );
        return $dailyTransctionLogFeeTax;
    }

    public static function _notificationLog($transactionDetailID, $paymentProduct, $partyId, $banificiaryId, $amount, $dateTimeOfTrans)
    {
        $notificationLog = array();
        if ( $partyId )
        {
            $notificationLog[] = array(
                'PartyID'               => $partyId,
                'LinkedTransactionID'   => $transactionDetailID,
                'NotificationType'      => 'Alert',
                'SentDateTime'          => $dateTimeOfTrans,
                'ReceipentAcknowledged' => 'Y',
                // 'Message'               => $paymentProduct->ProductName . 'Transaction Success. INR ' . $amount . ' Sent Successfully',
                'MessageStatus'         => 'UnRead',
                'CreatedBy'             => 'User',
                'CreatedDateTime'       => $dateTimeOfTrans,
                'ReceipentReceivedDate' => $dateTimeOfTrans
            );
        }

        if ( $banificiaryId )
        {
            $notificationLog[] = array(
                'PartyID'               => $banificiaryId,
                'LinkedTransactionID'   => $transactionDetailID,
                'NotificationType'      => 'Alert',
                'SentDateTime'          => $dateTimeOfTrans,
                'ReceipentAcknowledged' => 'Y',
                // 'Message'               => $paymentProduct->ProductName . 'Transaction Success. INR ' . $amount . ' Recived Successfully',
                'MessageStatus'         => 'UnRead',
                'CreatedBy'             => 'User',
                'CreatedDateTime'       => $dateTimeOfTrans,
                'ReceipentReceivedDate' => $dateTimeOfTrans
            );
        }
        return $notificationLog;
    }

    // need to modify the scound level with queries
    /**
     * 
     * @param type $transactionDetailID
     * @param type $partyId
     * @param type $amount
     * @param type $partyInfo
     * @param type $banificiaryInfo
     * @param type $paymentProduct
     * @param type $fee
     * @param type $transDate
     * @param type $dateTimeOfTrans
     * @param type $request
     * @return string
     */
    public static function transctionbetweenWallets($transactionDetailID, $partyId, $amount, $partyInfo, $banificiaryInfo, $paymentProduct, $fee, $transDate, $dateTimeOfTrans, $request)
    {

        $transctionStats = array(
            'TransactionStatus' => 'Success',
            'UpdatedBy'         => 'Admin',
            'UpdatedDatetime'   => date('Y-m-d H:i:s')
        );

        // Transction Begin
        \DB::beginTransaction();
        try
        {
            \App\Models\Transactiondetail::where('TransactionDetailID', $transactionDetailID)->update($transctionStats);

            $partyUpdatedCurrentBalance   = number_format(($partyInfo->CurrentAmount - $amount), 2, '.', '');
            $partyUpdatedAvailableBalance = number_format(($partyInfo->AvailableBalance - $amount), 2, '.', '');

            // update Party balance
            $updateUserBal                   = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);
            $updateUserBal->AvailableBalance -= $amount;
            $updateUserBal->CurrentAmount    -= $amount;
            $updateUserBal->save();

            // update Benificiary Balance
            $updateBenficiaryBal                   = \App\Models\Walletaccount::find($banificiaryInfo->WalletAccountId);
            $updateBenficiaryBal->AvailableBalance += $amount;
            $updateBenficiaryBal->CurrentAmount    += $amount;
            $updateBenficiaryBal->save();

            if ( ( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ) )
            {
                $feeWalletAcc                   = self::getAdminWalletInfo(2);
                $taxWalletAcc                   = self::getAdminWalletInfo(3);
                // update Fee wallet balance
                $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
                $updateFeeBal->AvailableBalance += $fee['feeAmount'];
                $updateFeeBal->CurrentAmount    += $fee['feeAmount'];
                $updateFeeBal->save();

                // update Tax wallet Balance
                $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
                $updateTaxBal->AvailableBalance += $fee['taxAmount'];
                $updateTaxBal->CurrentAmount    += $fee['taxAmount'];
                $updateTaxBal->save();
            }
            //current amount update in aggregate table
            \App\Libraries\Statistics::monthlyDebitTransactionsUpdate($partyId, $amount);
            \App\Libraries\Statistics::monthlyCreditTransactionsUpdate($banificiaryInfo->UserWalletID, $amount);

            \DB::commit();
            // all good
        }
        catch ( \Exception $e )
        {
            \Log::error('TransctionId: ' . $transactionDetailID . ' | ' . $e);
            \DB::rollback();
            // something went wrong
            $response['error'] = TRUE;
            $response['info']  = trans('messages.wrong');
            return $response;
        }
        //get transaction details
        $request->request->add([ 'transactionId' => $transactionDetailID ]);

        $transactionDetails             = \App\Models\Transactiondetail::where('TransactionDetailID', $transactionDetailID)->first();
        $getCommentsTxnEventLog         = \App\Models\Transactioneventlog::where('TransactionDetailID', $transactionDetailID)->select('Comments')->first();
        $transactionDetails['Comments'] = $getCommentsTxnEventLog->Comments;
        $response['response']           = array( 'AvailableBalance' => $partyUpdatedAvailableBalance, 'CurrentAmount' => $partyUpdatedCurrentBalance, 'NonWithdrawAmount' => $partyInfo->NonWithdrawAmount, 'HoldAmount' => $partyInfo->HoldAmount, 'TransactionDetails' => $transactionDetails );
        $response['error']              = FALSE;
        $response['info']               = 'Transction Successfully Completed.';
        return $response;
    }

    public static function getAdminWalletInfo($userId)
    {
        $userExsist = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.PreferredPrimaryCurrency', 'walletaccount.WalletAccountId', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $userId)
                ->first();
        return( ! $userExsist ) ? FALSE : $userExsist;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    public static function _userIdValidate($partyId, $idType = 'partyId')
    {
        $whereConditions = array(
            'userwallet.UserWalletID' => $partyId
        );
        if ( $idType == 'mobile' )
        {
            $whereConditions = array(
                'userwallet.MobileNumber' => $partyId
            );
        }
        $userExsist = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.FirstName', 'userwallet.UserWalletID', 'userwallet.MobileNumber', 'userwallet.EmailID', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'userwallet.PreferredPrimaryCurrency', 'userwallet.IsVerificationCompleted', 'userwallet.WalletPlanTypeId')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where($whereConditions)
                ->first();
        return( ! $userExsist ) ? FALSE : $userExsist;
    }

    /*
     *  _userIdValidate - User Id Check Method
     *  @param Party Id $partyId
     *  @return Boolean
     */

    public static function _beneficiaryValidate($BeneficiaryID, $benfType = 'eWallet', $isMobile = FALSE, $isTrpleClik = FALSE)
    {
        if ( $benfType == 'eWallet' )
        {
            $selectArray = array(
                'userwallet.FirstName',
                'userwallet.LastName',
                'userwallet.UserWalletID',
                'userbeneficiary.BeneficiaryWalletID',
                'userwallet.MobileNumber',
                'walletaccount.WalletAccountId',
                'walletaccount.WalletAccountId',
                'walletaccount.WalletAccountNumber',
                'walletaccount.AvailableBalance',
                'walletaccount.CurrentAmount',
                'userwallet.PreferredPrimaryCurrency',
                'userwallet.IsVerificationCompleted',
                'userwallet.WalletPlanTypeId',
                'userbeneficiary.BeneficiaryWalletID',
                'userbeneficiary.TripleClickAmount',
                'userbeneficiary.isTripleClickUser',
                'userbeneficiary.yesBankBeneficiaryId',
                'userbeneficiary.yesBankBeneficiaryStatus'
            );

            $whereArray = array(
                'userbeneficiary.BeneficiaryMobileNumber' => $BeneficiaryID
            );

            if ( $isMobile === TRUE )
            {
                $whereArray = array(
                    'userwallet.MobileNumber' => $BeneficiaryID
                );
            }
            if ( $isTrpleClik !== FALSE )
            {
                $whereArray['userbeneficiary.isTripleClickUser'] = $isTrpleClik;
            }

            $beneficiaryExsist = \App\Models\Userbeneficiary::select($selectArray)
                    ->join('userwallet', 'userwallet.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                    ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userbeneficiary.BeneficiaryWalletID')
                    ->where($whereArray)
                    ->first();

            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }

        if ( $benfType == 'Bank' )
        {
            $beneficiaryExsist = \App\Models\Userbeneficiary::select('BeneficiaryID', 'yesBankBeneficiaryId', 'yesBankBeneficiaryStatus', 'UserWalletID', 'BeneficiaryWalletID')
                    ->where('AccountNumber', $BeneficiaryID)
                    ->first();
            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }

        if ( $benfType == 'Vcard' )
        {
            $beneficiaryExsist = \App\Models\Userbeneficiary::select('BeneficiaryID', 'UserWalletID', 'BeneficiaryWalletID', 'BeneficiaryMobileNumber', 'WalletBeneficiaryName', 'yesBankBeneficiaryId', 'yesBankBeneficiaryStatus')
                    ->where('BeneficiaryID', $BeneficiaryID)
                    ->first();
            return( ! $beneficiaryExsist ) ? FALSE : $beneficiaryExsist;
        }
        return FALSE;
    }

    /**
     * code for insertion of card details for add money In party prefered payment data
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for insertion of card details for add money
    public static function _savePartyPereferdPayment($request, $card_id)
    {
        $party_prefered          = new \App\Models\Partypreferredpaymentmethod;
        $party_prefered->PartyID = $request->input('partyId');
        $holder                  = $request->input('cardholderName');
        $encKey                  = sha1(config('vwallet.ENCRYPT_STATIC'));
        if ( $card_id != '' && $request->input('savecardList') == 'Y' )
        {
            // code to fetch saved card data from externalcard and insert into partyprefered data
            $select_wallet_savedcards          = \App\Models\Userexternalcard::
                    select('MoneySourceCode', 'CardVendor', 'CardNumber', 'CardExpiryDate', 'CardHolderName')->where('UserExternalCardID', '=', $card_id)->first();
            $party_prefered->CardHolderName    = $select_wallet_savedcards->CardHolderName;
            $party_prefered->MoneySourceCode   = $select_wallet_savedcards->MoneySourceCode;
            $party_prefered->CardNumber        = $select_wallet_savedcards->CardNumber;
            $party_prefered->CardDisplayNumber = $select_wallet_savedcards->CardDisplayNumber;
            $party_prefered->CardExpiryDate    = $select_wallet_savedcards->CardExpiryDate;
            $party_prefered->SaveCardReference = 'Y';
            $party_prefered->CreatedDateTime   = date('Y-m-d H:i:s');
            $party_prefered->CreatedBy         = $select_wallet_savedcards->PartyID;
        }
        else
        {
            // code to insert if card is not saved in saved list.this is direct card data
            if ( $request->input('moneySource') != 'NBK' )
            {

                $encryCardNo                       = \App\Libraries\Crypt\Encryptor::encrypt($request->input('cardNo'), $encKey);
                $holder                            = $request->input('cardholderName');
                $party_prefered->CardHolderName    = $request->input('cardholderName');
                $party_prefered->CardNumber        = $encryCardNo;
                $party_prefered->CardDisplayNumber = substr_replace($request->input('cardNo'), str_repeat('x', strlen($request->input('cardNo')) - 4), 0, -4);
                $party_prefered->CardExpiryDate    = $request->input('cardExp');
                $party_prefered->SaveCardReference = ($request->input('saveMyCard') == 'Y') ? 'Y' : 'N';
            }
            if ( $request->input('moneySource') == 'NBK' )
            {
                $holder                      = $request->input('netBankingCode');
                $party_prefered->NetBankCode = $request->input('netBankingCode');
            }
            $party_prefered->MoneySourceCode = $request->input('moneySource');
            $party_prefered->CreatedDateTime = date('Y-m-d H:i:s');
            $party_prefered->CreatedBy       = $request->input('partyId');
        }
        // code to insert cards details starts

        $party_prefered->save();

        // code to insert external card if user needs to save his card in external card
        // code to insert cards details starts
        if ( $request->input('saveMyCard') == 'Y' )
        {
            //$select_wallet_savedcards = \App\Models\Userexternalcard::
            //      select('MoneySourceCode', 'CardVendor', 'CardNumber', 'CardExpiryDate', 'CardHolderName')->where('CardNumber', '=', $encryCardNo)
            //    ->where('PartyID', '=', $request->input('partyId'))
            //  ->count();

            $cardNo                   = $request->input('cardNo');
            //\DB::enableQueryLog();
            $select_wallet_savedcards = \App\Models\Userexternalcard::
                            select('MoneySourceCode', 'CardVendor', 'CardNumber', 'CardExpiryDate', 'CardHolderName')
                            ->where('PartyID', '=', $request->input('partyId'))->get();
            $cardExist                = FALSE;
            if ( count($select_wallet_savedcards) > 0 )
            {
                foreach ( $select_wallet_savedcards as $key => $card )
                {
                    if ( (isset($card->CardNumber) == TRUE) && (\App\Libraries\Crypt\Decryptor::decrypt($card->CardNumber, sha1(config('vwallet.ENCRYPT_STATIC'))) == $cardNo) )
                    {
                        $cardExist = TRUE;
                    }
                }
            }
            if ( $cardExist !== TRUE )
            {
                $wallet_recharge                    = new \App\Models\Userexternalcard;
                //  $wallet_recharge->UserExternalCardID = $party_prefered->PaymentMethodID;
                $wallet_recharge->PartyID           = $request->input('partyId');
                $wallet_recharge->CardHolderName    = $request->input('cardholderName');
                $wallet_recharge->MoneySourceCode   = $request->input('moneySource');
                $wallet_recharge->CardNumber        = $encryCardNo;
                $wallet_recharge->CardDisplayNumber = substr_replace($request->input('cardNo'), str_repeat('x', strlen($request->input('cardNo')) - 4), 0, -4);
                $wallet_recharge->CardExpiryDate    = $request->input('cardExp');
                $wallet_recharge->CreatedDateTime   = date('Y-m-d H:i:s');
                $wallet_recharge->CreatedBy         = $request->input('partyId');
                $wallet_recharge->save();
            }
        }
        return array( $party_prefered->PaymentMethodID, $holder );
    }

    public static function topupCardInfo($request)
    {
        $cardId                  = $request->input('savedcardId');
        $preffredPaymentMethodID = $request->input('savedcardId');

        // save party preferedpayment
        list($preffredPaymentMethodID, $cardHolderName) = self::_savePartyPereferdPayment($request, $cardId);
        $thirdPartyInfo = array(
            'PreffredPaymentMethodID' => $preffredPaymentMethodID,
            'CardHolderName'          => $cardHolderName
        );

        return $thirdPartyInfo;
    }

    public static function walletToBankPrefferance($request, $partyInfo, $benificiaryInfo, $fee)
    {

        $party_prefered               = new \App\Models\Userexternalbeneficiary;
        $party_prefered->UserWalletID = $request->input('partyId');
        if ( $request->input('isSavedBenificiary') == 'Y' )
        {
            // code to fetch saved card data from externalcard and insert into partyprefered data
            $selectBenificiary                     = \App\Models\Userbeneficiary::
                            select('BeneficiaryWalletID', 'BeneficiaryType', 'BankIFSCCode', 'MMID', 'AccountName', 'AccountNumber', 'BeneficiaryMobileNumber', 'WalletBeneficiaryName', 'BeneficiaryVPA')
                            ->where('BeneficiaryID', '=', $request->input('beneficiaryId'))->first();
            $party_prefered->BeneficiaryWalletID   = $selectBenificiary->BeneficiaryWalletID;
            $party_prefered->BeneficiaryType       = $selectBenificiary->BeneficiaryType;
            $party_prefered->WalletBeneficiaryName = $selectBenificiary->WalletBeneficiaryName;
            $party_prefered->Comments              = $selectBenificiary->Comments;

            if ( $request->input('businessModelId') == 2 )
            {
                $party_prefered->BeneficiaryVPA = $selectBenificiary->BeneficiaryVPA;
            }

            if ( $request->input('businessModelId') == 3 )
            {
                $party_prefered->AccountName   = $selectBenificiary->AccountName;
                $party_prefered->BankIFSCCode  = $selectBenificiary->BankIFSCCode;
                $party_prefered->AccountNumber = $selectBenificiary->AccountNumber;
            }

            if ( $request->input('businessModelId') == 4 )
            {
                $party_prefered->MMID                    = $selectBenificiary->MMID;
                $party_prefered->BeneficiaryMobileNumber = $selectBenificiary->BeneficiaryMobileNumber;
            }
            $holder                          = $selectBenificiary->WalletBeneficiaryName;
            $party_prefered->CreatedDateTime = date('Y-m-d H:i:s');
            $party_prefered->CreatedBy       = $selectBenificiary->PartyID;
        }
        else
        {
            // code to insert if benificiary is not saved in saved list.this is direct benificiary data
            $party_prefered->BeneficiaryWalletID = $benificiaryInfo->UserWalletID;
            $party_prefered->BeneficiaryType     = $fee['transctionVendor'];
//            $party_prefered->WalletBeneficiaryName = $request->input('benificiaryName');
            $party_prefered->Comments            = $request->input('remarks');
            $party_prefered->AccountName         = $partyInfo->FirstName . '' . $partyInfo->LastName;
            if ( $request->input('businessModelId') == 2 )
            {
                $party_prefered->BeneficiaryVPA = $request->input('vpa');
            }

            if ( $request->input('businessModelId') == 3 )
            {
                $party_prefered->BankIFSCCode  = $request->input('ifscCode');
                $party_prefered->AccountNumber = $request->input('accountNumber');
            }

            if ( $request->input('businessModelId') == 4 )
            {
                $party_prefered->MMID                    = $request->input('mmid');
                $party_prefered->BeneficiaryMobileNumber = $request->input('benificiaryMobileNumber');
            }
            $holder                          = $partyInfo->FirstName . '' . $partyInfo->LastName;
            $party_prefered->CreatedDateTime = date('Y-m-d H:i:s');
            $party_prefered->CreatedBy       = $request->input('partyId');
        }
        $party_prefered->save();

        // code to insert external benificiary if user needs to save his benificiary in external benificiary
        // code to insert benificiary details starts
        if ( $request->input('saveThisBenificiar') == 'Y' )
        {
            $whereArray = array();

            switch ( $request->input('businessModelId') )
            {
                case 2:
                    $whereArray['BeneficiaryVPA'] = $request->input('vpa');
                    break;
                case 3:
                    $whereArray['AccountNumber']  = $request->input('accountNumber');
                    $whereArray['BankIFSCCode']   = $request->input('ifscCode');
                    break;
                case 4:
                    $whereArray['MMID']           = $request->input('mmid');
                    break;
                default:
                    break;
            }

            $checkBenificiary = \App\Models\Userbeneficiary::
                    select('BeneficiaryWalletID', 'BeneficiaryType', 'BankIFSCCode', 'MMID', 'AccountName', 'AccountNumber', 'BeneficiaryMobileNumber', 'WalletBeneficiaryName', 'BeneficiaryVPA')
                    ->where('BeneficiaryType', '=', $fee['transctionVendor'])
                    ->where('UserWalletID', '=', $request->input('partyId'))
                    ->where($whereArray)
                    ->count();
            if ( $checkBenificiary <= 0 )
            {
                $saveBenificiary                        = new \App\Models\Userbeneficiary;
                $saveBenificiary->UserWalletID          = $request->input('partyId');
                $saveBenificiary->BeneficiaryWalletID   = $benificiaryInfo->UserWalletID;
                $saveBenificiary->BeneficiaryType       = $fee['transctionVendor'];
                $saveBenificiary->WalletBeneficiaryName = $request->input('benificiaryName');
                $saveBenificiary->Comments              = $request->input('remarks');
                $saveBenificiary->AccountName           = $partyInfo->FirstName . '' . $partyInfo->LastName;
                if ( $request->input('businessModelId') == 2 )
                {
                    $saveBenificiary->BeneficiaryVPA = $request->input('vpa');
                }

                if ( $request->input('businessModelId') == 3 )
                {
                    $saveBenificiary->AccountName   = $partyInfo->FirstName . '' . $partyInfo->LastName;
                    $saveBenificiary->BankIFSCCode  = $request->input('ifscCode');
                    $saveBenificiary->AccountNumber = $request->input('accountNumber');
                }

                if ( $request->input('businessModelId') == 4 )
                {
                    $saveBenificiary->MMID                    = $request->input('mmid');
                    $saveBenificiary->BeneficiaryMobileNumber = $request->input('benificiaryMobileNumber');
                }
                $saveBenificiary->CreatedDateTime = date('Y-m-d H:i:s');
                $saveBenificiary->CreatedBy       = $request->input('partyId');
                $saveBenificiary->save();
            }
        }
        $thirdPartyInfo = array(
            //'PreffredPaymentMethodID' => $party_prefered->PaymentMethodID,
            'PreffredPaymentMethodID' => 1,
            'CardHolderName'          => $holder
        );
        return $thirdPartyInfo;
    }

    public static function cashBack($partyId, $offerPamentInfo, $benefitTrackData = '')
    {
        //$promoCode     = $offerPamentInfo['promoCode'];
        $offerAmount   = $offerPamentInfo['offerAmount'];
        //$event         = $offerPamentInfo['event'];
        $message       = $offerPamentInfo['message'];
        //$vendor        = $offerPamentInfo['vendor'];
        //$txnType       = $offerPamentInfo['txnType'];
        //$amountTag     = $offerPamentInfo['amountTag'];
        $adminWalletId = $offerPamentInfo['offerAdminWalletAccountId'];

        $paymentsProductEventID = $offerPamentInfo['productEventId']; // this is for payment product
        // this business model id i.e from table payments entity
        $partyInfo              = self::_userIdValidate($partyId);

        //$admin_wallet           = \App\Libraries\Helpers::GetAdminData();
        $admin_wallet = self::_userIdValidate($adminWalletId);

        $paymentProduct = self::getPaymentProductEventInfo($paymentsProductEventID);

        $txn_detail_id = self::_saveTransactiondetail($offerPamentInfo, $admin_wallet, $partyInfo, $benefitTrackData);

        // code to insert transactionEventlog
        self::_saveTransactionEventLog($txn_detail_id, $offerPamentInfo, $partyInfo);

        // code to insert Daily transactionEventlog
        self::_saveTopupDailyTransactionLog($txn_detail_id, $offerPamentInfo, $partyInfo);

        // code to update notifications
        self::_saveNotificationlog($txn_detail_id, $partyId, $message);

        if ( $benefitTrackData )
        {
            $userOfferBenefitTrack = self::_saveUserOfferBenefitTracker($txn_detail_id, $benefitTrackData);
        }
        // update user wallet balance starts      
        $update_wallet_balance = self::_updateCashBackWalletBalance($partyInfo, $admin_wallet, $offerPamentInfo);
        if ( $update_wallet_balance == TRUE )
        {
            $updateTxnDetails                    = \App\Models\Transactiondetail::find($txn_detail_id);
            $updateTxnDetails->TransactionStatus = config('vwallet.transactionStatus.success');
            $updateTxnDetails->save();
        }
        //update transaction aggregates
        \App\Libraries\Statistics::monthlyCreditTransactionsUpdate($partyId, $offerAmount);
// code to update txn detail status ends

        return $update_wallet_balance;
    }

    public static function userOfferStatsInsert($partyId, $offerPamentInfo, $status = 'View')
    {
        $dateTimeOfTrans = date('Y-m-d H:i:s');
        $userOfferStats  = array(
            'UserWalletID'     => $partyId,
            'OfferBusinessID'  => $offerPamentInfo['offerBusinessId'],
            'PromoCode'        => $offerPamentInfo['promoCode'],
            'UserActivity'     => $status,
            'ActivityDateTime' => $dateTimeOfTrans,
            'AmountUsed'       => $offerPamentInfo['offerAmount'],
            'CreatedDateTime'  => $dateTimeOfTrans,
            'CreatedBy'        => 'User',
        );
        return \App\Models\Useroffersstat::insertGetId($userOfferStats);
    }

    /**
     * code for to check _updateCashBackWalletBalance in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    private static function _updateCashBackWalletBalance($partyInfo, $admin_wallet, $offerPamentInfo)
    {

        $amount = $offerPamentInfo['offerAmount'];
        // code to update user balance
        //$admin_wallet = \App\Libraries\Helpers::GetAdminData();
        if ( $admin_wallet->CurrentAmount >= $amount )
        {


            $update_adminWallet                   = \App\Models\Walletaccount::find($admin_wallet->WalletAccountId);
            $update_adminWallet->AvailableBalance = $update_adminWallet->AvailableBalance - $amount;
            $update_adminWallet->CurrentAmount    = $update_adminWallet->CurrentAmount - $amount;
            $update_adminWallet->save();

            $update_userWallet = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);

            if ( $offerPamentInfo['benefitCashBackTyp'] == 'NW' )
            {
                $update_userWallet->NonWithdrawAmount = $partyInfo->NonWithdrawAmount + $amount;
            }
            else
            {
                $update_userWallet->AvailableBalance = $partyInfo->AvailableBalance + $amount;
                $update_userWallet->CurrentAmount    = $partyInfo->CurrentAmount + $amount;
            }
            $update_userWallet->save();

            return TRUE;
        }
        return FALSE;
    }

    /**
     * code for to check _updateTransactionEvent in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for update transaction detail event code for reversal
    private static function _updateTransactionEvent($txn_detail_id, $status, $event)
    {
        $update_txn_detail                    = \App\Models\Transactiondetail::find($txn_detail_id);
        $update_txn_detail->EventCode         = $event;
        $update_txn_detail->TransactionStatus = $status;
        $update_txn_detail->TransactionDate   = date('Y-m-d H:i:s');
        $update_txn_detail->save();
    }

    /**
     * code for insertion of transaction event log table
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for transaction event log
    private static function _saveTransactionEventLog($txn_detail_id, $offerPamentInfo, $partyInfo)
    {
        // code to insert cards details starts
        $insert_event_log                      = new \App\Models\Transactioneventlog;
        $insert_event_log->TransactionDetailID = $txn_detail_id;
        $insert_event_log->PaymentsProductID   = $offerPamentInfo['PaymentsProductID'];
        $insert_event_log->EventCode           = $offerPamentInfo['event'];
        $insert_event_log->Comments            = $offerPamentInfo['message'];
        $insert_event_log->EventDate           = date('Y-m-d H:i:s');
        $insert_event_log->CreatedBy           = $partyInfo->UserWalletID;
        $insert_event_log->CreatedDateTime     = date('Y-m-d H:i:s');
        $insert_event_log->save();

        // code to insert cards details ends
    }

    /**
     * code for to check _saveTopupDailyTransactionLog in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for transaction event log
    private static function _saveTopupDailyTransactionLog($txn_detail_id, $offerPamentInfo, $partyInfo)
    {

        $sel_daily_txn_log = \App\Models\Dailytransactionlog::select('TransactionLineNumber')
                ->where('TransactionDetailID', $txn_detail_id)
                ->orderby('DailyTransactionSequenceNumber', 'DESC')
                ->first();

        $insert_line_number                           = ( ! empty($sel_daily_txn_log)) ? $sel_daily_txn_log->TransactionLineNumber + 1 : 1;
        // code to insert user txn details starts
        $insert_event_log                             = new \App\Models\Dailytransactionlog();
        $insert_event_log->TransactionDetailID        = $txn_detail_id;
        $insert_event_log->TransactionLineNumber      = $insert_line_number;
        $insert_event_log->TransactionWalletAccountId = $partyInfo->WalletAccountId;
        $insert_event_log->PartyId                    = $partyInfo->UserWalletID;
        $insert_event_log->TransactionCcyAmount       = $offerPamentInfo['offerAmount'];
        $insert_event_log->LocalCcyAmount             = $offerPamentInfo['offerAmount'];
        $insert_event_log->AcctCcyAmount              = $offerPamentInfo['offerAmount'];
        $insert_event_log->AccountCurrency            = 'INR';
        $insert_event_log->TransactionCurrency        = 'INR';
        $insert_event_log->LocalCurrency              = 'INR';
        $insert_event_log->ExchangeRate               = 'INR';
        $insert_event_log->EventCode                  = $offerPamentInfo['event'];
        $insert_event_log->CrDrIndicator              = 'C';
        $insert_event_log->TransactionDate            = date('Y-m-d');
        $insert_event_log->ValueDate                  = date('Y-m-d');
        $insert_event_log->BankReconciliationStatus   = 'N';
        $insert_event_log->BankReconciliationStatus   = 'N';
        $insert_event_log->AmountTag                  = $offerPamentInfo['amountTag'];
        $insert_event_log->PaymentsProductID          = $offerPamentInfo['PaymentsProductID'];
        $insert_event_log->CreatedBy                  = config('vwallet.adminTypeKey');
        $insert_event_log->UpdatedBy                  = config('vwallet.adminTypeKey');
        $insert_event_log->CreatedDateTime            = date('Y-m-d H:i:s');
        $insert_event_log->save();

        // code for admin txn details starts
        // code to insert user txn details starts
        $admin_data                                         = \App\Libraries\Helpers::GetAdminData();
        $insert_admin_event_log                             = new \App\Models\Dailytransactionlog();
        $insert_admin_event_log->TransactionDetailID        = $txn_detail_id;
        $insert_admin_event_log->TransactionLineNumber      = $insert_line_number;
        $insert_admin_event_log->TransactionWalletAccountId = $admin_data->WalletAccountId;
        $insert_admin_event_log->PartyId                    = $admin_data->UserWalletId;
        $insert_admin_event_log->TransactionCcyAmount       = $offerPamentInfo['offerAmount'];
        $insert_admin_event_log->LocalCcyAmount             = $offerPamentInfo['offerAmount'];
        $insert_admin_event_log->AcctCcyAmount              = $offerPamentInfo['offerAmount'];
        $insert_admin_event_log->AccountCurrency            = 'INR';
        $insert_admin_event_log->TransactionCurrency        = 'INR';
        $insert_admin_event_log->LocalCurrency              = 'INR';
        $insert_admin_event_log->ExchangeRate               = 'INR';
        $insert_admin_event_log->EventCode                  = $offerPamentInfo['event'];
        $insert_admin_event_log->CrDrIndicator              = 'D';
        $insert_admin_event_log->TransactionDate            = date('Y-m-d');
        $insert_admin_event_log->ValueDate                  = date('Y-m-d');
        $insert_admin_event_log->BankReconciliationStatus   = 'N';
        $insert_admin_event_log->BankReconciliationStatus   = 'N';
        $insert_admin_event_log->AmountTag                  = $offerPamentInfo['amountTag'];
        $insert_admin_event_log->PaymentsProductID          = $offerPamentInfo['PaymentsProductID'];
        $insert_admin_event_log->CreatedBy                  = config('vwallet.adminTypeKey');
        $insert_admin_event_log->UpdatedBy                  = config('vwallet.adminTypeKey');
        $insert_admin_event_log->CreatedDateTime            = date('Y-m-d H:i:s');
        $insert_admin_event_log->save();
        return TRUE;
        //return $insert_event_log->DailyTransactionSequenceNumber;
        // code to insert cards details ends
    }

    /**
     * code for to check _saveNotificationlog in add money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for Insert Notifications
    private static function _saveNotificationlog($txn_detail_id, $partyId, $message)
    {
        // code to insert cards details starts
        $insert_notif_log                        = new \App\Models\Notificationlog;
        $insert_notif_log->LinkedtransactionID   = $txn_detail_id;
        $insert_notif_log->PartyID               = $partyId;
        $insert_notif_log->NotificationType      = 'Alert';
        $insert_notif_log->SentDateTime          = date('Y-m-d H:i:s');
        $insert_notif_log->ReceipentAcknowledged = 'N';
        $insert_notif_log->ReceipentReceivedDate = date('Y-m-d H:i:s');
        $insert_notif_log->Message               = $message;
        $insert_notif_log->CreatedBy             = config('vwallet.adminTypeKey');
        $insert_notif_log->UpdatedBy             = config('vwallet.adminTypeKey');
        $insert_notif_log->CreatedDateTime       = date('Y-m-d H:i:s');
        $insert_notif_log->save();

        // code to insert cards details ends
    }

    private static function _saveUserOfferBenefitTracker($txn_id, $benefitTrackData)
    {
        $offerBenefitData                              = \App\Models\Userofferbenefittracker::find($benefitTrackData->UserOfferBenefitTrackerID);
        $offerBenefitData->CashbackTransactionDetailID = $txn_id;
        $offerBenefitData->UpdatedDateTime             = date('Y-m-d H:i:s');
        $offerBenefitData->save();
        return TRUE;
    }

    /**
     * code for insertion of transaction data
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card
     *
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for insertion of card details for add money
    private static function _saveTransactiondetail($pamentInfo, $admin_wallet, $partyInfo, $benefitTrackData = '')
    {
        //print_r($admin_wallet->UserWalletID);exit;
        // code to insert cards details starts
        $transactionOrderId = self::getTransctionId();
        $status             = config('vwallet.transactionStatus.pending');

        $insert_txn_details                         = new \App\Models\Transactiondetail;
        $insert_txn_details->PaymentsProductID      = $pamentInfo['PaymentsProductID'];
        $insert_txn_details->TransactionOrderID     = $transactionOrderId;
        $insert_txn_details->UserWalletId           = $admin_wallet->UserWalletID;
        $insert_txn_details->FromAccountId          = $admin_wallet->WalletAccountId;
        $insert_txn_details->FromAccountType        = 'WalletAccount';
        $insert_txn_details->ToAccountType          = 'WalletAccount';
        $insert_txn_details->ToAccountId            = $partyInfo->WalletAccountId;
        $insert_txn_details->ToWalletId             = $partyInfo->UserWalletID;
        $insert_txn_details->Amount                 = $pamentInfo['offerAmount'];
        $insert_txn_details->TransactionTypeCode    = 'Deposit';
        $insert_txn_details->TransactionDate        = date('Y-m-d H:i:s');
        $insert_txn_details->TransactionVendor      = $pamentInfo['vendor'];
        $insert_txn_details->TransactionStatus      = $status;
        $insert_txn_details->TransactionMode        = 'credit';
        $insert_txn_details->TransactionType        = $pamentInfo['txnType'];
        $insert_txn_details->EventCode              = $pamentInfo['event'];
        $insert_txn_details->FromAccountName        = $admin_wallet->FirstName;
        // $insert_txn_details->UserSessionUsageDetailID = (\App\Libraries\Helpers::getUserSessionId($request)!=FALSE) ? \App\Libraries\Helpers::getUserSessionId($request) : 0;
        $insert_txn_details->FeeAmount              = '0.00';
        $insert_txn_details->TaxAmount              = '0.00';
        $insert_txn_details->CreatedBy              = config('vwallet.adminTypeKey');
        $insert_txn_details->UpdatedBy              = config('vwallet.adminTypeKey');
        $insert_txn_details->CreatedDatetime        = date('Y-m-d H:i:s');
        //$insert_txn_details->PromoCode           = ( ! empty($request->input('promoCode'))) ? $request->input('promoCode') : NULL;
        $insert_txn_details->RefTransactionDetailID = ($benefitTrackData) ? $benefitTrackData->ParentTransactionDetailID : NULL;

        $insert_txn_details->save();

        // get the insert id
        return $insert_txn_details->TransactionDetailID;

        // code to insert cards details ends
    }

    /**
     * Cash back will be given by cronjob, Internal application use
     * @param type $request
     * @param type $requestArray
     */
    public static function cashBackTransaction($request, $verifiedPromoCodeData, $benefitTrackData = '')
    {
        $cashBackStatus = FALSE;
        if ( $verifiedPromoCodeData->is_error == FALSE )
        {
            $offerData = $verifiedPromoCodeData->res_data;
            if ( ! empty($offerData) )
            {
                $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);
                if ( ! empty($pamentInfo) && ($pamentInfo['event'] == 'CASHBACK' || $pamentInfo['event'] == 'DELAYECB') )
                {
                    $partyId        = $request->input('partyId');
                    $insertCashback = FALSE;
                    $insertCashback = self::cashBack($partyId, $pamentInfo, $benefitTrackData);
                    if ( $insertCashback == TRUE )
                    {
                        $insertUserOfferStats = self::userOfferStatsInsert($partyId, $pamentInfo, 'Used');
                    }
                    $cashBackStatus = TRUE;
                }
            }
        }
        return $cashBackStatus;
    }

    /**
     * get promo info
     * @param type $promoCode
     * @return type
     */
    private static function bussinessPromoinfo($promoCode)
    {
        $selectedColumns = $selectedColumns = [
            'promotionaloffers.PromoCode',
            'offerbenefits.BenefitApplicableChannel',
            'offerbenefits.BenefitFixedAmount',
            'offerbenefits.BenefictPercentageValue',
        ];
        $dbResults       = \App\Models\Offerbenefit::join('offerbenefitsbridge', 'offerbenefits.BenefitBusinessID', '=', 'offerbenefitsbridge.BenefitBusinessID')
                        //->join('userwallet', 'dailytransactionlog.PartyId', '=', 'userwallet.UserWalletID')
                        ->rightjoin('promotionaloffers', 'promotionaloffers.OfferBusinessID', '=', 'offerbenefitsbridge.OfferBusinessID')
                        ->orderBy('offerbenefits.BenefitCategoryRefID', 'ASC')
                        ->where(array( 'promotionaloffers.PromoCode' => $promoCode, 'promotionaloffers.ActiveInd' => 'Y' ))
                        ->get($selectedColumns)->first();
        return $dbResults;
    }

    /**
     * upiPromoCodeInfo get upi promo code data
     * @param type $request
     * @return array
     */
    /* public static function upiCashBackTransaction($request)
      {
      $requestArray = self::upiPromoCodeInfo($request);
      $verifyJsonData = \App\Libraries\OfferRules::verifyPromoCode($requestArray);
      return $cashBackStatus = self::cashBackTransaction($request, $verifyJsonData);
      } */
    public static function upiPromoCodeInfo($request)
    {
        $requestArray = [];
        $dbResults    = self::bussinessPromoinfo('VWUPI50');
        $countResults = count($dbResults);
        if ( $countResults != 1 )
        {
            return $requestArray;
        }
        $requestArray['aplicableChannel'] = $request->header('device-type');
        $requestArray['promoCode']        = $dbResults->PromoCode;
        $requestArray['offerCategoryId']  = 'UPI';
        $requestArray['offerUserTypeId']  = 'Customer';
        //$requestArray['benefitCategoryId'] = 0;
        $requestArray['amount']           = $dbResults->BenefitFixedAmount;
        $requestArray['partyId']          = $request->input('partyId');
        return $requestArray;
    }

    public static function referralPromoCodeInfo($request)
    {
        $requestArray = [];
        $dbResults    = self::bussinessPromoinfo('REFERRURF50');
        $countResults = count($dbResults);
        if ( $countResults != 1 )
        {
            return $requestArray;
        }
        $requestArray['aplicableChannel'] = $request->header('device-type');
        $requestArray['promoCode']        = $dbResults->PromoCode;
        $requestArray['offerCategoryId']  = 'Referral';
        $requestArray['offerUserTypeId']  = 'Customer';
        $requestArray['paymentProductId'] = '7';
        $requestArray['amount']           = $dbResults->BenefitFixedAmount;
        $requestArray['partyId']          = $request->input('partyId');
        return $requestArray;
    }

    public static function violaSignupPromoCodeInfo($request)
    {
        $requestArray = [];
        $dbResults    = self::bussinessPromoinfo('VIOLAREFERR100');
        $countResults = count($dbResults);
        if ( $countResults != 1 )
        {
            return $requestArray;
        }
        $requestArray['aplicableChannel'] = $request->header('device-type');
        $requestArray['promoCode']        = $dbResults->PromoCode;
        $requestArray['offerCategoryId']  = 'Referral';
        $requestArray['offerUserTypeId']  = 'Customer';
        $requestArray['amount']           = $dbResults->BenefitFixedAmount;
        $requestArray['partyId']          = $request->input('partyId');
        return $requestArray;
    }

    public static function getBilldeskName($prodId)
    {
        $billerName           = FALSE;
        $billdeskNameRecharge = \App\Models\Billerlist::select('BillerName')
                ->where('BillerId', $prodId)
                ->first();

        if ( ! empty($billdeskNameRecharge) )
        {

            return $billerName = $billdeskNameRecharge['BillerName'];
        }

        $billdeskNamePayments = \App\Models\Billdeskbiller::select('BillerName')
                ->where('BillerID', $prodId)
                ->first();

        if ( ! empty($billdeskNamePayments) )
        {

            return $billerName = $billdeskNamePayments['BillerName'];
        }
        return $billerName;
    }

    public static function getTxnMobileNumber($txnId)
    {

        $getBillNumber = \App\Models\Transactiondetail::select('userbillertransactions.SubscriberNumber')
                ->join('userbillertransactions', 'userbillertransactions.TransactionDetailID', '=', 'transactiondetail.TransactionDetailID')
                ->where('transactiondetail.TransactionDetailID', $txnId)
                ->first();
        if ( ! empty($getBillNumber) )
        {
            return $getBillNumber['SubscriberNumber'];
        }

        $getBillrecharges = \App\Models\Transactiondetail::select('userrechargedetails.SubscriberNumber')
                ->join('userrechargedetails', 'userrechargedetails.TransactionDetailID', '=', 'transactiondetail.TransactionDetailID')
                ->where('transactiondetail.TransactionDetailID', $txnId)
                ->first();
        if ( ! empty($getBillrecharges) )
        {
            return $getBillrecharges['SubscriberNumber'];
        }
    }

    public static function getTopupCardNumber($txnId)
    {
        $selTxnTopupCardName = \App\Models\Transactiondetail::select('partypreferredpaymentmethod.CardDisplayNumber', 'partypreferredpaymentmethod.NetBankCode', 'transactiondetail.TransactionVendor')
                ->join('partypreferredpaymentmethod', 'partypreferredpaymentmethod.PaymentMethodID', '=', 'transactiondetail.FromAccountId')
                ->where('transactiondetail.TransactionDetailID', $txnId)
                ->first();

        if ( ! empty($selTxnTopupCardName) )
        {
            list($typeOfpayment) = explode('-', $selTxnTopupCardName['TransactionVendor']);
            if ( strpos($selTxnTopupCardName['TransactionVendor'], '-') != FALSE )
            {

                list($pg1, $typeOfpayment) = explode('-', $selTxnTopupCardName['TransactionVendor']);
            }
            $cardNumBankName = ( ! empty($selTxnTopupCardName['CardDisplayNumber'])) ? str_replace('x', '', $selTxnTopupCardName['CardDisplayNumber']) : $selTxnTopupCardName['NetBankCode'];

            return array( $cardNumBankName, $typeOfpayment );
        }
        return '';
    }

    public static function getProductId($eventCode)
    {
        return $selEventGetId = \App\Models\Userwallet::select('UserWalletID')
                        ->where('FirstName', $eventCode)->first();
    }

    /**
     * 
     * @param type $partyId
     */
    public static function getWalletInfo($partyId)
    {
        $whereConditions = array(
            'userwallet.UserWalletID' => $partyId
        );
        $userWalletData  = \App\Models\Userwallet::select('userwallet.UserWalletID', 'walletaccount.WalletAccountId', 'walletaccount.WalletAccountNumber', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount', 'userwallet.PreferredPrimaryCurrency', 'userwallet.WalletPlanTypeId')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where($whereConditions)
                ->first();
        $retunInfo       = [];
        if ( count($userWalletData) > 0 )
        {
            $retunInfo['AvailableBalance']         = number_format($userWalletData->AvailableBalance, 2, '.', '');
            $retunInfo['CurrentAmount']            = number_format($userWalletData->CurrentAmount, 2, '.', '');
            $retunInfo['NonWithdrawAmount']        = number_format($userWalletData->NonWithdrawAmount, 2, '.', '');
            $retunInfo['HoldAmount']               = number_format($userWalletData->HoldAmount, 2, '.', '');
            $retunInfo['PreferredPrimaryCurrency'] = $userWalletData->PreferredPrimaryCurrency;
            $retunInfo['PreferredPrimaryCurrency'] = $userWalletData->PreferredPrimaryCurrency;
            $userTransactionStats                  = \App\Libraries\Statistics::getUserTransactionStats($partyId);
            $retunInfo['monthUtilityAmount']       = $userTransactionStats['monthUtilityAmount'];
            $retunInfo['monthCrditLimitAmount']    = $userTransactionStats['monthCrditLimitAmount'];
        }
        return $retunInfo;
    }

    /**
     * 
     * @param type $paryId
     * @param type $subscriberNumber
     * @param type $billerId
     * @return type
     */
    public static function remindTransactionInfo($paryId, $subscriberNumber, $billerId)
    {
        //get reminder status
        $remindTrasactions = \App\Models\Remindtransaction::select('RemindTransactionId', 'PartyId', 'ActionType')
                        ->where(array( 'Status' => 'Y', 'PartyId' => $paryId, 'SubscriberNumber' => $subscriberNumber, 'BillerId' => $billerId ))
                        ->orderby('RemindTransactionId', 'DESC')->first();
        $result            = array( 'remindVariable' => NULL, 'remindTransactionId' => NULL, 'subscriberNumber' => $subscriberNumber, 'billerId' => $billerId );
        if ( count($remindTrasactions) > 0 )
        {
            $result['remindVariable']      = $remindTrasactions->ActionType;
            $result['remindTransactionId'] = $remindTrasactions->RemindTransactionId;
        }
        return $result;
    }

    /**
     * 
     * @param type $paryId
     * @param type $trasactionId
     * @return type
     */
    public static function reminderByTransactionId($paryId, $trasactionId)
    {
        $result   = array( 'remindVariable' => NULL, 'remindTransactionId' => NULL, 'subscriberNumber' => NULL, 'billerId' => NULL );
        $billInfo = \App\Models\Userbillertransaction::select('UserWalletID', 'SubscriberNumber', 'RechargeBillerId')->where(array( 'UserWalletID' => $paryId, 'TransactionDetailID' => $trasactionId ))->first();

        if ( count($billInfo) > 0 )
        {
            $result = self::remindTransactionInfo($billInfo->UserWalletID, $billInfo->SubscriberNumber, $billInfo->RechargeBillerId);
        }
        return $result;
    }

    /**
     * Method used to send cron job notifications
     * @param type $params
     * @param type $request
     * @return type
     */
    public static function sendCronMessage($params, $request)
    {
        $templateData = array(
            'partyId'        => 1,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW079',
            'senderEmail'    => 'mahesh.bairi@violamoney.com',
            'senderMobile'   => '9949207896',
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(
                'fullName'    => 'Admin',
                'title'       => $params['title'],
                'description' => $params['description'],
                'baseUrl'     => config('vwallet.baseUrl'),
                'date'        => date('d/m/Y'),
                'time'        => date('H:i A'),
                'supportURL'  => "violamoney.com",
            )
        );
        $notifyStatus = \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        if ( ! $notifyStatus )
        {
            return FALSE;
        }
        return TRUE;
    }

}
