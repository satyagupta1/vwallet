<?php

//include_once "library/OAuthStore.php";
//include_once "library/OAuthRequester.php";

namespace App\Http\Controllers\RedBus;

use App\Libraries\Seatseller\OAuthStore;
use App\Libraries\Seatseller\OAuthRequester;

//use App\Viola\seatseller\library\OAuthStore;
//use App\Viola\seatseller\library\OAuthRequester;
class SeatsellerController extends \App\Http\Controllers\Controller {

    /**
     * @desc This will create an instance when class is called
     */
    public function __construct()
    {
        $this->createdBy = config('vwallet.adminTypeKey');
        $this->key       = config('vwallet.redbus')['key'];
        $this->secret    = config('vwallet.redbus')['secret'];
        $this->baseUrl   = config('vwallet.redbus')['apiUrl'];
    }

    /**
     * @desc _invokeGetRequest method is used to connect with red bus service using Get Method
     * @param $requestUrl type string
     * @return array
     */
    private function _invokeGetRequest($requestUrl)
    {
        $output_arr  = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $url         = $this->baseUrl . $requestUrl;
        $curlOptions = array(CURLOPT_HTTPHEADER => array('Content-Type: application/json'), CURLOPT_TIMEOUT => 0, CURLOPT_CONNECTTIMEOUT => 0);
        $options     = array('consumer_key' => $this->key, 'consumer_secret' => $this->secret);

        OAuthStore::instance("2Leg", $options);
        $method  = "GET";
        $params  = null;
        $request = new OAuthRequester($url, $method, $params);
        $result  = $request->doRequest();
        if ($result['code'] > 200)
        {
            $output_arr['display_msg'] = array('info' => $result['body']);
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        $resultBody             = json_decode($result['body']);
        if (!$resultBody)
        {
            $resultBody = $result['body'];
        }
        $output_arr['res_data'] = $resultBody;
        return $output_arr;
    }

    /**
     * @desc  _invokePostRequest method is used to connect with red bus service using Post Method
     * @param $requestUrl type string
     * @param $blockRequest type string
     * @return array
     */
    private function _invokePostRequest($requestUrl, $blockRequest)
    {
        $output_arr   = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $url          = $this->baseUrl . $requestUrl;
        $curl_options = array(CURLOPT_HTTPHEADER => array('Content-Type: application/json'), CURLOPT_TIMEOUT => 0, CURLOPT_CONNECTTIMEOUT => 0);
        $options      = array('consumer_key' => $this->key, 'consumer_secret' => $this->secret);
        OAuthStore::instance("2Leg", $options);
        $method       = "POST";
        $params       = null;
        $request      = new OAuthRequester($url, $method, $params, $blockRequest);
        //echo "Timeout is: " . $curl_options[CURLOPT_TIMEOUT] . "<hr></br>";
        //echo "Connection timeout is: " . $curl_options[CURLOPT_CONNECTTIMEOUT] . "<hr></br>";
        $result       = $request->doRequest(0, $curl_options);
        if ($result['code'] > 200)
        {
            $output_arr['display_msg'] = array('info' => $result['body']);
            return $output_arr;
        }
        $output_arr['is_error'] = FALSE;
        $resultBody             = json_decode($result['body']);
        if (!$resultBody)
        {
            $resultBody = $result['body'];
        }
        $output_arr['res_data'] = $resultBody;
        return $output_arr;
    }

    /**
     * @desc  getCities method is used to get all city names
     * @return array
     */
    public function getCities()
    {
        $output_arr = array('is_error' => FALSE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $getCities  = \App\Models\Buscity::select('RedBusId as id', 'Name as name', 'State as state', 'RedBusStateId as stateId', 'IsTopCity as isTopCity')->orderBy('Name', 'asc')->get();
        if (!$getCities)
        { 
            $output_arr['display_msg'] = array('info' => 'No cities found.');
            return response()->json($output_arr);
        }
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = array('cities' => $getCities);
        return response()->json($output_arr);
    }

    /**
     * @desc  getCitiesStore method to store city names in DB
     * @return array
     */
    public function getCitiesStore()
    {
        ini_set('max_execution_time', 0);
        $redbusResponse = $this->_invokeGetRequest("sources");
        foreach ($redbusResponse['res_data']->cities as $value)
        {
            $Buscity                = new Buscity;
            $Buscity->RedBusId      = $value->id;
            $Buscity->Name          = $value->name;
            $Buscity->State         = (isset($value->state)) ? $value->state : 'NA';
            $Buscity->RedBusStateId = (isset($value->stateId)) ? $value->stateId : '0';
            $Buscity->save();
        }
        return response()->json($redbusResponse);
    }

    /**
     * @desc  getBusList method is used to get all buses list
     * @param $fromCityId type INT
     * @param $toCityId type INT
     * @param $journyDate type string
     * return array
     */
    public function getBusList(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateTrips($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }
        $sourceId       = $request->input('fromCityId');
        $destinationId  = $request->input('toCityId');
        $date           = $request->input('journyDate'); // Formate - YYYY-m-d
        $redbusResponse = $this->_invokeGetRequest("availabletrips?source=" . $sourceId . "&destination=" . $destinationId . "&doj=" . $date);
        //print_r($redbusResponse); exit;

        if ($redbusResponse['is_error'] == TRUE)
        {
            return response()->json($redbusResponse);
        }

        if (!isset($redbusResponse['res_data']->availableTrips))
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array('info' => 'No buses found.');
            return response()->json($output_arr);
        }
        $finalResponse = $redbusResponse['res_data']->availableTrips;
        if (!is_array($finalResponse))
        {
            $finalResponse = array($redbusResponse['res_data']->availableTrips);
        }
        foreach ($finalResponse as $key => $mainFinalResponse)
        {

            $boardingTimes = isset($mainFinalResponse->boardingTimes) ? $mainFinalResponse->boardingTimes : array();
            $droppingTimes = isset($mainFinalResponse->droppingTimes) ? $mainFinalResponse->droppingTimes : array();
            $fareDetails   = isset($mainFinalResponse->fareDetails) ? $mainFinalResponse->fareDetails : array();
            $fares         = isset($mainFinalResponse->fares) ? $mainFinalResponse->fares : array();

            $finalResponse[$key]->boardingTimes = (is_array($boardingTimes)) ? $boardingTimes : array((array) $boardingTimes);
            $finalResponse[$key]->droppingTimes = (is_array($droppingTimes)) ? $droppingTimes : array((array) $droppingTimes);
            $finalResponse[$key]->fareDetails   = (is_array($fareDetails)) ? $fareDetails : array((array) $fareDetails);
            $finalResponse[$key]->fares         = (is_array($fares)) ? $fares : (array) $fares;
        }

        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = array('availableTrips' => $finalResponse);
        return $output_arr;
    }

    /**
     * @desc  getBusLayout method is used to get all buses layouts
     * @param $tripId type INT
     * return array
     */
    public function getBusLayout(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => trans('messages.emptyRecords')), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateBusLayout($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }
        $tripId         = $request->input('tripId');
        $redbusResponse = $this->_invokeGetRequest("tripdetails?id=" . $tripId);
        return response()->json($redbusResponse);
    }

    /**
     * @desc  blockTicket method is used to block seat in bus
     * @param $inventoryItems type INT
     * return array
     */
    public function blockTicket(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateBlockTicket($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }

        $partyId         = $request->input('partyId');
        $onwardBookingId = $request->input('onwardBookingId');
        $partyInfo       = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $inventoryItemsJson = is_array($request->input('inventoryItems')) ? $request->input('inventoryItems') : json_decode($request->input('inventoryItems'), true);
        \Log::info('Reponse shekar:  ' . json_encode($request->input()));
        if (!$inventoryItemsJson)
        {
            $output_arr['display_msg'] = array('inventoryItems' => 'Please enter inventory items.');
            return response()->json($output_arr);
        }
        $inventoryItems = array();
        $error          = 0;
        $i              = 0;
        $totalFare      = 0;
        foreach ($inventoryItemsJson as $data)
        {
            if (($data['seatName'] == '') || ($data['fare'] == '') || ($data['passenger'] == ''))
            {
                $error ++;
            }
            $inventoryItems[$i] = array(
                'seatName'   => $data['seatName'],
                'fare'       => $data['fare'],
                'ladiesSeat' => $data['ladiesSeat'],
                'passenger'  => array(
                    'name'    => $data['passenger']['name'],
                    'mobile'  => $data['passenger']['mobile'],
                    'title'   => $data['passenger']['title'],
                    'email'   => $data['passenger']['email'],
                    'age'     => $data['passenger']['age'],
                    'gender'  => $data['passenger']['gender'],
                    'primary' => (!empty($data['passenger']['primary'])) ? $data['passenger']['primary'] : 0,
                )
            );
            if (isset($data['passenger']['idNumber']))
            {
                $inventoryItems[$i]['passenger']['idNumber'] = $data['passenger']['idNumber'];
                $inventoryItems[$i]['passenger']['idType']   = $data['passenger']['idType'];
            }
            $i ++;
            $totalFare += $data['fare'];
        }
        if ($error > 0)
        {
            $output_arr['display_msg'] = array('inventoryItems' => 'Please enter valid inventory items.');
            return response()->json($output_arr);
        }
        $ticketInitiationParams = array(
            'availableTripId' => $request->input('availableTripId'),
            'inventoryItems'  => $inventoryItems,
            'source'          => $request->input('source'),
            'destination'     => $request->input('destination'),
            'boardingPointId' => $request->input('boardingPointId'),
        );

        $redbusResponse = $this->_invokePostRequest("blockTicket", json_encode($ticketInitiationParams));

        if ($redbusResponse['is_error'] === TRUE)
        {
            return response()->json($redbusResponse);
        }

        //$updatedFareParams       = array('BlockKey' => $redbusResponse['res_data']);
        //$bookedTicketFareDetails = $this->_invokePostRequest("getUpdatedFare", json_encode($updatedFareParams));

        /* $updatedFareParams       = array('BlockKey' => 'SMZKMTqRkf');
          print_r(json_encode($updatedFareParams));
          $bookedTicketFareDetails = $this->_invokeGetRequest("getUpdatedFare?BlockKey=SMZKMTqRkf");
          //$bookedTicketFareDetails = $this->_invokePostRequest("getUpdatedFare",json_encode($updatedFareParams));
          print_r($bookedTicketFareDetails); exit;
          $totalFare = $bookedTicketFareDetails['res_data']['updatedFare'];
         */

        $bookingDetails = array(
            'BlockingId'           => $redbusResponse['res_data'],
            'UserWalletID'         => $partyId,
            'RedbusBookingId'      => $request->input('availableTripId'),
            'Source'               => $request->input('source'),
            'Destination'          => $request->input('destination'),
            'JourneyDate'          => $request->input('journyDate'),
            'NoOfBookedSeats'      => $request->input('noOfSeats'),
            'TotalFare'            => $totalFare,
            'BoardingPointId'      => $request->input('boardingPointId'),
            'BoardingPoint'        => $request->input('boardingPoint'),
            'BoardingAddress'      => $request->input('boardingAddress'),
            'DroppingPointId'      => $request->input('droppingPointId'),
            'DroppngPoint'         => $request->input('droppingPoint'),
            'DroppingAddress'      => $request->input('droppingPointAddress'),
            'BoardingTime'         => $request->input('boardingTime'),
            'DroppingTime'         => $request->input('droppingTime'),
            'BoardingLandmark'     => $request->input('boardingLandmark'),
            'DroppingLandmark'     => $request->input('droppingLandmark'),
            'IsCancellable'        => $request->input('isCancellable'),
            'IsPartialCancellable' => $request->input('isPartialCancellable'),
            'CancelStatus'         => 'None',
            'CreatedDateTime'      => date('Y-m-d H:i:s'),
            'CreatedBy'            => 'User',
            'UpdatedDateTime'      => date('Y-m-d H:i:s'),
            'UpdatedBy'            => 'User',
            'BookingStatus'        => 'Blocked',
            'OperatorName'         => $request->input('operatorName'),
            'CancellationPolicy'   => $request->input('cancellationPolicy'),
            'ReturnBooking'        => $request->input('returnTicket'),
            'BusType'              => $request->input('busType')
        );

        $returnBookingId = \App\Models\Bookingmaster::insertGetId($bookingDetails);

        if (!$returnBookingId)
        {
            $output_arr['display_msg'] = array('info' => 'Something went wrong.');
            return response()->json($output_arr);
        }

        $passangerDetails = array();

        foreach ($inventoryItemsJson as $data)
        {
            $passangerDetails[] = array(
                'BookingMasterId'       => $returnBookingId,
                'PassengerName'         => $data['passenger']['name'],
                'MobileNumber'          => $data['passenger']['mobile'],
                'Email'                 => $data['passenger']['email'],
                'Age'                   => $data['passenger']['age'],
                'IdType'                => (isset($data['passenger']['idType']) ? $data['passenger']['idType'] : ''),
                'IdNumber'              => (isset($data['passenger']['idNumber']) ? $data['passenger']['idNumber'] : ''),
                'IsPrimary'             => $data['passenger']['primary'],
                'BookingStaus'          => 'Pending',
                'Gender'                => $data['passenger']['gender'],
                'SeatNum'               => $data['seatName'],
                'BaseFare'              => $data['baseFare'],
                'Fare'                  => $data['fare'],
                'Title'                 => $data['passenger']['title'],
                'OperatorServiceCharge' => $data['operatorServiceCharge'],
                'ServiceFees'           => $data['serviceFees']
            );
        }

        \App\Models\Bookingdetail::insert($passangerDetails);

        $onwardBookingFare = $totalFare;
        $returnBookingFare = '';
        if ($request->input('returnTicket') == 'Y')
        {
            $onwardBookinDetails                = \App\Models\Bookingmaster::find($onwardBookingId);
            $onwardBookinDetails->ReturnBooking = $returnBookingId;
            $onwardBookinDetails->save();
            $onwardBookingFare                  = $onwardBookinDetails->TotalFare;
            $returnBookingFare                  = $totalFare;
        }

        $feeDetails    = $this->feeDetails($partyInfo, $onwardBookingFare, $returnBookingFare);
        $superCashData = \App\Libraries\OfferRules::getSuperCash($partyId, $totalFare);
        if ($superCashData['is_error'] === TRUE)
        {
            $superCash = 0;
        }
        else
        {
            $superCash = $superCashData['res_data']['superCash'];
        }
        $feeDetails['onwardFeeDetails']['superCash'] = $superCash;
        $output_arr['is_error']                      = FALSE;
        $output_arr['res_data']                      = array();
        if ($request->input('returnTicket') == 'Y')
        {
            $bookingDetails['Id']                           = $returnBookingId;
            $output_arr['res_data']['returnBookingDetails'] = array(
                'masterData'       => $bookingDetails,
                'passengerDetails' => $passangerDetails,
                'feeDetails'       => $feeDetails['returnFeeDetails']
            );
            $onwardPassangerDetails                         = \App\Models\Bookingdetail::where('BookingMasterId', '=', $onwardBookingId)->get();
            $output_arr['res_data']['onwardBookingDetails'] = array(
                'masterData'       => $onwardBookinDetails,
                'passengerDetails' => $onwardPassangerDetails,
                'feeDetails'       => $feeDetails['onwardFeeDetails']
            );
        }
        else
        {
            $bookingDetails['Id']                           = $returnBookingId;
            $output_arr['res_data']['onwardBookingDetails'] = array(
                'masterData'       => $bookingDetails,
                'passengerDetails' => $passangerDetails,
                'feeDetails'       => $feeDetails['onwardFeeDetails']
            );
        }

        return response()->json($output_arr);
    }

    /**
     * @desc  Transaction Initiation method is used to ticketgeneration in bus
     * @param partyId type INT
     * @param transactionAmount type float
     * @param onwardWithReturn type Bool
     * @param owTktFareDetails type Object
     * @param returnTktFareDetails type Object
     * return array
     */
    private function transactionInitiation($request, $partyInfo, $feeDetails)
    {
        $output_arr = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());

        $onwardWithReturn = $request->input('onwardWithReturn');
        $onwardBookingId  = $request->input('onwardBookingId');
        $returnBookingId  = $request->input('returnBookingId');
        $onwardFeeDetails = $feeDetails['onwardFeeDetails'];
        $totalAmount      = $onwardFeeDetails['transAmount'];
        if ($onwardWithReturn == 'Y')
        {
            $returnFeeDetails = $feeDetails['returnFeeDetails'];
            $totalAmount      += $returnFeeDetails['transAmount'];
        }
        if ($partyInfo->CurrentAmount < $totalAmount)
        {
            $output_arr['display_msg'] = array('info' => 'You don`t have sufficient balance.');
            return $output_arr;
        }

        // check blockId Exists
        $checkBlockIdExists = \App\Models\Bookingmaster::select('TransactionId')->where('Id', $onwardBookingId)
                ->first();

        if ($checkBlockIdExists['TransactionId'] != '')
        {
            $output                = array('is_error' => TRUE, 'status_code' => 200, 'res_data' => array());
            $output['display_msg'] = array('info' => 'Transaction Already Initiated');
            return $output;
        }



        $entityId       = $request->input('entityId');
        $sesionId       = \App\Libraries\TransctionHelper::getUserSessionId($request);
        $paymentProduct = $feeDetails['paymentProduct'];

        $cgst            = (isset($onwardFeeDetails['CGST']) ? $onwardFeeDetails['CGST'] : 0) + (isset($returnFeeDetails['CGST']) ? $returnFeeDetails['CGST'] : 0);
        $sgst            = (isset($onwardFeeDetails['SGST']) ? $onwardFeeDetails['SGST'] : 0) + (isset($returnFeeDetails['SGST']) ? $returnFeeDetails['SGST'] : 0);
        $igst            = (isset($onwardFeeDetails['IGST']) ? $onwardFeeDetails['IGST'] : 0) + (isset($returnFeeDetails['IGST']) ? $returnFeeDetails['IGST'] : 0);
        $onwardFixFeeAmt = number_format(str_replace('Rs ', '', $onwardFeeDetails['transFixFeeAmt']), 2, '.', '');
        $returnFixFeeAmt = isset($returnFeeDetails['transFixFeeAmt']) ? number_format(str_replace('Rs ', '', $returnFeeDetails['transFixFeeAmt']), 2, '.', '') : 0;
        $transFixFeeAmt  = $onwardFixFeeAmt + $returnFixFeeAmt;
        $amount          = $onwardFeeDetails['amount'] + (isset($returnFeeDetails['amount']) ? $returnFeeDetails['amount'] : 0);
        $taxAmount       = $onwardFeeDetails['taxAmount'] + (isset($returnFeeDetails['taxAmount']) ? $returnFeeDetails['taxAmount'] : 0);
        $feeAmount       = $onwardFeeDetails['feeAmount'] + (isset($returnFeeDetails['feeAmount']) ? $returnFeeDetails['feeAmount'] : 0);
        $totalFeeDetails = array(
            'amount'             => $amount,
            'taxAmount'          => $taxAmount,
            'feeAmount'          => $feeAmount,
            'transAmount'        => $totalAmount,
            'transctionVendor'   => $onwardFeeDetails['transctionVendor'],
            'CGST'               => $cgst,
            'SGST'               => $sgst,
            'IGST'               => $igst,
            'transactionFeePerc' => $onwardFeeDetails['transactionFeePerc'],
            'transFixFeeAmt'     => 'Rs ' . number_format($transFixFeeAmt, 2, '.', ''),
        );

        $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $totalAmount, $totalFeeDetails, $partyInfo, TRUE, 'Pending', $sesionId);

        if ($transRes['error'] == TRUE)
        {
            $output_arr['display_msg'] = array('info' => $transRes['info']);
            return $output_arr;
        }

        $onwardBookingMaster = \App\Models\Bookingmaster::find($onwardBookingId);

        $bookingFareDetails = array();
        if ($onwardWithReturn == 'Y')
        {
            $returnBookingMaster                    = \App\Models\Bookingmaster::find($returnBookingId);
            $returnBookingMaster->TransactionId     = $transRes['response']['TransactionDetails']['TransactionDetailID'];
            $returnBookingMaster->TransactionDate   = date('Y-m-d H:i:s');
            $returnBookingMaster->TransactionStatus = 'Success';
            $returnBookingMaster->save();

            $bookingFareDetails[] = array(
                'BookingMasterId' => $returnBookingId,
                'BaseFare'        => isset($returnFeeDetails['amount']) ? $returnFeeDetails['amount'] : 0,
                'ServiceCharges'  => isset($returnFeeDetails['feeAmount']) ? $returnFeeDetails['feeAmount'] : 0,
                'Tax'             => isset($returnFeeDetails['taxAmount']) ? $returnFeeDetails['taxAmount'] : 0,
                'CreatedDateTime' => date('Y-m-d H:i:s'),
                'CreatedBy'       => 'user',
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'UpdatedBy'       => 'user'
            );
        }

        $onwardBookingMaster->TransactionId     = $transRes['response']['TransactionDetails']['TransactionDetailID'];
        $onwardBookingMaster->TransactionDate   = date('Y-m-d H:i:s');
        $onwardBookingMaster->TransactionStatus = 'Success';
        $onwardBookingMaster->save();

        $transactionDetails                    = \App\Models\Transactiondetail::find($transRes['response']['TransactionDetails']['TransactionDetailID']);
        $transactionDetails->TransactionStatus = 'Success';
        $transactionDetails->save();

        $bookingFareDetails[] = array(
            'BookingMasterId' => $onwardBookingId,
            'BaseFare'        => isset($onwardFeeDetails['amount']) ? $onwardFeeDetails['amount'] : 0,
            'ServiceCharges'  => isset($onwardFeeDetails['feeAmount']) ? $onwardFeeDetails['feeAmount'] : 0,
            'Tax'             => isset($onwardFeeDetails['taxAmount']) ? $onwardFeeDetails['taxAmount'] : 0,
            'CreatedDateTime' => date('Y-m-d H:i:s'),
            'CreatedBy'       => 'user',
            'UpdatedDateTime' => date('Y-m-d H:i:s'),
            'UpdatedBy'       => 'user'
        );

        \App\Models\Bookingfaredetail::insert($bookingFareDetails);
        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = $transRes;

        return $output_arr;
    }

    private function feeDetails($partyInfo, $onwardBookingFare, $returnBookingFare = '')
    {
        $feeDetails        = array();
        $partyId           = $partyInfo->UserWalletID;
        $partyTransLimit   = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $onwardBookingFare);
        $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

        $paymentsProductEventID = 201;
        $entityId               = 1;
        // Payment Product Info
        $paymentProduct         = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductEventID);

        // fee calculation 
        $feeDetails['onwardFeeDetails'] = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $onwardBookingFare, $entityId, $partyId);
        $feeDetails['paymentProduct']   = $paymentProduct;

        if ($returnBookingFare)
        {
            $feeDetails['returnFeeDetails'] = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $returnBookingFare, $entityId, $partyId);
        }

        return $feeDetails;
    }

    /**
     * @desc  confirmTicket method is used to confirm ticket in bus
     * @param partyId type INT
     * @param entityId type INT
     * @param transactionAmount type float
     * @param onwardWithReturn type Bool
     * @param owTktFareDetails type Object
     * @param returnTktFareDetails type Object
     * return array
     */
    public function confirmTicket(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateconfirmTicket($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }

        $partyId          = $request->input('partyId');
        $onwardWithReturn = $request->input('onwardWithReturn');
        $onwardBookingId  = $request->input('onwardBookingId');
        $returnBookingId  = $request->input('returnBookingId');
        $partyInfo        = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        if ($partyInfo === FALSE)
        {
            $output['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output;
        }

        $bookingDetails = \App\Models\Bookingmaster::find($onwardBookingId);
        if (!$bookingDetails)
        {
            $output_arr['display_msg'] = array('bookingId' => 'Please enter valid onward booking ID');
            return response()->json($output_arr);
        }

        $returnBookingFare = '';
        if ($onwardWithReturn == 'Y')
        {
            $returnBookingDetails = \App\Models\Bookingmaster::find($returnBookingId);
            if (!$returnBookingDetails)
            {
                $output_arr['display_msg'] = array('bookingId' => 'Please enter valid return booking ID');
                return response()->json($output_arr);
            }

            $returnBookingFare = $returnBookingDetails->TotalFare;
        }

        if ($returnBookingFare == '')
        {
            $returnBookingFare = 0;
        }

        $bookingFare     = (float) $bookingDetails->TotalFare + (($returnBookingFare) ? (float) $returnBookingFare : 0);
        $mergeData       = array('amount' => $bookingFare);
        $request->merge($mergeData);
        // Verify Promocode 
        $verifyPromocode = \App\Libraries\Helpers::initiateOffer($request);
        if ($verifyPromocode !== TRUE)
        {
            return response()->json($verifyPromocode);
        }

        $feeDetails = $this->feeDetails($partyInfo, $bookingDetails->TotalFare, $returnBookingFare);

        // call transaction Initiation
        $txnInitiaion = $this->transactionInitiation($request, $partyInfo, $feeDetails);

        if ($txnInitiaion['is_error'] == TRUE)
        {
            $this->_bustTicketCommunications($request, $onwardBookingId);
            return response()->json($txnInitiaion);
        }

        $confirmTicketParams = array(
            'availableTripId' => $bookingDetails->RedbusBookingId,
            'boardingPointId' => $bookingDetails->BoardingPointId,
            'destination'     => $bookingDetails->Destination,
            'source'          => $bookingDetails->Source,
        );

        $onwardTicketPassengers = \App\Models\Bookingdetail::where('BookingMasterId', '=', $onwardBookingId)->get();
        foreach ($onwardTicketPassengers as $passengerDetails)
        {
            $confirmTicketParams['inventoryItems'][] = array(
                'fare'       => $passengerDetails->Fare,
                'ladiesSeat' => ($passengerDetails->Gender == 'MALE') ? "false" : "true",
                'passenger'  => array(
                    'address'  => $bookingDetails->PassengerAddress,
                    'age'      => $passengerDetails->Age,
                    'email'    => $passengerDetails->Email,
                    'gender'   => $passengerDetails->Gender,
                    'idNumber' => $passengerDetails->IdNumber,
                    'idType'   => $passengerDetails->IdType,
                    'mobile'   => $passengerDetails->MobileNumber,
                    'name'     => $passengerDetails->PassengerName,
                    'primary'  => ($passengerDetails->IsPrimary == 'Y') ? 'true' : 'false',
                    'title'    => $passengerDetails->Title,
                )
            );
        }
        $onWardTicketResponse = $this->_invokePostRequest("bookticket?blockKey=" . $bookingDetails->BlockingId, json_encode($confirmTicketParams));

        if ($onWardTicketResponse['is_error'] == TRUE)
        {
            $bookingDetails->BookingStatus   = 'Failed';
            $bookingDetails->UpdatedDateTime = date('Y-m-d H:i:s');
            $bookingDetails->save();

            if ($request->input('onwardWithReturn') == 'Y')
            {
                $returnBookingDetails->BookingStatus   = 'Failed';
                $returnBookingDetails->UpdatedDateTime = date('Y-m-d H:i:s');
                $returnBookingDetails->save();
            }

            \App\Models\Bookingdetail::where('BookingMasterId', '=', $bookingDetails->BookingMasterId)->update(['BookingStaus' => 'Failed']);

            return $onWardTicketResponse;
        }
        // 

        $onTicketDetails                   = $this->_invokegetRequest("ticket?tin=" . $onWardTicketResponse['res_data']);
        $onOperatorContactNo               = $onTicketDetails['res_data']->pickUpContactNo;
        $bookingDetails->PnrNum            = $onWardTicketResponse['res_data'];
        $bookingDetails->TicketNum         = $onWardTicketResponse['res_data'];
        $bookingDetails->OperatorContactNo = $onOperatorContactNo;
        $bookingDetails->BookingStatus     = 'Success';
        $bookingDetails->UpdatedDateTime   = date('Y-m-d H:i:s');
        $bookingDetails->save();

        \App\Models\Bookingdetail::where('BookingMasterId', '=', $onwardBookingId)->update(['BookingStaus' => 'Success']);
        $this->_bustTicketCommunications($request, $onwardBookingId);
        $onWardTicketDetails                       = $this->getTicketDetails($onwardBookingId);
        $output_arr['res_data']['ticketDetails'][] = ($onWardTicketDetails) ? $onWardTicketDetails : array('info' => 'No Data');
        $returnStatus                              = 'Success';
        if ($onwardWithReturn == 'Y')
        {
            $returnTicketPassengers = \App\Models\Bookingdetail::where('BookingMasterId', '=', $returnBookingId)->get();
            $confirmTicketParams    = array(
                'availableTripId' => $returnBookingDetails->RedbusBookingId,
                'boardingPointId' => $returnBookingDetails->BoardingPointId,
                'destination'     => $returnBookingDetails->Destination,
                'source'          => $returnBookingDetails->Source,
            );
            foreach ($returnTicketPassengers as $passengerDetails)
            {
                $confirmTicketParams['inventoryItems'][] = array(
                    'fare'       => $passengerDetails->Fare,
                    'ladiesSeat' => ($passengerDetails->Gender == 'MALE') ? "false" : "true",
                    'passenger'  => array(
                        'address'  => $returnBookingDetails->PassengerAddress,
                        'age'      => $passengerDetails->Age,
                        'email'    => $passengerDetails->Email,
                        'gender'   => $passengerDetails->Gender,
                        'idNumber' => $passengerDetails->IdNumber,
                        'idType'   => $passengerDetails->IdType,
                        'mobile'   => $passengerDetails->MobileNumber,
                        'name'     => $passengerDetails->PassengerName,
                        'primary'  => ($passengerDetails->IsPrimary == 'Y') ? 'true' : 'false',
                        'title'    => $passengerDetails->Title,
                    )
                );
            }
            $returnTicketResponse = $this->_invokePostRequest("bookticket?blockKey=" . $returnBookingDetails->BlockingId, json_encode($confirmTicketParams));

            if ($returnTicketResponse['is_error'] == TRUE)
            {
                $returnStatus = 'Failed';
            }
            else
            {
                $returnStatus = 'Success';
                \App\Libraries\Helpers::updateOfferDetails($request, $txnInitiaion['res_data']['response']['TransactionDetails']['TransactionDetailID']);
                $this->_bustTicketCommunications($request, $returnBookingId);
            }

            $reTicketDetails                         = $this->_invokegetRequest("ticket?tin=" . $returnTicketResponse['res_data']);
            $reOperatorContactNo                     = $reTicketDetails['res_data']->pickUpContactNo;
            $returnBookingDetails->PnrNum            = $returnTicketResponse['res_data'];
            $returnBookingDetails->TicketNum         = $returnTicketResponse['res_data'];
            $returnBookingDetails->OperatorContactNo = $reOperatorContactNo;
            $returnBookingDetails->BookingStatus     = $returnStatus;
            $returnBookingDetails->UpdatedDateTime   = date('Y-m-d H:i:s');
            $returnBookingDetails->save();

            \App\Models\Bookingdetail::where('BookingMasterId', '=', $returnBookingId)->update(['BookingStaus' => $returnStatus]);
            $returnTicketDetails                       = $this->getTicketDetails($returnBookingId);
            $output_arr['res_data']['ticketDetails'][] = ($returnTicketDetails) ? $returnTicketDetails : array('info' => 'No Data');
            //return booking communication
        }
        else
        {
            \App\Libraries\Helpers::updateOfferDetails($request, $txnInitiaion['res_data']['response']['TransactionDetails']['TransactionDetailID']);
        }

        $partyBalance                             = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        $output_arr['res_data']['accountBalance'] = array(
            'availableBalance'   => $partyBalance->AvailableBalance,
            'currentAmount'      => $partyBalance->CurrentAmount,
            'TransactionOrderID' => $txnInitiaion['res_data']['response']['TransactionDetails']['TransactionOrderID']
        );
        $output_arr['is_error']                   = FALSE;
        return response()->json($output_arr);
    }

    public function getTicketDetails($bookingId)
    {
        $getBookingsQuery  = $this->getBookingsQuery($bookingId);
        $getBookingDetails = $getBookingsQuery[0];
        if ($getBookingsQuery === FALSE)
        {
            return FALSE;
        }
        $SourcecityName      = $this->getCityName($getBookingDetails['Source']);
        $DestinationcityName = $this->getCityName($getBookingDetails['Destination']);
        $passengerDetails    = $this->getPassengerDetails($bookingId);
        $bookingDetails      = array(
            'bookingId'            => $bookingId,
            'RedbusBookingId'      => $getBookingDetails['RedbusBookingId'],
            'TransactionDetailId'  => $getBookingDetails['TransactionDetailID'],
            'TicketNum'            => $getBookingDetails['TicketNum'],
            'Source'               => ($SourcecityName != '') ? $SourcecityName : '',
            'Destination'          => ($DestinationcityName != '') ? $DestinationcityName : '',
            'JourneyDate'          => $getBookingDetails['JourneyDate'],
            'ticketBookedDate'     => $getBookingDetails['TransactionDate'],
            'NoOfBookedSeats'      => $getBookingDetails['NoOfBookedSeats'],
            'NoOfCancelledSeats'   => $getBookingDetails['NoOfCancelledSeats'],
            'ServiceId'            => $getBookingDetails['ServiceId'],
            'OperatorName'         => $getBookingDetails['OperatorName'],
            'OperatorContactNo'    => $getBookingDetails['OperatorContactNo'],
            'CancelStatus'         => $getBookingDetails['CancelStatus'],
            'BookingStatus'        => $getBookingDetails['BookingStatus'],
            'IsCancellable'        => $getBookingDetails['IsCancellable'],
            'IsPartialCancellable' => $getBookingDetails['IsPartialCancellable'],
            'CancelStatus'         => $getBookingDetails['CancelStatus'],
            'CancellationPolicy'   => $getBookingDetails['CancellationPolicy'],
            'BusType'              => $getBookingDetails['BusType'],
            'boardingDetails'      => array(
                'BoardingPoint'    => $getBookingDetails['BoardingPoint'],
                'BoardingPointId'  => $getBookingDetails['BoardingPointId'],
                'BoardingAddress'  => $getBookingDetails['BoardingAddress'],
                'BoardingTime'     => $getBookingDetails['BoardingTime'],
                'BoardingLandmark' => $getBookingDetails['BoardingLandmark'],
            ),
            'destinationDetails'   => array(
                'DroppingPointId'  => $getBookingDetails['DroppingPointId'],
                'DroppngPoint'     => $getBookingDetails['DroppngPoint'],
                'DroppingAddress'  => $getBookingDetails['DroppingAddress'],
                'DroppingTime'     => $getBookingDetails['DroppingTime'],
                'DroppingLandmark' => $getBookingDetails['DroppingLandmark'],
            ),
            'fareDetails'          => array(
                'TotalFare'      => $getBookingDetails['TotalFare'],
                'BaseFare'       => $getBookingDetails['BaseFare'],
                'ServiceCharges' => $getBookingDetails['ServiceCharges'],
                'Tax'            => $getBookingDetails['Tax'],
                'TollGate'       => $getBookingDetails['TollGate'],
                'ReservChrgs'    => $getBookingDetails['ReservChrgs'],
                'OnlineFee'      => $getBookingDetails['OnlineFee'],
            ),
            'passengerDetails'     => $passengerDetails,
        );

        return $bookingDetails;
    }

    private function updateUserAdminWallet($transactionDetailID, $partyInfo, $adminInfo, $transAmount, $fee, $paymentProduct)
    {
        $transctionStats = array(
            'TransactionStatus' => 'Success',
            'UpdatedBy'         => 'Admin',
            'UpdatedDatetime'   => date('Y-m-d H:i:s')
        );

        \App\Models\Transactiondetail::where('TransactionOrderID', $transactionDetailID)->update($transctionStats);

        // update Party balance
        $updateUserBal                   = \App\Models\Walletaccount::find($partyInfo->WalletAccountId);
        $updateUserBal->AvailableBalance -= $transAmount;
        $updateUserBal->CurrentAmount    -= $transAmount;
        $updateUserBal->save();

        // update Benificiary Balance
        $updateBenficiaryBal                   = \App\Models\Walletaccount::find($adminInfo->WalletAccountId);
        $updateBenficiaryBal->AvailableBalance += $transAmount;
        $updateBenficiaryBal->CurrentAmount    += $transAmount;
        $updateBenficiaryBal->save();

        if (( $paymentProduct->FeeApplicable == 'Y' ) && ( $fee['feeAmount'] != 0 ))
        {
            $feeWalletAcc                   = self::getAdminWalletInfo(2);
            $taxWalletAcc                   = self::getAdminWalletInfo(3);
            // update Fee wallet balance
            $updateFeeBal                   = \App\Models\Walletaccount::find($feeWalletAcc->WalletAccountId);
            $updateFeeBal->AvailableBalance += $fee['feeAmount'];
            $updateFeeBal->CurrentAmount    += $fee['feeAmount'];
            $updateFeeBal->save();

            // update Tax wallet Balance
            $updateTaxBal                   = \App\Models\Walletaccount::find($taxWalletAcc->WalletAccountId);
            $updateTaxBal->AvailableBalance += $fee['taxAmount'];
            $updateTaxBal->CurrentAmount    += $fee['taxAmount'];
            $updateTaxBal->save();
        }
        //current amount update in aggregate table
        \App\Libraries\Statistics::monthlyDebitTransactionsUpdate($partyInfo->UserWalletID, $transAmount);
        \App\Libraries\Statistics::monthlyCreditTransactionsUpdate($adminInfo->UserWalletID, $transAmount);
    }

    public static function getAdminWalletInfo($userId)
    {
        $userExsist = \App\Models\Userwallet::select('userwallet.UserWalletID', 'userwallet.PreferredPrimaryCurrency', 'walletaccount.WalletAccountId', 'walletaccount.AvailableBalance', 'walletaccount.CurrentAmount', 'walletaccount.NonWithdrawAmount', 'walletaccount.HoldAmount')
                ->join('walletaccount', 'walletaccount.UserWalletId', '=', 'userwallet.UserWalletID')
                ->where('userwallet.UserWalletID', $userId)
                ->first();
        return(!$userExsist ) ? FALSE : $userExsist;
    }

    /**
     * @desc  ticketCancellationCharges method is used to cancellation charges details
     * @param bookingId type Int
     * @param partialCancellation type bool
     * return object
     */
    public function ticketCancellationCharges(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateCancelTicket($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }

        $partyId   = $request->input('partyId');
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }

        $bookingId           = $request->input('bookingId');
        $partialCancellation = $request->input('partialCancellation');
        $bookingSeats        = explode(',', $request->input('seatToCancel'));
        $bookingDetails      = \App\Models\Bookingmaster::find($bookingId);
        \Log::debug('booking Info: ' . json_encode($bookingDetails, JSON_PRETTY_PRINT));
        if (!$bookingDetails)
        {
            $output_arr['display_msg'] = array('bookingId' => 'Please enter valid booking ID');
            return response()->json($output_arr);
        }
        if ($bookingDetails->IsCancellable == 'N')
        {
            $output_arr['display_msg'] = array('info' => 'Ticket can`t be cancelled');
            return response()->json($output_arr);
        }
        if (($bookingDetails->IsPartialCancellable == 'N') && ($partialCancellation == 'Y'))
        {
            $output_arr['display_msg'] = array('info' => 'Ticket can`t be cancelled partially');
            return response()->json($output_arr);
        }
        $bookingPassangerDetails = \App\Models\Bookingdetail::where('BookingMasterId', '=', $bookingId)
                        ->whereIn('SeatNum', $bookingSeats)->get();
        if (!$bookingPassangerDetails)
        {
            $output_arr['display_msg'] = array('SeatNum' => 'Please enter valid seat details.');
            return response()->json($output_arr);
        }
        \log::debug("cancellationdata?tin=" . $bookingDetails->TicketNum);
        $tcktCancDetails = $this->_invokeGetRequest("cancellationdata?tin=" . $bookingDetails->TicketNum);
        if ($tcktCancDetails['is_error'] == TRUE)
        {
            $output_arr['display_msg'] = $tcktCancDetails['display_msg'];
            return response()->json($output_arr);
        }
        $seatsFareList       = (array) $tcktCancDetails['res_data']->cancellationCharges;
        $cancellationCharges = 0;

        if (!is_array($seatsFareList['entry']))
        {

            $cancellationCharges += $seatsFareList['entry']->value;
        }
        else if (is_array($seatsFareList['entry']))
        {
            foreach ($seatsFareList['entry'] as $details)
            {
                if (in_array($details->key, $bookingSeats))
                {
                    $cancellationCharges += number_format($details->value, 2, '.', '');
                }
            }
        }

        $totalFare = 0;
        foreach ($bookingPassangerDetails as $seatDetails)
        {
            $totalFare += number_format($seatDetails->Fare, 2, '.', '');
        }
        $totalTicketFare        = $totalFare - $cancellationCharges;
        $transactionDetails     = \App\Models\Transactiondetail::find($bookingDetails->TransactionId);
        $paymentEntityDetails   = \App\Models\Paymententity::where('PaymentEntityName', '=', $transactionDetails->TransactionVendor)->get();
        $paymentsProductEventID = 202;
        $entityId               = $paymentEntityDetails[0]['PaymentEntityID'];
        $partyTransLimit        = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $totalTicketFare);
        $partyWalletPlanId      = isset($partyTransLimit['response']['WalletPlanTypeId']) ? $partyTransLimit['response']['WalletPlanTypeId'] : 1;

        // Payment Product Info
        $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductEventID);

        // fee calculation 
        $fee = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $totalTicketFare, $entityId, $partyId);

        $output_arr['res_data'] = array(
            'bookingId'                => $bookingId,
            'feeDetails'               => $fee,
            'totalCancellationCharges' => $cancellationCharges,
            'totalFare'                => $totalFare,
            'refundAmount'             => $totalTicketFare - ($fee['taxAmount'] + $fee['feeAmount'])
        );
        $output_arr['is_error'] = FALSE;
        return response()->json($output_arr);
    }

    public function cancelTicket(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array('info' => ''), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateCancelTicket($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }

        $partyId   = $request->input('partyId');
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ($partyInfo === FALSE)
        {
            $output_arr['display_msg'] = array('info' => trans('messages.invalidUser'));
            return $output_arr;
        }
        $bookingId           = $request->input('bookingId');
        $partialCancellation = $request->input('partialCancellation');
        $seatToCancel        = explode(',', $request->input('seatToCancel'));

        $bookingDetails = \App\Models\Bookingmaster::find($bookingId);
        if (!$bookingDetails)
        {
            $output_arr['display_msg'] = array('bookingId' => 'Please enter valid booking ID');
            return response()->json($output_arr);
        }
        if ($partialCancellation == 'Y')
        {
            $bookingPassangerDetails = \App\Models\Bookingdetail::where('BookingMasterId', '=', $bookingId)
                            ->where('IsPrimary', '=', 'Y')
                            ->whereIn('SeatNum', $seatToCancel)->get();

            if (count($bookingPassangerDetails) > 0)
            {
                $output_arr['display_msg'] = array('info' => 'Primary Passenger Ticket can`t be cancel');
                return response()->json($output_arr);
            }
        }

        $cancelTicketFareParams   = array('tin' => $bookingDetails->PnrNum, 'seatsToCancel' => $seatToCancel);
        $bookingTicketFareDetails = $this->_invokePostRequest("cancelticket", json_encode($cancelTicketFareParams));

        //\Log::debug('can Res' . json_encode($bookingTicketFareDetails, JSON_PRETTY_PRINT));
        if ($bookingTicketFareDetails['is_error'] == TRUE)
        {
            return response()->json($bookingTicketFareDetails);
        }

        $cancelCharges = $bookingTicketFareDetails['res_data']->cancellationCharge;

        $txnId = $bookingDetails->TransactionId;

        //     $seatToCancel = $request->input('seatToCancel');
        $passengersIds     = array();
        $fares             = array();
        unset($passengersIds);
        unset($fares);
        $getAllPassengerId = $this->getPassengersDetails($bookingId, $seatToCancel);
        \Log::debug('pax info' . json_encode($getAllPassengerId, JSON_PRETTY_PRINT));
        foreach ($getAllPassengerId as $mainPassengerId)
        {
            $passengersIds[] = $mainPassengerId->PassengerId;
            $fares[]         = $mainPassengerId->Fare;
        }

        $implodePassengers = implode(',', $passengersIds);
        if (count($getAllPassengerId) <= 0)
        {
            $output_arr['display_msg'] = array('info' => 'Passenger Not Found With This Seat Number and Booking Id');
            return response()->json($output_arr);
        }

        $cancellationCharges = $cancelCharges;
        $baseFare            = array_sum($fares);
        $finalAmount         = 0;
        if ($baseFare >= $cancellationCharges)
        {
            $finalAmount = $baseFare - $cancellationCharges;
        }

        $orderAmountBus = array(
            'cancellationCharges' => $cancellationCharges, // this is in rupees
            'finalAmount'         => $finalAmount,
        );

        $offerDebitDetails = \App\Libraries\Helpers::debitOfferAmount($txnId, $partyInfo);
        if ($offerDebitDetails)
        {
            $orderAmountBus['offerDetails'] = $offerDebitDetails;
        }

        $refundTRans   = \App\Libraries\RedbusRefund::paymentRefund($txnId, $orderAmountBus);
        \Log::debug('refund Trans' . json_encode($refundTRans, JSON_PRETTY_PRINT));
        $responseSatus = $refundTRans;

        if (!is_array($refundTRans))
        {
            return $refundTRans;
        }
        $updateTicket = array(
            'BookingStaus' => 'Cancelled',
        );
        \App\Models\Bookingdetail::
                whereIn('SeatNum', $seatToCancel)
                ->where('BookingMasterId', $bookingId)
                ->update($updateTicket);

        $insertCancellation = array(
            'BookingMasterId'     => $bookingId,
            'PassengerId'         => $implodePassengers,
            'CancellationDate'    => date('Y-m-d h:m:s'),
            'CancellationCharges' => $cancellationCharges,
            'RefundAmt'           => (!empty($refundTRans['Amount'])) ? $refundTRans['Amount'] : 0,
            'SeatNum'             => $request->input('seatToCancel'),
            'CancellationStatus'  => 'Cancelled',
            'ViolaServiceFee'     => $refundTRans['feeAmount'],
            'CGST'                => $refundTRans['taxAmount'],
            'TransactionId'       => $refundTRans['transactionId'],
        );

        $cancelId = \App\Models\Bookingcancellation::insertGetId($insertCancellation);

        $seatsCount   = count($seatToCancel);
        $statusTicket = $cancelStatus = 'Partial';
        if ($partialCancellation == 'N')
        {
            $cancelStatus = 'Full';
            $statusTicket = 'Cancelled';
        }
        $updatData = array(
            'NoOfCancelledSeats' => $seatsCount,
            'CancelStatus'       => $cancelStatus,
            'BookingStatus'      => $statusTicket
        );
        \App\Models\Bookingmaster::where('Id', $bookingId)->update($updatData);

        // Ticket Details
        $cancellationDetails                      = $this->getTicketDetails($bookingId);
        $partyBalance                             = \App\Libraries\TransctionHelper::_userIdValidate($partyId);
        $output_arr['res_data']['accountBalance'] = array(
            'availableBalance'   => $partyBalance->AvailableBalance,
            'currentAmount'      => $partyBalance->CurrentAmount,
            'TransactionOrderID' => $refundTRans['TransactionOrderID']
        );
        $this->_cancelTicketCommunications($request, $cancelId);

        $output_arr['is_error']                  = FALSE;
        $output_arr['display_msg']               = array('info' => trans('messages.amountReversalSuccess'));
        $output_arr['res_data']['ticketDetails'] = $cancellationDetails;
        return json_encode($output_arr);
    }

    public function getTxnId($bookingId)
    {
        return \App\Models\Bookingmaster::select('TransactionId')
                        ->where('Id', $bookingId)->first();
    }

    public function getPassengersDetails($bookingMasterId, $seatToCancel)
    {
        return $pasngerDet = \App\Models\Bookingdetail::select('Fare', 'PassengerId')
                //  join('bookingfaredetails','bookingfaredetails.BookingMasterId','bookingdetails.BookingMasterId')
                ->whereIn('bookingdetails.SeatNum', $seatToCancel)
                ->where('bookingdetails.BookingMasterId', $bookingMasterId)
                ->get();
    }

    /**
     * @desc In this we Will get all bus bookings list for particular details
     * 
     * @param $partyId type INT
     * return array
     */
    public function getBusBookings(\Illuminate\Http\Request $request)
    {
        $output = array('is_error' => TRUE, 'display_msg' => array('info' => trans('messages.emptyRecords')), 'status_code' => 200, 'res_data' => array());
        $valid  = $this->_validateResendData($request, $type   = 'busbookingsList');
        if ($valid !== TRUE)
        {
            $output['display_msg'] = $valid;
            return response()->json($output);
        }

        $getBookingsQuery = $this->getBookingsQuery($request->input('partyId'), TRUE);
        if ($getBookingsQuery === FALSE)
        {
            $output['is_error']    = FALSE;
            $output['display_msg'] = 'No Details Found';
            return response()->json($output);
        }

        $res_data = array();

        foreach ($getBookingsQuery as $loopgetBookingsQuery)
        {
            $SourcecityName      = $this->getCityName($loopgetBookingsQuery['Source']);
            $DestinationcityName = $this->getCityName($loopgetBookingsQuery['Destination']);
            $bookingId           = $loopgetBookingsQuery['Id'];
            $passengerDetails    = $this->getPassengerDetails($bookingId);

            $res_data[] = array(
                'bookingId'            => $bookingId,
                'RedbusBookingId'      => $loopgetBookingsQuery['RedbusBookingId'],
                'TransactionDetailId'  => $loopgetBookingsQuery['TransactionDetailID'],
                'TransactionOrderID'   => $loopgetBookingsQuery['TransactionOrderID'],
                'TicketNum'            => $loopgetBookingsQuery['TicketNum'],
                'Source'               => ($SourcecityName != '') ? $SourcecityName : '',
                'Destination'          => ($DestinationcityName != '') ? $DestinationcityName : '',
                'JourneyDate'          => $loopgetBookingsQuery['JourneyDate'],
                'ticketBookedDate'     => $loopgetBookingsQuery['TransactionDate'],
                'NoOfBookedSeats'      => $loopgetBookingsQuery['NoOfBookedSeats'],
                'NoOfCancelledSeats'   => $loopgetBookingsQuery['NoOfCancelledSeats'],
                'ServiceId'            => $loopgetBookingsQuery['ServiceId'],
                'OperatorName'         => $loopgetBookingsQuery['OperatorName'],
                'CancelStatus'         => $loopgetBookingsQuery['CancelStatus'],
                'BookingStatus'        => $loopgetBookingsQuery['BookingStatus'],
                'IsCancellable'        => $loopgetBookingsQuery['IsCancellable'],
                'IsPartialCancellable' => $loopgetBookingsQuery['IsPartialCancellable'],
                'CancelStatus'         => $loopgetBookingsQuery['CancelStatus'],
                'CancellationPolicy'   => $loopgetBookingsQuery['CancellationPolicy'],
                'BusType'              => $loopgetBookingsQuery['BusType'],
                'boardingDetails'      => array(
                    'BoardingPoint'    => $loopgetBookingsQuery['BoardingPoint'],
                    'BoardingPointId'  => $loopgetBookingsQuery['BoardingPointId'],
                    'BoardingAddress'  => $loopgetBookingsQuery['BoardingAddress'],
                    'BoardingTime'     => $loopgetBookingsQuery['BoardingTime'],
                    'BoardingLandmark' => $loopgetBookingsQuery['BoardingLandmark'],
                ),
                'destinationDetails'   => array(
                    'DroppingPointId'  => $loopgetBookingsQuery['DroppingPointId'],
                    'DroppngPoint'     => $loopgetBookingsQuery['DroppngPoint'],
                    'DroppingAddress'  => $loopgetBookingsQuery['DroppingAddress'],
                    'DroppingTime'     => $loopgetBookingsQuery['DroppingTime'],
                    'DroppingLandmark' => $loopgetBookingsQuery['DroppingLandmark'],
                ),
                'fareDetails'          => array(
                    'TotalFare'      => $loopgetBookingsQuery['TotalFare'],
                    'BaseFare'       => $loopgetBookingsQuery['BaseFare'],
                    'ServiceCharges' => $loopgetBookingsQuery['ServiceCharges'],
                    'Tax'            => $loopgetBookingsQuery['Tax'],
                    'TollGate'       => $loopgetBookingsQuery['TollGate'],
                    'ReservChrgs'    => $loopgetBookingsQuery['ReservChrgs'],
                    'OnlineFee'      => $loopgetBookingsQuery['OnlineFee'],
                ),
                'passengerDetails'     => $passengerDetails,
            );
        }

        // get all bookings list query joining transaction detail,booking Master,booking details

        $output['is_error']                   = FALSE;
        $output['res_data']['ticketDetails']  = $res_data;
        $output['res_data']['accountBalance'] = (object) array();
        $output['display_msg']['info']        = trans('messages.requestSuccessfull');
        return response()->json($output);
    }

    /**
     * @desc Return data from query for bus booking list
     * 
     * @param $partyId type INT
     * return array
     */
    public function getBookingsQuery($partyId, $partyIdTrue = FALSE)
    {
        $selectArray = array(
            'bookingmaster.Id',
            'bookingmaster.RedbusBookingId',
            'bookingmaster.Source',
            'bookingmaster.Destination',
            'bookingmaster.JourneyDate',
            'bookingmaster.NoOfBookedSeats',
            'bookingmaster.NoOfCancelledSeats',
            'bookingmaster.IsCancellable',
            'bookingmaster.IsPartialCancellable',
            'bookingmaster.CancelStatus',
            'bookingmaster.CancellationPolicy',
            'bookingmaster.BusType',
            'bookingmaster.ServiceId',
            'bookingmaster.TotalFare',
            'bookingmaster.BoardingPoint',
            'bookingmaster.BoardingPointId',
            'bookingmaster.BoardingAddress',
            'bookingmaster.DroppingPointId',
            'bookingmaster.DroppngPoint',
            'bookingmaster.DroppingAddress',
            'bookingmaster.TicketNum',
            'bookingmaster.BoardingTime',
            'bookingmaster.DroppingTime',
            'bookingmaster.BoardingLandmark',
            'bookingmaster.DroppingLandmark',
            'bookingmaster.OperatorName',
            'bookingmaster.OperatorContactNo',
            'bookingmaster.CancelStatus',
            'bookingmaster.BookingStatus',
            'bookingfaredetails.BaseFare',
            'bookingfaredetails.ServiceCharges',
            'bookingfaredetails.Tax',
            'bookingfaredetails.TollGate',
            'bookingfaredetails.ReservChrgs',
            'bookingfaredetails.OnlineFee',
            'transactiondetail.TransactionDetailID',
            'transactiondetail.TransactionOrderID',
            'transactiondetail.TransactionDate',
        );


        if ($partyIdTrue)
        {
            $queryBooking = \App\Models\Transactiondetail::
                            join('bookingmaster', 'bookingmaster.TransactionId', 'transactiondetail.TransactionDetailID')
                            ->join('bookingfaredetails', 'bookingfaredetails.BookingMasterId', 'bookingmaster.Id')
                            ->select($selectArray)->where('bookingmaster.UserWalletId', $partyId)->get();
        }
        else
        {
            $queryBooking = \App\Models\Transactiondetail::
                            join('bookingmaster', 'bookingmaster.TransactionId', 'transactiondetail.TransactionDetailID')
                            ->join('bookingfaredetails', 'bookingfaredetails.BookingMasterId', 'bookingmaster.Id')
                            ->select($selectArray)->where('bookingmaster.Id', $partyId)->get();
        }

        //print_r(\DB::getQueryLog()); exit;

        return (count($queryBooking) > 0) ? $queryBooking : FALSE;
    }

    /**
     * @desc Return City Name for bus bookings list
     * 
     * @param $cityId type INT
     * return string
     */
    public function getCityName($cityId)
    {
        $cityName = \App\Models\Buscity::select('Name')->where('RedBusId', $cityId)->first();
        return $cityName['Name'];
    }

    /**
     * @desc Query for passenger details for bus bookings method
     * 
     * @param $bookingMasterId type INT
     * return string if data exist other wise will return Boolean
     */
    public function getPassengerDetails($bookingMasterId)
    {

        $passengerDet = \App\Models\Bookingdetail::select(
                                'PassengerId', 'PassengerName', 'Age', 'IdType', 'IdNumber', 'BookingStaus', 'Gender', 'SeatNum', 'MobileNumber', 'Email', 'AlternateMobileNumber', 'BaseFare', 'OperatorServiceCharge', 'ServiceFees', 'Fare')
                        ->where('BookingMasterId', $bookingMasterId)->orderBy('PassengerId', 'asc')->get();
        return (count($passengerDet) > 0) ? $passengerDet : FALSE;
    }

    /**
     * @desc In this we Will get all bus bookings list for particular details
     * 
     * @param $partyId type INT
     * return array
     */
    public function resendTicket(\Illuminate\Http\Request $request)
    {
        $partyId = $request->input('partyId');
        $txnId   = $request->input('transactiondetailId');
        $output  = array('is_error' => TRUE, 'display_msg' => array('info' => trans('messages.emptyRecords')), 'status_code' => 200, 'res_data' => array());
        $valid   = $this->_validateResendData($request, $type    = 'resendTicket');
        if ($valid !== TRUE)
        {
            $output['display_msg'] = $valid;
            return response()->json($output);
        }

        $validateMobileorEmail = $this->validate_mobile($request->input('mobileorEmail'));

        $queryBooking = \App\Models\Transactiondetail::
                join('bookingmaster', 'bookingmaster.TransactionId', 'transactiondetail.TransactionDetailID')
                ->join('bookingfaredetails', 'bookingfaredetails.BookingMasterId', 'bookingmaster.Id')
                ->select('bookingmaster.TicketNum', 'bookingmaster.Source', 'bookingmaster.Destination', 'bookingmaster.JourneyDate', 'bookingmaster.ServiceId', 'bookingmaster.TotalFare', 'bookingmaster.Id','bookingmaster.BusType','bookingmaster.BoardingLandmark','bookingmaster.BoardingPoint','bookingmaster.TransactionId')
                ->where('transactiondetail.UserWalletId', $partyId)
                ->where('transactiondetail.TransactionDetailID', $txnId)
                ->first();

        $bookingMasterId = (empty($queryBooking)) ? 0 : $queryBooking['Id'];

        $passengersDetails = $this->getPassengerDetails($bookingMasterId);

        if (empty($passengersDetails))
        {
            $output['is_error']            = TRUE;
            $output['res_data']            = '';
            $output['display_msg']['info'] = 'No Details Found';
            return response()->json($output);
        }

        foreach ($passengersDetails as $mainPassengers)
        {
            $passengersArray[] = $mainPassengers['SeatNum'];
        }

        $implodeDataPassengers = implode(',', $passengersArray);

        if ($validateMobileorEmail == 'mobile')
        {
            $templateData = array(
                'partyId'        => $partyId,
                'linkedTransId'  => '0', // if transction related notification
                'templateKey'    => 'VW073',
                'reciverId'      => '', // if reciver / sender exsists
                'reciverEmail'   => '',
                'reciverMobile'  => '',
                'senderEmail'    => '',
                'senderMobile'   => $request->input('mobileorEmail'),
                'templateParams' => array(// template utalisation parameters
                    'ticketNo'         => $queryBooking['TicketNum'],
                    'from'             => $this->getCityName($queryBooking['Source']),
                    'to'               => $this->getCityName($queryBooking['Destination']),
                    'srvceno'          => $queryBooking['ServiceId'],
                    'passengerDetails' => $implodeDataPassengers,
                    'fare'             => $queryBooking['TotalFare'],
                    'doj'              => $queryBooking['JourneyDate'],
                )
            );
        }
        if ($validateMobileorEmail == 'email')
        {
            $templateData                = array(
                'partyId'        => $partyId,
                'linkedTransId'  => '0', // if transction related notification
                'templateKey'    => 'VW074',
                'reciverId'      => '', // if reciver / sender exsists
                'reciverEmail'   => '',
                'reciverMobile'  => '',
                'senderEmail'    => $request->input('mobileorEmail'),
                'senderMobile'   => '',
                'templateParams' => array(// template utalisation parameters
                    'ticketNo'         => $queryBooking['TicketNum'],
                    'from'             => $this->getCityName($queryBooking['Source']),
                    'to'               => $this->getCityName($queryBooking['Destination']),
                    'srvceno'          => $queryBooking['ServiceId'],
                    'passengerDetails' => $implodeDataPassengers,
                    'fare'             => $queryBooking['TotalFare'],
                    'doj'              => $queryBooking['JourneyDate'],
                )
            );
            $templateData['senderEmail'] = $request->input('mobileorEmail');
        }
        if(isset($passengersDetails[0]['PassengerName'])){
        $passangerNameExplode = explode(' ', $passengersDetails[0]['PassengerName']);   
        $passangerName = isset($passangerNameExplode[0]) ? $passangerNameExplode[0] : $passengersDetails[0]['PassengerName'];
        }else{
        $passangerName = '';    
        }
        
        $templateData['templateParams']['fullName'] = $passangerName;
        $templateData['templateParams']['boardingPointLocation'] = $queryBooking['BoardingPoint'];
        $templateData['templateParams']['landmark'] = $queryBooking['BoardingLandmark'];
        $templateData['templateParams']['busType'] = $queryBooking['BusType'];
        $encodeString = 'partyId='.$partyId.'&transactiondetailId='.$queryBooking['TransactionId'].'&PnrNum='.$queryBooking['TicketNum'];
        $pdfUrlEncode = urlencode(base64_encode($encodeString));
        $templateData['templateParams']['printUrl'] = url().'/generate-pdf/'.$pdfUrlEncode;
        $notidId = \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);

        // get all bookings list query joining transaction detail,booking Master,booking details
        if ($notidId)
        {
            $output['is_error']            = FALSE;
            $output['res_data']            = '';
            $output['display_msg']['info'] = 'Resend Request Processed Successfully';
            return response()->json($output);
        }

        $output['is_error']            = TRUE;
        $output['res_data']            = '';
        $output['display_msg']['info'] = 'Resend Request Failed';
        return response()->json($output);
    }

    /**
     * @desc Validate request data for bus bookings
     * @param $partyId type INT
     * @return array
     */
    private function _validateResendData($request, $type)
    {
        $validate['partyId'] = 'required';
        $messages            = array();
        if ($type == 'resendTicket')
        {
            $validate['transactiondetailId'] = 'required|numeric';
            $validate['mobileorEmail']       = 'required';
            $validateMobileorEmail           = $this->validate_mobile($request->input('mobileorEmail'));
            if ($validateMobileorEmail == 'mobile')
            {
                $validate['mobileorEmail']       = 'regex:/^[0-9]{10}+$/';
                $messages['mobileorEmail.regex'] = 'Enter a Valid Mobile Number';
            }
            if ($validateMobileorEmail == 'email')
            {
                $validate['mobileorEmail']       = 'email';
                $messages['mobileorEmail.email'] = 'Enter a Valid Email';
            }
        }


        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /**
     * @desc Validate request data for Trips 
     * @param $fromCityId type INT
     * @param $toCityId type INT
     * @param $journyDate type string
     * @return array
     */
    private function _validateTrips($request)
    {
        $validate = array(
            'fromCityId' => 'required|numeric',
            'journyDate' => 'required',
        );

        return $this->_validationResponse($request, $validate);
    }

    /**
     * @desc Validate request data for Bus Layout 
     * @param $tripId type INT
     * @return array
     */
    private function _validateBusLayout($request)
    {
        $validate = array(
            'tripId' => 'required|numeric',
        );
        return $this->_validationResponse($request, $validate);
    }

    /**
     * @desc Validate request data for Ticket Initiation 
     * @param $availableTripId type INT
     * @param $inventoryItems type INT
     * @return array
     */
    private function _validateBlockTicket($request)
    {
        $validate = array(
            'availableTripId'      => 'required',
            'inventoryItems'       => 'required',
            'source'               => 'required|numeric',
            'destination'          => 'required|numeric',
            'boardingPointId'      => 'numeric',
            'partyId'              => 'required|numeric',
            'journyDate'           => 'required',
            'noOfSeats'            => 'required|numeric',
            'boardingPoint'        => 'required',
            'boardingAddress'      => 'required',
            'droppingPointId'      => 'numeric',
            'droppingPoint'        => 'required',
            'droppingPointAddress' => 'required',
            'boardingTime'         => 'required',
            'droppingTime'         => 'required',
            'boardingLandmark'     => 'required',
            'droppingLandmark'     => 'required',
            'isCancellable'        => 'required',
            'isPartialCancellable' => 'required',
            'operatorName'         => 'required',
        );
        return $this->_validationResponse($request, $validate);
    }

    private function _validateTicketIntiation($request)
    {
        $validate = array(
            'partyId'           => 'required|numeric',
            'owTktFareDetails'  => 'required',
            'onwardWithReturn'  => 'required',
            'transactionAmount' => 'required',
            'entityId'          => 'required|numeric'
        );
        return $this->_validationResponse($request, $validate);
    }

    private function _validationResponse($request, $validate)
    {
        $validate_response = \Validator::make($request->all(), $validate);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    private function _validateCancelTicket($request)
    {
        $validate['bookingId']           = 'required|numeric';
        $validate['partialCancellation'] = 'required|in:Y,N';
        $validate['seatToCancel']        = 'required';
        return $this->_validationResponse($request, $validate);
    }

    public function validate_mobile($mobileEmail)
    {
        if (is_numeric($mobileEmail))
        {
            return 'mobile';
        }
        else
        {

            return 'email';
        }
    }

    private function _validateconfirmTicket($request)
    {
        $validate = array(
            'partyId'          => 'required|numeric',
            'onwardBookingId'  => 'required|numeric',
            'entityId'         => 'required|numeric',
            'onwardWithReturn' => 'required|in:Y,N',
        );
        if ($request->input('onwardWithReturn') == 'Y')
        {
            $validate['returnBookingId'] = 'required|numeric';
        }
        return $this->_validationResponse($request, $validate);
    }

    public function getBpDpList(\Illuminate\Http\Request $request)
    {
        $output_arr      = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());
        $validateRequest = $this->_validateTrips($request);
        if ($validateRequest !== TRUE)
        {
            $output_arr['display_msg'] = $validateRequest;
            return response()->json($output_arr);
        }
        $sourceId      = $request->input('fromCityId');
        $destinationId = $request->input('toCityId');
        $date          = $request->input('journyDate');
        $bustList      = $this->_invokeGetRequest("availabletrips?source=" . $sourceId . "&destination=" . $destinationId . "&doj=" . $date);

        if ($bustList['is_error'] == TRUE)
        {
            return response()->json($bustList);
        }

        $areas          = array();
        $boardingPoints = array();
        $droppingPoints = array();
        $operators      = array();
        if (!isset($bustList['res_data']->availableTrips))
        {
            $output_arr['is_error']    = FALSE;
            $output_arr['display_msg'] = array('info' => 'No data');
            return response()->json($output_arr);
        }
        $busListData = (is_array($bustList['res_data']->availableTrips)) ? $bustList['res_data']->availableTrips : (array($bustList['res_data']->availableTrips));

        foreach ($busListData as $details)
        {
            if (isset($details->boardingTimes) && !is_array($details->boardingTimes))
            {
                $boardingPoints[] = (array) $details->boardingTimes;
            }

            if (isset($details->boardingTimes) && is_array($details->boardingTimes))
            {
                foreach ($details->boardingTimes as $boardings)
                {
                    $boardingPoints[] = (array) $boardings;
                }
            }

            if (isset($details->droppingTimes) && !is_array($details->droppingTimes))
            {
                $droppingPoints[] = (array) $details->droppingTimes;
            }

            if (isset($details->droppingTimes) && is_array($details->droppingTimes))
            {
                foreach ($details->droppingTimes as $droppings)
                {
                    $droppingPoints[] = (array) $droppings;
                }
            }

            $operators[] = $details->travels;
        }
        $boardingArr             = array_unique(array_column($boardingPoints, 'bpName'));
        $areas['boardingPoints'] = array_intersect_key($boardingPoints, $boardingArr);

        $droppingArr             = array_unique(array_column($droppingPoints, 'bpName'));
        $areas['droppingPoints'] = array_intersect_key($droppingPoints, $droppingArr);

        $output_arr['is_error'] = FALSE;
        $output_arr['res_data'] = array('boardingPoints' => array_values($areas['boardingPoints']), 'droppingPoints' => array_values($areas['droppingPoints']), 'operators' => array_values(array_unique($operators)));
        return response()->json($output_arr);
    }

    public function ticketDetails()
    {
        $confirmTicketDetails = $this->_invokegetRequest("ticket?tin=53ME2PB8");
        print_r($confirmTicketDetails);
    }

    public function cancelTicketDetails()
    {
        $confirmTicketDetails = $this->_invokePostRequest("cancelticket", '{"tin":"43B3Z7B6","seatsToCancel":["2"]}');
        //$updateFareDetails = $this->_invokeGetRequest('getUpdatedFare?BlockKey=m6YefvUJyE');
        print_r($confirmTicketDetails);
    }

    private static function _bustTicketCommunications($request, $bookingId)
    {
        $templateData    = [];
        $partyId         = $request->input('partyId');
        $bookingDetails  = \App\Models\Bookingmaster::find($bookingId);
        $returnBookingId = $bookingDetails->ReturnBooking;
        $date            = date('m/d/Y', strtotime($bookingDetails->CreatedDateTime));
        $time            = date('h:m A', strtotime($bookingDetails->CreatedDateTime));
        if ($returnBookingId > 0)
        {
            $returnBookingDetails = \App\Models\Bookingmaster::find($returnBookingId);
        }

        $userDetails = \App\Models\Userwallet::select('userwallet.FirstName', 'userwallet.LastName', 'userwallet.EmailID', 'userwallet.MobileNumber')
                ->where('userwallet.UserWalletID', $partyId)
                ->first();

        $senderName = $userDetails->FirstName . ' ' . $userDetails->LastName;
        if ($bookingDetails->TransactionStatus == 'Success')
        {
            $templateData = [
                'partyId'        => $partyId,
                'linkedTransId'  => '0', // if transction related notification
                'templateKey'    => 'VW075',
                'senderEmail'    => $userDetails->EmailID,
                'senderMobile'   => $userDetails->MobileNumber,
                'reciverId'      => '', // if reciver / sender exsists
                'reciverEmail'   => '',
                'reciverMobile'  => '',
                'templateParams' => array(// template utalisation parameters
                    'primaryName'        => $senderName,
                    'cardnumber'         => 'XXXX',
                    'amount'             => $bookingDetails->TotalFare,
                    'totalamount'        => $bookingDetails->TotalFare,
                    'onwardTicket'       => $bookingDetails->TicketNum,
                    'frombusType'        => $bookingDetails->BusType,
                    'onwardoperatorName' => $bookingDetails->OperatorName,
                    'fromseatNumbers'    => $bookingDetails->NoOfBookedSeats,
                    'fromserviceNumber'  => $bookingDetails->ServiceId,
                    'boardingFrom'       => $bookingDetails->BoardingPoint . ',  ' . $bookingDetails->BoardingAddress,
                    'fromCity'           => $bookingDetails->BoardingPoint . ',  ' . $bookingDetails->BoardingAddress,
                    'toCity'             => $bookingDetails->DroppngPoint . ',  ' . $bookingDetails->DroppingAddress,
                    'boardingFromPoint'  => $bookingDetails->BoardingPoint,
                    'journeyDateFrom'    => $bookingDetails->JourneyDate,
                    'boardingTime'       => $bookingDetails->BoardingTime . ' ' . $bookingDetails->JourneyDate,
                    'returnTiket'        => 'N',
                    'date'               => $date,
                    'time'               => $time,
                )
            ];
            if ($returnBookingId > 0)
            {
                $templateData['templateParams']['returnTiket']        = 'Y';
                $templateData['templateParams']['journeyDateTo']      = $returnBookingDetails->JourneyDate;
                $templateData['templateParams']['returnTicketNumber'] = $returnBookingDetails->TicketNum;
                $templateData['templateParams']['tobusType']          = $returnBookingDetails->BusType;
                $templateData['templateParams']['returnboardingFrom'] = $returnBookingDetails->BoardingPoint;
                $templateData['templateParams']['returnseatNumbers']  = $returnBookingDetails->NoOfBookedSeats;
            }
        }

        if ($bookingDetails->TransactionStatus == 'Failed')
        {
            $templateData = [
                'partyId'        => $partyId,
                'linkedTransId'  => '0', // if transction related notification
                'templateKey'    => 'VW025',
                'senderEmail'    => $userDetails->EmailID,
                'senderMobile'   => $userDetails->MobileNumber,
                'reciverId'      => '', // if reciver / sender exsists
                'reciverEmail'   => '',
                'reciverMobile'  => '',
                'templateParams' => array(// template utalisation parameters
                    'customerName'       => $senderName,
                    'cardnumber'         => '',
                    'amount'             => $bookingDetails->TotalFare,
                    'bookingId'          => $bookingDetails->TicketNum,
                    'busServiceprovider' => $bookingDetails->OperatorName,
                    'busOperator'        => $bookingDetails->OperatorName,
                    'referenceNum'       => $bookingDetails->RedbusBookingId,
                    'placeOne'           => $bookingDetails->BoardingPoint . ',  ' . $bookingDetails->BoardingAddress,
                    'placeTwo'           => $bookingDetails->DroppngPoint . ',  ' . $bookingDetails->DroppingAddress,
                    'date'               => $date,
                    'time'               => $time,
                    'supportURL'         => "violamoney.com",
                )
            ];
        }
        if ($templateData)
        {
            \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
        }
        return TRUE;
        // End Communication
    }

    private static function _cancelTicketCommunications($request, $cancelId)
    {

        $partyId          = $request->input('partyId');
        $cancellationData = \App\Models\Bookingcancellation::find($cancelId);
        $bookingDetails   = \App\Models\Bookingmaster::find($cancellationData->BookingMasterId);

        $date = date('m/d/Y', strtotime($cancellationData->CreatedDateTime));
        $time = date('h:m A', strtotime($cancellationData->CreatedDateTime));

        $userDetails = \App\Models\Userwallet::select('userwallet.FirstName', 'userwallet.LastName', 'userwallet.EmailID', 'userwallet.MobileNumber')
                ->where('userwallet.UserWalletID', $partyId)
                ->first();

        $senderName = $userDetails->FirstName . ' ' . $userDetails->LastName;

        $templateData = [
            'partyId'        => $partyId,
            'linkedTransId'  => '0', // if transction related notification
            'templateKey'    => 'VW026',
            'senderEmail'    => $userDetails->EmailID,
            'senderMobile'   => $userDetails->MobileNumber,
            'reciverId'      => '', // if reciver / sender exsists
            'reciverEmail'   => '',
            'reciverMobile'  => '',
            'templateParams' => array(// template utalisation parameters
                'primaryName'         => $senderName,
                'refundAmt'           => $cancellationData->RefundAmt,
                'cancellationCharges' => $cancellationData->CancellationCharges,
                'cancellationFare'    => $cancellationData->CancellationFare,
                'seatNumbers'         => $cancellationData->SeatNum,
                'operator'            => $bookingDetails->OperatorName,
                'servicenumber'       => $bookingDetails->BusType,
                'orginalAmount'       => $bookingDetails->TotalFare,
                'date'                => $date,
                'time'                => $time,
            )
        ];
        \App\Libraries\Notifications\NotificationHelper::sendNotifications($request, $templateData);
    }

}
