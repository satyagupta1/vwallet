<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactioneventlog
 * 
 * @property int $TransactionEventDetailID
 * @property int $TransactionDetailID
 * @property int $PaymentsProductID
 * @property \Carbon\Carbon $EventDate
 * @property string $Comments
 * @property string $EventCode
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 *
 * @package App\Models
 */
class Feedback extends Eloquent
{
	protected $table = 'feedback';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'Id' => 'int',
		'TransactionID' => 'varchar',
		'Ratings' => 'smallint',
                'Comments' => 'text',
                'FeedBackType' => 'varchar',
                'TransactionModule' => 'varchar',
                'CreatedDateTime' => 'datetime',
                'CreatedBy' => 'varchar',
	];

	protected $dates = [
		'CreatedDateTime',
	];

	protected $fillable = [
		'TransactionID',
		'Ratings',
		'Comments',
		'FeedBackType',
		'TransactionModule',
		'CreatedDateTime',
		'CreatedBy',
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID')
					->where('transactiondetail.TransactionDetailID', '=', 'Feedback.TransactionDetailID');
	}
}
