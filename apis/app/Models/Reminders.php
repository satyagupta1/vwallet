<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 22 Feb 2018 11:07:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Reminder
 * 
 * @property int $ReminderId
 * @property int $PartyId
 * @property string $Title
 * @property string $Description
 * @property \Carbon\Carbon $DateTime
 * @property string $NotificationCycleType
 * @property int $NotificationCyclePeriod
 * @property \Carbon\Carbon $NextExectionDateTime
 * @property string $Status
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \Illuminate\Database\Eloquent\Collection $remindernotifications
 * @property \Illuminate\Database\Eloquent\Collection $usertransactionreminders
 *
 * @package App\Models
 */
class Reminders extends Eloquent {

	protected $primaryKey = 'ReminderId';
	public $timestamps = false;

	protected $casts = [
		'PartyId' => 'int',
		'RemindTransactionId' => 'int',
		'NotificationCyclePeriod' => 'int'
	];

	protected $dates = [
		'NextExectionDateTime',
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyId',
		'RemindTransactionId',		
		'NotificationCycleType',
		'NotificationCyclePeriod',
		'NextExectionDateTime',
		'ReminderType',
		'Status',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyId');
	}

	public function remindernotifications()
	{
		return $this->hasMany(\App\Models\Remindernotification::class, 'RemiderId');
	}

	public function userreminders()
	{
		return $this->hasMany(\App\Models\Userreminder::class, 'ReminderId');
	}
}
