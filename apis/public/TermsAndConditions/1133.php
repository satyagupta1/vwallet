<?php include 'header.php';?>
<div>
    <h3>Terms Conditions</h3>
    <ul>
       <li>Get upto 10% discount to a max of INR 100 on Bus Tickets booked using Viola Wallet.</li>
        <li>The code can be redeemed only 3 times per user.</li>
        <li>Offer is valid till 30th April, 2018</li>
        <li>Limited period offer.</li>
        <li>The offer is applicable on Bus Booking only.</li>
    </ul> 
</div>
<?php include 'footer.php';?>