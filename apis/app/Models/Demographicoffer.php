<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 23 Jan 2018 12:03:32 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Demographicoffer
 * 
 * @property int $DemoGraphicOffersId
 * @property int $OfferBusinessID
 * @property string $ActiveInd
 * @property string $Key
 * @property string $Value
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Promotionaloffer $promotionaloffer
 *
 * @package App\Models
 */
class Demographicoffer extends Eloquent
{
	protected $primaryKey = 'DemoGraphicOffersId';
	public $timestamps = false;

	protected $casts = [
		'OfferBusinessID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'OfferBusinessID',
		'ActiveInd',
		'Key',
		'Value',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function promotionaloffer()
	{
		return $this->belongsTo(\App\Models\Promotionaloffer::class, 'OfferBusinessID', 'OfferBusinessID');
	}
}
