<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Dth Recharge Fails
 * 
 *  param $amount decimal
 *  param $balanceamount decimal
 *  param $rechargedate Date
 *  param $date Date
 *  param $time time
 *  param $rechargeid varchar
 *  param $operator varchar
 *  param $offeramount int
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Payment Declined',
        'text'    => $salutationName . ',<br/><br/>
Sorry, your DTH recharge of  Rs ' . $amount . ' failed. Your transaction details are given below<br/><br/>
Recharge Date: ' . $rechargedate . '<br/>
Time: ' . $time . '<br/>
Recharge ID: ' . $rechargeid . '<br/>
Operator: ' . $operator . '<br/>
Offer Amount: ' . $offeramount . '<br/><br/>
In case money has been deducted from your wallet, rest assured that your money is safe and will be refunded automatically and should reflect in your wallet/card/account within 10 days depending on your bank.<br/><br/>
Kindly check with your operator or try again later.'],
    'sms'   => [
        'text' => 'Hello, DTH Recharge of INR ' . $amount . ' declined due to <insufficient funds/ Network error/ no response from operator) on ' . $date . ' at ' . $time . '. Balance amount' . $balanceamount,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'DTH recharge declined',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'DTH recharge of INR ' . $amount . '  was declined',
    ],
];

echo json_encode($array);

/* End of file dth_recharge_fails */
