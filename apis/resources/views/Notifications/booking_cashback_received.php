<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback Received device template
 *  param $fullname varchar
 *  param $amount varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Enjoy your cashback!',
        'text'    => $salutationName . ',<br/><br/>
Great news- you received cashback of Rs '.$amount.' on your Movie Tickets.<br/>
Please refer your friends and help them receive cashbacks. You also earn more cashbacks on each referral.<br/>
Invite Friends Now.',
    ],
    'sms'   => [
        'text' => $amount.' % cashback received on Movie Ticket Booking! Apply Promo Code RCFC',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cashback on Movie Ticket received',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'offers'
    ],
    'web'   => [
        'text' => 'Cashback on Movie Ticket  being processed.',
    ],
];

echo json_encode($array);

/* End of file cashback_received.php */
