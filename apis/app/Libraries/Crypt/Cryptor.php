<?php
/**
 * Cryptor class
 * @category  Encryptioin
 * @package   Viola Card
 * @author    Viola Services (India) PVT LTD
 * @license   https://www.v-card.com Licence
 */

namespace App\Libraries\Crypt;

use \stdClass;

require_once __DIR__ . '/functions.php';

class Cryptor
{
    const DEFAULT_SCHEMA_VERSION = 3;

    static protected $config;

    /**
    * Generator key is the method which we are using to generating key for the encyrption the string.
    * @param string $salt = salt to generate key
    * @param string $password
    * @param string $verson = for which version we need to generate the encryption key.
    *
    * @return string generated key.
    */
    public static function generateKey($salt, $password, $version = self::DEFAULT_SCHEMA_VERSION)
    {
        Cryptor::configure($version);
        return Cryptor::makeKey($salt, $password);
    }

    /**
    * aesCtrLittleEndianCrypt.
    *
    * @param string $payload
    * @param string $key
    * @param string $iv
    *
    * @return string
    */
    protected static function aesCtrLittleEndianCrypt($payload, $key, $iv)
    {
        $numOfBlocks = ceil(strlen($payload) / strlen($iv));
        $counter = '';
        for ($i = 0; $i < $numOfBlocks; ++$i) {
            $counter .= $iv;

            // Yes, the next line only ever increments the first character
            // of the counter string, ignoring overflow conditions.  This
            // matches CommonCrypto's behavior!
            $iv[0] = chr(ord($iv[0]) + 1);
        }

        return $payload ^ Cryptor::encryptInternal($key, $counter, 'ecb');
    }

    /**
    * encryptInternal.
    *
    * @param string $key
    * @param string $payload
    * @param string $mode
    * @param bool $iv
    *
    * @return string
    */
    protected static function encryptInternal($key, $payload, $mode, $iv = null)
    {
        return openssl_encrypt($payload, self::$config->algorithm . $mode, $key, OPENSSL_RAW_DATA, (string)$iv);
    }

    /**
    * encryptInternal.
    *
    * @param object $components
    * @param string $hmacKey
    *
    * @return string
    */
    protected static function makeHmac(stdClass $components, $hmacKey)
    {
        $hmacMessage = '';
        if (self::$config->hmac->includesHeader) {
            $hmacMessage .= ''
                . $components->headers->version
                . $components->headers->options
                . (isset($components->headers->encSalt) ? $components->headers->encSalt : '')
                . (isset($components->headers->hmacSalt) ? $components->headers->hmacSalt : '')
                . $components->headers->iv;
        }

        $hmacMessage .= $components->ciphertext;

        $hmac = hash_hmac(self::$config->hmac->algorithm, $hmacMessage, $hmacKey, true);

        if (self::$config->hmac->includesPadding) {
            $hmac = str_pad($hmac, self::$config->hmac->length, chr(0));
        }

        return $hmac;
    }

    protected static function makeKey($salt, $password)
    {
        if (self::$config->truncatesMultibytePasswords) {
            $utf8Length = mb_strlen($password, 'utf-8');
            $password = substr($password, 0, $utf8Length);
        }

        $algo = self::$config->pbkdf2->prf;
        $iterations = self::$config->pbkdf2->iterations;
        $length = self::$config->pbkdf2->keyLength;

        return hash_pbkdf2($algo, $password, $salt, $iterations, $length, true);
    }

    protected static function configure($version)
    {
        $config = new stdClass;

        $config->algorithm = 'aes-256-';
        $config->saltLength = 8;
        $config->ivLength = 16;

        $config->pbkdf2 = new stdClass;
        $config->pbkdf2->prf = 'sha1';
        $config->pbkdf2->iterations = 10000;
        $config->pbkdf2->keyLength = 32;

        $config->hmac = new stdClass();
        $config->hmac->length = 32;

        if (!$version) {
            Cryptor::configureVersionZero($config);
        } elseif ($version <= 3) {
            $config->mode = 'cbc';
            $config->options = 1;
            $config->hmac->algorithm = 'sha256';
            $config->hmac->includesPadding = false;

            switch ($version) {
                case 1:
                    $config->hmac->includesHeader = false;
                    $config->truncatesMultibytePasswords = true;
                    break;

                case 2:
                    $config->hmac->includesHeader = true;
                    $config->truncatesMultibytePasswords = true;
                    break;

                case 3:
                    $config->hmac->includesHeader = true;
                    $config->truncatesMultibytePasswords = false;
                    break;
            }
        } else {
            // throw new \RuntimeException('Unsupported schema version ' . $version);
            return 'Unsupported schema version ' . $version;
        }

        self::$config = $config;
    }

    private static function configureVersionZero(stdClass $config)
    {
        $config->mode = 'ctr';
        $config->options = 0;
        $config->hmac->includesHeader = false;
        $config->hmac->algorithm = 'sha1';
        $config->hmac->includesPadding = true;
        $config->truncatesMultibytePasswords = true;
    }
}
