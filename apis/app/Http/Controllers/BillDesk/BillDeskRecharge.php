<?php

namespace App\Http\Controllers\BillDesk;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class BillDeskRecharge extends \App\Http\Controllers\Controller {

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function __construct()
    {
        $this->encKey = sha1(config('vwallet.ENCRYPT_STATIC'));
    }
    
    /**
     * This used to call validationCheck_wallet method in this listWallets method
     * @desc In this we will check validationCheck_wallet method for validation of party id and device type
     * @desc If all validations have no errors will call _getPartyWalletAccounts and _getPartyExternalCards methods to get list of all wallets details and card details in array list response
     * 
     * @param PartyId integer
     *  
     * @return Array
     */
    // code for reversal transaction of add topup
    public function BillDeskRecharge(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );


        // check validation
        $chk_valid = $this->_ValidationBilldeskRecharge($request);
        if ( $chk_valid !== TRUE )
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        // get walletAccountId from transactionDetail table
        // check wallet exists with correct user or not
        // update user wallet balance starts
        $partyId           = $request->input('partyId');
        $amount            = $request->input('amount');
        $entityId          = $request->input('businessModelid');
        $transStaus        = $request->input('status');
        //trans_type whether it is WW or WB
        $paymentsProductEventID = $request->input('prodEventId');
        $mainProdId = 6;
        $bankRefId = $request->input('bankRefId');
        $payementId = $request->input('paymentId');

       

        // get Logged in User data if Exists.
        $partyInfo = \App\Libraries\TransctionHelper::_userIdValidate($partyId);

        if ( $partyInfo === FALSE )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidUser') );
            return $output_arr;
        }
        
        // session Check
         $sesionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
//        if (is_bool($sesionId))
//        {
//            $output_arr['display_msg'] = array('info' => trans('messages.invalidSession'));
//            return $output_arr;
//        }


        // session Check
        //$sessionId = \App\Libraries\TransctionHelper::getUserSessionId($request);
//        if ( is_bool($sessionId) )
//        {
//            $output_arr['display_msg'] = array( 'info' => trans('messages.invalidSession') );
//            return $output_arr;
//        }
        // check Balance
        if ( $partyInfo->CurrentAmount < $amount )
        {
            $output_arr['display_msg'] = array( 'info' => trans('messages.notEnoughBalance') );
            return $output_arr;
        }

        // code for success case        
        if ( $transStaus == 'Success' )
        {
             if($request->input('offerData')!=NULL) {
                $offerData = $request->input('offerData');
                $pamentInfo = \App\Libraries\OfferRules::getBenifitPaymentInfo($offerData);                
                if(!empty($pamentInfo)) {
                    $offerPamentInfo = array('offerPamentInfo' => $pamentInfo);
                    $request->request->add($offerPamentInfo);
                }
            }
            if($request->input('superCash')>0) {
                $requestArray = array('partyId' => $partyId, 'amount' => $amount);
                $superCashPaymentInfo = \App\Libraries\OfferRules::getSuperCashPaymentInfo($requestArray);                              
                if(!empty($superCashPaymentInfo)) {
                    $superCashInput = array('superCashPamentInfo' => $superCashPaymentInfo);
                    $request->request->add($superCashInput);
                }
            }
            
       
            $partyTransLimit   = \App\Libraries\TransctionHelper::userTransctionLimitChecker($partyId, $partyInfo->WalletPlanTypeId, $amount);
            $partyWalletPlanId = $partyTransLimit['response']['WalletPlanTypeId'];

            // Payment Product Info
            $paymentProduct = \App\Libraries\TransctionHelper::getPaymentProductEventInfo($paymentsProductEventID);

            // fee calculation 
            $fee      = \App\Libraries\TransctionHelper::addFeeForTransction($paymentProduct, $partyWalletPlanId, $amount, $entityId, $partyId);
            

            $transRes = \App\Libraries\TransctionHelper::transctionLevelOne($request, $entityId, $paymentProduct, $amount, $fee, $partyInfo, TRUE, $transStaus, $sesionId);
            $orderId  = $transRes['response']['TransactionDetails']['TransactionOrderID'];

            //\DB::enableQueryLog();
            $updateRefIdMain                  = \App\Models\Transactiondetail::select('TransactionDetailID', 'PaymentsProductID', '')->where('TransactionOrderID', $orderId)->first();
            //    print_r(\DB::getQueryLog());
            $updateTxnDetails                 = \App\Models\Transactiondetail::find($updateRefIdMain->TransactionDetailID);
            $updateTxnDetails->RespPGTrackNum = ( ! empty($payementId)) ? $payementId : 0;
            $updateTxnDetails->RespBankRefNum = ( ! empty($bankRefId)) ? $bankRefId : 0;
            $updateTxnDetails->save();

            // insertion in user recharge details
            if($request->input('event') == 'RECPRP')
            {
           $this->insertRechargeDetails($partyId, $updateRefIdMain->TransactionDetailID, $request, 'Success');
            }

            if ( $transRes['error'] )
            {
                $output_arr['display_msg'] = array( 'info' => $transRes['info']);
                return $output_arr;
            }
            $request->request->add([ 'transactionId' => $updateRefIdMain->TransactionDetailID ]); 
            $request->request->add([ 'productId' => $updateRefIdMain->PaymentsProductID ]); 
            $request->request->add([ 'transactionAmount' => $updateRefIdMain->Amounts ]); 
            \App\Libraries\OfferBussness::referralFirstTranscation($request);

            $resArray = array(
                'pageStatus'         => 'openBrowser',
                'transactionOrderId' => $transRes['response']['TransactionDetails']['TransactionOrderID'],
                'transactionDetailId' => $transRes['response']['TransactionDetails']['TransactionDetailID'],
                'result'             => ''
            );

            $output_arr['display_msg'] = array( 'info' => 'Billdesk Transaction Success With Rs. '.$amount  /*'info' => $transRes['info']*/ );
            $output_arr['is_error']    = FALSE;
            $output_arr['res_data']    = $resArray;
            return $output_arr;
        }

        // code for failure case
        
        $transactionOrderId = \App\Libraries\TransctionHelper::getTransctionId();
        $adminData = \App\Libraries\Helpers::GetAdminData();
      
       $txn_detail_id      = $this->_saveTransactiondetail($request, $mainProdId, $transactionOrderId, $request->input('amount'), $transStaus,$partyInfo,$adminData);
        // code to insert transactionEventlog
        $this->_saveTransactionEventLog($request, $txn_detail_id, $mainProdId, $request->input('event'),$status='Recharge Failed');

        // code to insert Daily transactionEventlog
        $this->_saveDailyTransactionLog($request, $txn_detail_id, $mainProdId, $adminData->WalletAccountId, $request->input('event'));

        $this->_saveNotificationlog($request, $txn_detail_id, $message   = 'Transaction Failed. Failed To Do Recharge');
        
        // code to update 
        $updateTxnDetails = \App\Models\Transactiondetail::find($txn_detail_id);
            $updateTxnDetails->RespPGTrackNum = (!empty($payementId))?$payementId:0;
            $updateTxnDetails->RespBankRefNum = (!empty($bankRefId))?$bankRefId:0;
            $updateTxnDetails->save();
            
             // insertion in user recharge details
            if($request->input('event') == 'RECPRP')
            {
       $this->insertRechargeDetails($partyId, $txn_detail_id, $request, 'Failed');
            }
        $output_arr['is_error']    = FALSE;
        $output_arr['display_msg'] = array( 'info' => trans('messages.failureTransactionBilldesk') );
        return json_encode($output_arr);
    }

     // update recharge details
    public function insertRechargeDetails($partyId, $txnId, $request, $status)
    {
        // insert user recharge details
        $insertUserRHdetails                        = new \App\Models\Userrechargedetail;
        $insertUserRHdetails->UserWalletID          = $partyId;
        $insertUserRHdetails->TransactionDetailID   = $txnId;
        $insertUserRHdetails->RechargeCategory      = '';
        $insertUserRHdetails->RechargeType          = '';
        $insertUserRHdetails->SubscriberNumber      = $request->input('subcriberMobile');
        $insertUserRHdetails->SubScriberName        = '';
        $insertUserRHdetails->UseroperatorfeeplanID = $request->input('operatorPlanId');
        $insertUserRHdetails->Amount                = $request->input('amount');
        $insertUserRHdetails->Status                = $status;
        $insertUserRHdetails->CreatedBy             = 'User';
        $insertUserRHdetails->PaymentID             = ( ! empty($request->input('paymentId'))) ? $request->input('paymentId') : 0;
        $insertUserRHdetails->save();
    }

    /**
     * code for insertion of transaction event log table
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for transaction event log
     private function _saveTransactionEventLog($request, $txn_detail_id, $payment_product_id, $event,$status)
    {
        // code to insert cards details starts
        $insert_event_log                      = new \App\Models\Transactioneventlog;
        $insert_event_log->TransactionDetailID = $txn_detail_id;
        $insert_event_log->PaymentsProductID   = $payment_product_id;
        $insert_event_log->EventCode           = $event;
        $insert_event_log->Comments            = (!empty($status))?$status:'';
        $insert_event_log->EventDate           = date('Y-m-d H:i:s');
        $insert_event_log->CreatedBy           = $request->input('partyId');
        $insert_event_log->CreatedDateTime     = date('Y-m-d H:i:s');
        $insert_event_log->save();

        // code to insert cards details ends
    }

   

    /**
     * code for to check _saveNotificationlog in add money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for Insert Notifications
    private function _saveNotificationlog($request, $txn_detail_id, $message)
    {
        // code to insert cards details starts
        $insert_notif_log                        = new \App\Models\Notificationlog;
        $insert_notif_log->LinkedtransactionID   = $txn_detail_id;
        $insert_notif_log->PartyID               = $request->input('partyId');
        $insert_notif_log->NotificationType      = 'Alert';
        $insert_notif_log->SentDateTime          = date('Y-m-d H:i:s');
        $insert_notif_log->ReceipentAcknowledged = 'N';
        $insert_notif_log->ReceipentReceivedDate = date('Y-m-d H:i:s');
        $insert_notif_log->Message               = $message;
        $insert_notif_log->CreatedBy             = $request->input('partyId');
        $insert_notif_log->CreatedDateTime       = date('Y-m-d H:i:s');
        $insert_notif_log->save();

        // code to insert cards details ends
    }

    

    /**
     * code for to check _getUserWalletId in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    //code to check for wallet and user matches found with correct id
    private function _getUserWalletId($request)
    {

        // code to get kyc accepted or not from table users
        $check_walletAccount = \App\Models\Transactiondetail::select('ToAccountId', 'PaymentsProductID', 'Amount', 'EventCode', 'TransactionDate')
                ->where('TransactionDetailID', $request->input('transactionId'))
                ->where('ToWalletId', $request->input('partyId'))
                ->first();
        return ( ! empty($check_walletAccount)) ? $check_walletAccount : FALSE;
        // return $check_limit->IsVerificationCompleted;
    }

    /**
     * code for to check _updateTransactionEvent in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for update transaction detail event code for reversal
    private function _saveTransactiondetail($request, $payment_product_id, $txn_id, $amount, $status, $partyInfo,$adminData)
    {
        // code to insert cards details starts


        $insert_txn_details                      = new \App\Models\Transactiondetail;
        $insert_txn_details->PaymentsProductID   = $payment_product_id;
        $insert_txn_details->TransactionOrderID  = $txn_id;
        $insert_txn_details->UserWalletId        = (!empty($partyInfo->UserWalletID))?$partyInfo->UserWalletID:0;
        $insert_txn_details->FromAccountId       = (!empty($partyInfo->WalletAccountId))?$partyInfo->WalletAccountId:0;;
        $insert_txn_details->FromAccountType     = 'WalletAccount';
        $insert_txn_details->ToAccountType       = 'WalletAccount';
        $insert_txn_details->ToAccountId         = (!empty($adminData->WalletAccountId))?$adminData->WalletAccountId:0;
        $insert_txn_details->ToWalletId          = (!empty($adminData->UserWalletID))?$adminData->UserWalletID:0;
        $insert_txn_details->Amount              = $amount;
        $insert_txn_details->TransactionTypeCode = 'Transfer';
        $insert_txn_details->TransactionDate     = date('Y-m-d H:i:s');
        $insert_txn_details->TransactionVendor   = 'Viola wallet';
        $insert_txn_details->TransactionStatus   = $status;
        $insert_txn_details->TransactionMode     = 'Debit';
        $insert_txn_details->TransactionType     = 'RECHARGE';
        $insert_txn_details->EventCode           = '';
        $insert_txn_details->FromAccountName     = (!empty($partyInfo->FirstName))?$partyInfo->FirstName:0;;
        // $insert_txn_details->UserSessionUsageDetailID = (\App\Libraries\Helpers::getUserSessionId($request)!=FALSE) ? \App\Libraries\Helpers::getUserSessionId($request) : 0;
        $insert_txn_details->CreatedBy           = 'User';
        $insert_txn_details->CreatedDatetime     = date('Y-m-d H:i:s');
        $insert_txn_details->save();

        // get the insert id
        return $insert_txn_details->TransactionDetailID;

        // code to insert cards details ends
    }

    /**
     * code for to check _saveTopupReversalDailyTransactionLog in Refund topup money method
     * @desc this is used in add Money method only
     * @desc In this we will store our card details if user wants to save his card 
     * 
     * @param  PartyId Integer
     * @param  card_hname
     * @return interger
     */
    // code for transaction event log
    private function _saveDailyTransactionLog($request, $txn_detail_id, $payment_product_id, $walletacnt_id, $event)
    {
        $sel_daily_txn_log = \App\Models\Dailytransactionlog::select('TransactionLineNumber')
                ->where('TransactionDate', date('Y-m-d'))
                ->orderby('DailyTransactionSequenceNumber', 'DESC')
                ->first();

        $insert_line_number                           = ( ! empty($sel_daily_txn_log)) ? $sel_daily_txn_log->TransactionLineNumber + 1 : 1;
        // code to insert user txn details starts
        $insert_event_log                             = new \App\Models\Dailytransactionlog();
        $insert_event_log->TransactionDetailID        = $txn_detail_id;
        $insert_event_log->TransactionLineNumber      = $insert_line_number;
        $insert_event_log->TransactionWalletAccountId = $walletacnt_id;
        $insert_event_log->PartyId                    = $request->input('partyId');
        $insert_event_log->TransactionCcyAmount       = $request->input('amount');
        $insert_event_log->LocalCcyAmount             = $request->input('amount');
        $insert_event_log->AcctCcyAmount              = $request->input('amount');
        $insert_event_log->AccountCurrency            = 'INR';
        $insert_event_log->TransactionCurrency        = 'INR';        
        $insert_event_log->EventCode                  = $event;
        $insert_event_log->CrDrIndicator              = 'D';
        $insert_event_log->TransactionDate            = date('Y-m-d');
        $insert_event_log->ValueDate                  = date('Y-m-d');
        $insert_event_log->AmountTag                  = 'TRAN_AMT';
        $insert_event_log->PaymentsProductID          = $payment_product_id;
        $insert_event_log->CreatedBy                  = $request->input('partyId');
        $insert_event_log->CreatedDateTime            = date('Y-m-d H:i:s');
        $insert_event_log->save();

        // code for admin txn details starts
        // code to insert user txn details starts
        $admin_data                                         = \App\Libraries\Helpers::GetAdminData();
        $insert_admin_event_log                             = new \App\Models\Dailytransactionlog();
        $insert_admin_event_log->TransactionDetailID        = $txn_detail_id;
        $insert_admin_event_log->TransactionLineNumber      = $insert_line_number;
        $insert_admin_event_log->TransactionWalletAccountId = $admin_data->WalletAccountId;
        $insert_admin_event_log->PartyId                    = $admin_data->UserWalletId;
        $insert_admin_event_log->TransactionCcyAmount       = $request->input('amount');
        $insert_admin_event_log->LocalCcyAmount             = $request->input('amount');
        $insert_admin_event_log->AcctCcyAmount              = $request->input('amount');
        $insert_admin_event_log->AccountCurrency            = 'INR';
        $insert_admin_event_log->TransactionCurrency         = 'INR';
        $insert_admin_event_log->EventCode                  = $event;
        $insert_admin_event_log->CrDrIndicator              = 'C';
        $insert_admin_event_log->TransactionDate            = date('Y-m-d');
        $insert_admin_event_log->ValueDate                  = date('Y-m-d');
        $insert_admin_event_log->AmountTag                  = 'TRAN_AMT';
        $insert_admin_event_log->PaymentsProductID          = $payment_product_id;
        $insert_admin_event_log->CreatedBy                  = $request->input('partyId');
        $insert_admin_event_log->CreatedDateTime            = date('Y-m-d H:i:s');
        $insert_admin_event_log->save();

        //return $insert_event_log->DailyTransactionSequenceNumber;
        // code to insert cards details ends
    }

   

    // methods for refund money in topup ends

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param PartyId integer
     * @param WalletAcntId integer
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _ValidationBilldeskRecharge($request)
    {

        // validations
        $validate = array(
            'partyId'       => 'required|numeric',
          //  'transactionId' => 'required',
            'amount'        => 'required|numeric',
            'status'        => 'required|in:Success,Failed',
            'event'         => 'required',
        );
        $messages = array();

        // validation for mobile device


        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
