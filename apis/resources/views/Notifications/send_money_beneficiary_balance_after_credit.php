<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Balance details after crediting funds template
 *  param $fullname varchar
 *  param $amount varchar
 *  param $beneficiaryName varchar
 *  param $dateTime varchar 
 *  param $transactionId varchar 
 *  param $balanceAmount varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Credit Alert!',
        'text'    => $salutationName . ', <br/><br/>
            INR ' . $amount . ' credited to '.$beneficiaryName.' on ' . $dateTime . '. <br/>
            Transaction ID: <br/>
            Current Balance: ' . $balanceAmount],
    'sms'   => [
        'text' => ' 
Credit Alert!
Dear ' . $fullname . ',
INR '.$amount.' was received from ' . $beneficiaryName . ' on '.$dateTime.'. 
    Transaction ID: ' . $transactionId . ' 
    Current Balance: ' . $balanceAmount . '
',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'INR '.$amount.' credited to your ViolaWallet',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'transactions'
    ],
    'web'   => [
        'text' => 'INR '.$amount.' credited',
    ],
];

echo json_encode($array);

/* End of file send_money_beneficiary_balance_after_credit.php */
