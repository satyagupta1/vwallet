<?php
/*
*
* Mashup library for CodeIgniter
* created by Mahendra Vikhar (mvik86)
*
* This code distributed with The MIT License (MIT), so be free to use it anywhere
*
* The mashup library is used to convert your numetic string into some mashup without hashing or encrypting.
*
* @category  PHP cryptograph
* @author    Mahendra Vikhar
* @license   MIT License (MIT)
*
*/

namespace App\Libraries\Mash;


class Mashup{

  // the string which we use for the mashup
  protected static $mashup_string = 'bcdefghjkmnpqrstuwxyz';

  // the string which we use for separator
  protected static $separator_string = 'viola';

  /**
  * mashup_str is the function to mashup the QR code with other garbage value.
  * @param string $p = this is the product id from 01 to 16
  * @param int $i = id of the card.
  * @param double $a = amount, by default 0.00
  *
  * @return string mashed
  */
  public static function mashup_str($p, $i, $a)
  {
    $separated_mashup = substr(str_shuffle(self::$separator_string), 0, 1);

    $mashup_with_string = $p.$i;

    $lenth = strlen($mashup_with_string);

    $final_str = '';
    for ($z=0; $z<=$lenth; $z++)
    {
      $added_mashup = self::getmashup();

      $start = $z;
      $separator = rand(1,3);
      $string_part = substr($mashup_with_string, $z, $separator);
      $final_str .= $added_mashup.$string_part;
      $z = ($start+$separator) -1;
    }

    $final_amt = str_replace('.', '', $a);
    $amt_length = strlen($final_amt);
    $finalamt = '';
    for ($y=0; $y<=$amt_length; $y++)
    {
      $amt_mashup = self::getmashup();

      $st = $y;
      $sep = rand(1,3);
      $amt_part = substr($final_amt, $y, $sep);
      $finalamt .= $amt_mashup.$amt_part;
      $y = ($st+$sep) -1;
    }
    return $final_str.$separated_mashup.$finalamt;
  }

  /**
  * mashup_str is the function to mashup the QR code with other garbage value.
  * @param string $mashedup_string = this is the mashed string
  *
  * @param array $return_arr = this array has the value product id, card id, amount
  */
  public static function unmashup_str($mashedup_string)
  {
    $mashup_arr = str_split(self::$mashup_string);

    $separator_arr = str_split(self::$separator_string);
    $separator_arr_len = count($separator_arr);
    $out = '';
    for ($i=0; $i<$separator_arr_len; $i++)
    {
      $separator_pos = strpos($mashedup_string, $separator_arr{$i});
      if($separator_pos)
      {
        $sepate = explode($separator_arr{$i}, $mashedup_string);
      }
    }
    $amt_str_new = str_ireplace($mashup_arr, '', $sepate[1]);
    $amt_str_before_dec = substr($amt_str_new, 0, -2);
    $amt_str_after_dec = substr($amt_str_new, -2, 2);
    $amt = $amt_str_before_dec.'.'.$amt_str_after_dec;
    // $return_amt = (double) $amt;
    $return_amt = number_format($amt,2,".", '');


    $id_str_new = str_ireplace($mashup_arr, '', $sepate[0]);
    $product_id = substr($id_str_new, 0, 2);
    $card_id = substr($id_str_new, 2);

    $return_arr = array(
      'product_id' => $product_id,
      'card_id' => $card_id,
      'amount' => $return_amt,
    );
    return $return_arr;
  }

  private function getmashup()
  {
    $mlength = rand(1,3);
    return substr(str_shuffle(self::$mashup_string), 0, $mlength);
  }
}

?>
