<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Transaction Details after booking
 * 
 *  param $amount decimal
 *  param $from varchar
 *  param $to varchar
 *  param $email varchar
 *  param $placeOne varchar
 *  param $placeTwo varchar
 *  param $ticketNumber varchar
 *  param $busType varchar
 *  param $landmark varchar
 *  param $boardingFrom varchar
 *  param $seatNum varchar
 *  param $placename varchar
 *  param $operatorName varchar
 *  param $date Date
 *  param $day varchar
 *  param $datetime datetime
 *  param $departureDateTime datetime
 *  param $fareAmount decimal
 *  param $discountFareAmount decimal
 *  param $savingsAmount decimal
 *  param $boardingPointLocation varchar
 *  param $fullname varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Ticket for ' . $from . ' - ' . $to . '. ',
        'text'    => $salutationName . ',<br/><br/>   
                        You Bus ticket booking is confirmed<br/>
                        e-ticket sent to : ' . $email . '<br/>

                        ' . $placeOne . ' to ' . $placeTwo . ', ' . $date . ' & ' . $day . '
                        Onward Journey<br/>                                    
                        Ticket Number:' . $ticketNumber . '<br/>                  
                        Bus Type:' . $busType . ' Land Mark:' . $landmark . '<br/>    
                        Boarding from:' . $boardingFrom . '<br/>                   
                        Seat no.:' . $seatNum . '<br/><br/>                                     

                        Return Journey<br/>
                        Ticket Number:' . $ticketNumber . '<br/>
                        Bus Type:' . $busType . '  Land Mark:' . $landmark . '<br/>
                        Boarding from:' . $boardingFrom . '<br/>
                        Seat no. :' . $seatNum . '<br/> 
                        PRINT',
    ],
    'sms'   => [
        'text' => 'Congratulations on your ticket booking on ViolaWallet. Your savings are <>. You just paid <> for the fare of<original_fare>',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Bus Ticket From ' . $placeOne . ' to ' . $placeTwo . ' confirmed ',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'booking'
    ],
    'web'   => [
        'text' => 'Busbooking confirmed. Ticket number:' . $ticketNumber,
    ],
];

echo json_encode($array);

/* End of file transaction_details_after_booking */
