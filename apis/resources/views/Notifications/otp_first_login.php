<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * OTP at First login on a device template
 * 
 *  param $fullname varchar
 *  param $otpCode varchar
 *  param $email varchar
 *  param $deviceName varchar
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Alert! Signin from new device',
        'text'    => $salutationName . ', <br/><br/>
                        '.$otpCode.' is the One Time Password (OTP) for your ViolaWallet. <br/>
                        Above OTP is valid for '.$otpExpiryMinutes.' minutes from the time you have generated it. Please note,  this OTP is only valid for this transaction and cannot be used for any other transaction. OTP has also been sent to your registered mobile number.<br/>
                        Please do not share this One Time Password with anyone.',
    ],
    'sms'   => [
        'text' => $otpCode . ' is your One Time Passcode(OTP). Please use this code to verify your login. Good to see you on ViolaWallet',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Security alert! Sign-in from a new device',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'alerts'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file otp_first_login.php */
