<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 08 Nov 2017 11:35:56 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactionapimessage
 * 
 * @property int $TransactionDetailID
 * @property string $MessageType
 * @property string $MessageFilePath
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDateTime
 *
 * @package App\Models
 */
class Transactionapimessage extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TransactionDetailID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime'
	];

	protected $fillable = [
		'MessageFilePath',
		'CreatedBy',
		'CreatedDateTime'
	];
}
