<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 31 Oct 2017 11:24:43 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Productplanconfigparameter
 * 
 * @property int $ProductPlanConfigParameterId
 * @property int $ConfigParamNum
 * @property string $ParameterTagName
 * @property string $ParameterTagValue
 * @property string $ActiveFlag
 * @property \Carbon\Carbon $EffectiveDate
 * @property \Carbon\Carbon $ExpiryDate
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $productplanconfigs
 *
 * @package App\Models
 */
class Productplanconfigparameter extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'ConfigParamNum' => 'int'
	];

	protected $dates = [
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'ParameterTagName',
		'ParameterTagValue',
		'ActiveFlag',
		'EffectiveDate',
		'ExpiryDate',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function productplanconfigs()
	{
		return $this->hasMany(\App\Models\Productplanconfig::class, 'ConfigParamNum', 'ConfigParamNum');
	}
}
