<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Usertransactionsurvey
 * 
 * @property int $SurveyID
 * @property int $UserWalletID
 * @property int $TransactionDetailID
 * @property string $SurveyResult
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \App\Models\Userwarrenty $userwarrenty
 *
 * @package App\Models
 */
class Usertransactionsurvey extends Eloquent
{
	protected $table = 'usertransactionsurvey';
	protected $primaryKey = 'SurveyID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'SurveyID' => 'int',
		'UserWalletID' => 'int',
		'TransactionDetailID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'UserWalletID',
		'TransactionDetailID',
		'SurveyResult',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}

	public function userwarrenty()
	{
		return $this->belongsTo(\App\Models\Userwarrenty::class, 'UserWalletID');
	}
}
