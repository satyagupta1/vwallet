<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Thank you Mail template
 *  param $fullname varchar
 *  param $queryName varchar
 *  param $ticketNumber varchar
 *  param $solutionDescription varchar
 * 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation . ' ' . $fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Ticket Number : ' . $ticketNumber . ' from ViolaWallet',
        'text'    => $salutationName . ',<br/><br/>
                        Thank you for contacting us regarding ' . $queryName . '. 
                            We believe that ' . $ticketNumber . ' has now been resolved now.
                        ' . $solutionDescription . '
                        Status: Closed
                        If you find that the problem still persists, kindly click on the below status as Re-Open 
                        as soon as possible. We will reopen the query and investigate again
                        Status: Re-Open',
    ],
    'sms'   => [
        'text' => 'Hello ' . $fullname . ',
                    Thank you for contacting us regarding '.$queryName.'. 
                    We believe that ' . $ticketNumber . ' has now been resolved now.',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Ticket number ' . $ticketNumber . ' resolved',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'booking'
    ],
    'web'   => [
        'text' => 'Query ' . $queryName . ' raised. Ticket number:' . $ticketNumber . '',
    ],
];

echo json_encode($array);

/* End of file support_thank_you_mail.php */
