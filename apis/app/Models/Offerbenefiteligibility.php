<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 07:36:07 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Offerbenefiteligibility
 * 
 * @property int $OfferBenefitEligibilityID
 * @property int $BenefitBusinessID
 * @property float $BenefitVersion
 * @property int $RuleCategoryRefID
 * @property int $RuleNameRefID
 * @property int $RuleOperatorRefID
 * @property string $RuleParamValues
 * 
 * @property \App\Models\Offerbenefit $offerbenefit
 *
 * @package App\Models
 */
class Offerbenefiteligibility extends Eloquent
{
	protected $table = 'offerbenefiteligibility';
	protected $primaryKey = 'OfferBenefitEligibilityID';
	public $timestamps = false;

	protected $casts = [
		'BenefitBusinessID' => 'int',
		'BenefitVersion' => 'float',
		'RuleCategoryRefID' => 'int',
		'RuleNameRefID' => 'int',
		'RuleOperatorRefID' => 'int'
	];

	protected $fillable = [
		'BenefitBusinessID',
		'BenefitVersion',
		'RuleCategoryRefID',
		'RuleNameRefID',
		'RuleOperatorRefID',
		'RuleParamValues'
	];

	public function offerbenefit()
	{
		return $this->belongsTo(\App\Models\Offerbenefit::class, 'BenefitBusinessID', 'BenefitBusinessID')
					->where('offerbenefits.BenefitBusinessID', '=', 'offerbenefiteligibility.BenefitBusinessID')
					->where('offerbenefits.BenefitVersion', '=', 'offerbenefiteligibility.BenefitVersion');
	}
}
