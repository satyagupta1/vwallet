<?php

namespace App\Http\Controllers\Payments;

/**
 * PaymentsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@vwallet.in>
 * @license   http://vwallet.in Licence
 */
class CcavenueController extends \App\Http\Controllers\Controller {

    public function __construct()
    {
        $this->encKey = config('vwallet.ENCRYPT_SALT');
    }

    /*
     *  Get CcAvenue Response
     *  @param Request $request
     *  @return HTML
     */

    public function paymentResponse(\Illuminate\Http\Request $request)
    {
        //return $returnData = \App\Libraries\Payments\Ccavenue::kotakApiRequest($request);
        $messageContent = NULL;
        $messageArray   = array();
        $response       = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        if ( $request->input('encResp') !== NULL )
        {
            $order_transaction_id = 0;
            $workingKey           = config('vwallet.workingKeyCcA');  //Working Key should be provided here.
            $encResponse          = $_POST["encResp"];   //This is the response sent by the CCAvenue Server
            $rcvdString           = \App\Libraries\Payments\Ccavenue::decrypt($encResponse, $workingKey);  //Crypto Decryption used as per the specified working key.
            //if you want to html
            $order_status         = NULL;
            $decryptValues        = explode('&', $rcvdString);
            $dataSize             = sizeof($decryptValues);

            for ( $i = 0; $i < $dataSize; $i ++ )
            {
                $information = explode('=', $decryptValues[$i]);
                //if ( $i == 3 )
                //$order_status = $information[1];
                $msgKey      = $information[0];
                if ( $msgKey == 'order_status' )
                {
                    $order_status = $information[1];
                }
                if ( $msgKey == 'order_id' )
                {
                    $order_transaction_id = $information[1];
                }

                if ( $msgKey == 'amount' )
                {
                    $order_amount = $information[1];
                }



                $messageArray[$msgKey] = $information[1];
            }

            $attrSuccessful  = trans('messages.attributes.successful');
            $attrTransaction = trans('messages.attributes.transaction');

            if ( $order_status === "Success" )
            {
                $ourStatus      = "Success";
                $successMsg     = trans('messages.moduleStatus', [ 'module' => $attrTransaction, 'status' => $attrSuccessful ]);
                $displayMessage = $successMsg;
            }
            else if ( $order_status === "Aborted" )
            {
                $ourStatus      = "Failed";
                $displayMessage = trans('messages.ccAvanueAborted');
            }
            else if ( $order_status === "Failure" )
            {
                $ourStatus      = "Failed";
                $displayMessage = trans('messages.ccAvanueFailure');
            }
            else
            {
                $ourStatus      = "Failed";
                $displayMessage = trans('messages.securityError');
            }

            //we got response from CCAvenue            

            $update_txn_detail                    = \App\Models\Transactiondetail::where('TransactionOrderID', '=', $order_transaction_id)->first();
            $update_txn_detail->TransactionStatus = $ourStatus;
            $update_txn_detail->save();

            if ( $order_status === "Success" )
            {
                //save only if success
                // code for admin wallet update starts
                $admin_wallet                         = \App\Libraries\Helpers::GetAdminData();
                $update_adminWallet                   = \App\Models\Walletaccount::find($admin_wallet->WalletAccountId);
                $update_adminWallet->AvailableBalance = $update_adminWallet->AvailableBalance - $order_amount;
                $update_adminWallet->CurrentAmount    = $update_adminWallet->CurrentAmount - $order_amount;
                $update_adminWallet->save();

                // code for user wallet update starts    
                $update_userWallet                   = \App\Models\Walletaccount::find($update_txn_detail->ToAccountId);
                $update_userWallet->AvailableBalance = $update_userWallet->AvailableBalance + $order_amount;
                $update_userWallet->CurrentAmount    = $update_userWallet->CurrentAmount + $order_amount;
                $update_userWallet->save();
            }


            // code to insert cards details starts
            $insert_event_log           = \App\Models\Transactioneventlog::where('TransactionDetailID', '=', $update_txn_detail->TransactionDetailID)->first();
            $insert_event_log->Comments = $displayMessage;
            $insert_event_log->save();

            // code to insert notificationlog starts
            $insert_notif_log                        = new \App\Models\Notificationlog;
            $insert_notif_log->LinkedtransactionID   = $update_txn_detail->TransactionDetailID;
            $insert_notif_log->PartyID               = $update_txn_detail->ToWalletId;
            $insert_notif_log->NotificationType      = 'Alert';
            $insert_notif_log->SentDateTime          = date('Y-m-d H:i:s');
            $insert_notif_log->ReceipentAcknowledged = 'N';
            $insert_notif_log->ReceipentReceivedDate = date('Y-m-d H:i:s');
            $insert_notif_log->Message               = "Your transaction " . $order_status;
            $insert_notif_log->CreatedBy             = $update_txn_detail->ToWalletId;
            $insert_notif_log->CreatedDateTime       = date('Y-m-d H:i:s');
            $insert_notif_log->save();
        }
        else
        {
            $displayMessage = trans('messages.securityError');
        }
        exit;
        $messageArray['msg_heading']        = $displayMessage;
        $jsonEncodeData                     = json_encode($messageArray);
        $responseDataEncrypt                = \App\Libraries\Helpers::encryptData($jsonEncodeData, $this->encKey, TRUE);
        $response['is_error']               = FALSE;
        $response['res_data']['pageStatus'] = 'closeBrowser';
        $response['res_data']['result']     = $responseDataEncrypt;
        return $response;
        // $displayMessage1 = "<center>".$displayMessage."</center>";
        // $html = view('payments.ccavResponse')->with('messageContent', $displayMessage1);        
        //return response($html)
        //  ->header('Content-Type', 'html')            
        // ->header('Page-Status', 'Close_Browser');
    }

    public function paymentNetbankOptions(\Illuminate\Http\Request $request)
    {
        $baseUrl         = config('vwallet.baseUrl');
        $ccavenueBaseUrl = config('vwallet.ccavenueBaseUrl');
        $accessCodeCcA   = config('vwallet.accessCodeCcA');

        $output   = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );
        $curl     = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $ccavenueBaseUrl . "transaction/transaction.do?command=getJsonData&access_code=" . $accessCodeCcA . "&currency=INR&amount=10000.00",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                'content-type: application/json; charset=utf-8',
                "access-control-allow-origin: *",
                "referer: " . $baseUrl,
            ),
        ));
        $response = curl_exec($curl);


        $err = curl_error($curl);

        curl_close($curl);
        $jsonFinal = NULL;
        $sendArray = [];
        if ( $err )
        {
            //echo "cURL Error #:" . $err;
            $output['display_msg']['info'] = $err;
        }
        else
        {
            $output['is_error'] = FALSE;
            $jsonString         = substr($response, 12, -1);
            $jsonFinal          = ( array ) json_decode($jsonString);
            foreach ( $jsonFinal as $rawData )
            {
                if(!isset($rawData->payOpt)) {
                    break;
                }
                $payOpt    = $rawData->payOpt;
                $cardInfo  = json_decode($rawData->$payOpt);
                $cardNames = [];
                $cardType  = $payOpt;
                foreach ( $cardInfo as $card )
                {
                    $cardType    = $card->cardType;
                    $cardNames[] = $card->cardName;
                }
                $sendArray[$cardType] = $cardNames;
            }
        }
        $output['res_data'] = $sendArray;
        return response()->json($output);
    }

}

/* End of file CcavenueController.php */ /* Location: ./Http/Test/TestController.php */
