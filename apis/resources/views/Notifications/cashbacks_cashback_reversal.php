<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Cashback reversal template
 *  param $fullname varchar
 *  param $amount varchar 
 *  param $cashbackPercentage numeric 
 *  param $goodsName varchar 
 *  param $date varchar 
 *  param $medium varchar 
 *  param $referenceNumber varchar 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Cashback Reversal',
        'text'    => $salutationName . ',<br/><br/>
You received a cashback of INR ' . $amount . ' on the purchase of ' . $goodsName . '.  
    The goods were returned / transaction was declined therefore,  we have reversed the ' . $cashbackPercentage . '% cashback from your ViolaWallet.
<br/>Reference Number: ' . $referenceNumber],
    'sms'   => [
        'text' => '
You received a cashback of INR ' . $amount . ' on the purchase of ' . $goodsName . '. 
The goods were returned / transaction was declined therefore, we have reversed the ' . $cashbackPercentage . '% cashback from your ViolaWallet.
Reference Number: ' . $referenceNumber,
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Cashback reversal',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => 'INR ' . $amount . ' cashback reversed',
    ],
];

echo json_encode($array);

/* End of file cashbacks_cashback_reversal.php */
