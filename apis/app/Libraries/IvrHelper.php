<?php

namespace App\Libraries;

class IvrHelper {
    /*
     *  profileDetails - To Read User Information
     *  @param Request $request
     *  @return JSON
     */

    public static function userVerify($request, $validMethod)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // validate input information        
        $validationCheck = self::$validMethod($request);
        if ( $validationCheck !== TRUE )
        {
            $output_arr['res_data'] = $validationCheck;
            return $output_arr;
        }

        // user exsistens test
        $mobNumber = $request->input('mobileNumber');
        $userCheck = self::userValidate($mobNumber);
        if ( ! $userCheck )
        {
            $output_arr['res_data'] = array( 'info' => trans('messages.wrongUser') );
            return $output_arr;
        }

        // if user is in active state block him here
        if ( $userCheck->ProfileStatus != 'A' )
        {
            switch ( $userCheck->ProfileStatus )
            {
                case 'N':
                    $stat = 'You have not finished registration.';
                    break;
                case 'D':
                    $stat = 'Your account is Deactivated.';
                    break;
                case 'F':
                    $stat = 'Your account is Frozen.';
                    break;

                default:
                    $stat = 'Your account not active.';
                    break;
            }
            $output_arr['res_data'] = array( 'info' => $stat );
            return $output_arr;
        }

        $output_arr['res_data'] = $userCheck;
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    /*
     *  userValidate - User Id Check Method
     *  @param Mobile Number $mobNumber
     *  @return Boolean
     */

    public static function userValidate($mobNumber)
    {
        // select Array to get info from DB
        $select = array(
            'UserWalletID',
            'ProfileStatus',
            'FirstName',
            'MiddleName',
            'LastName'
        );

        $userExsist = \App\Models\Userwallet::select($select)
                        ->where('MobileNumber', '=', $mobNumber)->first();

        if ( ! $userExsist )
        {
            return FALSE;
        }
        else
        {
            return $userExsist;
        }
    }

    /*
     *  _userValidation - Internal Validation
     *  @param Request $request
     *  @return JSON
     */

    private static function _userValidation($request)
    {
        $validations = array(
            'mobileNumber' => 'required|numeric|digits:10',
        );
        $messages    = [
            'mobileNumber.required' => 'Not a valid mobile number',
            'mobileNumber.numeric'  => 'Not a valid mobile number',
            'mobileNumber.digits'   => 'Not a valid mobile number'
        ];

        return self::_validateMethod($request, $validations, $messages);
    }

    /*
     *  hashKeyValidate - Validate Hash key before process change password 
     *  @param Request $request
     *  @return JSON
     */

    public static function hashKeyValidate($request)
    {
        $validations = array(
            'hashKey' => 'required',
        );
        $messages    = [
            'hashKey.required' => 'Request is not valid'
        ];

        return self::_validateMethod($request, $validations, $messages);
    }

    /*
     *  hashKeyOtp - Validate Hash key OTP before set new change password 
     *  @param Request $request
     *  @return JSON
     */

    public static function hashKeyOtp($request)
    {
        $validations = array(
            'otpNumber' => 'required|numeric|digits:6',
            'otpId'     => 'required|numeric',
        );
        $messages    = [
            'otpNumber.required' => 'OTP is required',
            'otpNumber.numeric'  => 'Enter valid OTP',
            'otpNumber.digits'   => 'Enter valid OTP',
            'otpId.required'     => 'Enter valid OTP',
            'otpId.numeric'      => 'Enter valid OTP',
        ];

        return self::_validateMethod($request, $validations, $messages);
    }

    /*
     *  _validateMethod - validation Helaper method to validate Request
     *  @param Request $request
     *  @return JSON
     */

    private static function _validateMethod($request, $validations, $messages)
    {
        $validate_response = \Validator::make($request->all(), $validations, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

    /*
     *  generateOtp - Genrate Randum Number for OTP
     *  @param None
     *  @return Number
     */

    public static function generateOtp()
    {
        return mt_rand(100000, 999999);
    }

    /**
     *  sendOtp - send OTP to user
     *  @param 
     *  @return bool
     */
    public static function sendOtp($params)
    {
        $otp = $params['otpNumber'];
        // send SMS
//        \App\Libraries\Smshub::send_message($param);
        // conditions for resend OTP
        if ( isset($params['attempts']) && $params['attempts'] > 0 )
        {
            $otpMsg   = 'We have sent an OTP to ' . $params['mobileNumber'] . ' mobile number.Please verify by entering OTP';
            $attempts = $params['attempts'] - 1;
            $otpData  = array(
                'OTP'           => $otp,
                'CreatedBy'     => 'User',
                'UpdatedBy'     => 'User',
                'NumOfAttempts' => $attempts,
            );
            \App\Models\Partyotp::where('OTPRequestID', $params['OTPRequestID'])
                    ->where('ToMobileNumber', $params['mobile_no'])->update($otpData);

            $inserytOTPData = $params['OTPRequestID'];
        }

        // First time OTP sending
        if ( ! isset($params['attempts']) )
        {
            $otpMsg         = $params['message'];
            $otpData        = array(
                'PartyID'             => $params['UserWalletID'],
                'OTP'                 => $otp,
                'TransactionTypeCode' => $params['typeCode'],
                'CreatedDateTime'     => date('Y-m-d H:i:s'),
                'CreatedBy'           => 'User',
                'NumOfAttempts'       => 3,
                'ToMobileNumber'      => $params['mobile_no']
            );
            $inserytOTPData = \App\Models\Partyotp::insertGetId($otpData);
        }

        if ( $inserytOTPData )
        {
            $data_v = array(
                'otpMssage' => $otpMsg,
                'otpNumber' => $otp,
                'otpId'     => $inserytOTPData,
                'otpType'   => $params['typeCode']
            );
            return $data_v;
        }
        return FALSE;
    }

    /*
     *  validateOTP - Validate OTP
     *  @param Request $request
     *  @return JSON
     */

    public static function validateOtp($params)
    {
        $validateOtp = \App\Models\Partyotp::select('OTP', 'NumOfAttempts')
                        ->where('OTPRequestID', $params['otpId'])->first();
        if ( ! $validateOtp )
        {
            return FALSE;
        }
        else
        {

            return ( $validateOtp->OTP == $params['otpNumber'] ) ? TRUE : FALSE;
        }
    }

}
