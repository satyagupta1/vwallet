<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Cardnotification
 * 
 * @property int $CardNotificationId
 * @property int $idCardUsers
 * @property string $NotificationType
 * @property string $Message
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Carduser $carduser
 *
 * @package App\Models
 */
class Cardnotification extends Eloquent
{
	protected $table = 'cardnotification';
	protected $primaryKey = 'CardNotificationId';
	public $timestamps = false;

	protected $casts = [
		'idCardUsers' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'idCardUsers',
		'NotificationType',
		'Message',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function carduser()
	{
		return $this->belongsTo(\App\Models\Carduser::class, 'idCardUsers');
	}
}
