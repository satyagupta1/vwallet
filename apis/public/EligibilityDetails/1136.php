<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a Mobile/DTH recharge of Transaction Amount >= INR 50 </li>
        <li>System should check if the transaction date is less than 30th June, 2018</li>
        <li>System should calculate the Cashback Amount = 10% of Transaction Amount</li>
        <li>Cashback Amount should be credited into the Customer's account provided the Cashback amount doesnt exceed INR 150.</li>        
    </ul> 
</div>
<?php include 'footer.php';?>
