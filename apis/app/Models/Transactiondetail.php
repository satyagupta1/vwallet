<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Transactiondetail
 * 
 * @property int $TransactionDetailID
 * @property string $TransactionOrderID
 * @property int $PaymentsProductID
 * @property int $UserWalletId
 * @property int $FromAccountId
 * @property string $FromAccountName
 * @property string $FromAccountType
 * @property string $ToAccountType
 * @property int $ToAccountId
 * @property int $ToWalletId
 * @property float $Amount
 * @property string $TransactionTypeCode
 * @property \Carbon\Carbon $TransactionDate
 * @property string $TransactionVendor
 * @property string $TransactionStatus
 * @property string $TransactionMode
 * @property int $UserSessionUsageDetailID
 * @property string $TransactionType
 * @property string $EventCode
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDatetime
 * @property float $FeeAmount
 * @property float $TaxAmount
 * @property string $PartyMobileNumber
 * @property string $PartyEmailID
 * @property string $RespPGTrackNum
 * @property string $RespBankRefNum
 * @property string $PromoCode
 * @property float $DiscountAmount
 * @property int $RefTransactionDetailID
 * @property float $WalletAmount
 * @property float $CashbackAmount
 * @property float $VoucherDiscount
 * @property int $UserOfferBenefitTrackerID
 * 
 * @property \App\Models\Paymentsproduct $paymentsproduct
 * @property \Illuminate\Database\Eloquent\Collection $dailytransactionlogs
 * @property \Illuminate\Database\Eloquent\Collection $merchanttransactiondetails
 * @property \App\Models\Transactionacknowledgement $transactionacknowledgement
 * @property \Illuminate\Database\Eloquent\Collection $transactioneventlogs
 * @property \App\Models\Transactionschedule $transactionschedule
 * @property \Illuminate\Database\Eloquent\Collection $userofferbenefittrackers
 * @property \Illuminate\Database\Eloquent\Collection $userrechargedetails
 * @property \Illuminate\Database\Eloquent\Collection $usertransactionsurveys
 * @property \Illuminate\Database\Eloquent\Collection $zztransactionpromotions
 *
 * @package App\Models
 */
class Transactiondetail extends Eloquent
{
	protected $table = 'transactiondetail';
	public $timestamps = false;
        public $primaryKey = 'TransactionDetailID';

	protected $casts = [
		'PaymentsProductID' => 'int',
		'UserWalletId' => 'int',
		'FromAccountId' => 'int',
		'ToAccountId' => 'int',
		'ToWalletId' => 'int',
		'Amount' => 'float',
		'UserSessionUsageDetailID' => 'int',
		'FeeAmount' => 'float',
		'TaxAmount' => 'float',
		'DiscountAmount' => 'float',
		'RefTransactionDetailID' => 'int',
		'WalletAmount' => 'float',
		'CashbackAmount' => 'float',
		'VoucherDiscount' => 'float',
		'UserOfferBenefitTrackerID' => 'int'
	];

	protected $dates = [
		'TransactionDate',
		'CreatedDatetime',
		'UpdatedDatetime'
	];

	protected $fillable = [
		'TransactionOrderID',
		'UserWalletId',
		'FromAccountId',
		'FromAccountName',
		'FromAccountType',
		'ToAccountType',
		'ToAccountId',
		'ToWalletId',
		'Amount',
		'TransactionTypeCode',
		'TransactionDate',
		'TransactionVendor',
		'TransactionStatus',
		'TransactionMode',
		'UserSessionUsageDetailID',
		'TransactionType',
		'EventCode',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDatetime',
		'FeeAmount',
		'TaxAmount',
		'PartyMobileNumber',
		'PartyEmailID',
		'RespPGTrackNum',
		'RespBankRefNum',
		'PromoCode',
		'DiscountAmount',
		'RefTransactionDetailID',
		'WalletAmount',
		'CashbackAmount',
		'VoucherDiscount',
		'UserOfferBenefitTrackerID'
	];

	public function paymentsproduct()
	{
		return $this->belongsTo(\App\Models\Paymentsproduct::class, 'PaymentsProductID');
	}

	public function partysessionusagedetail()
	{
		return $this->belongsTo(\App\Models\Partysessionusagedetail::class, 'UserSessionUsageDetailID');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'ToWalletId', 'UserWalletId')
					->where('walletaccount.UserWalletId', '=', 'transactiondetail.ToWalletId')
					->where('walletaccount.WalletAccountId', '=', 'transactiondetail.ToWalletAccountId');
	}

	public function dailytransactionlogs()
	{
		return $this->hasMany(\App\Models\Dailytransactionlog::class, 'TransactionDetailID');
	}
        
            public function merchanttransactiondetails()
        {
            return $this->hasMany(\App\Models\Merchanttransactiondetail::class, 'TransactionDetailID');
        }

        public function transactionacknowledgement()
        {
            return $this->hasOne(\App\Models\Transactionacknowledgement::class, 'TransactionID');
        }

        public function transactionschedule()
        {
            return $this->hasOne(\App\Models\Transactionschedule::class, 'TransactionID');
        }

        public function userofferbenefittrackers()
        {
            return $this->hasMany(\App\Models\Userofferbenefittracker::class, 'ParentPaymentsProductID', 'PaymentsProductID');
        }

        public function partypreferredpaymentmethods()
	{
		return $this->hasMany(\App\Models\Partypreferredpaymentmethod::class, 'TransactionDetailID');
	}
        
        public function userrechargedetails()
	{
		return $this->hasMany(\App\Models\Userrechargedetail::class, 'TransactionDetailID');
	}

        

	public function transactioneventlogs()
	{
		return $this->hasMany(\App\Models\Transactioneventlog::class, 'TransactionDetailID');
	}

	public function transactionpromotions()
	{
		return $this->hasMany(\App\Models\Transactionpromotion::class, 'TransactionDetailID');
	}

	public function usertransactionsurveys()
	{
		return $this->hasMany(\App\Models\Usertransactionsurvey::class, 'TransactionDetailID');
	}
}
