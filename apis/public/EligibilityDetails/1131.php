<?php include 'header.php';?>
<div>
    <h3>Eligibility Details</h3>
    <ul>
        <li>For each transaction, system should identify if it is a Electricity Bill Payment of Transaction Amount >= INR 500.</li>
        <li>System should check if the transaction date is less than 31st Jan, 2018.</li>
        <li>System should credit INR 75 cashback into the users wallet for every successfull electricity bill payment >=  INR 500.</li>
    </ul> 
</div>
<?php include 'footer.php';?>