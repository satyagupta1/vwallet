<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Sep 2017 07:38:39 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;;

/**
 * Class Usertransactionstat
 * 
 * @property int $UserWalletID
 * @property \Carbon\Carbon $TransactionDate
 * @property float $DayAmount
 * @property int $DayTransactionCount
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Userwallet $userwallet
 *
 * @package App\Models
 */
class Usertransactionstat extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'UserWalletID' => 'int',
		'DayAmount' => 'float',
		'DayTransactionCount' => 'int'
	];

	protected $dates = [
		'TransactionDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'DayAmount',
		'DayTransactionCount',
		'CreatedDatetime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function userwallet()
	{
		return $this->belongsTo(\App\Models\Userwallet::class, 'UserWalletID');
	}
}
