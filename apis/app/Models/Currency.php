<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Currency
 * 
 * @property string $CurrencyCode
 * @property string $CurrencyName
 * @property string $CurrencySymbol
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $bankbranches
 * @property \Illuminate\Database\Eloquent\Collection $countries
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 * @property \Illuminate\Database\Eloquent\Collection $userwallets
 *
 * @package App\Models
 */
class Currency extends Eloquent
{
	protected $table = 'currency';
	protected $primaryKey = 'CurrencyCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CurrencyName',
		'CurrencySymbol',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function bankbranches()
	{
		return $this->hasMany(\App\Models\Bankbranch::class, 'OperatingCurrencyCode');
	}

	public function countries()
	{
		return $this->belongsToMany(\App\Models\Country::class, 'countrycurrency', 'CurrencyCode', 'CountryCode')
					->withPivot('CreatedDateTime', 'CreatedBy', 'UpdatedDateTime', 'UpdatedBy');
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class, 'CurrencyCode');
	}

	public function userwallets()
	{
		return $this->hasMany(\App\Models\Userwallet::class, 'PreferredSecondaryCurrency');
	}
}
