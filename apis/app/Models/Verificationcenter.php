<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Verificationcenter
 * 
 * @property int $VerificationCenterID
 * @property string $CenterName
 * @property int $CenterAddressID
 * @property string $CenterTiming
 * @property string $IsCenterActive
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Partyaddress $partyaddress
 * @property \Illuminate\Database\Eloquent\Collection $merchantverfications
 * @property \App\Models\Userverificationtracker $userverificationtracker
 *
 * @package App\Models
 */
class Verificationcenter extends Eloquent
{
	protected $table = 'verificationcenter';
	protected $primaryKey = 'VerificationCenterID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'VerificationCenterID' => 'int',
		'CenterAddressID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'CenterName',
		'CenterAddressID',
		'CenterTiming',
		'IsCenterActive',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'VerificationCenterID');
	}

	public function partyaddress()
	{
		return $this->belongsTo(\App\Models\Partyaddress::class, 'CenterAddressID');
	}

	public function merchantverfications()
	{
		return $this->hasMany(\App\Models\Merchantverfication::class, 'VerificationCenterID');
	}

	public function userverificationtracker()
	{
		return $this->hasOne(\App\Models\Userverificationtracker::class, 'VerficiationCenterID');
	}
}
