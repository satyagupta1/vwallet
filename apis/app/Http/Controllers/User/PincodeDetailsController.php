<?php

namespace App\Http\Controllers\User;

class PincodeDetailsController extends \App\Http\Controllers\Controller {

    public function getPincodeDetails(\Illuminate\Http\Request $request)
    {
        $output_arr = array( 'is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array() );

        $checkValidate = $this->_validatePincode($request);
        //$checkValidate = TRUE;
        if ( $checkValidate !== TRUE )
        {
            $output_arr['display_msg'] = $checkValidate;
            return response()->json($output_arr);
        }

        $output_arr['is_error']    = FALSE;
        //Pincode Details
        $getPincodeDetails         = \App\Models\Pincodes::select('PinCode', 'DistrictName', 'StateName', 'CountryCode', 'CountryName')
                        ->where('PinCode', $request->input('pinCode'))->first();
        $output_arr['res_data']    = array( $getPincodeDetails );
        (count($getPincodeDetails) > 0) ? $output_arr['display_msg'] = array() : $output_arr['display_msg'] = array( 'info' => trans('messages.emptyRecords'));
        return json_encode($output_arr);
    }

    private function _validatePincode($request)
    {
        $validate = array(
            'pinCode' => 'required|numeric|digits_between:5,6',
        );
        $messages = array();

        $validate_response = \Validator::make($request->all(), $validate, $messages);

        $vali_response = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
