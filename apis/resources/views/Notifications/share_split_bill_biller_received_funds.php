<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *   When biller received funds (With Transaction and details and Available balance) template
 *  param $amount varchar
 *  param $shareFriendName varchar 
 */

$array = [
    'email' => [
        'subject' => '',
        'text'    => ''
    ],
    'sms'   => [
        'text' => '  
            Received Rs.' . $amount . ' from ' . $shareFriendName . ' in your ViolaWallet. 
                 ',
    ],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file share_split_bill_biller_received_funds.php */
