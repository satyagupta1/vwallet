<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 *  Balance Details after Crediting Funds (To Merchant) template  
 *  param $cahsbackPercentage numeric 
 *  param $posMerchantName varchar 
 *  param $amount varchar  
 *  param $tracnsactionId varchar  
 */

$array = [
    'email' => [
        'subject' => '',
        'text'    => ''
    ],
    'sms'   => [
        'text' => '
            Received Rs.'.$amount.' from '.$posMerchantName.' in your ViolaWallet. 
                Transaction ID:'.$tracnsactionId.' Scan code to Pay at the merchant name and receive 
                '.$cahsbackPercentage.'% cashback.'],
    'push'  => [
        'title'              => '',
        'body'               => '',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => '',
    ],
];

echo json_encode($array);

/* End of file pos_balance_after_crediting_funds.php */
