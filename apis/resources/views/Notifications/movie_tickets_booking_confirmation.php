<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * Transaction Details after booking & Booking Confirmation template
 * 
 *  param $fullname varchar
 *  param $bookingId int
 *  param $seats int
 *  param $movieName varchar
 *  param $movieDate date 
 *  param $movieDay varchar 
 *  param $movieTiming varchar 
 *  param $theaterName varchar  
 *  param $ticketNumber varchar 
 *  param $paymentType varchar 
 *  param $ticketAmount decimal 
 *  param $bookingFees decimal 
 *  param $paymentGatewayCharges decimal 
 *  param $totalPaid decimal 
 *  param $supportURL varchar 
 */

$salutation = trans('messages.salutation');
$salutationName = ($fullname) ? $salutation.' '.$fullname : $salutation;
$array = [
    'email' => [
        'subject' => 'Your Movie Ticket',
        'text'    => $salutationName . ',<br/><br/>
                     Your booking is confirmed! Booking ID: ' . $bookingId . '<br/>
Movie name: ' . $movieName . '<br/>
Timings, date & Day: ' . $movieTiming . ', ' . $movieDate . ' & ' . $movieDay . '<br/>
Theatre name:  ' . $theaterName . '<br/>
Ticket number: ' . $ticketNumber . '<br/>
Payment Type: ' . $paymentType . '<br/>
TICKET AMOUNT: ' . $ticketAmount . '<br/>                                                                                           Rs XX
Seats: ' . $seats . '     <br/>  <br/>                                                                                                                Ticket number.

INTERNET HANDLING FEES <br/> 
Booking Fees: ' . $bookingFees . '<br/>                                                                                                                   XX
Integrated GST (IGST) @ 18%<br/> <br/> 

Payment Gateway Charges: ' . $paymentGatewayCharges . '<br/>  
Integrated GST (IGST) @ 18%<br/> 

Total Paid: ' . $totalPaid],
    'sms'   => [
        'text' => 'Hi ' . $fullname . '

Booking ID:' . $bookingId . ' Seats: ' . $seats . '
  seat(s) for movie name: ' . $movieName . ' Date: ' . $movieDate . ' Day: ' . $movieDay . ' 
      and Timings: ' . $movieTiming . ' 

at Theatre name: ' . $theaterName . ' 

Please carry your CC/DC card which was used for booking tickets.

',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Movie Ticket booking confirmed',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'booking'
    ],
    'web'   => [
        'text' => 'Movie name confirmed. Booking ID',
    ],
];

echo json_encode($array);

/* End of file movie_tickets_booking_confirmation.php */
