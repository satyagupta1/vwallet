<?php

/**
 * A language file for text and email alert
 * @category PHP
 * @package  ViolaWallet
 * @author  Viola Services (India) PVT LTD
 */
/*
 * When Bill uploaded and shared with group member. (With amount details) template
 *  param $fullname varchar
 *  param $customerName varchar
 *  param $billName varchar
 *  param $loginLink varchar  
 */

$salutation = trans('messages.salutation');
$salutationName = ($customerName) ? $salutation . ' ' . $customerName : $salutation;
$array = [
    'email' => [
        'subject' => 'Split and Pay',
        'text'    => $salutationName . ',<br/><br/>      
            ' . $fullname . ' has uploaded and split a bill ' . $billName . ' with you.
                To check the amount and settle, log in to ' . $loginLink],
    'sms'   => [
        'text' => ' 
' . $fullname . ' has uploaded and split a bill ' . $billName . ' with you. To check the amount
    and settle, log in to ' . $loginLink . '
 
   ',
    ],
    'push'  => [
        'title'              => 'ViolaWallet',
        'body'               => 'Pay ' . $billName . ' through Share & split bill',
        'summaryText'        => '',
        'notificationType'   => 'bigTextStyle',
        'screenNavigationId' => 'general'
    ],
    'web'   => [
        'text' => 'Pay ' . $billName . ' through Share & split bill',
    ],
];

echo json_encode($array);

/* End of file share_split_bill_shared_with_group_member.php */
