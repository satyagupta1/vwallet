<?php

/**
 * A Send email with Template file
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */

namespace App\Libraries\Communication;

// use libraries
use App\Libraries\Communication\Functions;
use App\Libraries\Communication\Emails\emailNow;

/**
 * A Sendemail_lib class
 * @category  PHP
 * @package   vwallet
 * @author    Viola Services (India) PVT LTD <development@violamoney.com>
 * @license   https://violamoney.com Licence
 */
class Sendemail_lib {

    /**
     * ---------------------------------------------------------------------
     * send_new method.
     * ---------------------------------------------------------------------
     * The method send new is used for sending email to user with a common
     * email template.
     *
     * @param array $email_data ;
     *                          $emailData = array(
     *                          	'email_type_key', // the key of the language file.
     *                          	'email_id', // the id on which we have to send message.
     *                          	'fullname', // the name of user for which we have to send the email.
     *                          	'...', // other array according to language file.
     *                          );
     *
     * @return boolean $email_status;
     * */
    public static function sendNow($emailData = array())
    {
        $dateTime       = date('Y-m-d H:i:s');
        $email_template = trans('communication' . DIRECTORY_SEPARATOR . 'emailtemp.' . $emailData['notification_for'] . '.text');
        $replace_data   = trans('communication' . DIRECTORY_SEPARATOR . 'emailtemp.' . $emailData['notification_for'] . '.text_vari');
        $subject        = trans('communication' . DIRECTORY_SEPARATOR . 'emailtemp.' . $emailData['notification_for'] . '.subject');
        if ( ! is_array($replace_data) )
        {
            $replace_data = array();
        }
       // $replace_data_length = count($replace_data);
     //   $replace_count       = 0;
        $replace_with        = array();

        $email_replace_data = json_decode($emailData['email_data']);
        // subject occurrence
        $sub_occu           = strpos($subject, '{{site_name}}');
        //$site_name          = $emailData['application_name'];
        $site_name = config('vwallet.applicationName');
        $appNameKey = config('vwallet.applicationNameKey');

        if ( $sub_occu !== FALSE )
        {
            $subject = str_replace('{{site_name}}', $site_name, $subject);
        }
        /*
        // each email will have the subject that's why the count will update by default.
        $replace_data_length = $replace_data_length - 1;
        $replace_count       = $replace_count + 1;

        if ( in_array('fullname', $replace_data) )
        {
            $replace_with[0]     = $email_replace_data->fullname;
            $replace_data_length = $replace_data_length - 1;
            $replace_count       = $replace_count + 1;
        }

        $replace_with[1] = $subject;

        for ( $i = 0; $i < $replace_data_length; $i ++  )
        {
            $email_data_key = ( int ) $replace_count;
            $key            = $replace_data{$email_data_key};

            $replace_with{$replace_count} = $email_replace_data->$key;
            $replace_count ++;
        } */
        foreach( $replace_data as $key => $val )
        {
            if( $email_replace_data->$val )
            {
                $replace_with[] = $email_replace_data->$val;
            }
            $replace_data[$key] = '{{' . $val . '}}';
        }
        if( ! isset($replace_data['site_name']) )
        {
            $replace_data[] = '{{site_name}}';
            $replace_with[] = $site_name;
        }
        
        $final_email_content       = str_ireplace($replace_data, $replace_with, $email_template);
    //    $final_email_content = str_ireplace(array( '{{', '}}' ), array( '', '' ), $email_content);

        $email_body      = Sendemail_lib::_mergeEmailData( $appNameKey, $subject, $final_email_content);
        $send_mail_array = array( 'site_name' => $site_name, 'subject' => strip_tags($subject), 'to_email' => $emailData['email_id'], 'to_name' => ucwords(strtolower($replace_with[0])), 'mail_body' => $email_body, 'attachment' => '', );
    //    $send_mail_array['to_email'] = 'mahesh.bairi@violamoney.com';//for test
        $email_status = emailNow::sendEmail($send_mail_array);
        if ( $email_status === TRUE )
        {
            $ReceipentAcknowledged = 'Y';
        }
        else
        {
            $ReceipentAcknowledged = 'N';
        }


        // add records into database for log
        $email_notification_log = [
            'PartyID'                 => $emailData['party_id'],
            'LinkedTransactionID'     => $emailData['linked_id'],
            'NotificationType'        => 'Email',
            'SentDateTime'            => $dateTime,
            'ReceipentAcknowledged'   => $ReceipentAcknowledged,
            'ReceipentReceivedDate'   => $dateTime,
            'ReceipentDeliveryReport' => $email_status,
            'CreatedBy'               => config('communication/config_v.CreatedBy'),
            'CreatedDateTime'         => $dateTime,
            'ToAddress'               => $emailData['email_id'],
        ];

        // add records into database for log parameters
        $email_notificationlogparameters = [
            'NotificationID'    => '',
            'TemplateParameter' => $emailData['email_data'],
            'TemplateValue'     => $emailData['notification_for'], //$email_body,
            'CreatedDateTime'   => $dateTime,
            'CreatedBy'         => config('communication/config_v.CreatedBy'),
        ];
        Functions::createCommunicationLog($email_notification_log, $email_notificationlogparameters);

        return $email_status;
    }

    /**
     * ---------------------------------------------------------------------
     * _get_email_template method.
     * ---------------------------------------------------------------------
     * The method used to get the email tempalte from the folder and this will
     * merge into the data.
     *
     * @return string $file_content_data;
     * */
    private function _getEmailTemplate()
    {
        $tempalate_name    = 'email_template.html';
        $complete_path     = DOCUMENTPATH . 'other/email-template/' . $tempalate_name;
        $file_content      = fopen($complete_path, 'r');
        $file_content_data = fread($file_content, filesize($complete_path));

        return $file_content_data;
    }

    /**
     * ---------------------------------------------------------------------
     * _merge_email_data method.
     * ---------------------------------------------------------------------
     * The method used to merge the subject and the email content into the template
     * this methos call one more private method called _getEmailTemplate() which
     * return the email template from the folder.
     *
     * @param string $subject      ;
     * @param string $mail_content ;
     *
     * @return string $final_email_body;
     * */
    private static function _mergeEmailData($app_name, $subject = '', $mail_content = '')
    {
        // $email_template = $this->_getEmailTemplate();
        // $email_template = $CI->load->view('email_templates/'.strtolower($app_name).'_email', '', TRUE);
        $email_template   = view('communication' . DIRECTORY_SEPARATOR . 'email_templates.' . strtolower($app_name));
        $replace_to       = array( '{{subject}}', '{{mail_contant}}' );
        $replace_with     = array( $subject, $mail_content );
        $final_email_body = str_ireplace($replace_to, $replace_with, $email_template);

        return $final_email_body;
    }

}

/* End of file Sendemail_lib.php */