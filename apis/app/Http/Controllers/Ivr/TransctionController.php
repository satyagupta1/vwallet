<?php

/**
 * ProfileController file
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */

namespace App\Http\Controllers\Ivr;

/**
 * ProfileController class
 * @category  PHP
 * @package   V-Card
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class TransctionController extends \App\Http\Controllers\Controller {

    // records fetch limit
    private $recLimit      = 5;
    // deposit count
    private $depositLimit  = 3;
    // withdrawal count
    private $withdrawLimit = 3;

    /*
     *  recentTransction - Get Recent Transction List
     *  @param Request $request
     *  @return JSON
     */

    public function recentTransction(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        $userInfo = $validateUser['res_data'];

        $partyId     = $userInfo->UserWalletID;
        // select fields
        $selectArray = array(
            'transactiondetail.TransactionDetailID',
            'transactiondetail.Amount',
            'transactiondetail.TransactionDate',
            'transactiondetail.TransactionMode',
            'transactiondetail.TransactionTypeCode',
            'transactiondetail.TransactionStatus',
            'transactiondetail.TransactionType',
            'transactiondetail.TransactionVendor',
            'transactiondetail.TransactionOrderID',
        );

        $walletSend = array(
            'userwallet.FirstName as reciverName',
        );
        $selectarr  = array_merge($selectArray, $walletSend);

        $walletReciver = array(
            'userwallet.FirstName as senderName',
        );
        $selectRearr   = array_merge($selectArray, $walletReciver);

        $topupUpi       = $this->topupUpi($selectArray, $partyId, $this->recLimit);
        $topupNonUpi    = $this->topupNonUpi($selectArray, $partyId, $this->recLimit);
        $walletTransfer = $this->walletTransfer($selectarr, $partyId, $this->recLimit);
        $walletRecive   = $this->walletRecived($selectRearr, $partyId, $this->recLimit);
        $peerTransfer   = $this->p2pTransfer($selectArray, $partyId, $this->recLimit);
        $withdraw       = $this->withdraw($selectArray, $partyId, $this->recLimit);

        $result    = array_merge($topupUpi, $topupNonUpi, $walletTransfer, $walletRecive, $peerTransfer, $withdraw);
//        print_r($result);
        $topResult = $result;
        if ( $result && count($result) > $this->recLimit )
        {
            // order the reports based on transction Id
            usort($result, array( $this, 'cmp' ));
            // return top reult based on limit
            $topResult = array_slice($result, 0, $this->recLimit);
        }

        $output_arr['res_data'] = ($topResult) ? $topResult : array( 'info' => trans('messages.emptyRecords') );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    /*
     *  recentDeposits - Get Recent Deposit
     *  @param Request $request
     *  @return JSON
     */

    public function recentDeposits(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        $userInfo = $validateUser['res_data'];

        $partyId     = $userInfo->UserWalletID;
        // select fields
        $selectArray = array(
            'transactiondetail.TransactionDetailID',
            'transactiondetail.Amount',
            'transactiondetail.TransactionDate',
            'transactiondetail.TransactionMode',
            'transactiondetail.TransactionTypeCode',
            'transactiondetail.TransactionStatus',
            'transactiondetail.TransactionType',
            'transactiondetail.TransactionVendor',
            'transactiondetail.TransactionOrderID',
        );

        $topupUpi    = $this->topupUpi($selectArray, $partyId, $this->depositLimit);
        $topupNonUpi = $this->topupNonUpi($selectArray, $partyId, $this->depositLimit);

        $result    = array_merge($topupUpi, $topupNonUpi);
//        print_r($result);
        $topResult = $result;
        if ( $result && count($result) > $this->depositLimit )
        {
            // order the reports based on transction Id
            usort($result, array( $this, 'compareArray' ));
            // return top reult based on limit
            $topResult = array_slice($result, 0, $this->depositLimit);
        }

        $output_arr['res_data'] = ($topResult) ? $topResult : array( 'info' => trans('messages.emptyRecords') );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    /*
     *  recentWithdrawal - Get Recent Withdrawals
     *  @param Request $request
     *  @return JSON
     */

    public function recentWithdrawal(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr = array( 'is_error' => TRUE, 'status_code' => 200, 'res_data' => array() );

        // call validateUser Method for common validation 
        $validateUser = \App\Libraries\IvrHelper::userVerify($request, '_userValidation');
        if ( $validateUser['is_error'] === TRUE )
        {
            return $validateUser;
        }
        $userInfo = $validateUser['res_data'];

        $partyId     = $userInfo->UserWalletID;
        // select fields
        $selectArray = array(
            'transactiondetail.TransactionDetailID',
            'transactiondetail.Amount',
            'transactiondetail.TransactionDate',
            'transactiondetail.TransactionMode',
            'transactiondetail.TransactionTypeCode',
            'transactiondetail.TransactionStatus',
            'transactiondetail.TransactionType',
            'transactiondetail.TransactionVendor',
            'transactiondetail.TransactionOrderID',
        );

        $result = $this->withdraw($selectArray, $partyId, $this->withdrawLimit);

//        print_r($result);
        $topResult = $result;
        if ( $result && count($result) > $this->withdrawLimit )
        {
            // order the reports based on transction Id
            usort($result, array( $this, 'cmp' ));
            // return top reult based on limit
            $topResult = array_slice($result, 0, $this->withdrawLimit);
        }

        $output_arr['res_data'] = ($topResult) ? $topResult : array( 'info' => trans('messages.emptyRecords') );
        $output_arr['is_error'] = FALSE;
        return $output_arr;
    }

    /*
     *  compareArray - Compare Array by key for filtering highest value
     *  @param Request $request
     *  @return JSON
     */

    public function compareArray($a, $b)
    {
        if ( $a['TransactionDetailID'] == $b['TransactionDetailID'] )
        {
            return 0;
        }
        return ($a['TransactionDetailID'] > $b['TransactionDetailID']) ? -1 : 1;
    }

    /*
     *  topupUpi - Topup query for UPI transctions
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function topupUpi($select, $partyId, $count)
    {
        // topup UPI        
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('userexternalbeneficiary', 'transactiondetail.FromAccountId', '=', 'userexternalbeneficiary.BeneficiaryID')
                        ->where('transactiondetail.FromAccountType', 'ExternalCardID')
                        ->where('transactiondetail.ToAccountType', 'WalletAccount')
                        ->where('transactiondetail.TransactionVendor', '=', 'UPI')
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();
        return $query;
    }

    /*
     *  topupNonUpi - Topup query for Non UPI transctions
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function topupNonUpi($select, $partyId, $count)
    {
        // topup non UPI
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                        ->where('transactiondetail.FromAccountType', 'ExternalCardID')
                        ->where('transactiondetail.ToAccountType', 'WalletAccount')
                        ->where('transactiondetail.TransactionVendor', '!=', 'UPI')
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();
        return $query;
    }

    /*
     *  walletTransfer - Wallet to wallet transctions for sent money
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function walletTransfer($select, $partyId, $count)
    {
        // wallet to wallet debit
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('partypreferredpaymentmethod', 'transactiondetail.FromAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                        ->join('userwallet', 'transactiondetail.ToAccountId', '=', 'userwallet.UserWalletID')
                        ->where('transactiondetail.FromAccountType', 'WalletAccount')
                        ->where('transactiondetail.ToAccountType', 'WalletAccount')
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();
        return $query;
    }

    /*
     *  walletRecived - Wallet to wallet transctions for recived money
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function walletRecived($select, $partyId, $count)
    {
        // wallet to wallet credit
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('partypreferredpaymentmethod', 'transactiondetail.ToAccountId', '=', 'partypreferredpaymentmethod.PaymentMethodID')
                        ->join('userwallet', 'transactiondetail.UserWalletId', '=', 'userwallet.UserWalletID')
                        ->where('transactiondetail.FromAccountType', 'WalletAccount')
                        ->where('transactiondetail.ToAccountType', 'WalletAccount')
                        ->where('transactiondetail.ToAccountId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();
        return $query;
    }

    /*
     *  p2pTransfer - Query for UPI peer to peer transctions
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function p2pTransfer($select, $partyId, $count)
    {
        // Peer to peer transction from UPI
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('userexternalbeneficiary', 'transactiondetail.FromAccountId', '=', 'userexternalbeneficiary.BeneficiaryID')
                        ->where('transactiondetail.FromAccountType', 'ExternalCardID')
                        ->where('transactiondetail.ToAccountType', 'ExternalCardID')
                        ->where('transactiondetail.TransactionVendor', '=', 'UPI')
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();

        return $query;
    }

    /*
     *  withdraw - Query for withdrawal transctions
     *  @param Array $select
     *  @param Int $partyId
     *  @param Int $count
     *  @return Array
     */

    public function withdraw($select, $partyId, $count)
    {
        // topup UPI        
        $query = \App\Models\Transactiondetail::select($select)
                        ->join('userexternalbeneficiary', 'transactiondetail.FromAccountId', '=', 'userexternalbeneficiary.BeneficiaryID')
                        ->where('transactiondetail.FromAccountType', 'WalletAccount')
                        ->where('transactiondetail.ToAccountType', 'ExternalCardID')
                        ->where('transactiondetail.TransactionVendor', '=', 'UPI')
                        ->where('transactiondetail.UserWalletId', $partyId)
                        ->orderBy('transactiondetail.TransactionDetailID', 'DESC')
                        ->take($count)
                        ->get()->toArray();
        return $query;
    }

}
