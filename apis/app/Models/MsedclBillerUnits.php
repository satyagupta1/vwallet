<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property string $CountryCode
 * @property string $CountryName
 * @property string $ISDCode
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $currencies
 *
 * @package App\Models
 */
class MsedclBillerUnits extends Eloquent
{
	protected $table = 'msedclbillerunits';
	protected $primaryKey = 'Id';
	public $incrementing = false;
	public $timestamps = false;

	
	protected $fillable = [
		'Id',
		'TorrentId',
		'BillingNo',
	];


}
