<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Communicationtemplate
 * 
 * @property int $TemplateID
 * @property string $TemplateName
 * @property string $TemplatePath
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $communicationeventconfigs
 * @property \Illuminate\Database\Eloquent\Collection $communicationtemplateparameters
 *
 * @package App\Models
 */
class Communicationtemplate extends Eloquent
{
	protected $table = 'communicationtemplate';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TemplateID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TemplatePath',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function communicationeventconfigs()
	{
		return $this->hasMany(\App\Models\Communicationeventconfig::class, 'SMSTemplateID');
	}

	public function communicationtemplateparameters()
	{
		return $this->hasMany(\App\Models\Communicationtemplateparameter::class, 'TemplateID');
	}
}
