<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Dailytransactionlog
 * 
 * @property int $DailyTransactionSequenceNumber
 * @property int $TransactionLineNumber
 * @property int $TransactionDetailID
 * @property string $EventCode
 * @property \Carbon\Carbon $TransactionDate
 * @property \Carbon\Carbon $ValueDate
 * @property string $BankReconciliationStatus
 * @property int $TransactionWalletAccountId
 * @property int $PartyId
 * @property string $TransactionCurrency
 * @property string $AccountCurrency
 * @property string $LocalCurrency
 * @property float $ExchangeRate
 * @property float $TransactionCcyAmount
 * @property float $LocalCcyAmount
 * @property float $AcctCcyAmount
 * @property string $AmountTag
 * @property string $IsInternalJournalEntry
 * @property int $PaymentsProductID
 * @property string $CrDrIndicator
 * @property int $TransactionMerchantWalletAccountId
 * @property int $TransactionMerchantWalletId
 * @property string $TransactionMerchantWalletCurrencyCode
 * @property string $CreatedBy
 * @property \Carbon\Carbon $CreatedDatetime
 * @property string $UpdatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * 
 * @property \App\Models\Paymentsproduct $paymentsproduct
 * @property \App\Models\Transactiondetail $transactiondetail
 * @property \App\Models\Walletaccount $walletaccount
 * @property \Illuminate\Database\Eloquent\Collection $partypreferredpaymentmethods
 *
 * @package App\Models
 */
class Dailytransactionlog extends Eloquent
{
	protected $table = 'dailytransactionlog';
	protected $primaryKey = 'DailyTransactionSequenceNumber';
	public $timestamps = false;

	protected $casts = [
		'TransactionLineNumber' => 'int',
		'TransactionDetailID' => 'int',
		'TransactionWalletAccountId' => 'int',
		'PartyId' => 'int',
		'ExchangeRate' => 'float',
		'TransactionCcyAmount' => 'float',
		'LocalCcyAmount' => 'float',
		'AcctCcyAmount' => 'float',
		'PaymentsProductID' => 'int',
		'TransactionMerchantWalletAccountId' => 'int',
		'TransactionMerchantWalletId' => 'int'
	];

	protected $dates = [
		'TransactionDate',
		'ValueDate',
		'CreatedDatetime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TransactionLineNumber',
		'TransactionDetailID',
		'EventCode',
		'TransactionDate',
		'ValueDate',
		'BankReconciliationStatus',
		'TransactionWalletAccountId',
		'PartyId',
		'TransactionCurrency',
		'AccountCurrency',
		'LocalCurrency',
		'ExchangeRate',
		'TransactionCcyAmount',
		'LocalCcyAmount',
		'AcctCcyAmount',
		'AmountTag',
		'IsInternalJournalEntry',
		'PaymentsProductID',
		'CrDrIndicator',
		'TransactionMerchantWalletAccountId',
		'TransactionMerchantWalletId',
		'TransactionMerchantWalletCurrencyCode',
		'CreatedBy',
		'CreatedDatetime',
		'UpdatedBy',
		'UpdatedDateTime'
	];

	public function paymentsproduct()
	{
		return $this->belongsTo(\App\Models\Paymentsproduct::class, 'PaymentsProductID');
	}

	public function transactiondetail()
	{
		return $this->belongsTo(\App\Models\Transactiondetail::class, 'TransactionDetailID');
	}

	public function walletaccount()
	{
		return $this->belongsTo(\App\Models\Walletaccount::class, 'TransactionMerchantWalletAccountId')
					->where('walletaccount.WalletAccountId', '=', 'dailytransactionlog.TransactionMerchantWalletAccountId')
					->where('walletaccount.UserWalletId', '=', 'dailytransactionlog.TransactionMerchantWalletId');
	}

	public function partypreferredpaymentmethods()
	{
		return $this->hasMany(\App\Models\Partypreferredpaymentmethod::class, 'DailyTransactionSequenceNumber');
	}
}
