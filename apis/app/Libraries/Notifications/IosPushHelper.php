<?php

namespace App\Libraries\Notifications;

class IosPushHelper {

    public static function sendNotification($deviceToken, $message, $notificationId)
    {
        // Log variables
        $requestId      = uniqid();
        $startTime      = microtime(TRUE);
        // Log Messge 
        $requestDetails = 'IOS Request ID : ' . $requestId . ', Start Time : ' . date('d-m-Y H:i:s.u') . ', decviceId: ' . $deviceToken . ', Templet : ' . json_encode($message, JSON_PRETTY_PRINT);
        \Log::info('IOS message :  ' .json_encode($requestDetails, JSON_PRETTY_PRINT)); 
        
        $notifyUpdate = array(
            'ReceipentReceivedDate' => date('Y-m-d H:i:s')
        );

        if ( ! $deviceToken )
        {
            return 'device unique id required.';
        }

        if ( ! $message )
        {
            return 'Please enter message text.';
        }

        $ctx = stream_context_create();
        
        // devVWallet.pem.pem is our certificate file
        stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__ . '\devVWallet.pem');

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if ( ! $fp )
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = $err . ' ' . $errstr;
            
            \Log::info('Failed to connect IOS service :  ' . $err . ' ' . $errstr);
            \App\Models\Notificationlog::where('NotificationID', $notificationId)->update($notifyUpdate);
            
            return FALSE;
        }
        
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        
//        \Log::info('IOS message :  ' .json_encode($body, JSON_PRETTY_PRINT));

        // Encode the payload as JSON
        $payload = json_encode($body);
        
        // Build the binary notification
        $msg     = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);

        if ( ! $result )
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'N';
            $notifyUpdate['ReceipentDeliveryReport'] = $result;

            $requestDetails = 'IOS Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . ' Message Status: ' . $result;
            \Log::info('Reponse:  ' . $requestDetails);
        }
        else
        {
            $notifyUpdate['ReceipentAcknowledged']   = 'Y';
            $notifyUpdate['ReceipentDeliveryReport'] = json_encode($result);

            $requestDetails = 'IOS Request ID : ' . $requestId . ', End Time : ' . date('d-m-Y H:i:s.u') . ', Duration : ' . number_format(microtime(true) - $startTime, 3) . ' Message Status: Sent ' . $result;
            \Log::info('Reponse:  ' . $requestDetails);
        }

        \App\Models\Notificationlog::where('NotificationID', $notificationId)->update($notifyUpdate);
        return $result;
    }

}
