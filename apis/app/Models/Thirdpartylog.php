<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 May 2018 11:36:35 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Thirdpartylog
 * 
 * @property int $Id
 * @property int $partyId
 * @property string $requestId
 * @property string $thirdPartyName
 * @property string $thirdPartyCall
 * @property string $referenceId
 * @property string $referenceTable
 * @property string $request
 * @property string $response
 * @property string $status
 * @property \Carbon\Carbon $requestOn
 * @property \Carbon\Carbon $responseOn
 *
 * @package App\Models
 */
class Thirdpartylog extends Eloquent
{
        protected $table = 'thirdpartylogs';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $casts = [
		'partyId' => 'int'
	];

	protected $dates = [
		'requestOn',
		'responseOn'
	];

	protected $fillable = [
		'partyId',
		'requestId',
		'thirdPartyName',
		'thirdPartyCall',
		'referenceId',
		'referenceTable',
		'request',
		'response',
		'status',
		'requestOn',
		'responseOn'
	];
}
