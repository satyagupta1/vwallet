<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Addresstype
 * 
 * @property string $AddressTypeCode
 * @property string $TypeDescription
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partyaddresses
 *
 * @package App\Models
 */
class Addresstype extends Eloquent
{
	protected $table = 'addresstype';
	protected $primaryKey = 'AddressTypeCode';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'TypeDescription',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function partyaddresses()
	{
		return $this->hasMany(\App\Models\Partyaddress::class, 'AddressTypeCode');
	}
}
