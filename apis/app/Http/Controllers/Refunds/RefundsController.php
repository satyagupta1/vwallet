<?php

namespace App\Http\Controllers\Refunds;

/**
 * NotificationsController class
 * @category  PHP
 * @package   V-Wallet
 * @author    Viola Services (India) PVT LTD <development@viola.group>
 * @license   http://vwallet.in Licence
 */
class RefundsController extends \App\Http\Controllers\Controller {

    /**
     * This used to call _validationCheck_notifications method in this list_notifications method
     * @desc In this we will check _validationCheck_notifications method for validation of party id , limit , offset
     * @desc If all validations have no errors will call _all_user_notifications method to get list of all Notification details in array list 
     * 
     * @param PartyId integer
     * @param PartyId integer
     * @param PartyId integer
     *  
     * @return JSON
     */
    public function __construct()
    {
        $this->encKey = sha1(config('vwallet.ENCRYPT_STATIC'));
    }

    /**
     * This used to call validationCheck_wallet method in this listWallets method
     * @desc In this we will check validationCheck_wallet method for validation of party id and device type
     * @desc If all validations have no errors will call _getPartyWalletAccounts and _getPartyExternalCards methods to get list of all wallets details and card details in array list response
     * 
     * @param PartyId integer
     *  
     * @return Array
     */
    // code for reversal transaction of add topup
    public function refundInitiation(\Illuminate\Http\Request $request)
    {
        // Default Response
        $output_arr  = array('is_error' => TRUE, 'display_msg' => array(), 'status_code' => 200, 'res_data' => array());

        // check validation
        $chk_valid = $this->_ValidateRefundInitiation($request);
        if ($chk_valid !== TRUE)
        {
            $output_arr['display_msg'] = $chk_valid;
            return json_encode($output_arr);
        }
        $txnDetailId = $request->input('transactionId');
        $amount = $request->input('amount');
        $transactionDetails = \App\Libraries\PaymentRefund::paymentRefund($txnDetailId, $amount);
        return $transactionDetails;
    }

    // methods for refund money in topup ends

    /**
     * Validate before saving debit card (not used in new design)
     * @desc  this is used to check validation for add topup
     * @param Request $request
     * @param Amount float
     * @param CurrencyCode string
     * @param saveCardlist char
     * @param device char
     * 
     * If there is no errors
     * @return Boolean
     * 
     * If errors are there 
     * @return Array
     */
    private function _ValidateRefundInitiation($request)
    {
        // validations
        $validate = array(
            'transactionId' => 'required|numeric',
            'amount'        => 'numeric'
        );
        $messages = array();

        // validation for mobile device
        $validate_response = \Validator::make($request->all(), $validate, $messages);
        $vali_response     = \App\Libraries\Helpers::validateErrorResponse($validate_response);
        return $vali_response;
    }

}
