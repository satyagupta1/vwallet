<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 18 Aug 2017 05:03:52 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Partyreferral
 * 
 * @property int $PartyID
 * @property string $ReferralCode
 * @property string $ReferralMediumCode
 * @property string $RefereeName
 * @property string $RefereeMobileNumber
 * @property string $RefereeEmailId
 * @property \Carbon\Carbon $CreatedDateTime
 * @property string $CreatedBy
 * @property \Carbon\Carbon $UpdatedDateTime
 * @property string $UpdatedBy
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\Referralmedium $referralmedium
 *
 * @package App\Models
 */
class Partyreferral extends Eloquent
{
	protected $table = 'partyreferral';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'PartyID' => 'int'
	];

	protected $dates = [
		'CreatedDateTime',
		'UpdatedDateTime'
	];

	protected $fillable = [
		'PartyID',
		'ReferralCode',
		'ReferralMediumCode',
		'RefereeName',
		'RefereeMobileNumber',
		'RefereeEmailId',
		'CreatedDateTime',
		'CreatedBy',
		'UpdatedDateTime',
		'UpdatedBy'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'PartyID');
	}

	public function referralmedium()
	{
		return $this->belongsTo(\App\Models\Referralmedium::class, 'ReferralMediumCode');
	}
}
